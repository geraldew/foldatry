#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# deforestry = methods for reducing a tree by finding duplications
#
# This forms part of the foldatry application but can be used as an
# independent command line tool.
#
# --------------------------------------------------
# Copyright (C) 2018-2024  Gerald Waters - Melbourne, Australia
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# ==================================================
# Imports - Standard
# --------------------------------------------------

import sys as im_sys
import os as im_os
# import os.path as osp
import operator

import pathlib as pthlb

import copy as im_copy

# ==================================================
# Imports - Custom
# --------------------------------------------------

import commontry as cmntry
import applitry as apltry

import exploratry as explrtry

from matchsubtry import matchsubtry_command

from congruentry import congruentry_paths_action
from congruentry import congruentry_files_action

from filedupetry import filedupetry_collect
from filedupetry import hasherdabbery

from purgetry import purgetry_path_command
from purgetry import purgetry_file_command
#from purgetry import purgetry_file_command_with_rems

# ==================================================
# Support Stock
# --------------------------------------------------

def ThisIsDeprecated( p_function_name ):
	print( "Function " + p_function_name + "is DEPRECATED !")
	print( "Which means it should not have been called."  )
	input( "Press ENTER to continue regardless, or do Ctrl-C to break and inspect the call stack."  )

# ==================================================
# Support Custom
# --------------------------------------------------

def mod_code_name():
	return "deforestry"

def mod_code_abbr():
	return "dfrstry"

def mod_show_name():
	return "Deforestry"

# --------------------------------------------------
# Support Custom
# --------------------------------------------------

def list_with_item_removed( the_list, the_item):
	# Brief:
	# make a copy of a list, but with a specific item missing
	# Parameters:
	# - the_list
	# - the_item
	# Returns:
	# Notes:
	# was used by a failed method, so can now be removed
	# interesting to note that the failure was from using this function
	# as this is removing an item by matching its value, which thus might remove multiple list tiems
	# when what was needed was to have a list minus a specific item
	# Code:
	build_list = []
	for an_item in the_list :
		if an_item != the_item :
			build_list.append( an_item)
	return build_list

# NOT CURRENTLY USED
def pathlist_self_clash_fail( the_list):
	# Brief:
	# detect if the_item is already listed or clashes as a sub-folder with something in the_list
	# Parameters:
	# Returns:
	# Notes:
	# earlier abandoned method, can be removed 
	# Code:
	# parameters:
	# - a_list & b_list = a lists of paths to test
	any_clash = False
	any_sub = False
	for the_item in the_list :
		test_list = list_with_item_removed( the_list, the_item )
		clash_was, made_sub_was = explrtry.pathlist_path_clash( test_list, False, the_item )
		any_clash = any_clash or clash_was
		any_sub = any_sub or made_sub_was
	return any_clash, any_sub

# NOT CURRENTLY USED
def addtostrlist_ifnotalready( the_list, the_item ):
	# Brief:
	# only add the item if it is not already there as a string
	# Parameters:
	# - the_list
	# - the_item
	# Returns:
	# Notes:
	# Code:
	# Note: this 
	if not (the_item in the_list) :
		the_list.append( the_item)
		#p_multi_log.do_logs( "dbs", "Added: " + the_item )
	# else :
		#p_multi_log.do_logs( "dba", "Already had: " + the_item )

# NOT CURRENTLY USED
def pathstringpair_first_high_level_diff_a( p_pathstr_a, p_pathstr_b):
	# Brief:
	# Parameters:
	# - p_pathstr_a
	# - p_pathstr_b
	# Returns:
	# - a_distinct
	# - b_distinct
	# Notes:
	# Code:
	# a
	lst_split_a = im_os.path.split( p_pathstr_a)
	path_a_head = lst_split_a[0]
	path_a_tail = lst_split_a[1]
	lst_path_a_heads = explrtry.splitall( path_a_head )
	len_a_heads = len( lst_path_a_heads)
	#p_multi_log.do_logs( "db", "Path pair A" )
	#p_multi_log.do_logs( "db", "Path: " + p_pathstr_a )
	#p_multi_log.do_logs( "db", "Tail: " + path_a_tail )
	#p_multi_log.do_logs( "db", "Head length: " + str(len_a_heads) )
	# b
	lst_split_b = im_os.path.split( p_pathstr_b)
	path_b_head = lst_split_b[0]
	path_b_tail = lst_split_b[1]
	lst_path_b_heads = explrtry.splitall( path_b_head )
	len_b_heads = len( lst_path_b_heads)
	#p_multi_log.do_logs( "db", "Path pair B" )
	#p_multi_log.do_logs( "db", "Path: " + p_pathstr_b )
	#p_multi_log.do_logs( "db", "Tail: " + path_b_tail )
	#p_multi_log.do_logs( "db", "Head length: " + str(len_b_heads) )
	# 
	if len_a_heads <= len_b_heads :
		len_2_check = len_a_heads
	else :
		len_2_check = len_b_heads
	no_diff = True
	i = 0
	while i < len_2_check and no_diff :
		no_diff = lst_path_a_heads[i] == lst_path_b_heads[i]
		if no_diff :
			i = i + 1
	if no_diff :
		if path_a_tail == path_b_tail :
			a_distinct = ""
			b_distinct = ""
			#p_multi_log.do_logs( "db", "no difference after all" )
		else :
			# difference must just be the tail so return the full paths
			a_distinct = p_pathstr_a
			b_distinct = p_pathstr_b
			#p_multi_log.do_logs( "db", "difference must just be the tail" )
	else :
		a_distinct = im_os.path.join( *lst_path_a_heads[0:i])
		b_distinct = im_os.path.join( *lst_path_b_heads[0:i])
		#p_multi_log.do_logs( "db", "difference in the path" )
	return a_distinct, b_distinct

# NOT CURRENTLY USED
def pathstringpair_first_high_level_diff( p_pathstr_a, p_pathstr_b):
	# Brief:
	# Parameters:
	# - p_pathstr_a
	# - p_pathstr_b
	# Returns:
	# - a_distinct
	# - b_distinct
	# Notes:
	# Code:
	# a
	lst_split_a = explrtry.splitall( p_pathstr_a)
	len_split_a = len( lst_split_a)
	#p_multi_log.do_logs( "dbs", "Path pair A" )
	#p_multi_log.do_logs( "dbs", "Path: " + p_pathstr_a )
	#p_multi_log.do_logs( "dbs", "Head: " + lst_split_a[0] )
	#p_multi_log.do_logs( "dbs", "Tail: " + lst_split_a[-1] )
	#p_multi_log.do_logs( "dbs", "List length: " + str(len_split_a) )
	# b
	lst_split_b = explrtry.splitall( p_pathstr_b)
	len_split_b = len( lst_split_b)
	#p_multi_log.do_logs( "dbs", "Path pair B" )
	#p_multi_log.do_logs( "dbs", "Path: " + p_pathstr_b )
	#p_multi_log.do_logs( "dbs", "Head: " + lst_split_b[0] )
	#p_multi_log.do_logs( "dbs", "Tail: " + lst_split_b[-1] )
	#p_multi_log.do_logs( "dbs", "List length: " + str(len_split_b) )
	if len_split_a <= len_split_b :
		len_2_check = len_split_a
	else :
		len_2_check = len_split_b
	no_diff = True
	i = 0
	while i < len_2_check and no_diff :
		no_diff = ( lst_split_a[i] == lst_split_b[i] )
		if no_diff :
			i = i + 1
	if no_diff :
		a_distinct = ""
		b_distinct = ""
		#p_multi_log.do_logs( "dbs", "no difference after all" )
	else :
		a_distinct = im_os.path.join( *lst_split_a[0:i+1])
		b_distinct = im_os.path.join( *lst_split_b[0:i+1])
		#p_multi_log.do_logs( "dbs", "difference in the path" )
	return a_distinct, b_distinct

# NOT CURRENTLY USED
def pathstring_first_high_level_diff_to_pathstring( p_pathstr_a, p_pathstr_ref ):
	# Brief:
	# Parameters:
	# - p_pathstr_a
	# - p_pathstr_ref
	# Returns:
	# - a_distinct
	# - i
	# Notes:
	# Code:
	# a
	lst_split_a = explrtry.splitall( p_pathstr_a)
	len_split_a = len( lst_split_a)
	# if True:
		#p_multi_log.do_logs( "dbs", "Path pair A" )
		#p_multi_log.do_logs( "dbs", "Path: " + p_pathstr_a )
		#p_multi_log.do_logs( "dbs", "Head: " + lst_split_a[0] )
		#p_multi_log.do_logs( "dbs", "Tail: " + lst_split_a[-1] )
		#p_multi_log.do_logs( "dbs", "List length: " + str(len_split_a) )
	# b
	lst_split_r = explrtry.splitall( p_pathstr_ref)
	len_split_r = len( lst_split_r)
	# if True:
		#p_multi_log.do_logs( "dbs", "Path pair B" )
		#p_multi_log.do_logs( "dbs", "Path: " + p_pathstr_b )
		#p_multi_log.do_logs( "dbs", "Head: " + lst_split_b[0] )
		#p_multi_log.do_logs( "dbs", "Tail: " + lst_split_b[-1] )
		#p_multi_log.do_logs( "dbs", "List length: " + str(len_split_b) )
	# 
	if len_split_a <= len_split_r :
		len_2_check = len_split_a
	else :
		len_2_check = len_split_r
	no_diff = True
	i = 0
	while i < len_2_check and no_diff :
		no_diff = ( lst_split_a[i] == lst_split_r[i] )
		if no_diff :
			i = i + 1
	if no_diff :
		a_distinct = ""
		# b_distinct = ""
		#p_multi_log.do_logs( "dbs", "no difference after all" )
	else :
		a_distinct = im_os.path.join( *lst_split_a[0:i+1])
		#p_multi_log.do_logs( "dbs", "difference in the path" )
	#p_multi_log.do_logs( "dbs", str(i) )
	return a_distinct, i


# ==================================================
# Features - i.e. requiring awareness of custom structures
# --------------------------------------------------

# --------------------------------------------------
# Approach == as Lists of Folders
# --------------------------------------------------

def does_this_folderlist_have_survivors_vs_disposals( p_lst_foldertpls, p_lst_paths_survive, p_lst_paths_dispose ):
	# Brief:
	# Parameters:
	# - p_lst_foldertpls
	# - p_lst_paths_survive
	# - p_lst_paths_dispose
	# Returns:
	# - in_both
	# - lst_survive
	# - lst_dispose
	# Notes:
	# Code:
	log_strl_str = "b"
	log_prefix = "DTFLHSVD_"
	#p_multi_log.do_logs( log_strl_str, "does_this_folderlist_have_survivors_vs_disposals" )
	#p_multi_log.do_logs( log_strl_str, log_prefix + "len of p_lst_foldertpls is " + str(len(p_lst_foldertpls)) )
	lst_survive = []
	lst_dispose = []
	in_survive_any = False
	in_dispose_any = False
	in_both = False
	i = 0
	for tpl_pi in p_lst_foldertpls :
		i = i + 1
		s_i = "(" + str(i) + ")"
		# per folder
		f_path = tpl_pi.pi_thispath
		#p_multi_log.do_logs( log_strl_str, log_prefix + s_i + "testing path: " + f_path )
		# survivors
		in_survives = False
		j = 0
		for f_survive in p_lst_paths_survive :
			j = j + 1
			s_j = "(s" + str(j) + ")"
			#p_multi_log.do_logs( log_strl_str, log_prefix + s_i + s_j + "-against survivor: " + f_survive )
			sbfldrnss_survive = explrtry.subfolderness_a_b( f_path, f_survive )
			in_this_survive = sbfldrnss_survive in ["a", "="]
			#p_multi_log.do_logs( log_strl_str, log_prefix + s_i + s_j + "in_this_survive: " + str(in_this_survive) )
			in_survives = in_survives or in_this_survive
			#p_multi_log.do_logs( log_strl_str, log_prefix + s_i + s_j + "in_survives: " + str(in_survives) )
		if in_survives :
			lst_survive.append( f_path)
		in_survive_any = in_survive_any or in_survives
		#p_multi_log.do_logs( log_strl_str, log_prefix + s_i + "in_survive_any: " + str(in_survive_any) )
		# disposals
		in_disposes = False
		j = 0
		for f_dispose in p_lst_paths_dispose :
			j = j + 1
			s_j = "(d" + str(j) + ")"
			#p_multi_log.do_logs( log_strl_str, log_prefix + s_i + s_j + "against disposal: " + f_dispose )
			sbfldrnss_dispose = explrtry.subfolderness_a_b( f_path, f_dispose )
			in_this_dispose = sbfldrnss_dispose in ["a", "="]
			#p_multi_log.do_logs( log_strl_str, log_prefix + s_i + s_j + "in_this_dispose: " + str(in_this_dispose) )
			in_disposes = in_disposes or in_this_dispose
			#p_multi_log.do_logs( log_strl_str, log_prefix + s_i + s_j + "in_disposes: " + str(in_disposes) )
		if in_disposes :
			lst_dispose.append( f_path)
		in_dispose_any = in_dispose_any or in_disposes
		#p_multi_log.do_logs( log_strl_str, log_prefix + s_i + "in_dispose_any: " + str(in_dispose_any) )
	in_both = in_survive_any and in_dispose_any
	if in_both :
		pass
		#p_multi_log.do_logs( log_strl_str, log_prefix + "in both overall: " + str(in_both) )
	else :
		pass
		#lst_survive = []
		#lst_dispose = []
	return in_both, lst_survive, lst_dispose

def check_then_purge( p_lst_survive, p_lst_dispose, p_Cngrntry_FnameMatchSetting_S, p_Cngrntry_FnameMatchSetting_D, \
		p_docongruentbeforedelete, p_congruentstringent, p_ReportFailuresStringent, \
		p_EnumDeleteMode, p_bashfilename_str, p_size, p_fn_Check_NoInterruption, p_multi_log ):
	# Brief:
	# this operates on lists of folders where they are expected to be identical 
	# and merely need a final check before deleting those in the disposal list
	# Parameters:
	# - p_lst_survive
	# - p_lst_dispose
	# - p_EnumDeleteMode - method control to pass to purgetry
	# - p_bashfilename_str - filename of script to generate
	# Returns:
	# - can_purge
	# - did_purge
	# Notes:
	# Code:
	p_multi_log.do_logs( "dbp", "check_then_purge" )
	len_p_lst_survive = len(p_lst_survive)
	len_p_lst_dispose = len(p_lst_dispose)
	p_multi_log.do_logs( "dbp", "p_lst_survive" + " has " + str( len_p_lst_survive) )
	p_multi_log.do_logs( "dbp", "p_lst_dispose" + " has " + str( len_p_lst_dispose) )
	can_purge = False
	did_purge = False
	if ( len_p_lst_survive > 0 ) and ( len_p_lst_dispose > 0 ) :
		f_s = p_lst_survive[0]
		for f_d in p_lst_dispose :
			if p_fn_Check_NoInterruption :
				if p_docongruentbeforedelete :
					t_StopNotCongruent = True
					t_StopNotMoiety = True
					t_ReportDiffs = False
					t_SummaryDiffs = False
					t_GenCopyScript = False 
					p_multi_log.do_logs( "db", "Call congruentry" )
					chckd_congruent, chckd_subset, chckd_extra_side = \
						congruentry_paths_action( \
							f_s, f_d, p_Cngrntry_FnameMatchSetting_S, p_Cngrntry_FnameMatchSetting_D, \
							p_congruentstringent, t_StopNotCongruent, t_StopNotMoiety, \
							t_ReportDiffs, t_SummaryDiffs, p_ReportFailuresStringent, t_GenCopyScript, \
							p_multi_log )
					p_multi_log.do_logs( "pdb", "Got chckd_congruent: " + str(chckd_congruent) )
					p_multi_log.do_logs( "pdb", "Got chckd_subset: " + str(chckd_subset) )
					p_multi_log.do_logs( "pdb", "Got chckd_extra_side: " + str(chckd_extra_side) )
					can_purge = ( chckd_congruent and chckd_subset and chckd_extra_side == "=" )
				else :
					# not doing a congruency check
					can_purge = True
				if can_purge :
					p_multi_log.do_logs( "db", "Can now call purgetry for: " + cmntry.pfn4print( f_d ) )
					did_purge = purgetry_path_command( f_d, p_EnumDeleteMode, p_bashfilename_str, p_size, p_multi_log )
			else :
				break
	return can_purge, did_purge

def make_dct_hash_sizes( p_dct_hash_lst_fldrs ):
	# Brief:
	# sort the hashes by size
	# get the largest hash
	# Parameters:
	# - p_dct_hash_lst_fldrs
	# Returns:
	# - dct_size_hashes
	# Notes:
	# Code:
	#p_multi_log.do_logs( "dbs", "make_dct_hash_sizes" )
	dct_size_hashes = {}
	for k_hsh in p_dct_hash_lst_fldrs.keys() :
		#p_multi_log.do_logs( "b", "hash: " + k_hsh )
		if len( p_dct_hash_lst_fldrs[k_hsh] ) > 1 :
			# get the size from the first folder
			v_lst_flr_item_0 = p_dct_hash_lst_fldrs[k_hsh][0]
			# that gets an item of type tpl_PathInfo = namedtuple('tpl_PathInfo', 'pi_thispath pi_parentpath pi_foldcount pi_filecount pi_sizesum')
			this_siz = v_lst_flr_item_0.pi_sizesum
			#p_multi_log.do_logs( "b", "size: " + str( this_siz) )
			if this_siz in dct_size_hashes :
				# add to hash list for that size
				dct_size_hashes[this_siz].append( k_hsh )
				#p_multi_log.do_logs( "b", "added to list at that size" )
			else :
				# make an entry for that size
				dct_size_hashes[this_siz] = [k_hsh]
				#p_multi_log.do_logs( "b", "made a list at that size" )
		else :
			pass
			#p_multi_log.do_logs( "b", "Only one folder for that hash" )
	return dct_size_hashes

def purge_break_after_dflt() :
	return 1000

def purge_break_after_maximum() :
	return 1000000

def process_and_purge_by_hash_from_largest( p_dct_hash_lst_fldrs, \
		p_lst_paths_survive, p_lst_paths_dispose, p_Cngrntry_FnameMatchSetting_S, p_Cngrntry_FnameMatchSetting_D, \
		p_break_after, p_docongruentbeforedelete, p_congruentstringent, p_ReportFailuresStringent, p_EnumDeleteMode, p_bashfilename_str, p_fn_Check_NoInterruption, p_multi_log ):
	# Brief:
	# see if it has folders in both disposals and survivors
	# if Yes then send the disposals for purging
	# Parameters:
	# - p_dct_hash_lst_fldrs - reference dictionary of hashes and the lists of folders of those hashes
	# - p_lst_paths_survive - list of paths to survive
	# - p_lst_paths_dispose - list of paths where things can be disposed
	# - p_break_after - set a maximum number of purgings to do (really, a debugging feature)
	# - p_docongruentbeforedelete
	# - p_congruentstringent
	# - p_EnumDeleteMode - method control to pass to purgetry
	# - p_bashfilename_str - filename of script to generate
	# Returns:
	# did_some_by_hash
	# Notes:
	# Code:
	p_multi_log.do_logs( "dbs", "process_and_purge_by_hash_from_largest" )
	# impose and upper limit on the folder trees to purge - this is a during-development thing, remove later
	if p_break_after < 0 :
		break_after = purge_break_after_maximum()
	elif p_break_after > 0 :
		break_after = p_break_after
	else:
		break_after = purge_break_after_dflt()
	p_multi_log.do_logs( "dbs", "will only attempt to purge " + str(break_after) + " folder trees" )
	did_some_by_hash = False
	dct_size_hashes = make_dct_hash_sizes( p_dct_hash_lst_fldrs )
	sh_sz_count = len( dct_size_hashes)
	p_multi_log.do_logs( "db", "there are " + str( sh_sz_count) + " sizes of hashes." )
	if sh_sz_count > 0 :
		# get desending list of sizes
		sz_lst = sorted( list( dct_size_hashes.keys() ), reverse=True )
		sz_at = 0
		# for the top n sizes
		for siz in sz_lst:
			if p_fn_Check_NoInterruption():
				# for each size
				# limit to top n
				sz_at = sz_at + 1
				if sz_at > break_after:
					break
				p_multi_log.do_logs( "db", "Hash size #" + str( sz_at) + " of" + str( sh_sz_count) + " size = " + cmntry.hndy_GetHumanReadable(siz) + " " + str( siz) )
				# get the hashes
				h_l = dct_size_hashes[siz]
				if __debug__:
					p_multi_log.do_logs( "b", "has " + str( len( h_l)) + " hashes." )
				hz_count = len( h_l)
				hz_at = 0
				for hsh in h_l:
					# per hash
					hz_at = hz_at + 1
					p_multi_log.do_logs( "db", "Hash #" + str( hz_at) + " of " + str( hz_count) + " hash: " + hsh )
					# for each hash, get the list of folders
					lst_fldrtpl = p_dct_hash_lst_fldrs[hsh]
					if __debug__:
						p_multi_log.do_logs( "b", "This hash has " + str( len( lst_fldrtpl) ) + " paths: " )
						p_multi_log.do_logs( "b", "Calling: does_this_folderlist_have_survivors_vs_disposals" )
					has_both, lst_survive, lst_dispose = does_this_folderlist_have_survivors_vs_disposals( \
						lst_fldrtpl, p_lst_paths_survive, p_lst_paths_dispose )
					if has_both :
						if __debug__:
							p_multi_log.do_logs( "dbs", "We can automate purging for hash: " + hsh + " of size = " + cmntry.hndy_GetHumanReadable(siz) )
							p_multi_log.do_logs( "dbs", "Purging " + str( len( lst_dispose) ) + " paths: " )
							p_multi_log.do_logs( "dbs", " an example is:" + cmntry.pfn4print( lst_dispose[0] ) )
							p_multi_log.do_logs( "dbs", "Keeping " + str( len( lst_survive) ) + " paths: " )
							p_multi_log.do_logs( "dbs", " an example is:" + cmntry.pfn4print( lst_survive[0] ) )
						can_purge, did_purge = check_then_purge( lst_survive, lst_dispose, p_Cngrntry_FnameMatchSetting_S, p_Cngrntry_FnameMatchSetting_D, \
							p_docongruentbeforedelete, p_congruentstringent, p_ReportFailuresStringent, p_EnumDeleteMode, p_bashfilename_str, siz, p_fn_Check_NoInterruption, p_multi_log )
						if __debug__:
							p_multi_log.do_logs( "dbs", cmntry.hndy_str_of_boolean( can_purge, "Wanted purged: " ) )
							p_multi_log.do_logs( "dbs", cmntry.hndy_str_of_boolean( did_purge, "Did purge: " ) )
						did_some_by_hash = did_some_by_hash or did_purge
					else :
						if __debug__:
							if len( lst_dispose ) == 0 :
								p_multi_log.do_logs( "dbs", "Any duplicates are only in the survival paths." )
							else:
								p_multi_log.do_logs( "dbs", "No clear hash survivor so cannot automate purging for hash: " + hsh )
							if len( lst_dispose ) > 0 :
								p_multi_log.do_logs( "dbs", "Example disposal item is:" + cmntry.pfn4print( lst_dispose[0] ) )
							if len( lst_survive ) > 0 :
								p_multi_log.do_logs( "dbs", "Example survival item is:" + cmntry.pfn4print( lst_survive[0] ) )
			else:
				p_multi_log.do_logs( "spdb", mod_code_name() + ": Aborted in process_and_purge_by_hash_from_largest during for siz in sz_lst" )
				break
	return did_some_by_hash

def process_and_purge_by_superhash_from_largest( p_dct_sized_superhashes, p_dct_hash_lst_fldrs, \
		p_lst_paths_survive, p_lst_paths_dispose, p_Cngrntry_FnameMatchSetting_S, p_Cngrntry_FnameMatchSetting_D, \
		p_break_after, p_docongruentbeforedelete, p_congruentstringent, p_ReportFailuresStringent, p_EnumDeleteMode, p_bashfilename_str, \
		p_fn_Check_NoInterruption, p_multi_log ):
	# Brief:
	# sort the superhashes by size
	# get the largest superhash
	# see if it has folders in both disposals and survivors
	# if Yes then send the disposals for purging
	# Parameters:
	# - p_dct_sized_superhashes - sized set of parent hash points
	# - p_dct_hash_lst_fldrs - reference dictionary of hashes and the lists of folders of those hashes
	# - p_lst_paths_survive - survivor paths
	# - p_lst_paths_dispose - disposal paths
	# - p_break_after - set a maximum number of purgings to do (really, a debugging feature)
	# - p_docongruentbeforedelete
	# - p_congruentstringent - 
	# - p_EnumDeleteMode - method control to pass to purgetry
	# - p_bashfilename_str - filename of script to generate
	# Returns:
	# did_some_by_superhash
	# Notes:
	# Code:
	p_multi_log.do_logs( "pdb", "process_and_purge_by_superhash_from_largest" )
	# impose and upper limit on the folder trees to purge - this is a during-development thing, remove later
	if p_break_after < 0 :
		break_after = purge_break_after_maximum()
	elif p_break_after > 0 :
		break_after = p_break_after
	else:
		break_after = purge_break_after_dflt()
	p_multi_log.do_logs( "pdb", "will only attempt to purge " + str(break_after) + " folder trees" )
	did_some_by_superhash = False
	sh_sz_count = len( p_dct_sized_superhashes)
	p_multi_log.do_logs( "pdb", "there are " + str( sh_sz_count) + " sizes of super hashes." )
	if sh_sz_count > 0 :
		# get desending list of sizes
		sz_lst = sorted( list( p_dct_sized_superhashes.keys() ), reverse=True )
		sz_at = 0
		# for the top n sizes
		for siz in sz_lst:
			if p_fn_Check_NoInterruption():
				# for each size
				# limit to top n
				sz_at = sz_at + 1
				if sz_at > break_after:
					break
				p_multi_log.do_logs( "db", "SuperHash size #" + str( sz_at) + " of " + str( sh_sz_count) + " size = " + cmntry.hndy_GetHumanReadable(siz) + " " + str( siz) )
				# get the hashes
				h_l = p_dct_sized_superhashes[siz]
				p_multi_log.do_logs( "db", "has " + str( len( h_l)) + " super hashes." )
				hz_count = len( h_l)
				hz_at = 0
				for hsh in h_l:
					# per hash
					if p_fn_Check_NoInterruption():
						hz_at = hz_at + 1
						p_multi_log.do_logs( "db", "Hash #" + str( hz_at) + " of" + str( hz_count) + " hash: " + hsh )
						# for each hash, get the list of folders
						lst_fldrtpl = p_dct_hash_lst_fldrs[hsh]
						if __debug__:
							p_multi_log.do_logs( "b", str( len( lst_fldrtpl) ) + " paths: " )
						has_both, lst_survive, lst_dispose = does_this_folderlist_have_survivors_vs_disposals( \
							lst_fldrtpl, p_lst_paths_survive, p_lst_paths_dispose )
						if has_both :
							p_multi_log.do_logs( "dbs", "We can automate purging" + " of size = " + cmntry.hndy_GetHumanReadable(siz) )
							p_multi_log.do_logs( "dbs", "Keeping " + str( len( lst_survive) ) + " paths: " )
							p_multi_log.do_logs( "dbs", "Purging " + str( len( lst_dispose) ) + " paths: " )
							p_multi_log.do_logs( "pdb", "Calling check_then_purge" )
							can_purge, did_purge = check_then_purge( lst_survive, lst_dispose, p_Cngrntry_FnameMatchSetting_S, p_Cngrntry_FnameMatchSetting_D, \
								p_docongruentbeforedelete, p_congruentstringent, p_ReportFailuresStringent, p_EnumDeleteMode, p_bashfilename_str, siz, p_fn_Check_NoInterruption, p_multi_log )
							p_multi_log.do_logs( "pbs", "Wanted purged: " + str( can_purge ) )
							p_multi_log.do_logs( "pbs", "Did purge: " + str( did_purge ) )
							did_some_by_superhash = did_some_by_superhash or did_purge
						else :
							pass
							p_multi_log.do_logs( "dbs", "No clear SuperHash survivor so cannot automate purging." )
							if len( lst_dispose ) > 0 :
								p_multi_log.do_logs( "dbs", "Example disposal item is:" + cmntry.pfn4print( lst_dispose[0] ) )
							if len( lst_survive ) > 0 :
								p_multi_log.do_logs( "dbs", "Example survival item is:" + cmntry.pfn4print( lst_survive[0] ) )
					else:
						p_multi_log.do_logs( "spdb", mod_code_name() + ": Aborted in process_and_purge_by_superhash_from_largest during for hsh in h_l" )
						break
			else:
				p_multi_log.do_logs( "spdb", mod_code_name() + ": Aborted in process_and_purge_by_superhash_from_largest during for siz in sz_lst" )
				break
	return did_some_by_superhash

def does_this_filelist_have_survivors_vs_disposals( lst_tplfilinf, p_lst_paths_survive, p_lst_paths_dispose ):
	# Brief:
	# for a list of file references, determine if there is both some in a survival path and some in a disposal path
	# and get back which are in which set 
	# Parameters:
	# - lst_tplfilinf :: list := list of named tuples as returned from Filedupetry
	# - p_lst_paths_survive :: list := list of strings
	# - p_lst_paths_dispose :: list := list of strings
	# Returns:
	# - in_both :: boolean
	# - lst_survive
	# - lst_dispose
	# Notes:
	# Code:
	log_strl_str = "b"
	log_prefix = "DTFLHSVD_"
	#p_multi_log.do_logs( "dbs", "does_this_folderlist_have_survivors_vs_disposals" )
	#p_multi_log.do_logs( "b", log_prefix + "len of p_lst_foldertpls is " + str(len(lst_tplfilinf)) )
	lst_survive = []
	lst_dispose = []
	in_survive_any = False
	in_dispose_any = False
	in_both = False
	i = 0
	for i_tplfilinf in lst_tplfilinf :
		i = i + 1
		s_i = "(" + str(i) + ")"
		# per folder
		i_pathfile = i_tplfilinf.fi_pfn # tpl_FileInfo = namedtuple('tpl_FileInfo', 'fi_pfn fi_fname fi_fsize fi_fhash')
		#p_multi_log.do_logs( "b", log_prefix + s_i + "testing pathfile: " + i_pathfile )
		# survivors
		in_survives = False
		j = 0
		for f_survive in p_lst_paths_survive :
			j = j + 1
			s_j = "(s" + str(j) + ")"
			#p_multi_log.do_logs( log_strl_str, log_prefix + s_i + s_j + "-against survivor: " + f_survive )
			sbfldrnss_survive = explrtry.subfolderness_a_b( i_pathfile, f_survive )
			in_this_survive = sbfldrnss_survive in ["a", "="]
			#p_multi_log.do_logs( log_strl_str, log_prefix + s_i + s_j + "in_this_survive: " + str(in_this_survive) )
			in_survives = in_survives or in_this_survive
			#p_multi_log.do_logs( log_strl_str, log_prefix + s_i + s_j + "in_survives: " + str(in_survives) )
		if in_survives :
			lst_survive.append( i_pathfile)
		in_survive_any = in_survive_any or in_survives
		#p_multi_log.do_logs( log_strl_str, log_prefix + s_i + "in_survive_any: " + str(in_survive_any) )
		# disposals
		in_disposes = False
		j = 0
		for f_dispose in p_lst_paths_dispose :
			j = j + 1
			s_j = "(d" + str(j) + ")"
			#p_multi_log.do_logs( log_strl_str, log_prefix + s_i + s_j + "against disposal: " + f_dispose )
			sbfldrnss_dispose = explrtry.subfolderness_a_b( i_pathfile, f_dispose )
			in_this_dispose = sbfldrnss_dispose in ["a", "="]
			#p_multi_log.do_logs( log_strl_str, log_prefix + s_i + s_j + "in_this_dispose: " + str(in_this_dispose) )
			in_disposes = in_disposes or in_this_dispose
			#p_multi_log.do_logs( log_strl_str, log_prefix + s_i + s_j + "in_disposes: " + str(in_disposes) )
		if in_disposes :
			lst_dispose.append( i_pathfile)
		in_dispose_any = in_dispose_any or in_disposes
		#p_multi_log.do_logs( log_strl_str, log_prefix + s_i + "in_dispose_any: " + str(in_dispose_any) )
	in_both = in_survive_any and in_dispose_any
	if in_both :
		pass
		#p_multi_log.do_logs( log_strl_str, log_prefix + "in both overall: " + str(in_both))
	else :
		pass
		#lst_survive = []
		#lst_dispose = []
	return in_both, lst_survive, lst_dispose

def does_lst_pfn_have_survivors_vs_disposals( p_lst_pfn, p_lst_paths_survive, p_lst_paths_dispose ):
	# Brief:
	# for a list of file references, determine if there is both some in a survival path and some in a disposal path
	# and get back which are in which set 
	# Parameters:
	# - p_lst_pfn :: list := list of parth-filenames being checked and/or split
	# - p_lst_paths_survive :: list := list of path strings - i.e. potential parent folders
	# - p_lst_paths_dispose :: list := list of path strings - i.e. potential parent folders
	# Returns:
	# - in_both :: boolean
	# - r_lst_survive
	# - r_lst_dispose
	# Notes:
	# Code:
	log_strl_str = "b"
	log_prefix = "DTFLHSVD_"
	#p_multi_log.do_logs( "dbs", "does_lst_pfn_have_survivors_vs_disposals" )
	#p_multi_log.do_logs( "b", log_prefix + "len of p_lst_foldertpls is " + str(len(p_lst_pfn)) )
	r_lst_survive = []
	r_lst_dispose = []
	in_survive_any = False
	in_dispose_any = False
	in_both = False
	i = 0
	for i_pathfile in p_lst_pfn :
		i = i + 1
		s_i = "(" + str(i) + ")"
		#p_multi_log.do_logs( "b", log_prefix + s_i + "testing pathfile: " + i_pathfile )
		# survivors
		in_survives = False
		j = 0
		for f_survive in p_lst_paths_survive :
			j = j + 1
			s_j = "(s" + str(j) + ")"
			#p_multi_log.do_logs( log_strl_str, log_prefix + s_i + s_j + "-against survivor: " + f_survive )
			sbfldrnss_survive = explrtry.subfolderness_a_b( i_pathfile, f_survive )
			in_this_survive = sbfldrnss_survive in ["a", "="]
			#p_multi_log.do_logs( log_strl_str, log_prefix + s_i + s_j + "in_this_survive: " + str(in_this_survive) )
			in_survives = in_survives or in_this_survive
			#p_multi_log.do_logs( log_strl_str, log_prefix + s_i + s_j + "in_survives: " + str(in_survives) )
		if in_survives :
			r_lst_survive.append( i_pathfile)
		in_survive_any = in_survive_any or in_survives
		#p_multi_log.do_logs( log_strl_str, log_prefix + s_i + "in_survive_any: " + str(in_survive_any) )
		# disposals
		in_disposes = False
		j = 0
		for f_dispose in p_lst_paths_dispose :
			j = j + 1
			s_j = "(d" + str(j) + ")"
			#p_multi_log.do_logs( log_strl_str, log_prefix + s_i + s_j + "against disposal: " + f_dispose )
			sbfldrnss_dispose = explrtry.subfolderness_a_b( i_pathfile, f_dispose )
			in_this_dispose = sbfldrnss_dispose in ["a", "="]
			#p_multi_log.do_logs( log_strl_str, log_prefix + s_i + s_j + "in_this_dispose: " + str(in_this_dispose) )
			in_disposes = in_disposes or in_this_dispose
			#p_multi_log.do_logs( log_strl_str, log_prefix + s_i + s_j + "in_disposes: " + str(in_disposes) )
		if in_disposes :
			r_lst_dispose.append( i_pathfile)
		in_dispose_any = in_dispose_any or in_disposes
		#p_multi_log.do_logs( log_strl_str, log_prefix + s_i + "in_dispose_any: " + str(in_dispose_any) )
	r_in_both = in_survive_any and in_dispose_any
	if r_in_both :
		pass
		#p_multi_log.do_logs( log_strl_str, log_prefix + "in both overall: " + str( r_in_both))
	return r_in_both, r_lst_survive, r_lst_dispose


# NOTE: this gets used in the foldatry module
def default_badmatch_list( ) :
	return [ ").", ".~"]

def does_this_badmatch_these( s_this, lst_these) :
	mark_this = False
	for bad_s in lst_these :
		mark_this = ( bad_s in s_this)
		if mark_this :
			break
	return mark_this

def string_pad_any_numeric( s):
	return cmntry.string_naturalised(s)

def get_path_length( s):
	pp = pthlb.PurePath( s)
	p = pp.parents[0]
	return len(str(p))

def get_path_depth( s):
	pp = pthlb.PurePath( s)
	pd = len( pp.parts )
	return pd


def select_surplus_by_strategy( lst_tplfilinf, p_EnumKeepWhich, p_use_names, p_lst_names_bad, p_multi_log ):
	# Brief:
	# for a list of file references, determine which should be deleted and which kept
	# Parameters:
	# - lst_tplfilinf :: list := list of named tuples as returned from Filedupetry
	# - p_str_strategy_code :: string := strategy code
	# Returns:
	# - str_survive
	# - lst_dispose
	# Notes:
	# Code:
	log_str = "def select_surplus_by_strategy"
	log_str = "\n" + "passed a list of " + str(len(lst_tplfilinf)) + " files"
	log_str = "\n" + "p_EnumKeepWhich = " + apltry.EnumKeepWhich_Attrbt_Brief( p_EnumKeepWhich)
	log_str = "\n" + "p_use_names = " + str(p_use_names)
	log_str = "\n" + "p_lst_names_bad = " + str(p_lst_names_bad)
	if __debug__:
		p_multi_log.do_logs( "b", log_str )
	log_strl_str = "b"
	log_prefix = "SSBS_"
	# get the pathfile refs from the tuples
	lst_dispose = [f.fi_pfn for f in lst_tplfilinf]
	# replacement polarity decider
	if apltry.EnumKeepWhich_Operator_is_LT( p_EnumKeepWhich ) :
		op_compare = operator.lt
		if __debug__:
			p_multi_log.do_logs( "b", "p_EnumKeepWhich means GT" )
	else :
		op_compare = operator.gt
		if __debug__:
			p_multi_log.do_logs( "b", "p_EnumKeepWhich means LT" )
	#
	i_grp_mthd = apltry.EnumKeepWhich_Method( p_EnumKeepWhich )
	if __debug__:
		p_multi_log.do_logs( "b", "i_grp_mthd is " + str(i_grp_mthd) )
	# loop to find the list index of the survivor
	# i is loop index, 
	l_i = 0 
	# survivor index and (test)string
	s_i = l_i
	s_s = ""
	# list of indexes that were disposal matched
	lst_m_i = []
	for i_pathfile in lst_dispose :
		if __debug__:
			p_multi_log.do_logs( "b", log_prefix + str(l_i) + " testing pathfile: " + cmntry.pfn4print( i_pathfile ) )
		if p_use_names :
			# use a boolean for later flexibility
			mark_this = does_this_badmatch_these( i_pathfile, p_lst_names_bad)
			if mark_this :
				lst_m_i.append( l_i )
				if __debug__:
					p_multi_log.do_logs( "b", "matched bad name: " + cmntry.pfn4print( i_pathfile ) )
		# decide what to compare - i.e. build a custom string just for that purpose
		if i_grp_mthd == apltry.EnumKeepWhichMethod.ByAlpha :
			if __debug__:
				p_multi_log.do_logs( "b", "use ByAlpha" )
			t_s = i_pathfile
		elif i_grp_mthd == apltry.EnumKeepWhichMethod.ByNumeric :
			if __debug__:
				p_multi_log.do_logs( "b", "use ByNumeric" )
			t_s = string_pad_any_numeric( i_pathfile )
		elif i_grp_mthd == apltry.EnumKeepWhichMethod.ByPathLength  :
			if __debug__:
				p_multi_log.do_logs( "b", "use ByPathLength" )
			t_s = str( get_path_length( i_pathfile)).zfill(5) + "|" + i_pathfile
		elif i_grp_mthd == apltry.EnumKeepWhichMethod.ByPathDepth :
			if __debug__:
				p_multi_log.do_logs( "b", "use ByPathDepth" )
			t_s = str( get_path_depth( i_pathfile)).zfill(4) + "|" + i_pathfile
		else : # i_grp_mthd is apltry.EnumKeepWhichMethod.ByAge
			if __debug__:
				p_multi_log.do_logs( "b", "use ELSE (byAge)" )
			t_s = string_pad_any_numeric( str( im_os.path.getctime( i_pathfile) ) ) + "|" + i_pathfile
		# now do the comparison
		if len( s_s) == 0 : # or i == 0 really, same thing, first entry
			s_s = t_s
			s_i = l_i
			if __debug__:
				p_multi_log.do_logs( "b", "set 1st :> " + cmntry.pfn4print( s_s ) )
		elif op_compare( t_s, s_s ):
			# elif p_EnumKeepWhich in [ apltry.EnumKeepWhich.KeepNew.value, apltry.EnumKeepWhich.KeepAlphaLast.value ] :
			#if t_s > s_s :
			s_s = t_s
			s_i = l_i
			if __debug__:
				p_multi_log.do_logs( "b", "change to: " + cmntry.pfn4print( s_s ) )
		else :
			pass
			#if i_s < s_s :
			#s_s = i_s
			#s_i = i
			p_multi_log.do_logs( "b", "change:< " + cmntry.pfn4print( s_s ) )
		l_i = l_i + 1
	if p_use_names :
		if __debug__:
			p_multi_log.do_logs( "b", "matches of bad names = " + str(len(lst_m_i)) )
		if s_i in lst_m_i :
			# this means we picked a keeper that matches the bad names, so what to do?
			if __debug__:
				p_multi_log.do_logs( "b", "we picked a keeper that matches the bad names" + s_s )
			# can we find an unpicked one that isn't bad?
			f_i = 0
			for i_pathfile in lst_dispose :
				if __debug__:
					p_multi_log.do_logs( "b", "Checking item " + str( f_i) + " " + i_pathfile )
				if f_i != s_i :
					if not does_this_badmatch_these( i_pathfile, p_lst_names_bad) :
						if __debug__:
							p_multi_log.do_logs( "b", "That'll do! " )
						s_i = f_i
						break
				f_i = f_i + 1
	# cover empty list case
	if len(lst_dispose) > 0 :
		# pull the chosen survivor out of the disposal list
		str_survive = lst_dispose.pop( s_i)
		if __debug__:
			p_multi_log.do_logs( "b", "Keeping: " + cmntry.pfn4print( str_survive ) )
			p_multi_log.do_logs( "b", "Dropping: " )
		for s in lst_dispose :
			if __debug__:
				p_multi_log.do_logs( "b", "File: " + cmntry.pfn4print( s ) )
	else :
		str_survive = ""
	return str_survive, lst_dispose

def check_then_erode( p_lst_survive, p_lst_dispose, p_docongruentbeforedelete, p_congruentstringent, p_ReportFailuresStringent, \
	p_enum_method, p_bashfilename_str, p_multi_log ):
	# Brief:
	# this operates on lists of identical files
	# which merely need a final check before deleting those in the disposal list
	# much of the time each list will be a single file but the method is able to cope with more
	# Parameters:
	# - p_lst_survive :: list :=
	# - p_lst_dispose :: list :=
	# - p_enum_method :: DeleteMode := enumeration from commontry
	# - p_bashfilename_str :: string :=
	# Returns:
	# :: boolean := can_erode
	# :: boolean := did_erode
	# Notes:
	# Code:
	p_multi_log.do_logs( "db", "check_then_erode" )
	len_p_lst_survive = len(p_lst_survive)
	len_p_lst_dispose = len(p_lst_dispose)
	p_multi_log.do_logs( "db", "p_lst_survive" + " has " + str( len_p_lst_survive) )
	p_multi_log.do_logs( "db", "p_lst_dispose" + " has " + str( len_p_lst_dispose) )
	lst_wish_eroded = []
	lst_did_eroded = []
	r_Lst_StringentFailures = []
	if ( len_p_lst_survive > 0 ) and ( len_p_lst_dispose > 0 ) :
		# as assume other processes have decided these are all identical just use the first survivor in list
		f_s = p_lst_survive[0]  # note the [0]
		for f_d in p_lst_dispose :
			p_multi_log.do_logs( "db", "Call congruentry" )
			if p_docongruentbeforedelete : 
				# note that p_congruentstringent is currently irrelevant here but might be added later
				p_multi_log.do_logs( "sdb", "Calling congruentry_files_action" + " for:" )
				p_multi_log.do_logs( "sdb", "Dispose: " + cmntry.pfn4print( f_d ) )
				p_multi_log.do_logs( "sdb", "Survive: " + cmntry.pfn4print( f_s ) )
				can_erode = congruentry_files_action( f_s, f_d, p_multi_log)
				if p_ReportFailuresStringent and not can_erode :
					r_Lst_StringentFailures.append( ( f_s, f_d ) )
			else :
				can_erode = True
			p_multi_log.do_logs( "db", "Got checked_congruent: " + str(can_erode) )
			if can_erode :
				p_multi_log.do_logs( "dbs", "Can now call purgetry_file for: " + cmntry.pfn4print( f_d ) )
				i_lst_rem_ante = []
				i_lst_rem_post = []
				for f in p_lst_survive :
					i_lst_rem_post.append( "Keep " + cmntry.pfn4print( f ) )
				did_erode = purgetry_file_command( f_d, p_enum_method, p_bashfilename_str, i_lst_rem_ante, i_lst_rem_post, p_multi_log )
				if did_erode :
					lst_did_eroded.append( f_d )
				else :
					lst_wish_eroded.append( f_d )
	wish_eroded = ( len( lst_wish_eroded) > 0 )
	did_eroded = ( len( lst_did_eroded) > 0 )
	if wish_eroded or did_eroded :
		if did_eroded :
			if __debug__:
				p_multi_log.do_logs( "b", "Did Erode:" )
			for f in lst_did_eroded :
				if __debug__:
					p_multi_log.do_logs( "b", cmntry.pfn4print( f ) )
		elif wish_eroded :
			if __debug__:
				p_multi_log.do_logs( "b", "Wanted to Erode:" )
			for f in lst_wish_eroded :
				if __debug__:
					p_multi_log.do_logs( "b", cmntry.pfn4print( f ) )
		if __debug__:
			p_multi_log.do_logs( "b", "Survived:" )
		for f in p_lst_survive :
			if __debug__:
				p_multi_log.do_logs( "b", cmntry.pfn4print( f ) )
	return wish_eroded, did_eroded, r_Lst_StringentFailures

def process_and_erode_disposals_by_strategy_in_delorder( \
		p_lst_paths_survive, p_lst_paths_dispose, \
		p_dct_pfn_filemeta, p_dct_shortmatchkey_lst_pfn, p_dct_fullmatchkey_lst_pfn, \
		p_FileFlags, p_DelOrderMode, \
		p_docongruentbeforedelete, p_congruentstringent, p_ReportFailuresStringent, p_EnumDeleteMode, \
		p_bashfilename_str, p_fn_Check_NoInterruption, p_multi_log ) :
	# Brief:
	# with the results of a filedupetry run. and given lists of survival and disposal folders,
	# delete files as directed, acting first on the largest size files, while always pre-checking
	# that we're safe to delete a file because there are other verified copies to remain
	# Parameters:
	# - p_FileFlags
	# - p_lst_paths_survive :: list := 
	# - p_lst_paths_dispose :: list := 
	# - p_dct_pfn_filemeta, p_dct_shortmatchkey_lst_pfn, p_dct_fullmatchkey_lst_pfn :: dictionary := 
	# - p_docongruentbeforedelete :: boolean := 
	# - p_congruentstringent :: boolean := 
	# - p_EnumDeleteMode :: DeleteMode := enumeration from commontry
	# - p_bashfilename_str :: string := 
	# Returns:
	# :: boolean := did we (or did we think we) deleted any files 
	# Notes:
	# for the return value, it's not clear whether to distinguish between actually deleting files and 
	# setting up to delete files as would be the case if the directed action is to build a script 
	# Code:
	if False:
		print( "process_and_erode_disposals_by_strategy_in_delorder" )
		print( "p_FileFlags" )
		print( p_FileFlags )
		print( "p_DelOrderMode" )
		print( p_DelOrderMode )
		print( "p_docongruentbeforedelete" )
		print( p_docongruentbeforedelete )
		print( "p_congruentstringent" )
		print( p_congruentstringent )
		print( "p_EnumDeleteMode" )
		print( p_EnumDeleteMode )
		print( "p_bashfilename_str" )
		print( p_bashfilename_str )
		input( "Press ENTER" )
	i_dct_matchkeytuple_lst_pfn = {}
	def DelOrderMode_SortValueForKeyTuple( p_keytuple ):
		nonlocal p_DelOrderMode
		nonlocal i_dct_matchkeytuple_lst_pfn
		return DelOrderMode_DeriveSortValueForKeyTuple( p_keytuple, p_DelOrderMode, i_dct_matchkeytuple_lst_pfn, p_dct_pfn_filemeta )
	def DelOrderMode_SortReverse():
		nonlocal p_DelOrderMode
		return DelOrderMode_DeriveSortReverse( p_DelOrderMode )
	if __debug__:
		p_multi_log.do_logs( "dbs", "process_and_erode_disposals_by_strategy_in_delorder" )
	# impose and upper limit on the folder trees to purge - this is a during-development thing, remove later
	did_files_by_hash = False
	# which tuple index type are we using? Current code can use the same sort-item-value function by relying on sub-element coincidence but leave as two calls in case that changes
	if p_FileFlags.ff_Hash :
		i_dct_matchkeytuple_lst_pfn = p_dct_fullmatchkey_lst_pfn
		# get list of keys - later make this a smart call to get them sorted as per the Fileflags 
		i_lst_unsorted_keys = p_dct_fullmatchkey_lst_pfn.keys()
		i_lst_keys_grps = sorted( i_lst_unsorted_keys, key= lambda x: DelOrderMode_SortValueForKeyTuple( x ), reverse=DelOrderMode_SortReverse() )
	else :
		i_dct_matchkeytuple_lst_pfn = p_dct_shortmatchkey_lst_pfn
		# get list of keys - later make this a smart call to get them sorted as per the Fileflags 
		i_lst_unsorted_keys = p_dct_shortmatchkey_lst_pfn.keys()
		i_lst_keys_grps = sorted( i_lst_unsorted_keys, key= lambda x: DelOrderMode_SortValueForKeyTuple( x ), reverse=DelOrderMode_SortReverse() )
		pass
	i_grps_count = len( i_dct_matchkeytuple_lst_pfn )
	if i_grps_count > 0 :
		i_tuplkey_at = 0
		# for the top n sizes
		for i_tuplkey in i_lst_keys_grps:
			if p_fn_Check_NoInterruption():
				# for each i_tuplkeye
				if False:
					print( "i_tuplkey" )
					print( i_tuplkey )
				i_tuplkey_at = i_tuplkey_at + 1
				p_multi_log.do_logs( "db", "i_tuplkeye #" + str( i_tuplkey_at) + " with " + str( i_grps_count) + " of i_tuplkeye = " + cmntry.hndy_GetHumanReadable(i_tuplkey_at) + " " + str( i_tuplkey_at) )
				# get the hashes
				i_lst_pfn = i_dct_matchkeytuple_lst_pfn[ i_tuplkey ]
				if __debug__:
					p_multi_log.do_logs( "b", str( len( i_lst_pfn) ) + " files: " )
				i_has_both, i_lst_survive, i_lst_dispose = does_lst_pfn_have_survivors_vs_disposals( \
					i_lst_pfn, p_lst_paths_survive, p_lst_paths_dispose )
				if i_has_both :
					i_size = GetSizeOfExampleInList( p_dct_pfn_filemeta, i_lst_dispose )
					p_multi_log.do_logs( "dbs", "Tackling sets of files, each is size: " + cmntry.hndy_GetHumanReadable( i_size ) + \
						"   Intend to keep " + str( len( i_lst_survive) ) + " files, while eroding " + str( len( i_lst_dispose) ) + " files: " )
					wished_erode, did_erode, i_Lst_StringentFailures = check_then_erode( i_lst_survive, i_lst_dispose, \
						p_docongruentbeforedelete, p_congruentstringent, p_ReportFailuresStringent, p_EnumDeleteMode, p_bashfilename_str, p_multi_log )
					p_multi_log.do_logs( "dbs", "Result: Did erode some: " + str( did_erode ) + "  Wanted some eroded but could not: " + str( wished_erode ) )
					did_files_by_hash = did_files_by_hash or did_erode
				else :
					p_multi_log.do_logs( "dbs", "No disposal+survivor combination for this matchkey so cannot automate purging." )
					if len( i_lst_dispose ) > 0 :
						p_multi_log.do_logs( "dbs", "Example disposal item is:" + cmntry.pfn4print( i_lst_dispose[0] ) )
					if len( i_lst_survive ) > 0 :
						p_multi_log.do_logs( "dbs", "Example survival item is:" + cmntry.pfn4print( i_lst_survive[0] ) )
			else:
				p_multi_log.do_logs( "spdb", mod_code_name() + ": Aborted in process_and_erode_disposals_by_strategy_in_delorder during for i_tuplkey in i_lst_keys_grps" )
				break
	return did_files_by_hash

# -------- replacements section

def GetSizeOfExampleInList( p_dct_pfn_filemeta, p_lst_pfn ):
	r_size = None
	if len( p_lst_pfn ) > 0 :
		i_pfn = p_lst_pfn[ 0]
		i_pfn_filemeta = p_dct_pfn_filemeta[ i_pfn ]
		r_size = i_pfn_filemeta.fi_fsize
	return r_size
def GetWhenOfExampleInList( p_dct_pfn_filemeta, p_lst_pfn ):
	r_when = None
	if len( p_lst_pfn ) > 0 :
		i_pfn_filemeta = p_dct_pfn_filemeta[ p_lst_pfn[ 0] ]
		r_when = i_pfn_filemeta.fi_fwhen
	return r_when
def GetNameOfExampleInList( p_dct_pfn_filemeta, p_lst_pfn ):
	r_name = None
	if len( p_lst_pfn ) > 0 :
		i_pfn_filemeta = p_dct_pfn_filemeta[ p_lst_pfn[ 0] ]
		r_name = i_pfn_filemeta.fi_fname
	return r_name


# this is a more generic Reverse boolean function
def DelOrderMode_DeriveSortReverse( p_DelOrderMode ):
	r_reverse = False
	if p_DelOrderMode == apltry.EnumDelOrder.DelFromBig :
		r_reverse = True
	elif p_DelOrderMode == apltry.EnumDelOrder.DelSmaller :
		r_reverse = False
	elif p_DelOrderMode == apltry.EnumDelOrder.DelOldest :
		r_reverse = False
	elif p_DelOrderMode == apltry.EnumDelOrder.DelNewest :
		r_reverse = False
	elif p_DelOrderMode == apltry.EnumDelOrder.DelAmost :
		r_reverse = False
	elif p_DelOrderMode == apltry.EnumDelOrder.DelZmost :
		r_reverse = False
	return r_reverse

# Now, let's rework the function for providing a sorting value.
# Here's the generic:
def DelOrderMode_DeriveSortValueForKeyTuple( p_keytuple, p_DelOrderMode, i_dct_tuplekey_lst_pfn, p_dct_pfn_filemeta ):
	r_value = None
	if p_DelOrderMode == apltry.EnumDelOrder.DelFromBig or p_DelOrderMode == apltry.EnumDelOrder.DelSmaller :
		r_value = GetSizeOfExampleInList( p_dct_pfn_filemeta, i_dct_tuplekey_lst_pfn[ p_keytuple ] )
	elif p_DelOrderMode == apltry.EnumDelOrder.DelOldest or p_DelOrderMode == apltry.EnumDelOrder.DelNewest :
		r_value = GetWhenOfExampleInList( p_dct_pfn_filemeta, i_dct_tuplekey_lst_pfn[ p_keytuple ] )
	elif p_DelOrderMode == apltry.EnumDelOrder.DelAmost or p_DelOrderMode == apltry.EnumDelOrder.DelZmost :
		r_value = GetNameOfExampleInList( p_dct_pfn_filemeta, i_dct_tuplekey_lst_pfn[ p_keytuple ] )
	else :
		print( "what the?")
		print( "p_keytuple")
		print( p_keytuple)
		print( "p_DelOrderMode")
		print( p_DelOrderMode)
		print( "i_dct_tuplekey_lst_pfn")
		print( i_dct_tuplekey_lst_pfn)
		print( "p_dct_pfn_filemeta")
		print( p_dct_pfn_filemeta)
	return r_value


# this is the replacment function, rewritten to handle the revised matchkey method that expands beyond the by-size original approach
# depending on the controls we might use various parts of the passed dictionaries
# it is the role of this function to 
# if have been passed a 
# 
def process_and_erode_in_match_groups_per_strategy( \
		p_dct_pfn_filemeta, p_dct_shortmatchkey_lst_pfn, p_dct_fullmatchkey_lst_pfn, \
		p_FileFlags, p_EnumKeepWhich, p_DelOrderMode, p_ignore_zero_len, p_use_names, p_lst_names_bad, p_break_after, \
		p_docongruentbeforedelete, p_congruentstringent, p_ReportFailuresStringent, p_EnumDeleteMode, p_bashfilename_str, p_multi_log ) :
	# p_dct_pfn_filemeta :: a dictionary keyed by pathfilename with values being a named tuple of type tpl_FileMeta
	# p_dct_shortmatchkey_lst_pfn :: a dictionary keyed by a tuple of shortmatchkey with values being lists of pathfilename
	# p_dct_fullmatchkey_lst_fileinfohash :: a dictionary keyed by a tuple of fullmatchkey with values being lists of pathfilename
	# dummy assignment, just to make nonlocal happy lower down, but before the variable is really assignedd - dumb Python!
	if False:
		print( "process_and_erode_in_match_groups_per_strategy" )
		print( "p_FileFlags" )
		print( p_FileFlags )
		print( "p_EnumKeepWhich" )
		print( p_EnumKeepWhich )
		print( "p_DelOrderMode" )
		print( p_DelOrderMode )
		print( "p_ignore_zero_len" )
		print( p_ignore_zero_len )
		print( "p_use_names" )
		print( p_use_names )
		print( "p_lst_names_bad" )
		print( p_lst_names_bad )
		print( "p_break_after" )
		print( p_break_after )
		print( "p_docongruentbeforedelete" )
		print( p_docongruentbeforedelete )
		print( "p_congruentstringent" )
		print( p_congruentstringent )
		print( "p_EnumDeleteMode" )
		print( p_EnumDeleteMode )
		print( "p_bashfilename_str" )
		print( p_bashfilename_str )
		input( "Press ENTER" )
	i_dct_tuplekey_lst_pfn = {}
	#
	def DelOrderMode_SortValueForKeyTuple( p_keytuple ):
		# depending on the p_DelOrderMode this returns a value for the nominated list-of-files for use in a "sorted" call
		# as the reason for there being such a list is that they were all the same in some way, then merely grabbing the first will do the job
		# and thus we pull out the required inside-the-named-tuple value - e.g Size, When
		nonlocal p_DelOrderMode
		nonlocal p_dct_pfn_filemeta
		nonlocal i_dct_tuplekey_lst_pfn
		return DelOrderMode_DeriveSortValueForKeyTuple( p_keytuple, p_DelOrderMode, i_dct_tuplekey_lst_pfn, p_dct_pfn_filemeta )
	def DelOrderMode_SortReverse():
		# depending on the p_DelOrderMode this returns either True or False for use in a "sorted" call
		nonlocal p_DelOrderMode
		return DelOrderMode_DeriveSortReverse( p_DelOrderMode )
	def erode_matchedgroup_per_strategy( p_lst_pfn ):
		# here, we get passed the list of file infos and need to work out which to keep and which to delete, according to the p_EnumKeepWhich
		nonlocal p_EnumKeepWhich
		nonlocal p_FileFlags
		nonlocal p_dct_pfn_filemeta
		i_EnumKeepWhich = apltry.EnumKeepWhich.KeepOld
		if p_FileFlags.ff_Hash :
			pass
		else :
			pass
		if __debug__:
			p_multi_log.do_logs( "b", str( len( p_lst_pfn) ) + " files: " )
		# make a list of 
		i_lst_FileMeta = []
		for i_pfn in p_lst_pfn :
			i_lst_FileMeta.append( p_dct_pfn_filemeta[ i_pfn ] )
		str_survive, lst_dispose = select_surplus_by_strategy( i_lst_FileMeta, p_EnumKeepWhich, \
			p_use_names, p_lst_names_bad, p_multi_log )
		did_erode = False # ensure we have something to return
		if len( str_survive) > 0 and len( lst_dispose) > 0 :
			# i_size = GetSizeOfExampleInList( p_dct_pfn_filemeta, lst_dispose )
			if __debug__:
				p_multi_log.do_logs( "b", "We can automate erosion." )
				p_multi_log.do_logs( "b", "Keeping file: " + cmntry.pfn4print( str_survive ) + " " )
				p_multi_log.do_logs( "b", "Eroding " + str( len( lst_dispose) ) + " files: " )
			# here we call the same function as originally written
			# while it only uses the first item in the survive list, it gets passed the whole list in case that should go into logs or other output
			# hence, even though here we've already pulled out just one example, we need to make that a list-of-one to pass it
			wished_erode, did_erode, i_Lst_StringentFailures = check_then_erode( [str_survive], lst_dispose, \
				p_docongruentbeforedelete, p_congruentstringent, p_ReportFailuresStringent, p_EnumDeleteMode, p_bashfilename_str, p_multi_log )
			if __debug__:
				p_multi_log.do_logs( "b", "Wanted some eroded but could not: " + str( wished_erode ) )
				p_multi_log.do_logs( "b", "Did erode some: " + str( did_erode ) )
		else :
			pass
			if __debug__:
				p_multi_log.do_logs( "b", "No clear group survivor so cannot automate purging." )
		return did_erode
	did_all_ok = True
	if p_FileFlags.ff_Hash :
		i_dct_tuplekey_lst_pfn = p_dct_fullmatchkey_lst_pfn # p_dct_fullmatchkey_lst_fileinfohash
	else :
		i_dct_tuplekey_lst_pfn = p_dct_shortmatchkey_lst_pfn
	# assemble a list of the keys with two or more files
	i_lst_keys = []
	for t_key in i_dct_tuplekey_lst_pfn.keys():
		if len( i_dct_tuplekey_lst_pfn[ t_key] ) > 1 :
			#print("t_key")
			#print(t_key)
			#print("i_dct_tuplekey_lst_pfn[ t_key]")
			#print(i_dct_tuplekey_lst_pfn[ t_key])
			if not p_ignore_zero_len or GetSizeOfExampleInList( p_dct_pfn_filemeta, i_dct_tuplekey_lst_pfn[ t_key] ) > 0 :
				i_lst_keys.append( t_key )
	# now i_lst_keys is the subset of tuple-keys into the dictionary of pathname lists
	# now we want to sort those by the DelOrderMode so that we have the keys to the match groups in the desired order
	i_sorted_key = sorted( i_lst_keys, key= lambda x: DelOrderMode_SortValueForKeyTuple( x ), reverse=DelOrderMode_SortReverse() )
	#synatx is: sorted(iterable, key=key, reverse=reverse) 
	for t_key in i_sorted_key:
		did_ok = erode_matchedgroup_per_strategy( i_dct_tuplekey_lst_pfn[ t_key] )
		did_all_ok = did_all_ok and did_ok
	r_did_files = False
	return r_did_files

def make_dct_matchkey_lst_pfn_in_both_sets( p_dct_shortmatchkey_lst_pfn, p_lst_paths_survive, p_lst_paths_dispose, p_multi_log ):
	def def_name():
		return "make_dct_matchkey_lst_pfn_in_both_sets"
	p_multi_log.do_logs( "spdb", def_name() + ": Passed p_dct_shortmatchkey_lst_pfn of length " + str( len( p_dct_shortmatchkey_lst_pfn) ) )
	r_dct_matchkey_lst_pfn_in_both_sets = {}
	i_key_checked_count = 0
	i_key_added_count = 0
	i_time_mark = cmntry.is_n_to_show_progess_timed_get_mark()
	for i_matchkey in p_dct_shortmatchkey_lst_pfn.keys() :
		i_key_checked_count += 1
		i_lst_pfn = p_dct_shortmatchkey_lst_pfn[ i_matchkey ]
		i_has_both, i_lst_survive, i_lst_dispose = does_lst_pfn_have_survivors_vs_disposals( \
			i_lst_pfn, p_lst_paths_survive, p_lst_paths_dispose )
		if i_has_both :
			r_dct_matchkey_lst_pfn_in_both_sets[ i_matchkey ] = i_lst_pfn
			i_key_added_count += 1
		b, i_time_mark = cmntry.is_n_to_show_progess_timed_stock( i_key_checked_count, i_time_mark )
		if b :
			p_multi_log.do_logs( "spdb", def_name() + ": Keys checked = " + str( i_key_checked_count) + " Keys added = " + str( i_key_added_count) )
	p_multi_log.do_logs( "spdb", def_name() + ": Returning r_dct_matchkey_lst_pfn_in_both_sets of length " + str( len( r_dct_matchkey_lst_pfn_in_both_sets) ) )
	return r_dct_matchkey_lst_pfn_in_both_sets

# --------------------------------------------------
# Command - as would be called by other modules
# --------------------------------------------------

# general idea is:
# - get passed two sets of folders (survive, dispose), a flag and a logging control setup
# - combine the two folder sets
# - call the matchsubtry_command to find all the replicated places in all the folders
# - analyse the results to find all the replicants that can be safely removed (i.e. delete in dispose, keep in survive)
# - - currently part-implemented in "find_common_roots"
# - use congruenty to double-check eash deletion candidate
# - use purgetry to perform the verified deletions

# to be considered:
# - how it might be possible to offer a user choosing among replicants that are only in the disposal set
# - whether that step should be before processing the suvivor-v-disposal deletions
# - - maybe offer to support both

# TO DO: add FnameMatchSetting to the parameters - 
# - need to check out all the places that call this

def prunatry_command_recce( p_lst_paths_survive, p_lst_paths_dispose, \
		p_do_sbtry_super, p_do_sbtry_fldrs, p_do_filedupetry, \
		p_sbtry_hash_files, \
		p_FileFlags, p_DelOrderMode, \
		p_docongruentbeforedelete, p_congruentstringent, p_EnumDeleteMode ):
	# as an exercise, let's compile ALL the reasons why
	r_is_ok = True
	r_lst_why = [] # this will be a list of strings
	# A
	if isinstance( p_lst_paths_survive, list ) :
		for i_path_survive in p_lst_paths_survive :
			a_is_ok, a_lst_why = cmntry.consider_path_str( i_path_survive, "Survival" )
			r_is_ok = r_is_ok and a_is_ok
			r_lst_why.extend( a_lst_why )
	else:
		r_is_ok = False
		r_lst_why.append( "Survivals is not a list" )
	# B
	if isinstance( p_lst_paths_dispose, list ) :
		for i_path_dispose in p_lst_paths_dispose :
			b_is_ok, b_lst_why = cmntry.consider_path_str( i_path_dispose, "Disposal" )
			r_is_ok = r_is_ok and b_is_ok
			r_lst_why.extend( b_lst_why )
	else:
		r_is_ok = False
		r_lst_why.append( "Disposals is not a list" )
	return r_is_ok, r_lst_why

def prunatry_command( p_lst_paths_survive, p_lst_paths_dispose, \
		p_do_sbtry_super, p_do_sbtry_fldrs, p_do_filedupetry, \
		p_sbtry_hash_files, \
		p_FileFlags, p_DelOrderMode, \
		p_docongruentbeforedelete, p_congruentstringent, p_ReportFailuresStringent, p_EnumDeleteMode, \
		p_fn_Check_NoInterruption, p_multi_log):
	# Brief:
	# Provides a common base of functionality callable by either the GUI or command line routines
	# The "prunatry" approach uses a base concept of supplying two sets of folders:
	# - a survivor set, that will not be affected
	# - a disposal set, that WILL be affected
	# Parameters:
	# - p_lst_paths_survive :: list of strings := giving the paths to be analysed then kept intact
	# - p_lst_paths_dispose :: list of strings := giving the paths to be analysed then kept modified
	# - p_do_sbtry_super :: boolean := run matchsubtry and purge using superfolders & superhashes 
	# - p_do_sbtry_fldrs :: boolean := run matchsubtry and purge with just folder hashes
	# - p_do_filedupetry :: boolean := run filedupetry to do file hashing regardless of the folders they're in
	# - p_sbtry_hash_files :: boolean := should file contents be part of the matchsubtry hash, rather than just name and size
	# - p_docongruentbeforedelete
	# - p_congruentstringent :: boolean := should file contents be done for the fileduptry run 
	# - p_EnumDeleteMode :: string := code value for the purging method to follow when calling purgetry
	# - p_dct_rdlg :: dictionary of log flags and filenames as per Commontry module
	# Returns:
	# - a quite basic Boolean of whether things were done or not
	# Notes:
	# Code:
	zz_procname = "prunatry_command"
	# print( zz_procname )
	i_multi_log = im_copy.deepcopy( p_multi_log)
	cmntry.re_filename_multilog_object( i_multi_log, zz_procname )
	def def_name():
		return zz_procname
	def def_name_prfx():
		return def_name() + " "
	# Debugging info
	i_multi_log.do_logs( "b", "Called: " + def_name() )
	i_multi_log.do_logs( "spdb", def_name_prfx() + " " + cmntry.GetStringToShow_tpl_FileFlags( p_FileFlags) )
	#i_multi_log.do_logs( "b", " p_lst_paths_survive: " + p_lst_paths_survive )
	#i_multi_log.do_logs( "b", " p_lst_paths_dispose: " + p_lst_paths_dispose )
	i_multi_log.do_logs( "b", " p_do_sbtry_super: " + str( p_do_sbtry_super ) )
	i_multi_log.do_logs( "b", " p_do_sbtry_fldrs: " + str( p_do_sbtry_fldrs ) )
	i_multi_log.do_logs( "b", " p_do_filedupetry: " + str( p_do_filedupetry ) )
	i_multi_log.do_logs( "b", " p_sbtry_hash_files: " + str( p_sbtry_hash_files ) )
	i_multi_log.do_logs( "b", " p_docongruentbeforedelete: " + str( p_docongruentbeforedelete ) )
	i_multi_log.do_logs( "b", " p_congruentstringent: " + str( p_congruentstringent ) )
	#
	i_break_after = -1
	#
	i_multi_log.do_logs( "spdb", def_name() + ": Pre-process" )
	i_multi_log.do_logs( "db", "p_lst_paths_survive" + " has " + str( len(p_lst_paths_survive)) )
	i_multi_log.do_logs( "db", "p_lst_paths_dispose" + " has " + str( len(p_lst_paths_dispose)) )
	# combine the two lists and call matchsubtry
	i_multi_log.do_logs( "pdb", "Making a combination of Survivors and Disposals" )
	# - combine the two lists into a fresh copy
	i_lst_all_tree_paths = p_lst_paths_dispose.copy()
	i_lst_all_tree_paths.extend(p_lst_paths_survive)
	i_multi_log.do_logs( "db", "i_lst_all_tree_paths" + " has " + str( len(i_lst_all_tree_paths)) )
	i_use_datetime_str = cmntry.bfff_datetime_ident_str() 
	i_bashfilename_str = cmntry.scrf_make_bash_filename( i_use_datetime_str, zz_procname )
	if p_do_sbtry_super or p_do_sbtry_fldrs :
		# - call matchsubtry - should get back these things:
		#     mtc_dct_hash_lst_fldrs - dictionary of hashes, each with a list of folders
		#     mtc_dct_fldr_selves - dictionary of folders
		#     mtc_dct_fldr_superfolder - dictionary of folders, each with their superfolder
		#     mtc_dct_superfolder_folders - dictionary of superfolders, each with a list of fsub-folders
		#     mtc_dct_hash_best_superhash - dictionary of hashes, each their best superhash
		#     mtc_dct_superhash_hashes - dictionary of superhashes, each with all their subhashes
		#     mtc_dct_sized_superhashes - dictionary of sizes, each with a list of superhashes
		i_multi_log.do_logs( "pdb", def_name() + ": Step 0 Traversals and Collation" )
		dct_hash_lst_fldrs, dct_fldr_selves, dct_fldr_superfolder, \
			dct_superfolder_folders, dct_hash_best_superhash, dct_superhash_hashes, \
			dct_sized_superhashes = matchsubtry_command( i_lst_all_tree_paths, p_sbtry_hash_files, p_fn_Check_NoInterruption, i_multi_log)
		i_multi_log.do_logs( "db", mod_code_name() + " matchsubtry_command results" )
		i_multi_log.do_logs( "db", "dct_hash_lst_fldrs" + " has " + str( len(dct_hash_lst_fldrs)) )
		i_multi_log.do_logs( "db", "dct_fldr_selves" + " has " + str( len(dct_fldr_selves)) )
		i_multi_log.do_logs( "db", "dct_fldr_superfolder" + " has " + str( len(dct_fldr_superfolder)) )
		i_multi_log.do_logs( "db", "dct_superfolder_folders" + " has " + str( len(dct_superfolder_folders)) )
		i_multi_log.do_logs( "db", "dct_hash_best_superhash" + " has " + str( len(dct_hash_best_superhash)) )
		i_multi_log.do_logs( "db", "dct_superhash_hashes" + " has " + str( len(dct_superhash_hashes)) )
		i_multi_log.do_logs( "db", "dct_sized_superhashes" + " has " + str( len(dct_sized_superhashes)) )
		# p_EnumDeleteMode = "makebashrm"
		if p_fn_Check_NoInterruption():
			if p_do_sbtry_super and ( len(dct_sized_superhashes) > 0 ) :
				i_multi_log.do_logs( "pdb", def_name() + ": Step 1 Super Hash Process" )
				i_Cngrntry_Custom_FnameMatchSetting_A = apltry.FnameMatchSetting()
				i_Cngrntry_Custom_FnameMatchSetting_B = apltry.FnameMatchSetting()
				did_some_by_superhash = process_and_purge_by_superhash_from_largest( dct_sized_superhashes, dct_hash_lst_fldrs, \
					p_lst_paths_survive, p_lst_paths_dispose, i_Cngrntry_Custom_FnameMatchSetting_A, i_Cngrntry_Custom_FnameMatchSetting_B, \
					i_break_after, \
					p_docongruentbeforedelete, p_congruentstringent, p_ReportFailuresStringent, p_EnumDeleteMode, i_bashfilename_str, \
					p_fn_Check_NoInterruption, i_multi_log )
			else :
				did_some_by_superhash = False
		else :
			did_some_by_superhash = False
			i_multi_log.do_logs( "spdb", mod_code_name() + ": Aborted before calling: process_and_purge_by_superhash_from_largest" )
		if p_do_sbtry_fldrs :
			i_multi_log.do_logs( "pdb", def_name() + ": Step 2 Folder Hash Process" )
			i_Cngrntry_Custom_FnameMatchSetting_A = apltry.FnameMatchSetting()
			i_Cngrntry_Custom_FnameMatchSetting_B = apltry.FnameMatchSetting()
			did_some_by_hash = process_and_purge_by_hash_from_largest( dct_hash_lst_fldrs, \
				p_lst_paths_survive, p_lst_paths_dispose, i_Cngrntry_Custom_FnameMatchSetting_A, i_Cngrntry_Custom_FnameMatchSetting_B, \
				i_break_after, p_docongruentbeforedelete, p_congruentstringent, p_ReportFailuresStringent, p_EnumDeleteMode, i_bashfilename_str, \
				p_fn_Check_NoInterruption, i_multi_log )
		else :
			did_some_by_hash = False
	else :
		did_some_by_superhash = False
		did_some_by_hash = False
	# preparation for bringing in use of filedupetry for pure file hash matching
	if p_do_filedupetry :
		i_multi_log.do_logs( "pdb", def_name() + ": Step 3 File Process" )
		i_multi_log.do_logs( "pdb", def_name() + ": Step 3 A calling: filedupetry_collect" )
		# Collect file information by traversing all the paths
		i_dct_pfn_filemeta, i_dct_shortmatchkey_lst_pfn = \
			filedupetry_collect( i_lst_all_tree_paths, p_FileFlags, i_multi_log)
		i_dct_shortmatchkey_lst_pfn_in_both_sets = make_dct_matchkey_lst_pfn_in_both_sets( i_dct_shortmatchkey_lst_pfn, \
			p_lst_paths_survive, p_lst_paths_dispose, i_multi_log )
		if False:
			print( "i_dct_shortmatchkey_lst_pfn" )
			print( i_dct_shortmatchkey_lst_pfn )
			print( "i_dct_shortmatchkey_lst_pfn_in_both_sets" )
			print( i_dct_shortmatchkey_lst_pfn_in_both_sets )
			input( "It seems we have a problem!")
		if p_FileFlags.ff_Hash :
			i_multi_log.do_logs( "pdb", mod_code_name() + ": Step 3 B calling: hasherdabbery" )
			# first, run through all files deriving hashes and storing them
			i_include_lists_of_one = False # just making this clear
			i_dct_fullmatchkey_lst_pfn = hasherdabbery( i_dct_shortmatchkey_lst_pfn_in_both_sets, i_dct_pfn_filemeta, i_include_lists_of_one, p_FileFlags, i_multi_log)
		else :
			i_multi_log.do_logs( "pdb", def_name_prfx() + "No Step 3 B (Hashing) Required" )
			# set empty dictionary for the return
			i_dct_fullmatchkey_lst_pfn = {}
		if p_fn_Check_NoInterruption():
			i_multi_log.do_logs( "spdb", mod_code_name() + ": Step 3 C calling: process_and_erode_disposals_by_strategy_in_delorder" )
			did_files_by_hash = process_and_erode_disposals_by_strategy_in_delorder( \
				p_lst_paths_survive, p_lst_paths_dispose, 
				i_dct_pfn_filemeta, i_dct_shortmatchkey_lst_pfn_in_both_sets, i_dct_fullmatchkey_lst_pfn, \
				p_FileFlags, p_DelOrderMode, \
				p_docongruentbeforedelete, p_congruentstringent, p_ReportFailuresStringent, p_EnumDeleteMode, \
				i_bashfilename_str, p_fn_Check_NoInterruption, i_multi_log )
		else:
			did_files_by_hash = False
			i_multi_log.do_logs( "spdb", mod_code_name() + ": Aborted before doing Step 3 C calling: process_and_erode_disposals_by_strategy_in_delorder" )
		if p_ReportFailuresStringent :
			pass
	else :
		did_files_by_hash = False
	return ( did_some_by_superhash or did_some_by_hash or did_files_by_hash )

def defoliatry_command_recce( p_lst_paths, \
		p_FileFlags, \
		p_EnumKeepWhich, p_DelOrderMode, \
		p_ignore_zero_len, p_use_names, p_lst_names_bad, \
		p_docongruentbeforedelete, p_congruentstringent, p_ReportFailuresStringent, p_EnumDeleteMode):
	# as an exercise, let's compile ALL the reasons why
	r_is_ok = True
	r_lst_why = [] # this will be a list of strings
	# paths
	if isinstance( p_lst_paths, list ) :
		for i_path in p_lst_paths :
			a_is_ok, a_lst_why = cmntry.consider_path_str( i_path, "Path list" )
			r_is_ok = r_is_ok and a_is_ok
			r_lst_why.extend( a_lst_why )
	else:
		r_is_ok = False
		r_lst_why.append( "Was not passed a list" )
	return r_is_ok, r_lst_why

def defoliatry_command( p_lst_paths, \
		p_FileFlags, \
		p_EnumKeepWhich, p_DelOrderMode, \
		p_ignore_zero_len, p_use_names, p_lst_names_bad, \
		p_docongruentbeforedelete, p_congruentstringent, p_ReportFailuresStringent, p_EnumDeleteMode, p_multi_log):
	# Brief:
	# Provides a common base of functionality callable by either the GUI or command line routines
	# Unlike the "prunatry" approach, this uses a base concept that deletions may occur across the
	# supplied set of folders, according to several possible strategies
	# Parameters:
	# - p_lst_paths :: list of strings := giving the paths to be analysed
	# - p_dct_rdlg :: dictionary of log flags and filenames as per Commontry module
	# Returns:
	# - a quite basic Boolean of whether things were done or not
	# Notes:
	# Code:
	zz_procname = "defoliatry_command"
	# print( zz_procname )
	i_multi_log = im_copy.deepcopy( p_multi_log)
	cmntry.re_filename_multilog_object( i_multi_log, zz_procname )
	def def_name():
		return zz_procname
	def def_name_prfx():
		return def_name() + " "
	# Debugging info
	i_multi_log.do_logs( "b", "Called: " + def_name() )
	i_multi_log.do_logs( "dbsp", def_name_prfx() + "(p_FileFlags Hash, Size, Name, When) " + str(p_FileFlags.ff_Hash) + "," + str(p_FileFlags.ff_Size) + "," + str(p_FileFlags.ff_Name) + "," + str(p_FileFlags.ff_When) )
	i_multi_log.do_logs( "dbs", def_name_prfx() + ": Step 0 Prep" )
	i_multi_log.do_logs( "db", "p_lst_paths" + " has " + str( len(p_lst_paths)) )
	i_use_datetime_str = cmntry.bfff_datetime_ident_str()
	i_bashfilename_str = cmntry.scrf_make_bash_filename( i_use_datetime_str, zz_procname )
	# new method
	i_multi_log.do_logs( "dbs", def_name_prfx() + ": Step 1 filedupetry_collect" )
	# Test calling of rewritten feature
	#i_dct_pfn_filemeta, i_dct_shortmatchkey_lst_pfn, i_dct_hash_lst_pfn, i_dct_fullmatchkey_lst_fileinfohash, i_dct_fullmatchkey_lst_pfn = \
	#	filedupetry_collect( p_lst_paths, p_FileFlags, i_multi_log)
	i_dct_pfn_filemeta, i_dct_shortmatchkey_lst_pfn = filedupetry_collect( p_lst_paths, p_FileFlags, i_multi_log)
	# no need for anything between
	if p_FileFlags.ff_Hash :
		i_multi_log.do_logs( "dbs", def_name() + ": Step 2 Hash collection" )
		# run through all files deriving hashes and storing them
		i_include_lists_of_one = False # just making this clear
		i_dct_fullmatchkey_lst_pfn = hasherdabbery( i_dct_shortmatchkey_lst_pfn, i_dct_pfn_filemeta, i_include_lists_of_one, p_FileFlags, i_multi_log)
	else :
		i_multi_log.do_logs( "dbsp", def_name_prfx() + "No Step 2 Hashing Required" )
		# set empty dictionary for the return
		i_dct_fullmatchkey_lst_pfn = {}
	i_multi_log.do_logs( "dbs", def_name() + ": Step 3 process_and_erode" )
	did_files = process_and_erode_in_match_groups_per_strategy( \
		i_dct_pfn_filemeta, i_dct_shortmatchkey_lst_pfn, i_dct_fullmatchkey_lst_pfn, \
		p_FileFlags, p_EnumKeepWhich, p_DelOrderMode, p_ignore_zero_len, p_use_names, p_lst_names_bad, \
		-1, p_docongruentbeforedelete, p_congruentstringent, p_ReportFailuresStringent, p_EnumDeleteMode, i_bashfilename_str, i_multi_log ) 
	i_multi_log.do_logs( "dbs", def_name() + " = completed!" )
	return ( did_files )

# --------------------------------------------------
# Interactive
# --------------------------------------------------

# Removed - all GUI interaction is now handled by the foldatry module

# --------------------------------------------------
# Command line mode
# --------------------------------------------------

# Removed - all GUI interaction is now handled by the foldatry module


# --------------------------------------------------
# Log controls
# --------------------------------------------------

def setup_logging() :
	# this uses the custom wrapper module "multilog"
	m_log = cmntry.make_multilog_object( mod_code_name() )
	return m_log

# main to execute only if run as a program
# Note: the GUI has been moved to Foldatry
if __name__ == "__main__":
	print( "This is the module: deforestry")
	# later do some smart command line parameter checking
	# e.g. if enough info is passed and/or indication to avoid the GUI
	# setup logging
	multi_log = setup_logging()
	if __debug__:
		multi_log.do_logs( "b", "Not currently running any tests for this module.")
	print( "Not currently running any tests for this module.")
