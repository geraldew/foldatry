#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# applitry = custom features for the foldatry application
#
# --------------------------------------------------
# Copyright (C) 2019-2024  Gerald Waters - Melbourne, Australia
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# --------------------------------------------------
# This is a set of things to be used after being imported into the various
# parts of the foldatry application.
# When compete it will have no purpose in being run on its own, but during
# development, executing this module will run various slef-consistency checks.
#
# As a module, it will hold a number of
# - classes
# - enumerations 
# - extensions of the enumerations
# that need to defined for use throughout foldatry
#
# It is being developed to provide replacements for features in the module:
# - commontry
#
# When the replacments are all done, then commontry will merely hold more
# generic functions, that might well be useful in other programs.

# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# The replacement features
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

# This revamp covers:
# - a revised convention for the custom support Enumeratons - servicing both:
#   - defined specific values used across Foldatry - EnumMatchMode  etc
#   - defined specific values provisioned with Tuples for use in Comboboxes - EnumDeleteMode etc
# - enumerated Settings for saving and reloading
# by revisions as
# - a revised convention of Enumerations combined with support objects - diaply and code values for all the Settings that will be saved and reloaded
# - a revised pair of Classes:
#   - one for the non-GUI operation - using just the reloading of saved settings
#   - one for the GUI operation - using both the saving and reloading of saved settings
#     - note that this class can't /know/ about the GUi elements, so  it much provide functions for use by the GUI code

# To that end, the following code will be in three sections:
# - Section A - revised rendition of extended Enumerations for Commontry - things that will be used by various modules
#     - SubSection 1 - Support
#     - SubSection 2 - the Cultivated Enumerations
#         - Category I enumerations
#         - Category J enumerations - both as a GUI ComboBox and JSON setting
#     - SubSection 3 - class FnameMatchSetting
# - Section B - revised enumeration of Settings for just the Foldatry module
# - Section C - revised rendition of Settings class objects
# - Section D - enumeration of the possibke processes to run

# Underlying theses will be some other, internal construction:
# - a named tuple (nmdtpl_XenumAttrbt) in Section A for use by the various custom support Enumeratons, with support functions to ensure their integrity
# - a named tuple (nmdtpl_SettingAttrbt) in Section B for use by the Settings, with support functions to ensure their integrity

from enum import Enum, unique, auto
from collections import namedtuple
import commontry as cmntry

import json as im_json

# ==================================================
# Just for this test code
# --------------------------------------------------
import random as im_random

def decision(probability):
    return im_random.random() < probability

def PrintAlert( p_InProcName, p_Description):
	# as this module isn't intended to use the MultiLog system, this is the handler for significant debug Print actions
	if __debug__:
		print( "ALERT! (in " + p_InProcName + ") " + p_Description )

# $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
# SECTION A - new rendition of cultivated enumerations for Commontry
# --------------------------------------------------

# These are the "cultivated enumerations" which are ones where there are additional values
# held per enumeration-value than just the enumeration itself. In general these are used for two
# purposes:
# - Category I - only internal-to program-use and including population/control of Tkinter widgets or quotation to outputs (live display, logs etc)
# - Category J - internal-to program-use and also used as values saved and loaded as JSON

# Note: this section does not include:
# - simple enumerations, that require no additional coincident values
# - the enumerations used just to support the Settings system for those, see the next new major section

# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# SubSection 1 - Support
# --------------------------------------------------

# ==================================================
# NamedTuple for use with Extended Enumerated attributes
# --------------------------------------------------
# The idea is that there will be a number of these Enumerations that are to be
# furnished with extended attributes and so this named tuple is for use by each of them

# In some contexts it might seem verbose to have one spelled out indepdendently but
# that is precisely the point, so that any one of them can be changed to use some other
# structuring if a need for that becomes apparent.
# Besides, a lot of the point here is to have code that specifies inherent features
# without the need for resources fomr outside the written program

# We will later use most of these custom enumerations in an organised fashion
# where we want the main program to have loadable "settings" of these but that
# concept should only be known to the main module, whereas each custom enumeration
# will be used across any relevant modules.

# conjured term is: Xenum == eXtended Enumeration - for which we add a NamedTuple to hold its attributes

nmdtpl_XenumAttrbt = namedtuple('nmdtpl_XenumAttrbt', 'XenumJval XenumBrief XenumShow XenumSort')

# where:
# - Jval = abbreviation-key, for use in encoded situations, must be unique so it can be used as a key or unique value e.g. in JSON
# - Brief - for use in ComboxBox selections
# - Show - for use in displays that need to be clear about meaning and not concerned about brevity
# - Sort - for use in ordering displays

# have a simple bullet proof type-safe assignment function
# this ensures that we get back a tuple of strings
def Make_XenumAttrbt( p_XenumJval, p_XenumBrief, p_XenumShow, p_XenumSort ):
	# p_XenumJval = a string that will be used for the JSON value storage - this needs to be unique per Enumeration
	# p_XenumBrief = a brief display string
	# p_XenumShow - a fuller descriptive string
	# p_XenumSort = a sorting code to control display order independent of the other attributes - e.g. so the most commonly needed Enumeration-value is first
	# this function is provided to ensure that all the passed parameter attributes become strings
	i_XenumJval = str( p_XenumJval )
	i_XenumBrief = str( p_XenumBrief )
	i_XenumShow = str( p_XenumShow )
	i_XenumSort = str( p_XenumSort )
	return nmdtpl_XenumAttrbt( i_XenumJval, i_XenumBrief, i_XenumShow, i_XenumSort )

# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# SubSection 2 - the Cultivated Enumerations
# --------------------------------------------------

# --------------------------------------------------
# Here we will have each of the Cultivated Enumerations, each of which will have a
# discretely unobvious dictionary of extended attributes per enumeration value
# after being set, these are essential read-only resources, which is why they are unobvious 

# There are two sets of Cultivated Enumerations:
# - Category I enumerations
#   - EnumClonatryMode - just a GUI feature DEPRECATED
#   - EnumMatchMode` - a quick-set option for the four boolean match parts (JustHash, HashEtName, HashEtWhen, HashEtAll, NoHashing, JustName, JustWhen, ModeCustom)
#   - EnumFileSysArch - a quick-set options for the parts: character-encoding (itself a category J), and booleans for case-sensitive, MS-bad, MS-chars, MS8.3
# - Category J enumerations - both as a GUI ComboBox and JSON setting
#   - EnumDelOrder - which match groups to prioritise, 
#   - EnumDeleteMode - how to actually delete things
#   - EnumKeepWhich - which duplicates to keep (KeepOld, KeepNew, KeepAlphaFirst, KeepAlphaLast, KeepNumericFirst, KeepNumericLast, KeepPathLongest, KeepPathShortest, KeepPathDeepest, KeepPathShallowest)
#   - EnumCharEnc - a subset of known string character-encoding values

#   - EnumRenametryMode - operation modes for Renametry
#

# **************************************************
# Category I enumerations
# --------------------------------------------------

# Note: for these, we re-use the same Extended Attribute nametuple base, 
# while we don't need all of that e.g. the JSON value - re-using 
# the same tuple format is less confusing than making up yet another custom one

# - EnumMatchMode (replaces DefoliatryMode ) coded
# - EnumClonatryMode (replaces ClonatryMode) coded  DEPRECATED
# - EnumFileSysArch (replaces FileSysArchetype ) coded
# - EnumGenCopyScriptAs (replaces GenCopyScriptMode ) coded

# ==================================================
# Extended Enumeration EnumMatchMode - not used by Settings so doesn't need extras
# --------------------------------------------------

@unique
class EnumMatchMode(Enum):
	JustHash = auto()
	HashEtName = auto()
	HashEtWhen = auto()
	HashEtAll = auto()
	JustSize = auto()
	JustName = auto()
	JustWhen = auto()
	SizeNameWhen = auto()
	ModeCustom = auto()

# The internal-use-only structures made by run-once setups - these get created when the module is substantiated

# An internal dictionary of attributes - thereby at run-time is just a quick lookup 
# create the dictionary empty
_Dct_EnumMatchMode_Attrbt = {}
# populate the dictionary
_Dct_EnumMatchMode_Attrbt[ EnumMatchMode.JustHash ] = \
	Make_XenumAttrbt( "JustHash", "File content hashing", "Just File content hashing", "A" )
_Dct_EnumMatchMode_Attrbt[ EnumMatchMode.HashEtName ] = \
	Make_XenumAttrbt( "HashEtName", "Content hash and Name", "Content hash and Name", "B" )
_Dct_EnumMatchMode_Attrbt[ EnumMatchMode.HashEtWhen ] = \
	Make_XenumAttrbt( "HashEtWhen", "Content hash and When", "Content hash and When", "C" )
_Dct_EnumMatchMode_Attrbt[ EnumMatchMode.HashEtAll ] = \
	Make_XenumAttrbt( "HashEtAll", "Content hash, Name and When", "Content hash, Name and When", "D" )
_Dct_EnumMatchMode_Attrbt[ EnumMatchMode.JustSize ] = \
	Make_XenumAttrbt( "JustSize", "No hashing just by Size", "No hashing just by Size", "E" )
_Dct_EnumMatchMode_Attrbt[ EnumMatchMode.JustName ] = \
	Make_XenumAttrbt( "JustName", "No hashing just by Name", "No hashing just by Name", "F" )
_Dct_EnumMatchMode_Attrbt[ EnumMatchMode.JustWhen ] = \
	Make_XenumAttrbt( "JustWhen", "No hashing just by When", "No hashing just by When", "G" )
_Dct_EnumMatchMode_Attrbt[ EnumMatchMode.SizeNameWhen ] = \
	Make_XenumAttrbt( "SizeNameWhen", "No hashing, by Size,Name,When", "No hashing, by Size,Name,When", "H" )
_Dct_EnumMatchMode_Attrbt[ EnumMatchMode.ModeCustom ] = \
	Make_XenumAttrbt( "ModeCustom", "Custom combination", "Custom combination", "I" )

# A reverse lookup of the Jval text values - and in doing so confirm all are there and are unique
_Dct_EnumMatchMode_Jval_To_Enum = {}
_Dct_EnumMatchMode_Jval_To_Enum = {}
for i_EnumMatchMode in EnumMatchMode :
	# test for clash in reverse dictionary
	if _Dct_EnumMatchMode_Attrbt[ i_EnumMatchMode ].XenumJval in _Dct_EnumMatchMode_Jval_To_Enum :
		# trigger an error
		x = 1 / 0
	else :
		# add to reverse dictionary
		_Dct_EnumMatchMode_Jval_To_Enum[ _Dct_EnumMatchMode_Attrbt[ i_EnumMatchMode ].XenumJval ] = i_EnumMatchMode

# An internal list of pair tuples for use in selectors - first item is the enumeration, second is the string to show 
# example: this can be passed to a modifiec ComboBox widget, usch as Foldatry's PairTupleCombobox
# create the list empty 
_Lst_EnumMatchMode_ComboBoxPairTuples = []
# populate the list
for i_EnumMatchMode in EnumMatchMode :
	# make a tuple of the enum and the brief - as that's what we'll want for Combobox gadgets
	i_tpl = ( i_EnumMatchMode, _Dct_EnumMatchMode_Attrbt[ i_EnumMatchMode ].XenumBrief )
	# add the tuple to the dictionary
	_Lst_EnumMatchMode_ComboBoxPairTuples.append( i_tpl )

# Function calls - so that other uses don't have to know about the internals

# Define which is the default value in the enumeration
def EnumMatchMode_Default() :
	return EnumMatchMode.SizeNameWhen

# - Functions for fetching each of the parts

def EnumMatchMode_Attrbt_Jval( p_EnumMatchMode ):
	if isinstance( p_EnumMatchMode, EnumMatchMode) :
		return _Dct_EnumMatchMode_Attrbt[ p_EnumMatchMode ].XenumJval
	else :
		return None

def EnumMatchMode_Attrbt_Brief( p_EnumMatchMode ):
	if isinstance( p_EnumMatchMode, EnumMatchMode) :
		return _Dct_EnumMatchMode_Attrbt[ p_EnumMatchMode ].XenumBrief
	else :
		return None

def EnumMatchMode_Attrbt_Show( p_EnumMatchMode ):
	if isinstance( p_EnumMatchMode, EnumMatchMode) :
		return _Dct_EnumMatchMode_Attrbt[ p_EnumMatchMode ].XenumShow
	else :
		return None

def EnumMatchMode_Attrbt_Sort( p_EnumMatchMode ):
	if isinstance( p_EnumMatchMode, EnumMatchMode) and p_EnumMatchMode in _Dct_EnumMatchMode_Attrbt :
		return _Dct_EnumMatchMode_Attrbt[ p_EnumMatchMode ].XenumSort
	else :
		return None

# - second, for fetching the list of pair tuples, this is what will be passed to the Combobox gadget

def EnumMatchMode_LstPairTpls( ):
	return _Lst_EnumMatchMode_ComboBoxPairTuples

# third, provide a translation from the JSON value

def EnumMatchMode_For_Jval_Get_Enum( p_txt ) :
	if isinstance( p_txt, str) and p_txt in _Dct_EnumMatchMode_Jval_To_Enum :
		return _Dct_EnumMatchMode_Jval_To_Enum[ p_txt ]
	else :
		return None

# --------------------------------------------------
# custom calls

def EnumMatchMode_GetCheckCombination( p_EnumMatchMode ):
	r_Hash = False
	r_Size = False
	r_Name = False
	r_When = False
	if p_EnumMatchMode == EnumMatchMode.JustHash :
		r_Hash = True
		r_Size = True
	elif p_EnumMatchMode == EnumMatchMode.HashEtName :
		r_Hash = True
		r_Size = True
		r_Name = True
	elif p_EnumMatchMode == EnumMatchMode.HashEtWhen :
		r_Hash = True
		r_Size = True
		r_When = True
	elif p_EnumMatchMode == EnumMatchMode.HashEtAll :
		r_Hash = True
		r_Size = True
		r_Name = True
		r_When = True
	elif p_EnumMatchMode == EnumMatchMode.JustSize :
		r_Size = True
	elif p_EnumMatchMode == EnumMatchMode.JustName :
		r_Name = True
	elif p_EnumMatchMode == EnumMatchMode.JustWhen :
		r_When = True
	elif p_EnumMatchMode == EnumMatchMode.SizeNameWhen :
		r_Size = True
		r_Name = True
		r_When = True
	elif p_EnumMatchMode == EnumMatchMode.ModeCustom :
		r_Size = True
		r_Name = True
	return r_Hash, r_Size, r_Name, r_When

def EnumMatchMode_Combination_IsViable( p_Hash, p_Size, p_Name, p_When ):
	if p_Hash or p_Size or p_Name or p_When :
		return True
	else :
		return False

def EnumMatchMode_ForChecks_GetCombination( p_Hash, p_Size, p_Name, p_When) :
	r_EnumMatchMode = EnumMatchMode.JustHash
	i_hit = False
	for i_EnumMatchMode in EnumMatchMode :
		if ( p_Hash, p_Size, p_Name, p_When) == EnumMatchMode_GetCheckCombination( i_EnumMatchMode ) :
			r_EnumMatchMode = i_EnumMatchMode
			i_hit = True
			break
	if not i_hit :
		if not p_Hash :
			r_EnumMatchMode = EnumMatchMode.ModeCustom
	return r_EnumMatchMode


# ==================================================
# Extended Enumeration EnumClonatryMode - not used by Settings so doesn't need extras DEPRECATED
# --------------------------------------------------

@unique
class EnumClonatryMode(Enum):
	DoDistinctry = auto()
	DoClonatry = auto() # was DoCopyAnew
	DoDubatry = auto()
	DoMergetry = auto() # was DoCopyMerge
	DoSynopsitry = auto() # was DoCopyStructure
	DoMigratry = auto() # was DoCopyMigrate
	DoSkeletry = auto() # was DoWhiteAnt
	DoMovetry = auto()
	DoShiftatry = auto()
	DoShiftafile = auto()

# The internal-use-only structures made by run-once setups - these get created when the module is substantiated

# An internal dictionary of attributes - thereby at run-time is just a quick lookup 
# create the dictionary empty
_Dct_EnumClonatryMode_Attrbt = {}
# populate the dictionary
_Dct_EnumClonatryMode_Attrbt[ EnumClonatryMode.DoDistinctry ] = \
	Make_XenumAttrbt( "DoDistinctry", "Distinctry", "copy the things in A that are not in B, to C ", "A" )
_Dct_EnumClonatryMode_Attrbt[ EnumClonatryMode.DoClonatry ] = \
	Make_XenumAttrbt( "DoClonatry", "Clonatry", "Clone a tree: copy to new location", "B" )
_Dct_EnumClonatryMode_Attrbt[ EnumClonatryMode.DoDubatry ] = \
	Make_XenumAttrbt( "DoDubatry", "Dubatry", "Dub a tree: copy to location without clashing", "C" )
_Dct_EnumClonatryMode_Attrbt[ EnumClonatryMode.DoMergetry ] = \
	Make_XenumAttrbt( "DoMergetry", "Mergetry", "Merge a tree: copy into perhaps existing structures with over-writing", "D" )
_Dct_EnumClonatryMode_Attrbt[ EnumClonatryMode.DoSynopsitry ] = \
	Make_XenumAttrbt( "DoSynopsitry", "Synopsitry", "Synopsis of a tree: copy folder structures but not the files", "E" )
_Dct_EnumClonatryMode_Attrbt[ EnumClonatryMode.DoMigratry ] = \
	Make_XenumAttrbt( "DoMigratry", "Migratry", "Migrate a tree: move folders and files, leaving behind empty folders", "F" )
_Dct_EnumClonatryMode_Attrbt[ EnumClonatryMode.DoSkeletry ] = \
	Make_XenumAttrbt( "DoSkeletry", "Skeletry", "Skeletonise a tree: delete files but not the folder structures", "G" )
_Dct_EnumClonatryMode_Attrbt[ EnumClonatryMode.DoMovetry ] = \
	Make_XenumAttrbt( "DoMovetry", "Movetry", "Move a tree: ordinary copy and then delete source", "H" )
_Dct_EnumClonatryMode_Attrbt[ EnumClonatryMode.DoShiftatry ] = \
	Make_XenumAttrbt( "DoShiftatry", "Shiftatry", "Shift a tree: relocate by multiple sub-shifts", "I" )
_Dct_EnumClonatryMode_Attrbt[ EnumClonatryMode.DoShiftafile ] = \
	Make_XenumAttrbt( "DoShiftafile", "Shiftafile", "Shift a file: relocate by multiple sub-shifting parts of a file", "J" )

# A reverse lookup of the Jval text values - and in doing so confirm all are there and are unique
_Dct_EnumClonatryMode_Jval_To_Enum = {}
_Dct_EnumClonatryMode_Jval_To_Enum = {}
for i_EnumClonatryMode in EnumClonatryMode :
	# test for clash in reverse dictionary
	if _Dct_EnumClonatryMode_Attrbt[ i_EnumClonatryMode ].XenumJval in _Dct_EnumClonatryMode_Jval_To_Enum :
		# trigger an error
		x = 1 / 0
	else :
		# add to reverse dictionary
		_Dct_EnumClonatryMode_Jval_To_Enum[ _Dct_EnumClonatryMode_Attrbt[ i_EnumClonatryMode ].XenumJval ] = i_EnumClonatryMode

# An internal list of pair tuples for use in selectors - first item is the enumeration, second is the string to show 
# example: this can be passed to a modifiec ComboBox widget, usch as Foldatry's PairTupleCombobox
# create the list empty 
_Lst_EnumClonatryMode_ComboBoxPairTuples = []
# populate the list
for i_EnumClonatryMode in EnumClonatryMode :
	# make a tuple of the enum and the brief - as that's what we'll want for Combobox gadgets
	i_tpl = ( i_EnumClonatryMode, _Dct_EnumClonatryMode_Attrbt[ i_EnumClonatryMode ].XenumBrief )
	# add the tuple to the dictionary
	_Lst_EnumClonatryMode_ComboBoxPairTuples.append( i_tpl )

# Function calls - so that other uses don't have to know about the internals

# Define which is the default value in the enumeration
def EnumClonatryMode_Default() :
	return EnumClonatryMode.DoDistinctry

# - Functions for fetching each of the parts

def EnumClonatryMode_Attrbt_Jval( p_EnumClonatryMode ):
	if isinstance( p_EnumClonatryMode, EnumClonatryMode) :
		return _Dct_EnumClonatryMode_Attrbt[ p_EnumClonatryMode ].XenumJval
	else :
		return None

def EnumClonatryMode_Attrbt_Brief( p_EnumClonatryMode ):
	if isinstance( p_EnumClonatryMode, EnumClonatryMode) :
		return _Dct_EnumClonatryMode_Attrbt[ p_EnumClonatryMode ].XenumBrief
	else :
		return None

def EnumClonatryMode_Attrbt_Show( p_EnumClonatryMode ):
	if isinstance( p_EnumClonatryMode, EnumClonatryMode) :
		return _Dct_EnumClonatryMode_Attrbt[ p_EnumClonatryMode ].XenumShow
	else :
		return None

def EnumClonatryMode_Attrbt_Sort( p_EnumClonatryMode ):
	if isinstance( p_EnumClonatryMode, EnumClonatryMode) and p_EnumClonatryMode in _Dct_EnumClonatryMode_Attrbt :
		return _Dct_EnumClonatryMode_Attrbt[ p_EnumClonatryMode ].XenumSort
	else :
		return None

# - second, for fetching the list of pair tuples, this is what will be passed to the Combobox gadget

def EnumClonatryMode_LstPairTpls( ):
	return _Lst_EnumClonatryMode_ComboBoxPairTuples

# third, provide a translation from the JSON value

def EnumClonatryMode_For_Jval_Get_Enum( p_txt ) :
	if isinstance( p_txt, str) and p_txt in _Dct_EnumClonatryMode_Jval_To_Enum :
		return _Dct_EnumClonatryMode_Jval_To_Enum[ p_txt ]
	else :
		return None

def EnumClonatryMode_Triples( p_mode_enum ) :
	if p_mode_enum == EnumClonatryMode.DoDistinctry :
		# "Work out which things are on A but not on B and then copy those things to C"
		str_a = "Look for things in A"
		str_b = "that are not in B"
		str_c = "then copy them to C"
	elif p_mode_enum == EnumClonatryMode.DoClonatry :
		# "Copy to new location - i.e. don't allow any mixing"
		str_a = "Look for things in A"
		str_b = "--ignore this--"
		str_c = "then copy them fresh to C"
	elif p_mode_enum == EnumClonatryMode.DoMergetry :
		# "Copy from A structures into B structures, merging the contents"
		str_a = "Look for things in A"
		str_b = "--ignore this--"
		str_c = "then merge them into C"
	elif p_mode_enum == EnumClonatryMode.DoSynopsitry :
		# "Copy only the structure of A into B - i.e. folders only, no files."
		str_a = "Look for things in A"
		str_b = "--ignore this--"
		str_c = "Copy the folder structures into C"
	elif p_mode_enum == EnumClonatryMode.DoMigratry :
		# "Copy files and folders of A into B leaving behind only the folder structure in A"
		str_a = "Copy from here then DELETE FILES here but leave folders intact"
		str_b = "--ignore this--"
		str_c = "Write Files and folders to here"
	elif p_mode_enum == EnumClonatryMode.DoSkeletry :
		# "Clear the A structure of files leaving the folders in place"
		str_a = "DELETE FILES here but leave folders intact"
		str_b = "--ignore this--"
		str_c = "--ignore this--"
	else :
		# "Unknown selection"
		str_a = ""
		str_b = ""
		str_c = ""
	return str_a, str_b, str_c


# ==================================================
# Extended Enumeration EnumFileSysArch - not used by Settings so doesn't need extras
# --------------------------------------------------

@unique
class EnumFileSysArch(Enum):
	FSA_Linux = auto()
	FSA_Windows = auto()
	FSA_DosWin = auto()
	FSA_PCDOS = auto()
	FSA_Custom = auto()

# The internal-use-only structures made by run-once setups - these get created when the module is substantiated

# An internal dictionary of attributes - thereby at run-time is just a quick lookup 
# create the dictionary empty
_Dct_EnumFileSysArch_Attrbt = {}
# populate the dictionary
_Dct_EnumFileSysArch_Attrbt[ EnumFileSysArch.FSA_Linux ] = \
	Make_XenumAttrbt( "FSA_Linux", "Linux", "extX - no assumed encoding (but in Python3 this requires double-handling)", "A" )
_Dct_EnumFileSysArch_Attrbt[ EnumFileSysArch.FSA_Windows ] = \
	Make_XenumAttrbt( "FSA_Windows", "modern Windows", "latter-day NTFS - UTF-8", "B" )
_Dct_EnumFileSysArch_Attrbt[ EnumFileSysArch.FSA_DosWin ] = \
	Make_XenumAttrbt( "FSA_DosWin", "early Windows", "Win9X and early NTFS - cp1252", "C" )
_Dct_EnumFileSysArch_Attrbt[ EnumFileSysArch.FSA_PCDOS ] = \
	Make_XenumAttrbt( "FSA_PCDOS", "PC-DOS MS-DOS", "Those early DOS block symbols cp437", "D" )
_Dct_EnumFileSysArch_Attrbt[ EnumFileSysArch.FSA_Custom ] = \
	Make_XenumAttrbt( "FSA_Custom", "Custom", "something else", "E" )

# A reverse lookup of the Jval text values - and in doing so confirm all are there and are unique
_Dct_EnumFileSysArch_Jval_To_Enum = {}
_Dct_EnumFileSysArch_Jval_To_Enum = {}
for i_EnumFileSysArch in EnumFileSysArch :
	# test for clash in reverse dictionary
	if _Dct_EnumFileSysArch_Attrbt[ i_EnumFileSysArch ].XenumJval in _Dct_EnumFileSysArch_Jval_To_Enum :
		# trigger an error
		x = 1 / 0
	else :
		# add to reverse dictionary
		_Dct_EnumFileSysArch_Jval_To_Enum[ _Dct_EnumFileSysArch_Attrbt[ i_EnumFileSysArch ].XenumJval ] = i_EnumFileSysArch

# An internal list of pair tuples for use in selectors - first item is the enumeration, second is the string to show 
# example: this can be passed to a modifiec ComboBox widget, usch as Foldatry's PairTupleCombobox
# create the list empty 
_Lst_EnumFileSysArch_ComboBoxPairTuples = []
# populate the list
for i_EnumFileSysArch in EnumFileSysArch :
	# make a tuple of the enum and the brief - as that's what we'll want for Combobox gadgets
	i_tpl = ( i_EnumFileSysArch, _Dct_EnumFileSysArch_Attrbt[ i_EnumFileSysArch ].XenumBrief )
	# add the tuple to the dictionary
	_Lst_EnumFileSysArch_ComboBoxPairTuples.append( i_tpl )

# Function calls - so that other uses don't have to know about the internals

# Define which is the default value in the enumeration
def EnumFileSysArch_Default() :
	return EnumFileSysArch.FSA_Linux

# - Functions for fetching each of the parts

def EnumFileSysArch_Attrbt_Jval( p_EnumFileSysArch ):
	if isinstance( p_EnumFileSysArch, EnumFileSysArch) :
		return _Dct_EnumFileSysArch_Attrbt[ p_EnumFileSysArch ].XenumJval
	else :
		return None

def EnumFileSysArch_Attrbt_Brief( p_EnumFileSysArch ):
	if isinstance( p_EnumFileSysArch, EnumFileSysArch) :
		return _Dct_EnumFileSysArch_Attrbt[ p_EnumFileSysArch ].XenumBrief
	else :
		return None

def EnumFileSysArch_Attrbt_Show( p_EnumFileSysArch ):
	if isinstance( p_EnumFileSysArch, EnumFileSysArch) :
		return _Dct_EnumFileSysArch_Attrbt[ p_EnumFileSysArch ].XenumShow
	else :
		return None

def EnumFileSysArch_Attrbt_Sort( p_EnumFileSysArch ):
	if isinstance( p_EnumFileSysArch, EnumFileSysArch) and p_EnumFileSysArch in _Dct_EnumFileSysArch_Attrbt :
		return _Dct_EnumFileSysArch_Attrbt[ p_EnumFileSysArch ].XenumSort
	else :
		return None

# - second, for fetching the list of pair tuples, this is what will be passed to the Combobox gadget

def EnumFileSysArch_LstPairTpls( ):
	return _Lst_EnumFileSysArch_ComboBoxPairTuples

# third, provide a translation from the JSON value

def EnumFileSysArch_For_Jval_Get_Enum( p_txt ) :
	if isinstance( p_txt, str) and p_txt in _Dct_EnumFileSysArch_Jval_To_Enum :
		return _Dct_EnumFileSysArch_Jval_To_Enum[ p_txt ]
	else :
		return None

# ==================================================
# Extended Enumeration EnumGenCopyScriptAs - not used by Settings so doesn't need extras
# --------------------------------------------------

@unique
class EnumGenCopyScriptAs(Enum):
	GCS_Bash = auto()
	#GCS_DosBat = auto() # not yet supported
	#GCS_WinCmd = auto() # not yet supported
	#GCS_Powershell = auto() # not yet supported

# The internal-use-only structures made by run-once setups - these get created when the module is substantiated

# An internal dictionary of attributes - thereby at run-time is just a quick lookup 
# create the dictionary empty
_Dct_EnumGenCopyScriptAs_Attrbt = {}
# populate the dictionary
_Dct_EnumGenCopyScriptAs_Attrbt[ EnumGenCopyScriptAs.GCS_Bash ] = \
	Make_XenumAttrbt( "GCS_Bash", "bash", "Linux bash script", "A" )
#_Dct_EnumGenCopyScriptAs_Attrbt[ EnumGenCopyScriptAs.GCS_DosBat ] = \
#	Make_XenumAttrbt( "GCS_DosBat", "MS-DOS batch script", "MS-DOS .BAT script", "B" )
#_Dct_EnumGenCopyScriptAs_Attrbt[ EnumGenCopyScriptAs.GCS_WinCmd ] = \
#	Make_XenumAttrbt( "GCS_WinCmd", "Windows command script", "Windows .CMD script", "C" )
#_Dct_EnumGenCopyScriptAs_Attrbt[ EnumGenCopyScriptAs.GCS_Powershell ] = \
#	Make_XenumAttrbt( "GCS_Powershell", "Powershell script", "Windows Powershell script", "D" )

# A reverse lookup of the Jval text values - and in doing so confirm all are there and are unique
_Dct_EnumGenCopyScriptAs_Jval_To_Enum = {}
_Dct_EnumGenCopyScriptAs_Jval_To_Enum = {}
for i_EnumGenCopyScriptAs in EnumGenCopyScriptAs :
	# test for clash in reverse dictionary
	if _Dct_EnumGenCopyScriptAs_Attrbt[ i_EnumGenCopyScriptAs ].XenumJval in _Dct_EnumGenCopyScriptAs_Jval_To_Enum :
		# trigger an error
		x = 1 / 0
	else :
		# add to reverse dictionary
		_Dct_EnumGenCopyScriptAs_Jval_To_Enum[ _Dct_EnumGenCopyScriptAs_Attrbt[ i_EnumGenCopyScriptAs ].XenumJval ] = i_EnumGenCopyScriptAs

# An internal list of pair tuples for use in selectors - first item is the enumeration, second is the string to show 
# example: this can be passed to a modifiec ComboBox widget, usch as Foldatry's PairTupleCombobox
# create the list empty 
_Lst_EnumGenCopyScriptAs_ComboBoxPairTuples = []
# populate the list
for i_EnumGenCopyScriptAs in EnumGenCopyScriptAs :
	# make a tuple of the enum and the brief - as that's what we'll want for Combobox gadgets
	i_tpl = ( i_EnumGenCopyScriptAs, _Dct_EnumGenCopyScriptAs_Attrbt[ i_EnumGenCopyScriptAs ].XenumBrief )
	# add the tuple to the dictionary
	_Lst_EnumGenCopyScriptAs_ComboBoxPairTuples.append( i_tpl )

# Function calls - so that other uses don't have to know about the internals

# Define which is the default value in the enumeration
def EnumGenCopyScriptAs_Default() :
	return EnumGenCopyScriptAs.GCS_Bash

# - Functions for fetching each of the parts

def EnumGenCopyScriptAs_Attrbt_Jval( p_EnumGenCopyScriptAs ):
	if isinstance( p_EnumGenCopyScriptAs, EnumGenCopyScriptAs) :
		return _Dct_EnumGenCopyScriptAs_Attrbt[ p_EnumGenCopyScriptAs ].XenumJval
	else :
		return None

def EnumGenCopyScriptAs_Attrbt_Brief( p_EnumGenCopyScriptAs ):
	if isinstance( p_EnumGenCopyScriptAs, EnumGenCopyScriptAs) :
		return _Dct_EnumGenCopyScriptAs_Attrbt[ p_EnumGenCopyScriptAs ].XenumBrief
	else :
		return None

def EnumGenCopyScriptAs_Attrbt_Show( p_EnumGenCopyScriptAs ):
	if isinstance( p_EnumGenCopyScriptAs, EnumGenCopyScriptAs) :
		return _Dct_EnumGenCopyScriptAs_Attrbt[ p_EnumGenCopyScriptAs ].XenumShow
	else :
		return None

def EnumGenCopyScriptAs_Attrbt_Sort( p_EnumGenCopyScriptAs ):
	if isinstance( p_EnumGenCopyScriptAs, EnumGenCopyScriptAs) and p_EnumGenCopyScriptAs in _Dct_EnumGenCopyScriptAs_Attrbt :
		return _Dct_EnumGenCopyScriptAs_Attrbt[ p_EnumGenCopyScriptAs ].XenumSort
	else :
		return None

# - second, for fetching the list of pair tuples, this is what will be passed to the Combobox gadget

def EnumGenCopyScriptAs_LstPairTpls( ):
	return _Lst_EnumGenCopyScriptAs_ComboBoxPairTuples

# third, provide a translation from the JSON value

def EnumGenCopyScriptAs_For_Jval_Get_Enum( p_txt ) :
	if isinstance( p_txt, str) and p_txt in _Dct_EnumGenCopyScriptAs_Jval_To_Enum :
		return _Dct_EnumGenCopyScriptAs_Jval_To_Enum[ p_txt ]
	else :
		return None


# **************************************************
# Category J enumerations
# --------------------------------------------------

# - EnumKeepWhich (replaces InGroupMode ) coded
# - EnumDelOrder (replaces DelOrderMode ) coded
# - EnumDeleteMode (replaces DeletionMode ) coded
# - EnumCharEnc (replaces CharEncMode ) coded

# - EnumRenametryMode (new for Renametry)

# ==================================================
# Extended Enumeration EnumKeepWhich
# --------------------------------------------------

class EnumKeepWhich(Enum):
	KeepOld = auto() # "All but Oldest"
	KeepNew = auto() # "All but Newest"
	KeepAlphaFirst = auto() # "All but First Alphabetical"
	KeepAlphaLast = auto() # "All but Last Alphabetical"
	KeepNumericFirst = auto() # "All but First Numerical"
	KeepNumericLast = auto() # "All but Last Numerical"
	KeepPathLongest = auto() # "All but Longest Path"
	KeepPathShortest = auto() # "All but Shortest Path"
	KeepPathDeepest = auto() # "All but Deepest Path"
	KeepPathShallowest = auto() # "All but Shallowest Path"

# The internal-use-only structures made by run-once setups - these get created when the module is substantiated

# An internal dictionary of attributes - thereby at run-time is just a quick lookup 
# create the dictionary empty
_Dct_EnumKeepWhich_Attrbt = {}
# populate the dictionary
_Dct_EnumKeepWhich_Attrbt[ EnumKeepWhich.KeepOld ] = \
	Make_XenumAttrbt( "KeepOld", "All but Oldest", "All but Oldest", "A" )
_Dct_EnumKeepWhich_Attrbt[ EnumKeepWhich.KeepNew ] = \
	Make_XenumAttrbt( "KeepNew", "All but Newest", "All but Newest", "B" )
_Dct_EnumKeepWhich_Attrbt[ EnumKeepWhich.KeepAlphaFirst ] = \
	Make_XenumAttrbt( "KeepAlphaFirst", "All but First Alphabetical", "All but First Alphabetical", "C" )
_Dct_EnumKeepWhich_Attrbt[ EnumKeepWhich.KeepAlphaLast ] = \
	Make_XenumAttrbt( "KeepAlphaLast", "All but Last Alphabetical", "All but Last Alphabetical", "D" )
_Dct_EnumKeepWhich_Attrbt[ EnumKeepWhich.KeepNumericFirst ] = \
	Make_XenumAttrbt( "KeepNumericFirst", "All but First Numerical", "All but First Numerical", "E" )
_Dct_EnumKeepWhich_Attrbt[ EnumKeepWhich.KeepNumericLast ] = \
	Make_XenumAttrbt( "KeepNumericLast", "All but Last Numerical", "All but Last Numerical", "F" )
_Dct_EnumKeepWhich_Attrbt[ EnumKeepWhich.KeepPathLongest ] = \
	Make_XenumAttrbt( "KeepPathLongest", "All but Longest Path", "All but Longest Path", "G" )
_Dct_EnumKeepWhich_Attrbt[ EnumKeepWhich.KeepPathShortest ] = \
	Make_XenumAttrbt( "KeepPathShortest", "All but Shortest Path", "All but Shortest Path", "H" )
_Dct_EnumKeepWhich_Attrbt[ EnumKeepWhich.KeepPathDeepest ] = \
	Make_XenumAttrbt( "KeepPathDeepest", "All but Deepest Path", "All but Deepest Path", "I" )
_Dct_EnumKeepWhich_Attrbt[ EnumKeepWhich.KeepPathShallowest ] = \
	Make_XenumAttrbt( "KeepPathShallowest", "All but Shallowest Path", "All but Shallowest Path", "J" )

# A reverse lookup of the Jval text values - and in doing so confirm all are there and are unique
_Dct_EnumKeepWhich_Jval_To_Enum = {}
_Dct_EnumKeepWhich_Jval_To_Enum = {}
for i_EnumKeepWhich in EnumKeepWhich :
	# test for clash in reverse dictionary
	if _Dct_EnumKeepWhich_Attrbt[ i_EnumKeepWhich ].XenumJval in _Dct_EnumKeepWhich_Jval_To_Enum :
		# trigger an error
		x = 1 / 0
	else :
		# add to reverse dictionary
		_Dct_EnumKeepWhich_Jval_To_Enum[ _Dct_EnumKeepWhich_Attrbt[ i_EnumKeepWhich ].XenumJval ] = i_EnumKeepWhich

# An internal list of pair tuples for use in selectors - first item is the enumeration, second is the string to show 
# example: this can be passed to a modifiec ComboBox widget, usch as Foldatry's PairTupleCombobox
# create the list empty 
_Lst_EnumKeepWhich_ComboBoxPairTuples = []
# populate the list
for i_EnumKeepWhich in EnumKeepWhich :
	# make a tuple of the enum and the brief - as that's what we'll want for Combobox gadgets
	i_tpl = ( i_EnumKeepWhich, _Dct_EnumKeepWhich_Attrbt[ i_EnumKeepWhich ].XenumBrief )
	# add the tuple to the dictionary
	_Lst_EnumKeepWhich_ComboBoxPairTuples.append( i_tpl )

# Function calls - so that other uses don't have to know about the internals

# Define which is the default value in the enumeration
def EnumKeepWhich_Default() :
	return EnumKeepWhich.KeepOld

# - Functions for fetching each of the parts

def EnumKeepWhich_Attrbt_Jval( p_EnumKeepWhich ):
	if isinstance( p_EnumKeepWhich, EnumKeepWhich) :
		return _Dct_EnumKeepWhich_Attrbt[ p_EnumKeepWhich ].XenumJval
	else :
		return None

def EnumKeepWhich_Attrbt_Brief( p_EnumKeepWhich ):
	if isinstance( p_EnumKeepWhich, EnumKeepWhich) :
		return _Dct_EnumKeepWhich_Attrbt[ p_EnumKeepWhich ].XenumBrief
	else :
		return None

def EnumKeepWhich_Attrbt_Show( p_EnumKeepWhich ):
	if isinstance( p_EnumKeepWhich, EnumKeepWhich) :
		return _Dct_EnumKeepWhich_Attrbt[ p_EnumKeepWhich ].XenumShow
	else :
		return None

def EnumKeepWhich_Attrbt_Sort( p_EnumKeepWhich ):
	if isinstance( p_EnumKeepWhich, EnumKeepWhich) and p_EnumKeepWhich in _Dct_EnumKeepWhich_Attrbt :
		return _Dct_EnumKeepWhich_Attrbt[ p_EnumKeepWhich ].XenumSort
	else :
		return None

# - second, for fetching the list of pair tuples, this is what will be passed to the Combobox gadget

def EnumKeepWhich_LstPairTpls( ):
	return _Lst_EnumKeepWhich_ComboBoxPairTuples

# third, provide a translation from the JSON value

def EnumKeepWhich_For_Jval_Get_Enum( p_txt ) :
	if isinstance( p_txt, str) and p_txt in _Dct_EnumKeepWhich_Jval_To_Enum :
		return _Dct_EnumKeepWhich_Jval_To_Enum[ p_txt ]
	else :
		return None

# --------------------------------------------------
# additional custom functions
# these are a bit of a hack, being run-time derivations when they should probably be just pulls from a stored constant dictionary
# but that's something that can be done later sometime if there's a need

def EnumKeepWhich_Operator_is_LT( e ) :
	return e in [ EnumKeepWhich.KeepOld, EnumKeepWhich.KeepAlphaFirst, EnumKeepWhich.KeepNumericFirst, \
		EnumKeepWhich.KeepPathLongest, EnumKeepWhich.KeepPathDeepest]

@unique
class EnumKeepWhichMethod(Enum):
	ByAge = auto() 
	ByAlpha = auto() 
	ByNumeric = auto() 
	ByPathLength = auto() 
	ByPathDepth = auto() 

def EnumKeepWhich_Method( e ) :
	if e in [ EnumKeepWhich.KeepOld, EnumKeepWhich.KeepNew ] :
		return EnumKeepWhichMethod.ByAge 
	elif e in [ EnumKeepWhich.KeepAlphaFirst, EnumKeepWhich.KeepAlphaLast ] :
		return EnumKeepWhichMethod.ByAlpha 
	elif e in [ EnumKeepWhich.KeepNumericFirst, EnumKeepWhich.KeepNumericLast ] :
		return EnumKeepWhichMethod.ByNumeric 
	elif e in [ EnumKeepWhich.KeepPathLongest, EnumKeepWhich.KeepPathShortest ] :
		return EnumKeepWhichMethod.ByPathLength 
	elif e in [ EnumKeepWhich.KeepPathDeepest, EnumKeepWhich.KeepPathShallowest ] :
		return EnumKeepWhichMethod.ByPathDepth
	else :
		return EnumKeepWhichMethod.ByAge 

# ==================================================
# Extended Enumeration EnumDelOrder
# --------------------------------------------------

class EnumDelOrder(Enum):
	DelFromBig = auto() # "FromBigger"
	DelSmaller = auto() # "FromSmaller"
	DelCommon = auto() # "FromCommon"
	DelRarer = auto() # "FromRarer"
	DelAmost = auto() # "FromAlphabeticFirst"
	DelZmost = auto() # "FromAlphabeticLast"
	DelOldest = auto() # "FromOldest"
	DelNewest = auto() # "FromNewest"

# The internal-use-only structures made by run-once setups - these get created when the module is substantiated

# An internal dictionary of attributes - thereby at run-time is just a quick lookup 
# create the dictionary empty
_Dct_EnumDelOrder_Attrbt = {}
# populate the dictionary
_Dct_EnumDelOrder_Attrbt[ EnumDelOrder.DelFromBig ] = \
	Make_XenumAttrbt( "DelFromBig", "Largest first", "Largest first", "A" )
_Dct_EnumDelOrder_Attrbt[ EnumDelOrder.DelSmaller ] = \
	Make_XenumAttrbt( "DelSmaller", "Smallest first", "Smallest first", "B" )
_Dct_EnumDelOrder_Attrbt[ EnumDelOrder.DelCommon ] = \
	Make_XenumAttrbt( "DelCommon", "Most replicated first", "Most replicated first", "C" )
_Dct_EnumDelOrder_Attrbt[ EnumDelOrder.DelRarer ] = \
	Make_XenumAttrbt( "DelRarer", "Least replicated first", "Least replicated first", "D" )
_Dct_EnumDelOrder_Attrbt[ EnumDelOrder.DelAmost ] = \
	Make_XenumAttrbt( "DelAmost", "from Alphabetic first", "from Alphabetic first", "E" )
_Dct_EnumDelOrder_Attrbt[ EnumDelOrder.DelZmost ] = \
	Make_XenumAttrbt( "DelZmost", "from Alphabetic last", "from Alphabetic last", "F" )
_Dct_EnumDelOrder_Attrbt[ EnumDelOrder.DelOldest ] = \
	Make_XenumAttrbt( "DelOldest", "from Oldest first", "from Oldest first", "G" )
_Dct_EnumDelOrder_Attrbt[ EnumDelOrder.DelNewest ] = \
	Make_XenumAttrbt( "DelNewest", "from Newest first", "from Newest first", "H" )

# A reverse lookup of the Jval text values - and in doing so confirm all are there and are unique
_Dct_EnumDelOrder_Jval_To_Enum = {}
_Dct_EnumDelOrder_Jval_To_Enum = {}
for i_EnumDelOrder in EnumDelOrder :
	# test for clash in reverse dictionary
	if _Dct_EnumDelOrder_Attrbt[ i_EnumDelOrder ].XenumJval in _Dct_EnumDelOrder_Jval_To_Enum :
		# trigger an error
		x = 1 / 0
	else :
		# add to reverse dictionary
		_Dct_EnumDelOrder_Jval_To_Enum[ _Dct_EnumDelOrder_Attrbt[ i_EnumDelOrder ].XenumJval ] = i_EnumDelOrder

# An internal list of pair tuples for use in selectors - first item is the enumeration, second is the string to show 
# example: this can be passed to a modifiec ComboBox widget, usch as Foldatry's PairTupleCombobox
# create the list empty 
_Lst_EnumDelOrder_ComboBoxPairTuples = []
# populate the list
for i_EnumDelOrder in EnumDelOrder :
	# make a tuple of the enum and the brief - as that's what we'll want for Combobox gadgets
	i_tpl = ( i_EnumDelOrder, _Dct_EnumDelOrder_Attrbt[ i_EnumDelOrder ].XenumBrief )
	# add the tuple to the dictionary
	_Lst_EnumDelOrder_ComboBoxPairTuples.append( i_tpl )

# Function calls - so that other uses don't have to know about the internals

# Define which is the default value in the enumeration
def EnumDelOrder_Default() :
	return EnumDelOrder.DelFromBig

# - Functions for fetching each of the parts

def EnumDelOrder_Attrbt_Jval( p_EnumDelOrder ):
	if isinstance( p_EnumDelOrder, EnumDelOrder) :
		return _Dct_EnumDelOrder_Attrbt[ p_EnumDelOrder ].XenumJval
	else :
		return None

def EnumDelOrder_Attrbt_Brief( p_EnumDelOrder ):
	if isinstance( p_EnumDelOrder, EnumDelOrder) :
		return _Dct_EnumDelOrder_Attrbt[ p_EnumDelOrder ].XenumBrief
	else :
		return None

def EnumDelOrder_Attrbt_Show( p_EnumDelOrder ):
	if isinstance( p_EnumDelOrder, EnumDelOrder) :
		return _Dct_EnumDelOrder_Attrbt[ p_EnumDelOrder ].XenumShow
	else :
		return None

def EnumDelOrder_Attrbt_Sort( p_EnumDelOrder ):
	if isinstance( p_EnumDelOrder, EnumDelOrder) and p_EnumDelOrder in _Dct_EnumDelOrder_Attrbt :
		return _Dct_EnumDelOrder_Attrbt[ p_EnumDelOrder ].XenumSort
	else :
		return None

# - second, for fetching the list of pair tuples, this is what will be passed to the Combobox gadget

def EnumDelOrder_LstPairTpls( ):
	return _Lst_EnumDelOrder_ComboBoxPairTuples

# third, provide a translation from the JSON value

def EnumDelOrder_For_Jval_Get_Enum( p_txt ) :
	if isinstance( p_txt, str) and p_txt in _Dct_EnumDelOrder_Jval_To_Enum :
		return _Dct_EnumDelOrder_Jval_To_Enum[ p_txt ]
	else :
		return None

# --------------------------------------------------
# additional custom functions

def EnumDelOrder_IsViableForDefModeFlags( p_EnumDelOrder, p_Hash, p_Size, p_Name, p_When ):
	if p_EnumDelOrder in [ EnumDelOrder.DelFromBig, EnumDelOrder.DelSmaller ] :
		return ( p_Hash or p_Size )
	elif p_EnumDelOrder in [ EnumDelOrder.DelCommon, EnumDelOrder.DelRarer ] :
		return True
	elif p_EnumDelOrder in [ EnumDelOrder.DelAmost, EnumDelOrder.DelZmost ] :
		return ( p_Name )
	elif p_EnumDelOrder in [ EnumDelOrder.DelOldest, EnumDelOrder.DelNewest ] :
		return ( p_When )
	else :
		return True


# ==================================================
# Extended Enumeration EnumDeleteMode
# --------------------------------------------------

class EnumDeleteMode(Enum):
	DryRun = auto() # "DelMod_Dry"
	#Trash = auto() # "DelMod_Trash"
	DeleteNow = auto() # "DelMod_Delete"
	#SecureWipe = auto() # "DelMod_Secure"
	Bash = auto() # "DelMod_Bash"
	#DosBat = auto()
	#WinCmd = auto()
	#Powershell = auto()

# The internal-use-only structures made by run-once setups - these get created when the module is substantiated

# An internal dictionary of attributes - thereby at run-time is just a quick lookup 
# create the dictionary empty
_Dct_EnumDeleteMode_Attrbt = {}
# populate the dictionary
_Dct_EnumDeleteMode_Attrbt[ EnumDeleteMode.DryRun ] = \
	Make_XenumAttrbt( "DryRun", "Dry Run", "Process but no deletion", "A" )
#_Dct_EnumDeleteMode_Attrbt[ EnumDeleteMode.Trash ] = \
#	Make_XenumAttrbt( "Trash", "Trash / Recycle", "Reversible delete", "B" )
_Dct_EnumDeleteMode_Attrbt[ EnumDeleteMode.DeleteNow ] = \
	Make_XenumAttrbt( "DeleteNow", "Immediate Deletion", "Can NOT be undone!", "C" )
#_Dct_EnumDeleteMode_Attrbt[ EnumDeleteMode.SecureWipe ] = \
#	Make_XenumAttrbt( "SecureWipe", "Secure wipe", "Delete all trace", "D" )
_Dct_EnumDeleteMode_Attrbt[ EnumDeleteMode.Bash ] = \
	Make_XenumAttrbt( "Bash", "Bash script", "Linux default shell script", "E" )
#_Dct_EnumDeleteMode_Attrbt[ EnumDeleteMode.DosBat ] = \
#	Make_XenumAttrbt( "DosBat", "MS-DOS batch", "MS-DOS .BAT file", "F" )
#_Dct_EnumDeleteMode_Attrbt[ EnumDeleteMode.WinBat ] = \
#	Make_XenumAttrbt( "WinCmd", "Windows command", "Windows .CMD file", "G" )
#_Dct_EnumDeleteMode_Attrbt[ EnumDeleteMode.Powershell ] = \
#	Make_XenumAttrbt( "Powershell", "Powershell script", "Windows Powershell script", "H" )

# A reverse lookup of the Jval text values - and in doing so confirm all are there and are unique
_Dct_EnumDeleteMode_Jval_To_Enum = {}
_Dct_EnumDeleteMode_Jval_To_Enum = {}
for i_EnumDeleteMode in EnumDeleteMode :
	# test for clash in reverse dictionary
	if _Dct_EnumDeleteMode_Attrbt[ i_EnumDeleteMode ].XenumJval in _Dct_EnumDeleteMode_Jval_To_Enum :
		# trigger an error
		x = 1 / 0
	else :
		# add to reverse dictionary
		_Dct_EnumDeleteMode_Jval_To_Enum[ _Dct_EnumDeleteMode_Attrbt[ i_EnumDeleteMode ].XenumJval ] = i_EnumDeleteMode

# An internal list of pair tuples for use in selectors - first item is the enumeration, second is the string to show 
# example: this can be passed to a modifiec ComboBox widget, usch as Foldatry's PairTupleCombobox
# create the list empty 
_Lst_EnumDeleteMode_ComboBoxPairTuples = []
# populate the list
for i_EnumDeleteMode in EnumDeleteMode :
	# make a tuple of the enum and the brief - as that's what we'll want for Combobox gadgets
	i_tpl = ( i_EnumDeleteMode, _Dct_EnumDeleteMode_Attrbt[ i_EnumDeleteMode ].XenumBrief )
	# add the tuple to the dictionary
	_Lst_EnumDeleteMode_ComboBoxPairTuples.append( i_tpl )

# Function calls - so that other uses don't have to know about the internals

# Define which is the default value in the enumeration
def EnumDeleteMode_Default() :
	return EnumDeleteMode.Bash

# - Functions for fetching each of the parts

def EnumDeleteMode_Attrbt_Jval( p_EnumDeleteMode ):
	if isinstance( p_EnumDeleteMode, EnumDeleteMode) :
		return _Dct_EnumDeleteMode_Attrbt[ p_EnumDeleteMode ].XenumJval
	else :
		return None

def EnumDeleteMode_Attrbt_Brief( p_EnumDeleteMode ):
	if isinstance( p_EnumDeleteMode, EnumDeleteMode) :
		return _Dct_EnumDeleteMode_Attrbt[ p_EnumDeleteMode ].XenumBrief
	else :
		return None

def EnumDeleteMode_Attrbt_Show( p_EnumDeleteMode ):
	if isinstance( p_EnumDeleteMode, EnumDeleteMode) :
		return _Dct_EnumDeleteMode_Attrbt[ p_EnumDeleteMode ].XenumShow
	else :
		return None

def EnumDeleteMode_Attrbt_Sort( p_EnumDeleteMode ):
	if isinstance( p_EnumDeleteMode, EnumDeleteMode) and p_EnumDeleteMode in _Dct_EnumDeleteMode_Attrbt :
		return _Dct_EnumDeleteMode_Attrbt[ p_EnumDeleteMode ].XenumSort
	else :
		return None

# - second, for fetching the list of pair tuples, this is what will be passed to the Combobox gadget

def EnumDeleteMode_LstPairTpls( ):
	return _Lst_EnumDeleteMode_ComboBoxPairTuples

# third, provide a translation from the JSON value

def EnumDeleteMode_For_Jval_Get_Enum( p_txt ) :
	if isinstance( p_txt, str) and p_txt in _Dct_EnumDeleteMode_Jval_To_Enum :
		return _Dct_EnumDeleteMode_Jval_To_Enum[ p_txt ]
	else :
		return None

# --------------------------------------------------
# additional custom functions

def EnumDeleteMode_IsImmediate( p_EnumDeleteMode ):
	return p_EnumDeleteMode == EnumDeleteMode.DeleteNow

# ==================================================
# Extended Enumeration EnumCharEnc
# --------------------------------------------------

class EnumCharEnc(Enum):
	ChEnc_None = auto()
	ChEnc_UTF8 = auto() # modern standard Unicode
	ChEnc_cp1252 = auto() # aka ANSI
	ChEnc_cp437 = auto() # the old DOS line-box symbols
	ChEnd_ASCII = auto() # 7 bit ASCII

# The internal-use-only structures made by run-once setups - these get created when the module is substantiated

# An internal dictionary of attributes - thereby at run-time is just a quick lookup 
# create the dictionary empty
_Dct_EnumCharEnc_Attrbt = {}
# populate the dictionary
_Dct_EnumCharEnc_Attrbt[ EnumCharEnc.ChEnc_None ] = \
	Make_XenumAttrbt( "ChEnc_None", "Unspecified", "specifiying no specific encoding", "A" )
_Dct_EnumCharEnc_Attrbt[ EnumCharEnc.ChEnc_UTF8 ] = \
	Make_XenumAttrbt( "ChEnc_UTF8", "UTF-8 (modern)", "UTF-8 (modern)", "B" )
_Dct_EnumCharEnc_Attrbt[ EnumCharEnc.ChEnc_cp1252 ] = \
	Make_XenumAttrbt( "ChEnc_cp1252", "CodePage 1252 aka ANSI", "CodePage 1252 aka ANSI", "C" )
_Dct_EnumCharEnc_Attrbt[ EnumCharEnc.ChEnc_cp437 ] = \
	Make_XenumAttrbt( "ChEnc_cp437", "CodePage 437 (old DOS line-box symbols)", "CodePage 437 (old DOS line-box symbols)", "D" )
_Dct_EnumCharEnc_Attrbt[ EnumCharEnc.ChEnd_ASCII ] = \
	Make_XenumAttrbt( "ChEnd_ASCII", "ASCII (7 bit)", "ASCII (7 bit)", "E" )

# A reverse lookup of the Jval text values - and in doing so confirm all are there and are unique
_Dct_EnumCharEnc_Jval_To_Enum = {}
_Dct_EnumCharEnc_Jval_To_Enum = {}
for i_EnumCharEnc in EnumCharEnc :
	# test for clash in reverse dictionary
	if _Dct_EnumCharEnc_Attrbt[ i_EnumCharEnc ].XenumJval in _Dct_EnumCharEnc_Jval_To_Enum :
		# trigger an error
		x = 1 / 0
	else :
		# add to reverse dictionary
		_Dct_EnumCharEnc_Jval_To_Enum[ _Dct_EnumCharEnc_Attrbt[ i_EnumCharEnc ].XenumJval ] = i_EnumCharEnc

# An internal list of pair tuples for use in selectors - first item is the enumeration, second is the string to show 
# example: this can be passed to a modifiec ComboBox widget, usch as Foldatry's PairTupleCombobox
# create the list empty 
_Lst_EnumCharEnc_ComboBoxPairTuples = []
# populate the list
for i_EnumCharEnc in EnumCharEnc :
	# make a tuple of the enum and the brief - as that's what we'll want for Combobox gadgets
	i_tpl = ( i_EnumCharEnc, _Dct_EnumCharEnc_Attrbt[ i_EnumCharEnc ].XenumBrief )
	# add the tuple to the dictionary
	_Lst_EnumCharEnc_ComboBoxPairTuples.append( i_tpl )

# Function calls - so that other uses don't have to know about the internals

# Define which is the default value in the enumeration
def EnumCharEnc_Default() :
	return EnumCharEnc.ChEnc_UTF8

# - Functions for fetching each of the parts

def EnumCharEnc_Attrbt_Jval( p_EnumCharEnc ):
	if isinstance( p_EnumCharEnc, EnumCharEnc) :
		return _Dct_EnumCharEnc_Attrbt[ p_EnumCharEnc ].XenumJval
	else :
		return None

def EnumCharEnc_Attrbt_Brief( p_EnumCharEnc ):
	if isinstance( p_EnumCharEnc, EnumCharEnc) :
		return _Dct_EnumCharEnc_Attrbt[ p_EnumCharEnc ].XenumBrief
	else :
		return None

def EnumCharEnc_Attrbt_Show( p_EnumCharEnc ):
	if isinstance( p_EnumCharEnc, EnumCharEnc) :
		return _Dct_EnumCharEnc_Attrbt[ p_EnumCharEnc ].XenumShow
	else :
		return None

def EnumCharEnc_Attrbt_Sort( p_EnumCharEnc ):
	if isinstance( p_EnumCharEnc, EnumCharEnc) and p_EnumCharEnc in _Dct_EnumCharEnc_Attrbt :
		return _Dct_EnumCharEnc_Attrbt[ p_EnumCharEnc ].XenumSort
	else :
		return None

# - second, for fetching the list of pair tuples, this is what will be passed to the Combobox gadget

def EnumCharEnc_LstPairTpls( ):
	return _Lst_EnumCharEnc_ComboBoxPairTuples

# third, provide a translation from the JSON value

def EnumCharEnc_For_Jval_Get_Enum( p_txt ) :
	if isinstance( p_txt, str) and p_txt in _Dct_EnumCharEnc_Jval_To_Enum :
		return _Dct_EnumCharEnc_Jval_To_Enum[ p_txt ]
	else :
		return None

# --------------------------------------------------
# additional custom functions

def EnumCharEnc_PythonRef( e ) :
	if e == EnumCharEnc.ChEnc_UTF8 :
		return "utf_8"
	elif e == EnumCharEnc.ChEnc_cp1252 :
		return "cp1252"
	else :
		return "utf_8"




# ==================================================
# Extended Enumeration EnumRenametryMode
# --------------------------------------------------
# created to support intended features of Renametry
# tihs consists of two enumerations:
# - EnumRenametryDirection = whether to endocde or decode
# - EnumRenametryMode = various methods available - mainly to allow for later variations from the initial scheme

# --------------------------------------------------
# - EnumRenametryDirection 

class EnumRenametryDirection(Enum):
	DoEncode = auto()
	DoDecode = auto()

# Define which is the default value in the enumeration
def EnumRenametryDirection_Default() :
	return EnumRenametryDirection.DoEncode

# The internal-use-only structures made by run-once setups - these get created when the module is substantiated

# An internal dictionary of attributes - thereby at run-time is just a quick lookup 
# create the dictionary empty
_Dct_EnumRenametryDirection_Attrbt = {}
# populate the dictionary
_Dct_EnumRenametryDirection_Attrbt[ EnumRenametryDirection.DoEncode ] = \
	Make_XenumAttrbt( "DoEncode", "Encode from original", "e.g. | becomes ", "A" )
_Dct_EnumRenametryDirection_Attrbt[ EnumRenametryDirection.DoDecode ] = \
	Make_XenumAttrbt( "DoDecode", "Decode back to original", "e.g. % becomes |", "B" )

# A reverse lookup of the Jval text values - and in doing so confirm all are there and are unique
_Dct_EnumRenametryDirection_Jval_To_Enum = {}
for i_EnumRenametryDirection in EnumRenametryDirection :
	# test for clash in reverse dictionary
	if _Dct_EnumRenametryDirection_Attrbt[ i_EnumRenametryDirection ].XenumJval in _Dct_EnumRenametryDirection_Jval_To_Enum :
		# trigger an error
		x = 1 / 0
	else :
		# add to reverse dictionary
		_Dct_EnumRenametryDirection_Jval_To_Enum[ _Dct_EnumRenametryDirection_Attrbt[ i_EnumRenametryDirection ].XenumJval ] = i_EnumRenametryDirection

# An internal list of pair tuples for use in selectors - first item is the enumeration, second is the string to show 
# example: this can be passed to a modifiec ComboBox widget, usch as Foldatry's PairTupleCombobox
# create the list empty 
_Lst_EnumRenametryDirection_ComboBoxPairTuples = []
# populate the list
for i_EnumRenametryDirection in EnumRenametryDirection :
	# make a tuple of the enum and the brief - as that's what we'll want for Combobox gadgets
	i_tpl = ( i_EnumRenametryDirection, _Dct_EnumRenametryDirection_Attrbt[ i_EnumRenametryDirection ].XenumBrief )
	# add the tuple to the dictionary
	_Lst_EnumRenametryDirection_ComboBoxPairTuples.append( i_tpl )

# Function calls - so that other uses don't have to know about the internals

# - second, for fetching the list of pair tuples, this is what will be passed to the Combobox gadget

def EnumRenametryDirection_LstPairTpls( ):
	return _Lst_EnumRenametryDirection_ComboBoxPairTuples

# third, provide a translation from the JSON value

def EnumRenametryDirection_For_Jval_Get_Enum( p_txt ) :
	if isinstance( p_txt, str) and p_txt in _Dct_EnumRenametryDirection_Jval_To_Enum :
		return _Dct_EnumRenametryDirection_Jval_To_Enum[ p_txt ]
	else :
		return None

# --------------------------------------------------
# - EnumRenametryMode
# Note: initially there will be just one mode, but it's easy to imagine having others

class EnumRenametryMode(Enum):
	#DosWinLossy = auto()
	DosWinKeepy = auto()
	#DateTranspose = auto()
	#DateTransDash = auto()

# The internal-use-only structures made by run-once setups - these get created when the module is substantiated

# An internal dictionary of attributes - thereby at run-time is just a quick lookup 
# create the dictionary empty
_Dct_EnumRenametryMode_Attrbt = {}
# populate the dictionary
_Dct_EnumRenametryMode_Attrbt[ EnumRenametryMode.DosWinKeepy ] = \
	Make_XenumAttrbt( "DosWinKeepy", "DOS/Windows recoverable", "for DOS/Windows - recoverable", "B" )
#_Dct_EnumRenametryMode_Attrbt[ EnumRenametryMode.DosWinLossy ] = \
#	Make_XenumAttrbt( "DosWinLossy", "DOS/Windows", "for DOS/Windows - destructive", "A" )
#_Dct_EnumRenametryMode_Attrbt[ EnumRenametryMode.DateTranspose ] = \
#	Make_XenumAttrbt( "DateTranspose", "Date Transpose", "date transformation - from MMDDYYYY to YYYYMMDD", "C" )
#_Dct_EnumRenametryMode_Attrbt[ EnumRenametryMode.DateTransDash ] = \
#	Make_XenumAttrbt( "DateTransDash", "Date transpose with dashes", "date transformation - from MMDDYYYY to YYYY-MM-DD", "D" )

# A reverse lookup of the Jval text values - and in doing so confirm all are there and are unique
_Dct_EnumRenametryMode_Jval_To_Enum = {}
for i_EnumRenametryMode in EnumRenametryMode :
	# test for clash in reverse dictionary
	if _Dct_EnumRenametryMode_Attrbt[ i_EnumRenametryMode ].XenumJval in _Dct_EnumRenametryMode_Jval_To_Enum :
		# trigger an error
		x = 1 / 0
	else :
		# add to reverse dictionary
		_Dct_EnumRenametryMode_Jval_To_Enum[ _Dct_EnumRenametryMode_Attrbt[ i_EnumRenametryMode ].XenumJval ] = i_EnumRenametryMode

# An internal list of pair tuples for use in selectors - first item is the enumeration, second is the string to show 
# example: this can be passed to a modifiec ComboBox widget, usch as Foldatry's PairTupleCombobox
# create the list empty 
_Lst_EnumRenametryMode_ComboBoxPairTuples = []
# populate the list
for i_EnumRenametryMode in EnumRenametryMode :
	# make a tuple of the enum and the brief - as that's what we'll want for Combobox gadgets
	i_tpl = ( i_EnumRenametryMode, _Dct_EnumRenametryMode_Attrbt[ i_EnumRenametryMode ].XenumBrief )
	# add the tuple to the dictionary
	_Lst_EnumRenametryMode_ComboBoxPairTuples.append( i_tpl )

# Function calls - so that other uses don't have to know about the internals

# Define which is the default value in the enumeration
def EnumRenametryMode_Default() :
	return EnumRenametryMode.DosWinKeepy

# - Functions for fetching each of the parts

def EnumRenametryMode_Attrbt_Jval( p_EnumRenametryMode ):
	if isinstance( p_EnumRenametryMode, EnumRenametryMode) :
		return _Dct_EnumRenametryMode_Attrbt[ p_EnumRenametryMode ].XenumJval
	else :
		return None

def EnumRenametryMode_Attrbt_Brief( p_EnumRenametryMode ):
	if isinstance( p_EnumRenametryMode, EnumRenametryMode) :
		return _Dct_EnumRenametryMode_Attrbt[ p_EnumRenametryMode ].XenumBrief
	else :
		return None

def EnumRenametryMode_Attrbt_Show( p_EnumRenametryMode ):
	if isinstance( p_EnumRenametryMode, EnumRenametryMode) :
		return _Dct_EnumRenametryMode_Attrbt[ p_EnumRenametryMode ].XenumShow
	else :
		return None

def EnumRenametryMode_Attrbt_Sort( p_EnumRenametryMode ):
	if isinstance( p_EnumRenametryMode, EnumRenametryMode) and p_EnumRenametryMode in _Dct_EnumRenametryMode_Attrbt :
		return _Dct_EnumRenametryMode_Attrbt[ p_EnumRenametryMode ].XenumSort
	else :
		return None

# - second, for fetching the list of pair tuples, this is what will be passed to the Combobox gadget

def EnumRenametryMode_LstPairTpls( ):
	return _Lst_EnumRenametryMode_ComboBoxPairTuples

# third, provide a translation from the JSON value

def EnumRenametryMode_For_Jval_Get_Enum( p_txt ) :
	if isinstance( p_txt, str) and p_txt in _Dct_EnumRenametryMode_Jval_To_Enum :
		return _Dct_EnumRenametryMode_Jval_To_Enum[ p_txt ]
	else :
		return None

# --------------------------------------------------
# any additional custom functions

# = currently none

# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# SubSection 3 - class FnameMatchSetting
# --------------------------------------------------

# Note: not renamed, just copied from commontry but then adapted 
# to use the new I enumerations

# Classes for File/Folder Name Match Settings

# First the parts of a Name Match Setting combination - these will be used as dictionary keys
@unique
class FnameMatchElement( Enum):
	EnumCharEnc = "EnumCharEnc"
	DoCaseSensitive = "DoCaseSensitive"
	DoMsBadNames = "DoMsBadNames"
	DoMsBadChars = "DoMsBadChars"
	DoMs8p3 = "DoMs8p3"

def FnameMatch_SetElementsAsDefaults( r_dct ):
	# assumes r_dct is already a dictionary
	r_dct[ FnameMatchElement.EnumCharEnc ] = EnumCharEnc.ChEnc_UTF8
	r_dct[ FnameMatchElement.DoCaseSensitive ] = True
	r_dct[ FnameMatchElement.DoMsBadNames ] = False
	r_dct[ FnameMatchElement.DoMsBadChars ] = False
	r_dct[ FnameMatchElement.DoMs8p3 ] = False

def FnameMatch_SetElements_For_EnumFileSysArch( r_dct, p_EnumFileSysArch ):
	if p_EnumFileSysArch == EnumFileSysArch.FSA_Linux :
		r_dct[ FnameMatchElement.EnumCharEnc ] = EnumCharEnc.ChEnc_None
		r_dct[ FnameMatchElement.DoCaseSensitive ] = True
		r_dct[ FnameMatchElement.DoMsBadNames ] = False
		r_dct[ FnameMatchElement.DoMsBadChars ] = False
		r_dct[ FnameMatchElement.DoMs8p3 ] = False
	elif p_EnumFileSysArch == EnumFileSysArch.FSA_Windows :
		r_dct[ FnameMatchElement.EnumCharEnc ] = EnumCharEnc.ChEnc_UTF8
		r_dct[ FnameMatchElement.DoCaseSensitive ] = False
		r_dct[ FnameMatchElement.DoMsBadNames ] = True
		r_dct[ FnameMatchElement.DoMsBadChars ] = True
		r_dct[ FnameMatchElement.DoMs8p3 ] = False
	elif p_EnumFileSysArch == EnumFileSysArch.FSA_DosWin :
		r_dct[ FnameMatchElement.EnumCharEnc ] = EnumCharEnc.ChEnc_cp1252
		r_dct[ FnameMatchElement.DoCaseSensitive ] = False
		r_dct[ FnameMatchElement.DoMsBadNames ] = True
		r_dct[ FnameMatchElement.DoMsBadChars ] = True
		r_dct[ FnameMatchElement.DoMs8p3 ] = True
	elif p_EnumFileSysArch == EnumFileSysArch.FSA_PCDOS :
		r_dct[ FnameMatchElement.EnumCharEnc ] = EnumCharEnc.ChEnc_cp437
		r_dct[ FnameMatchElement.DoCaseSensitive ] = False
		r_dct[ FnameMatchElement.DoMsBadNames ] = True
		r_dct[ FnameMatchElement.DoMsBadChars ] = True
		r_dct[ FnameMatchElement.DoMs8p3 ] = False
	else :
		FnameMatch_SetElementsAsDefaults( r_dct )

# --------------------------------------------------
# Class as data type for Name Match settings
# --------------------------------------------------

class FnameMatchSetting():
	def __init__( self ):
		# make sure these things exist
		self.dct = {}
		FnameMatch_SetElementsAsDefaults( self.dct )
	# gets
	def get_UseEnumCharEnc( self ):
		return self.dct[ FnameMatchElement.EnumCharEnc ]
	def get_DoCharEnc( self ):
		return not ( self.dct[ FnameMatchElement.EnumCharEnc ] == EnumCharEnc.ChEnc_None )
	def get_DoCaseSensitive( self):
		return self.dct[ FnameMatchElement.DoCaseSensitive ]
	def get_DoMsBadNames( self ):
		return self.dct[ FnameMatchElement.DoMsBadNames ]
	def get_DoMsBadChars( self ):
		return self.dct[ FnameMatchElement.DoMsBadChars ]
	def get_DoMs8p3( self ):
		return self.dct[ FnameMatchElement.DoMs8p3 ]
	# sets for each setting
	def set_UseEnumCharEnc( self, p_EnumCharEnc):
		if isinstance( p_EnumCharEnc, EnumCharEnc ):
			self.dct[ FnameMatchElement.EnumCharEnc ] = p_EnumCharEnc
	def set_DoCaseSensitive( self, p_Bool):
		self.dct[ FnameMatchElement.DoCaseSensitive ] = ( p_Bool == True )
	def set_DoMsBadNames( self, p_Bool):
		self.dct[ FnameMatchElement.DoMsBadNames ] = ( p_Bool == True )
	def set_DoMsBadChars( self, p_Bool ):
		self.dct[ FnameMatchElement.DoMsBadChars ] = ( p_Bool == True )
	def set_DoMs8p3( self, p_Bool):
		self.dct[ FnameMatchElement.DoMs8p3 ] = ( p_Bool == True )
	# set by EnumFileSysArch
	def set_For_EnumFileSysArch( self, p_EnumFileSysArch):
		FnameMatch_SetElements_For_EnumFileSysArch( self.dct, p_EnumFileSysArch )

# example usage
# v = FnameMatchSetting()
# v.set_For_EnumFileSysArch( EnumFileSysArch.FSA_Windows )

def Make_StrDescription_For_FnameMatchSetting( p_FnameMatchSetting ):
	r_str = "Encoding: " + EnumCharEnc_Attrbt_Show( p_FnameMatchSetting.get_UseEnumCharEnc() )
	# print( "r_str: " + r_str )
	if p_FnameMatchSetting.get_DoCaseSensitive() :
		r_str += "; " + "Case Sensitive"
	else :
		r_str += "; " + "NOT Case Sensitive"
	if p_FnameMatchSetting.get_DoMsBadChars() :
		r_str += "; " + "Handle Microsoft forbidden characters"
	if p_FnameMatchSetting.get_DoMsBadNames() :
		r_str += "; " + "Handle Microsoft forbidden names." + " "
	if p_FnameMatchSetting.get_DoMs8p3() :
		r_str += "; " + "Handle DOS 8.3 names." + " "
	# print( "r_str: " + r_str )
	return r_str

def For_FnameMatchSetting_Get_DoWeDo2ndRound( p_FnameMatchSetting ):
	r_bool = not p_FnameMatchSetting.get_DoCaseSensitive()
	r_bool = r_bool or p_FnameMatchSetting.get_DoMsBadNames()
	r_bool = r_bool or p_FnameMatchSetting.get_DoMsBadChars()
	r_bool = r_bool or p_FnameMatchSetting.get_DoMs8p3()
	return r_bool

def For_FnameMatchSettings_Get_DoWeDo2ndRound( p_FnameMatchSetting_A, p_FnameMatchSetting_B ):
	#zz_procname = "For_FnameMatchSettings_Get_DoWeDo2ndRound"
	r_bool = For_FnameMatchSetting_Get_DoWeDo2ndRound( p_FnameMatchSetting_A ) 
	if not r_bool :
		r_bool = For_FnameMatchSetting_Get_DoWeDo2ndRound( p_FnameMatchSetting_B )
	if not r_bool :
		# now consider if the encodings are different
		r_bool = p_FnameMatchSetting_A.get_UseEnumCharEnc() == p_FnameMatchSetting_B.get_UseEnumCharEnc()
	return r_bool

def For_FnameMatchSetting_Get_CharEncOrDefault( p_FnameMatchSetting ):
	r_UseEnumCharEnc = EnumCharEnc_Default()
	if p_FnameMatchSetting.get_DoCharEnc() :
		r_UseEnumCharEnc = p_FnameMatchSetting.get_UseEnumCharEnc()
	return r_UseEnumCharEnc

# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# SubSection 4 - the actions
# --------------------------------------------------

@unique
class EnumAction(Enum):
	ActionPrunatry = auto() 
	ActionDefoliatry = auto() 
	ActionTrimatry = auto() 
	# Congruentry actions
	ActionCongruentryPaths = auto() 
	ActionCongruentryFiles = auto() 
	# Replatry actions
	ActionReplatryDistinctry = auto()
	ActionReplatryClonatry = auto()
	ActionReplatryDubatry = auto()
	ActionReplatryMergetry = auto()
	ActionReplatrySynopsitry = auto()
	ActionReplatryMigratry = auto()
	ActionReplatrySkeletry = auto()
	ActionReplatryMovetry = auto()
	ActionReplatryShiftatry = auto()
	ActionReplatryShiftafile = auto()
	# Summatry actions
	ActionSummatry = auto() 
	ActionCheckNames = auto() 
	ActionCountExtensions = auto() 

nmdtpl_XenumAction = namedtuple('nmdtpl_XenumAction', 'XenumArgShort XenumArgLong')

# where:
# - Jval = abbreviation-key, for use in encoded situations, must be unique so it can be used as a key or unique value e.g. in JSON
# - Brief - for use in ComboxBox selections
# - Show - for use in displays that need to be clear about meaning and not concerned about brevity
# - Sort - for use in ordering displays

# have a simple bullet proof type-safe assignment function
# this ensures that we get back a tuple of strings
def Make_XenumAction( p_XenumArgShort, p_XenumArgLong ):
	# p_XenumArgShort = a string that will be used for the JSON value storage - this needs to be unique per Enumeration
	# p_XenumArgLong = a brief display string
	# this function is provided to ensure that all the passed parameter attributes become strings
	i_XenumArgShort = str( p_XenumArgShort )
	i_XenumArgLong = str( p_XenumArgLong )
	return nmdtpl_XenumAction( i_XenumArgShort, i_XenumArgLong )

_Dct_EnumAction_Xenum = {}
# populate the dictionary of custom extended enumarations
# note that it will be assumed to later prepend the short with a dash (-) and the long with double-dash (--)
# so those aren't specified here
_Dct_EnumAction_Xenum[ EnumAction.ActionPrunatry ] = \
	Make_XenumAction( "PR", "PRUNATRY" )
_Dct_EnumAction_Xenum[ EnumAction.ActionDefoliatry ] = \
	Make_XenumAction( "DF", "Defoliatry" )
_Dct_EnumAction_Xenum[ EnumAction.ActionTrimatry ] = \
	Make_XenumAction( "TR", "Trimatry" )
_Dct_EnumAction_Xenum[ EnumAction.ActionCongruentryPaths ] = \
	Make_XenumAction( "CP", "CongruentryPaths" )
_Dct_EnumAction_Xenum[ EnumAction.ActionCongruentryFiles ] = \
	Make_XenumAction( "CF", "CongruentryFiles" )
_Dct_EnumAction_Xenum[ EnumAction.ActionReplatryDistinctry ] = \
	Make_XenumAction( "DS", "Distinctry" )
_Dct_EnumAction_Xenum[ EnumAction.ActionReplatryClonatry ] = \
	Make_XenumAction( "CL", "Clonatry" )
_Dct_EnumAction_Xenum[ EnumAction.ActionReplatryDubatry ] = \
	Make_XenumAction( "DU", "Dubatry" )
_Dct_EnumAction_Xenum[ EnumAction.ActionReplatryMergetry ] = \
	Make_XenumAction( "MG", "Mergetry" )
_Dct_EnumAction_Xenum[ EnumAction.ActionReplatrySynopsitry ] = \
	Make_XenumAction( "SY", "Synopsitry" )
_Dct_EnumAction_Xenum[ EnumAction.ActionReplatryMigratry ] = \
	Make_XenumAction( "MI", "Migratry" )
_Dct_EnumAction_Xenum[ EnumAction.ActionReplatrySkeletry ] = \
	Make_XenumAction( "SK", "Skeletry" )
_Dct_EnumAction_Xenum[ EnumAction.ActionReplatryMovetry ] = \
	Make_XenumAction( "MV", "Movetry" )
_Dct_EnumAction_Xenum[ EnumAction.ActionReplatryShiftatry ] = \
	Make_XenumAction( "SD", "Shiftatry" )
_Dct_EnumAction_Xenum[ EnumAction.ActionReplatryShiftafile ] = \
	Make_XenumAction( "SF", "Shiftafile" )
_Dct_EnumAction_Xenum[ EnumAction.ActionSummatry ] = \
	Make_XenumAction( "SU", "Summatry" )
_Dct_EnumAction_Xenum[ EnumAction.ActionCheckNames ] = \
	Make_XenumAction( "CN", "CheckNames" )
_Dct_EnumAction_Xenum[ EnumAction.ActionCountExtensions ] = \
	Make_XenumAction( "CE", "CountExtensions" )

# Build a reverse lookup of the XenumArgShort text values - and in doing so confirm all are there and they are unique
_Dct_EnumAction_Xenum_ArgShort_To_Enum = {}
for i_EnumAction in EnumAction :
	# test for clash in reverse dictionary
	if _Dct_EnumAction_Xenum[ i_EnumAction ].XenumArgShort in _Dct_EnumAction_Xenum_ArgShort_To_Enum :
		# trigger an error
		x = 1 / 0
	else :
		# add to reverse dictionary
		_Dct_EnumAction_Xenum_ArgShort_To_Enum[ _Dct_EnumAction_Xenum[ i_EnumAction ].XenumArgShort ] = i_EnumAction

# make a function for non-local use to return an enumeration for a given XenumArgShort text value
def EnumAction_For_ArgShort_Get_Enum( p_txt ) :
	if isinstance( p_txt, str) and p_txt in _Dct_EnumAction_Xenum_ArgShort_To_Enum :
		return _Dct_EnumAction_Xenum_ArgShort_To_Enum[ p_txt ]
	else :
		return None

# Build a reverse lookup of the XenumArgLong text values - and in doing so confirm all are there and they are unique
_Dct_EnumAction_Xenum_ArgLong_To_Enum = {}
for i_EnumAction in EnumAction :
	# test for clash in reverse dictionary
	if _Dct_EnumAction_Xenum[ i_EnumAction ].XenumArgLong in _Dct_EnumAction_Xenum_ArgLong_To_Enum :
		# trigger an error
		x = 1 / 0
	else :
		# add to reverse dictionary
		_Dct_EnumAction_Xenum_ArgLong_To_Enum[ _Dct_EnumAction_Xenum[ i_EnumAction ].XenumArgLong ] = i_EnumAction

# make a function for non-local use to return an enumeration for a given XenumArgLong text value
def EnumAction_For_ArgLong_Get_Enum( p_txt ) :
	if isinstance( p_txt, str) and p_txt in _Dct_EnumAction_Xenum_ArgLong_To_Enum :
		return _Dct_EnumAction_Xenum_ArgLong_To_Enum[ p_txt ]
	else :
		return None

# make a function for non-local use to return an enumeration for a given argument text value
def EnumAction_For_Argument_Get_Enum( p_txt ) :
	if p_txt[0:3] == "---" :
		return None
	elif p_txt[0:2] == "--" :
		i_txt = p_txt[2:]
		return EnumAction_For_ArgLong_Get_Enum( i_txt )
	elif p_txt[0:1] == "-" :
		i_txt = p_txt[1:]
		return EnumAction_For_ArgShort_Get_Enum( i_txt )
	else :
		return None


# $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
# SECTION B - new rendition of Settings for Foldatry
# $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

# --------------------------------------------------
# Define Enumeration of all the data types that the Settings will be needing to support
# this will be a combination of simple obvious types and the custom enumerations
# --------------------------------------------------

@unique
class EnumSettingDataType(Enum):
	# the three scalar types
	OneBoolean = auto() # Boolean
	OneString = auto() # String
	ListString = auto() # List of String
	# the custom enumeration types
	OneKeepWhichMatch = auto() # custom enumeration EnumKeepWhich
	OneGroupDeleteOrder = auto() # custom enumeration EnumDelOrder
	OneDeletionMode = auto() # custom enumeration EnumDeleteMode
	OneCharEncoding = auto() # custom enumeration EnumCharEnc
	OneGenCopyScriptAs = auto() # custom enumeration EnumGenCopyScriptAs

# --------------------------------------------------
# Define Enumeration of all the Saveable Settings
# This is a value for each and every setting
# --------------------------------------------------

@unique
class EnumSettingDef(Enum):
	# Prunatry ------------------
	PrntryLstDisposalPaths = auto()
	PrntryLstSurvivalPaths = auto()
	#
	PrntryDoSuperMatch = auto() 
	PrntryDoFolderMatch = auto() 
	PrntryDoFileMatch = auto() 
	PrntryDoMatchStringent = auto() 
	#
	PrntryFileDoMatchByHash = auto() 
	PrntryFileDoMatchBySize = auto() 
	PrntryFileDoMatchByName = auto() 
	PrntryFileDoMatchByWhen = auto() 
	PrntryMtchGrpDelOrder = auto() 
	#
	PrntryDoCongruent = auto() 
	PrntryDoCngrntStringent = auto() 
	PrntryReportStringentDiffs = auto() 
	PrntryDeletionMode = auto()  # was PrntryPikDeletionMode
	# Defoliatry ------------------
	DfltryLstPaths = auto() 
	#
	DfltryDoIgnoreZeroLength = auto() 
	DfltryFileDoMatchByHash = auto() # was DfltryDoUseHashMatch
	DfltryFileDoMatchBySize = auto() 
	DfltryFileDoMatchByName = auto() 
	DfltryFileDoMatchByWhen = auto() 
	DfltryMtchGrpDelOrder = auto() # was DfltryPikGroupDelOrder
	DfltryFileEnumKeepWhich = auto() # was DfltryPikInMatchGroup
	#
	DfltryDoUseDiscardMarkers = auto() 
	# DfltryLstDisposalMatches = auto() # concept withdrawn, deferred to equivalent list in the Settings area
	DfltryDoCongruent = auto() 
	DfltryDoCngrntStringent = auto() 
	DfltryReportStringentDiffs = auto() 
	DfltryDeletionMode = auto() # was DfltryPikDeletionMode
	# Trimatry ------------------
	TrmtryLstPaths = auto()
	TrmtryDoCanDelTopFolders = auto()
	TrmtryDoNestedEmptyFolders = auto()
	TrmtryDoTreatZeroAsNothing = auto()
	TrmtryDoExceptKeepIfEmpty = auto()
	TrmtryDoExceptNoMedia = auto()
	TrmtryDoRenameInsteadOfDelete = auto()
	TrmtryDeletionMode = auto()
	TrmtryDoDelFromDeepest = auto()
	# ------------------
	# Replatry -- 
		# Distinctry ------------------
		# Dstnctry
		# DstnctryStrPathA = auto()
		# DstnctryStrPathB = auto()
		# DstnctryStrPathC = auto()
		# DstnctryRecreateSourceFolder = auto()
		# DstnctryDoKeepTouch = auto()
		# DstnctryDoKeepAttrib = auto()
		# DstnctryDoKeepPerm = auto()
		# DstnctryDoKeepMeta = auto()
	# - Clonatry ------------------
	ClntryStrPathA = auto()
	ClntryStrPathB = auto()
	ClntryRecreateSourceFolder = auto()
	ClntryDoKeepTouch = auto()
	ClntryDoKeepAttrib = auto()
	ClntryDoKeepPerm = auto()
	ClntryDoKeepMeta = auto()
		# Dubatry ------------------
		# DbtryStrPathA = auto()
		# DbtryStrPathB = auto()
		# Mergetry ------------------
		# MrgtryStrPathA = auto()
		# MrgtryStrPathB = auto()
		# Synopsitry ------------------
		# SynpstryStrPathA = auto()
		# SynpstryStrPathB = auto()
		# SynpstryRecreateSourceFolder = auto()
		# SynpstryDoKeepTouch = auto()
		# SynpstryDoKeepAttrib = auto()
		# SynpstryDoKeepPerm = auto()
		# SynpstryDoKeepMeta = auto()
		# Skeletry ------------------
		# SkltryStrPathA = auto()
		# SkltryStrPathB = auto()
		# SkltryDeletionMode = auto()
		# Migratry ------------------
		# MgrtryStrPathA = auto()
		# MgrtryStrPathB = auto()
		# MgrtryRecreateSourceFolder = auto()
		# MgrtryDoKeepTouch = auto()
		# MgrtryDoKeepAttrib = auto()
		# MgrtryDoKeepPerm = auto()
		# MgrtryDoKeepMeta = auto()
		# Movetry ------------------
		# MvtryStrPathA = auto()
		# MvtryStrPathB = auto()
		# Shiftatry ------------------
		# ShfttryStrPathA = auto()
		# ShfttryStrPathB = auto()
		# Shiftafile ------------------
		# ShftflStrFileA = auto()
		# ShftflStrFileB = auto()
	# Congruentry ------------------
	# - Two paths
	# - - A
	CngrntryTwoPathAStrPath = auto() 
	CngrntryTwoPathACustomCharEnc = auto()
	CngrntryTwoPathACustomDoCase = auto()
	CngrntryTwoPathACustomDoMsName = auto()
	CngrntryTwoPathACustomDoMsChar = auto()
	CngrntryTwoPathACustomDoMs8p3 = auto()
	# - - B
	CngrntryTwoPathBStrPath = auto()
	CngrntryTwoPathBCustomCharEnc = auto()
	CngrntryTwoPathBCustomDoCase = auto()
	CngrntryTwoPathBCustomDoMsName = auto()
	CngrntryTwoPathBCustomDoMsChar = auto()
	CngrntryTwoPathBCustomDoMs8p3 = auto()
	# Options
	CngrntryAnalyseStringent = auto()
	CngrntryStopNotCongruent = auto()
	CngrntryStopNotMoiety = auto()
	CngrntryReportDiffs = auto()
	CngrntryDiffSummary = auto()
	CngrntryReportStringentDiffs = auto()
	CngrntryGenCopyScript = auto()
	CngrntryCopyScriptShell = auto()
	# - Two files
	CngrntryTwoFileAStrFileref = auto()
	CngrntryTwoFileBStrFileref = auto()
	# Veritry ------------------ PLANNED
	# VrtryLstPathsA = auto()
	# VrtryLstPathsB = auto()
	# VrtryFileDoMatchByHash = auto() 
	# VrtryFileDoMatchBySize = auto() 
	# VrtryFileDoMatchByName = auto() 
	# VrtryFileDoMatchByWhen = auto() 
	# VrtryDoCngrntStringent = auto() 
	# VrtryReportStringentGoods = auto() 
	# VrtryReportStringentFails = auto() 
	# VrtryMtchGrpOrder = auto()
	# Summatry ------------------
	SmmtryLstPaths = auto()
	SmmtryDoMsName = auto()
	SmmtryDoMsChar = auto()
	# Cartulatry ------------------ PLANNED
	# CrtltryLstPaths = auto()
	# CrtltryLstFileExlusions = auto()
	# CrtltryFileExlusionsApplyFullPath = auto()
	# CrtltryLstFolderExlusions = auto()
	# CrtltryFolderExlusionsApplyFullPath = auto()
	# CrtltryDoIgnoreZeroLength = auto() 
	# CrtltryRunName = auto()  # Quite likely to change !!
	# Other Settings  ------------------
	SttngLstDiscardNameMarkers = auto() # currently only used by Defoliatry but pit in Settings both for GUi simplicity and because these might be used elsewhere
	# Logging ------------------
	LggngStatus = auto()
	LggngResults = auto()
	LggngProcess = auto()
	LggngDetails = auto()
	LggngDebug = auto()

# --------------------------------------------------
# NamedTuple for use with Extended Enumerated attributes
# Somewhat similar to what we did for the custom enuemrations
# here give outselves a way to see and then use some more attributes
# for each enumeration. These will be stored in a dictionary keyed by
# the above EnumSettingDef values so the named tuple does not need to
# store that in itself
# --------------------------------------------------

nmdtpl_SettingAttrbt = namedtuple('nmdtpl_SettingAttrbt', 'SttngType SttngDefault SttngJkey SttngShow')

# Now that we've defined all the settings data types
# Make a bullet-proof function for making the named tuples nmdtpl_SettingAttrbt
# such that the defaults must be either None or match the type of that Setting

def Make_SettingAttrbt( p_SttngType, p_SttngDefault, p_SttngJkey, p_SttngShow ):
	if not isinstance( p_SttngType, EnumSettingDataType) :
		# no point constructing anything
		return None
	else :
		# bulletproofing of the default value
		if p_SttngDefault is None :
			# no need to check anything
			i_SttngDefault = None
		elif p_SttngType == EnumSettingDataType.OneBoolean :
			i_SttngDefault = ( p_SttngDefault == True )
		elif p_SttngType == EnumSettingDataType.OneString :
			i_SttngDefault = str( p_SttngDefault)
		elif p_SttngType == EnumSettingDataType.ListString :
			if isinstance( p_SttngDefault, list) :
				# later add a check that all items in the list are strings or convert them all to strings
				i_SttngDefault = []
				for list_item in p_SttngDefault :
					i_SttngDefault.add( str( list_item) )
			else :
				i_SttngDefault = None
		elif p_SttngType == EnumSettingDataType.OneGroupDeleteOrder :
			if isinstance( p_SttngDefault, EnumDelOrder) :
				i_SttngDefault = p_SttngDefault
			else :
				i_SttngDefault = None
		elif p_SttngType == EnumSettingDataType.OneGenCopyScriptAs :
			if isinstance( p_SttngDefault, EnumGenCopyScriptAs ) :
				i_SttngDefault = p_SttngDefault
			else :
				i_SttngDefault = None
		else :
			# later this will be changed to trigger an error as the idea is for no uncertainties in the setting definitions
			# but until we've covered all the custom enumerations as types of settings then we'll this to show us which are yet to do
			PrintAlert( "Make_SettingAttrbt", "Unhandled Setting Attribute Data Type. Not critical, but shouldn't happen." )
			i_SttngDefault = p_SttngDefault
			#OneDeletionMode = auto()
			#OneDefoliatryMode = auto()
			#OneKeepWhichMatch = auto()
			#OneGroupDeleteOrder = auto()
			#OnePathArchetype = auto()
			#OneCharEncoding = auto()
		# ensure the others are strings
		i_SttngJkey = "" + p_SttngJkey
		i_SttngShow = "" + p_SttngShow
		# make and return the named tuple
		return nmdtpl_SettingAttrbt( p_SttngType, i_SttngDefault, i_SttngJkey, i_SttngShow )

# ==================================================
# Single execution of definition objects
# --------------------------------------------------
# This section gets executed once when the module gets substantiated
# default - there's no such thing as a default in this case

# --------------------------------------------------
# - internal dictionary of attributes - thereby at run-time is just a quick lookup 
# --------------------------------------------------

# A dictionary of nmdtpl_SettingAttrbt objects, which has fields: SttngType SttngDefault SttngJkey SttngShow

# create the dictionary empty
_Dct_SettingDef_Attrbt = {}

# populate the dictionary - by manually setting the named tuples - is verbose but this is where it really gets set so that's fine

	# Prunatry
_Dct_SettingDef_Attrbt[ EnumSettingDef.PrntryLstDisposalPaths ] = \
	Make_SettingAttrbt( EnumSettingDataType.ListString, None, "PrntryLstDisposalPaths", "Prunatry: Disposal list of folders" )
_Dct_SettingDef_Attrbt[ EnumSettingDef.PrntryLstSurvivalPaths ] = \
	Make_SettingAttrbt( EnumSettingDataType.ListString, None, "PrntryLstSurvivalPaths", "Prunatry: Survival list of folders" )
_Dct_SettingDef_Attrbt[ EnumSettingDef.PrntryDoSuperMatch ] = \
	Make_SettingAttrbt( EnumSettingDataType.OneBoolean, True, "PrntryDoSuperMatch", "Prunatry: Do the Matchsubtry Superfolder part?" )
_Dct_SettingDef_Attrbt[ EnumSettingDef.PrntryDoFolderMatch ] = \
	Make_SettingAttrbt( EnumSettingDataType.OneBoolean, True, "PrntryDoFolderMatch", "Prunatry: Do the Matchsubtry Folder part?" )
_Dct_SettingDef_Attrbt[ EnumSettingDef.PrntryDoFileMatch ] = \
	Make_SettingAttrbt( EnumSettingDataType.OneBoolean, True, "PrntryDoFileMatch", "Prunatry: Do the Filedupetry part?" )
_Dct_SettingDef_Attrbt[ EnumSettingDef.PrntryDoMatchStringent ] = \
	Make_SettingAttrbt( EnumSettingDataType.OneBoolean, False, "PrntryDoMatchStringent", "Prunatry: Do stringent matching (in Super & Folder modes)?" )
_Dct_SettingDef_Attrbt[ EnumSettingDef.PrntryFileDoMatchByHash ] = \
	Make_SettingAttrbt( EnumSettingDataType.OneBoolean, False, "PrntryFileDoMatchByHash", "Prunatry: In FileMatch, matching by Hash" )
_Dct_SettingDef_Attrbt[ EnumSettingDef.PrntryFileDoMatchBySize ] = \
	Make_SettingAttrbt( EnumSettingDataType.OneBoolean, False, "PrntryFileDoMatchBySize", "Prunatry: In FileMatch, matching by Size" )
_Dct_SettingDef_Attrbt[ EnumSettingDef.PrntryFileDoMatchByName ] = \
	Make_SettingAttrbt( EnumSettingDataType.OneBoolean, False, "PrntryFileDoMatchByName", "Prunatry: In FileMatch, matching by Name" )
_Dct_SettingDef_Attrbt[ EnumSettingDef.PrntryFileDoMatchByWhen ] = \
	Make_SettingAttrbt( EnumSettingDataType.OneBoolean, False, "PrntryFileDoMatchByWhen", "Prunatry: In FileMatch, matching by When" )
_Dct_SettingDef_Attrbt[ EnumSettingDef.PrntryMtchGrpDelOrder ] = \
	Make_SettingAttrbt( EnumSettingDataType.OneGroupDeleteOrder, False, "PrntryMtchGrpDelOrder", "Prunatry: In FileMatch, order of matched groups to delete" )
_Dct_SettingDef_Attrbt[ EnumSettingDef.PrntryDoCongruent ] = \
	Make_SettingAttrbt( EnumSettingDataType.OneBoolean, True, "PrntryDoCongruent", "Prunatry: Do congruency check before deleting" )
_Dct_SettingDef_Attrbt[ EnumSettingDef.PrntryDoCngrntStringent ] = \
	Make_SettingAttrbt( EnumSettingDataType.OneBoolean, True, "PrntryDoCngrntStringent", "Prunatry: Do stringent congruency check" )
_Dct_SettingDef_Attrbt[ EnumSettingDef.PrntryReportStringentDiffs ] = \
	Make_SettingAttrbt( EnumSettingDataType.OneBoolean, False, "PrntryReportStringentDiffs", "Prunatry: Report Stringent Differences?" )
_Dct_SettingDef_Attrbt[ EnumSettingDef.PrntryDeletionMode ] = \
	Make_SettingAttrbt( EnumSettingDataType.OneDeletionMode , None, "PrntryDeletionMode", "Prunatry: Deletion Method" )
	# Defoliatry
_Dct_SettingDef_Attrbt[ EnumSettingDef.DfltryLstPaths ] = \
	Make_SettingAttrbt( EnumSettingDataType.ListString, None, "DfltryLstPaths", "Defoliatry: Disposal list of folders" )
_Dct_SettingDef_Attrbt[ EnumSettingDef.DfltryFileDoMatchByHash ] = \
	Make_SettingAttrbt( EnumSettingDataType.OneBoolean, True, "DfltryFileDoMatchByHash", "Defoliatry: use hash matching (dormant option)" )
_Dct_SettingDef_Attrbt[ EnumSettingDef.DfltryFileDoMatchBySize ] = \
	Make_SettingAttrbt( EnumSettingDataType.OneBoolean, True, "DfltryFileDoMatchBySize", "Defoliatry: use hash matching (dormant option)" )
_Dct_SettingDef_Attrbt[ EnumSettingDef.DfltryFileDoMatchByName ] = \
	Make_SettingAttrbt( EnumSettingDataType.OneBoolean, True, "DfltryFileDoMatchByName", "Defoliatry: use hash matching (dormant option)" )
_Dct_SettingDef_Attrbt[ EnumSettingDef.DfltryFileDoMatchByWhen ] = \
	Make_SettingAttrbt( EnumSettingDataType.OneBoolean, True, "DfltryFileDoMatchByWhen", "Defoliatry: use hash matching (dormant option)" )
_Dct_SettingDef_Attrbt[ EnumSettingDef.DfltryDoUseDiscardMarkers ] = \
	Make_SettingAttrbt( EnumSettingDataType.OneBoolean, True, "DfltryDoUseDiscardMarkers", "Defoliatry: Use Discard Markers?" )
# _Dct_SettingDef_Attrbt[ EnumSettingDef.DfltryLstDisposalMatches ] = \
# 	Make_SettingAttrbt( EnumSettingDataType.ListString, None, "DfltryLstDisposalMatches", "Defoliatry: list of strongs to match for disposals" )
_Dct_SettingDef_Attrbt[ EnumSettingDef.DfltryDoIgnoreZeroLength ] = \
	Make_SettingAttrbt( EnumSettingDataType.OneBoolean, True, "DfltryDoIgnoreZeroLength", "Defoliatry: Ignore Zero length files?" )
_Dct_SettingDef_Attrbt[ EnumSettingDef.DfltryFileEnumKeepWhich ] = \
	Make_SettingAttrbt( EnumSettingDataType.OneKeepWhichMatch , None, "DfltryFileEnumKeepWhich", "Defoliatry: Within Match Group Selection method" )
_Dct_SettingDef_Attrbt[ EnumSettingDef.DfltryMtchGrpDelOrder ] = \
	Make_SettingAttrbt( EnumSettingDataType.OneGroupDeleteOrder , None, "DfltryMtchGrpDelOrder", "Defoliatry: Group Deletion Order" )
_Dct_SettingDef_Attrbt[ EnumSettingDef.DfltryDoCongruent ] = \
	Make_SettingAttrbt( EnumSettingDataType.OneBoolean, True, "DfltryDoCongruent", "Defoliatry: Do congruency check before deleting" )
_Dct_SettingDef_Attrbt[ EnumSettingDef.DfltryDoCngrntStringent ] = \
	Make_SettingAttrbt( EnumSettingDataType.OneBoolean, True, "DfltryDoCngrntStringent", "Defoliatry: Do stringent congruency check" )
_Dct_SettingDef_Attrbt[ EnumSettingDef.DfltryReportStringentDiffs ] = \
	Make_SettingAttrbt( EnumSettingDataType.OneBoolean, False, "DfltryReportStringentDiffs", "Defoliatry: Report Stringent Differences?" )
_Dct_SettingDef_Attrbt[ EnumSettingDef.DfltryDeletionMode ] = \
	Make_SettingAttrbt( EnumSettingDataType.OneDeletionMode , None, "DfltryDeletionMode", "Defoliatry: Deletion Method" )
	# Trimatry
_Dct_SettingDef_Attrbt[ EnumSettingDef.TrmtryLstPaths ] = \
	Make_SettingAttrbt( EnumSettingDataType.ListString, None, "TrmtryLstPaths", "Trimatry: list of folders" )
_Dct_SettingDef_Attrbt[ EnumSettingDef.TrmtryDoCanDelTopFolders ] = \
	Make_SettingAttrbt( EnumSettingDataType.OneBoolean, False, "TrmtryDoCanDelTopFolders", "Trimatry: Do deletion of top level folders?" )
_Dct_SettingDef_Attrbt[ EnumSettingDef.TrmtryDoNestedEmptyFolders ] = \
	Make_SettingAttrbt( EnumSettingDataType.OneBoolean, True, "TrmtryDoNestedEmptyFolders", "Trimatry: Treat nested empty folders as empty?" )
_Dct_SettingDef_Attrbt[ EnumSettingDef.TrmtryDoTreatZeroAsNothing ] = \
	Make_SettingAttrbt( EnumSettingDataType.OneBoolean, True, "TrmtryDoTreatZeroAsNothing", "Trimatry: Treat zero length files as empty?" )
_Dct_SettingDef_Attrbt[ EnumSettingDef.TrmtryDoExceptKeepIfEmpty ] = \
	Make_SettingAttrbt( EnumSettingDataType.OneBoolean, True, "TrmtryDoExceptKeepIfEmpty", "Trimatry: Do not delete folders containing a .keepifempty file?" )
_Dct_SettingDef_Attrbt[ EnumSettingDef.TrmtryDoExceptNoMedia ] = \
	Make_SettingAttrbt( EnumSettingDataType.OneBoolean, True, "TrmtryDoExceptNoMedia", "Trimatry: Do not delete folders containing a .nomedia file?" )
# TrmtryDoRenameInsteadOfDelete
_Dct_SettingDef_Attrbt[ EnumSettingDef.TrmtryDoRenameInsteadOfDelete ] = \
	Make_SettingAttrbt( EnumSettingDataType.OneBoolean, True, "TrmtryDoRenameInsteadOfDelete", "Trimatry: Rename instead of deleting?" )

_Dct_SettingDef_Attrbt[ EnumSettingDef.TrmtryDeletionMode ] = \
	Make_SettingAttrbt( EnumSettingDataType.OneDeletionMode , None, "TrmtryDeletionMode", "Trimatry: Deletion Method" )
_Dct_SettingDef_Attrbt[ EnumSettingDef.TrmtryDoDelFromDeepest ] = \
	Make_SettingAttrbt( EnumSettingDataType.OneBoolean, False, "TrmtryDoDelFromDeepest", "Trimatry: Delete deepest folders first?" )
	# Clonatry
_Dct_SettingDef_Attrbt[ EnumSettingDef.ClntryStrPathA ] = \
	Make_SettingAttrbt( EnumSettingDataType.OneString , None, "ClntryStrPathA", "Clonatry: Folder A" )
_Dct_SettingDef_Attrbt[ EnumSettingDef.ClntryStrPathB ] = \
	Make_SettingAttrbt( EnumSettingDataType.OneString , None, "ClntryStrPathB", "Clonatry: Folder B" )
_Dct_SettingDef_Attrbt[ EnumSettingDef.ClntryRecreateSourceFolder ] = \
	Make_SettingAttrbt( EnumSettingDataType.OneBoolean , None, "ClntryRecreateSourceFolder", "Clonatry: Recreate source folder" )
_Dct_SettingDef_Attrbt[ EnumSettingDef.ClntryDoKeepTouch ] = \
	Make_SettingAttrbt( EnumSettingDataType.OneBoolean, True, "ClntryDoKeepTouch", "Clonatry: keep file/folder date/time stamps ?" )
_Dct_SettingDef_Attrbt[ EnumSettingDef.ClntryDoKeepAttrib ] = \
	Make_SettingAttrbt( EnumSettingDataType.OneBoolean, True, "ClntryDoKeepAttrib", "Clonatry: keep file/folder attributes ?" )
_Dct_SettingDef_Attrbt[ EnumSettingDef.ClntryDoKeepPerm ] = \
	Make_SettingAttrbt( EnumSettingDataType.OneBoolean, False, "ClntryDoKeepPerm", "Clonatry: keep file/folder permissions ?" )
_Dct_SettingDef_Attrbt[ EnumSettingDef.ClntryDoKeepMeta ] = \
	Make_SettingAttrbt( EnumSettingDataType.OneBoolean, False, "ClntryDoKeepMeta", "Clonatry: keep other file/folder metadata ?" )
	# Congruentry
	# - Path A
_Dct_SettingDef_Attrbt[ EnumSettingDef.CngrntryTwoPathAStrPath ] = \
	Make_SettingAttrbt( EnumSettingDataType.OneString , None, "CngrntryTwoPathAStrPath", "Congruentry: Path A" )
_Dct_SettingDef_Attrbt[ EnumSettingDef.CngrntryTwoPathACustomCharEnc ] = \
	Make_SettingAttrbt( EnumSettingDataType.OneCharEncoding, None, "CngrntryTwoPathACustomCharEnc", "Congruentry: Path A: character encoding" )
_Dct_SettingDef_Attrbt[ EnumSettingDef.CngrntryTwoPathACustomDoCase ] = \
	Make_SettingAttrbt( EnumSettingDataType.OneBoolean, True, "CngrntryTwoPathACustomDoCase", "Congruentry: Path A: treat names as case-sensitive ?" )
_Dct_SettingDef_Attrbt[ EnumSettingDef.CngrntryTwoPathACustomDoMsName ] = \
	Make_SettingAttrbt( EnumSettingDataType.OneBoolean, False, "CngrntryTwoPathACustomDoMsName", "Congruentry: Path A: re-match for names not allowed in DOS/Windows ?" )
_Dct_SettingDef_Attrbt[ EnumSettingDef.CngrntryTwoPathACustomDoMsChar ] = \
	Make_SettingAttrbt( EnumSettingDataType.OneBoolean, False, "CngrntryTwoPathACustomDoMsChar", "Congruentry: Path A: re-match for characters not allowed in DOS/Windows names ?" )
_Dct_SettingDef_Attrbt[ EnumSettingDef.CngrntryTwoPathACustomDoMs8p3 ] = \
	Make_SettingAttrbt( EnumSettingDataType.OneBoolean, False, "CngrntryTwoPathACustomDoMs8p3", "Congruentry: Path A: re-match for DOS 8.3 names ?" )
	# - Path B
_Dct_SettingDef_Attrbt[ EnumSettingDef.CngrntryTwoPathBStrPath ] = \
	Make_SettingAttrbt( EnumSettingDataType.OneString , None, "CngrntryTwoPathBStrPath", "Congruentry: Path B" )
_Dct_SettingDef_Attrbt[ EnumSettingDef.CngrntryTwoPathBCustomCharEnc ] = \
	Make_SettingAttrbt( EnumSettingDataType.OneCharEncoding, None, "CngrntryTwoPathBCustomCharEnc", "Congruentry: Path B: character encoding" )
_Dct_SettingDef_Attrbt[ EnumSettingDef.CngrntryTwoPathBCustomDoCase ] = \
	Make_SettingAttrbt( EnumSettingDataType.OneBoolean, True, "CngrntryTwoPathBCustomDoCase", "Congruentry: Path B: treat names as case-sensitive ?" )
_Dct_SettingDef_Attrbt[ EnumSettingDef.CngrntryTwoPathBCustomDoMsName ] = \
	Make_SettingAttrbt( EnumSettingDataType.OneBoolean, False, "CngrntryTwoPathBCustomDoMsName", "Congruentry: Path B: re-match for names not allowed in DOS/Windows ?" )
_Dct_SettingDef_Attrbt[ EnumSettingDef.CngrntryTwoPathBCustomDoMsChar ] = \
	Make_SettingAttrbt( EnumSettingDataType.OneBoolean, False, "CngrntryTwoPathBCustomDoMsChar", "Congruentry: Path B: re-match for characters not allowed in DOS/Windows names ?" )
_Dct_SettingDef_Attrbt[ EnumSettingDef.CngrntryTwoPathBCustomDoMs8p3 ] = \
	Make_SettingAttrbt( EnumSettingDataType.OneBoolean, False, "CngrntryTwoPathBCustomDoMs8p3", "Congruentry: Path B: re-match for DOS 8.3 names ?" )
#
_Dct_SettingDef_Attrbt[ EnumSettingDef.CngrntryAnalyseStringent ] = \
	Make_SettingAttrbt( EnumSettingDataType.OneBoolean, False, "CngrntryAnalyseStringent", "Congruentry: CngrntryAnalyseStringent ?" )
_Dct_SettingDef_Attrbt[ EnumSettingDef.CngrntryStopNotCongruent ] = \
	Make_SettingAttrbt( EnumSettingDataType.OneBoolean, False, "CngrntryStopNotCongruent", "Congruentry: CngrntryStopNotCongruent ?" )
_Dct_SettingDef_Attrbt[ EnumSettingDef.CngrntryStopNotMoiety ] = \
	Make_SettingAttrbt( EnumSettingDataType.OneBoolean, False, "CngrntryStopNotMoiety", "Congruentry: CngrntryStopNotMoiety ?" )
_Dct_SettingDef_Attrbt[ EnumSettingDef.CngrntryReportDiffs ] = \
	Make_SettingAttrbt( EnumSettingDataType.OneBoolean, False, "CngrntryReportDiffs", "Congruentry: CngrntryReportDiffs ?" )
_Dct_SettingDef_Attrbt[ EnumSettingDef.CngrntryDiffSummary ] = \
	Make_SettingAttrbt( EnumSettingDataType.OneBoolean, False, "CngrntryDiffSummary", "Congruentry: CngrntryDiffSummary ?" )
_Dct_SettingDef_Attrbt[ EnumSettingDef.CngrntryReportStringentDiffs ] = \
	Make_SettingAttrbt( EnumSettingDataType.OneBoolean, False, "CngrntryReportStringentDiffs", "Congruentry: Report Stringent Differences ?" )
_Dct_SettingDef_Attrbt[ EnumSettingDef.CngrntryGenCopyScript ] = \
	Make_SettingAttrbt( EnumSettingDataType.OneBoolean, False, "CngrntryGenCopyScript", "Congruentry: CngrntryGenCopyScript ?" )
_Dct_SettingDef_Attrbt[ EnumSettingDef.CngrntryCopyScriptShell ] = \
	Make_SettingAttrbt( EnumSettingDataType.OneGenCopyScriptAs, False, "CngrntryCopyScriptShell", "Congruentry: CngrntryCopyScriptShell ?" )
	# - File refs A & B
_Dct_SettingDef_Attrbt[ EnumSettingDef.CngrntryTwoFileAStrFileref ] = \
	Make_SettingAttrbt( EnumSettingDataType.OneString , None, "CngrntryTwoFileAStrFileref", "Congruentry: Fileref A" )
_Dct_SettingDef_Attrbt[ EnumSettingDef.CngrntryTwoFileBStrFileref ] = \
	Make_SettingAttrbt( EnumSettingDataType.OneString , None, "CngrntryTwoFileBStrFileref", "Congruentry: Fileref B" )
	# Summatry
_Dct_SettingDef_Attrbt[ EnumSettingDef.SmmtryLstPaths ] = \
	Make_SettingAttrbt( EnumSettingDataType.ListString, None, "SmmtryLstPaths", "Summatry: list of folders" )
_Dct_SettingDef_Attrbt[ EnumSettingDef.SmmtryDoMsName ] = \
	Make_SettingAttrbt( EnumSettingDataType.OneBoolean, None, "SmmtryDoMsName", "Summatry: check for names not allowed in DOS/Windows ?" )
_Dct_SettingDef_Attrbt[ EnumSettingDef.SmmtryDoMsChar ] = \
	Make_SettingAttrbt( EnumSettingDataType.OneBoolean, None, "SmmtryDoMsChar", "Summatry: check for characters not allowed in DOS/Windows names ?" )
	# Other Settings 
_Dct_SettingDef_Attrbt[ EnumSettingDef.SttngLstDiscardNameMarkers ] = \
	Make_SettingAttrbt( EnumSettingDataType.ListString, None, "SttngLstDiscardNameMarkers", "Settings: Discard Marker strings list" )
	# Logging flags
_Dct_SettingDef_Attrbt[ EnumSettingDef.LggngStatus ] = \
	Make_SettingAttrbt( EnumSettingDataType.OneBoolean, None, "LggngStatus", "Logging: of Status events ?" )
_Dct_SettingDef_Attrbt[ EnumSettingDef.LggngResults ] = \
	Make_SettingAttrbt( EnumSettingDataType.OneBoolean, None, "LggngResults", "Logging: of Results events ?" )
_Dct_SettingDef_Attrbt[ EnumSettingDef.LggngProcess ] = \
	Make_SettingAttrbt( EnumSettingDataType.OneBoolean, None, "LggngProcess", "Logging: of Process events ?" )
_Dct_SettingDef_Attrbt[ EnumSettingDef.LggngDetails ] = \
	Make_SettingAttrbt( EnumSettingDataType.OneBoolean, None, "LggngDetails", "Logging: of Details events ?" )
_Dct_SettingDef_Attrbt[ EnumSettingDef.LggngDebug ] = \
	Make_SettingAttrbt( EnumSettingDataType.OneBoolean, None, "LggngDebug", "Logging: of Debug events ?" )

# --------------------------------------------------
# Checks - the idea is that these will cause errors if there are unallowed values in the settings
# --------------------------------------------------

# confirm the SttngJkey values are all there and are unique
_Dct_SettingDef_Attrbt_Jkey_To_Enum = {}
for i_EnumSettingDef in EnumSettingDef :
	if _Dct_SettingDef_Attrbt[ i_EnumSettingDef ].SttngJkey in _Dct_SettingDef_Attrbt_Jkey_To_Enum :
		# trigger an error
		raise Exception("The values written for the internal _Dct_SettingDef_Attrbt has a repeated value of SttngJkey")
		x = 1 / 0
	else :
		# add to reverse dictionary
		_Dct_SettingDef_Attrbt_Jkey_To_Enum[ _Dct_SettingDef_Attrbt[ i_EnumSettingDef ].SttngJkey ] = i_EnumSettingDef

# --------------------------------------------------
# WHY DO I WANT THIS BIT?
# - internal dictionary of pair tuples for use in selectors - first item is the enumeration, second is the string to show 
# example: this can be passed to a modifiec ComboBox widget, usch as Foldatry's PairTupleCombobox
# NOT CURRENTLY NEEDING THIS BUT LEAVING THE CODE HERE ANYWAY

# create the list empty 
_Lst_SettingDef_ComboBoxPairTuples = []
# populate the list
for i_EnumSettingDef in EnumSettingDef :
	#if i_EnumSettingDef in _Dct_SettingDef_Attrbt :
	i_tpl = ( i_EnumSettingDef, _Dct_SettingDef_Attrbt[ i_EnumSettingDef ].SttngShow )
	_Lst_SettingDef_ComboBoxPairTuples.append( i_tpl )
# --------------------------------------------------

# --------------------------------------------------
# SettingDef parts Function calls
# --------------------------------------------------
# so that other uses don't have to know about the internals

def SettingDef_DataType( p_EnumSettingDef ):
	return _Dct_SettingDef_Attrbt[ p_EnumSettingDef ].SttngType

def SettingDef_Default( p_EnumSettingDef ):
	return _Dct_SettingDef_Attrbt[ p_EnumSettingDef ].SttngDefault

def SettingDef_Jkey( p_EnumSettingDef ):
	return _Dct_SettingDef_Attrbt[ p_EnumSettingDef ].SttngJkey

def SettingDef_Show( p_EnumSettingDef ):
	return _Dct_SettingDef_Attrbt[ p_EnumSettingDef ].SttngShow

# For use with JSOL - we need a function to convert from the JSON string key to the enumeration equivalent 

def SettingDef_ForJsonKey_GetEnum( p_str_json_key ) :
	# for a given JSON key, return a matching EnumSettingDef
	if p_str_json_key in _Dct_SettingDef_Attrbt_Jkey_To_Enum :
		r_esd = _Dct_SettingDef_Attrbt_Jkey_To_Enum[ p_str_json_key ]
		r_kok = True
	else :
		r_kok = False 
		r_esd = None
	return r_kok, r_esd

# --------------------------------------------------
# Value dictionary builder
# --------------------------------------------------
# so that internal-to-the-foldatry-module this can be called to create a dictionary of settings
# that can then be modified, saved, loaded during the life of the program

def SettingItem_Make_Dct_Of_Values_With_Defaults():
	#print( "SettingItem_Make_Dct_Of_Values_With_Defaults")
	# create and return a dictionary of all the settings, preset with default values
	r_dct = {}
	for i_EnumSettingDef in EnumSettingDef :
		if False:
			print( "i_EnumSettingDef" )
			print( i_EnumSettingDef )
			print( "SettingDef_Default( i_EnumSettingDef )" )
			print( SettingDef_Default( i_EnumSettingDef ) )
		r_dct[ i_EnumSettingDef ] = SettingDef_Default( i_EnumSettingDef )
	return r_dct

# ==================================================
# Type-smart support functions
# --------------------------------------------------

# This will check that the value is ok for the specified setting or return a None
def SettingItem_EnsureValueSuitsType( p_EnumSettingDef, p_value ) :
	i_SettingItem_DataType = SettingDef_DataType( p_EnumSettingDef )
	if i_SettingItem_DataType == EnumSettingDataType.OneBoolean :
		i_SttngVal = ( p_value == True )
	elif i_SettingItem_DataType == EnumSettingDataType.OneString :
		i_SttngVal = str( p_value)
	elif i_SettingItem_DataType == EnumSettingDataType.ListString :
		if isinstance( p_value, list) :
			# later add a check that all items in the list are strings or convert them all to strings
			i_SttngVal = []
			for list_item in p_value :
				i_SttngVal.append( str( list_item) )
		else :
			i_SttngVal = None
	elif i_SettingItem_DataType == EnumSettingDataType.OneKeepWhichMatch :
		if isinstance( p_value, EnumKeepWhich ) :
			i_SttngVal = p_value
		else :
			i_SttngVal = None
	elif i_SettingItem_DataType == EnumSettingDataType.OneGroupDeleteOrder :
		if isinstance( p_value, EnumDelOrder ) :
			i_SttngVal = p_value
		else :
			i_SttngVal = None
	elif i_SettingItem_DataType == EnumSettingDataType.OneDeletionMode :
		if isinstance( p_value, EnumDeleteMode ) :
			i_SttngVal = p_value
		else :
			i_SttngVal = None
	elif i_SettingItem_DataType == EnumSettingDataType.OneCharEncoding :
		if isinstance( p_value, EnumCharEnc ) :
			i_SttngVal = p_value
		else :
			i_SttngVal = None
	elif i_SettingItem_DataType == EnumSettingDataType.OneGenCopyScriptAs :
		if isinstance( p_value, EnumGenCopyScriptAs ) :
			i_SttngVal = p_value
		else :
			i_SttngVal = None
	else :
		PrintAlert( "SettingItem_EnsureValueSuitsType", "Unhandled Setting Attribute Data Type. Not critical, but shouldn't happen." )
		i_SttngVal = p_value
	return i_SttngVal

# JSON translations to and from our internal data types

# - from JSON to Custom typing

def SettingItem_GetCustomValueForJsonValue( p_EnumSettingDef, p_JsonValue ) :
	# gatekeeper for ensuring the right data type is set into the instance Settings_Values dictionary
	#print( "SettingItem_GetCustomValueForJsonValue")
	dt = SettingDef_DataType( p_EnumSettingDef )
	if dt == EnumSettingDataType.OneString :
		if isinstance( p_JsonValue, str) :
			r_v = p_JsonValue
			r_isok = True
		else :
			try:
				r_v = str( p_JsonValue )
				r_isok = True
			except: 
				r_isok = False
	elif dt == EnumSettingDataType.OneBoolean :
		if isinstance( p_JsonValue, bool) :
			r_v = p_JsonValue
			r_isok = True
		else :
			r_v = cmntry.hndy_str_to_boolean( p_JsonValue )
			r_isok = not ( r_v is None )
	elif dt == EnumSettingDataType.ListString :
		if isinstance( p_JsonValue, list): 
			r_v = p_JsonValue
			r_isok = True
		else :
			try:
				r_v = list( p_JsonValue )
				r_isok = True
			except: 
				r_isok = False
	elif dt == EnumSettingDataType.OneKeepWhichMatch :
		r_v = EnumKeepWhich_For_Jval_Get_Enum( p_JsonValue )
		r_isok = not ( r_v is None )
	elif dt == EnumSettingDataType.OneGroupDeleteOrder :
		r_v = EnumDelOrder_For_Jval_Get_Enum( p_JsonValue )
		r_isok = not ( r_v is None )
	elif dt == EnumSettingDataType.OneDeletionMode :
		r_v = EnumDeleteMode_For_Jval_Get_Enum( p_JsonValue )
		r_isok = not ( r_v is None )
	elif dt == EnumSettingDataType.OneCharEncoding :
		r_v = EnumCharEnc_For_Jval_Get_Enum( p_JsonValue )
		r_isok = not ( r_v is None )
	elif dt == EnumSettingDataType.OneGenCopyScriptAs :
		r_v = EnumGenCopyScriptAs_For_Jval_Get_Enum( p_JsonValue )
		r_isok = not ( r_v is None )
	else :
		r_isok = False
		r_v = ""
	return r_isok, r_v

def SettingItem_MakeSettingValue_Into_Serialisable( p_EnumSettingDef, p_Value ) :
	i_procname = "SettingItem_MakeSettingValue_Into_Serialisable"
	r_sv = None # dummy value shouldn't ever be used
	if isinstance( p_EnumSettingDef, EnumSettingDef) :
		# get the data type
		dt = SettingDef_DataType( p_EnumSettingDef )
		# work through all the custom data types
		if dt == EnumSettingDataType.ListString :
			if p_Value is None :
				r_sv = [ ]
			elif isinstance( p_Value, list ):
				r_sv = [ ]
				for thing in p_Value :
					if isinstance( thing, str ):
						r_sv.append( thing )
					else :
						try:
							r_sv.append( str( thing ) )
						except:
							PrintAlert( "SettingItem_MakeSettingValue_Into_Serialisable", "Un-stringable list content. Not critical, but shouldn't happen." )
			elif isinstance( p_value, str ):
				r_sv = [ p_Value ]
			else :
				try:
					r_sv = [ str( thing ) ]
				except:
					PrintAlert( "SettingItem_MakeSettingValue_Into_Serialisable", "Un-stringable content to make into list. Not critical, but shouldn't happen." )
		elif dt == EnumSettingDataType.OneKeepWhichMatch :
			r_sv = EnumKeepWhich_Attrbt_Jval( p_Value )
			r_isok = not ( r_sv is None )
		elif dt == EnumSettingDataType.OneGroupDeleteOrder :
			r_sv = EnumDelOrder_Attrbt_Jval( p_Value )
			r_isok = not ( r_sv is None )
		elif dt == EnumSettingDataType.OneDeletionMode :
			r_sv = EnumDeleteMode_Attrbt_Jval( p_Value )
			r_isok = not ( r_sv is None )
		elif dt == EnumSettingDataType.OneCharEncoding :
			r_sv = EnumCharEnc_Attrbt_Jval( p_Value )
			r_isok = not ( r_sv is None )
		else :
			r_sv = str( p_Value )
		r_isok = True
	else :
		r_isok = False
	return r_isok, r_sv


# $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
# SECTION C - new rendition of Settings class objects
# $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

# This is done as two classes, mainly so that the second class can be a superset of the first
# So that the first class can be used for a single object in the command line mode
# and the second class can be used for a single object in the GUI mode

# Hence we'll have
#   class FoldatrySettings:
# i.e based on nothing, then
#   class FoldatryGuiSettings( FoldatrySettings ):
# which is based on the first class

# ==================================================
# Note that the first class only needs to be able to Load from file, as saving will be done in/by the GUI mode
# Hence for an object of the FoldatrySettings class, we only need:
# 	.LoadSettingsFromFle
# to load, and then
#   .Settings_Values[ x ]
# to get the various settings, where x is an enumeration of type EnumSettingDef
# All other aspects are internal to the class

class FoldatrySettings:
	"""For instances of Foldatry Settings and what to do with them"""
	
	def __init__(self):
		# give this FoldatrySettings object, a a dictionary of values
		# create instance variables for every possible setting, to from then on manage per EnumSettingDef
		self.Dct_Settings_Values = SettingItem_Make_Dct_Of_Values_With_Defaults()
	
	def Set_SettingValue( self, p_EnumSettingDef, p_value ) :
		if p_EnumSettingDef in EnumSettingDef :
			self.Dct_Settings_Values[ p_EnumSettingDef ] = SettingItem_EnsureValueSuitsType( p_EnumSettingDef, p_value )

	def Get_SettingValue( self, p_EnumSettingDef ) :
		if p_EnumSettingDef in EnumSettingDef :
			return self.Dct_Settings_Values[ p_EnumSettingDef ]
		else :
			return None
	
	def Get_Setting_Value_AsSerialisable( self, p_EnumSettingDef ) :
		if p_EnumSettingDef in EnumSettingDef :
			r_isok, r_sv = SettingItem_MakeSettingValue_Into_Serialisable( p_EnumSettingDef, self.Dct_Settings_Values[ p_EnumSettingDef] )
		else :
			r_isok = False
			r_sv = None 
		return r_isok, r_sv
	
	def LoadSettingsFromFle( self, str_fileref ) : # , p_multi_log
		# the assumption here is that if there is a setting in the file then it becomes the setting in memory 
		# what goes here will depend on the chosen Save method
		#p_multi_log.do_logs( "rb", "LoadSettingsFromFle to " + str_fileref + " LOADING" )
		# print( "LoadSettingsFromFle to " + str_fileref + " LOADING" )
		r_got_ok = False
		r_load_count = 0
		if len( str_fileref ) > 0 :
			if True : # place to put file exists check
				try :
					with open( str_fileref, "r") as read_file:
						try :
							incoming = im_json.load( read_file )
							r_got_ok = True
						except: 
							pass
				except: 
					pass
					# print( "Failed to opoen file " + str_fileref )
		if r_got_ok :
			# now the work of applying the settings
			for j_key in incoming.keys() :
				kok, i_EnumSettingDef = SettingDef_ForJsonKey_GetEnum( j_key )
				if kok:
					vok, sv = SettingItem_GetCustomValueForJsonValue( i_EnumSettingDef, incoming[ j_key ])
					if vok:
						#print( "Setting a value to internal set:")
						self.Dct_Settings_Values[ i_EnumSettingDef] = sv
						r_load_count += 1
						if __debug__:
							pass
							# p_multi_log.do_logs( "b", "Set for key " + str( i_EnumSettingDef.name) + " to " + str( sv)  )
			#p_multi_log.do_logs( "rb", "LoadSettingsFromFle to " + str_fileref + " LOADED !" )
			#print( "LoadSettingsFromFle to " + str_fileref + " LOADED !" )
		else :
			pass
			#p_multi_log.do_logs( "rb", "LoadSettingsFromFle to " + str_fileref + " FAILED !" )
			#print( "LoadSettingsFromFle to " + str_fileref + " FAILED !" )
		return r_got_ok, r_load_count

# ==================================================
# The second needs for more functionality than the first, needing also:
# - to be able to Save to a file, where the values are pulled from the selected GUI elements
# Hence for an object of the FoldatryGuiSettings class, we also need:
# 	.SaveFlaggedSettingsIntoFile
# to save having first written , and then
#   .Settings_Values[ x ] = 
# to get the various settings, where x is an enumeration of type EnumSettingDef
# All other aspects are internal to the class

class FoldatryGuiSettings( FoldatrySettings ):
	"""For instances of FoldatryGuiSettings and what to do with them"""
	# this is for the GUI mode and adds a dictionary of flags to control which are affected by loads and saves
	# class variables as these are always the same
	
	def __init__(self):
		# Call the base class init
		FoldatrySettings.__init__(self)
		# that should have created the dictionary of setting values - all set to defaults
		# now add a dictionary of Boolean flags, these are to say which settings should be loaded or saved
		self.Settings_Flags = {}
		for i_EnumSettingDef in EnumSettingDef :
			self.Settings_Flags[ i_EnumSettingDef ] = True

	def SettingFlag_GetFlag( self, p_EnumSettingDef) :
		if isinstance( p_EnumSettingDef, EnumSettingDef):
			return self.Settings_Flags[ p_EnumSettingDef ]
		else :
			return None

	def SettingFlag_SetFlag( self, p_EnumSettingDef, p_bool ) :
		if isinstance( p_EnumSettingDef, EnumSettingDef) and isinstance( p_bool, bool ) :
			self.Settings_Flags[ p_EnumSettingDef ] = p_bool

	def SaveFlaggedSettingsIntoFile( self, str_fileref ) :
		# for now, just write to the log, later probably write out JSON
		# p_multi_log.do_logs( "rb", "SaveFlaggedSettingsIntoFile to " + str_fileref )
		# to do the export to JSON we'll need an object of just the right structure
		# simplest way to ensure that is to build one
		# print( "SaveFlaggedSettingsIntoFile BEGIN")
		dct_prep = {}
		s_count = 0
		r_count = 0
		for i_EnumSettingDef in EnumSettingDef :
			if self.Settings_Flags[ i_EnumSettingDef] :
				t = "WILL "
				is1, gotval = self.Get_Setting_Value_AsSerialisable( i_EnumSettingDef )
				if is1 :
					dct_prep[ SettingDef_Jkey( i_EnumSettingDef) ] = gotval
					s_count = s_count + 1
					t = t + SettingDef_Show( i_EnumSettingDef ) + ": " + str( gotval )
				else :
					t = t + SettingDef_Show( i_EnumSettingDef ) + ": " + "!unserialisable!"
			else :
				t = "WONT "
				t = t + SettingDef_Show( i_EnumSettingDef ) + ": " + "didn't even look at value:"
			if __debug__:
				pass
				# print( t)
				#p_multi_log.do_logs( "rb", t )
		if len( dct_prep ) > 0 :
			# yet to add full file save Try-Except handling code
			with open( str_fileref, 'w') as outfile:
				im_json.dump(dct_prep, outfile)
				r_count = s_count
		# print( "SaveFlaggedSettingsIntoFile ENDED")
		return r_count

	def LoadFlaggedSettingsFromFile( self, str_fileref ) :
		# unlike the load in the base class, here the flags control which becomes the setting in memory 
		# what goes here will depend on the chosen Save method
		#p_multi_log.do_logs( "rb", "LoadFlaggedSettingsFromFile to " + str_fileref )
		gotok = False
		r_lst_loaded_settings = []
		if len( str_fileref ) > 0 :
			if True : # place to put file exists check
				try :
					with open( str_fileref, "r") as read_file:
						try :
							incoming = im_json.load( read_file )
							gotok = True
						except: 
							pass
				except: 
					pass
		if gotok :
			# now the work of applying the settings
			for skynm in incoming.keys() :
				kok, i_EnumSettingDef = SettingDef_ForJsonKey_GetEnum( skynm )
				if kok and self.Settings_Flags[ i_EnumSettingDef] :
					#print( i_EnumSettingDef )
					#print( incoming[ skynm ] )
					vok, sv = SettingItem_GetCustomValueForJsonValue( i_EnumSettingDef, incoming[ skynm ])
					if vok:
						#print( "Setting a value to internal set:")
						#print( sv )
						self.Dct_Settings_Values[ i_EnumSettingDef] = sv
						r_lst_loaded_settings.append( i_EnumSettingDef)
		# return the list of settings actually loaded
		return r_lst_loaded_settings

# $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
# SECTION D - enumeration of possible processes
# $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

@unique
class RunnblProcessState(Enum):
	Idle = auto() # nothing happening, so a process run can be started
	Running = auto() # currently running, so a run of that process run can NOT be started
	# Paused = auto() # potentially used later on to 
	Braking = auto() # means that a break has been requested but the process has not yet come all the way out of its levels
	Cancelled = auto() # probably a brief state, between the process code completing and a GUI update notice pending (after which the will return to Idle

# In general the state orders will either be:
# - Idle, Running, Idle
# - Idle, Running, Braking, Cancelled, Idle

# enum for the Runnable Processes
# these will be keys into several control structures
@unique
class RunnableProcess(Enum):
	RunPrntry = auto()  # t01
	RunDfltry = auto()  # t03
	RunTrmtry = auto()  # t16_Trim
	RunRpltryDstnctry = auto()  # t14_Dstnctry
	RunRpltryClntry = auto()  # t14_Clntry
	RunRpltryDbtry = auto()  # t14_Dbtry
	RunRpltryMrgtry = auto()  # t14_Mrgtry
	RunRpltrySynpstry = auto()  # t14_Synpstry
	RunRpltryMgrtry = auto()  # t14_Mgrtry
	RunRpltrySkltry = auto()  # t14_Skltry
	RunRpltryMvtry = auto()  # t14_Mvtry
	RunRpltryShfttry = auto()  # t14_Shfttry
	RunRpltryShftfl = auto()  # t14_Shftfl
	RunOrgntryCnprntry = auto()  # t17_st01
	RunCngrntryPaths = auto()  # t02_Paths
	RunCngrntryFiles = auto()  # t02_Files
	RunVrtry = auto()  # t19_
	RunSmmtryGnrl = auto()  # t05_Summ
	RunSmmtryChkDosWin = auto()  # t05_Chek
	RunSmmtryCntsByFe = auto()  # t05_countsbfe
	RunCrtltry = auto() # t15_Crtltry

# create a global dictionary to hold a state for each run process
_Dct_RunnableProcess_CurrentState = {}

# pre-populate the dictionary
for i_RunnableProcess in RunnableProcess :
	_Dct_RunnableProcess_CurrentState[ i_RunnableProcess ] = RunnblProcessState.Idle

# provide a function for setting the state of a run process
def RunnableProcess_SetState( p_RunnableProcess, p_State ):
	if p_RunnableProcess in RunnableProcess and p_State in RunnblProcessState :
		_Dct_RunnableProcess_CurrentState[ i_RunnableProcess ] = p_State

# provide a function for getting the state of a run process
def RunnableProcess_GetState( p_RunnableProcess ):
	if p_RunnableProcess in RunnableProcess and p_State in RunnblProcessState :
		return _Dct_RunnableProcess_CurrentState[ i_RunnableProcess ]
	else :
		return None

# calls that provide meaning
# the idea being to define these here rather than separately out in calling space
# Reminder, as stated above, the state orders will either be:
# - Idle, Running, Idle
# - Idle, Running, Braking, Cancelled, Idle

# provide a function for checking whether the process ...
def RunnableProcess_DoNotInterfere( p_RunnableProcess):
	if p_RunnableProcess in RunnableProcess :
		return _Dct_RunnableProcess_CurrentState[ i_RunnableProcess ] in [ RunnblProcessState.Running, RunnblProcessState.Braking ] # RunnblProcessState.Paused, 
	else:
		return False

# provide a function for checking whether the process ...
def RunnableProcess_CheckIs_Interruptable( p_RunnableProcess):
	if p_RunnableProcess in RunnableProcess :
		return _Dct_RunnableProcess_CurrentState[ i_RunnableProcess ] in [ RunnblProcessState.Running ]
	else:
		return False

# provide a function for checking whether the process ...
def RunnableProcess_CheckIs_HasBeenToldToInterrupt( p_RunnableProcess):
	if p_RunnableProcess in RunnableProcess :
		return _Dct_RunnableProcess_CurrentState[ i_RunnableProcess ] in [ RunnblProcessState.Braking ] # RunnblProcessState.Paused, 
	else:
		return False

# provide a function for checking whether the process ...
def RunnableProcess_Check_NoInterruption( p_RunnableProcess):
	if p_RunnableProcess in RunnableProcess :
		return not _Dct_RunnableProcess_CurrentState[ i_RunnableProcess ] in [ RunnblProcessState.Braking ]
	else:
		return True

# $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
# MAIN - test calls
# $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

def Test_Classes():
	pass

def Test_Base_Enumeration():
	print( "def Test_Base_Enumeration" )
	print( "type I enumerations" )
	print( "_Dct_EnumMatchMode_Attrbt" )
	print( _Dct_EnumMatchMode_Attrbt )
	print( "_Dct_EnumClonatryMode_Attrbt" )
	print( _Dct_EnumClonatryMode_Attrbt )
	print( "_Dct_EnumFileSysArch_Attrbt" )
	print( _Dct_EnumFileSysArch_Attrbt )
	print( "_Dct_EnumGenCopyScriptAs_Attrbt" )
	print( _Dct_EnumGenCopyScriptAs_Attrbt )
	#
	print( "type J enumerations" )
	print( "_Dct_EnumKeepWhich_Attrbt" )
	print( _Dct_EnumKeepWhich_Attrbt )
	print( "_Dct_EnumDelOrder_Attrbt" )
	print( _Dct_EnumDelOrder_Attrbt )
	print( "_Dct_EnumDeleteMode_Attrbt" )
	print( _Dct_EnumDeleteMode_Attrbt )
	print( "_Dct_EnumCharEnc_Attrbt" )
	print( _Dct_EnumCharEnc_Attrbt )

def Test_Base_TuplePairs():
	print( "def Test_Base_Enumeration" )
	print( "type I enumerations" )
	print( "EnumMatchMode_LstPairTpls()" )
	print( EnumMatchMode_LstPairTpls() )
	print( "EnumClonatryMode_LstPairTpls()" )
	print( EnumClonatryMode_LstPairTpls() )
	print( "EnumFileSysArch_LstPairTpls()" )
	print( EnumFileSysArch_LstPairTpls() )
	print( "EnumGenCopyScriptAs_LstPairTpls()" )
	print( EnumGenCopyScriptAs_LstPairTpls() )
	#
	print( "type J enumerations" )
	print( "EnumKeepWhich_LstPairTpls()" )
	print( EnumKeepWhich_LstPairTpls() )
	print( "EnumDelOrder_LstPairTpls()" )
	print( EnumDelOrder_LstPairTpls() )
	print( "EnumDeleteMode_LstPairTpls()" )
	print( EnumDeleteMode_LstPairTpls() )
	print( "EnumCharEnc_LstPairTpls()" )
	print( EnumCharEnc_LstPairTpls() )

def Test_Settings_Enumeration():
	print( "_Dct_SettingDef_Attrbt" )
	print( _Dct_SettingDef_Attrbt )

def Test_Settings_TuplePairs():
	print( "_Lst_SettingDef_ComboBoxPairTuples" )
	print( _Lst_SettingDef_ComboBoxPairTuples )

def Test_Settings_Base_Class_Loopy( p_str_settingsfilename ):
	print( "\nLoop Testing the Settings Class" )
	# create a settings object of default values
	t_FldtryCLI_Settings = FoldatrySettings()
	print( "\nTesting LoadSettingsFromFle" )
	loaded_ok, loaded_count = t_FldtryCLI_Settings.LoadSettingsFromFle( p_str_settingsfilename )
	print( loaded_ok )
	print( loaded_count )
	# 
	for i_esd in EnumSettingDef :
		t_var = t_FldtryCLI_Settings.Get_SettingValue( i_esd )
		print( i_esd )
		print( t_var )
	print( "end of Loop Testing the Settings Class" )

def Test_Settings_GUI_Class( p_str_settingsfilename ):
	print( "\nTesting the GUi Class")
	FldtryWndw_Settings = FoldatryGuiSettings()
	# inspect the flagged settings and check that at least some are flagged
	print( "\nInspect Flags")
	found = False
	for i_EnumSettingDef in EnumSettingDef :
		i = SettingDef_Show( i_EnumSettingDef )
		b = FldtryWndw_Settings.SettingFlag_GetFlag( i_EnumSettingDef)
		i_ok, t = FldtryWndw_Settings.Get_Setting_Value_AsSerialisable( i_EnumSettingDef )
		if not i_ok :
			t = "No stored value!"
		elif t is None :
			t = "Value IS None!"
		print( "Setting: (" + i + ") Flag: " + str( b) + " Value: " )
		print( t )
		found = found or b
	#
	print( "\nTest of Saving all the settings")
	if found :
		print( "Calling the Saving of settings")
		i_saved_settings_count = FldtryWndw_Settings.SaveFlaggedSettingsIntoFile( p_str_settingsfilename )
		print( "Saved " + str( i_saved_settings_count ) + " settings")
	else :
		print( "No settings flagged for Saving")
	#
	print( "\nTest of Loading the settings")
	loaded_ok, loaded_count = FldtryWndw_Settings.LoadSettingsFromFle( p_str_settingsfilename )
	print( loaded_ok )
	print( loaded_count )

	print( "\nTest of Saving only some of the settings")
	i_str_settingsfilename = "/home/ger/foldatry_scratch_test_partial_settings_file.json"
	print( "Setting random flags")
	for i_EnumSettingDef in EnumSettingDef :
		i = SettingDef_Show( i_EnumSettingDef )
		b = FldtryWndw_Settings.SettingFlag_SetFlag( i_EnumSettingDef, decision( 0.7 ) )
	print( "Calling the Saving of settings")
	i_saved_settings_count = FldtryWndw_Settings.SaveFlaggedSettingsIntoFile( i_str_settingsfilename )
	print( "Saved " + str( i_saved_settings_count ) + " settings")
	print( "\nTest of re-loading the partial settings")
	loaded_ok, loaded_count = FldtryWndw_Settings.LoadSettingsFromFle( i_str_settingsfilename )
	print( loaded_ok )
	print( loaded_count )

# main 
if __name__ == "__main__":
	print( "Testing")
	Test_Base_Enumeration()
	Test_Base_TuplePairs()
	Test_Settings_Enumeration()
	Test_Settings_TuplePairs()
	testfilename = "/home/ger/foldatry_scratch_test_settings_file.json"
	Test_Settings_Base_Class_Loopy( testfilename)
	Test_Settings_GUI_Class( testfilename ) # on first run, this will save to that file
	print( "Tested")
