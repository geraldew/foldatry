#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# distinctry = do things with distinct things in trees
#
# This forms part of the foldatry application but can be used as an
# independent command line tool.
#
# --------------------------------------------------
# Copyright (C) 2018-2024  Gerald Waters - Melbourne, Australia
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# --------------------------------------------------
# Imports - Standard
# --------------------------------------------------

import sys as im_sys
import os as im_os
# import os.path as osp
import operator

import tkinter as tkntr 
import tkinter.ttk as tkntr_ttk 
from tkinter import filedialog
from tkinter import messagebox 

import pathlib as pthlb

# --------------------------------------------------
# Imports - Custom
# --------------------------------------------------

import commontry as cmntry

import exploratry as explrtry

from matchsubtry import matchsubtry_command

from deforestry import does_this_folderlist_have_survivors_vs_disposals

# --------------------------------------------------
# Support Stock
# --------------------------------------------------

# --------------------------------------------------
# Support Custom
# --------------------------------------------------

def mod_code_name():
	return "distinctry"

def mod_code_abbr():
	return "dstnctry"

def mod_show_name():
	return "Distinctry"

# --------------------------------------------------
# simpler folder trees
# --------------------------------------------------

class obj_folder():
	def __init__(self, p_path, p_name):
		self.str_path = p_path
		self.str_name = p_name
		self.lst_str_filenm = [] # of just file names
		self.lst_obj_folder = [] # of obj_folder objects
	def Add_File(self, p_filename ):
		self.lst_str_filenm.append( p_filename )
	def Add_Fldr(self, p_foldername ):
		#print( "! Add_Fldr" )
		#print( "In Obj path: " + self.str_path )
		#print( "In Obj name: " + self.str_name )
		#print( "Adding name: " + p_foldername )
		what_path_here = im_os.path.join( self.str_path, self.str_name )
		i_obj_folder = obj_folder( what_path_here, p_foldername )
		self.lst_obj_folder.append( i_obj_folder )
		return i_obj_folder

def print_obj_folder( p_obj_fldr ):
	def str_indention( p_depth) :
		return " " * p_depth
	def recurse_print_obj_folder( p_obj_fldr, p_depth ):
		print( str_indention( p_depth) + "In Obj path: " + p_obj_fldr.str_path )
		print( str_indention( p_depth) + "In Obj name: " + p_obj_fldr.str_name )
		for fn in p_obj_fldr.lst_str_filenm :
			print( str_indention( p_depth) + " " + fn )
		for ob in p_obj_fldr.lst_obj_folder :
			recurse_print_obj_folder( ob, p_depth + 1 )
	print( "print_obj_folder" )
	recurse_print_obj_folder( p_obj_fldr, 0 )

def tree_build_from_path( p_path, p_multi_log ):
	# blah blah
	def traverse_tree_recurse( p_obj_folder, p_depth) :
		nonlocal r_ncalls
		nonlocal r_maxdepth
		r_ncalls += 1
		if p_depth > r_maxdepth :
			r_maxdepth = p_depth
		#print( "i_ncalls: " + str( i_ncalls ) )
		#print( "p_depth: " + str( p_depth ) )
		#print( "Obj path: " + p_obj_folder.str_path )
		#print( "Obj name: " + p_obj_folder.str_name )
		#
		path_to_scan = im_os.path.join( p_obj_folder.str_path, p_obj_folder.str_name )
		#print( "Will scan path: " + path_to_scan )
		entries = explrtry.get_lst_sorted_os_scandir( path_to_scan)
		for entry in entries:
			if entry.is_file(follow_symlinks=False):
				p_obj_folder.Add_File( entry.name )
				#print( "Added file: " + entry.name )
			elif entry.is_dir(follow_symlinks=False):
				i_obj_folder = p_obj_folder.Add_Fldr( entry.name )
				#print( "Added folder: " + entry.name )
				# recursive call
				#print( "Recursing" )
				traverse_tree_recurse( i_obj_folder, p_depth + 1 )
		return
	#print( "tree_build_from_path" )
	#print( "initial path: " + p_path )
	# create initial object
	r_obj_folder = obj_folder( p_path, "" )
	# set tracking variables
	r_ncalls = 0
	r_maxdepth = 0
	# call recursive process
	#print( "first call of recursive process" )
	traverse_tree_recurse( r_obj_folder, 1 )
	#print( "back from recursive process" )
	return r_obj_folder, r_ncalls, r_maxdepth

def tree_get_subtraction_from_obj_folder_a_items_of_lst_matched_super_folders_b( p_obj_folder_a, \
		p_lst_matched_super_folders_b, p_multi_log ):
	# p_obj_folder_a is a linked structure of objects
	# p_lst_matched_super_folders is a list of disposable folders, wherein a folder means and all its contents
	# so what could do, is navigate our way into the tree (of p_obj_folder_a) 
	# and where we find paths that are in the list, remove them
	def test_sub_paths_and_cull_list_matches( p_obj_folder) :
		i_len = len( p_obj_folder.lst_obj_folder)
		for i in range( i_len - 1, -1, -1):
			test_path = im_os.path.join( p_obj_folder.lst_obj_folder[i].str_path, p_obj_folder.lst_obj_folder[i].str_name )
			if test_path in p_lst_matched_super_folders_b :
				del p_obj_folder.lst_obj_folder[i]
			else :
				test_sub_paths_and_cull_list_matches( p_obj_folder.lst_obj_folder[i] )
		return
	root_test_path = im_os.path.join( p_obj_folder_a.str_path, p_obj_folder_a.str_name )
	if root_test_path in p_lst_matched_super_folders_b :
		pass
	else :
		test_sub_paths_and_cull_list_matches( p_obj_folder_a )

# --------------------------------------------------
# borrowed and adapted routines
# --------------------------------------------------

def process_by_superhash_from_largest( p_dct_sized_superhashes, p_dct_hash_lst_fldrs, \
		p_lst_paths_survive, p_lst_paths_dispose, p_break_after, p_multi_log ):
	# Brief:
	# sort the superhashes by size
	# get the largest superhash
	# see if it has folders in both disposals and survivors
	# if Yes then send the disposals for purging
	# Parameters:
	# - p_dct_sized_superhashes - sized set of parent hash points
	# - p_dct_hash_lst_fldrs - reference dictionary of hashes and the lists of folders of those hashes
	# - p_lst_paths_survive - survivor paths
	# - p_lst_paths_dispose - disposal paths
	# - p_break_after - set a maximum number of purgings to do (really, a debugging feature)
	# Returns:
	# had_some_has_both
	# Notes:
	# Code:
	def def_name():
		return "process_by_superhash_from_largest"
	def break_after_maximum():
		return 100
	def break_after_dflt():
		return 100
	p_multi_log.do_logs( "dbs", "process_by_superhash_from_largest" )
	# impose and upper limit on the folder trees to purge - this is a during-development thing, remove later
	if p_break_after < 0 :
		break_after = break_after_maximum()
	elif p_break_after > 0 :
		break_after = p_break_after
	else:
		break_after = break_after_dflt()
	p_multi_log.do_logs( "dbs", "will only attempt to purge " + str(break_after) + " folder trees" )
	r_had_some_has_both = False
	sh_sz_count = len( p_dct_sized_superhashes)
	p_multi_log.do_logs( "db", "there are " + str( sh_sz_count) + " sizes of super hashes." )
	r_lst_matched_super_folders = []
	if sh_sz_count > 0 :
		# get desending list of sizes
		sz_lst = sorted( list( p_dct_sized_superhashes.keys() ), reverse=True )
		sz_at = 0
		# for the top n sizes
		for siz in sz_lst:
			# for each size
			# limit to top n
			sz_at = sz_at + 1
			if sz_at > break_after:
				break
			p_multi_log.do_logs( "db", "SuperHash size #" + str( sz_at) + " of " + str( sh_sz_count) + " size = " + cmntry.hndy_GetHumanReadable(siz) + " " + str( siz) )
			# get the hashes
			h_l = p_dct_sized_superhashes[siz]
			p_multi_log.do_logs( "db", "has " + str( len( h_l)) + " super hashes." )
			hz_count = len( h_l)
			hz_at = 0
			for hsh in h_l:
				# per hash
				hz_at = hz_at + 1
				p_multi_log.do_logs( "db", "Hash #" + str( hz_at) + " of" + str( hz_count) + " hash: " + hsh )
				# for each hash, get the list of folders
				lst_fldrtpl = p_dct_hash_lst_fldrs[hsh]
				if __debug__:
					p_multi_log.do_logs( "b", str( len( lst_fldrtpl) ) + " paths: " )
				has_both, lst_survive, lst_dispose = does_this_folderlist_have_survivors_vs_disposals( \
					lst_fldrtpl, p_lst_paths_survive, p_lst_paths_dispose )
				if has_both :
					p_multi_log.do_logs( "dbs", "We can use this" )
					p_multi_log.do_logs( "dbs", "Keeping " + str( len( lst_survive) ) + " paths: " )
					p_multi_log.do_logs( "dbs", "Purging " + str( len( lst_dispose) ) + " paths: " )
					r_had_some_has_both = True
					r_lst_matched_super_folders.extend( lst_dispose )
				else :
					pass
					p_multi_log.do_logs( "dbs", "No clear SuperHash survivor so cannot automate purging." )
	return r_had_some_has_both, r_lst_matched_super_folders

def find_matching_superfolders( p_str_patha, p_str_pathb, p_fn_Check_NoInterruption, p_multi_log) :
	def def_name():
		return "find_matching_superfolders"
	lst_tree_paths = [ p_str_patha, p_str_pathb ]
	p_lst_paths_survive = [ p_str_pathb ]
	p_lst_paths_dispose = [ p_str_patha ]
	#
	r_had_some_has_both = False
	r_lst_matched_super_folders = []
	#
	p_sbtry_hash_files = False
	if True :
		p_multi_log.do_logs( "dbs", def_name() + ": Step 1 matchsubtry_command" )
		dct_hash_lst_fldrs, dct_fldr_selves, dct_fldr_superfolder, \
			dct_superfolder_folders, dct_hash_best_superhash, dct_superhash_hashes, \
			dct_sized_superhashes = matchsubtry_command( lst_tree_paths, p_sbtry_hash_files, p_fn_Check_NoInterruption, p_multi_log)
		p_multi_log.do_logs( "db", mod_code_name() + " matchsubtry_command results" )
		p_multi_log.do_logs( "db", "dct_hash_lst_fldrs" + " has " + str( len(dct_hash_lst_fldrs)) )
		p_multi_log.do_logs( "db", "dct_fldr_selves" + " has " + str( len(dct_fldr_selves)) )
		p_multi_log.do_logs( "db", "dct_fldr_superfolder" + " has " + str( len(dct_fldr_superfolder)) )
		p_multi_log.do_logs( "db", "dct_superfolder_folders" + " has " + str( len(dct_superfolder_folders)) )
		p_multi_log.do_logs( "db", "dct_hash_best_superhash" + " has " + str( len(dct_hash_best_superhash)) )
		p_multi_log.do_logs( "db", "dct_superhash_hashes" + " has " + str( len(dct_superhash_hashes)) )
		p_multi_log.do_logs( "db", "dct_sized_superhashes" + " has " + str( len(dct_sized_superhashes)) )
		if ( len(dct_sized_superhashes) > 0 ) :
			p_multi_log.do_logs( "dbs", def_name() + ": Step 2 process_by_superhash_from_largest" )
			r_had_some_has_both, r_lst_matched_super_folders = process_by_superhash_from_largest( \
				dct_sized_superhashes, dct_hash_lst_fldrs, \
				p_lst_paths_survive, p_lst_paths_dispose, -1, p_multi_log )
			if r_had_some_has_both :
				p_multi_log.do_logs( "pdb", "Ok, we have work to do." )
				p_multi_log.do_logs( "pdb", "There are " + str( len( r_lst_matched_super_folders ) ) + " folders to work on." )
			else :
				p_multi_log.do_logs( "pdb", "Had superfolder matches but only within one side." )
		else :
			p_multi_log.do_logs( "pdb", "No superfolder matches found." )
	return r_had_some_has_both, r_lst_matched_super_folders


def distinctry_paths_command( p_str_path_test, p_str_path_reference, p_str_path_target, p_fn_Check_NoInterruption, p_multi_log) :
	def def_name():
		return "distinctry_paths_command"
	print( def_name() )
	had_some_has_both, lst_matched_super_folders = find_matching_superfolders( p_str_path_test, p_str_path_reference, p_fn_Check_NoInterruption, p_multi_log)
	if had_some_has_both :
		p_multi_log.do_logs( "pdb", "Doing: flesh out p_str_path_test to a full folder+file tree" )
		tree_obj_folder, i_ncalls, i_maxdepth = tree_build_from_path( p_str_path_reference, p_multi_log )
		p_multi_log.do_logs( "pdb", "Doing: subtract the lst_matched_super_folders from full_test_tree" )
		tree_get_subtraction_from_obj_folder_a_items_of_lst_matched_super_folders_b( tree_obj_folder, \
			lst_matched_super_folders, p_multi_log )
		print_obj_folder( tree_obj_folder )
		p_multi_log.do_logs( "pdb", "to do: check that the destination is viable" )
		p_multi_log.do_logs( "pdb", "to do: check outcome method: direct or make script" )

def setup_logging() :
	# this uses the custom wrapper module "multilog"
	m_log = cmntry.make_multilog_object( mod_code_name(), "" )
	return m_log

if __name__ == "__main__":
	print( "This is the module: distinctry")
	# later do some smart command line parameter checking
	# e.g. if enough info is passed and/or indication to avoid the GUI
	# setup logging
	multi_log = setup_logging()
	if __debug__:
		multi_log.do_logs( "b", "Invocation as Main")
	i_str_path = "/home/ger/dev_tests/"
	#i_str_path = "/home/ger"
	print( "Testing tree_build_from_path for path: " + i_str_path )
	i_obj_folder, i_ncalls, i_maxdepth = tree_build_from_path( i_str_path, multi_log )
	print( "i_ncalls: " + str( i_ncalls ) )
	print( "i_maxdepth: " + str( i_maxdepth ) )
	print_obj_folder( i_obj_folder )
