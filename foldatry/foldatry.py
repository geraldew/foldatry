#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# foldatry = manage folder trees
#
# --------------------------------------------------
# Copyright (C) 2018-2025  Gerald Waters - Melbourne, Australia
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# --------------------------------------------------
# Source Code commentary
# --------------------------------------------------

# Just a very brief note here - see the support wiki pages for more
# This is a single-page multiple-tab interface program
# The code elements (objects, functions etc) for each tab are
# (mostly, hoperfully) distinct which prefixes of:
#   t01_
#   t02_
#   t16_
# etc, but while these were originally numbered to match the order the
# tabs appear from left to right, that association no longer holds and
# rather than rename them, they are now just used as distinct values
# and will appear in the source code either in alpha order or to match
# the sequence of tkinter "pack" placements

# --------------------------------------------------
# Imports - Standard
# --------------------------------------------------

import sys as im_sys
#import os as im_os
#import os.path as osp

import tkinter as m_tkntr 
import tkinter.ttk as m_tkntr_ttk 
import tkinter.scrolledtext as m_tkntr_tkst
#from tkinter import filedialog
#from tkinter import messagebox 
import tkinter.font as m_tkntr_font 

import platform as im_pltfrm

import json

# need the logging module so we can add handlers to the multilog - see below
import logging as im_lggng

from collections import namedtuple

from threading import Thread as fi_Thread 

from queue import Queue as fi_Queue

import pathlib

from enum import Enum, unique, auto

import copy as im_copy

# ------- development assistance
import codegen as im_codegen

# --------------------------------------------------
# Imports - Custom
# --------------------------------------------------

# support modules - aliased
import commontry as cmntry
import applitry as apltry
import exploratry as explrtry

# customised logging module
import multilog

# feature modules - just import selected functions
from deforestry import prunatry_command_recce, prunatry_command
from deforestry import defoliatry_command
from deforestry import default_badmatch_list

from trimatry import trimatry_command_recce, trimatry_command

from clonatry import \
	distinctry_command, clonatry_command, dubatry_command, mergetry_command, synopsitry_command, \
	migratry_command, skeletry_command, \
	movetry_command_content_of_one_folder, movetry_command_list_of_folders, \
	shiftatry_command, shiftafile_command

from congruentry import congruentry_paths_command_recce, congruentry_paths_command, congruentry_files_command_recce, congruentry_files_command

from summatry import summatry_command_recce, summatry_command, summatry_checkfor_recce, summatry_checkfor, summatry_counts_by_filename_ext_recce, summatry_counts_by_filename_ext

from veritry import veritry_command

from organitry import canprunatry_command, renametry_command

from purgetry import remove_file_now as purgetry_remove_file_now

from cartulatry import cartulatry_command_recce, cartulatry_command

# --------------------------------------------------
# Global scope objects
# --------------------------------------------------

# currently none required

# --------------------------------------------------
# Support Custom
# --------------------------------------------------

def app_code_name():
	return "foldatry"

def app_code_abbr():
	return "fldtry"

def app_show_name():
	return "Foldatry"

def app_code_abbr_s():
	return app_code_abbr() + " "

def app_version_code_show() :
	return "v0.9.7.8.8"

def app_platform_show() :
	i_max_unicode, r_ucs_mode = cmntry.python_ucs_mode()
	return r_ucs_mode

def app_show_name_version() :
	i_EnumOS, i_WhichOS = cmntry.GetWhichOS()
	return app_show_name() + " " + app_version_code_show() + " with Python mode: " + app_platform_show() + " on " + i_WhichOS

def app_show_brief_intro() :
	return """
A tool for automated tidying up of large amounts of files and folders.
It exists to tackle amounts of files that would be too many to make per-match judgements.
It mainly attempts to do things that other file tools do not, so for regular file management use something else, something you like.

Interface Overview

When run in GUI mode (that's the default), Foldatry presents as a single window with multiple interface sections as different "tabs".
The independent interactive tabs are:

* Prunatry - for operations using two lists, one for things that won't be affected, and one for things that will be deleted
* Defoliatry - for finding replicated files and using automated methods to choose which to delete and which to keep
* Trimatry - for finding and removing empty files and/or folders
* Replatry - for various copying operations, each in a sub-tab - only partially implemented, some parts have mock interface only
* Organitry - for various organising operations, each in a sub-tab
* Congruentry - for testing two file paths to see if their contents are identical
* Summatry - for making a summary of nominated folders
* Cartulatry - for cataloguing the files and folders of selected folder locations

* Settings - a tab to allow for adjustment of control values. This has sub-tabs for:
 - Discard Markers - a list of patterns, which if matching a file will mark it as priority for disposal
 - Fingerprinting options
 - Save Settings - Saving and Loading
 - Logging

There also some display-only tabs that shows log notes made as the program runs. The Status tab now shows the most recent 500 lines of debug info that is/was directed to the screen.
The current (rough) intention for the three tabs are:

* Process - notes about the stoppings and startings of runs
* Results - quotations of when result sets/files have been made (itself still only a rough idea)
* Status - something visual to let the user see how/that things are progressing.

Generally, each tab can be run independent of the others, but it still might not be wise to do that, for two reasons:
- they might be competing to access the same file paths/drives
- they will all write to the display-only tabs as they go along

For more information see the User Guide via
https://gitlab.com/geraldew/foldatry
"""

def app_show_about_replatry() :
	return """
Where "Replatry" is short-hand for re-place a tree and the various features are:

- Distinctry - (distinct tree) - copy the things in A that are not in B, to C - see separate note just about that. Really no apparent good name, so at least this one is distinct
- Clonatry - (clone a tree) - copy to new location - after the operation, it should be possible to use Congruentry to prove that the copy was all good.
- Dubatry (dub a tree) - copy to location but only where non-clashing - core idea is that no files are deleted by this process. Here the name is borrowed from music mixing where a dub is an addition to the existing sound. For this, the brief is that all files and folders at the target do not get overwritten. Might need an option for whether a folder clash leads to new folders.
- Mergetry - (merge a tree) - copy into perhaps existing structures with over-writing, or maybe automated renaming of files at the target - after this operation it should be possible to do a moiety run of Congruentry to prove that the process was good.
- Synopsitry (synopsis of a tree) - copy folder structures but not the files -  i.e. create a synopsis
- Migratry - (migrate a tree) - move folders and files, leaving behind the folder structures - the anlogy being how people migrate leaving gaps where they used to live
- Skeletry - (skeleton a tree) - delete files but not the folder structures - i.e. reduce to a skeleton
- Movetry - (move a tree) - ordinary combination of copy and then delete source - i.e. requires enough space at the target to hold it all, then prove intact before copying at source
- Shiftatry - (shift a tree) - relocate by multiple sub-shifts - the idea here is to minimise the free space required at the target (to be the size of the largest file)
- Shiftafile - (shift a file) - relocate by multiple sub-shifts of internal parts of a file - a much more dubious kind of operation, as during the process there is no longer an intact file at either end. This will require considerable extra attention to make bullet-proof.
"""

def app_show_about_Distinctry() :
	return "Distinctry - (distinct tree) - copy the things in A that are not in B, to C"

def app_show_about_Clonatry() :
	return "Clonatry - (clone a tree) - copy to new location"

def app_show_about_Dubatry() :
	return "Dubatry (dub a tree) - copy to location but only without clashing"

def app_show_about_Mergetry() :
	return "Mergetry - (merge a tree) - copy into structures with existing files affected"

def app_show_about_Synopsitry() :
	return "Synopsitry (synopsis of a tree) - copy folder structures but not the files"

def app_show_about_Migratry() :
	return "Migratry - (migrate a tree) - move folders and files, leaving behind the folder structures"

def app_show_about_Skeletry() :
	return "Skeletry - (skeleton a tree) - delete files but not the folder structures"

def app_show_about_Movetry() :
	return "Movetry - (move a tree) - copy and then delete source"

def app_show_about_Shiftatry() :
	return "Shiftatry - (shift a tree) - relocate tree by multiple sub-shifts"

def app_show_about_Shiftafile() :
	return "Shiftafile - (shift a file) - relocate one file by multiple sub-shifts"

def app_show_about_organitry() :
	return """
Where "Organitry" is short-hand for organise a tree and the various features are:

- Canprunatry - (can prune a tree) - discover the places where Prunatry could be used to reduce folder+file storage
"""

# $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
# Meta Support Stock
# ----------------------------------------------------------------------
# These do not provide any important function for the application
# but rather, as a method for checking that the program is comprehensive 
# in ways that it _should_ be
# 
# As well as this section, there are also other code sections, seek them
# by the same symbols in a line $$$$$$$$$$$

# Control whether checks will cause crashes or will generate Python code
# The general idea is that if there's a lot of Settings enumerations being
# added then NOT being in prove mode will detect oversights and generate
# Python code.
# But most of the time, leave in Prove mode to affirm full coverage and
# crash for any failings.
def DevelopMode_ProveMode():
	return True

def DevelopModeHeadString():
	return ( "#" * 20 ) + " Begin Develop Mode Notice" + ( "=" * 20 )

def DevelopModeDescriptionString():
	return """As we are running in Develop Mode, additional checks will be done and reported to the terminal display.
You can control whether in Prove or Check mode by editing def DevelopMode_ProveMode()
- when Running in Prove mode, failed checks will crash.
- when Running in Check mode, where Settings Enumerations are found to be incompletely implemented, 
generated Python code will be output as example texts to be added to the program source code.
"""

def DevelopModeProveString():
	return "Now running in Prove mode, failed checks will crash."

def DevelopModeCheckString():
	return """Now running in Check mode, generated Python code will be output."""

def DevelopModeSectionString():
	return ( "=" * 20 ) + " Develop Mode " + ( "=" * 20 )

def DevelopModeAnteCodeString():
	return "Suggested Python code follows:\n" + ( "-" * 20 ) + ( "V" * 10 ) + ( "-" * 20 )

def DevelopModePostCodeString():
	return ( "-" * 20 ) + ( "^" * 10 ) + ( "-" * 20 )

def DevelopModeTailString():
	return ( "#" * 20 ) + " End of Develop Mode  Notice" + ( "=" * 20 )

# we create an enum for the various aspects of the settings that needs to be covered
# note: originally envisioned to be needed for multiple aspects but currently only for one
@unique
class EnumMetaSettingAspect(Enum):
	# WhichForm = auto() 
	# SettingTitle = auto() 
	# Description = auto() 
	# SettingEnum = auto() 
	# HasDctSettingDefAttrbt = auto() # Yes/No
	# HasTestSettingsBaseClass = auto() 
	WidgetObject = auto() 
	# HasGetSettingFromGui = auto() 
	# HasSetSettingIntoGui = auto() 
	# HasCmdLine = auto() 

# then we create a structure - of empty dictionaries - that we will put values into thoughout our program
# initially, this will be always enabled, but can later be couched inside if clauses for only when in debug mode
if __debug__ :
	_Dct_ESD_EMSA = {}
	# ensure there is an empty dictionary for every setting enumeration value
	for esd in apltry.EnumSettingDef :
		for emsa in EnumMetaSettingAspect :
			_Dct_ESD_EMSA[ esd ] = {}

# now we need a function to let us assign into this metadata from around the program text
# that any calls to this function will need to be inside an if __debug__ otherwise it will crash
def Set_ESD_EMSA( p_EnumSettingDef, p_EnumMetaSettingAspect, p_value ):
	_Dct_ESD_EMSA[ p_EnumSettingDef ][ p_EnumMetaSettingAspect ] = p_value

# we now create a function that can be run after all definitions are done
# as this includes coverage by the GUI, this will need to be called by 
# the "main" of the GUi function definition

def ESD_EMSA_Prove():
	# this is designed to crash if things are not perfect
	# while things are fine, the print statements are commented out
	# we will rely on the specific error to indicate which value caused a failure
	#print( "=" * 40 )
	#print( "ESD_EMSA_Check" )
	# if all has been covered then there should be no errors when running the following
	for esd in apltry.EnumSettingDef :
		#print( esd )
		for emsa in EnumMetaSettingAspect :
			#print( emsa )
			t = _Dct_ESD_EMSA[ esd ][ emsa ]
			#print( t)

def ESD_EMSA_Check():
	# this is designed to report the omissions
	print( DevelopModeSectionString() )
	print( "ESD_EMSA_Check" )
	# if all has been covered then there should be no errors when running the following
	for esd in apltry.EnumSettingDef :
		if esd in _Dct_ESD_EMSA :
			for emsa in EnumMetaSettingAspect :
				if emsa in _Dct_ESD_EMSA[ esd ] :
					t = _Dct_ESD_EMSA[ esd ][ emsa ]
				elif emsa == EnumMetaSettingAspect.WidgetObject :
					t_lines = im_codegen.ForSettings_Generate_Python_Lines_TkWidget_AddToTrackingDictionary( esd )
					print( DevelopModeAnteCodeString() )
					print( t_lines)
					print( DevelopModePostCodeString() )
				else :
					print( "In _Dct_ESD_EMSA key" )
					print( esd )
					print( "we have a missing sub-key of" )
					print( emsa )
		else :
			print( "We have a missing key in _Dct_ESD_EMSA" )
			print( esd )

# the idea is that by calling that function, the program will crash for any value not present
# thereby telling us to add a call of Set_ESD_EMSA at the place where that aspect is implemented

# $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

# --------------------------------------------------
# Support Stock
# --------------------------------------------------


def PseudoNoInterruptCheck():
	# this just returns True - because it will be passed to processing functions for CLI mode
	# where it will always indicate 
	# while p_Check_NoInterrupt():
	return True

# --------------------------------------------------
# enum for the CLI instruction arguments
# These are used by the CLI processing and so only lists the ones currently supported for that kind of operation
# NOTE this may therefore not include some that are available in the GUI and are still evolving too much.
# and may include some not meaningful in the GUI mode
# --------------------------------------------------
@unique
class EnumCmdInstruction(Enum):
	LoadSettings = auto() 
	DoPrunatry = auto() 
	DoDefoliatry = auto() 
	DoTrimatry = auto() 
	DoCongruentryPaths = auto() 
	DoCongruentryFiles = auto() 
	DoSummatry = auto() 
	DoCheckNames = auto() 
	DoCountExtensions = auto() 
	ShowCmdLineHelp = auto() 

nmdtpl_Instruction = namedtuple('nmdtpl_Instruction', 'Instruction FileRef')

# --------------------------------------------------
# Class extentions - tkinter
# --------------------------------------------------

class tkntr_ScrollableFrame( m_tkntr.Frame):
	def __init__( self, master, **kwargs):
		m_tkntr.Frame.__init__( self, master, kwargs)
		# create a canvas object and a vertical scrollbar for scrolling it
		self.vscrollbar = m_tkntr.Scrollbar( self, orient=m_tkntr.VERTICAL)
		self.vscrollbar.pack( side=m_tkntr.RIGHT, fill=m_tkntr.Y,  expand=False)
		self.canvas = m_tkntr.Canvas( self, bg='#999999', bd=0, height=450, highlightthickness=0, yscrollcommand=self.vscrollbar.set)
		self.canvas.pack( side=m_tkntr.LEFT, fill=m_tkntr.BOTH, expand=True)
		self.vscrollbar.config( command=self.canvas.yview)
		# reset the view
		self.canvas.xview_moveto( 0)
		self.canvas.yview_moveto( 0)
		# create a frame inside the canvas which will be scrolled with it
		self.interior = m_tkntr.Frame( self.canvas, kwargs)
		self.canvas.create_window(0, 0, window=self.interior, anchor=m_tkntr.NW)

class tkntr_VerticalScrolledFrame( m_tkntr.Frame):
	"""A pure Tkinter scrollable frame that actually works!
	* Use the 'interior' attribute to place widgets inside the scrollable frame
	* Construct and pack/place/grid normally
	* This frame only allows vertical scrolling
	"""
	def __init__(self, parent, *args, **kw):
		m_tkntr.Frame.__init__(self, parent, *args, **kw)
		# create a canvas object and a vertical scrollbar for scrolling it
		vscrollbar = m_tkntr.Scrollbar( self, orient=m_tkntr.VERTICAL)
		vscrollbar.pack( fill=m_tkntr.Y, side=m_tkntr.RIGHT, expand=m_tkntr.FALSE)
		canvas = m_tkntr.Canvas( self, bd=0, highlightthickness=0, yscrollcommand=vscrollbar.set)
		canvas.pack( side=m_tkntr.LEFT, fill=m_tkntr.BOTH, expand=m_tkntr.TRUE)
		vscrollbar.config( command=canvas.yview)
		# reset the view
		canvas.xview_moveto(0)
		canvas.yview_moveto(0)
		# create a frame inside the canvas which will be scrolled with it
		self.interior = interior = m_tkntr.Frame(canvas)
		interior_id = canvas.create_window( 0, 0, window=interior, anchor=m_tkntr.NW)
		# track changes to the canvas and frame width and sync them,
		# also updating the scrollbar
		def _configure_interior(event):
			# update the scrollbars to match the size of the inner frame
			size = (interior.winfo_reqwidth(), interior.winfo_reqheight())
			canvas.config( scrollregion="0 0 %s %s" % size)
			if interior.winfo_reqwidth() != canvas.winfo_width():
				# update the canvas's width to fit the inner frame
				# canvas.config( width=interior.winfo_reqwidth()) # height=interior.winfo_reqheight
				canvas.config( width=interior.winfo_reqwidth(), height=interior.winfo_reqheight() )
		interior.bind('<Configure>', _configure_interior)
		def _configure_canvas(event):
			if interior.winfo_reqwidth() != canvas.winfo_width():
				# update the inner frame's width to fill the canvas
				canvas.itemconfigure(interior_id, width=canvas.winfo_width())
		canvas.bind('<Configure>', _configure_canvas)

# current attempt
class PairTupleCombobox( m_tkntr_ttk.Combobox ):
	def _process_listPairTuple( self, ip_listPairTuple ):
		# process the passed in list of pair-tuples to build internal resource lists per combobox object
		r_list_keys = [] 
		r_list_shows = [] 
		for tpl in ip_listPairTuple:
			r_list_keys.append( tpl[ 0] )
			r_list_shows.append( tpl[ 1] )
		return r_list_keys, r_list_shows
	def __init__( self, container, p_listPairTuple, p_defaultKey, *args, **kwargs):
		self.i_list_keys, self.i_list_shows = self._process_listPairTuple( p_listPairTuple )
		super().__init__(container, *args, **kwargs)
		self['values'] = tuple( self.i_list_shows )
		# still need to set the default value from the nominated key
		try:
			t_default_key_index = self.i_list_keys.index( p_defaultKey ) 
			self.current( t_default_key_index )
		except:
			pass
	def setSelectedKey( self, p_key ):
		try:
			t_key_index = self.i_list_keys.index( p_key ) 
			self.current( t_key_index )
		except:
			pass
	def getSelectedKey( self ):
		try:
			i_index = self.current()
			return self.i_list_keys[ i_index ]
		except:
			return None

# --------------------------------------------------
# Class extentions - Logging
# --------------------------------------------------

# Cls_Lggng_TextHandler_Tkinter_ScrolledText
class Cls_Lggng_TextHandler(im_lggng.Handler):
	"""This class allows you to log to a Tkinter Text or ScrolledText widget"""
	def __init__(self, text):
		# run the regular Handler __init__
		im_lggng.Handler.__init__(self)
		# Store a reference to the Text it will log to
		self.text = text

	def emit(self, record):
		msg = self.format(record)
		def append():
			self.text.configure(state='normal')
			self.text.insert(m_tkntr.END, '\n' + msg)
			self.text.configure( state=m_tkntr.DISABLED )
			# Autoscroll to the bottom
			self.text.yview(m_tkntr.END)
		# This is necessary because we can't modify the Text from other threads
		self.text.after(0, append)

class Cls_Lggng_TextHandler_Tkinter_ScrolledText_Limited( im_lggng.Handler ):
	"""This class allows you to log to a Tkinter Text or ScrolledText widget"""
	def __init__(self, text):
		# run the regular Handler __init__
		im_lggng.Handler.__init__(self)
		# Store a reference to the Text it will log to
		self.text = text
	def emit(self, record):
		msg = self.format(record)
		def append():
			self.text.configure(state='normal')
			# if already has X lines then delete the first one
			# the index property returns a composite "l.c" string
			if int(self.text.index('end-1c').split('.')[0]) > 499 : # returns line count
				self.text.delete('1.0', '1.end + 1 char')
			self.text.insert(m_tkntr.END, '\n' + msg)
			self.text.configure( state=m_tkntr.DISABLED )
			# Autoscroll to the bottom
			self.text.yview(m_tkntr.END)
		# This is necessary because we can't modify the Text from other threads
		self.text.after(0, append)

# --------------------------------------------------
# Custom Settings Supports
# --------------------------------------------------

# --------------------------------------------------
# Interactive
# --------------------------------------------------

def getpathref_fromuser( show_title, from_folder_name) :
	# Brief:
	# present a dialog box for selection of a folder path
	# Parameters:
	# - show_title - string to put on the title bar
	# - from_folder_name - location from which to initially show the folder tree
	# Returns:
	# - a string of the selected folder, or an empty string for no selection
	# Notes:
	# Code:
	if len(show_title) == 0 :
		show_title = "Select a Folder"
	if len(from_folder_name) == 0 :
		from_folder_name = str( pathlib.Path.home() )
	root = m_tkntr.Tk()
	root.withdraw()
	FolderName = m_tkntr.filedialog.askdirectory( title=show_title, initialdir=from_folder_name)
	return FolderName

def getfileref_fromuser( show_title, from_folder_name) :
	# Brief:
	# present a dialog box for selection of a folder path
	# Parameters:
	# - show_title - string to put on the title bar
	# - from_folder_name - location from which to initially show the folder tree
	# Returns:
	# - a string of the selected folder, or an empty string for no selection
	# Notes:
	# Code:
	if len(show_title) == 0 :
		show_title = "Select a File"
	if len(from_folder_name) == 0 :
		from_folder_name = str( pathlib.Path.home() )
	root = m_tkntr.Tk()
	root.withdraw()
	FolderName = m_tkntr.filedialog.askopenfilename( title=show_title, initialdir=from_folder_name)
	return FolderName

def getloadfileref_fromuser( show_title, from_folder_name) :
	# Brief:
	# present a dialog box for selection of a folder path
	# Parameters:
	# - show_title - string to put on the title bar
	# - from_folder_name - location from which to initially show the folder tree
	# Returns:
	# - a string of the selected folder, or an empty string for no selection
	# Notes:
	# Code:
	if len(show_title) == 0 :
		show_title = "Select a File to Load from"
	if len(from_folder_name) == 0 :
		from_folder_name = str( pathlib.Path.home() )
	root = m_tkntr.Tk()
	root.withdraw()
	FolderName = m_tkntr.filedialog.askopenfilename( title=show_title, initialdir=from_folder_name)
	return FolderName

def getsavefileref_fromuser( show_title, from_folder_name) :
	# Brief:
	# present a dialog box for selection of a folder path
	# Parameters:
	# - show_title - string to put on the title bar
	# - from_folder_name - location from which to initially show the folder tree
	# Returns:
	# - a string of the selected folder, or an empty string for no selection
	# Notes:
	# Code:
	if len(show_title) == 0 :
		show_title = "Select a File to Save to"
	if len(from_folder_name) == 0 :
		from_folder_name = str( pathlib.Path.home() )
	root = m_tkntr.Tk()
	root.withdraw()
	FolderName = m_tkntr.filedialog.asksaveasfilename( title=show_title, initialdir=from_folder_name)
	return FolderName

# ......................................................................

def FoldatryWindow( p_window_context, p_multi_log) : 
	# Brief:
	# Provide all the details of the user interface 
	# Parameters:
	# - p_window_context - a tkinter parent reference
	# Returns:
	# Notes:
	# the structure used here is to first have all the sub Defs for the element actions
	# then define all the elements
	# As there is zero likelihood of multiple instances, it is NOT written as a Class
	# Code layout:
	# - Feature defs
	# - - all tabs
	# - - per tab
	# - GUI action defs
	# - - all tabs
	# - - per tab
	# - GUI definition block

	# --------------------------------------------------
	# but first, some (temporary?) vode hacking functions
	# the idea being to use these as AB test controls during new implementations
	# --------------------------------------------------
	def gui_tab_experimenting():
		if __debug__:
			return True
		else :
			return False
	def t01_new_thread_signalling():
		return True

	# --------------------------------------------------
	# enum for the RunMessages - this does not need to be known outside the GUI mode
	@unique
	class RunStateMessages(Enum):
		t01_Done = auto() 
		t02_Paths_Done = auto() 
		t02_Files_Done = auto() 
		t03_Done = auto() 
		t05_Summ_Done = auto() 
		t05_Chek_Done = auto() 
		t05_countsbfe_Done = auto() 
		t14_Dstnctry_Done = auto() 
		t14_Clntry_Done = auto() 
		t14_Dbtry_Done = auto() 
		t14_Mrgtry_Done = auto() 
		t14_Synpstry_Done = auto() 
		t14_Mgrtry_Done = auto() 
		t14_Skltry_Done = auto() 
		t14_Mvtry_Done = auto() 
		t14_Shfttry_Done = auto() 
		t14_Shftfl_Done = auto() 
		t15_Crtltry_Done = auto() 
		t16_Trim_Done = auto() 
		t17_st01_Done = auto() 
		t19_Done = auto() 
	#
	# --------------------------------------------------
	# creat an object type to hold a set of state and lock booleans
	# we'll only have the one of these
	class RunStati:
		RunState = False
		t01_Ctrl_Lock = False
		t02_Ctrl_Lock = False
		t03_Ctrl_Lock = False
		t05_Ctrl_Lock = False
		# Replatry
		t14_Dstnctry_Ctrl_Lock = False 
		t14_Clntry_Ctrl_Lock = False 
		t14_Dbtry_Ctrl_Lock = False 
		t14_Mrgtry_Ctrl_Lock = False 
		t14_Synpstry_Ctrl_Lock = False 
		t14_Mgrtry_Ctrl_Lock = False 
		t14_Skltry_Ctrl_Lock = False 
		t14_Mvtry_Ctrl_Lock = False 
		t14_Shfttry_Ctrl_Lock = False 
		t14_Shftfl_Ctrl_Lock = False 
		#
		t15_Ctrl_Lock = False
		t16_Ctrl_Lock = False
		t17_st01_Ctrl_Lock = False
		t17_st02_Ctrl_Lock = False
		t19_Ctrl_Lock = False
	#
	# --------------------------------------------------
	# Pre-Function "Main" section
	# Here are variables that need to be known to the function definitions code
	# there'll be more "main" section after all the internal def declarations
	# --------------------------------------------------
	i_TheRunStati = RunStati
	#
	i_queue = fi_Queue()
	#
	i_Cngrntry_Custom_FnameMatchSetting_A = apltry.FnameMatchSetting()
	i_Cngrntry_Custom_FnameMatchSetting_B = apltry.FnameMatchSetting()
	#
	# --------------------------------------------------
	# now start defining all the functions that the GUI will need
	# --------------------------------------------------
	# String contants defs
	# - Process button states
	def Strng_RunProcess():
		return "Run process"
	def Strng_HaltProcess():
		return "Halt process"
	def Strng_ProcessCompleted():
		return "Process completed"
	# - Other Buttons
	def Strng_Browse():
		return "Browse.."
	def Strng_AddToList():
		return "Add to list"
	def Strng_AddSubs():
		return "Add subfolders"
	# - Progress alerts
	def Strng_ProcessAbandoned():
		return "Abandoned processing"
	# Feature defs
	def bool_to_chkboxvar( p_bool):
		if p_bool :
			v = 1
		else :
			v = 0
		return v
	# tkinter settings functions
	# tk metrics
	def tk_padx():
		return 9
	def tk_pady():
		return 3
	# tk colours
	def colour_fg_ask():
		return "NavyBlue" # "grey30" 
	def colour_bg_neutral():
		return "light gray" # "LightGoldenrod1" # "bisque" # "honeydew2" "light gray"
	def colour_bg_neutral2():
		return "goldenrod1" # "LightGoldenrod3" # "light slate gray" # "burlywood2" # "honeydew2" 
	def colour_bg_danger():
		return "plum1" # Violet
	def colour_bg_modify():
		return "light salmon" # pink
	def colour_bg_safely():
		return "PaleGreen1"
	def colour_bg_compare():
		return "Turquoise"
	def colour_bg_config():
		return "orange"
	def colour_bg_cngrntry_fldrs():
		return "LightGoldenrod1" # "bisque" # "honeydew2" "light gray"
	def colour_bg_cngrnt_a():
		return "DarkSeaGreen1"
	def colour_bg_cngrnt_b():
		return "CadetBlue1"
	def colour_bg_summatry():
		return "DarkSeaGreen1"
	def colour_bg_trimatry():
		return "light salmon"
	def colour_bg_summary():
		return "CadetBlue2"
	def colour_bg_checkfor():
		return "CadetBlue1"
	def colour_bg_countsbfe():
		return "CadetBlue2"
	def colour_bg_replatry():
		return "ivory2" # "thistle1"
	def colour_bg_organitry():
		return "thistle1"
	def colour_bg_cartulatry():
		return "Turquoise"
	def colour_bg_folders_cartulatry():
		return "lightblue"
	def colour_bg_cartulatry_exclusions():
		return "lightblue1"
	def colour_bg_cartulatry_exclude_files():
		return "lightblue2"
	def colour_bg_cartulatry_exclude_folds():
		return "lightblue3"
	# Run State check and Messagebox
	def not_running_else_say( str_action ) :
		# concept here is to return whether we're free to run a process and if not to show a message about it
		if i_TheRunStati.RunState :
			# Choice between using showinfo, showwarning and showerror
			m_tkntr.messagebox.showwarning(app_show_name(), str_action + " blocked! A run is in process.") 
			i_not_running = False
		else :
			i_not_running = True
		return i_not_running
	# UpdateLoop - to be run as a background thread
	def background_loop( m_stati ):
		# the purpose for this loop, is that it will be set running in a thread of its own
		# and will be used to asynchronously change things in the GUI, mainly as triggered
		# from messages put into a GUI-global queue by each of the process threads
		# note: we've passed in a reference to a global, mainly just so we had a parameter at the calling point
		# it's really just only going to i_TheRunStati anyway
		# currently, this means the message-queueing to tell the GUI about a change of process state
		# is the same place where the various thread states are also set.
		# Or should I separate them, and have each place where a thread puts the message on the queue also amend the central record of its state?
		# I thought I had a reason for one over the other but can't recall it.
		while True:
			i_msg = i_queue.get()
			if i_msg == RunStateMessages.t01_Done :
				m_stati.RunState = False
				if t01_new_thread_signalling():
					apltry.RunnableProcess_SetState( apltry.RunnableProcess.RunPrntry, apltry.RunnblProcessState.Idle )
				else:
					m_stati.t01_Ctrl_Lock = False
				t01_button_prunatry_text.set( Strng_RunProcess() )
				t01_label_prunatry_status.configure( text = Strng_ProcessCompleted() )
			elif i_msg == RunStateMessages.t02_Paths_Done :
				m_stati.RunState = False
				m_stati.t02_Ctrl_Lock = False
				t02_button_paths_process_text.set( Strng_RunProcess() )
				t02_label_paths_process_status.configure( text = Strng_ProcessCompleted() )
			elif i_msg == RunStateMessages.t02_Files_Done :
				m_stati.RunState = False
				m_stati.t02_Ctrl_Lock = False
				t02_button_files_process_text.set( Strng_RunProcess() )
				t02_label_files_process_status.configure( text = Strng_ProcessCompleted() )
			elif i_msg == RunStateMessages.t03_Done :
				m_stati.RunState = False
				m_stati.t03_Ctrl_Lock = False
				t03_button_dedupe_text.set( Strng_RunProcess() )
				t03_label_dedupe_status.configure( text = Strng_ProcessCompleted() )
			elif i_msg == RunStateMessages.t05_Summ_Done :
				m_stati.RunState = False
				m_stati.t05_Ctrl_Lock = False
				t05_button_action_var.set( Strng_RunProcess() )
				t05_label_action_status.configure( text = Strng_ProcessCompleted() )
			elif i_msg == RunStateMessages.t05_Chek_Done :
				m_stati.RunState = False
				m_stati.t05_Ctrl_Lock = False
				t05_button_checkfor_var.set( Strng_RunProcess() )
				t05_label_checkfor_status.configure( text = Strng_ProcessCompleted() )
			elif i_msg == RunStateMessages.t05_countsbfe_Done :
				m_stati.RunState = False
				m_stati.t05_Ctrl_Lock = False
				t05_button_countsbfe_var.set( Strng_RunProcess() )
				t05_label_countsbfe_status.configure( text = Strng_ProcessCompleted() + "!!" )
			# Replatry
			elif i_msg == RunStateMessages.t14_Clntry_Done :
				m_stati.RunState = False
				m_stati.t14_Clntry_Ctrl_Lock = False
				t14_Clntry_button_Process_text.set( Strng_RunProcess() )
				t14_Clntry_label_Process_Status.configure( text = Strng_ProcessCompleted() )
			elif i_msg == RunStateMessages.t14_Synpstry_Done :
				m_stati.RunState = False
				m_stati.t14_Synpstry_Ctrl_Lock = False
				t14_Synpstry_button_Process_text.set( Strng_RunProcess() )
				t14_Synpstry_label_Process_Status.configure( text = Strng_ProcessCompleted() )
			elif i_msg == RunStateMessages.t14_Skltry_Done :
				m_stati.RunState = False
				m_stati.t14_Skltry_Ctrl_Lock = False
				t14_Skltry_button_Process_text.set( Strng_RunProcess() )
				t14_Skltry_label_Process_Status.configure( text = Strng_ProcessCompleted() )
			elif i_msg == RunStateMessages.t14_Mgrtry_Done :
				m_stati.RunState = False
				m_stati.t14_Mgrtry_Ctrl_Lock = False
				t14_Mgrtry_button_Process_text.set( Strng_RunProcess() )
				t14_Mgrtry_label_Process_Status.configure( text = Strng_ProcessCompleted() )
			elif i_msg == RunStateMessages.t14_Mgrtry_Done :
				m_stati.RunState = False
				m_stati.t14_Mvtry_Ctrl_Lock = False
				t14_Mvtry_button_Process_text.set( Strng_RunProcess() )
				t14_Mvtry_label_Process_Status.configure( text = Strng_ProcessCompleted() )
			elif i_msg == RunStateMessages.t15_Crtltry_Done :
				m_stati.RunState = False
				m_stati.t15_Ctrl_Lock = False
				t15_button_action_var.set( Strng_RunProcess() )
				t15_label_action_status.configure( text = Strng_ProcessCompleted() )
			elif i_msg == RunStateMessages.t16_Trim_Done :
				m_stati.RunState = False
				m_stati.t16_Ctrl_Lock = False
				t16_button_action_var.set( Strng_RunProcess() )
				t16_label_action_status.configure( text = Strng_ProcessCompleted() )
			elif i_msg == RunStateMessages.t17_st01_Done :
				m_stati.RunState = False
				m_stati.t17_st01_Ctrl_Lock = False
				t17_st01_button_dedupe_text.set( Strng_RunProcess() )
				t17_st01_label_dedupe_status.configure( text = Strng_ProcessCompleted() )
			elif i_msg == RunStateMessages.t17_st02_Done :
				m_stati.RunState = False
				m_stati.t17_st02_Ctrl_Lock = False
				t17_st02_button_dedupe_text.set( Strng_RunProcess() )
				t17_st02_label_dedupe_status.configure( text = Strng_ProcessCompleted() )
			elif i_msg == RunStateMessages.t19_Done :
				m_stati.RunState = False
				m_stati.t19_Ctrl_Lock = False
				t19_button_prunatry_text.set( Strng_RunProcess() )
				t19_label_prunatry_status.configure( text = Strng_ProcessCompleted() )
	# Other generically named functions
	def DoNotRunProcessesAsThreads():
		return ( t06_t5_chkvr_thread_status.get() == 1 )
	# Now we have the functions per Tab - in their numeric order
	# t01 = for tab Prunatry
	def t01_get_disposal_list() :
		tpl_disposal = t01_lb_disposal.get(0, m_tkntr.END)
		lst_disposal = list(tpl_disposal)
		return lst_disposal
	def t01_set_disposal_list( lst_disposal ) :
		lst_get = t01_get_disposal_list() 
		for pth in lst_disposal :
			if not (pth in lst_get) :
				t01_lb_disposal.insert( 0, pth)
	def t01_get_survival_list() :
		tpl_survival = t01_lb_survival.get(0, m_tkntr.END)
		lst_survival = list(tpl_survival)
		return lst_survival
	def t01_set_survival_list( lst_survival ) :
		lst_get = t01_get_survival_list() 
		for pth in lst_survival :
			if not (pth in lst_get) :
				t01_lb_survival.insert( 0, pth)
	def t01_check_path_for_clashes( str_path ) :
		nonlocal p_multi_log
		i_lst_disposal = t01_get_disposal_list()
		i_lst_survival = t01_get_survival_list()
		clash_disposal, made_sub_disposal = explrtry.pathlist_path_clash( i_lst_disposal, str_path, False, p_multi_log)
		clash_survival, made_sub_survival = explrtry.pathlist_path_clash( i_lst_survival, str_path, False, p_multi_log)
		return clash_disposal, made_sub_disposal, clash_survival, made_sub_survival
	def t01_check_disposal_pathlist_for_clashes() :
		nonlocal p_multi_log
		i_lst_disposal = t01_get_disposal_list()
		any_clash, any_sub = explrtry.pathlist_self_clash( i_lst_disposal, False, p_multi_log )
		return not any_clash
	def t01_check_survival_pathlist_for_clashes() :
		nonlocal p_multi_log
		i_lst_survival = t01_get_survival_list()
		any_clash, any_sub = explrtry.pathlist_self_clash( i_lst_survival, False, p_multi_log )
		return not any_clash
	def t01_check_both_pathlists_for_clashes() :
		nonlocal p_multi_log
		i_lst_disposal = t01_get_disposal_list()
		i_lst_survival = t01_get_survival_list()
		some_clash = explrtry.pathlist_pathlist_clash( i_lst_disposal, i_lst_survival, False, p_multi_log)
		return not some_clash
	def t01_swap_lists_disposal_survival() :
		i_lst_disposal = t01_get_disposal_list()
		i_lst_survival = t01_get_survival_list()
		t01_lb_disposal.delete(0, m_tkntr.END) 
		t01_lb_survival.delete(0, m_tkntr.END) 
		t01_set_disposal_list( i_lst_survival ) 
		t01_set_survival_list( i_lst_disposal ) 
	def t01_fail_check_parts_selection() :
		t_SuperM = ( t01_chkvr_super.get() == 1 )
		t_FolderM = ( t01_chkvr_fldrs.get() == 1 )
		t_FileM = ( t01_chkvr_files.get() == 1 )
		# fail if none are selected
		return not t_SuperM and not t_FolderM and not t_FileM
	def t01_fail_check_risky_process() :
		# see if the current settings look risky and if so check with the user
		r_aborted_process = True
		# collect boolean states
		t_SuperM = ( t01_chkvr_super.get() == 1 )
		t_FolderM = ( t01_chkvr_fldrs.get() == 1 )
		t_FileM = ( t01_chkvr_files.get() == 1 )
		t_MatchStringent = ( t01_chkvr_stringent.get() == 1 )
		t_ContentHash, d_UseSize, d_UseName, d_UseWhen = t01_defmod_checkboxes_get()
		t_DoCongruent = ( t01_chkvr_cngrnt_bf_del.get() == 1 )
		t_CongruentStringent = ( t01_chkvr_cngrnt_strngnt.get() == 1 )
		t_EnumDeleteMode = t01_cb_EnumDeleteMode.getSelectedKey()
		# judge the combinations
		if ( ( ( t_SuperM or t_FolderM ) and t_MatchStringent ) or ( t_FileM and t_ContentHash ) or (t_DoCongruent and t_CongruentStringent ) ) \
			or not ( t_EnumDeleteMode == apltry.EnumDeleteMode.DeleteNow ) :
			r_aborted_process = False
		if r_aborted_process :
			question_answer = m_tkntr.messagebox.askquestion("Exit Application", "This doesn't? look like a safe operation.\nAre you quite sure about these settings?", icon="warning")
			r_aborted_process = not ( question_answer == 'yes' )
		if False:
			# used when checking the logic of the riskiness check
			if r_aborted_process:
				t_show_aborted_process = "r_aborted_process is True"
			else:
				t_show_aborted_process = "r_aborted_process is False"
			m_tkntr.messagebox.showwarning( "Test of Prunatry Warning Check", t_show_aborted_process + "\nwill now override to True so nothing should run") 
		return r_aborted_process
	# For Tab Congruenty
	# none needed
	# For Tab Defoliatry
	def t03_get_paths_list() :
		tpl_paths = t03_lb_paths.get(0, m_tkntr.END)
		lst_paths = list( tpl_paths)
		return lst_paths
	def t03_set_paths_list( lst_paths ) :
		lst_get = t03_get_paths_list() 
		for pth in lst_paths :
			if not (pth in lst_get) :
				t03_lb_paths.insert( 0, pth)
	def t03_check_path_for_clashes( str_path ) :
		nonlocal p_multi_log
		i_lst_paths = t03_get_paths_list()
		r_is_clash, r_is_sub = explrtry.pathlist_path_clash( i_lst_paths, str_path, False, p_multi_log)
		return r_is_clash, r_is_sub
	def t03_path_addpath_if_okto( p_path):
		added_path = False
		if ( t03_chkvr_autoclashcheck_paths.get() == 1 ) :
			i_is_clash, i_is_sub = t03_check_path_for_clashes( p_path )
			if not i_is_clash :
				t03_lb_paths.insert( 0, p_path)
				added_path = True
			else: 
				# provide info about the clashing
				str_advice = "That path would clash!"
				if i_is_sub :
					str_advice = str_advice + "\nMakes Sub-folder."
				elif i_is_clash :
					str_advice = str_advice + "\nAlready in list."
				m_tkntr.messagebox.showwarning(app_show_name() + " - Adding Survival Path", str_advice) 
		else :
			# go straight ahead and add, relying on user to then do check
			t03_lb_paths.insert( 0, p_path)
			added_path = True
		if added_path :
			t03_label_paths_alert.configure( text = "Added path: " + p_path)
	def t03_check_paths_for_clashes() :
		nonlocal p_multi_log
		i_lst_paths = t03_get_paths_list()
		any_clash, any_sub = explrtry.pathlist_self_clash( i_lst_paths, False, p_multi_log)
		return not any_clash
	# For Tab Summatry
	def t05_get_folder_list() :
		tpl_folder = t05_lb_folderlist.get(0, m_tkntr.END)
		lst_folder = list( tpl_folder )
		return lst_folder
	def t05_set_folder_list( lst_folder ) :
		lst_get = t05_get_folder_list() 
		for pth in lst_folder :
			if not (pth in lst_get) :
				t05_lb_folderlist.insert( 0, pth)
	def t05_check_path_for_folder_clashes( str_path ) :
		nonlocal p_multi_log
		i_lst_folder = t05_get_folder_list()
		clash_folder, made_sub_folder = explrtry.pathlist_path_clash( i_lst_folder, str_path, False, p_multi_log)
		return clash_folder, made_sub_folder
	def t05_check_folder_pathlist_for_clashes() :
		nonlocal p_multi_log
		i_lst_folder = t05_get_folder_list()
		any_clash, any_sub = explrtry.pathlist_self_clash( i_lst_folder, False, p_multi_log)
		return not any_clash
	# For Tab Cartulatry
	def t15_get_folder_list() :
		tpl_folder = t15_lb_folderlist.get(0, m_tkntr.END)
		lst_folder = list( tpl_folder )
		#print( "t15_get_folder_list")
		#print( lst_folder )
		return lst_folder
	def t15_set_folder_list( lst_folder ) :
		lst_get = t15_get_folder_list() 
		for pth in lst_folder :
			if not (pth in lst_get) :
				t15_lb_folderlist.insert( 0, pth)
	def t15_check_path_for_folder_clashes( str_path ) :
		nonlocal p_multi_log
		i_lst_folder = t15_get_folder_list()
		clash_folder, made_sub_folder = explrtry.pathlist_path_clash( i_lst_folder, str_path, False, p_multi_log)
		return clash_folder, made_sub_folder
	def t15_check_folder_pathlist_for_clashes() :
		nonlocal p_multi_log
		i_lst_folder = t15_get_folder_list()
		any_clash, any_sub = explrtry.pathlist_self_clash( i_lst_folder, False, p_multi_log)
		return not any_clash
	# file exclusion regular expression strings
	def t15_get_file_exclusions_list() :
		tpl_folder = t15_lb_file_exclusions.get(0, m_tkntr.END)
		lst_folder = list( tpl_folder )
		#print( "t15_get_folder_list")
		#print( lst_folder )
		return lst_folder
	def t15_set_file_exclusions_list( lst_folder ) :
		lst_get = t15_get_file_exclusions_list() 
		for pth in lst_folder :
			if not (pth in lst_get) :
				t15_lb_file_exclusions.insert( 0, pth)
	# folder exclusion regular expression strings
	def t15_get_fold_exclusions_list() :
		tpl_folder = t15_lb_fold_exclusions.get(0, m_tkntr.END)
		lst_folder = list( tpl_folder )
		#print( "t15_get_folder_list")
		#print( lst_folder )
		return lst_folder
	def t15_set_fold_exclusions_list( lst_folder ) :
		lst_get = t15_get_fold_exclusions_list() 
		for pth in lst_folder :
			if not (pth in lst_get) :
				t15_lb_fold_exclusions.insert( 0, pth)
	# For Tab Trimatry
	def t16_get_folder_list() :
		tpl_folder = t16_lb_folderlist.get(0, m_tkntr.END)
		lst_folder = list( tpl_folder )
		return lst_folder
	def t16_set_folder_list( lst_folder ) :
		lst_get = t16_get_folder_list() 
		for pth in lst_folder :
			if not (pth in lst_get) :
				t16_lb_folderlist.insert( 0, pth)
	def t16_check_path_for_folder_clashes( str_path ) :
		nonlocal p_multi_log
		i_lst_folder = t16_get_folder_list()
		clash_folder, made_sub_folder = explrtry.pathlist_path_clash( i_lst_folder, str_path, False, p_multi_log)
		return clash_folder, made_sub_folder
	def t16_check_folder_pathlist_for_clashes() :
		nonlocal p_multi_log
		i_lst_folder = t16_get_folder_list()
		any_clash, any_sub = explrtry.pathlist_self_clash( i_lst_folder, False, p_multi_log)
		return not any_clash
	# For Tab Organitry SubTab Canprunatry
	def t17_st01_get_paths_list() :
		tpl_paths = t17_st01_lb_paths.get(0, m_tkntr.END)
		lst_paths = list( tpl_paths)
		return lst_paths
	def t17_st01_set_paths_list( lst_paths ) :
		lst_get = t17_st01_get_paths_list() 
		for pth in lst_paths :
			if not (pth in lst_get) :
				t17_st01_lb_paths.insert( 0, pth)
	def t17_st01_check_path_for_clashes( str_path ) :
		nonlocal p_multi_log
		i_lst_paths = t17_st01_get_paths_list()
		r_is_clash, r_is_sub = explrtry.pathlist_path_clash( i_lst_paths, str_path, False, p_multi_log)
		return r_is_clash, r_is_sub
	def t17_st01_path_addpath_if_okto( p_path):
		added_path = False
		if ( t17_st01_chkvr_autoclashcheck_paths.get() == 1 ) :
			i_is_clash, i_is_sub = t17_st01_check_path_for_clashes( p_path )
			if not i_is_clash :
				t17_st01_lb_paths.insert( 0, p_path)
				added_path = True
			else: 
				# provide info about the clashing
				str_advice = "That path would clash!"
				if i_is_sub :
					str_advice = str_advice + "\nMakes Sub-folder."
				elif i_is_clash :
					str_advice = str_advice + "\nAlready in list."
				m_tkntr.messagebox.showwarning(app_show_name() + " - Adding Survival Path", str_advice) 
		else :
			# go straight ahead and add, relying on user to then do check
			t17_st01_lb_paths.insert( 0, p_path)
			added_path = True
		if added_path :
			t17_st01_label_paths_alert.configure( text = "Added path: " + p_path)
	def t17_st01_check_paths_for_clashes() :
		nonlocal p_multi_log
		i_lst_paths = t17_st01_get_paths_list()
		any_clash, any_sub = explrtry.pathlist_self_clash( i_lst_paths, False, p_multi_log)
		return not any_clash

	# For Tab Organitry SubTab Renametry
	def t17_st02_get_paths_list() :
		tpl_paths = t17_st02_lb_paths.get(0, m_tkntr.END)
		lst_paths = list( tpl_paths)
		return lst_paths
	def t17_st02_set_paths_list( lst_paths ) :
		lst_get = t17_st02_get_paths_list() 
		for pth in lst_paths :
			if not (pth in lst_get) :
				t17_st02_lb_paths.insert( 0, pth)
	def t17_st02_check_path_for_clashes( str_path ) :
		nonlocal p_multi_log
		i_lst_paths = t17_st02_get_paths_list()
		r_is_clash, r_is_sub = explrtry.pathlist_path_clash( i_lst_paths, str_path, False, p_multi_log)
		return r_is_clash, r_is_sub
	def t17_st02_path_addpath_if_okto( p_path):
		added_path = False
		if ( t17_st02_chkvr_autoclashcheck_paths.get() == 1 ) :
			i_is_clash, i_is_sub = t17_st02_check_path_for_clashes( p_path )
			if not i_is_clash :
				t17_st02_lb_paths.insert( 0, p_path)
				added_path = True
			else: 
				# provide info about the clashing
				str_advice = "That path would clash!"
				if i_is_sub :
					str_advice = str_advice + "\nMakes Sub-folder."
				elif i_is_clash :
					str_advice = str_advice + "\nAlready in list."
				m_tkntr.messagebox.showwarning(app_show_name() + " - Adding Survival Path", str_advice) 
		else :
			# go straight ahead and add, relying on user to then do check
			t17_st02_lb_paths.insert( 0, p_path)
			added_path = True
		if added_path :
			t17_st02_label_paths_alert.configure( text = "Added path: " + p_path)
	def t17_st02_check_paths_for_clashes() :
		nonlocal p_multi_log
		i_lst_paths = t17_st02_get_paths_list()
		any_clash, any_sub = explrtry.pathlist_self_clash( i_lst_paths, False, p_multi_log)
		return not any_clash


	# For Tab Settings
	# For Sub Tab 1 Discard Markers
	def get_malname_list() :
		tpl_malname = t06_t1_lb_malname.get(0, m_tkntr.END)
		lst_malname = list(tpl_malname)
		return lst_malname
	def set_malname_list( lst_malname ) :
		lst_get = get_malname_list() 
		for mn in lst_malname :
			if not (mn in lst_get) :
				t06_t1_lb_malname.insert( 0, mn)
	def check_name_for_malnamelist_clashes( str_name ) :
		nonlocal p_multi_log
		i_lst_malname = get_malname_list()
		clash_malname, made_sub = explrtry.pathlist_path_clash( i_lst_malname, str_name, False, p_multi_log)
		return clash_malname, made_sub
	def check_malnamelist_for_clashes() :
		i_lst_malname = get_malname_list()
		any_clash, any_sub = explrtry.pathlist_self_clash( i_lst_malname, False, p_multi_log)
		return not any_clash
	# Settings - 
	def UpdateSettingFlagsFromGuiCheckboxes() :
			# update the flag booleans from the GUI check boxes 
		for sk in apltry.EnumSettingDef :
			i = sk.value
			FldtryWndw_GuiSettings.SettingFlag_SetFlag( sk, ( t06_t4_dct_cbv[i].get() == 1 ) )
	# ==================================================================
	# Settings - Mapping to/from widgets
	# ------------------------------------------------------------------
	# Functions to support getting Settings values from the GUI widgets
	# First, we make a custom function for each widget
	# while these are _largely_ the same, they provide a consistent call 
	# for what are really specific for each - the name echoes the matching enumeration
	# Prunatry
	def GetSettingFromGui_PrntryLstDisposalPaths():
		r_setting_value = t01_get_disposal_list()
		r_isok = ( len( r_setting_value ) > 0 )
		return r_isok, r_setting_value
	def GetSettingFromGui_PrntryLstSurvivalPaths():
		r_setting_value = t01_get_survival_list()
		r_isok = ( len( r_setting_value ) > 0 )
		return r_isok, r_setting_value
	def GetSettingFromGui_PrntryDoSuperMatch():
		r_setting_value = ( t01_chkvr_super.get() == 1 )
		r_isok = True
		return r_isok, r_setting_value
	def GetSettingFromGui_PrntryDoFolderMatch():
		r_setting_value = ( t01_chkvr_fldrs.get() == 1 )
		r_isok = True
		return r_isok, r_setting_value
	def GetSettingFromGui_PrntryDoFileMatch():
		r_setting_value = ( t01_chkvr_files.get() == 1 )
		r_isok = True
		return r_isok, r_setting_value
	def GetSettingFromGui_PrntryDoMatchStringent():
		r_setting_value = ( t01_chkvr_stringent.get() == 1 )
		r_isok = True
		return r_isok, r_setting_value
	def GetSettingFromGui_PrntryFileDoMatchByHash():
		r_setting_value = ( t01_chkvr_defmod_hash.get() == 1 )
		r_isok = True
		return r_isok, r_setting_value
	def GetSettingFromGui_PrntryFileDoMatchBySize():
		r_setting_value = ( t01_chkvr_defmod_size.get() == 1 )
		r_isok = True
		return r_isok, r_setting_value
	def GetSettingFromGui_PrntryFileDoMatchByName():
		r_setting_value = ( t01_chkvr_defmod_name.get() == 1 )
		r_isok = True
		return r_isok, r_setting_value
	def GetSettingFromGui_PrntryFileDoMatchByWhen():
		r_setting_value = ( t01_chkvr_defmod_when.get() == 1 )
		r_isok = True
		return r_isok, r_setting_value
	def GetSettingFromGui_PrntryMtchGrpDelOrder():
		r_setting_value = t01_cb_EnumDelOrder.getSelectedKey()
		r_isok = not ( r_setting_value is None )
		return r_isok, r_setting_value
	def GetSettingFromGui_PrntryDoCongruent():
		r_setting_value = ( t01_chkvr_cngrnt_bf_del.get() == 1 )
		r_isok = True
		return r_isok, r_setting_value
	def GetSettingFromGui_PrntryDoCngrntStringent():
		r_setting_value = ( t01_chkvr_cngrnt_strngnt.get() == 1 )
		r_isok = True
		return r_isok, r_setting_value
	def GetSettingFromGui_PrntryReportStringentDiffs():
		r_setting_value = ( t01_chkvr_cngrnt_fails_strngnt.get() == 1 )
		r_isok = True
		return r_isok, r_setting_value
	def GetSettingFromGui_PrntryDeletionMode():
		r_setting_value = t01_cb_EnumDeleteMode.getSelectedKey()
		r_isok = not ( r_setting_value is None )
		return r_isok, r_setting_value
	# Defoliatry
	def GetSettingFromGui_DfltryLstPaths():
		r_setting_value = t03_get_paths_list()
		r_isok = ( len( r_setting_value ) > 0 )
		return r_isok, r_setting_value
	def GetSettingFromGui_DfltryDoIgnoreZeroLength():
		r_setting_value = ( t03_chkvr_ignore0len.get() == 1 )
		r_isok = True
		return r_isok, r_setting_value
	def GetSettingFromGui_DfltryFileDoMatchByHash():
		r_setting_value = ( t03_chkvr_defmod_hash.get() == 1 )
		r_isok = True
		return r_isok, r_setting_value
	def GetSettingFromGui_DfltryFileDoMatchBySize():
		r_setting_value = ( t03_chkvr_defmod_size.get() == 1 )
		r_isok = True
		return r_isok, r_setting_value
	def GetSettingFromGui_DfltryFileDoMatchByName():
		r_setting_value = ( t03_chkvr_defmod_name.get() == 1 )
		r_isok = True
		return r_isok, r_setting_value
	def GetSettingFromGui_DfltryFileDoMatchByWhen():
		r_setting_value = ( t03_chkvr_defmod_when.get() == 1 )
		r_isok = True
		return r_isok, r_setting_value
	def GetSettingFromGui_DfltryMtchGrpDelOrder():
		r_setting_value = t03_cb_EnumDelOrder.getSelectedKey()
		r_isok = True
		return r_isok, r_setting_value
	def GetSettingFromGui_DfltryFileEnumKeepWhich():
		r_setting_value = t03_cb_EnumKeepWhich.getSelectedKey()
		r_isok = not ( r_setting_value is None )
		return r_isok, r_setting_value
	def GetSettingFromGui_DfltryDoUseDiscardMarkers():
		r_setting_value = ( t03_chkvr_usediscards.get() == 1 )
		r_isok = True
		return r_isok, r_setting_value
	def GetSettingFromGui_DfltryDoCongruent():
		r_setting_value = ( t03_chkvr_cngrnt_bf_del.get() == 1 )
		r_isok = True
		return r_isok, r_setting_value
	def GetSettingFromGui_DfltryDoCngrntStringent():
		r_setting_value = ( t03_chkvr_cngrnt_strngnt.get() == 1 )
		r_isok = True
		return r_isok, r_setting_value
	def GetSettingFromGui_DfltryReportStringentDiffs():
		r_setting_value = ( t03_chkvr_cngrnt_fails_strngnt.get() == 1 )
		r_isok = True
		return r_isok, r_setting_value
	def GetSettingFromGui_DfltryDeletionMode():
		r_setting_value = t03_cb_EnumDeleteMode.getSelectedKey()
		r_isok = not ( r_setting_value is None )
		return r_isok, r_setting_value
	# Trimatry
	def GetSettingFromGui_TrmtryLstPaths():
		r_setting_value = t16_get_folder_list()
		r_isok = ( len( r_setting_value ) > 0 )
		return r_isok, r_setting_value
	def GetSettingFromGui_TrmtryDoCanDelTopFolders():
		r_setting_value = (t16_chkvr_CanDeleteTopFolders.get() == 1 )
		r_isok = True
		return r_isok, r_setting_value
	def GetSettingFromGui_TrmtryDoNestedEmptyFolders():
		r_setting_value = (t16_chkvr_TreatNestedEmptyFolders.get() == 1 )
		r_isok = True
		return r_isok, r_setting_value
	def GetSettingFromGui_TrmtryDoTreatZeroAsNothing():
		r_setting_value = (t16_chkvr_TreatZeroesAsNotThere.get() == 1 )
		r_isok = True
		return r_isok, r_setting_value
	def GetSettingFromGui_TrmtryDoExceptKeepIfEmpty():
		r_setting_value = (t16_chkvr_TreatKeepIfEmptyAsPresent.get() == 1 )
		r_isok = True
		return r_isok, r_setting_value
	def GetSettingFromGui_TrmtryDoExceptNoMedia():
		r_setting_value = (t16_chkvr_TreatNoMediaAsPresent.get() == 1 )
		r_isok = True
		return r_isok, r_setting_value
	def GetSettingFromGui_TrmtryDoRenameInsteadOfDelete():
		r_setting_value = (t16_chkvr_RenameFoldersRatherThanDelete.get() == 1 )
		r_isok = True
		return r_isok, r_setting_value
	def GetSettingFromGui_TrmtryDeletionMode():
		r_setting_value = t16_cb_EnumDeleteMode.getSelectedKey()
		r_isok = not ( r_setting_value is None )
		return r_isok, r_setting_value
	def GetSettingFromGui_TrmtryDoDelFromDeepest():
		r_setting_value = (t16_chkvr_DelFromDeep.get() == 1 )
		r_isok = True
		return r_isok, r_setting_value
	# Clonatry
	def GetSettingFromGui_ClntryStrPathA():
		r_setting_value = t14_Clntry_var_path_A.get()
		r_isok = ( len( r_setting_value ) > 0 )
		return r_isok, r_setting_value
	def GetSettingFromGui_ClntryStrPathB():
		r_setting_value = t14_Clntry_var_path_B.get()
		r_isok = ( len( r_setting_value ) > 0 )
		return r_isok, r_setting_value
	def GetSettingFromGui_ClntryRecreateSourceFolder():
		r_setting_value = (t14_Clntry_chkvr_RecreateSrcFldr.get() == 1 )
		r_isok = True
		return r_isok, r_setting_value
	def GetSettingFromGui_ClntryDoKeepTouch():
		r_setting_value = (t14_Clntry_chkvr_KeepTuch.get() == 1 )
		r_isok = True
		return r_isok, r_setting_value
	def GetSettingFromGui_ClntryDoKeepAttrib():
		r_setting_value = (t14_Clntry_chkvr_KeepAttr.get() == 1 )
		r_isok = True
		return r_isok, r_setting_value
	def GetSettingFromGui_ClntryDoKeepPerm():
		r_setting_value = (t14_Clntry_chkvr_KeepPrmt.get() == 1 )
		r_isok = True
		return r_isok, r_setting_value
	def GetSettingFromGui_ClntryDoKeepMeta():
		r_setting_value = (t14_Clntry_chkvr_KeepMeta.get() == 1 )
		r_isok = True
		return r_isok, r_setting_value
	# Congruentry - Folders
	def GetSettingFromGui_CngrntryTwoPathAStrPath ():
		r_setting_value = t02_var_patha.get()
		r_isok = ( len( r_setting_value ) > 0 )
		return r_isok, r_setting_value
	def GetSettingFromGui_CngrntryTwoPathACustomCharEnc():
		r_setting_value = t02_fA_Cstm_cb_CharEnc.getSelectedKey()
		r_isok = not ( r_setting_value is None )
		return r_isok, r_setting_value
	def GetSettingFromGui_CngrntryTwoPathACustomDoCase():
		r_setting_value = (t02_fA_Cstm_chkvr_CaseSensitive.get() == 1 )
		r_isok = True
		return r_isok, r_setting_value
	def GetSettingFromGui_CngrntryTwoPathACustomDoMsName():
		r_setting_value = (t02_fA_Cstn_chkvr_BadName.get() == 1 )
		r_isok = True
		return r_isok, r_setting_value
	def GetSettingFromGui_CngrntryTwoPathACustomDoMsChar():
		r_setting_value = (t02_fA_Cstm_chkvr_BadChars.get() == 1 )
		r_isok = True
		return r_isok, r_setting_value
	def GetSettingFromGui_CngrntryTwoPathACustomDoMs8p3():
		r_setting_value = (t02_fA_Cstn_chkvr_Wind8p3.get() == 1 )
		r_isok = True
		return r_isok, r_setting_value
	def GetSettingFromGui_CngrntryTwoPathBStrPath():
		r_setting_value = t02_var_pathb.get()
		r_isok = ( len( r_setting_value ) > 0 )
		return r_isok, r_setting_value
	def GetSettingFromGui_CngrntryTwoPathBCustomCharEnc():
		r_setting_value = t02_fB_Cstm_cb_CharEnc.getSelectedKey()
		r_isok = not ( r_setting_value is None )
		return r_isok, r_setting_value
	def GetSettingFromGui_CngrntryTwoPathBCustomDoCase():
		r_setting_value = (t02_fB_Cstm_chkvr_CaseSensitive.get() == 1 )
		r_isok = True
		return r_isok, r_setting_value
	def GetSettingFromGui_CngrntryTwoPathBCustomDoMsName():
		r_setting_value = (t02_fB_Cstn_chkvr_BadName.get() == 1 )
		r_isok = True
		return r_isok, r_setting_value
	def GetSettingFromGui_CngrntryTwoPathBCustomDoMsChar():
		r_setting_value = (t02_fB_Cstm_chkvr_BadChars.get() == 1 )
		r_isok = True
		return r_isok, r_setting_value
	def GetSettingFromGui_CngrntryTwoPathBCustomDoMs8p3():
		r_setting_value = (t02_fB_Cstn_chkvr_Wind8p3.get() == 1 )
		r_isok = True
		return r_isok, r_setting_value
	def GetSettingFromGui_CngrntryAnalyseStringent():
		r_setting_value = ( t02_chkvr_Stringent.get() == 1 )
		r_isok = True
		return r_isok, r_setting_value
	def GetSettingFromGui_CngrntryStopNotCongruent():
		r_setting_value = ( t02_chkvr_StopNotCongruent.get() == 1 )
		r_isok = True
		return r_isok, r_setting_value
	def GetSettingFromGui_CngrntryStopNotMoiety():
		r_setting_value = ( t02_chkvr_StopNotMoiety.get() == 1 )
		r_isok = True
		return r_isok, r_setting_value
	def GetSettingFromGui_CngrntryReportDiffs():
		r_setting_value = ( t02_chkvr_ReportDiffs.get() == 1 )
		r_isok = True
		return r_isok, r_setting_value
	def GetSettingFromGui_CngrntryDiffSummary():
		r_setting_value = ( t02_chkvr_SummaryDiffs.get() == 1 )
		r_isok = True
		return r_isok, r_setting_value
	def GetSettingFromGui_CngrntryReportStringentDiffs():
		r_setting_value = ( t02_chkvr_cngrnt_fails_strngnt.get() == 1 )
		r_isok = True
		return r_isok, r_setting_value
	def GetSettingFromGui_CngrntryGenCopyScript():
		r_setting_value = ( t02_chkvr_GenCopyScript.get() == 1 )
		r_isok = True
		return r_isok, r_setting_value
	def GetSettingFromGui_CngrntryCopyScriptShell():
		r_setting_value = t02_cb_EnumGenCopyScriptAs.getSelectedKey()
		r_isok = True
		return r_isok, r_setting_value
	# Congruentry - Files
	def GetSettingFromGui_CngrntryTwoFileAStrFileref():
		r_setting_value = t02_var_filea.get()
		r_isok = ( len( r_setting_value ) > 0 )
		return r_isok, r_setting_value
	def GetSettingFromGui_CngrntryTwoFileBStrFileref():
		r_setting_value = t02_var_fileb.get()
		r_isok = ( len( r_setting_value ) > 0 )
		return r_isok, r_setting_value
	# Summatry
	def GetSettingFromGui_SmmtryLstPaths():
		r_setting_value = t05_get_folder_list()
		r_isok = ( len( r_setting_value ) > 0 )
		return r_isok, r_setting_value
	def GetSettingFromGui_SmmtryDoMsName():
		r_setting_value = (t05_chkvr_checkfor_badwinfname.get() == 1 )
		r_isok = True
		return r_isok, r_setting_value
	def GetSettingFromGui_SmmtryDoMsChar():
		r_setting_value = (t05_chkvr_checkfor_badwinchars.get() == 1 )
		r_isok = True
		return r_isok, r_setting_value
	# Other Settings 
	def GetSettingFromGui_SttngLstDiscardNameMarkers():
		r_setting_value = t06_t1_get_LstDiscardNameMarkers()
		r_isok = ( len( r_setting_value ) > 0 )
		return r_isok, r_setting_value
	# Logging
	def GetSettingFromGui_LggngStatus():
		r_setting_value = (t06_t5_chkvr_log_status.get() == 1 )
		r_isok = True
		return r_isok, r_setting_value
	def GetSettingFromGui_LggngResults():
		r_setting_value = (t06_t5_chkvr_log_result.get() == 1 )
		r_isok = True
		return r_isok, r_setting_value
	def GetSettingFromGui_LggngProcess():
		r_setting_value = (t06_t5_chkvr_log_process.get() == 1 )
		r_isok = True
		return r_isok, r_setting_value
	def GetSettingFromGui_LggngDetails():
		r_setting_value = (t06_t5_chkvr_log_detail.get() == 1 )
		r_isok = True
		return r_isok, r_setting_value
	def GetSettingFromGui_LggngDebug():
		r_setting_value = (t06_t5_chkvr_log_debug.get() == 1 )
		r_isok = True
		return r_isok, r_setting_value
	# ..................................................................
	# -- then, we have a function that can make a dictionary of all those functions
	# this replaces a tedious if elif function and even though at runtime this makes no significant difference
	# - the computer time is not that important
	# it meshes better with the ability to self-check that all the current Settings items are handled
	def Make_Dct_Fn_GetSettingFromGui():
		r_dct_Fn = {}
		# Prunatry
		r_dct_Fn[ apltry.EnumSettingDef.PrntryLstDisposalPaths ] = GetSettingFromGui_PrntryLstDisposalPaths
		r_dct_Fn[ apltry.EnumSettingDef.PrntryLstSurvivalPaths ] = GetSettingFromGui_PrntryLstSurvivalPaths
		r_dct_Fn[ apltry.EnumSettingDef.PrntryDoSuperMatch ] = GetSettingFromGui_PrntryDoSuperMatch
		r_dct_Fn[ apltry.EnumSettingDef.PrntryDoFolderMatch ] = GetSettingFromGui_PrntryDoFolderMatch
		r_dct_Fn[ apltry.EnumSettingDef.PrntryDoFileMatch ] = GetSettingFromGui_PrntryDoFileMatch
		r_dct_Fn[ apltry.EnumSettingDef.PrntryDoMatchStringent ] = GetSettingFromGui_PrntryDoMatchStringent
		r_dct_Fn[ apltry.EnumSettingDef.PrntryFileDoMatchByHash ] = GetSettingFromGui_PrntryFileDoMatchByHash
		r_dct_Fn[ apltry.EnumSettingDef.PrntryFileDoMatchBySize ] = GetSettingFromGui_PrntryFileDoMatchBySize
		r_dct_Fn[ apltry.EnumSettingDef.PrntryFileDoMatchByName ] = GetSettingFromGui_PrntryFileDoMatchByName
		r_dct_Fn[ apltry.EnumSettingDef.PrntryFileDoMatchByWhen ] = GetSettingFromGui_PrntryFileDoMatchByWhen
		r_dct_Fn[ apltry.EnumSettingDef.PrntryMtchGrpDelOrder ] = GetSettingFromGui_PrntryMtchGrpDelOrder
		r_dct_Fn[ apltry.EnumSettingDef.PrntryDoCongruent ] = GetSettingFromGui_PrntryDoCongruent
		r_dct_Fn[ apltry.EnumSettingDef.PrntryDoCngrntStringent ] = GetSettingFromGui_PrntryDoCngrntStringent
		r_dct_Fn[ apltry.EnumSettingDef.PrntryReportStringentDiffs ] = GetSettingFromGui_PrntryReportStringentDiffs
		r_dct_Fn[ apltry.EnumSettingDef.PrntryDeletionMode ] = GetSettingFromGui_PrntryDeletionMode
		# Defoliatry
		r_dct_Fn[ apltry.EnumSettingDef.DfltryLstPaths  ] = GetSettingFromGui_DfltryLstPaths 
		r_dct_Fn[ apltry.EnumSettingDef.DfltryDoIgnoreZeroLength  ] = GetSettingFromGui_DfltryDoIgnoreZeroLength 
		r_dct_Fn[ apltry.EnumSettingDef.DfltryFileDoMatchByHash ] = GetSettingFromGui_DfltryFileDoMatchByHash
		r_dct_Fn[ apltry.EnumSettingDef.DfltryFileDoMatchBySize  ] = GetSettingFromGui_DfltryFileDoMatchBySize 
		r_dct_Fn[ apltry.EnumSettingDef.DfltryFileDoMatchByName  ] = GetSettingFromGui_DfltryFileDoMatchByName 
		r_dct_Fn[ apltry.EnumSettingDef.DfltryFileDoMatchByWhen  ] = GetSettingFromGui_DfltryFileDoMatchByWhen 
		r_dct_Fn[ apltry.EnumSettingDef.DfltryMtchGrpDelOrder ] = GetSettingFromGui_DfltryMtchGrpDelOrder
		r_dct_Fn[ apltry.EnumSettingDef.DfltryFileEnumKeepWhich ] = GetSettingFromGui_DfltryFileEnumKeepWhich
		r_dct_Fn[ apltry.EnumSettingDef.DfltryDoUseDiscardMarkers  ] = GetSettingFromGui_DfltryDoUseDiscardMarkers 
		r_dct_Fn[ apltry.EnumSettingDef.DfltryDoCongruent  ] = GetSettingFromGui_DfltryDoCongruent 
		r_dct_Fn[ apltry.EnumSettingDef.DfltryDoCngrntStringent  ] = GetSettingFromGui_DfltryDoCngrntStringent 
		r_dct_Fn[ apltry.EnumSettingDef.DfltryReportStringentDiffs  ] = GetSettingFromGui_DfltryReportStringentDiffs 
		r_dct_Fn[ apltry.EnumSettingDef.DfltryDeletionMode ] = GetSettingFromGui_DfltryDeletionMode
		# Trimatry
		r_dct_Fn[ apltry.EnumSettingDef.TrmtryLstPaths ] = GetSettingFromGui_TrmtryLstPaths
		r_dct_Fn[ apltry.EnumSettingDef.TrmtryDoCanDelTopFolders ] = GetSettingFromGui_TrmtryDoCanDelTopFolders
		r_dct_Fn[ apltry.EnumSettingDef.TrmtryDoNestedEmptyFolders ] = GetSettingFromGui_TrmtryDoNestedEmptyFolders
		r_dct_Fn[ apltry.EnumSettingDef.TrmtryDoTreatZeroAsNothing ] = GetSettingFromGui_TrmtryDoTreatZeroAsNothing
		r_dct_Fn[ apltry.EnumSettingDef.TrmtryDoExceptKeepIfEmpty ] = GetSettingFromGui_TrmtryDoExceptKeepIfEmpty
		r_dct_Fn[ apltry.EnumSettingDef.TrmtryDoExceptNoMedia ] = GetSettingFromGui_TrmtryDoExceptNoMedia
		r_dct_Fn[ apltry.EnumSettingDef.TrmtryDoRenameInsteadOfDelete ] = GetSettingFromGui_TrmtryDoRenameInsteadOfDelete
		r_dct_Fn[ apltry.EnumSettingDef.TrmtryDeletionMode ] = GetSettingFromGui_TrmtryDeletionMode
		r_dct_Fn[ apltry.EnumSettingDef.TrmtryDoDelFromDeepest ] = GetSettingFromGui_TrmtryDoDelFromDeepest
		# Replatry
		# -- Clonatry
		r_dct_Fn[ apltry.EnumSettingDef.ClntryStrPathA ] = GetSettingFromGui_ClntryStrPathA
		r_dct_Fn[ apltry.EnumSettingDef.ClntryStrPathB ] = GetSettingFromGui_ClntryStrPathB
		r_dct_Fn[ apltry.EnumSettingDef.ClntryRecreateSourceFolder ] = GetSettingFromGui_ClntryRecreateSourceFolder
		r_dct_Fn[ apltry.EnumSettingDef.ClntryDoKeepTouch ] = GetSettingFromGui_ClntryDoKeepTouch
		r_dct_Fn[ apltry.EnumSettingDef.ClntryDoKeepAttrib ] = GetSettingFromGui_ClntryDoKeepAttrib
		r_dct_Fn[ apltry.EnumSettingDef.ClntryDoKeepPerm ] = GetSettingFromGui_ClntryDoKeepPerm
		r_dct_Fn[ apltry.EnumSettingDef.ClntryDoKeepMeta ] = GetSettingFromGui_ClntryDoKeepMeta
		# Congruentry - Paths
		r_dct_Fn[ apltry.EnumSettingDef.CngrntryTwoPathAStrPath  ] = GetSettingFromGui_CngrntryTwoPathAStrPath 
		r_dct_Fn[ apltry.EnumSettingDef.CngrntryTwoPathACustomCharEnc ] = GetSettingFromGui_CngrntryTwoPathACustomCharEnc
		r_dct_Fn[ apltry.EnumSettingDef.CngrntryTwoPathACustomDoCase ] = GetSettingFromGui_CngrntryTwoPathACustomDoCase
		r_dct_Fn[ apltry.EnumSettingDef.CngrntryTwoPathACustomDoMsName ] = GetSettingFromGui_CngrntryTwoPathACustomDoMsName
		r_dct_Fn[ apltry.EnumSettingDef.CngrntryTwoPathACustomDoMsChar ] = GetSettingFromGui_CngrntryTwoPathACustomDoMsChar
		r_dct_Fn[ apltry.EnumSettingDef.CngrntryTwoPathACustomDoMs8p3 ] = GetSettingFromGui_CngrntryTwoPathACustomDoMs8p3
		r_dct_Fn[ apltry.EnumSettingDef.CngrntryTwoPathBStrPath ] = GetSettingFromGui_CngrntryTwoPathBStrPath
		r_dct_Fn[ apltry.EnumSettingDef.CngrntryTwoPathBCustomCharEnc ] = GetSettingFromGui_CngrntryTwoPathBCustomCharEnc
		r_dct_Fn[ apltry.EnumSettingDef.CngrntryTwoPathBCustomDoCase ] = GetSettingFromGui_CngrntryTwoPathBCustomDoCase
		r_dct_Fn[ apltry.EnumSettingDef.CngrntryTwoPathBCustomDoMsName ] = GetSettingFromGui_CngrntryTwoPathBCustomDoMsName
		r_dct_Fn[ apltry.EnumSettingDef.CngrntryTwoPathBCustomDoMsChar ] = GetSettingFromGui_CngrntryTwoPathBCustomDoMsChar
		r_dct_Fn[ apltry.EnumSettingDef.CngrntryTwoPathBCustomDoMs8p3 ] = GetSettingFromGui_CngrntryTwoPathBCustomDoMs8p3
		r_dct_Fn[ apltry.EnumSettingDef.CngrntryAnalyseStringent ] = GetSettingFromGui_CngrntryAnalyseStringent
		r_dct_Fn[ apltry.EnumSettingDef.CngrntryStopNotCongruent ] = GetSettingFromGui_CngrntryStopNotCongruent
		r_dct_Fn[ apltry.EnumSettingDef.CngrntryStopNotMoiety ] = GetSettingFromGui_CngrntryStopNotMoiety
		r_dct_Fn[ apltry.EnumSettingDef.CngrntryReportDiffs ] = GetSettingFromGui_CngrntryReportDiffs
		r_dct_Fn[ apltry.EnumSettingDef.CngrntryDiffSummary ] = GetSettingFromGui_CngrntryDiffSummary
		r_dct_Fn[ apltry.EnumSettingDef.CngrntryReportStringentDiffs ] = GetSettingFromGui_CngrntryReportStringentDiffs
		r_dct_Fn[ apltry.EnumSettingDef.CngrntryGenCopyScript ] = GetSettingFromGui_CngrntryGenCopyScript
		r_dct_Fn[ apltry.EnumSettingDef.CngrntryCopyScriptShell ] = GetSettingFromGui_CngrntryCopyScriptShell
		# Congruentry - Files
		r_dct_Fn[ apltry.EnumSettingDef.CngrntryTwoFileAStrFileref ] = GetSettingFromGui_CngrntryTwoFileAStrFileref
		r_dct_Fn[ apltry.EnumSettingDef.CngrntryTwoFileBStrFileref ] = GetSettingFromGui_CngrntryTwoFileBStrFileref
		# Summatry
		r_dct_Fn[ apltry.EnumSettingDef.SmmtryLstPaths ] = GetSettingFromGui_SmmtryLstPaths
		r_dct_Fn[ apltry.EnumSettingDef.SmmtryDoMsName ] = GetSettingFromGui_SmmtryDoMsName
		r_dct_Fn[ apltry.EnumSettingDef.SmmtryDoMsChar ] = GetSettingFromGui_SmmtryDoMsChar
		# Other
		r_dct_Fn[ apltry.EnumSettingDef.SttngLstDiscardNameMarkers ] = GetSettingFromGui_SttngLstDiscardNameMarkers
		# Logging
		r_dct_Fn[ apltry.EnumSettingDef.LggngStatus ] = GetSettingFromGui_LggngStatus
		r_dct_Fn[ apltry.EnumSettingDef.LggngResults ] = GetSettingFromGui_LggngResults
		r_dct_Fn[ apltry.EnumSettingDef.LggngProcess ] = GetSettingFromGui_LggngProcess
		r_dct_Fn[ apltry.EnumSettingDef.LggngDetails ] = GetSettingFromGui_LggngDetails
		r_dct_Fn[ apltry.EnumSettingDef.LggngDebug ] = GetSettingFromGui_LggngDebug
		
		return r_dct_Fn
	# ..................................................................
	# -- now we create a single static dictionary by calling that function
	# note that this is not a "def" but is a static variable used for the life of this module
	_Dct_Fn_GetSettingFromGui = Make_Dct_Fn_GetSettingFromGui()
	# ..................................................................
	# -- now we provide a new function that uses the static dictionary instead of the old chain of if elif statements
	def GetSettingFromGui( p_EnumSettingDef ):
		return _Dct_Fn_GetSettingFromGui[ p_EnumSettingDef]()
	# $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
	# ----------------------------------------------------------------------
	# -- and here we can do a dummy run through of all the settings enumerations to affirm that we've implemented them all
	def Prove_New_GetSettingFromGui():
		for i_EnumSettingDef in apltry.EnumSettingDef:
			t_isok, t_setting_value = GetSettingFromGui( i_EnumSettingDef )
	# -- and here we can do a dummy run through of all the settings enumerations to affirm that we've implemented them all
	def Check_New_GetSettingFromGui():
		print( DevelopModeSectionString() )
		print( "Check_New_GetSettingFromGui" )
		for i_EnumSettingDef in apltry.EnumSettingDef:
			if i_EnumSettingDef in _Dct_Fn_GetSettingFromGui :
				t_isok, t_setting_value = GetSettingFromGui( i_EnumSettingDef )
			else :
				print( "Missing a Settings enumeration in New_GetSettingFromGui" )
				print( i_EnumSettingDef )
				print( DevelopModeAnteCodeString() )
				print( im_codegen.ForSettings_Generate_Python_Function_GetSetting( i_EnumSettingDef ) )
				print( DevelopModePostCodeString() )
				print( DevelopModeAnteCodeString() )
				print( im_codegen.ForSettings_Generate_Python_Line_GetSetting_AssignFunction( i_EnumSettingDef ) )
				print( DevelopModePostCodeString() )
				print( DevelopModeAnteCodeString() )
				print("# for pasting into def command_line_actions")
				print( im_codegen.ForSettings_Generate_Python_Function_GetSetting_ForCommandLine( i_EnumSettingDef ) )
				print( DevelopModePostCodeString() )
	# ----------------------------------------------------------------------
	# $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
	# ------------------------------------------------------------------
	# Functions to support applying Settings to the GUI widgetd
	# ------------------------------------------------------------------
	# First, we make a custom function for each widget
	# while these are _largely_ the same, they provide a consistent call 
	# for what are really specific for each - the name echoes the matching enumeration
	def SetSettingIntoGui_PrntryLstDisposalPaths( p_val ):
		t01_set_disposal_list( p_val )
	def SetSettingIntoGui_PrntryLstSurvivalPaths( p_val ):
		t01_set_survival_list( p_val )
	def SetSettingIntoGui_PrntryDoSuperMatch( p_val ):
		t01_chkvr_super.set( p_val )
	def SetSettingIntoGui_PrntryDoFolderMatch( p_val ):
		t01_chkvr_fldrs.set( p_val )
	def SetSettingIntoGui_PrntryDoFileMatch( p_val ):
		t01_chkvr_files.set( p_val )
	def SetSettingIntoGui_PrntryDoMatchStringent( p_val ):
		t01_chkvr_stringent.set( p_val )
	def SetSettingIntoGui_PrntryFileDoMatchByHash( p_val ):
		t01_chkvr_defmod_hash.set( p_val )
	def SetSettingIntoGui_PrntryFileDoMatchBySize( p_val ):
		t01_chkvr_defmod_size.set( p_val )
	def SetSettingIntoGui_PrntryFileDoMatchByName( p_val ):
		t01_chkvr_defmod_name.set( p_val )
	def SetSettingIntoGui_PrntryFileDoMatchByWhen( p_val ):
		t01_chkvr_defmod_when.set( p_val )
	def SetSettingIntoGui_PrntryMtchGrpDelOrder( p_val ):
		t01_cb_EnumDelOrder.setSelectedKey( p_val )
	def SetSettingIntoGui_PrntryDoCongruent( p_val ):
		t01_chkvr_cngrnt_bf_del.set( p_val )
	def SetSettingIntoGui_PrntryDoCngrntStringent( p_val ):
		t01_chkvr_cngrnt_strngnt.set( p_val )
	def SetSettingIntoGui_PrntryReportStringentDiffs( p_val ):
		t01_chkvr_cngrnt_fails_strngnt.set( p_val )
	def SetSettingIntoGui_PrntryDeletionMode( p_val ):
		t01_cb_EnumDeleteMode.setSelectedKey( p_val )
	def SetSettingIntoGui_DfltryLstPaths( p_val ):
		t03_set_paths_list( p_val )
	def SetSettingIntoGui_DfltryDoIgnoreZeroLength( p_val ):
		t03_chkvr_ignore0len.set( p_val )
	def SetSettingIntoGui_DfltryFileDoMatchByHash( p_val ):
		t03_chkvr_defmod_hash.set( p_val )
	def SetSettingIntoGui_DfltryFileDoMatchBySize( p_val ):
		t03_chkvr_defmod_size.set( p_val )
	def SetSettingIntoGui_DfltryFileDoMatchByName( p_val ):
		t03_chkvr_defmod_name.set( p_val )
	def SetSettingIntoGui_DfltryFileDoMatchByWhen( p_val ):
		t03_chkvr_defmod_when.set( p_val )
	def SetSettingIntoGui_DfltryMtchGrpDelOrder( p_val ):
		t03_cb_EnumDelOrder.setSelectedKey( p_val )
	def SetSettingIntoGui_DfltryFileEnumKeepWhich( p_val ):
		t03_cb_EnumKeepWhich.setSelectedKey( p_val )
	def SetSettingIntoGui_DfltryDoUseDiscardMarkers( p_val ):
		t03_chkvr_usediscards.set( p_val )
	def SetSettingIntoGui_DfltryDoCongruent( p_val ):
		t03_chkvr_cngrnt_bf_del.set( p_val )
	def SetSettingIntoGui_DfltryDoCngrntStringent( p_val ):
		t03_chkvr_cngrnt_strngnt.set( p_val )
	def SetSettingIntoGui_DfltryReportStringentDiffs( p_val ):
		t03_chkvr_cngrnt_fails_strngnt.set( p_val )
	def SetSettingIntoGui_DfltryDeletionMode( p_val ):
		t03_cb_EnumDeleteMode.setSelectedKey( p_val )
	def SetSettingIntoGui_TrmtryLstPaths( p_val ):
		t16_set_folder_list( p_val )
	def SetSettingIntoGui_TrmtryDoCanDelTopFolders( p_val ):
		t16_chkvr_CanDeleteTopFolders.set( p_val )
	def SetSettingIntoGui_TrmtryDoNestedEmptyFolders( p_val ):
		t16_chkvr_TreatNestedEmptyFolders.set( p_val )
	def SetSettingIntoGui_TrmtryDoTreatZeroAsNothing( p_val ):
		t16_chkvr_TreatZeroesAsNotThere.set( p_val )
	def SetSettingIntoGui_TrmtryDoExceptKeepIfEmpty( p_val ):
		t16_chkvr_TreatKeepIfEmptyAsPresent.set( p_val )
	def SetSettingIntoGui_TrmtryDoExceptNoMedia( p_val ):
		t16_chkvr_TreatNoMediaAsPresent.set( p_val )
	def SetSettingIntoGui_TrmtryDoRenameInsteadOfDelete( p_val ):
		t16_chkvr_RenameFoldersRatherThanDelete.set( p_val )
	def SetSettingIntoGui_TrmtryDeletionMode( p_val ):
		t16_cb_EnumDeleteMode.setSelectedKey( p_val )
	def SetSettingIntoGui_TrmtryDoDelFromDeepest( p_val ):
		t16_chkvr_DelFromDeep.set( p_val )
	def SetSettingIntoGui_ClntryStrPathA( p_val ):
		t14_Clntry_var_path_A.set( p_val )
	def SetSettingIntoGui_ClntryStrPathB( p_val ):
		t14_Clntry_var_path_B.set( p_val )
	def SetSettingIntoGui_ClntryRecreateSourceFolder( p_val ):
		t14_Clntry_chkvr_RecreateSrcFldr.set( p_val )
	def SetSettingIntoGui_ClntryDoKeepTouch( p_val ):
		t14_Clntry_chkvr_KeepTuch.set( p_val )
	def SetSettingIntoGui_ClntryDoKeepAttrib( p_val ):
		t14_Clntry_chkvr_KeepAttr.set( p_val )
	def SetSettingIntoGui_ClntryDoKeepPerm( p_val ):
		t14_Clntry_chkvr_KeepPrmt.set( p_val )
	def SetSettingIntoGui_ClntryDoKeepMeta( p_val ):
		t14_Clntry_chkvr_KeepMeta.set( p_val )
	def SetSettingIntoGui_CngrntryTwoPathAStrPath( p_val ):
		t02_var_patha.set( p_val )
	def SetSettingIntoGui_CngrntryTwoPathACustomCharEnc( p_val ):
		t02_fA_Cstm_cb_CharEnc.setSelectedKey( p_val )
	def SetSettingIntoGui_CngrntryTwoPathACustomDoCase( p_val ):
		t02_fA_Cstm_chkvr_CaseSensitive.set( p_val )
	def SetSettingIntoGui_CngrntryTwoPathACustomDoMsName( p_val ):
		t02_fA_Cstn_chkvr_BadName.set( p_val )
	def SetSettingIntoGui_CngrntryTwoPathACustomDoMsChar( p_val ):
		t02_fA_Cstm_chkvr_BadChars.set( p_val )
	def SetSettingIntoGui_CngrntryTwoPathACustomDoMs8p3( p_val ):
		t02_fA_Cstn_chkvr_Wind8p3.set( p_val )
	def SetSettingIntoGui_CngrntryTwoPathBStrPath( p_val ):
		t02_var_pathb.set( p_val )
	def SetSettingIntoGui_CngrntryTwoPathBCustomCharEnc( p_val ):
		t02_fB_Cstm_cb_CharEnc.setSelectedKey( p_val )
	def SetSettingIntoGui_CngrntryTwoPathBCustomDoCase( p_val ):
		t02_fB_Cstm_chkvr_CaseSensitive.set( p_val )
	def SetSettingIntoGui_CngrntryTwoPathBCustomDoMsName( p_val ):
		t02_fB_Cstn_chkvr_BadName.set( p_val )
	def SetSettingIntoGui_CngrntryTwoPathBCustomDoMsChar( p_val ):
		t02_fB_Cstm_chkvr_BadChars.set( p_val )
	def SetSettingIntoGui_CngrntryTwoPathBCustomDoMs8p3( p_val ):
		t02_fB_Cstn_chkvr_Wind8p3.set( p_val )
	def SetSettingIntoGui_CngrntryAnalyseStringent( p_val ):
		t02_chkvr_Stringent.set( p_val )
	def SetSettingIntoGui_CngrntryStopNotCongruent( p_val ):
		t02_chkvr_StopNotCongruent.set( p_val )
	def SetSettingIntoGui_CngrntryStopNotMoiety( p_val ):
		t02_chkvr_StopNotMoiety.set( p_val )
	def SetSettingIntoGui_CngrntryReportDiffs( p_val ):
		t02_chkvr_ReportDiffs.set( p_val )
	def SetSettingIntoGui_CngrntryDiffSummary( p_val ):
		t02_chkvr_SummaryDiffs.set( p_val )
	def SetSettingIntoGui_CngrntryReportStringentDiffs( p_val ):
		t02_chkvr_cngrnt_fails_strngnt.set( p_val )
	def SetSettingIntoGui_CngrntryGenCopyScript( p_val ):
		t02_chkvr_GenCopyScript.set( p_val )
	def SetSettingIntoGui_CngrntryCopyScriptShell( p_val ):
		t02_cb_EnumGenCopyScriptAs.setSelectedKey( p_val )
	def SetSettingIntoGui_CngrntryTwoFileAStrFileref( p_val ):
		t02_var_filea.set( p_val )
	def SetSettingIntoGui_CngrntryTwoFileBStrFileref( p_val ):
		t02_var_fileb.set( p_val )
	def SetSettingIntoGui_SmmtryLstPaths( p_val ):
		t05_set_folder_list( p_val )
	def SetSettingIntoGui_SmmtryDoMsName( p_val ):
		t05_chkvr_checkfor_badwinfname.set( p_val )
	def SetSettingIntoGui_SmmtryDoMsChar( p_val ):
		t05_chkvr_checkfor_badwinchars.set( p_val )
	def SetSettingIntoGui_SttngLstDiscardNameMarkers( p_val ):
		t06_t1_set_LstDiscardNameMarkers( p_val )
	def SetSettingIntoGui_LggngStatus( p_val ):
		t06_t5_chkvr_log_status.set( p_val )
	def SetSettingIntoGui_LggngResults( p_val ):
		t06_t5_chkvr_log_result.set( p_val )
	def SetSettingIntoGui_LggngProcess( p_val ):
		t06_t5_chkvr_log_process.set( p_val )
	def SetSettingIntoGui_LggngDetails( p_val ):
		t06_t5_chkvr_log_detail.set( p_val )
	def SetSettingIntoGui_LggngDebug( p_val ):
		t06_t5_chkvr_log_debug.set( p_val )
	# ..................................................................
	# -- then, we have a function that can make a dictionary of all those functions
	# this replaces a tedious if elif function and even though at runtime this makes no significant difference
	# - the computer time is not that important
	# it meshes better with the ability to self-check that all the current Settings items are handled
	def Make_Dct_Fn_SetSettingIntoGui():
		r_dct_Fn = {}
		r_dct_Fn[ apltry.EnumSettingDef.PrntryLstDisposalPaths ] = SetSettingIntoGui_PrntryLstDisposalPaths
		r_dct_Fn[ apltry.EnumSettingDef.PrntryLstSurvivalPaths ] = SetSettingIntoGui_PrntryLstSurvivalPaths
		r_dct_Fn[ apltry.EnumSettingDef.PrntryDoSuperMatch ] = SetSettingIntoGui_PrntryDoSuperMatch
		r_dct_Fn[ apltry.EnumSettingDef.PrntryDoFolderMatch ] = SetSettingIntoGui_PrntryDoFolderMatch
		r_dct_Fn[ apltry.EnumSettingDef.PrntryDoFileMatch ] = SetSettingIntoGui_PrntryDoFileMatch
		r_dct_Fn[ apltry.EnumSettingDef.PrntryDoMatchStringent ] = SetSettingIntoGui_PrntryDoMatchStringent
		r_dct_Fn[ apltry.EnumSettingDef.PrntryFileDoMatchByHash ] = SetSettingIntoGui_PrntryFileDoMatchByHash
		r_dct_Fn[ apltry.EnumSettingDef.PrntryFileDoMatchBySize ] = SetSettingIntoGui_PrntryFileDoMatchBySize
		r_dct_Fn[ apltry.EnumSettingDef.PrntryFileDoMatchByName ] = SetSettingIntoGui_PrntryFileDoMatchByName
		r_dct_Fn[ apltry.EnumSettingDef.PrntryFileDoMatchByWhen ] = SetSettingIntoGui_PrntryFileDoMatchByWhen
		r_dct_Fn[ apltry.EnumSettingDef.PrntryMtchGrpDelOrder ] = SetSettingIntoGui_PrntryMtchGrpDelOrder
		r_dct_Fn[ apltry.EnumSettingDef.PrntryDoCongruent ] = SetSettingIntoGui_PrntryDoCongruent
		r_dct_Fn[ apltry.EnumSettingDef.PrntryDoCngrntStringent ] = SetSettingIntoGui_PrntryDoCngrntStringent
		r_dct_Fn[ apltry.EnumSettingDef.PrntryReportStringentDiffs ] = SetSettingIntoGui_PrntryReportStringentDiffs
		r_dct_Fn[ apltry.EnumSettingDef.PrntryDeletionMode ] = SetSettingIntoGui_PrntryDeletionMode
		r_dct_Fn[ apltry.EnumSettingDef.DfltryLstPaths ] = SetSettingIntoGui_DfltryLstPaths
		r_dct_Fn[ apltry.EnumSettingDef.DfltryDoIgnoreZeroLength ] = SetSettingIntoGui_DfltryDoIgnoreZeroLength
		r_dct_Fn[ apltry.EnumSettingDef.DfltryFileDoMatchByHash ] = SetSettingIntoGui_DfltryFileDoMatchByHash
		r_dct_Fn[ apltry.EnumSettingDef.DfltryFileDoMatchBySize ] = SetSettingIntoGui_DfltryFileDoMatchBySize
		r_dct_Fn[ apltry.EnumSettingDef.DfltryFileDoMatchByName ] = SetSettingIntoGui_DfltryFileDoMatchByName
		r_dct_Fn[ apltry.EnumSettingDef.DfltryFileDoMatchByWhen ] = SetSettingIntoGui_DfltryFileDoMatchByWhen
		r_dct_Fn[ apltry.EnumSettingDef.DfltryMtchGrpDelOrder ] = SetSettingIntoGui_DfltryMtchGrpDelOrder
		r_dct_Fn[ apltry.EnumSettingDef.DfltryFileEnumKeepWhich ] = SetSettingIntoGui_DfltryFileEnumKeepWhich
		r_dct_Fn[ apltry.EnumSettingDef.DfltryDoUseDiscardMarkers ] = SetSettingIntoGui_DfltryDoUseDiscardMarkers
		r_dct_Fn[ apltry.EnumSettingDef.DfltryDoCongruent ] = SetSettingIntoGui_DfltryDoCongruent
		r_dct_Fn[ apltry.EnumSettingDef.DfltryDoCngrntStringent ] = SetSettingIntoGui_DfltryDoCngrntStringent
		r_dct_Fn[ apltry.EnumSettingDef.DfltryReportStringentDiffs ] = SetSettingIntoGui_DfltryReportStringentDiffs
		r_dct_Fn[ apltry.EnumSettingDef.DfltryDeletionMode ] = SetSettingIntoGui_DfltryDeletionMode
		r_dct_Fn[ apltry.EnumSettingDef.TrmtryLstPaths ] = SetSettingIntoGui_TrmtryLstPaths
		r_dct_Fn[ apltry.EnumSettingDef.TrmtryDoCanDelTopFolders ] = SetSettingIntoGui_TrmtryDoCanDelTopFolders
		r_dct_Fn[ apltry.EnumSettingDef.TrmtryDoNestedEmptyFolders ] = SetSettingIntoGui_TrmtryDoNestedEmptyFolders
		r_dct_Fn[ apltry.EnumSettingDef.TrmtryDoTreatZeroAsNothing ] = SetSettingIntoGui_TrmtryDoTreatZeroAsNothing
		r_dct_Fn[ apltry.EnumSettingDef.TrmtryDoExceptKeepIfEmpty ] = SetSettingIntoGui_TrmtryDoExceptKeepIfEmpty
		r_dct_Fn[ apltry.EnumSettingDef.TrmtryDoExceptNoMedia ] = SetSettingIntoGui_TrmtryDoExceptNoMedia
		r_dct_Fn[ apltry.EnumSettingDef.TrmtryDoRenameInsteadOfDelete ] = SetSettingIntoGui_TrmtryDoRenameInsteadOfDelete
		r_dct_Fn[ apltry.EnumSettingDef.TrmtryDeletionMode ] = SetSettingIntoGui_TrmtryDeletionMode
		r_dct_Fn[ apltry.EnumSettingDef.TrmtryDoDelFromDeepest ] = SetSettingIntoGui_TrmtryDoDelFromDeepest
		r_dct_Fn[ apltry.EnumSettingDef.ClntryStrPathA ] = SetSettingIntoGui_ClntryStrPathA
		r_dct_Fn[ apltry.EnumSettingDef.ClntryStrPathB ] = SetSettingIntoGui_ClntryStrPathB
		r_dct_Fn[ apltry.EnumSettingDef.ClntryRecreateSourceFolder ] = SetSettingIntoGui_ClntryRecreateSourceFolder
		r_dct_Fn[ apltry.EnumSettingDef.ClntryDoKeepTouch ] = SetSettingIntoGui_ClntryDoKeepTouch
		r_dct_Fn[ apltry.EnumSettingDef.ClntryDoKeepAttrib ] = SetSettingIntoGui_ClntryDoKeepAttrib
		r_dct_Fn[ apltry.EnumSettingDef.ClntryDoKeepPerm ] = SetSettingIntoGui_ClntryDoKeepPerm
		r_dct_Fn[ apltry.EnumSettingDef.ClntryDoKeepMeta ] = SetSettingIntoGui_ClntryDoKeepMeta
		r_dct_Fn[ apltry.EnumSettingDef.CngrntryTwoPathAStrPath ] = SetSettingIntoGui_CngrntryTwoPathAStrPath
		r_dct_Fn[ apltry.EnumSettingDef.CngrntryTwoPathACustomCharEnc ] = SetSettingIntoGui_CngrntryTwoPathACustomCharEnc
		r_dct_Fn[ apltry.EnumSettingDef.CngrntryTwoPathACustomDoCase ] = SetSettingIntoGui_CngrntryTwoPathACustomDoCase
		r_dct_Fn[ apltry.EnumSettingDef.CngrntryTwoPathACustomDoMsName ] = SetSettingIntoGui_CngrntryTwoPathACustomDoMsName
		r_dct_Fn[ apltry.EnumSettingDef.CngrntryTwoPathACustomDoMsChar ] = SetSettingIntoGui_CngrntryTwoPathACustomDoMsChar
		r_dct_Fn[ apltry.EnumSettingDef.CngrntryTwoPathACustomDoMs8p3 ] = SetSettingIntoGui_CngrntryTwoPathACustomDoMs8p3
		r_dct_Fn[ apltry.EnumSettingDef.CngrntryTwoPathBStrPath ] = SetSettingIntoGui_CngrntryTwoPathBStrPath
		r_dct_Fn[ apltry.EnumSettingDef.CngrntryTwoPathBCustomCharEnc ] = SetSettingIntoGui_CngrntryTwoPathBCustomCharEnc
		r_dct_Fn[ apltry.EnumSettingDef.CngrntryTwoPathBCustomDoCase ] = SetSettingIntoGui_CngrntryTwoPathBCustomDoCase
		r_dct_Fn[ apltry.EnumSettingDef.CngrntryTwoPathBCustomDoMsName ] = SetSettingIntoGui_CngrntryTwoPathBCustomDoMsName
		r_dct_Fn[ apltry.EnumSettingDef.CngrntryTwoPathBCustomDoMsChar ] = SetSettingIntoGui_CngrntryTwoPathBCustomDoMsChar
		r_dct_Fn[ apltry.EnumSettingDef.CngrntryTwoPathBCustomDoMs8p3 ] = SetSettingIntoGui_CngrntryTwoPathBCustomDoMs8p3
		r_dct_Fn[ apltry.EnumSettingDef.CngrntryAnalyseStringent ] = SetSettingIntoGui_CngrntryAnalyseStringent
		r_dct_Fn[ apltry.EnumSettingDef.CngrntryStopNotCongruent ] = SetSettingIntoGui_CngrntryStopNotCongruent
		r_dct_Fn[ apltry.EnumSettingDef.CngrntryStopNotMoiety ] = SetSettingIntoGui_CngrntryStopNotMoiety
		r_dct_Fn[ apltry.EnumSettingDef.CngrntryReportDiffs ] = SetSettingIntoGui_CngrntryReportDiffs
		r_dct_Fn[ apltry.EnumSettingDef.CngrntryDiffSummary ] = SetSettingIntoGui_CngrntryDiffSummary
		r_dct_Fn[ apltry.EnumSettingDef.CngrntryReportStringentDiffs ] = SetSettingIntoGui_CngrntryReportStringentDiffs
		r_dct_Fn[ apltry.EnumSettingDef.CngrntryGenCopyScript ] = SetSettingIntoGui_CngrntryGenCopyScript
		r_dct_Fn[ apltry.EnumSettingDef.CngrntryCopyScriptShell ] = SetSettingIntoGui_CngrntryCopyScriptShell
		r_dct_Fn[ apltry.EnumSettingDef.CngrntryTwoFileAStrFileref ] = SetSettingIntoGui_CngrntryTwoFileAStrFileref
		r_dct_Fn[ apltry.EnumSettingDef.CngrntryTwoFileBStrFileref ] = SetSettingIntoGui_CngrntryTwoFileBStrFileref
		r_dct_Fn[ apltry.EnumSettingDef.SmmtryLstPaths ] = SetSettingIntoGui_SmmtryLstPaths
		r_dct_Fn[ apltry.EnumSettingDef.SmmtryDoMsName ] = SetSettingIntoGui_SmmtryDoMsName
		r_dct_Fn[ apltry.EnumSettingDef.SmmtryDoMsChar ] = SetSettingIntoGui_SmmtryDoMsChar
		r_dct_Fn[ apltry.EnumSettingDef.SttngLstDiscardNameMarkers ] = SetSettingIntoGui_SttngLstDiscardNameMarkers
		r_dct_Fn[ apltry.EnumSettingDef.LggngStatus ] = SetSettingIntoGui_LggngStatus
		r_dct_Fn[ apltry.EnumSettingDef.LggngResults ] = SetSettingIntoGui_LggngResults
		r_dct_Fn[ apltry.EnumSettingDef.LggngProcess ] = SetSettingIntoGui_LggngProcess
		r_dct_Fn[ apltry.EnumSettingDef.LggngDetails ] = SetSettingIntoGui_LggngDetails
		r_dct_Fn[ apltry.EnumSettingDef.LggngDebug ] = SetSettingIntoGui_LggngDebug
		return r_dct_Fn
	# ..................................................................
	# -- now we create a single static dictionary by calling that function
	# note that this is not a "def" but is a static variable used for the life of this module
	_Dct_Fn_SetSettingIntoGui = Make_Dct_Fn_SetSettingIntoGui()
	# -- now we provide a new function that uses the static dictionary instead of the old chain of if elif statements
	def SetSettingIntoGui( p_EnumSettingDef, p_val ):
		_Dct_Fn_SetSettingIntoGui[ p_EnumSettingDef]( p_val )
	# $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
	# ----------------------------------------------------------------------
	# -- and here we can do a dummy run through of all the settings enumerations to affirm that we've implemented them all
	def Prove_New_SetSettingIntoGui():
		for i_EnumSettingDef in apltry.EnumSettingDef:
			# pull out the current setting
			t_ok, t_val = _Dct_Fn_GetSettingFromGui[ i_EnumSettingDef]()
			# and then set it as the setting
			_Dct_Fn_SetSettingIntoGui[ i_EnumSettingDef]( t_val )
	# -- and here we can do a dummy run through of all the settings enumerations to affirm that we've implemented them all
	def Check_New_SetSettingIntoGui():
		print( DevelopModeSectionString() )
		print( "Check_New_SetSettingIntoGui" )
		i_lst_enums_to_print = []
		for i_EnumSettingDef in apltry.EnumSettingDef:
			#print( "i_EnumSettingDef" )
			#print( i_EnumSettingDef )
			if i_EnumSettingDef in _Dct_Fn_SetSettingIntoGui :
				# pull out the current setting
				t_ok, t_val = _Dct_Fn_GetSettingFromGui[ i_EnumSettingDef]()
				# and then set it as the setting
				_Dct_Fn_SetSettingIntoGui[ i_EnumSettingDef]( t_val )
			else :
				print( "Missing a Settings enumeration in New_SetSettingIntoGui")
				print( i_EnumSettingDef )
				i_lst_enums_to_print.append( i_EnumSettingDef )
		for i_enum_to_print in i_lst_enums_to_print:
			print( DevelopModeAnteCodeString() )
			print( im_codegen.ForSettings_Generate_Python_Function_SetSetting( i_enum_to_print ) )
			print( DevelopModePostCodeString() )
		for i_enum_to_print in i_lst_enums_to_print:
			print( DevelopModeAnteCodeString() )
			print( im_codegen.ForSettings_Generate_Python_Line_SetSetting_AssignFunction( i_enum_to_print ) )
			print( DevelopModePostCodeString() )
	# ----------------------------------------------------------------------
	# $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
	# ------- Saved Settings ----------------
	# -------------- Auto Saved Settings ----------------
	def AutoSave_Current_Settings():
		# Note that this really should only be called just before program exit
		# because it will turn on all the Settings flags
		# check that we have a place and filename for saving to
		#print("AutoSave_Current_Settings")
		t_fileref = cmntry.cfgf_set_autosave_config_fileref() # this find and/or construct a fileref ready for saving to
		if not t_fileref is None and len( t_fileref ) > 0 :
			# set all the flags to True
			for sk in apltry.EnumSettingDef :
				FldtryWndw_GuiSettings.SettingFlag_SetFlag( sk, True )
			# collect the flagged settings  
			for sk in apltry.EnumSettingDef :
				isok, sv = GetSettingFromGui( sk )
				if isok:
					FldtryWndw_GuiSettings.Set_SettingValue( sk, sv )
			# call for the save with the filename
			FldtryWndw_GuiSettings.SaveFlaggedSettingsIntoFile( t_fileref )
			t06_t4_label_settings_alert.configure( text = "Saved: " + t_fileref)
			return True, t_fileref
		else:
			return False, ""
	def DetectThenProcess_AutoSaved_Settings():
		# now load settings from the saved defaults if it exists
		#print("DetectThenProcess_AutoSaved_Settings")
		t_autosave_settings_fileref = cmntry.cfgf_get_autosave_config_fileref()
		#print( "t_autosave_settings_fileref" )
		#print( t_autosave_settings_fileref )
		if not t_autosave_settings_fileref is None and len( t_autosave_settings_fileref) > 0 :
			#print( "Calling FldtryWndw_GuiSettings.LoadSettingsFromFle()")
			FldtryWndw_GuiSettings.LoadSettingsFromFle( t_autosave_settings_fileref )
			for sk in apltry.EnumSettingDef :
				#print( "sk")
				#print( sk)
				#print( "FldtryWndw_GuiSettings.Get_SettingValue( sk )" )
				#print( FldtryWndw_GuiSettings.Get_SettingValue( sk ) )
				#print( "Calling SetSettingIntoGui( sk, FldtryWndw_GuiSettings.Get_SettingValue( sk ) )" )
				SetSettingIntoGui( sk, FldtryWndw_GuiSettings.Get_SettingValue( sk ) )
			return True
		return False
	def Delete_AutoSaved_Settings():
		t_fileref = cmntry.cfgf_get_autosave_config_fileref()
		if not t_fileref is None and len( t_fileref ) > 0 :
			t_didok = purgetry_remove_file_now( t_fileref )
			if t_didok :
				return True, t_fileref
		return False, ""
	# -------------- Default Saved Settings ----------------
	def Save_Current_Settings_ToBeDefault():
		#print("Save_Current_Settings_ToBeDefault")
		t_fileref = cmntry.cfgf_set_default_config_fileref() # this find and/or construct a fileref ready for saving to
		if not t_fileref is None and len( t_fileref ) > 0 :
			# work out which settings to save according to the check boxes 
			UpdateSettingFlagsFromGuiCheckboxes()
			none_yet = False
			for sk in apltry.EnumSettingDef :
				i = sk.value
				FldtryWndw_GuiSettings.Settings_Flags[ sk] = ( t06_t4_dct_cbv[i].get() == 1 )
				none_yet = none_yet and FldtryWndw_GuiSettings.Settings_Flags[ sk]
			# collect the flagged settings  
			for sk in apltry.EnumSettingDef :
				isok, sv = GetSettingFromGui( sk )
				if isok:
					FldtryWndw_GuiSettings.Set_SettingValue( sk, sv )
			FldtryWndw_GuiSettings.SaveFlaggedSettingsIntoFile( t_fileref )
			return True, t_fileref
		else:
			return False, ""
	def DetectThenProcess_Default_Settings():
		#print("DetectThenProcess_Default_Settings")
		# now load settings from the saved defaults if it exists
		t_default_settings_fileref = cmntry.cfgf_get_default_config_fileref()
		#print( "t_default_settings_fileref" )
		#print( t_default_settings_fileref )
		if not t_default_settings_fileref is None and len( t_default_settings_fileref) > 0 :
			#print( "Calling FldtryWndw_GuiSettings.LoadSettingsFromFle()")
			FldtryWndw_GuiSettings.LoadSettingsFromFle( t_default_settings_fileref )
			#print( "Applying Loaded Settings into GUI objects")
			for i_EnumSettingDef in apltry.EnumSettingDef :
				#print( "i_EnumSettingDef")
				#print( i_EnumSettingDef)
				#print( "FldtryWndw_GuiSettings.Get_SettingValue( i_EnumSettingDef )" )
				#print( FldtryWndw_GuiSettings.Get_SettingValue( i_EnumSettingDef ) )
				#print( "Calling SetSettingIntoGui( i_EnumSettingDef, FldtryWndw_GuiSettings.Get_SettingValue( i_EnumSettingDef ) )" )
				SetSettingIntoGui( i_EnumSettingDef, FldtryWndw_GuiSettings.Get_SettingValue( i_EnumSettingDef ) )
			return True
		return False
	def Delete_Default_Settings():
		t_fileref = cmntry.cfgf_get_default_config_fileref()
		if not t_fileref is None and len( t_fileref ) > 0 :
			t_didok = purgetry_remove_file_now( t_fileref )
			if t_didok :
				return True, t_fileref
		return False, ""
	# -------------- Any Saved Settings ----------------
	def DetectThenProcess_AnySaved_Settings():
		t_loaded_autosave = DetectThenProcess_AutoSaved_Settings()
		if t_loaded_autosave :
			# set the AutoSave flag on
			t06_t4_chkvr_autosave.set( 1 )
		else:
			t_loaded_default = DetectThenProcess_Default_Settings()
	# ==================================================================
	# ----------------------- GUI interaction defs
	# common among actions
	def t01_disposal_path_addpath_if_okto( p_path):
		added_path = False
		if ( t01_chkvr_autoclashcheck_disposals.get() == 1 ) :
			clash_disposal, made_sub_disposal, clash_survival, made_sub_survival = \
				t01_check_path_for_clashes( p_path )
			if (not clash_disposal) and (not clash_survival) :
				t01_lb_disposal.insert( 0, p_path)
				added_path = True
			else: 
				# provide info about the clashing
				str_advice = "That path would clash!"
				if made_sub_disposal :
					str_advice = str_advice + "\nMakes Sub-folder in Disposals."
				elif clash_disposal :
					str_advice = str_advice + "\nAlready in Disposals."
				if made_sub_survival :
					str_advice = str_advice + "\nMakes Sub-folder in Survivals."
				elif clash_survival :
					str_advice = str_advice + "\nAlready in Survivals"
				m_tkntr.messagebox.showwarning(app_show_name() + " - Adding Survival Path", str_advice) 
		else :
			# go straight ahead and add, relying on user to then do check
			t01_lb_disposal.insert( 0, p_path)
			added_path = True
		if added_path :
			t01_label_disposal_list_alert.configure( text = "Added path: " + p_path)
	def t01_survival_path_addpath_if_okto( p_path):
		added_path = False
		if ( t01_chkvr_autoclashcheck_survivals.get() == 1 ) :
			clash_disposal, made_sub_disposal, clash_survival, made_sub_survival = \
				t01_check_path_for_clashes( p_path )
			if (not clash_disposal) and (not clash_survival) :
				t01_lb_survival.insert( 0, p_path)
				added_path = True
			else: 
				# provide info about the clashing
				str_advice = "That path would clash!"
				if made_sub_disposal :
					str_advice = str_advice + "\nMakes Sub-folder in Disposals."
				elif clash_disposal :
					str_advice = str_advice + "\nAlready in Disposals."
				if made_sub_survival :
					str_advice = str_advice + "\nMakes Sub-folder in Survivals."
				elif clash_survival :
					str_advice = str_advice + "\nAlready in Survivals"
				m_tkntr.messagebox.showwarning(app_show_name() + " - Adding Survival Path", str_advice) 
		else :
			# go straight ahead and add, relying on user to then do check
			t01_lb_survival.insert( 0, p_path)
			added_path = True
		if added_path :
			t01_label_survival_list_alert.configure( text = "Added path: " + p_path)
	# specific actions
	def on_button_exit() :
		# protect from exiting while a process is running for when is asynchronous
		if t01_not_running_else_say( "Exiting" ) :
			i_autosave = ( t06_t4_chkvr_autosave.get() == 1 )
			if i_autosave :
				t01_label_prunatry_status.configure( text = "Autosaving the current settings..." )
				AutoSave_Current_Settings()
			t01_label_prunatry_status.configure( text = "Exiting..." )
			p_window_context.quit()
	# ----------------------------------------
	# For Tab Prunatry
	def t01_SetState_IsRunning() :
		if t01_new_thread_signalling():
			apltry.RunnableProcess_SetState( apltry.RunnableProcess.RunPrntry, apltry.RunnblProcessState.Running )
		else :
			i_TheRunStati.t01_Ctrl_Lock = True
	def t01_IsInState_DoNotInterfere() :
		if t01_new_thread_signalling():
			r_is_running = apltry.RunnableProcess_DoNotInterfere( apltry.RunnableProcess.RunPrntry)
		else :
			r_is_running = i_TheRunStati.t01_Ctrl_Lock
		return r_is_running
	def t01_not_running_else_say( str_action ) :
		# concept here is to return whether we're free to make settings and if not to show a message about it
		if t01_IsInState_DoNotInterfere() :
			m_tkntr.messagebox.showwarning(app_show_name(), str_action + " blocked! A Prunatry run is in process.") 
			i_not_running = False
		else :
			i_not_running = True
		return i_not_running
	def t01_update_label_re_warning( p_IsRisky ) :
		if p_IsRisky :
			i_txt_Risk = "Warning! Immediate deletion without content checking."
			i_fg = "red"
		else :
			i_txt_Risk = "_"
			i_fg = "black"
		t01_label_re_warning.configure( text = i_txt_Risk, fg = i_fg )
	def t01_check_riskiness() :
		# print( "t01_check_riskiness" )
		i_do_super = t01_chkvr_super.get()
		i_do_fold = t01_chkvr_fldrs.get()
		i_do_content_super_fold = t01_chkvr_stringent.get()
		i_do_file = t01_chkvr_files.get()
		i_Hash = t01_chkvr_defmod_hash.get()
		i_docongruentbeforedelete  = ( t01_chkvr_cngrnt_bf_del.get() == 1 )
		i_congruentstringent = ( t01_chkvr_cngrnt_strngnt.get() == 1 )
		i_EnumDeleteMode = t01_cb_EnumDeleteMode.getSelectedKey()
		i_riskiness = \
			apltry.EnumDeleteMode_IsImmediate( i_EnumDeleteMode ) \
			and \
			not ( i_docongruentbeforedelete and i_congruentstringent ) \
			and \
			( \
				( ( i_do_super or i_do_fold ) and not i_do_content_super_fold ) \
				or \
				( i_do_file and not i_Hash ) \
				)
		t01_update_label_re_warning( i_riskiness )
	#
	def t01_delmode_react() :
		# test t01_cb_EnumDeleteMode 
		t_EnumDeleteMode = t01_cb_EnumDeleteMode.getSelectedKey()
		if t_EnumDeleteMode == apltry.EnumDeleteMode.DeleteNow :
			t01_chkvr_cngrnt_bf_del.set(1)
			t01_chkvr_cngrnt_strngnt.set(1)
			t01_chkvr_cngrnt_fails_strngnt.set(1)
		t01_check_riskiness()
	# disposals
	def t01_label_path_disposal_on_rclick_clear() :
		t01_var_disposal.set( "" )
	def t01_on_button_browse_disposal() :
		got_path = t01_var_disposal.get()
		got_string = getpathref_fromuser( "Select a disposal path", got_path)
		t01_var_disposal.set( got_string )
	def t01_on_button_add_folder_disposal() :
		if t01_not_running_else_say( "Adding list item" ) :
			got_path = cmntry.hndy_str_to_goodpath( t01_var_disposal.get() )
			added_path = False
			if ( t01_chkvr_autoclashcheck_disposals.get() == 1 ) :
				clash_disposal, made_sub_disposal, clash_survival, made_sub_survival = \
					t01_check_path_for_clashes( got_path )
				if (not clash_disposal) and (not clash_survival) :
					t01_lb_disposal.insert( 0, got_path)
					added_path = True
				else: 
					# provide info about the clashing
					str_advice = "That path would clash!"
					if made_sub_disposal :
						str_advice = str_advice + "\nMakes Sub-folder in Disposals."
					elif clash_disposal :
						str_advice = str_advice + "\nAlready in Disposals."
					if made_sub_survival :
						str_advice = str_advice + "\nMakes Sub-folder in Survivals."
					elif clash_survival :
						str_advice = str_advice + "\nAlready in Survivals."
					m_tkntr.messagebox.showwarning(app_show_name() + " - Adding Disposal Path", str_advice) 
			else :
				# go straight ahead and add, relying on user to then do check
				t01_lb_disposal.insert( 0, got_path)
				added_path = True
			if added_path :
				t01_label_disposal_list_alert.configure( text = "Added path: " + got_path)
	def t01_on_button_add_subfolders_disposal() :
		# the idea here will be to add all the next level sub-folders of the selected folder 
		# get the list of paths
		if t01_not_running_else_say( "Adding disposal list item" ) :
			got_path = t01_var_disposal.get()
			i_lst_dspsl_pth = explrtry.Path_GetListOfFirstLevelSubPaths( got_path )
			for i_pth in i_lst_dspsl_pth :
				t01_disposal_path_addpath_if_okto( i_pth)
	def t01_on_button_swap_selected_disposal() :
		if t01_not_running_else_say( "Swapping Disposal list item" ) :
			lst_index = t01_lb_disposal.curselection()
			if len( lst_index) > 0 :
				if True:
					t01_label_disposal_list_alert.configure( text = "Can't yet do swaps!")
					m_tkntr.messagebox.showwarning(app_show_name(), "Sorry. Swapping not available yet!") 
				else:
					# get thing to move
					del_index = int(lst_index[0])
					was_path = t01_lb_disposal.get(del_index)
					# need to do clash check for insertion
					t01_lb_disposal.delete( del_index)
					t01_label_disposal_list_alert.configure( text = "Moved: " + was_path)
			else :
				t01_label_disposal_list_alert.configure( text = "No disposal selection.")
	def t01_on_button_remove_selected_disposal() :
		if t01_not_running_else_say( "Removing Disposal list item" ) :
			lst_index = t01_lb_disposal.curselection()
			if len( lst_index) > 0 :
				del_index = int(lst_index[0])
				was_path = t01_lb_disposal.get(del_index)
				t01_lb_disposal.delete( del_index)
				t01_label_disposal_list_alert.configure( text = "Removed: " + was_path)
			else :
				t01_label_disposal_list_alert.configure( text = "No disposal selection.")
	def t01_on_dblclck_selected_disposal() :
		if t01_not_running_else_say( "Noting disposal list item" ) :
			lst_index = t01_lb_disposal.curselection()
			if len( lst_index) > 0 :
				del_index = int(lst_index[0])
				got_path = t01_lb_disposal.get(del_index)
				t01_var_disposal.set( got_path)
	def t01_on_button_remove_all_disposals() :
		if t01_not_running_else_say( "Clearing disposal list" ) :
			t01_lb_disposal.delete(0, m_tkntr.END) # clear
			t01_label_disposal_list_alert.configure( text = "Cleared disposal list.")
	def t01_on_button_check_list_clashes_disposals() :
		if t01_not_running_else_say( "Checking disposal list for clashes" ) :
			if len(t01_get_disposal_list()) > 0 :
				is_all_good = t01_check_disposal_pathlist_for_clashes()
				if is_all_good :
					t01_label_disposal_list_alert.configure( text = "All good, no clashes in disposal list.")
				else :
					t01_label_disposal_list_alert.configure( text = "Problems in disposal list.")
					m_tkntr.messagebox.showwarning(app_show_name(), "Clashes in disposal list.!") 
			else :
				t01_label_disposal_list_alert.configure( text = "Empty disposal list.")
	def t01_on_chkbx_autoclashcheck_disposals():
		if ( t01_chkvr_autoclashcheck_disposals.get() == 1 ) :
			# so this means changing from Off to On
			# we should if all is ok before allowing this 
			if t01_check_both_pathlists_for_clashes():
				t01_label_disposal_list_alert.configure( text = "Auto is on.")
			else :
				t01_chkvr_autoclashcheck_disposals.set(0)
				t01_label_disposal_list_alert.configure( text = "Cannot return to Auto with clashes in lists.")
				m_tkntr.messagebox.showwarning(app_show_name(), "Cannot return to Auto with clashes in lists.") 
		else :
			t01_label_disposal_list_alert.configure( text = "Auto is off.")
	# survivals
	def t01_label_path_survival_on_rclick_clear() :
		t01_var_survival.set( "" )
	def t01_on_button_browse_survival() :
		got_path = t01_var_survival.get()
		got_string = getpathref_fromuser( "Select a survival path", got_path)
		t01_var_survival.set( got_string )
	def t01_on_button_add_folder_survival() :
		if t01_not_running_else_say( "Adding survival list item" ) :
			got_path = cmntry.hndy_str_to_goodpath( t01_var_survival.get() )
			t_ok, t_lst = cmntry.consider_path_str( got_path, cmntry.pfn4print( got_path) )
			if t_ok :
				t01_survival_path_addpath_if_okto( got_path)
			else :
				m_tkntr.messagebox.showwarning(app_show_name(), "\n".join( t_lst) ) 
	def t01_on_button_add_subs_survival() :
		# the idea here will be to add all the next level sub-folders of the selected folder 
		# get the list of paths
		if t01_not_running_else_say( "Adding survival list item" ) :
			got_path = t01_var_survival.get()
			i_lst_srvl_pth = explrtry.Path_GetListOfFirstLevelSubPaths( got_path )
			for i_pth in i_lst_srvl_pth :
				t01_survival_path_addpath_if_okto( i_pth)
	def t01_on_button_swap_selected_survival() :
		if t01_not_running_else_say( "Swapping Disposal list item" ) :
			lst_index = t01_lb_survival.curselection()
			if len( lst_index) > 0 :
				pass
				t01_label_survival_list_alert.configure( text = "Can't yet do swaps.")
			else :
				t01_label_survival_list_alert.configure( text = "No survival selection.")
	def t01_on_button_remove_selected_survival() :
		if t01_not_running_else_say( "Removing list item" ) :
			lst_index = t01_lb_survival.curselection()
			if len( lst_index) > 0 :
				del_index = int(lst_index[0])
				was_path = t01_lb_survival.get(del_index)
				t01_lb_survival.delete( del_index)
				t01_label_survival_list_alert.configure( text = "Removed: " + was_path)
			else :
				t01_label_survival_list_alert.configure( text = "No survival selection.")
	def t01_on_dblclck_selected_survival() :
		if t01_not_running_else_say( "Noting survival list item" ) :
			lst_index = t01_lb_survival.curselection()
			if len( lst_index) > 0 :
				del_index = int(lst_index[0])
				got_path = t01_lb_survival.get(del_index)
				t01_var_survival.set( got_path)
	def t01_on_button_remove_all_survivals() :
		if t01_not_running_else_say( "Clearing survival list" ) :
			t01_lb_survival.delete(0, m_tkntr.END)
			t01_label_survival_list_alert.configure( text = "Cleared survival list.")
	def t01_on_button_check_list_clashes_survivals() :
		if t01_not_running_else_say( "Checking survival list for clashes" ) :
			if len(t01_get_survival_list()) > 0 :
				is_all_good = t01_check_survival_pathlist_for_clashes()
				if is_all_good :
					t01_label_survival_list_alert.configure( text = "No clash among survival list.")
				else :
					t01_label_survival_list_alert.configure( text = "Problems in survival list.")
					m_tkntr.messagebox.showwarning(app_show_name(), "Clashes in survival list.!") 
			else :
				t01_label_survival_list_alert.configure( text = "Empty survival list.")
	def t01_on_chkbx_autoclashcheck_survivals():
		if ( t01_chkvr_autoclashcheck_survivals.get() == 1 ) :
			# so this means changing from Off to On
			# we should if all is ok before allowing this 
			if t01_check_both_pathlists_for_clashes():
				t01_label_survival_list_alert.configure( text = "Auto is on.")
			else :
				t01_chkvr_autoclashcheck_survivals.set(0)
				t01_label_survival_list_alert.configure( text = "Cannot return to Auto with clashes in lists.")
				m_tkntr.messagebox.showwarning(app_show_name(), "Cannot return to Auto with clashes in lists.") 
		else :
			t01_label_survival_list_alert.configure( text = "Auto is off.")
	# actions
	def t01_on_button_swaplists() :
		if t01_not_running_else_say( "Swapping the lists" ) :
			t01_swap_lists_disposal_survival()
			t01_label_prunatry_status.configure( text = "Swapped lists" )
	def t01_on_button_checklists() :
		if t01_not_running_else_say( "Checking both lists" ) :
			if t01_check_both_pathlists_for_clashes() :
				t01_label_prunatry_status.configure( text = "Both lists checked ok. No clashes." )
			else :
				m_tkntr.messagebox.showwarning(app_show_name(), "There is a clash somewhere across the lists!") 
				t01_label_prunatry_status.configure( text = "There is a clash somewhere!" )
	def t01_on_button_clearall():
		t01_label_path_disposal_on_rclick_clear()
		t01_on_button_remove_all_disposals()
		t01_label_path_survival_on_rclick_clear()
		t01_on_button_remove_all_survivals()
	def t01_on_defmod_combobox_release() :
		# get the current mode in the combobox
		i_EnumMatchMode = t01_cb_EnumMatchMode.getSelectedKey()
		# find what checkbox combo that amounts to
		i_Hash, i_Size, i_Name, i_When = apltry.EnumMatchMode_GetCheckCombination( i_EnumMatchMode )
		# reset the checkboxes
		t01_chkvr_defmod_hash.set( i_Hash )
		t01_chkvr_defmod_size.set( i_Size )
		t01_chkvr_defmod_name.set( i_Name )
		t01_chkvr_defmod_when.set( i_When )
		t01_on_EnumDelOrder_combobox_release()
	def t01_defmod_checkboxes_get() :
		r_Hash = t01_chkvr_defmod_hash.get()
		r_Size = t01_chkvr_defmod_size.get()
		r_Name = t01_chkvr_defmod_name.get()
		r_When = t01_chkvr_defmod_when.get()
		return r_Hash, r_Size, r_Name, r_When
	def t01_on_defmod_checkbox_release() :
		# get the current checkboxes
		i_Hash, i_Size, i_Name, i_When = t01_defmod_checkboxes_get()
		if i_Hash :
			t01_chkvr_defmod_size.set( True )
			i_Hash, i_Size, i_Name, i_When = t01_defmod_checkboxes_get()
		# find what mode value should be shown for that
		i_EnumMatchMode = apltry.EnumMatchMode_ForChecks_GetCombination( i_Hash, i_Size, i_Name, i_When)
		# reset the Combobox accordingly 
		t01_cb_EnumMatchMode.setSelectedKey( i_EnumMatchMode)
		t01_on_EnumDelOrder_combobox_release()
		t01_check_riskiness()
	def t01_on_EnumDelOrder_combobox_release() :
		# print( "t01_on_EnumDelOrder_combobox_release")
		i_EnumDelOrder = t01_cb_EnumDelOrder.getSelectedKey()
		i_Hash, i_Size, i_Name, i_When = t01_defmod_checkboxes_get()
		i_EnumDelOrder_IsViable = apltry.EnumDelOrder_IsViableForDefModeFlags( i_EnumDelOrder, i_Hash, i_Size, i_Name, i_When )
		# now do something to set or reset that combo box to show as viable or not
		if i_EnumDelOrder_IsViable :
			i_re_EnumDelOrder = "is viable"
			i_fg = "black"
		else :
			i_re_EnumDelOrder = "NOT viable with current mode checkboxes!"
			i_fg = "red"
		t01_label_re_EnumDelOrder.configure( text = i_re_EnumDelOrder, fg = i_fg )
	def t01_Throcess_Check_NoInterruption():
		# this is a function to pass through to the process so it can be aware of an interrupt request
		return apltry.RunnableProcess_Check_NoInterruption( apltry.RunnableProcess.RunPrntry)
	def t01_throcess_prunatry( p_lst_survival, p_lst_disposal, \
			p_do_sbtry_super, p_do_sbtry_fldrs, p_do_filedupetry, \
			p_sbtry_hash_super, \
			p_FileFlags, p_EnumDelOrder, \
			p_docongruentbeforedelete, p_congruentstringent, p_ReportFailuresStringent, p_EnumDeleteMode ):
		# this is the part that will be run as a thread when we've decided it is ok to do a run
		nonlocal p_multi_log
		nonlocal i_TheRunStati
		nonlocal i_queue
		i_TheRunStati.RunState = True
		t01_SetState_IsRunning()
		apltry.RunnableProcess_SetState( apltry.RunnableProcess.RunPrntry, apltry.RunnblProcessState.Running )
		t01_label_prunatry_status.configure( text = "Prunatry..." + " mode:" + apltry.EnumDeleteMode_Attrbt_Brief( p_EnumDeleteMode) )
		t01_button_prunatry_text.set( Strng_HaltProcess() )
		p_multi_log.do_logs( "p", "Calling prunatry_command")
		prunatry_command( p_lst_survival, p_lst_disposal, \
			p_do_sbtry_super, p_do_sbtry_fldrs, p_do_filedupetry, \
			p_sbtry_hash_super, \
			p_FileFlags, p_EnumDelOrder, \
			p_docongruentbeforedelete, p_congruentstringent, p_ReportFailuresStringent, p_EnumDeleteMode, t01_Throcess_Check_NoInterruption, p_multi_log)
		p_multi_log.do_logs( "p", "Back from prunatry_command")
		apltry.RunnableProcess_SetState( apltry.RunnableProcess.RunPrntry, apltry.RunnblProcessState.Idle )
		#i_TheRunStati.t01_Ctrl_Lock = False
		#i_TheRunStati.RunState = False
		#t01_label_prunatry_status.configure( text = "Dedupe was run." )
		#t01_button_prunatry_text.set("Run Defoliate !")
		i_queue.put( RunStateMessages.t01_Done )
		p_multi_log.do_logs( "sp", "Completed t01_throcess_prunatry " + (40 * "%") )
	def t01_on_button_prunatry() :
		# this is the part that will decide whether it is ok to do a run and then call _throcess_ as a thread 
		nonlocal i_TheRunStati
		nonlocal p_multi_log
		t06_t5_get_log_flags_from_checkboxes( )
		if apltry.RunnableProcess_CheckIs_Interruptable( apltry.RunnableProcess.RunPrntry): # check run state of Prunatry
			i_question_answer = m_tkntr.messagebox.askquestion("Prunatry process", "Prunatry process is currently running.\nAre you sure you want to stop it?", icon="warning")
			if i_question_answer == 'yes' :
				apltry.RunnableProcess_SetState( apltry.RunnableProcess.RunPrntry, apltry.RunnblProcessState.Braking )
				print( "apltry.RunnableProcess_SetState( apltry.RunnableProcess.RunPrntry, apltry.RunnblProcessState.Braking )")
		elif apltry.RunnableProcess_CheckIs_HasBeenToldToInterrupt( apltry.RunnableProcess.RunPrntry):
			m_tkntr.messagebox.showwarning(app_show_name(), "Prunatry process has already been signalled to interrupt itself.") 
		elif i_TheRunStati.RunState :
			if t01_not_running_else_say( "Hmm, only synchronous so probably shouldn't be seeing this!" ) :
				t_EnumDeleteMode_show = apltry.EnumDeleteMode_String( t01_cb_EnumDeleteMode.getSelectedKey() )
				t01_label_prunatry_status.configure( text = "Abandoned processing in mode:" + t_EnumDeleteMode_show )
				i_TheRunStati.RunState = False
				t01_button_prunatry_text.set( Strng_RunProcess() )
		elif t01_fail_check_parts_selection():
			m_tkntr.messagebox.showwarning(app_show_name(), "Cannot run. No parts selected!") 
		elif t01_fail_check_risky_process():
			m_tkntr.messagebox.showwarning(app_show_name(), "Run aborted!\nReconsider risky settings!") 
			t01_label_prunatry_status.configure( text = "No Run. Reconsider risky settings!" )
		elif not t01_check_both_pathlists_for_clashes() :
			m_tkntr.messagebox.showwarning(app_show_name(), "Run refused!\nThere is a clash somewhere across the lists!") 
			t01_label_prunatry_status.configure( text = "No Run. There is a clash somewhere!" )
		else :
			i_lst_survival = t01_get_survival_list()
			i_lst_disposal = t01_get_disposal_list()
			t_EnumDeleteMode_ok = t01_cb_EnumDeleteMode.getSelectedKey() in apltry.EnumDeleteMode
			if not t_EnumDeleteMode_ok :
				m_tkntr.messagebox.showwarning(app_show_name(), "Cannot run. No deletion mode selected!") 
			elif len( i_lst_disposal) == 0 :
				m_tkntr.messagebox.showwarning(app_show_name(), "Cannot run. No Disposal paths!") 
			else :
				i_do_sbtry_super = ( t01_chkvr_super.get() == 1 )
				i_do_sbtry_fldrs = ( t01_chkvr_fldrs.get() == 1 )
				i_do_filedupetry = ( t01_chkvr_files.get() == 1 )
				i_sbtry_hash_super = ( t01_chkvr_stringent.get() == 1 )
				i_UseHash, i_UseSize, i_UseName, i_UseWhen = t01_defmod_checkboxes_get()
				i_FileFlags = cmntry.tpl_FileFlags( i_UseName, i_UseSize, i_UseWhen, i_UseHash)
				i_EnumDelOrder = t01_cb_EnumDelOrder.getSelectedKey()
				i_docongruentbeforedelete  = ( t01_chkvr_cngrnt_bf_del.get() == 1 )
				i_congruentstringent = ( t01_chkvr_cngrnt_strngnt.get() == 1 )
				i_ReportFailuresStringent = ( t01_chkvr_cngrnt_fails_strngnt.get() == 1 )
				i_EnumDeleteMode = t01_cb_EnumDeleteMode.getSelectedKey()
				if apltry.EnumDeleteMode_IsImmediate( i_EnumDeleteMode ) and not ( i_docongruentbeforedelete and i_congruentstringent ):
					t_ask_check = m_tkntr.messagebox.askquestion( "Foldatry - Prunatry", \
						"You have selected immediate deletion without full congruency checking." + "\n" + "Are you sure you want to do this?" )
					if t_ask_check == 'yes' :
						i_final_check_ok = True
					else :
						i_final_check_ok = False
				else :
					i_final_check_ok = True
				if i_final_check_ok:
					if DoNotRunProcessesAsThreads():
						t01_throcess_prunatry( i_lst_survival, i_lst_disposal, \
							i_do_sbtry_super, i_do_sbtry_fldrs, i_do_filedupetry, \
							i_sbtry_hash_super, \
							i_FileFlags, i_EnumDelOrder, \
							i_docongruentbeforedelete, i_congruentstringent, i_ReportFailuresStringent, i_EnumDeleteMode )
						# now cheat a bit and use the same method the thread would use to return the widgets to normal
						i_queue.put( RunStateMessages.t01_Done )
					else:
						p_multi_log.do_logs( "p", "Setting thread for t01_throcess_prunatry")
						# run the process in a thread
						i_t1_thrd = fi_Thread( target = t01_throcess_prunatry, args = ( i_lst_survival, i_lst_disposal, \
							i_do_sbtry_super, i_do_sbtry_fldrs, i_do_filedupetry, \
							i_sbtry_hash_super, \
							i_FileFlags, i_EnumDelOrder, \
							i_docongruentbeforedelete, i_congruentstringent, i_ReportFailuresStringent, i_EnumDeleteMode ) )
						p_multi_log.do_logs( "p", "Starting thread for t01_throcess_prunatry")
						i_t1_thrd.start()
						p_multi_log.do_logs( "p", "Leaving thread to run in background")
				else :
					p_multi_log.do_logs( "sp", "Prunatry run abandoned.")
					t01_label_prunatry_status.configure( text = "Prunatry run abandoned." )
	# ----------------------------------------
	# For Tab Congruentry
	def t02_not_running_else_say( str_action ) :
		# concept here is to return whether we're free to make settings and if not to show a message about it
		if i_TheRunStati.t02_Ctrl_Lock :
			m_tkntr.messagebox.showwarning(app_show_name(), str_action + " blocked! A Congruentry run is in process.") 
			i_not_running = False
		else :
			i_not_running = True
		return i_not_running
	# ........................................
	# PATHS
	# Path A
	def t02_label_path_patha_on_rclick_clear() :
		t02_var_patha.set( "" )
	def t02_on_button_browse_patha() :
		if t02_not_running_else_say( "Browsing for Path A" ) :
			got_path = t02_var_patha.get()
			got_string = getpathref_fromuser( "Select Path A", got_path)
			t02_var_patha.set( got_string )
	# Path B
	def t02_label_path_pathb_on_rclick_clear() :
		t02_var_pathb.set( "" )
	def t02_on_button_browse_pathb() :
		if t02_not_running_else_say( "Browsing for Path B" ) :
			got_path = t02_var_pathb.get()
			got_string = getpathref_fromuser( "Select Path B", got_path)
			t02_var_pathb.set( got_string )
	# ........................................
	def t02_fA_ArchDesc_Unpacks():
		t02_fA_frame3.pack_forget()
	def t02_fA_ArchDesc_Repacks():
		t02_fA_frame3.pack( fill = m_tkntr.X, padx=tk_padx() )
	def t02_fA_Cstm_Unpacks():
		# Now unpack the Custom settings
		t02_fA_frame4.pack_forget()
	def t02_fA_Cstm_Repacks():
		# Now unpack the Custom settings
		t02_fA_frame4.pack( fill = m_tkntr.X, padx=tk_padx() )
	def t02_fA_reflect_EnumFileSysArch() :
		t_EnumFileSysArch = t02_fA_cb_EnumFileSysArch.getSelectedKey()
		if t_EnumFileSysArch == apltry.EnumFileSysArch.FSA_Custom :
			# show the custom settings
			if t02_fA_Cstm_Controls_Created :
				t02_fA_ArchDesc_Unpacks()
				t02_fA_Cstm_Repacks()
			t_str = apltry.Make_StrDescription_For_FnameMatchSetting( i_Cngrntry_Custom_FnameMatchSetting_A )
		else:
			# hide the custom settings
			if t02_fA_Cstm_Controls_Created :
				t02_fA_Cstm_Unpacks()
				t02_fA_ArchDesc_Repacks()
			# update the display
			t_FnameMatchSetting = apltry.FnameMatchSetting()
			t_FnameMatchSetting.set_For_EnumFileSysArch( t_EnumFileSysArch )
			t_str = apltry.Make_StrDescription_For_FnameMatchSetting( t_FnameMatchSetting )
		t02_fA_label_fsa_desc.configure( text = t_str )
	def t02_fA_on_selected_EnumFileSysArch() :
		t02_fA_reflect_EnumFileSysArch()
	def t02_fA_on_button_path_detect() :
		if t02_not_running_else_say( "Detection for path A" ) :
			got_path_a = t02_var_patha.get()
			fallback_enum_charenc_a = None
			i_path_a_fs_type, i_path_a_use_enum_charenc = explrtry.for_path_get_fstype_set_charenc( got_path_a, fallback_enum_charenc_a)
			t_detected = "Path A: " + i_path_a_fs_type + " " + apltry.EnumCharEnc_Attrbt_Brief( i_path_a_use_enum_charenc)
			t02_fA_label_detected.configure( text = t_detected )
	def t02_fA_get_settings() :
		r_FnameMatchSetting = apltry.FnameMatchSetting()
		t_EnumFileSysArch = t02_fA_cb_EnumFileSysArch.getSelectedKey()
		if t_EnumFileSysArch == apltry.EnumFileSysArch.FSA_Custom :
			# get values from all the separate custom GUI widgets
			i_EnumCharEnc = t02_fA_Cstm_cb_CharEnc.getSelectedKey()
			i_CaseSensitive = t02_fA_Cstm_chkvr_CaseSensitive.get()
			i_BadName = t02_fA_Cstn_chkvr_BadName.get()
			i_BadChars = t02_fA_Cstm_chkvr_BadChars.get()
			i_Wind8p3 = t02_fA_Cstn_chkvr_Wind8p3.get()
			# sets for each setting
			r_FnameMatchSetting.set_UseEnumCharEnc( i_EnumCharEnc)
			r_FnameMatchSetting.set_DoCaseSensitive( i_CaseSensitive)
			r_FnameMatchSetting.set_DoMsBadNames( i_BadName)
			r_FnameMatchSetting.set_DoMsBadChars( i_BadChars )
			r_FnameMatchSetting.set_DoMs8p3( i_Wind8p3 )
		else :
			r_FnameMatchSetting.set_For_EnumFileSysArch( t_EnumFileSysArch)
		return r_FnameMatchSetting
	# ........................................
	def t02_fB_ArchDesc_Unpacks():
		t02_fB_frame3.pack_forget()
	def t02_fB_ArchDesc_Repacks():
		t02_fB_frame3.pack( fill = m_tkntr.X, padx=tk_padx() )
	def t02_fB_Cstm_Unpacks():
		# Now unpack the Custom settings
		t02_fB_frame4.pack_forget()
	def t02_fB_Cstm_Repacks():
		# Now unpack the Custom settings
		t02_fB_frame4.pack( fill = m_tkntr.X, padx=tk_padx() )
	def t02_fB_reflect_EnumFileSysArch() :
		t_EnumFileSysArch = t02_fB_cb_EnumFileSysArch.getSelectedKey()
		if t_EnumFileSysArch == apltry.EnumFileSysArch.FSA_Custom :
			# show the custom settings
			if t02_fB_Cstm_Controls_Created :
				t02_fB_ArchDesc_Unpacks()
				t02_fB_Cstm_Repacks()
			t_str = apltry.Make_StrDescription_For_FnameMatchSetting( i_Cngrntry_Custom_FnameMatchSetting_B )
		else:
			# hide the custom settings
			if t02_fB_Cstm_Controls_Created :
				t02_fB_Cstm_Unpacks()
				t02_fB_ArchDesc_Repacks()
			# update the display
			t_FnameMatchSetting = apltry.FnameMatchSetting()
			t_FnameMatchSetting.set_For_EnumFileSysArch( t_EnumFileSysArch )
			t_str = apltry.Make_StrDescription_For_FnameMatchSetting( t_FnameMatchSetting )
		t02_fB_label_fsa_desc.configure( text = t_str )
	def t02_fB_on_selected_EnumFileSysArch() :
		t02_fB_reflect_EnumFileSysArch()
	def t02_fB_on_button_path_detect() :
		if t02_not_running_else_say( "Detection for path B" ) :
			got_path_b = t02_var_pathb.get()
			fallback_enum_charenc_b = None
			i_path_b_fs_type, i_path_b_use_enum_charenc = explrtry.for_path_get_fstype_set_charenc( got_path_b, fallback_enum_charenc_b)
			t_detected = "Path B: " + i_path_b_fs_type + " " + apltry.EnumCharEnc_Attrbt_Brief( i_path_b_use_enum_charenc)
			t02_fB_label_detected.configure( text = t_detected )
	def t02_fB_get_settings() :
		r_FnameMatchSetting = apltry.FnameMatchSetting()
		t_EnumFileSysArch = t02_fB_cb_EnumFileSysArch.getSelectedKey()
		if t_EnumFileSysArch == apltry.EnumFileSysArch.FSA_Custom :
			# get values from all the separate custom GUI widgets
			i_EnumCharEnc = t02_fB_Cstm_cb_CharEnc.getSelectedKey()
			i_CaseSensitive = t02_fB_Cstm_chkvr_CaseSensitive.get()
			i_BadName = t02_fB_Cstn_chkvr_BadName.get()
			i_BadChars = t02_fB_Cstm_chkvr_BadChars.get()
			i_Wind8p3 = t02_fB_Cstn_chkvr_Wind8p3.get()
			# sets for each setting
			r_FnameMatchSetting.set_UseEnumCharEnc( i_EnumCharEnc)
			r_FnameMatchSetting.set_DoCaseSensitive( i_CaseSensitive)
			r_FnameMatchSetting.set_DoMsBadNames( i_BadName)
			r_FnameMatchSetting.set_DoMsBadChars( i_BadChars )
			r_FnameMatchSetting.set_DoMs8p3( i_Wind8p3 )
		else :
			r_FnameMatchSetting.set_For_EnumFileSysArch( t_EnumFileSysArch)
		return r_FnameMatchSetting
	# ........................................
	# DEPRECATED SECTION
	# swap paths A and B
	def t02_on_button_path_swap() :
		if t02_not_running_else_say( "Swappping paths A & B" ) :
			got_path_a = t02_var_patha.get()
			got_path_b = t02_var_pathb.get()
			t02_var_patha.set( got_path_b)
			t02_var_pathb.set( got_path_a)
	def t02_on_button_paths_detects() :
		if t02_not_running_else_say( "Detection for paths A & B" ) :
			got_path_a = t02_var_patha.get()
			got_path_b = t02_var_pathb.get()
			fallback_enum_charenc_a = None
			fallback_enum_charenc_b = None
			i_path_a_fs_type, i_path_a_use_enum_charenc = explrtry.for_path_get_fstype_set_charenc( got_path_a, fallback_enum_charenc_a)
			i_path_b_fs_type, i_path_b_use_enum_charenc = explrtry.for_path_get_fstype_set_charenc( got_path_b, fallback_enum_charenc_b)
			t_detected = "Path A: " + i_path_a_fs_type + " " + apltry.EnumCharEnc_Attrbt_Brief( i_path_a_use_enum_charenc) + "  and  " + \
				"Path B: " + i_path_b_fs_type + " " + apltry.EnumCharEnc_Attrbt_Brief( i_path_b_use_enum_charenc)
			t02_label_detected.configure( text = t_detected )
	def t02_on_button_paths_analysis() :
		m_tkntr.messagebox.showwarning(app_show_name(), "Sorry, not yet implemented.") 
	# ........................................
	# Thread process - put here to be next to the button that calls it as a thread
	def t02_congruentry_paths_throcess( p_str_patha, p_str_pathb, p_FnameMatchSetting_A, p_FnameMatchSetting_B, \
			p_Stringent, p_StopNotCongruent, p_StopNotMoiety, p_ReportDiffs, p_SummaryDiffs, p_ReportFailuresStringent, p_GenCopyScript ) :
		nonlocal p_multi_log
		nonlocal i_TheRunStati
		nonlocal i_queue
		i_TheRunStati.RunState = True
		i_TheRunStati.t02_Ctrl_Lock = True
		p_multi_log.do_logs( "p", "Foldatry executes: congruentry_paths_command")
		got_chckd_congruent, got_chckd_subset, got_chckd_extra_side = \
			congruentry_paths_command( p_str_patha, p_str_pathb, p_FnameMatchSetting_A, p_FnameMatchSetting_B, \
				p_Stringent, p_StopNotCongruent, p_StopNotMoiety, p_ReportDiffs, p_SummaryDiffs, p_ReportFailuresStringent, p_GenCopyScript, \
				p_multi_log)
		p_multi_log.do_logs( "pr", "Congruentry Paths was run!")
		p_multi_log.do_logs( "r", "- Path A: " + p_str_patha )
		p_multi_log.do_logs( "r", "- Path B: " + p_str_pathb )
		str_outcome = "- outcome:"
		str_outcome = str_outcome + "\n" + "got_chckd_congruent: " + str( got_chckd_congruent )
		str_outcome = str_outcome + "\n" + "got_chckd_subset: " + str( got_chckd_subset )
		str_outcome = str_outcome + "\n" + "got_chckd_extra_side: " + got_chckd_extra_side
		p_multi_log.do_logs( "r", str_outcome)
		i_queue.put( RunStateMessages.t02_Paths_Done )
	def t02_on_button_congruentry_paths_process() :
		nonlocal i_TheRunStati
		nonlocal p_multi_log
		t06_t5_get_log_flags_from_checkboxes( )
		i_str_patha = t02_var_patha.get()
		i_str_pathb = t02_var_pathb.get()
		if i_TheRunStati.RunState :
			if t02_not_running_else_say( "Hmm, only synchronous so probably shouldn't be seeing this!" ) :
				t02_label_paths_process_status.configure( text = "Abandoned processing." )
				i_TheRunStati.RunState = False
				t02_button_paths_process_text.set( Strng_RunProcess() )
		elif ( len(i_str_patha) == 0 ) or ( len(i_str_pathb) == 0 ) :
			m_tkntr.messagebox.showwarning(app_show_name(), "At least one path is empty!") 
		else :
			any_clash, any_sub = explrtry.pathlist_self_clash( [ i_str_patha, i_str_pathb ], False, p_multi_log )
			if not ( any_clash or any_sub ) :
				t02_label_paths_process_status.configure( text = "Congruenting..." )
				t02_button_paths_process_text.set( Strng_HaltProcess() )
				# t02_cb_charset_a
				i_Stringent = t02_chkvr_Stringent.get()
				i_StopNotCongruent = t02_chkvr_StopNotCongruent.get()
				i_StopNotMoiety = t02_chkvr_StopNotMoiety.get()
				i_ReportDiffs = t02_chkvr_ReportDiffs.get()
				i_SummaryDiffs = t02_chkvr_SummaryDiffs.get()
				i_ReportFailuresStringent = ( t02_chkvr_cngrnt_fails_strngnt.get() == 1 )
				i_GenCopyScript = t02_chkvr_GenCopyScript.get()
				#
				i_FnameMatchSetting_A = t02_fA_get_settings()
				i_FnameMatchSetting_B = t02_fB_get_settings()
				if DoNotRunProcessesAsThreads() :
					# run not threaded - useful when debugging as thread running can hide errors
					t02_congruentry_paths_throcess( i_str_patha, i_str_pathb, i_FnameMatchSetting_A, i_FnameMatchSetting_B, \
						i_Stringent, i_StopNotCongruent, i_StopNotMoiety, i_ReportDiffs, i_SummaryDiffs, i_ReportFailuresStringent, i_GenCopyScript )
					# now cheat a bit and use the same method the thread would use to return the widgets to normal
					i_queue.put( RunStateMessages.t02_Paths_Done )
				else :
					# run the process in a thread
					p_multi_log.do_logs( "p", "Setting thread for t02_paths_throcess")
					i_t2p_thrd = fi_Thread( target = t02_congruentry_paths_throcess, args = ( i_str_patha, i_str_pathb, i_FnameMatchSetting_A, i_FnameMatchSetting_B, \
						i_Stringent, i_StopNotCongruent, i_StopNotMoiety, i_ReportDiffs, i_SummaryDiffs, i_ReportFailuresStringent, i_GenCopyScript ) )
					p_multi_log.do_logs( "p", "Starting thread t02_paths_throcess")
					i_t2p_thrd.start()
					p_multi_log.do_logs( "p", "Leaving thread to run in background")
			elif any_sub :
				m_tkntr.messagebox.showwarning(app_show_name(), "Run refused!\nOne path is a sub-path of the other!") 
				t02_label_paths_process_status.configure( text = "No Run. One path is a sub-path of the other!" )
			else :
				m_tkntr.messagebox.showwarning(app_show_name(), "Run refused!\nThere is a clash somewhere across the paths!") 
				t02_label_paths_process_status.configure( text = "No Run. There is a clash somewhere!" )
	# ........................................
	# FILES
	# File A
	def t02_label_path_filea_on_rclick_clear() :
		t02_var_filea.set( "" )
	def t02_on_button_browse_filea() :
		if t02_not_running_else_say( "Browsing for File A" ) :
			got_file = t02_var_filea.get()
			# need to add step of getting the path from the fileref
			got_path = explrtry.getpath_fromfileref( got_file )
			got_string = getfileref_fromuser( "Select File A", got_path)
			if len( got_string ) > 0 :
				t02_var_filea.set( got_string)
	# File B
	def t02_label_path_fileb_on_rclick_clear() :
		t02_var_fileb.set( "" )
	def t02_on_button_browse_fileb() :
		if t02_not_running_else_say( "Browsing for File B" ) :
			got_file = t02_var_fileb.get()
			# need to add step of getting the path from the fileref
			got_path = explrtry.getpath_fromfileref( got_file )
			got_string = getfileref_fromuser( "Select File B", got_path)
			if len( got_string ) > 0 :
				t02_var_fileb.set( got_string)
	# Thread process - put here to be next to the button that calls it as a thread
	def t02_congruentry_files_throcess( p_str_filea, p_str_fileb ) :
		nonlocal p_multi_log
		nonlocal i_TheRunStati
		nonlocal i_queue
		i_TheRunStati.RunState = True
		i_TheRunStati.t02_Ctrl_Lock = True
		got_chckd_congruent = congruentry_files_command( p_str_filea, p_str_fileb, p_multi_log)
		p_multi_log.do_logs( "pr", "Congruentry Files run:")
		p_multi_log.do_logs( "r", "File A: " + p_str_filea )
		p_multi_log.do_logs( "r", "File B: " + p_str_fileb )
		str_outcome = "- outcome:"
		str_outcome = str_outcome + "\n" + "got_chckd_congruent: " + str( got_chckd_congruent )
		p_multi_log.do_logs( "r", str_outcome)
		i_queue.put( RunStateMessages.t02_Files_Done )
	def t02_on_button_congruentry_files_process() :
		nonlocal i_TheRunStati
		nonlocal p_multi_log
		t06_t5_get_log_flags_from_checkboxes( )
		if i_TheRunStati.RunState :
			if t02_not_running_else_say( "Hmm, only synchronous so probably shouldn't be seeing this!" ) :
				t_EnumDeleteMode_show = apltry.EnumDeleteMode_String( t01_cb_EnumDeleteMode.getSelectedKey() )
				t02_label_paths_process_status.configure( text = "Abandoned processing in mode:" + t_EnumDeleteMode_show )
				i_TheRunStati.RunState = False
				t02_button_files_process_text.set( Strng_RunProcess() )
		elif t01_check_both_pathlists_for_clashes() :
			t02_label_files_process_status.configure( text = "Processing...")
			i_str_filea = t02_var_filea.get()
			i_str_fileb = t02_var_fileb.get()
			i_enum_EnumGenCopyScriptAs = t02_cb_EnumGenCopyScriptAs.getSelectedKey()
			if ( len(i_str_filea) > 0 ) and ( len(i_str_fileb) > 0 )  :
				t02_button_files_process_text.set( Strng_HaltProcess() )
				i_Stringent = t02_chkvr_files_Stringent.get()
				if DoNotRunProcessesAsThreads():
					t02_congruentry_files_throcess( i_str_filea, i_str_fileb )
					# now cheat a bit and use the same method the thread would use to return the widgets to normal
					i_queue.put( RunStateMessages.t02_Files_Done )
				else:
					# run the process in a thread
					p_multi_log.do_logs( "p", "Setting thread for t02_congruentry_files_throcess")
					i_t2f_thrd = fi_Thread( target = t02_congruentry_files_throcess, args = ( i_str_filea, i_str_fileb ) )
					p_multi_log.do_logs( "p", "Starting thread t02_congruentry_files_throcess")
					i_t2f_thrd.start()
					p_multi_log.do_logs( "p", "Leaving thread to run in background")
			else :
				m_tkntr.messagebox.showwarning(app_show_name(), "At least one file reference is empty!") 
				t02_label_files_process_status.configure( text = "Could not process.")
			# return GUI to normal
			#t02_label_paths_process_status.configure( text = "Run issued - see Results tab" )
			#t02_button_paths_process_text.set("Run Congruate !")
		else :
			m_tkntr.messagebox.showwarning(app_show_name(), "Run refused!") 
			t02_label_files_process_status.configure( text = "No Run." )
	# ----------------------------------------
	# t19_ For Tab Veritry
	def t19_not_running_else_say( str_action ) :
		# concept here is to return whether we're free to make settings and if not to show a message about it
		if i_TheRunStati.t19_Ctrl_Lock :
			m_tkntr.messagebox.showwarning(app_show_name(), str_action + " blocked! A Veritry run is in process.") 
			i_not_running = False
		else :
			i_not_running = True
		return i_not_running
	def t19_get_disposal_list() :
		tpl_disposal = t19_lb_disposal.get(0, m_tkntr.END)
		lst_disposal = list(tpl_disposal)
		return lst_disposal
	def t19_set_disposal_list( lst_disposal ) :
		lst_get = t19_get_disposal_list() 
		for pth in lst_disposal :
			if not (pth in lst_get) :
				t19_lb_disposal.insert( 0, pth)
	def t19_get_survival_list() :
		tpl_survival = t19_lb_survival.get(0, m_tkntr.END)
		lst_survival = list(tpl_survival)
		return lst_survival
	def t19_set_survival_list( lst_survival ) :
		lst_get = t19_get_survival_list() 
		for pth in lst_survival :
			if not (pth in lst_get) :
				t19_lb_survival.insert( 0, pth)
	def t19_check_path_for_clashes( str_path ) :
		nonlocal p_multi_log
		i_lst_disposal = t19_get_disposal_list()
		i_lst_survival = t19_get_survival_list()
		clash_disposal, made_sub_disposal = explrtry.pathlist_path_clash( i_lst_disposal, str_path, False, p_multi_log)
		clash_survival, made_sub_survival = explrtry.pathlist_path_clash( i_lst_survival, str_path, False, p_multi_log)
		return clash_disposal, made_sub_disposal, clash_survival, made_sub_survival
	def t19_check_disposal_pathlist_for_clashes() :
		nonlocal p_multi_log
		i_lst_disposal = t19_get_disposal_list()
		any_clash, any_sub = explrtry.pathlist_self_clash( i_lst_disposal, False, p_multi_log )
		return not any_clash
	def t19_check_survival_pathlist_for_clashes() :
		nonlocal p_multi_log
		i_lst_survival = t19_get_survival_list()
		any_clash, any_sub = explrtry.pathlist_self_clash( i_lst_survival, False, p_multi_log )
		return not any_clash
	def t19_check_both_pathlists_for_clashes() :
		nonlocal p_multi_log
		i_lst_disposal = t19_get_disposal_list()
		i_lst_survival = t19_get_survival_list()
		some_clash = explrtry.pathlist_pathlist_clash( i_lst_disposal, i_lst_survival, False, p_multi_log)
		return not some_clash
	def t19_swap_lists_disposal_survival() :
		i_lst_disposal = t19_get_disposal_list()
		i_lst_survival = t19_get_survival_list()
		t19_lb_disposal.delete(0, m_tkntr.END) 
		t19_lb_survival.delete(0, m_tkntr.END) 
		t19_set_disposal_list( i_lst_survival ) 
		t19_set_survival_list( i_lst_disposal ) 
	def t19_disposal_path_addpath_if_okto( p_path):
		added_path = False
		if ( t19_chkvr_autoclashcheck_disposals.get() == 1 ) :
			clash_disposal, made_sub_disposal, clash_survival, made_sub_survival = \
				t19_check_path_for_clashes( p_path )
			if (not clash_disposal) and (not clash_survival) :
				t19_lb_disposal.insert( 0, p_path)
				added_path = True
			else: 
				# provide info about the clashing
				str_advice = "That path would clash!"
				if made_sub_disposal :
					str_advice = str_advice + "\nMakes Sub-folder in Disposals."
				elif clash_disposal :
					str_advice = str_advice + "\nAlready in Disposals."
				if made_sub_survival :
					str_advice = str_advice + "\nMakes Sub-folder in Survivals."
				elif clash_survival :
					str_advice = str_advice + "\nAlready in Survivals"
				m_tkntr.messagebox.showwarning(app_show_name() + " - Adding Survival Path", str_advice) 
		else :
			# go straight ahead and add, relying on user to then do check
			t19_lb_disposal.insert( 0, p_path)
			added_path = True
		if added_path :
			t19_label_disposal_list_alert.configure( text = "Added path: " + p_path)
	def t19_survival_path_addpath_if_okto( p_path):
		added_path = False
		if ( t19_chkvr_autoclashcheck_survivals.get() == 1 ) :
			clash_disposal, made_sub_disposal, clash_survival, made_sub_survival = \
				t19_check_path_for_clashes( p_path )
			if (not clash_disposal) and (not clash_survival) :
				t19_lb_survival.insert( 0, p_path)
				added_path = True
			else: 
				# provide info about the clashing
				str_advice = "That path would clash!"
				if made_sub_disposal :
					str_advice = str_advice + "\nMakes Sub-folder in Disposals."
				elif clash_disposal :
					str_advice = str_advice + "\nAlready in Disposals."
				if made_sub_survival :
					str_advice = str_advice + "\nMakes Sub-folder in Survivals."
				elif clash_survival :
					str_advice = str_advice + "\nAlready in Survivals"
				m_tkntr.messagebox.showwarning(app_show_name() + " - Adding Survival Path", str_advice) 
		else :
			# go straight ahead and add, relying on user to then do check
			t19_lb_survival.insert( 0, p_path)
			added_path = True
		if added_path :
			t19_label_survival_list_alert.configure( text = "Added path: " + p_path)
	# disposals
	def t19_label_path_disposal_on_rclick_clear() :
		t19_var_disposal.set( "" )
	def t19_on_button_browse_disposal() :
		got_path = t19_var_disposal.get()
		got_string = getpathref_fromuser( "Select a disposal path", got_path)
		t19_var_disposal.set( got_string )
	def t19_on_button_add_folder_disposal() :
		if t19_not_running_else_say( "Adding list item" ) :
			got_path = cmntry.hndy_str_to_goodpath( t19_var_disposal.get() )
			added_path = False
			if ( t19_chkvr_autoclashcheck_disposals.get() == 1 ) :
				clash_disposal, made_sub_disposal, clash_survival, made_sub_survival = \
					t19_check_path_for_clashes( got_path )
				if (not clash_disposal) and (not clash_survival) :
					t19_lb_disposal.insert( 0, got_path)
					added_path = True
				else: 
					# provide info about the clashing
					str_advice = "That path would clash!"
					if made_sub_disposal :
						str_advice = str_advice + "\nMakes Sub-folder in Disposals."
					elif clash_disposal :
						str_advice = str_advice + "\nAlready in Disposals."
					if made_sub_survival :
						str_advice = str_advice + "\nMakes Sub-folder in Survivals."
					elif clash_survival :
						str_advice = str_advice + "\nAlready in Survivals."
					m_tkntr.messagebox.showwarning(app_show_name() + " - Adding Disposal Path", str_advice) 
			else :
				# go straight ahead and add, relying on user to then do check
				t19_lb_disposal.insert( 0, got_path)
				added_path = True
			if added_path :
				t19_label_disposal_list_alert.configure( text = "Added path: " + got_path)
	def t19_on_button_add_subfolders_disposal() :
		# the idea here will be to add all the next level sub-folders of the selected folder 
		# get the list of paths
		if t19_not_running_else_say( "Adding disposal list item" ) :
			got_path = t19_var_disposal.get()
			i_lst_dspsl_pth = explrtry.Path_GetListOfFirstLevelSubPaths( got_path )
			for i_pth in i_lst_dspsl_pth :
				t19_disposal_path_addpath_if_okto( i_pth)
	def t19_on_button_swap_selected_disposal() :
		if t19_not_running_else_say( "Swapping Disposal list item" ) :
			lst_index = t19_lb_disposal.curselection()
			if len( lst_index) > 0 :
				if True:
					t19_label_disposal_list_alert.configure( text = "Can't yet do swaps!")
					m_tkntr.messagebox.showwarning(app_show_name(), "Sorry. Swapping not available yet!") 
				else:
					# get thing to move
					del_index = int(lst_index[0])
					was_path = t19_lb_disposal.get(del_index)
					# need to do clash check for insertion
					t19_lb_disposal.delete( del_index)
					t19_label_disposal_list_alert.configure( text = "Moved: " + was_path)
			else :
				t19_label_disposal_list_alert.configure( text = "No disposal selection.")
	def t19_on_button_remove_selected_disposal() :
		if t19_not_running_else_say( "Removing Disposal list item" ) :
			lst_index = t19_lb_disposal.curselection()
			if len( lst_index) > 0 :
				del_index = int(lst_index[0])
				was_path = t19_lb_disposal.get(del_index)
				t19_lb_disposal.delete( del_index)
				t19_label_disposal_list_alert.configure( text = "Removed: " + was_path)
			else :
				t19_label_disposal_list_alert.configure( text = "No disposal selection.")
	def t19_on_dblclck_selected_disposal() :
		if t19_not_running_else_say( "Noting disposal list item" ) :
			lst_index = t19_lb_disposal.curselection()
			if len( lst_index) > 0 :
				del_index = int(lst_index[0])
				got_path = t19_lb_disposal.get(del_index)
				t19_var_disposal.set( got_path)
	def t19_on_button_remove_all_disposals() :
		if t19_not_running_else_say( "Clearing disposal list" ) :
			t19_lb_disposal.delete(0, m_tkntr.END) # clear
			t19_label_disposal_list_alert.configure( text = "Cleared disposal list.")
	def t19_on_button_check_list_clashes_disposals() :
		if t19_not_running_else_say( "Checking disposal list for clashes" ) :
			if len(t19_get_disposal_list()) > 0 :
				is_all_good = t19_check_disposal_pathlist_for_clashes()
				if is_all_good :
					t19_label_disposal_list_alert.configure( text = "All good, no clashes in disposal list.")
				else :
					t19_label_disposal_list_alert.configure( text = "Problems in disposal list.")
					m_tkntr.messagebox.showwarning(app_show_name(), "Clashes in disposal list.!") 
			else :
				t19_label_disposal_list_alert.configure( text = "Empty disposal list.")
	def t19_on_chkbx_autoclashcheck_disposals():
		if ( t19_chkvr_autoclashcheck_disposals.get() == 1 ) :
			# so this means changing from Off to On
			# we should if all is ok before allowing this 
			if t19_check_both_pathlists_for_clashes():
				t19_label_disposal_list_alert.configure( text = "Auto is on.")
			else :
				t19_chkvr_autoclashcheck_disposals.set(0)
				t19_label_disposal_list_alert.configure( text = "Cannot return to Auto with clashes in lists.")
				m_tkntr.messagebox.showwarning(app_show_name(), "Cannot return to Auto with clashes in lists.") 
		else :
			t19_label_disposal_list_alert.configure( text = "Auto is off.")
	# survivals
	def t19_label_path_survival_on_rclick_clear() :
		t19_var_survival.set( "" )
	def t19_on_button_browse_survival() :
		got_path = t19_var_survival.get()
		got_string = getpathref_fromuser( "Select a survival path", got_path)
		t19_var_survival.set( got_string )
	def t19_on_button_add_folder_survival() :
		if t19_not_running_else_say( "Adding survival list item" ) :
			got_path = cmntry.hndy_str_to_goodpath( t19_var_survival.get() )
			t_ok, t_lst = cmntry.consider_path_str( got_path, cmntry.pfn4print( got_path) )
			if t_ok :
				t19_survival_path_addpath_if_okto( got_path)
			else :
				m_tkntr.messagebox.showwarning(app_show_name(), "\n".join( t_lst) ) 
	def t19_on_button_add_subs_survival() :
		# the idea here will be to add all the next level sub-folders of the selected folder 
		# get the list of paths
		if t19_not_running_else_say( "Adding survival list item" ) :
			got_path = t19_var_survival.get()
			i_lst_srvl_pth = explrtry.Path_GetListOfFirstLevelSubPaths( got_path )
			for i_pth in i_lst_srvl_pth :
				t19_survival_path_addpath_if_okto( i_pth)
	def t19_on_button_swap_selected_survival() :
		if t19_not_running_else_say( "Swapping Disposal list item" ) :
			lst_index = t19_lb_survival.curselection()
			if len( lst_index) > 0 :
				pass
				t19_label_survival_list_alert.configure( text = "Can't yet do swaps.")
			else :
				t19_label_survival_list_alert.configure( text = "No survival selection.")
	def t19_on_button_remove_selected_survival() :
		if t19_not_running_else_say( "Removing list item" ) :
			lst_index = t19_lb_survival.curselection()
			if len( lst_index) > 0 :
				del_index = int(lst_index[0])
				was_path = t19_lb_survival.get(del_index)
				t19_lb_survival.delete( del_index)
				t19_label_survival_list_alert.configure( text = "Removed: " + was_path)
			else :
				t19_label_survival_list_alert.configure( text = "No survival selection.")
	def t19_on_dblclck_selected_survival() :
		if t19_not_running_else_say( "Noting survival list item" ) :
			lst_index = t19_lb_survival.curselection()
			if len( lst_index) > 0 :
				del_index = int(lst_index[0])
				got_path = t19_lb_survival.get(del_index)
				t19_var_survival.set( got_path)
	def t19_on_button_remove_all_survivals() :
		if t19_not_running_else_say( "Clearing survival list" ) :
			t19_lb_survival.delete(0, m_tkntr.END)
			t19_label_survival_list_alert.configure( text = "Cleared survival list.")
	def t19_on_button_check_list_clashes_survivals() :
		if t19_not_running_else_say( "Checking survival list for clashes" ) :
			if len(t19_get_survival_list()) > 0 :
				is_all_good = t19_check_survival_pathlist_for_clashes()
				if is_all_good :
					t19_label_survival_list_alert.configure( text = "No clash among survival list.")
				else :
					t19_label_survival_list_alert.configure( text = "Problems in survival list.")
					m_tkntr.messagebox.showwarning(app_show_name(), "Clashes in survival list.!") 
			else :
				t19_label_survival_list_alert.configure( text = "Empty survival list.")
	def t19_on_chkbx_autoclashcheck_survivals():
		if ( t19_chkvr_autoclashcheck_survivals.get() == 1 ) :
			# so this means changing from Off to On
			# we should if all is ok before allowing this 
			if t19_check_both_pathlists_for_clashes():
				t19_label_survival_list_alert.configure( text = "Auto is on.")
			else :
				t19_chkvr_autoclashcheck_survivals.set(0)
				t19_label_survival_list_alert.configure( text = "Cannot return to Auto with clashes in lists.")
				m_tkntr.messagebox.showwarning(app_show_name(), "Cannot return to Auto with clashes in lists.") 
		else :
			t19_label_survival_list_alert.configure( text = "Auto is off.")
	# actions
	def t19_on_button_swaplists() :
		if t19_not_running_else_say( "Swapping the lists" ) :
			t19_swap_lists_disposal_survival()
			t19_label_prunatry_status.configure( text = "Swapped lists" )
	def t19_on_button_checklists() :
		if t19_not_running_else_say( "Checking both lists" ) :
			if t19_check_both_pathlists_for_clashes() :
				t19_label_prunatry_status.configure( text = "Both lists checked ok. No clashes." )
			else :
				m_tkntr.messagebox.showwarning(app_show_name(), "There is a clash somewhere across the lists!") 
				t19_label_prunatry_status.configure( text = "There is a clash somewhere!" )
	def t19_on_button_clearall():
		t19_label_path_disposal_on_rclick_clear()
		t19_on_button_remove_all_disposals()
		t19_label_path_survival_on_rclick_clear()
		t19_on_button_remove_all_survivals()
	def t19_on_defmod_combobox_release() :
		# get the current mode in the combobox
		i_EnumMatchMode = t19_cb_EnumMatchMode.getSelectedKey()
		# find what checkbox combo that amounts to
		i_Hash, i_Size, i_Name, i_When = apltry.EnumMatchMode_GetCheckCombination( i_EnumMatchMode )
		# reset the checkboxes
		t19_chkvr_defmod_hash.set( i_Hash )
		t19_chkvr_defmod_size.set( i_Size )
		t19_chkvr_defmod_name.set( i_Name )
		t19_chkvr_defmod_when.set( i_When )
	def t19_defmod_checkboxes_get() :
		r_Hash = t19_chkvr_defmod_hash.get()
		r_Size = t19_chkvr_defmod_size.get()
		r_Name = t19_chkvr_defmod_name.get()
		r_When = t19_chkvr_defmod_when.get()
		return r_Hash, r_Size, r_Name, r_When
	def t19_on_defmod_checkbox_release() :
		# get the current checkboxes
		i_Hash, i_Size, i_Name, i_When = t19_defmod_checkboxes_get()
		if i_Hash :
			t19_chkvr_defmod_size.set( True )
			i_Hash, i_Size, i_Name, i_When = t19_defmod_checkboxes_get()
		# find what mode value should be shown for that
		i_EnumMatchMode = apltry.EnumMatchMode_ForChecks_GetCombination( i_Hash, i_Size, i_Name, i_When)
		# reset the Combobox accordingly 
		t19_cb_EnumMatchMode.setSelectedKey( i_EnumMatchMode)
		#t19_check_riskiness()
	def t19_on_EnumDelOrder_combobox_release() :
		# print( "t19_on_EnumDelOrder_combobox_release")
		i_EnumDelOrder = t19_cb_EnumDelOrder.getSelectedKey()
		i_Hash, i_Size, i_Name, i_When = t19_defmod_checkboxes_get()
		i_EnumDelOrder_IsViable = apltry.EnumDelOrder_IsViableForDefModeFlags( i_EnumDelOrder, i_Hash, i_Size, i_Name, i_When )
		# now do something to set or reset that combo box to show as viable or not
		if i_EnumDelOrder_IsViable :
			i_re_EnumDelOrder = "is viable"
			i_fg = "black"
		else :
			i_re_EnumDelOrder = "NOT viable with current mode checkboxes!"
			i_fg = "red"
		t19_label_re_EnumDelOrder.configure( text = i_re_EnumDelOrder, fg = i_fg )
	def t19_throcess_veritry( p_lst_survival, p_lst_disposal, \
			p_FileFlags, p_congruentstringent, p_ReportSuccessStringent, p_ReportFailuresStringent, p_EnumDelOrder ):
		# this is the part that will be run as a thread when we've decided it is ok to do a run
		nonlocal p_multi_log
		nonlocal i_TheRunStati
		nonlocal i_queue
		i_TheRunStati.RunState = True
		i_TheRunStati.t19_Ctrl_Lock = True
		t19_label_prunatry_status.configure( text = "Veritry..." )
		t19_button_prunatry_text.set( Strng_HaltProcess() )
		p_multi_log.do_logs( "p", "Calling veritry_command")
		veritry_command( p_lst_survival, p_lst_disposal, \
			p_FileFlags, p_congruentstringent, p_ReportSuccessStringent, p_ReportFailuresStringent, p_EnumDelOrder, p_multi_log)
		p_multi_log.do_logs( "sp", "Back from veritry_command")
		#i_TheRunStati.t19_Ctrl_Lock = False
		#i_TheRunStati.RunState = False
		#t19_label_prunatry_status.configure( text = "Dedupe was run." )
		#t19_button_prunatry_text.set("Run Defoliate !")
		i_queue.put( RunStateMessages.t19_Done )
		p_multi_log.do_logs( "sp", "Completed t19_throcess_veritry " + (40 * "%") )
	def t19_on_button_veritry() :
		# this is the part that will decide whether it is ok to do a run and then call _throcess_ as a thread 
		nonlocal i_TheRunStati
		nonlocal p_multi_log
		t06_t5_get_log_flags_from_checkboxes( )
		if i_TheRunStati.RunState :
			if t19_not_running_else_say( "Hmm, only synchronous so probably shouldn't be seeing this!" ) :
				t19_label_prunatry_status.configure( text = "Abandoned processing" )
				i_TheRunStati.RunState = False
				t19_button_prunatry_text.set( Strng_RunProcess() )
		elif not t19_check_both_pathlists_for_clashes() :
			m_tkntr.messagebox.showwarning(app_show_name(), "Run refused!\nThere is a clash somewhere across the lists!") 
			t19_label_prunatry_status.configure( text = "No Run. There is a clash somewhere!" )
		else :
			i_lst_survival = t19_get_survival_list()
			i_lst_disposal = t19_get_disposal_list()
			if len( i_lst_survival) == 0 or len( i_lst_disposal) == 0 :
				m_tkntr.messagebox.showwarning(app_show_name(), "Cannot run. No Disposal paths!") 
			else :
				i_UseHash, i_UseSize, i_UseName, i_UseWhen = t19_defmod_checkboxes_get()
				i_FileFlags = cmntry.tpl_FileFlags( i_UseName, i_UseSize, i_UseWhen, i_UseHash)
				i_congruentstringent = ( t19_chkvr_cngrnt_strngnt.get() == 1 )
				i_ReportSuccessStringent = ( t19_chkvr_cngrnt_goods_strngnt.get() == 1 )
				i_ReportFailuresStringent = ( t19_chkvr_cngrnt_fails_strngnt.get() == 1 )
				i_EnumDelOrder = t19_cb_EnumDelOrder.getSelectedKey()
				i_final_check_ok = True
				if i_final_check_ok:
					if DoNotRunProcessesAsThreads():
						t19_throcess_veritry( i_lst_survival, i_lst_disposal, \
							i_FileFlags, i_congruentstringent, i_ReportSuccessStringent, i_ReportFailuresStringent, i_EnumDelOrder )
						# now cheat a bit and use the same method the thread would use to return the widgets to normal
						i_queue.put( RunStateMessages.t19_Done )
					else:
						p_multi_log.do_logs( "p", "Setting thread for t19_throcess_veritry")
						# run the process in a thread
						i_t1_thrd = fi_Thread( target = t19_throcess_veritry, args = ( i_lst_survival, i_lst_disposal, \
							i_FileFlags, i_congruentstringent, i_ReportSuccessStringent, i_ReportFailuresStringent, i_EnumDelOrder ) )
						p_multi_log.do_logs( "p", "Starting thread for t19_throcess_veritry")
						i_t1_thrd.start()
						p_multi_log.do_logs( "p", "Leaving thread to run in background")
				else :
					p_multi_log.do_logs( "sp", "Veritry run abandoned.")
					t19_label_prunatry_status.configure( text = "Veritry run abandoned." )
	# ----------------------------------------
	# For Tab Defoliatry
	def t03_not_running_else_say( str_action ) :
		# concept here is to return whether we're free to make settings and if not to show a message about it
		if i_TheRunStati.t03_Ctrl_Lock :
			m_tkntr.messagebox.showwarning(app_show_name(), str_action + " blocked! A Defoliatry run is in process.") 
			i_not_running = False
		else :
			i_not_running = True
		return i_not_running
	def t03_update_label_re_warning( p_IsRisky ) :
		if p_IsRisky :
			i_txt_Risk = "Warning! Immediate deletion without content checking."
			i_fg = "red"
		else :
			i_txt_Risk = "_"
			i_fg = "black"
		t03_label_re_warning.configure( text = i_txt_Risk, fg = i_fg )
	def t03_check_riskiness() :
		i_Hash = t03_chkvr_defmod_hash.get()
		i_docongruentbeforedelete  = ( t03_chkvr_cngrnt_bf_del.get() == 1 )
		i_congruentstringent = ( t03_chkvr_cngrnt_strngnt.get() == 1 )
		i_EnumDeleteMode = t03_cb_EnumDeleteMode.getSelectedKey()
		i_riskiness = apltry.EnumDeleteMode_IsImmediate( i_EnumDeleteMode ) and not ( ( i_docongruentbeforedelete and i_congruentstringent ) or i_Hash )
		t03_update_label_re_warning( i_riskiness )
	def t03_label_paths_on_rclick_clear() :
		t03_var_path.set( "" )
	def t03_on_button_browse_paths() :
		got_path = t03_var_path.get()
		got_string = getpathref_fromuser( "Select a path", got_path)
		t03_var_path.set( got_string )
	def t03_on_button_add_folder() :
		if t03_not_running_else_say( "Adding list item" ) :
			got_path = cmntry.hndy_str_to_goodpath( t03_var_path.get() )
			added_path = False
			if ( t03_chkvr_autoclashcheck_paths.get() == 1 ) :
				clash_path, is_sub_path = \
					t03_check_path_for_clashes( got_path )
				if (not clash_path) :
					t03_lb_paths.insert( 0, got_path)
					added_path = True
				else: 
					# provide info about the clashing
					str_advice = "That path would clash!"
					if is_sub_path :
						str_advice = str_advice + "\nMakes Sub-folder in paths."
					elif clash_path :
						str_advice = str_advice + "\nAlready in paths."
					m_tkntr.messagebox.showwarning(app_show_name() + " - Adding Path", str_advice) 
			else :
				# go straight ahead and add, relying on user to then do check
				t03_lb_paths.insert( 0, got_path)
				added_path = True
			if added_path :
				t03_label_paths_alert.configure( text = "Added path: " + got_path)
	def t03_on_button_add_subfolders() :
		# the idea here will be to add all the next level sub-folders of the selected folder 
		# get the list of paths
		if t03_not_running_else_say( "Adding disposal list item" ) :
			got_path = t03_var_path.get()
			i_lst_dspsl_pth = explrtry.Path_GetListOfFirstLevelSubPaths( got_path )
			for i_pth in i_lst_dspsl_pth :
				t03_path_addpath_if_okto( i_pth)
	def t03_on_button_remove_selected_path() :
		if t03_not_running_else_say( "Removing list item" ) :
			lst_index = t03_lb_paths.curselection()
			if len( lst_index) > 0 :
				del_index = int(lst_index[0])
				was_path = t03_lb_paths.get(del_index)
				t03_lb_paths.delete( del_index)
				t03_label_paths_alert.configure( text = "Removed: " + was_path)
			else :
				t03_label_paths_alert.configure( text = "No selection.")
	def t03_on_dblclck_selected_path() :
		if t03_not_running_else_say( "Noting list item" ) :
			lst_index = t03_lb_paths.curselection()
			if len( lst_index) > 0 :
				del_index = int(lst_index[0])
				got_path = t03_lb_paths.get(del_index)
				t03_var_path.set( got_path)
	def t03_on_button_remove_all_paths() :
		if t03_not_running_else_say( "Clearing list" ) :
			t03_lb_paths.delete(0, m_tkntr.END) # clear
			t03_label_paths_alert.configure( text = "Cleared list.")
	def t03_on_button_check_list_clashes_paths() :
		if t03_not_running_else_say( "Checking list for clashes" ) :
			if len(t03_get_paths_list()) > 0 :
				is_all_good = t03_check_paths_for_clashes()
				if is_all_good :
					t03_label_paths_alert.configure( text = "All good, no clashes in list.")
				else :
					t03_label_paths_alert.configure( text = "Problems in list.")
					m_tkntr.messagebox.showwarning(app_show_name(), "Clashes in list.!") 
			else :
				t03_label_paths_alert.configure( text = "Empty list.")
	def t03_on_chkbx_autoclashcheck_paths():
		if ( t03_chkvr_autoclashcheck_paths.get() == 1 ) :
			# so this means changing from Off to On
			# we should if all is ok before allowing this 
			if t03_check_paths_for_clashes():
				t03_label_paths_alert.configure( text = "Auto is on.")
			else :
				t03_chkvr_autoclashcheck_paths.set(0)
				t03_label_paths_alert.configure( text = "Cannot return to Auto with clashes in lists.")
				m_tkntr.messagebox.showwarning(app_show_name(), "Cannot return to Auto with clashes in lists.") 
		else :
			t03_label_paths_alert.configure( text = "Auto is off.")
	def t03_on_defmod_combobox_release() :
		# get the current mode in the combobox
		i_EnumMatchMode = t03_cb_EnumMatchMode.getSelectedKey()
		# find what checkbox combo that amounts to
		i_Hash, i_Size, i_Name, i_When = apltry.EnumMatchMode_GetCheckCombination( i_EnumMatchMode )
		# reset the checkboxes
		t03_chkvr_defmod_hash.set( i_Hash )
		t03_chkvr_defmod_size.set( i_Size )
		t03_chkvr_defmod_name.set( i_Name )
		t03_chkvr_defmod_when.set( i_When )
		t03_on_EnumDelOrder_combobox_release()
	def t03_defmod_checkboxes_get() :
		r_Hash = t03_chkvr_defmod_hash.get()
		r_Size = t03_chkvr_defmod_size.get()
		r_Name = t03_chkvr_defmod_name.get()
		r_When = t03_chkvr_defmod_when.get()
		return r_Hash, r_Size, r_Name, r_When
	def t03_on_defmod_checkbox_release() :
		# get the current checkboxes
		i_Hash, i_Size, i_Name, i_When = t03_defmod_checkboxes_get()
		if i_Hash :
			t03_chkvr_defmod_size.set( True )
			i_Hash, i_Size, i_Name, i_When = t03_defmod_checkboxes_get()
		# find what mode value should be shown for that
		i_EnumMatchMode = apltry.EnumMatchMode_ForChecks_GetCombination( i_Hash, i_Size, i_Name, i_When)
		# reset the Combobox accordingly 
		t03_cb_EnumMatchMode.setSelectedKey( i_EnumMatchMode)
		t03_on_EnumDelOrder_combobox_release()
		t03_check_riskiness()
	def t03_on_EnumDelOrder_combobox_release() :
		# print( "t03_on_EnumDelOrder_combobox_release")
		i_EnumDelOrder = t03_cb_EnumDelOrder.getSelectedKey()
		i_Hash, i_Size, i_Name, i_When = t03_defmod_checkboxes_get()
		i_EnumDelOrder_IsViable = apltry.EnumDelOrder_IsViableForDefModeFlags( i_EnumDelOrder, i_Hash, i_Size, i_Name, i_When )
		# no do something to set or reset that combo box to show as viable or not
		if i_EnumDelOrder_IsViable :
			i_re_EnumDelOrder = "is viable"
			i_fg = "black"
		else :
			i_re_EnumDelOrder = "NOT viable with current mode checkboxes!"
			i_fg = "red"
		t03_label_re_EnumDelOrder.configure( text = i_re_EnumDelOrder, fg = i_fg )
	def t03_throcess_dedupe( p_lst_paths, \
			p_FileFlags, \
			p_EnumKeepWhich, p_EnumDelOrder, \
			p_ignore_zero_len, p_use_names, p_lst_names_bad, p_docongruentbeforedelete, p_congruentstringent, p_ReportFailuresStringent, p_EnumDeleteMode ) :
		nonlocal i_TheRunStati
		nonlocal p_multi_log
		nonlocal i_queue
		i_TheRunStati.RunState = True
		i_TheRunStati.t03_Ctrl_Lock = True
		t03_label_dedupe_status.configure( text = "Deduping..." + " mode:" + apltry.EnumDeleteMode_Attrbt_Brief( p_EnumDeleteMode) )
		t03_button_dedupe_text.set( Strng_HaltProcess() )
		p_multi_log.do_logs( "p", "Calling defoliatry_command")
		defoliatry_command( p_lst_paths, \
			p_FileFlags, \
			p_EnumKeepWhich, p_EnumDelOrder, \
			p_ignore_zero_len, p_use_names, p_lst_names_bad, p_docongruentbeforedelete, p_congruentstringent, p_ReportFailuresStringent, p_EnumDeleteMode, p_multi_log)
		p_multi_log.do_logs( "sp", "Back from defoliatry_command")
		#t03_label_dedupe_status.configure( text = "Dedupe was run." )
		#t03_button_dedupe_text.set("Run dedupe !")
		i_queue.put( RunStateMessages.t03_Done )
		p_multi_log.do_logs( "sp", "Completed t03_throcess_dedupe " + (40 * "%") )
	def t03_on_button_dedupe() :
		# set this up ready for when asynchronous running becomes the way
		nonlocal i_TheRunStati
		nonlocal p_multi_log
		t06_t5_get_log_flags_from_checkboxes( )
		i_lst_paths = t03_get_paths_list()
		cando_fldrlst = len( i_lst_paths) > 0
		cando_clashes = t03_check_paths_for_clashes()
		i_Hash, i_Size, i_Name, i_When = t03_defmod_checkboxes_get()
		cando_defmode = apltry.EnumMatchMode_Combination_IsViable( i_Hash, i_Size, i_Name, i_When )
		i_FileFlags = cmntry.tpl_FileFlags( i_Name, i_Size, i_When, i_Hash)
		if i_TheRunStati.RunState :
			if not_running_else_say( "Hmm, only synchronous so probably shouldn't be seeing this!" ) :
				t_EnumDeleteMode_show = apltry.EnumDeleteMode_String( t03_cb_EnumDeleteMode.getSelectedKey() )
				t03_label_dedupe_status.configure( text = "Abandoned processing in mode:" + t_EnumDeleteMode_show )
				i_TheRunStati.RunState = False
				t03_button_dedupe_text.set( Strng_RunProcess() )
		elif cando_fldrlst and cando_clashes and cando_defmode :
			#print( i_EnumKeepWhich )
			i_EnumKeepWhich = t03_cb_EnumKeepWhich.getSelectedKey()
			i_EnumDelOrder = t03_cb_EnumDelOrder.getSelectedKey()
			i_docongruentbeforedelete  = ( t03_chkvr_cngrnt_bf_del.get() == 1 )
			i_congruentstringent = ( t03_chkvr_cngrnt_strngnt.get() == 1 )
			i_ReportFailuresStringent = ( t03_chkvr_cngrnt_fails_strngnt.get() == 1 )
			i_EnumDeleteMode = t03_cb_EnumDeleteMode.getSelectedKey()
			i_lst_names_bad = get_malname_list()
			i_use_names = t03_chkvr_usediscards and ( len( i_lst_names_bad) > 0 )
			i_ignore_zero_len = t03_chkvr_ignore0len
			if apltry.EnumDeleteMode_IsImmediate( i_EnumDeleteMode ) and not ( i_docongruentbeforedelete and i_congruentstringent ):
				t_ask_check = m_tkntr.messagebox.askquestion( "Foldatry - Defoliatry", \
					"You have selected immediate deletion without full congruency checking." + "\n" + "Are you sure you want to do this?" )
				if t_ask_check == 'yes' :
					i_final_check_ok = True
				else :
					i_final_check_ok = False
			else :
				i_final_check_ok = True
			if i_final_check_ok:
				if DoNotRunProcessesAsThreads():
					t03_throcess_dedupe( i_lst_paths, \
						i_FileFlags, \
						i_EnumKeepWhich, i_EnumDelOrder, \
						i_ignore_zero_len, i_use_names, i_lst_names_bad, i_docongruentbeforedelete, i_congruentstringent, i_ReportFailuresStringent, i_EnumDeleteMode )
					# now cheat a bit and use the same method the thread would use to return the widgets to normal
					i_queue.put( RunStateMessages.t03_Done )
				else:
					# run the process in a thread
					p_multi_log.do_logs( "p", "Setting thread for t03_throcess_dedupe")
					i_t3_thrd = fi_Thread( target = t03_throcess_dedupe, args = ( i_lst_paths, \
						i_FileFlags, \
						i_EnumKeepWhich, i_EnumDelOrder, \
						i_ignore_zero_len, i_use_names, i_lst_names_bad, i_docongruentbeforedelete, i_congruentstringent, i_ReportFailuresStringent, i_EnumDeleteMode ))
					p_multi_log.do_logs( "p", "Starting thread for t03_throcess_dedupe")
					i_t3_thrd.start()
					p_multi_log.do_logs( "p", "Leaving thread to run in background")
			else :
				p_multi_log.do_logs( "sp", "Defoliatry run abandoned.")
				t03_label_dedupe_status.configure( text = "Defoliatry run abandoned." )
		elif not cando_clashes :
			m_tkntr.messagebox.showwarning(app_show_name(), "Run refused!\nThere is a clash somewhere across the lists!") 
			t03_label_dedupe_status.configure( text = "No Run. There is a clash somewhere!" )
		elif not cando_fldrlst :
			m_tkntr.messagebox.showwarning(app_show_name(), "Run refused!\nThere are no folders listed!") 
			t03_label_dedupe_status.configure( text = "No Run. There are no folders listed" )
		elif not cando_defmode :
			m_tkntr.messagebox.showwarning(app_show_name(), "Run refused!\nThe Defioliatry mode selection is invalid!") 
			t03_label_dedupe_status.configure( text = "No Run. Was not a valid Defioliatry." )
	# ----------------------------------------
	# For Tab Summatry
	def t05_not_running_else_say( str_action ) :
		# concept here is to return whether we're free to make settings and if not to show a message about it
		if i_TheRunStati.t05_Ctrl_Lock :
			m_tkntr.messagebox.showwarning(app_show_name(), str_action + " blocked! A Summatry run is in process.") 
			i_not_running = False
		else :
			i_not_running = True
		return i_not_running
	def t05_label_folder_on_rclick_clear() :
		t05_var_entry_folder.set( "" )
	def t05_on_button_browse_folder() :
		got_path = t05_var_entry_folder.get()
		got_string = getpathref_fromuser( "Select a Summatry path", got_path)
		t05_var_entry_folder.set( got_string )
	def t05_path_addpath_if_okto( p_path):
		added_path = False
		if ( t05_chkvr_autoclashcheck_paths.get() == 1 ) :
			i_is_clash, i_is_sub = t05_check_path_for_folder_clashes( p_path )
			if not i_is_clash :
				t05_lb_folderlist.insert( 0, p_path)
				added_path = True
			else: 
				# provide info about the clashing
				str_advice = "That path would clash!"
				if i_is_sub :
					str_advice = str_advice + "\nMakes Sub-folder."
				elif i_is_clash :
					str_advice = str_advice + "\nAlready in list."
				m_tkntr.messagebox.showwarning(app_show_name() + " - Adding Survival Path", str_advice) 
		else :
			# go straight ahead and add, relying on user to then do check
			t05_lb_folderlist.insert( 0, p_path)
			added_path = True
		if added_path :
			t05_label_alert_folderlist.configure( text = "Added path: " + p_path)

	def t05_on_button_add_folder() :
		if t05_not_running_else_say( "Adding list item" ) :
			got_path = cmntry.hndy_str_to_goodpath( t05_var_entry_folder.get() )
			added_path = False
			if ( t05_chkvr_autoclashcheck_paths.get() == 1 ) :
				clash_path, is_sub_path = \
					t05_check_path_for_folder_clashes( got_path )
				if (not clash_path) :
					t05_lb_folderlist.insert( 0, got_path)
					added_path = True
				else: 
					# provide info about the clashing
					str_advice = "That path would clash!"
					if is_sub_path :
						str_advice = str_advice + "\nMakes Sub-folder in Summatry lists."
					elif clash_path :
						str_advice = str_advice + "\nAlready in Summatry lists."
					m_tkntr.messagebox.showwarning(app_show_name() + " - Adding Summatry list Path", str_advice) 
			else :
				# go straight ahead and add, relying on user to then do check
				t05_lb_folderlist.insert( 0, got_path)
				added_path = True
			if added_path :
				t05_label_alert_folderlist.configure( text = "Added path: " + got_path)
	def t05_on_button_add_subfolders() :
		# the idea here will be to add all the next level sub-folders of the selected folder 
		# get the list of paths
		if t05_not_running_else_say( "Adding disposal list item" ) :
			got_path = cmntry.hndy_str_to_goodpath( t05_var_entry_folder.get() )
			i_lst_dspsl_pth = explrtry.Path_GetListOfFirstLevelSubPaths( got_path )
			for i_pth in i_lst_dspsl_pth :
				t05_path_addpath_if_okto( i_pth)
	def t05_on_button_remove_selected_folderlist() :
		if t05_not_running_else_say( "Removing Summatry list list item" ) :
			lst_index = t05_lb_folderlist.curselection()
			if len( lst_index) > 0 :
				del_index = int(lst_index[0])
				was_path = t05_lb_folderlist.get(del_index)
				t05_lb_folderlist.delete( del_index)
				t05_label_alert_folderlist.configure( text = "Removed: " + was_path)
			else :
				t05_label_alert_folderlist.configure( text = "No Summatry list selection.")
	def t05_on_dblclck_selected_folderlist() :
		if t05_not_running_else_say( "Noting Summatry list list item" ) :
			lst_index = t05_lb_folderlist.curselection()
			if len( lst_index) > 0 :
				del_index = int(lst_index[0])
				got_path = t05_lb_folderlist.get(del_index)
				t05_var_entry_folder.set( got_path)
	def t05_on_button_remove_all_folderlist() :
		if t05_not_running_else_say( "Clearing Summatry list list" ) :
			t05_lb_folderlist.delete(0, m_tkntr.END) # clear
			t05_label_alert_folderlist.configure( text = "Cleared Summatry list list.")
	def t05_on_button_check_list_clashes_folderlist() :
		if t05_not_running_else_say( "Checking Summatry list list for clashes" ) :
			if len(t05_get_folder_list()) > 0 :
				is_all_good = t05_check_folder_pathlist_for_clashes()
				if is_all_good :
					t05_label_alert_folderlist.configure( text = "All good, no clashes in Summatry list list.")
				else :
					t05_label_alert_folderlist.configure( text = "Problems in Summatry list list.")
					m_tkntr.messagebox.showwarning(app_show_name(), "Clashes in Summatry list list.!") 
			else :
				t05_label_alert_folderlist.configure( text = "Empty Summatry list list.")
	def t05_on_chkbx_autoclashcheck_paths():
		if ( t05_chkvr_autoclashcheck_paths.get() == 1 ) :
			# so this means changing from Off to On
			# we should if all is ok before allowing this 
			if t05_check_folder_pathlist_for_clashes():
				t05_label_alert_folderlist.configure( text = "Auto is on.")
			else :
				t05_chkvr_autoclashcheck_paths.set(0)
				t05_label_alert_folderlist.configure( text = "Cannot return to Auto with clashes in lists.")
				m_tkntr.messagebox.showwarning(app_show_name(), "Cannot return to Auto with clashes in lists.") 
		else :
			t05_label_alert_folderlist.configure( text = "Auto is off.")
	# Elements for calling summatry_command
	def t05_throcess_summatry( i_lst_folder, p_dummy ) :
		nonlocal i_TheRunStati
		nonlocal p_multi_log
		nonlocal i_queue
		i_TheRunStati.RunState = True
		i_TheRunStati.t05_Ctrl_Lock = True
		t05_label_action_status.configure( text = "Summarising..." )
		t05_button_action_var.set( Strng_HaltProcess() )
		p_multi_log.do_logs( "p", "Calling summatry_command")
		#for f in i_lst_folder :
		#print( "Folder to summarise: " + f )
		summatry_command( i_lst_folder, p_multi_log)
		p_multi_log.do_logs( "sp", "Back from summatry_command")
		#t05_label_action_status.configure( text = "Summatry was run." )
		#t05_button_action_var.set("Run Summatry !")
		i_queue.put( RunStateMessages.t05_Summ_Done )
		p_multi_log.do_logs( "sp", "Completed t05_throcess_summatry " + (40 * "%") )
	def t05_on_button_summatry() :
		# set this up ready for when asynchronous running becomes the way
		nonlocal i_TheRunStati
		nonlocal p_multi_log
		t06_t5_get_log_flags_from_checkboxes( )
		i_lst_folder = t05_get_folder_list()
		#for f in i_lst_folder :
		#print( "Folder to summarise: " + f )
		cando_fldrlst = len( i_lst_folder) > 0
		cando_clashes = t05_check_folder_pathlist_for_clashes()
		if i_TheRunStati.RunState :
			if not_running_else_say( "Hmm, only synchronous so probably shouldn't be seeing this!" ) :
				t05_label_action_status.configure( text = "Abandoned processing." )
				i_TheRunStati.RunState = False
				t05_button_action_var.set( Strng_RunProcess() )
		elif cando_fldrlst and cando_clashes :
			if DoNotRunProcessesAsThreads():
				summatry_command( i_lst_folder, p_multi_log)
				# now cheat a bit and use the same method the thread would use to return the widgets to normal
				i_queue.put( RunStateMessages.t05_Summ_Done )
			else:
				# run the process in a thread
				p_multi_log.do_logs( "p", "Setting thread for t05_throcess_summatry")
				i_t5a_thrd = fi_Thread( target = t05_throcess_summatry, args = ( i_lst_folder, 6 ))
				p_multi_log.do_logs( "p", "Starting thread t05_throcess_summatry")
				i_t5a_thrd.start()
				p_multi_log.do_logs( "p", "Leaving thread to run in background")
		elif not cando_clashes :
			m_tkntr.messagebox.showwarning(app_show_name(), "Run refused!\nThere is a clash somewhere across the lists!") 
			t05_label_action_status.configure( text = "No Run. There is a clash somewhere!" )
		elif not cando_fldrlst :
			m_tkntr.messagebox.showwarning(app_show_name(), "Run refused!\nThere are no folders listed!") 
			t05_label_action_status.configure( text = "No Run. There are no folders listed" )
	# Elements for calling summatry_checkfor - for the feature of checking names for problems
	def t05_throcess_checkfor( p_lst_folder, p_badwinfname, p_badwinchars ) :
		nonlocal i_TheRunStati
		nonlocal p_multi_log
		nonlocal i_queue
		i_TheRunStati.RunState = True
		i_TheRunStati.t05_Ctrl_Lock = True
		t05_label_checkfor_status.configure( text = "Checking for..." )
		t05_button_checkfor_var.set( Strng_HaltProcess() )
		p_multi_log.do_logs( "p", "Calling summatry_checkfor")
		summatry_checkfor( p_lst_folder, p_badwinfname, p_badwinchars, p_multi_log )
		p_multi_log.do_logs( "sp", "Back from summatry_checkfor")
		i_queue.put( RunStateMessages.t05_Chek_Done )
		p_multi_log.do_logs( "sp", "Completed t05_throcess_checkfor " + (40 * "%") )
	def t05_process_checkfor( p_lst_folder, p_badwinfname, p_badwinchars ) :
		nonlocal i_TheRunStati
		# summatry_checkfor( p_lst_folder, p_badwinfname, p_badwinchars, p_multi_log )
		if i_TheRunStati.RunState :
			m_tkntr.messagebox.showwarning(app_show_name(), "Run refused!\nSome Summatry CheckFor action is in progress!") 
			t05_label_checkfor_status.configure( text = "No Run. Summatry CheckFor action is in progress." )
		else :
			if DoNotRunProcessesAsThreads():
				t05_throcess_checkfor( p_lst_folder, p_badwinfname, p_badwinchars )
				# now cheat a bit and use the same method the thread would use to return the widgets to normal
				i_queue.put( RunStateMessages.t05_Chek_Done )
			else:
				# run the process in a thread
				p_multi_log.do_logs( "p", "Setting thread for t05_throcess_checkfor")
				i_t5b_thrd = fi_Thread( target = t05_throcess_checkfor, args = ( p_lst_folder, p_badwinfname, p_badwinchars ))
				p_multi_log.do_logs( "p", "Starting thread t05_throcess_checkfor")
				i_t5b_thrd.start()
				p_multi_log.do_logs( "p", "Leaving thread to run in background")
	def t05_on_button_checkfor() :
		i_lst_folder = t05_get_folder_list()
		i_badwinfname = t05_chkvr_checkfor_badwinfname.get()
		i_badwinchars = t05_chkvr_checkfor_badwinchars.get()
		if len( i_lst_folder ) == 0 :
			m_tkntr.messagebox.showwarning(app_show_name(), "Run refused!\nThere are no folders listed!") 
			t05_label_checkfor_status.configure( text = "No Run. There are no folders listed." )
		elif not ( i_badwinfname or i_badwinchars ) :
			m_tkntr.messagebox.showwarning(app_show_name(), "Run refused!\nThere are no options selected!") 
			t05_label_checkfor_status.configure( text = "No Run. There were no options selected." )
		else :
			t05_process_checkfor( i_lst_folder, i_badwinfname, i_badwinchars )
	# Elements for calling summatry_counts_by_filename_ext
	def t05_throcess_countsbfe( p_lst_folder ) :
		nonlocal i_TheRunStati
		nonlocal p_multi_log
		nonlocal i_queue
		i_TheRunStati.RunState = True
		i_TheRunStati.t05_Ctrl_Lock = True
		t05_label_countsbfe_status.configure( text = "Checking for..." )
		t05_button_countsbfe_var.set( Strng_HaltProcess() )
		# print( "t05_throcess_countsbfe" )
		# print( "p_lst_folder" )
		# print( p_lst_folder )
		p_multi_log.do_logs( "p", "Calling summatry_countsbfe")
		summatry_counts_by_filename_ext( p_lst_folder, p_multi_log )
		p_multi_log.do_logs( "sp", "Back from summatry_countsbfe")
		i_queue.put( RunStateMessages.t05_countsbfe_Done )
		p_multi_log.do_logs( "sp", "Completed t05_throcess_countsbfe " + (40 * "%") )
	def t05_process_countsbfe( p_lst_folder ) :
		nonlocal i_TheRunStati
		if i_TheRunStati.RunState :
			m_tkntr.messagebox.showwarning(app_show_name(), "Run refused!\nSummatry counts by filename extension action is in progress!") 
			t05_label_countsbfe_status.configure( text = "No Run. Summatry counts by filename extension action is in progress." )
		else :
			# print( "t05_process_countsbfe" )
			# print( "p_lst_folder" )
			# print( p_lst_folder )
			if DoNotRunProcessesAsThreads():
				t05_throcess_countsbfe( [ p_lst_folder ] )
				# now cheat a bit and use the same method the thread would use to return the widgets to normal
				i_queue.put( RunStateMessages.t05_countsbfe_Done )
			else:
				# run the process in a thread
				p_multi_log.do_logs( "p", "Setting thread for t05_throcess_countsbfe")
				i_t5b_thrd = fi_Thread( target = t05_throcess_countsbfe, args = ( [ p_lst_folder ] ) )
				p_multi_log.do_logs( "p", "Starting thread t05_throcess_countsbfe")
				i_t5b_thrd.start()
				p_multi_log.do_logs( "p", "Leaving thread to run in background")
	def t05_on_button_countsbfe() :
		i_lst_folder = t05_get_folder_list()
		# print( "t05_on_button_countsbfe" )
		# print( "i_lst_folder" )
		# print( i_lst_folder )
		if len( i_lst_folder ) == 0 :
			m_tkntr.messagebox.showwarning(app_show_name(), "Run refused!\nThere are no folders listed!") 
			t05_label_countsbfe_status.configure( text = "No Run. There are no folders listed." )
		else :
			# m_tkntr.messagebox.showwarning(app_show_name(), "About to process:\n" + "][".join( i_lst_folder) )
			t05_process_countsbfe( i_lst_folder )
	# ----------------------------------------
	# For Tab Replatry
	# - Distinctry
	def t14_Dstnctry_not_running_else_say( str_action ) :
		# concept here is to return whether we're free to make settings and if not to show a message about it
		if i_TheRunStati.t14_Dstnctry_Ctrl_Lock :
			m_tkntr.messagebox.showwarning(app_show_name(), str_action + " blocked! A Distinctry run is in process.") 
			i_not_running = False
		else :
			i_not_running = True
		return i_not_running
	def t14_Dstnctry_label_path_A_on_rclick_clear() :
		t14_Dstnctry_var_path_A.set( "" )
	def t14_Dstnctry_on_button_browse_path_A():
		if t14_Dstnctry_not_running_else_say( "Browsing for Distinctry Source Path" ) :
			got_path = t14_Dstnctry_var_path_A.get()
			got_string = getpathref_fromuser( "Select Distinctry Source Path", got_path)
			if len( got_string) > 0 :
				t14_Dstnctry_var_path_A.set( got_string )
	def t14_Dstnctry_label_path_B_on_rclick_clear() :
		t14_Dstnctry_var_path_B.set( "" )
	def t14_Dstnctry_on_button_browse_path_B():
		if t14_Dstnctry_not_running_else_say( "Browsing for Distinctry Check Path" ) :
			got_path = t14_Dstnctry_var_path_B.get()
			got_string = getpathref_fromuser( "Select Distinctry Check Path", got_path)
			if len( got_string) > 0 :
				t14_Dstnctry_var_path_B.set( got_string )
	def t14_Dstnctry_label_path_C_on_rclick_clear() :
		t14_Dstnctry_var_path_C.set( "" )
	def t14_Dstnctry_on_button_browse_path_C():
		if t14_Dstnctry_not_running_else_say( "Browsing for Distinctry Destination Path" ) :
			got_path = t14_Dstnctry_var_path_C.get()
			got_string = getpathref_fromuser( "Select Distinctry Destination Path", got_path)
			if len( got_string) > 0 :
				t14_Dstnctry_var_path_C.set( got_string )
	# part of which to be adapted to the t14 replacement
	"""
	def t04_on_button_clonatry() :
		# set this up ready for when asynchronous running becomes the way
		nonlocal i_TheRunStati
		nonlocal p_multi_log
		if i_TheRunStati.RunState :
			if t04_not_running_else_say( "Hmm, only synchronous so probably shouldn't be seeing this!" ) :
				t04_label_clonatry_status.configure( text = "Abandoned processing" )
				i_TheRunStati.RunState = False
				t04_button_clonatry_text.set( Strng_RunProcess() )
		elif True : # t04_check_pathlists_for_clashes() :
			t04_label_clonatry_status.configure( text = "Processing..." )
			 # get all three paths
			i_str_patha = t04_var_patha.get()
			i_str_pathb = t04_var_pathb.get()
			i_str_pathc = t04_var_pathc.get()
			# now decide what to do
			i_no_action_msg = "Clonatry was not attempted. " + "\n"
			# get the combo box choice
			i_cmode_enum = t04_get_selected_clonatry_mode()
			if i_cmode_enum in ( apltry.EnumClonatryMode.DoClonatry, apltry.EnumClonatryMode.DoMergetry, \
					apltry.EnumClonatryMode.DoSynopsitry, apltry.EnumClonatryMode.DoMigratry ) :
				if True :
					i_do_action = False
					i_no_action_msg += "That option is not yet functional!"
				elif ( len(i_str_patha) > 0 ) and ( len(i_str_pathc) > 0 ) :
					i_do_action = True
				else :
					i_do_action = False
					i_no_action_msg += "Not given required paths!"
			elif i_cmode_enum in ( [ apltry.EnumClonatryMode.DoSkeletry ] ) :
				if ( len(i_str_patha) > 0 ) :
					i_do_action = True
					t_ask_check = m_tkntr.messagebox.askquestion( \
						"Clonatry White Anting", \
						"This is a VERY destructive action of deleting all files under the path" + \
						"\n" + i_str_patha + 
						"\n" + "Are you sure you want to do this?" )
					if t_ask_check == 'yes' :
						i_do_action = True
					else :
						i_do_action = False
						i_no_action_msg += "Action avoided by user."
				else :
					i_do_action = False
					i_no_action_msg += "Not given required path!"
			elif i_cmode_enum in ( [ apltry.EnumClonatryMode.DoDistinctry ] ) :
				if True :
					i_do_action = False
					i_no_action_msg += "That option is not yet functional!"
				elif ( len(i_str_patha) > 0 ) and ( len(i_str_pathb) > 0 )  and ( len(i_str_pathc) > 0 ) :
					i_do_action = True
				else :
					i_do_action = False
					i_no_action_msg += "Not given required paths!"
			else :
				i_do_action = False
				i_no_action_msg += "No mode selection made!"
			if i_do_action :
				t04_button_clonatry_text.set( Strng_HaltProcess() )
				if DoNotRunProcessesAsThreads():
					t04_throcess_clonatry( i_cmode_enum, i_str_patha, i_str_pathb, i_str_pathc )
					# now cheat a bit and use the same method the thread would use to return the widgets to normal
					i_queue.put( RunStateMessages.t14_Skltry_Done )
				else:
					# run the process in a thread
					p_multi_log.do_logs( "spb", "Setting thread for t04_throcess_clonatry")
					i_t9_thrd = fi_Thread( target = t04_throcess_clonatry, args = ( i_cmode_enum, i_str_patha, i_str_pathb, i_str_pathc ) )
					p_multi_log.do_logs( "spb", "Starting thread t04_throcess_clonatry")
					i_t9_thrd.start()
					p_multi_log.do_logs( "spb", "Leaving thread to run in background")
			else :
				t04_label_clonatry_status.configure( text = i_no_action_msg )
				m_tkntr.messagebox.showwarning(app_show_name(), i_no_action_msg ) 
		else :
			m_tkntr.messagebox.showwarning(app_show_name(), "Run refused!\nThere is a clash somewhere across the lists!") 
			t04_label_clonatry_status.configure( text = "No Run. There is a clash somewhere!" )
	"""
	def t14_Dstnctry_on_button_Process():
		m_tkntr.messagebox.showwarning(app_show_name(), "Sorry, not yet implemented.")
	# --- Clonatry
	def t14_Clntry_not_running_else_say( str_action ) :
		# concept here is to return whether we're free to make settings and if not to show a message about it
		if i_TheRunStati.t14_Clntry_Ctrl_Lock :
			m_tkntr.messagebox.showwarning(app_show_name(), str_action + " blocked! A Clonatry run is in process.") 
			i_not_running = False
		else :
			i_not_running = True
		return i_not_running
	def t14_Clntry_label_path_A_on_rclick_clear() :
		t14_Clntry_var_path_A.set( "" )
	def t14_Clntry_on_button_browse_path_A():
		if t14_Clntry_not_running_else_say( "Browsing for Clonatry Source Path" ) :
			got_path = t14_Clntry_var_path_A.get()
			got_string = getpathref_fromuser( "Select Clonatry Source Path", got_path)
			if len( got_string) > 0 :
				t14_Clntry_var_path_A.set( got_string )
	def t14_Clntry_label_path_C_on_rclick_clear() :
		t14_Clntry_var_path_B.set( "" )
	def t14_Clntry_on_button_browse_path_B():
		if t14_Clntry_not_running_else_say( "Browsing for Clonatry Source Path" ) :
			got_path = t14_Clntry_var_path_B.get()
			got_string = getpathref_fromuser( "Select Clonatry Destination Path", got_path)
			if len( got_string) > 0 :
				t14_Clntry_var_path_B.set( got_string )
	def t14_Clntry_throcess( p_str_path_a, p_str_path_b, \
		p_RecreateSrcFldr, p_KeepTuch, p_KeepAttr, p_KeepPrmt, p_KeepMeta ) :
		nonlocal p_multi_log
		nonlocal i_TheRunStati
		nonlocal i_queue
		i_TheRunStati.RunState = True
		i_TheRunStati.t14_Clntry_Ctrl_Lock = True
		p_multi_log.do_logs( "r", "Calling clonatry_command:")
		p_multi_log.do_logs( "r", "Path A: " + cmntry.pfn4print( p_str_path_a ) )
		p_multi_log.do_logs( "r", "Path B: " + cmntry.pfn4print( p_str_path_b ) )
		clonatry_command( p_str_path_a, p_str_path_b, p_RecreateSrcFldr, p_KeepTuch, p_KeepAttr, p_KeepPrmt, p_KeepMeta, p_multi_log)
		str_outcome = "- outcome:"
		p_multi_log.do_logs( "r", str_outcome)
		i_queue.put( RunStateMessages.t14_Clntry_Done )
	def t14_Clntry_on_button_Process():
		# set this up ready for when asynchronous running becomes the way
		nonlocal i_TheRunStati
		nonlocal p_multi_log
		if i_TheRunStati.RunState :
			if t14_Clntry_not_running_else_say( "Hmm, only synchronous so probably shouldn't be seeing this!" ) :
				t14_Clntry_label_Process_Status.configure( text = "Abandoned processing" )
				i_TheRunStati.RunState = False
				t14_Clntry_button_Process_text.set( Strng_RunProcess() )
		else :
			t14_Clntry_label_Process_Status.configure( text = "Checking..." )
			i_nodo_msg = ""
			i_do_action = True
			 # get all three paths
			i_str_path_a = t14_Clntry_entry_path_A.get()
			i_str_path_b = t14_Clntry_entry_path_B.get()
			if ( i_str_path_a is None ) or ( len( i_str_path_a) == 0 ) or ( i_str_path_b is None ) or ( len( i_str_path_b) == 0 ) :
				i_do_action = False
				i_nodo_msg = "Both paths must be provided!"
			if i_do_action :
				t_any_clash, t_any_sub = explrtry.pathlist_self_clash( [ i_str_path_a, i_str_path_b ], False, p_multi_log )
				if t_any_clash :
					i_do_action = False
					i_nodo_msg = "There is a path clash!"
			if i_do_action :
				t_path_b_is_empty = explrtry.path_check_is_empty( i_str_path_b )
				if not t_path_b_is_empty :
					i_do_action = False
					i_nodo_msg = "Destination path must be empty!"
			if i_do_action :
				# even though not using these values yet, this code to fetch values from the controls forces Python to instantiate them
				# otherwise you won't see their default values showing up, all would just start the session as Off
				i_RecreateSrcFldr = t14_Clntry_chkvr_RecreateSrcFldr.get()
				i_KeepTuch = t14_Clntry_chkvr_KeepTuch.get()
				i_KeepAttr = t14_Clntry_chkvr_KeepAttr.get()
				i_KeepPrmt = t14_Clntry_chkvr_KeepPrmt.get()
				i_KeepMeta = t14_Clntry_chkvr_KeepMeta.get()
				t14_Clntry_label_Process_Status.configure( text = "Processing..." )
				t14_Clntry_button_Process_text.set( Strng_HaltProcess() )
				if DoNotRunProcessesAsThreads():
					t14_Clntry_throcess( i_str_path_a, i_str_path_b, i_RecreateSrcFldr, i_KeepTuch, i_KeepAttr, i_KeepPrmt, i_KeepMeta )
					# now cheat a bit and use the same method the thread would use to return the widgets to normal
					i_queue.put( RunStateMessages.t14_Clntry_Done )
				else:
					# run the process in a thread
					p_multi_log.do_logs( "spb", "Setting thread for t14_Clntry_throcess")
					i_t14_Clntry_thrd = fi_Thread( target = t14_Clntry_throcess, args = ( i_str_path_a, i_str_path_b, i_RecreateSrcFldr, i_KeepTuch, i_KeepAttr, i_KeepPrmt, i_KeepMeta ) )
					p_multi_log.do_logs( "spb", "Starting thread t14_Clntry_throcess")
					i_t14_Clntry_thrd.start()
					p_multi_log.do_logs( "spb", "Leaving thread to run in background")
			else :
				m_tkntr.messagebox.showwarning(app_show_name(), "Run refused!\n" + i_nodo_msg ) 
				t14_Clntry_label_Process_Status.configure( text = "No Run. " + i_nodo_msg )
		# m_tkntr.messagebox.showwarning(app_show_name(), "Sorry, not yet implemented.")
	# ----------------------------------------
	# --- Dubatry ----------------------------------------------------
	def t14_Dbtry_label_path_A_on_rclick_clear() :
		t14_Dbtry_var_path_A.set( "" )
	def t14_Dbtry_on_button_browse_path_A():
		if t14_not_running_else_say( "Browsing for Dubatry Source Path" ) :
			got_path = t14_Dbtry_var_path_A.get()
			got_string = getpathref_fromuser( "Select Dubatry Source Path", got_path)
			if len( got_string) > 0 :
				t14_Dbtry_var_path_A.set( got_string )
	def t14_Dbtry_label_path_C_on_rclick_clear() :
		t14_Dbtry_var_path_C.set( "" )
	def t14_Dbtry_on_button_browse_path_C():
		if t14_not_running_else_say( "Browsing for Dubatry Source Path" ) :
			got_path = t14_Dbtry_var_path_C.get()
			got_string = getpathref_fromuser( "Select Dubatry Destination Path", got_path)
			if len( got_string) > 0 :
				t14_Dbtry_var_path_C.set( got_string )
	def t14_Dbtry_on_button_Process():
		m_tkntr.messagebox.showwarning(app_show_name(), "Sorry, not yet implemented.")
	# --- Mergetry
	def t14_Mrgtry_label_path_A_on_rclick_clear() :
		t14_Mrgtry_var_path_A.set( "" )
	def t14_Mrgtry_on_button_browse_path_A():
		if t14_not_running_else_say( "Browsing for Mergetry Source Path" ) :
			got_path = t14_Mrgtry_var_path_A.get()
			got_string = getpathref_fromuser( "Select Mergetry Source Path", got_path)
			if len( got_string) > 0 :
				t14_Mrgtry_var_path_A.set( got_string )
	def t14_Mrgtry_label_path_C_on_rclick_clear() :
		t14_Mrgtry_var_path_C.set( "" )
	def t14_Mrgtry_on_button_browse_path_C():
		if t14_not_running_else_say( "Browsing for Mergetry Destination Path" ) :
			got_path = t14_Mrgtry_var_path_C.get()
			got_string = getpathref_fromuser( "Select Mergetry Destination Path", got_path)
			if len( got_string) > 0 :
				t14_Mrgtry_var_path_C.set( got_string )
	def t14_Mrgtry_on_button_Process():
		m_tkntr.messagebox.showwarning(app_show_name(), "Sorry, not yet implemented.")
	# --- Synopsitry ----------------------------------------------------
	def t14_Synpstry_not_running_else_say( str_action ) :
		# concept here is to return whether we're free to make settings and if not to show a message about it
		if i_TheRunStati.t14_Synpstry_Ctrl_Lock :
			m_tkntr.messagebox.showwarning(app_show_name(), str_action + " blocked! A Synopsitry run is in process.") 
			i_not_running = False
		else :
			i_not_running = True
		return i_not_running
	def t14_Synpstry_label_path_A_on_rclick_clear() :
		t14_Synpstry_var_path_A.set( "" )
	def t14_Synpstry_on_button_browse_path_A():
		if t14_Synpstry_not_running_else_say( "Browsing for Synopsitry Source Path" ) :
			got_path = t14_Synpstry_var_path_A.get()
			got_string = getpathref_fromuser( "Select Synopsitry Source Path", got_path)
			if len( got_string) > 0 :
				t14_Synpstry_var_path_A.set( got_string )
	def t14_Synpstry_label_path_C_on_rclick_clear() :
		t14_Synpstry_var_path_C.set( "" )
	def t14_Synpstry_on_button_browse_path_C():
		if t14_Synpstry_not_running_else_say( "Browsing for Synopsitry Source Path" ) :
			got_path = t14_Synpstry_var_path_C.get()
			got_string = getpathref_fromuser( "Select Synopsitry Destination Path", got_path)
			if len( got_string) > 0 :
				t14_Synpstry_var_path_C.set( got_string )
	def t14_Synpstry_throcess( p_str_path_a, p_str_path_b, \
		p_RecreateSrcFldr, p_KeepTuch, p_KeepAttr, p_KeepPrmt, p_KeepMeta ) :
		nonlocal p_multi_log
		nonlocal i_TheRunStati
		nonlocal i_queue
		i_TheRunStati.RunState = True
		i_TheRunStati.t14_Synpstry_Ctrl_Lock = True
		p_multi_log.do_logs( "r", "Calling synopsitry_command:")
		p_multi_log.do_logs( "r", "Path A: " + cmntry.pfn4print( p_str_path_a ) )
		p_multi_log.do_logs( "r", "Path B: " + cmntry.pfn4print( p_str_path_b ) )
		synopsitry_command( p_str_path_a, p_str_path_b, p_RecreateSrcFldr, p_KeepTuch, p_KeepAttr, p_KeepPrmt, p_KeepMeta, p_multi_log)
		str_outcome = "- outcome:"
		p_multi_log.do_logs( "r", str_outcome)
		i_queue.put( RunStateMessages.t14_Synpstry_Done )
	def t14_Synpstry_on_button_Process():
		# set this up ready for when asynchronous running becomes the way
		nonlocal i_TheRunStati
		nonlocal p_multi_log
		if i_TheRunStati.RunState :
			if t14_Synpstry_not_running_else_say( "Hmm, only synchronous so probably shouldn't be seeing this!" ) :
				t14_Synpstry_label_Process_Status.configure( text = "Abandoned processing" )
				i_TheRunStati.RunState = False
				t14_Synpstry_button_Process_text.set( Strng_RunProcess() )
		else :
			t14_Synpstry_label_Process_Status.configure( text = "Checking..." )
			i_nodo_msg = ""
			i_do_action = True
			 # get all three paths
			i_str_path_a = t14_Synpstry_entry_path_A.get()
			i_str_path_b = t14_Synpstry_entry_path_C.get()
			if ( i_str_path_a is None ) or ( len( i_str_path_a) == 0 ) or ( i_str_path_b is None ) or ( len( i_str_path_b) == 0 ) :
				i_do_action = False
				i_nodo_msg = "Both paths must be provided!"
			if i_do_action :
				t_any_clash, t_any_sub = explrtry.pathlist_self_clash( [ i_str_path_a, i_str_path_b ], False, p_multi_log )
				if t_any_clash :
					i_do_action = False
					i_nodo_msg = "There is a path clash!"
			if i_do_action :
				t_path_b_is_empty = explrtry.path_check_is_empty( i_str_path_b )
				if not t_path_b_is_empty :
					i_do_action = False
					i_nodo_msg = "Destination path must be empty!"
			if i_do_action :
				# even though not using these values yet, this code to fetch values from the controls forces Python to instantiate them
				# otherwise you won't see their default values showing up, all would just start the session as Off
				i_RecreateSrcFldr = t14_Synpstry_chkvr_RecreateSrcFldr.get()
				i_KeepTuch = t14_Synpstry_chkvr_KeepTuch.get()
				i_KeepAttr = t14_Synpstry_chkvr_KeepAttr.get()
				i_KeepPrmt = t14_Synpstry_chkvr_KeepPrmt.get()
				i_KeepMeta = t14_Synpstry_chkvr_KeepMeta.get()
				t14_Synpstry_label_Process_Status.configure( text = "Processing..." )
				t14_Synpstry_button_Process_text.set( Strng_HaltProcess() )
				if DoNotRunProcessesAsThreads():
					t14_Synpstry_throcess( i_str_path_a, i_str_path_b, i_RecreateSrcFldr, i_KeepTuch, i_KeepAttr, i_KeepPrmt, i_KeepMeta )
					# now cheat a bit and use the same method the thread would use to return the widgets to normal
					i_queue.put( RunStateMessages.t14_Synpstry_Done )
				else:
					# run the process in a thread
					p_multi_log.do_logs( "spb", "Setting thread for t14_Synpstry_throcess")
					i_t14_Synpstry_thrd = fi_Thread( target = t14_Synpstry_throcess, args = ( i_str_path_a, i_str_path_b, i_RecreateSrcFldr, i_KeepTuch, i_KeepAttr, i_KeepPrmt, i_KeepMeta ) )
					p_multi_log.do_logs( "spb", "Starting thread t14_Synpstry_throcess")
					i_t14_Synpstry_thrd.start()
					p_multi_log.do_logs( "spb", "Leaving thread to run in background")
			else :
				m_tkntr.messagebox.showwarning(app_show_name(), "Run refused!\n" + i_nodo_msg ) 
				t14_Synpstry_label_Process_Status.configure( text = "No Run. " + i_nodo_msg )
		#m_tkntr.messagebox.showwarning(app_show_name(), "Sorry, not yet implemented.")
	# --- Skeletry ----------------------------------------------------
	def t14_Skltry_not_running_else_say( str_action ) :
		# concept here is to return whether we're free to make settings and if not to show a message about it
		if i_TheRunStati.t14_Skltry_Ctrl_Lock :
			m_tkntr.messagebox.showwarning(app_show_name(), str_action + " blocked! A Skeletry run is in process.") 
			i_not_running = False
		else :
			i_not_running = True
		return i_not_running
	def t14_Skltry_label_path_A_on_rclick_clear() :
		t14_Skltry_var_path_A.set( "" )
	def t14_Skltry_on_button_browse_path_A():
		if t14_Skltry_not_running_else_say( "Browsing for Skeletry Path" ) :
			got_path = t14_Skltry_var_path_A.get()
			got_string = getpathref_fromuser( "Select Skeletry Path", got_path)
			if len( got_string) > 0 :
				t14_Skltry_var_path_A.set( got_string )
	def t14_throcess_Skltry( p_str_path, p_EnumDeleteMode ) :
		nonlocal p_multi_log
		nonlocal i_TheRunStati
		nonlocal i_queue
		i_TheRunStati.RunState = True
		i_TheRunStati.t14_Ctrl_Lock = True
		p_multi_log.do_logs( "r", "Calling skeletry_command:")
		p_multi_log.do_logs( "r", "Path A: " + cmntry.pfn4print( p_str_path ) )
		skeletry_command( p_str_path, p_EnumDeleteMode, p_multi_log)
		str_outcome = "- outcome:"
		p_multi_log.do_logs( "r", str_outcome)
		i_queue.put( RunStateMessages.t14_Skltry_Done )
	def t14_Skltry_on_button_Process():
		nonlocal i_TheRunStati
		nonlocal p_multi_log
		i_no_action_msg = ""
		if i_TheRunStati.RunState :
			if t14_Skltry_not_running_else_say( "Hmm, only synchronous so probably shouldn't be seeing this!" ) :
				t14_Skltry_label_Process_Status.configure( text = "Abandoned processing" )
				i_TheRunStati.RunState = False
				t14_Skltry_button_Process_text.set( Strng_RunProcess() )
		else :
			t14_Skltry_label_Process_Status.configure( text = "Processing..." )
			i_str_path = t14_Skltry_var_path_A.get()
			i_EnumDeleteMode = t14_Skltry_cb_EnumDeleteMode.getSelectedKey()
			if ( len(i_str_path) > 0 ) :
				i_do_action = True
				t_ask_check = m_tkntr.messagebox.askquestion( \
					"Skeletry", \
					"This is a VERY destructive action of deleting all files under the path" + \
					"\n" + i_str_path + 
					"\n" + "Are you sure you want to do this?" )
				if t_ask_check == 'yes' :
					i_do_action = True
				else :
					i_do_action = False
					i_no_action_msg += "Action avoided by user."
			else :
				i_do_action = False
				i_no_action_msg += "Not given required path!"
			if i_do_action :
				t14_Skltry_button_Process_text.set( Strng_HaltProcess() )
				if DoNotRunProcessesAsThreads():
					t14_throcess_Skltry( i_str_path, i_EnumDeleteMode )
					# now cheat a bit and use the same method the thread would use to return the widgets to normal
					i_queue.put( RunStateMessages.t14_Skltry_Done )
				else:
					# run the process in a thread
					p_multi_log.do_logs( "spb", "Setting thread for t14_throcess_Skltry")
					i_t14_thrd_Skltry = fi_Thread( target = t14_throcess_Skltry, args = ( i_str_path, i_EnumDeleteMode ) )
					p_multi_log.do_logs( "spb", "Starting thread t14_throcess_Skltry")
					i_t14_thrd_Skltry.start()
					p_multi_log.do_logs( "spb", "Leaving thread t14_throcess_Skltry to run in background")
			else :
				t14_Skltry_label_Process_Status.configure( text = i_no_action_msg )
				m_tkntr.messagebox.showwarning(app_show_name(), i_no_action_msg ) 
	# --- Migratry ----------------------------------------------------
	def t14_Mgrtry_not_running_else_say( str_action ) :
		# concept here is to return whether we're free to make settings and if not to show a message about it
		if i_TheRunStati.t14_Mgrtry_Ctrl_Lock :
			m_tkntr.messagebox.showwarning(app_show_name(), str_action + " blocked! A Migratry run is in process.") 
			i_not_running = False
		else :
			i_not_running = True
		return i_not_running
	def t14_Mgrtry_label_path_A_on_rclick_clear() :
		t14_Mgrtry_var_path_A.set( "" )
	def t14_Mgrtry_on_button_browse_path_A():
		if t14_Mgrtry_not_running_else_say( "Browsing for Migratry Source Path" ) :
			got_path = t14_Mgrtry_var_path_A.get()
			got_string = getpathref_fromuser( "Select Migratry Source Path", got_path)
			if len( got_string) > 0 :
				t14_Mgrtry_var_path_A.set( got_string )
	def t14_Mgrtry_label_path_C_on_rclick_clear() :
		t14_Mgrtry_var_path_C.set( "" )
	def t14_Mgrtry_on_button_browse_path_C():
		if t14_Mgrtry_not_running_else_say( "Browsing for Migratry Source Path" ) :
			got_path = t14_Mgrtry_var_path_C.get()
			got_string = getpathref_fromuser( "Select Migratry Destination Path", got_path)
			if len( got_string) > 0 :
				t14_Mgrtry_var_path_C.set( got_string )
	def t14_throcess_Mgrtry( p_str_path_a, p_str_path_b, p_RecreateSrcFldr, p_KeepTuch, p_KeepAttr, p_KeepPrmt, p_KeepMeta ) :
		nonlocal p_multi_log
		nonlocal i_TheRunStati
		nonlocal i_queue
		i_TheRunStati.RunState = True
		i_TheRunStati.t14_Ctrl_Lock = True
		p_multi_log.do_logs( "rb", "Calling migratry_command:")
		p_multi_log.do_logs( "rb", "Path A: " + cmntry.pfn4print( p_str_path_a ) )
		p_multi_log.do_logs( "rb", "Path B: " + cmntry.pfn4print( p_str_path_b ) )
		migratry_command( p_str_path_a, p_str_path_b, p_RecreateSrcFldr, p_KeepTuch, p_KeepAttr, p_KeepPrmt, p_KeepMeta, p_multi_log)
		str_outcome = "- outcome:"
		p_multi_log.do_logs( "r", str_outcome)
		i_queue.put( RunStateMessages.t14_Mgrtry_Done )
	def t14_Mgrtry_on_button_Process():
		nonlocal i_TheRunStati
		nonlocal p_multi_log
		i_no_action_msg = ""
		if i_TheRunStati.RunState :
			if t14_Mgrtry_not_running_else_say( "Hmm, only synchronous so probably shouldn't be seeing this!" ) :
				t14_Mgrtry_label_Process_Status.configure( text = "Abandoned processing" )
				i_TheRunStati.RunState = False
				t14_Mgrtry_button_Process_text.set( Strng_RunProcess() )
		else :
			t14_Mgrtry_label_Process_Status.configure( text = "Processing..." )
			i_str_path_a = t14_Mgrtry_var_path_A.get()
			i_str_path_b = t14_Mgrtry_var_path_C.get()
			# i_EnumDeleteMode = t14_Mgrtry_cb_EnumDeleteMode.getSelectedKey() # currently no such option
			if ( len(i_str_path_a) > 0 ) and ( len(i_str_path_b) > 0 )  :
				i_do_action = True
				t_ask_check = m_tkntr.messagebox.askquestion( \
					"Migratry", \
					"This is a VERY destructive action of deleting all files under the path" + \
					"\n" + i_str_path_a + 
					"\n" + "Are you sure you want to do this?" )
				if t_ask_check == 'yes' :
					i_do_action = True
				else :
					i_do_action = False
					i_no_action_msg += "Action avoided by user."
			else :
				i_do_action = False
				i_no_action_msg += "Not given required path!"
			if i_do_action :
				# even though not using these values yet, this code to fetch values from the controls forces Python to instantiate them
				# otherwise you won't see their default values showing up, all would just start the session as Off
				i_RecreateSrcFldr = t14_Mgrtry_chkvr_RecreateSrcFldr.get()
				i_KeepTuch = t14_Mgrtry_chkvr_KeepTuch.get()
				i_KeepAttr = t14_Mgrtry_chkvr_KeepAttr.get()
				i_KeepPrmt = t14_Mgrtry_chkvr_KeepPrmt.get()
				i_KeepMeta = t14_Mgrtry_chkvr_KeepMeta.get()
				t14_Mgrtry_button_Process_text.set( Strng_HaltProcess() )
				if DoNotRunProcessesAsThreads():
					t14_throcess_Mgrtry( i_str_path_a, i_str_path_b, i_RecreateSrcFldr, i_KeepTuch, i_KeepAttr, i_KeepPrmt, i_KeepMeta )
					# now cheat a bit and use the same method the thread would use to return the widgets to normal
					i_queue.put( RunStateMessages.t14_Mgrtry_Done )
				else:
					# run the process in a thread
					p_multi_log.do_logs( "spb", "Setting thread for t14_throcess_Mgrtry")
					i_t14_thrd_Mgrtry = fi_Thread( target = t14_throcess_Mgrtry, args = ( i_str_path_a, i_str_path_b, i_RecreateSrcFldr, i_KeepTuch, i_KeepAttr, i_KeepPrmt, i_KeepMeta ) )
					p_multi_log.do_logs( "spb", "Starting thread t14_throcess_Mgrtry")
					i_t14_thrd_Mgrtry.start()
					p_multi_log.do_logs( "spb", "Leaving thread t14_throcess_Mgrtry to run in background")
			else :
				t14_Mgrtry_label_Process_Status.configure( text = i_no_action_msg )
				m_tkntr.messagebox.showwarning(app_show_name(), i_no_action_msg ) 
	#def t14_Mgrtry_on_button_Process():
	#	m_tkntr.messagebox.showwarning(app_show_name(), "Sorry, not yet implemented.")
	# --- Movetry ----------------------------------------------------
	def t14_Mvtry_not_running_else_say( str_action ) :
		# concept here is to return whether we're free to make settings and if not to show a message about it
		if i_TheRunStati.t14_Mvtry_Ctrl_Lock :
			m_tkntr.messagebox.showwarning(app_show_name(), str_action + " blocked! A Movetry run is in process.") 
			i_not_running = False
		else :
			i_not_running = True
		return i_not_running
	def t14_Mvtry_do_clashcheck_overcase():
		return t14_Mvtry_chkvr_clashovercase_paths_A.get() == 1
	# Path A List
	def t14_Mvtry_label_path_A_on_rclick_clear() :
		t14_Mvtry_var_path_A.set( "" )
	def t14_Mvtry_on_button_browse_path_A():
		if t14_Mvtry_not_running_else_say( "Browsing for Movetry Source Path" ) :
			got_path = t14_Mvtry_var_path_A.get()
			got_string = getpathref_fromuser( "Select Movetry Source Path", got_path)
			if len( got_string) > 0 :
				t14_Mvtry_var_path_A.set( got_string )
	def t14_Mvtry_get_folder_list() :
		tpl_folder = t14_Mvtry_lb_paths_A.get(0, m_tkntr.END)
		lst_folder = list( tpl_folder )
		return lst_folder
	def t14_Mvtry_check_path_for_folder_clashes( str_path ) :
		nonlocal p_multi_log
		i_lst_folder = t14_Mvtry_get_folder_list()
		clash_folder, made_sub_folder = explrtry.pathlist_path_clash( i_lst_folder, str_path, t14_Mvtry_do_clashcheck_overcase(), p_multi_log)
		return clash_folder, made_sub_folder
	def t14_Mvtry_check_folder_pathlist_for_clashes() :
		nonlocal p_multi_log
		i_lst_folder = t14_Mvtry_get_folder_list()
		any_clash, any_sub = explrtry.pathlist_self_clash( i_lst_folder, t14_Mvtry_do_clashcheck_overcase(), p_multi_log)
		return not any_clash
	def t14_Mvtry_path_addpath_if_okto( p_path):
		added_path = False
		if ( t14_Mvtry_chkvr_autoclashcheck_paths_A.get() == 1 ) :
			i_is_clash, i_is_sub = t14_Mvtry_check_path_for_folder_clashes( p_path )
			if not i_is_clash :
				t14_Mvtry_lb_paths_A.insert( 0, p_path)
				added_path = True
			else: 
				# provide info about the clashing
				str_advice = "That path would clash!"
				if i_is_sub :
					str_advice = str_advice + "\nMakes Sub-folder."
				elif i_is_clash :
					str_advice = str_advice + "\nAlready in list."
				m_tkntr.messagebox.showwarning(app_show_name() + " - Adding Survival Path", str_advice) 
		else :
			# go straight ahead and add, relying on user to then do check
			t14_Mvtry_lb_paths_A.insert( 0, p_path)
			added_path = True
		if added_path :
			t14_Mvtry_label_paths_A_alert.configure( text = "Added path: " + p_path)
			t14_Mvtry_Check_CanHaveOption_DeleteSourceFolder()
	def t14_Mvtry_on_button_add_folder_paths_A() :
		if t14_Mvtry_not_running_else_say( "Adding list item" ) :
			got_path = cmntry.hndy_str_to_goodpath( t14_Mvtry_var_path_A.get() )
			added_path = False
			if ( t14_Mvtry_chkvr_autoclashcheck_paths_A.get() == 1 ) :
				clash_path, is_sub_path = \
					t14_Mvtry_check_path_for_folder_clashes( got_path )
				if (not clash_path) :
					t14_Mvtry_lb_paths_A.insert( 0, got_path)
					added_path = True
				else: 
					# provide info about the clashing
					str_advice = "That path would clash!"
					if is_sub_path :
						str_advice = str_advice + "\nMakes Sub-folder in Movetry lists."
					elif clash_path :
						str_advice = str_advice + "\nAlready in Movetry lists."
					m_tkntr.messagebox.showwarning(app_show_name() + " - Adding Movetry list Path", str_advice) 
			else :
				# go straight ahead and add, relying on user to then do check
				t14_Mvtry_lb_paths_A.insert( 0, got_path)
				added_path = True
			if added_path :
				t14_Mvtry_label_paths_A_alert.configure( text = "Added path: " + got_path)
				if t14_Mvtry_chkvr_RecreateSrcFldr.get() != 1 and len( t14_Mvtry_get_folder_list() ) > 1 :
					t14_Mvtry_chkvr_RecreateSrcFldr.set(1)
					m_tkntr.messagebox.showwarning(app_show_name() + " - Adding Movetry list Path", "For more than one source you must have Recreate Source Folder") 
					t14_Mvtry_label_paths_A_alert.configure( text = "Recreate Source Folder is now on.")
				t14_Mvtry_Check_CanHaveOption_DeleteSourceFolder()
	def t14_Mvtry_on_button_add_subfolders_paths_A() :
		# the idea here will be to add all the next level sub-folders of the selected folder 
		# get the list of paths
		if t14_Mvtry_not_running_else_say( "Adding disposal list item" ) :
			got_path = cmntry.hndy_str_to_goodpath( t14_Mvtry_var_path_A.get() )
			i_lst_dspsl_pth = explrtry.Path_GetListOfFirstLevelSubPaths( got_path )
			for i_pth in i_lst_dspsl_pth :
				t14_Mvtry_path_addpath_if_okto( i_pth)
	def t14_Mvtry_on_button_remove_selected_path_A() :
		if t14_Mvtry_not_running_else_say( "Removing Movetry list list item" ) :
			lst_index = t14_Mvtry_lb_paths_A.curselection()
			if len( lst_index) > 0 :
				del_index = int(lst_index[0])
				was_path = t14_Mvtry_lb_paths_A.get(del_index)
				t14_Mvtry_lb_paths_A.delete( del_index)
				t14_Mvtry_label_paths_A_alert.configure( text = "Removed: " + was_path)
			else :
				t14_Mvtry_label_paths_A_alert.configure( text = "No Movetry list selection.")
			t14_Mvtry_Check_CanHaveOption_DeleteSourceFolder()
	def t14_Mvtry_on_dblclck_selected_folderlist() :
		if t14_Mvtry_not_running_else_say( "Noting Movetry list list item" ) :
			lst_index = t14_Mvtry_lb_paths_A.curselection()
			if len( lst_index) > 0 :
				del_index = int(lst_index[0])
				got_path = t14_Mvtry_lb_paths_A.get(del_index)
				t14_Mvtry_var_path_A.set( got_path)
	def t14_Mvtry_on_button_remove_all_paths_A() :
		if t14_Mvtry_not_running_else_say( "Clearing Movetry list" ) :
			t14_Mvtry_lb_paths_A.delete(0, m_tkntr.END) # clear
			t14_Mvtry_label_paths_A_alert.configure( text = "Cleared Movetry list.")
			t14_Mvtry_Check_CanHaveOption_DeleteSourceFolder()
	def t14_Mvtry_on_button_check_list_clashes_paths_A() :
		if t14_Mvtry_not_running_else_say( "Checking Movetry list for clashes" ) :
			if len(t14_Mvtry_get_folder_list()) > 0 :
				is_all_good = t14_Mvtry_check_folder_pathlist_for_clashes()
				if is_all_good :
					t14_Mvtry_label_paths_A_alert.configure( text = "All good, no clashes in Movetry list.")
				else :
					t14_Mvtry_label_paths_A_alert.configure( text = "Problems in Movetry list.")
					m_tkntr.messagebox.showwarning(app_show_name(), "Clashes in Movetry list!") 
			else :
				t14_Mvtry_label_paths_A_alert.configure( text = "Empty Movetry list.")
	def t14_Mvtry_on_chkbx_autoclashcheck_paths_A():
		if ( t14_Mvtry_chkvr_autoclashcheck_paths_A.get() == 1 ) :
			# so this means changing from Off to On
			# we should if all is ok before allowing this 
			if t14_Mvtry_check_folder_pathlist_for_clashes():
				t14_Mvtry_label_paths_A_alert.configure( text = "Auto is on.")
			else :
				t14_Mvtry_chkvr_autoclashcheck_paths_A.set(0)
				t14_Mvtry_label_paths_A_alert.configure( text = "Cannot return to Auto with clashes in lists.")
				m_tkntr.messagebox.showwarning(app_show_name(), "Cannot return to Auto with clashes in lists.") 
		else :
			t14_Mvtry_label_paths_A_alert.configure( text = "Auto is off.")
	def t14_Mvtry_on_chkbx_clashovercase_paths_A():
		if ( t14_Mvtry_chkvr_clashovercase_paths_A.get() == 1 ) :
			# so this means changing from Off to On
			if ( t14_Mvtry_chkvr_autoclashcheck_paths_A.get() == 1 ) :
				# we should if all is ok before allowing this 
				if t14_Mvtry_check_folder_pathlist_for_clashes():
					t14_Mvtry_label_paths_A_alert.configure( text = "Treating as Case Insensitive")
				else :
					t14_Mvtry_chkvr_clashovercase_paths_A.set(0)
					t14_Mvtry_label_paths_A_alert.configure( text = "There are clashes.")
					m_tkntr.messagebox.showwarning(app_show_name(), "Cannot go to Case Insensitive with clashes in lists.") 
			else :
				# no checking
				t14_Mvtry_label_paths_A_alert.configure( text = "Treating as Case Insensitive.")
		else :
			# so this means changing from On to Off
			t14_Mvtry_label_paths_A_alert.configure( text = "Set to case sensitive.")
	# Path C
	def t14_Mvtry_label_path_C_on_rclick_clear() :
		t14_Mvtry_var_path_C.set( "" )
	def t14_Mvtry_on_button_browse_path_C():
		if t14_Mvtry_not_running_else_say( "Browsing for Movetry Destination Path" ) :
			got_path = t14_Mvtry_var_path_C.get()
			got_string = getpathref_fromuser( "Select Movetry Destination Path", got_path)
			if len( got_string) > 0 :
				t14_Mvtry_var_path_C.set( got_string )
	def t14_Mvtry_on_button_check_path_C():
		t_got_path = t14_Mvtry_var_path_C.get()
		t_path_c_is_empty = explrtry.path_check_is_empty( t_got_path )
		if t_path_c_is_empty :
			# destination path is empty = all good
			t14_Mvtry_SetLabel_Process( "Destination path seems okay." )
		else :
			# check that each source folder does not exist in the destination
			i_lst_paths_a = t14_Mvtry_get_folder_list()
			i_not_ok = False
			i_bad_count = 0
			for i_path_a in i_lst_paths_a :
				# get the folder name
				a_name = explrtry.getname_fromfileref( i_path_a )
				# make the destination path for that
				a_dest_name = explrtry.join_path_name( t_got_path, a_name )
				# check that it does not exist
				if explrtry.does_path_exist( a_dest_name) :
					i_not_ok = True
					i_bad_count += 1
			if i_not_ok:
				t14_Mvtry_SetLabel_Process( "Oop " + str( i_bad_count) + " destination path is not okay." )
	#
	def t14_Mvtry_chkbx_DeleteSourceAfter_on_release():
		t14_Mvtry_Check_ModeAlert()
	def t14_Mvtry_chkbx_RecreateSrcFldr_on_release():
		t14_Mvtry_Check_ModeAlert()
	#
	def t14_Mvtry_Check_CanHaveOption_DeleteSourceFolder():
		i_lst_paths_a = t14_Mvtry_get_folder_list()
		if len( i_lst_paths_a) < 2 :
			t14_Mvtry_chkbx_DeleteSourceAfter.configure( state= m_tkntr.NORMAL)
		else :
			# force ON and disable
			t14_Mvtry_chkvr_DeleteSourceAfter.set( 1)
			t14_Mvtry_chkbx_DeleteSourceAfter.configure( state= m_tkntr.DISABLED)
	def t14_Mvtry_Get_IsModeStateSingle( p_lst_paths_a, p_RecreateSrcFldr ):
		r_is_single = ( len( p_lst_paths_a) < 2 ) and not p_RecreateSrcFldr
		return r_is_single
	def t14_Mvtry_SetLabel_ModeState( p_str ):
		t14_Mvtry_label_modestate.configure( text = p_str )
	def t14_Mvtry_Check_ModeState( ):
		i_lst_paths_a = t14_Mvtry_get_folder_list()
		i_RecreateSrcFldr = t14_Mvtry_chkvr_RecreateSrcFldr.get()
		t_is_single = t14_Mvtry_Get_IsModeStateSingle( )
		if t_is_single:
			t14_Mvtry_SetLabel_ModeState( "Single folder" )
		else :
			t14_Mvtry_SetLabel_ModeState( "Multi folder" )
	def t14_Mvtry_SetLabel_ModeAlert( p_str ):
		t14_Mvtry_label_modealert.configure( text = p_str )
	def t14_Mvtry_Check_ModeAlert():
		i_lst_paths_a = t14_Mvtry_get_folder_list()
		i_RecreateSrcFldr = t14_Mvtry_chkvr_RecreateSrcFldr.get()
		t_is_single = t14_Mvtry_Get_IsModeStateSingle( i_lst_paths_a, i_RecreateSrcFldr)
		i_DeleteSourceAfter = t14_Mvtry_chkvr_DeleteSourceAfter.get()
		if t_is_single and i_DeleteSourceAfter :
			t14_Mvtry_SetLabel_ModeAlert( "Warning! Will delete source folder." )
		else :
			t14_Mvtry_SetLabel_ModeAlert( "" ) # i.e. no alert display
	def t14_Mvtry_SetLabel_Process( p_str ):
		t14_Mvtry_label_Process_Status.configure( text = p_str )
	def t14_Mvtry_on_button_Process():
		nonlocal p_multi_log
		i_lst_paths_a = t14_Mvtry_get_folder_list()
		if len( i_lst_paths_a ) < 1 :
			t14_Mvtry_SetLabel_Process( "No source paths specified!" )
			return
		i_str_path_b = t14_Mvtry_var_path_C.get()
		if len( i_str_path_b ) < 1 :
			t14_Mvtry_SetLabel_Process( "No destination path specified!" )
			return
		i_DeleteBadCopy = t14_Mvtry_chkvr_DeleteBadCopy.get()
		i_DeleteSourceAfter = t14_Mvtry_chkvr_DeleteSourceAfter.get()
		#
		i_RecreateSrcFldr = t14_Mvtry_chkvr_RecreateSrcFldr.get()
		i_KeepTuch = t14_Mvtry_chkvr_KeepTuch.get()
		i_KeepAttr = t14_Mvtry_chkvr_KeepAttr.get()
		i_KeepPrmt = t14_Mvtry_chkvr_KeepPrmt.get()
		i_KeepMeta = t14_Mvtry_chkvr_KeepMeta.get()
		if t14_Mvtry_Get_IsModeStateSingle( i_lst_paths_a, i_RecreateSrcFldr ) :
			# get the one path instead of a list
			i_str_path_a = i_lst_paths_a[0]
			i_ran_ok, i_did_ok, i_bad_msg = movetry_command_content_of_one_folder( i_str_path_a, i_str_path_b, i_DeleteBadCopy, i_KeepTuch, i_KeepAttr, i_KeepPrmt, i_KeepMeta, p_multi_log)
		else:
			# later make check boxes for these
			i_AbandonAtFirstFailure = True
			i_DeleteDestinationOnFailure = True
			i_ran_ok, i_did_ok, i_bad_msg = movetry_command_list_of_folders( i_lst_paths_a, i_str_path_b, i_AbandonAtFirstFailure, i_DeleteDestinationOnFailure, \
				i_KeepTuch, i_KeepAttr, i_KeepPrmt, i_KeepMeta, p_multi_log)
		# for now, ignore the return values and rely on the logging
	# --- Shiftatry ----------------------------------------------------
	def t14_Shfttry_label_path_A_on_rclick_clear() :
		t14_Shfttry_var_path_A.set( "" )
	def t14_Shfttry_on_button_browse_path_A():
		if t14_not_running_else_say( "Browsing for Shiftatry Source Path" ) :
			got_path = t14_Shfttry_var_path_A.get()
			got_string = getpathref_fromuser( "Select Shiftatry Source Path", got_path)
			if len( got_string) > 0 :
				t14_Shfttry_var_path_A.set( got_string )
	def t14_Shfttry_label_path_C_on_rclick_clear() :
		t14_Shfttry_var_path_C.set( "" )
	def t14_Shfttry_on_button_browse_path_C():
		if t14_not_running_else_say( "Browsing for Shiftatry Destination Path" ) :
			got_path = t14_Shfttry_var_path_C.get()
			got_string = getpathref_fromuser( "Select Shiftatry Destination Path", got_path)
			if len( got_string) > 0 :
				t14_Shfttry_var_path_C.set( got_string )
	def t14_Shfttry_on_button_Process():
		m_tkntr.messagebox.showwarning(app_show_name(), "Sorry, not yet implemented.")
	# --- Shiftafile
	def t14_Shftfl_label_file_A_on_rclick_clear() :
		t14_Shftfl_var_file_A.set( "" )
	def t14_Shftfl_on_button_browse_file_A():
		if t14_not_running_else_say( "Browsing for Shiftafile Source File" ) :
			got_file = t14_Shftfl_var_file_A.get()
			# need to add step of getting the path from the fileref
			got_path = explrtry.getpath_fromfileref( got_file )
			got_string = getfileref_fromuser( "Select Shiftafile Source File", got_path)
			if len( got_string) > 0 :
				t14_Shftfl_var_file_A.set( got_string )
	def t14_Shftfl_label_path_C_on_rclick_clear() :
		t14_Shftfl_var_path_C.set( "" )
	def t14_Shftfl_on_button_browse_path_C():
		if t14_not_running_else_say( "Browsing for Shiftafile Destination Path" ) :
			got_path = t14_Shftfl_var_path_C.get()
			got_string = getpathref_fromuser( "Select Shiftafile Destination Path", got_path)
			if len( got_string) > 0 :
				t14_Shftfl_var_path_C.set( got_string )
	def t14_Shftfl_on_button_Process():
		m_tkntr.messagebox.showwarning(app_show_name(), "Sorry, not yet implemented.")
	# ----------------------------------------
	# For Tab Cartulatry
	def t15_not_running_else_say( str_action ) :
		# concept here is to return whether we're free to make settings and if not to show a message about it
		if i_TheRunStati.t15_Ctrl_Lock :
			m_tkntr.messagebox.showwarning(app_show_name(), str_action + " blocked! A Cartulatry run is in process.") 
			i_not_running = False
		else :
			i_not_running = True
		return i_not_running
	def t15_label_folder_on_rclick_clear() :
		t15_var_entry_folder.set( "" )
	def t15_on_button_browse_folder() :
		got_path = t15_var_entry_folder.get()
		got_string = getpathref_fromuser( "Select a Cartulatry path", got_path)
		t15_var_entry_folder.set( got_string )
	def t15_path_addpath_if_okto( p_path):
		added_path = False
		if ( t15_chkvr_autoclashcheck_paths.get() == 1 ) :
			i_is_clash, i_is_sub = t15_check_path_for_folder_clashes( p_path )
			if not i_is_clash :
				t15_lb_folderlist.insert( 0, p_path)
				added_path = True
			else: 
				# provide info about the clashing
				str_advice = "That path would clash!"
				if i_is_sub :
					str_advice = str_advice + "\nMakes Sub-folder."
				elif i_is_clash :
					str_advice = str_advice + "\nAlready in list."
				m_tkntr.messagebox.showwarning(app_show_name() + " - Adding Survival Path", str_advice) 
		else :
			# go straight ahead and add, relying on user to then do check
			t15_lb_folderlist.insert( 0, p_path)
			added_path = True
		if added_path :
			t15_label_alert_folderlist.configure( text = "Added path: " + p_path)

	def t15_on_button_add_folder() :
		if t15_not_running_else_say( "Adding list item" ) :
			got_path = cmntry.hndy_str_to_goodpath( t15_var_entry_folder.get() )
			added_path = False
			if ( t15_chkvr_autoclashcheck_paths.get() == 1 ) :
				clash_path, is_sub_path = \
					t15_check_path_for_folder_clashes( got_path )
				if (not clash_path) :
					t15_lb_folderlist.insert( 0, got_path)
					added_path = True
				else: 
					# provide info about the clashing
					str_advice = "That path would clash!"
					if is_sub_path :
						str_advice = str_advice + "\nMakes Sub-folder in Cartulatry lists."
					elif clash_path :
						str_advice = str_advice + "\nAlready in Cartulatry lists."
					m_tkntr.messagebox.showwarning(app_show_name() + " - Adding Cartulatry list Path", str_advice) 
			else :
				# go straight ahead and add, relying on user to then do check
				t15_lb_folderlist.insert( 0, got_path)
				added_path = True
			if added_path :
				t15_label_alert_folderlist.configure( text = "Added path: " + got_path)
	def t15_on_button_add_subfolders() :
		# the idea here will be to add all the next level sub-folders of the selected folder 
		# get the list of paths
		if t15_not_running_else_say( "Adding disposal list item" ) :
			got_path = cmntry.hndy_str_to_goodpath( t15_var_entry_folder.get() )
			i_lst_dspsl_pth = explrtry.Path_GetListOfFirstLevelSubPaths( got_path )
			for i_pth in i_lst_dspsl_pth :
				t15_path_addpath_if_okto( i_pth)
	def t15_on_button_remove_selected_folderlist() :
		if t15_not_running_else_say( "Removing Cartulatry list item" ) :
			lst_index = t15_lb_folderlist.curselection()
			if len( lst_index) > 0 :
				del_index = int(lst_index[0])
				was_path = t15_lb_folderlist.get(del_index)
				t15_lb_folderlist.delete( del_index)
				t15_label_alert_folderlist.configure( text = "Removed: " + was_path)
			else :
				t15_label_alert_folderlist.configure( text = "No Cartulatry list selection.")
	def t15_on_dblclck_selected_folderlist() :
		if t15_not_running_else_say( "Noting Cartulatry list item" ) :
			lst_index = t15_lb_folderlist.curselection()
			if len( lst_index) > 0 :
				del_index = int(lst_index[0])
				got_path = t15_lb_folderlist.get(del_index)
				t15_var_entry_folder.set( got_path)
	def t15_on_button_remove_all_folderlist() :
		if t15_not_running_else_say( "Clearing Cartulatry list" ) :
			t15_lb_folderlist.delete(0, m_tkntr.END) # clear
			t15_label_alert_folderlist.configure( text = "Cleared Cartulatry list.")
	def t15_on_button_check_list_clashes_folderlist() :
		if t15_not_running_else_say( "Checking Cartulatry list for clashes" ) :
			if len(t15_get_folder_list()) > 0 :
				is_all_good = t15_check_folder_pathlist_for_clashes()
				if is_all_good :
					t15_label_alert_folderlist.configure( text = "All good, no clashes in Cartulatry list.")
				else :
					t15_label_alert_folderlist.configure( text = "Problems in Cartulatry list.")
					m_tkntr.messagebox.showwarning(app_show_name(), "Clashes in Cartulatry list.!") 
			else :
				t15_label_alert_folderlist.configure( text = "Empty Cartulatry list.")
	def t15_on_chkbx_autoclashcheck_paths():
		if ( t15_chkvr_autoclashcheck_paths.get() == 1 ) :
			# so this means changing from Off to On
			# we should if all is ok before allowing this 
			if t15_check_folder_pathlist_for_clashes():
				t15_label_alert_folderlist.configure( text = "Auto is on.")
			else :
				t15_chkvr_autoclashcheck_paths.set(0)
				t15_label_alert_folderlist.configure( text = "Cannot return to Auto with clashes in lists.")
				m_tkntr.messagebox.showwarning(app_show_name(), "Cannot return to Auto with clashes in lists.") 
		else :
			t15_label_alert_folderlist.configure( text = "Auto is off.")
	# managing the Folder exclusions
	def t15_on_button_add_file_exclusion():
		if t15_not_running_else_say( "Adding list item" ):
			got_file_exclusion = t15_var_entry_file_exclusion.get()
			t15_lb_file_exclusions.insert( 0, got_file_exclusion)
			t15_label_alert_file_exclusions.configure( text = "Added exclusion: " + got_file_exclusion)
	def t15_on_button_remove_selected_file_exclusion():
		if t15_not_running_else_say( "Removing Cartulatry list item" ) :
			lst_index = t15_lb_file_exclusions.curselection()
			if len( lst_index) > 0 :
				del_index = int(lst_index[0])
				was_path = t15_lb_file_exclusions.get(del_index)
				t15_lb_file_exclusions.delete( del_index)
				t15_label_alert_file_exclusions.configure( text = "Removed: " + was_path)
			else :
				t15_label_alert_file_exclusions.configure( text = "No Cartulatry list selection.")
	def t15_on_button_remove_all_file_exclusions():
		if t15_not_running_else_say( "Clearing Cartulatry list" ) :
			t15_lb_file_exclusions.delete(0, m_tkntr.END) # clear
			t15_label_alert_file_exclusions.configure( text = "Cleared Cartulatry list.")
	# managing the Folder exclusions
	def t15_on_button_add_fold_exclusion():
		if t15_not_running_else_say( "Adding list item" ):
			got_fold_exclusion = t15_var_entry_fold_exclusion.get()
			t15_lb_fold_exclusions.insert( 0, got_fold_exclusion)
			t15_label_alert_fold_exclusions.configure( text = "Added exclusion: " + got_fold_exclusion)
	def t15_on_button_remove_selected_fold_exclusion():
		if t15_not_running_else_say( "Removing Cartulatry list item" ) :
			lst_index = t15_lb_fold_exclusions.curselection()
			if len( lst_index) > 0 :
				del_index = int(lst_index[0])
				was_path = t15_lb_fold_exclusions.get(del_index)
				t15_lb_fold_exclusions.delete( del_index)
				t15_label_alert_fold_exclusions.configure( text = "Removed: " + was_path)
			else :
				t15_label_alert_fold_exclusions.configure( text = "No Cartulatry list selection.")
	def t15_on_button_remove_all_fold_exclusions():
		if t15_not_running_else_say( "Clearing Cartulatry list" ) :
			t15_lb_fold_exclusions.delete(0, m_tkntr.END) # clear
			t15_label_alert_fold_exclusions.configure( text = "Cleared Cartulatry list.")
	# running the process
	def t15_throcess_cartulatry( p_lst_folders, p_IgnoreZeroLengthFiles, p_lst_excl_folds, p_fullpath_fold_exclusions, p_lst_excl_files, p_fullpath_file_exclusions, p_run_name ) :
		nonlocal i_TheRunStati
		nonlocal p_multi_log
		nonlocal i_queue
		i_TheRunStati.RunState = True
		i_TheRunStati.t15_Ctrl_Lock = True
		t15_label_action_status.configure( text = "Cartulatry..." )
		t15_button_action_var.set( Strng_HaltProcess() )
		p_multi_log.do_logs( "sp", "Calling ")
		cartulatry_command( p_lst_folders, p_IgnoreZeroLengthFiles, p_lst_excl_folds, p_fullpath_fold_exclusions, p_lst_excl_files, p_fullpath_file_exclusions, p_run_name, p_multi_log)
		p_multi_log.do_logs( "sp", "Back from ")
		i_queue.put( RunStateMessages.t15_Crtltry_Done )
		p_multi_log.do_logs( "sp", "Completed t15_throcess_cartulatry " + (40 * "%") )
	def t15_on_button_cartulatry() :
		# set this up ready for when asynchronous running becomes the way
		nonlocal i_TheRunStati
		nonlocal p_multi_log
		t06_t5_get_log_flags_from_checkboxes()
		i_lst_folders = t15_get_folder_list()
		cando_fldrlst = len( i_lst_folders) > 0
		cando_clashes = t15_check_folder_pathlist_for_clashes()
		if i_TheRunStati.RunState :
			if not_running_else_say( "Hmm, only synchronous so probably shouldn't be seeing this!" ) :
				t15_label_action_status.configure( text = "Abandoned processing." )
				i_TheRunStati.RunState = False
				t15_button_action_var.set( Strng_RunProcess() )
		elif cando_fldrlst and cando_clashes :
			# we will run the process in a thread
			# collect the settings
			i_IgnoreZeroLengthFiles = t15_chkvr_IgnoreZeroLengthFiles.get()
			i_run_name = t15_var_run_name.get()
			i_lst_excl_folds = t15_get_fold_exclusions_list()
			i_fullpath_fold_exclusions = t15_chkvr_fullpath_fold_exclusions.get()
			i_lst_excl_files = t15_get_file_exclusions_list()
			i_fullpath_file_exclusions = t15_chkvr_fullpath_file_exclusions.get()
			#
			if DoNotRunProcessesAsThreads():
				t15_throcess_cartulatry( i_lst_folders, i_IgnoreZeroLengthFiles, i_lst_excl_folds, i_fullpath_fold_exclusions, i_lst_excl_files, i_fullpath_file_exclusions, i_run_name )
				# now cheat a bit and use the same method the thread would use to return the widgets to normal
				i_queue.put( RunStateMessages.t15_Crtltry_Done )
			else:
				# run the process in a thread
				p_multi_log.do_logs( "spb", "Setting thread for t15_throcess_cartulatry")
				i_t5a_thrd = fi_Thread( target = t15_throcess_cartulatry, args = ( [ i_lst_folders, i_IgnoreZeroLengthFiles, i_lst_excl_folds, i_fullpath_fold_exclusions, i_lst_excl_files, i_fullpath_file_exclusions, i_run_name ] ) ) # remember that args must be a list
				p_multi_log.do_logs( "spb", "Starting thread t15_throcess_cartulatry")
				i_t5a_thrd.start()
				p_multi_log.do_logs( "spb", "Leaving thread to run in background")
		elif not cando_clashes :
			m_tkntr.messagebox.showwarning(app_show_name(), "Run refused!\nThere is a clash somewhere across the lists!") 
			t15_label_action_status.configure( text = "No Run. There is a clash somewhere!" )
		elif not cando_fldrlst :
			m_tkntr.messagebox.showwarning(app_show_name(), "Run refused!\nThere are no folders listed!") 
			t15_label_action_status.configure( text = "No Run. There are no folders listed" )
	# ----------------------------------------
	# For Tab Trimatry
	def t16_not_running_else_say( str_action ) :
		# concept here is to return whether we're free to make settings and if not to show a message about it
		if i_TheRunStati.t16_Ctrl_Lock :
			m_tkntr.messagebox.showwarning(app_show_name(), str_action + " blocked! A Trimatry run is in process.") 
			i_not_running = False
		else :
			i_not_running = True
		return i_not_running
	def t16_label_folder_on_rclick_clear() :
		t16_var_entry_folder.set( "" )
	def t16_on_button_browse_folder() :
		got_path = t16_var_entry_folder.get()
		got_string = getpathref_fromuser( "Select a Trimatry path", got_path)
		t16_var_entry_folder.set( got_string )
	def t16_path_addpath_if_okto( p_path):
		added_path = False
		if ( t16_chkvr_autoclashcheck_paths.get() == 1 ) :
			i_is_clash, i_is_sub = t16_check_path_for_folder_clashes( p_path )
			if not i_is_clash :
				t16_lb_folderlist.insert( 0, p_path)
				added_path = True
			else: 
				# provide info about the clashing
				str_advice = "That path would clash!"
				if i_is_sub :
					str_advice = str_advice + "\nMakes Sub-folder."
				elif i_is_clash :
					str_advice = str_advice + "\nAlready in list."
				m_tkntr.messagebox.showwarning(app_show_name() + " - Adding Survival Path", str_advice) 
		else :
			# go straight ahead and add, relying on user to then do check
			t16_lb_folderlist.insert( 0, p_path)
			added_path = True
		if added_path :
			t16_label_alert_folderlist.configure( text = "Added path: " + p_path)
	def t16_on_button_add_folder() :
		if t16_not_running_else_say( "Adding list item" ) :
			got_path = cmntry.hndy_str_to_goodpath( t16_var_entry_folder.get() )
			added_path = False
			if ( t16_chkvr_autoclashcheck_paths.get() == 1 ) :
				clash_path, is_sub_path = \
					t16_check_path_for_folder_clashes( got_path )
				if (not clash_path) :
					t16_lb_folderlist.insert( 0, got_path)
					added_path = True
				else: 
					# provide info about the clashing
					str_advice = "That path would clash!"
					if is_sub_path :
						str_advice = str_advice + "\nMakes Sub-folder in Trimatry lists."
					elif clash_path :
						str_advice = str_advice + "\nAlready in Trimatry lists."
					m_tkntr.messagebox.showwarning(app_show_name() + " - Adding Trimatry list Path", str_advice) 
			else :
				# go straight ahead and add, relying on user to then do check
				t16_lb_folderlist.insert( 0, got_path)
				added_path = True
			if added_path :
				t16_label_alert_folderlist.configure( text = "Added path: " + got_path)
	def t16_on_button_add_subfolders() :
		# the idea here will be to add all the next level sub-folders of the selected folder 
		# get the list of paths
		if t16_not_running_else_say( "Adding list item" ) :
			got_path = cmntry.hndy_str_to_goodpath( t16_var_entry_folder.get() )
			i_lst_dspsl_pth = explrtry.Path_GetListOfFirstLevelSubPaths( got_path )
			for i_pth in i_lst_dspsl_pth :
				t16_path_addpath_if_okto( i_pth)
	def t16_on_button_remove_selected_folderlist() :
		if t16_not_running_else_say( "Removing Trimatry list item" ) :
			lst_index = t16_lb_folderlist.curselection()
			if len( lst_index) > 0 :
				del_index = int(lst_index[0])
				was_path = t16_lb_folderlist.get(del_index)
				t16_lb_folderlist.delete( del_index)
				t16_label_alert_folderlist.configure( text = "Removed: " + was_path)
			else :
				t16_label_alert_folderlist.configure( text = "No Trimatry list selection.")
	def t16_on_dblclck_selected_folderlist() :
		if t16_not_running_else_say( "Noting Trimatry list item" ) :
			lst_index = t16_lb_folderlist.curselection()
			if len( lst_index) > 0 :
				del_index = int(lst_index[0])
				got_path = t16_lb_folderlist.get(del_index)
				t16_var_entry_folder.set( got_path)
	def t16_on_button_remove_all_folderlist() :
		if t16_not_running_else_say( "Clearing Trimatry list" ) :
			t16_lb_folderlist.delete(0, m_tkntr.END) # clear
			t16_label_alert_folderlist.configure( text = "Cleared Trimatry list.")
	def t16_on_button_check_list_clashes_folderlist() :
		if t16_not_running_else_say( "Checking Trimatry list for clashes" ) :
			if len(t16_get_folder_list()) > 0 :
				is_all_good = t16_check_folder_pathlist_for_clashes()
				if is_all_good :
					t16_label_alert_folderlist.configure( text = "All good, no clashes in Trimatry list.")
				else :
					t16_label_alert_folderlist.configure( text = "Problems in Trimatry list.")
					m_tkntr.messagebox.showwarning(app_show_name(), "Clashes in Trimatry list.!") 
			else :
				t16_label_alert_folderlist.configure( text = "Empty Trimatry list.")
	def t16_on_chkbx_autoclashcheck_paths():
		if ( t16_chkvr_autoclashcheck_paths.get() == 1 ) :
			# so this means changing from Off to On
			# we should if all is ok before allowing this 
			if t16_check_folder_pathlist_for_clashes():
				t16_label_alert_folderlist.configure( text = "Auto is on.")
			else :
				t16_chkvr_autoclashcheck_paths.set(0)
				t16_label_alert_folderlist.configure( text = "Cannot return to Auto with clashes in lists.")
				m_tkntr.messagebox.showwarning(app_show_name(), "Cannot return to Auto with clashes in lists.") 
		else :
			t16_label_alert_folderlist.configure( text = "Auto is off.")
	# Elements for calling summatry_command
	def t16_throcess_trimatry( p_lst_folder, p_DoDeleteEmptyFiles, p_DoDeleteEmptyFolds, p_CanDeleteTopFolders, p_TreatNestedEmptyFolders, p_TreatZeroesAsNotThere, p_TreatKeepIfEmptyAsPresent, p_TreatNoMediaAsPresent, \
			p_RenameFoldersRatherThanDelete, p_EnumDeleteMode, p_del_from_deep ) :
		nonlocal i_TheRunStati
		nonlocal p_multi_log
		nonlocal i_queue
		i_TheRunStati.RunState = True
		i_TheRunStati.t16_Ctrl_Lock = True
		t16_label_action_status.configure( text = "Trimatry..." )
		t16_button_action_var.set( Strng_HaltProcess() )
		p_multi_log.do_logs( "p", "Calling trimatry_command")
		#for f in i_lst_folder :
		#print( "Folder to summarise: " + f )
		trimatry_command( p_lst_folder, p_DoDeleteEmptyFiles, p_DoDeleteEmptyFolds, p_CanDeleteTopFolders, p_TreatNestedEmptyFolders, \
			p_TreatZeroesAsNotThere, p_TreatKeepIfEmptyAsPresent, p_TreatNoMediaAsPresent, p_RenameFoldersRatherThanDelete, p_EnumDeleteMode, p_del_from_deep, p_multi_log)
		p_multi_log.do_logs( "sp", "Back from trimatry_command")
		#t16_label_action_status.configure( text = "Trimatry was run." )
		#t16_button_action_var.set("Run Trimatry !")
		i_queue.put( RunStateMessages.t16_Trim_Done )
		p_multi_log.do_logs( "sp", "Completed t16_throcess_trimatry " + (40 * "%") )
	def t16_on_button_trimatry() :
		# set this up ready for when asynchronous running becomes the way
		nonlocal i_TheRunStati
		nonlocal p_multi_log
		t06_t5_get_log_flags_from_checkboxes( )
		i_lst_folder = t16_get_folder_list()
		#for f in i_lst_folder :
		#( "Folder to summarise: " + f )
		i_DoDeleteEmptyFiles = t16_chkvr_DoDeleteEmptyFiles.get()
		i_DoDeleteEmptyFolds = t16_chkvr_DoDeleteEmptyFolds.get()
		cando_dowhat = i_DoDeleteEmptyFiles or i_DoDeleteEmptyFolds
		cando_fldrlst = len( i_lst_folder) > 0
		cando_clashes = t16_check_folder_pathlist_for_clashes()
		if i_TheRunStati.RunState :
			if not_running_else_say( "Hmm, only synchronous so probably shouldn't be seeing this!" ) :
				t16_label_action_status.configure( text = "Abandoned processing." )
				i_TheRunStati.RunState = False
				t16_button_action_var.set( Strng_RunProcess() )
		elif cando_dowhat and cando_fldrlst and cando_clashes :
			# we will run the process in a thread
			# collect the settings
			i_CanDeleteTopFolders = t16_chkvr_CanDeleteTopFolders.get()
			i_TreatNestedEmptyFolders = t16_chkvr_TreatNestedEmptyFolders.get()
			i_TreatZeroesAsNotThere = t16_chkvr_TreatZeroesAsNotThere.get()
			i_TreatKeepIfEmptyAsPresent = t16_chkvr_TreatKeepIfEmptyAsPresent.get()
			i_TreatNoMediaAsPresent = t16_chkvr_TreatNoMediaAsPresent.get()
			i_RenameFoldersRatherThanDelete = t16_chkvr_RenameFoldersRatherThanDelete.get()
			i_EnumDeleteMode = t16_cb_EnumDeleteMode.getSelectedKey()
			i_del_from_deep = t16_chkvr_DelFromDeep.get()
			#
			if DoNotRunProcessesAsThreads():
				t16_throcess_trimatry( i_lst_folder, i_DoDeleteEmptyFiles, i_DoDeleteEmptyFolds, \
					i_CanDeleteTopFolders, i_TreatNestedEmptyFolders, i_TreatZeroesAsNotThere, i_TreatKeepIfEmptyAsPresent, i_TreatNoMediaAsPresent, \
					i_RenameFoldersRatherThanDelete, i_EnumDeleteMode, i_del_from_deep )
				# now cheat a bit and use the same method the thread would use to return the widgets to normal
				i_queue.put( RunStateMessages.t16_Trim_Done )
			else:
				# run the process in a thread
				p_multi_log.do_logs( "p", "Setting thread for t16_throcess_trimatry")
				i_t5a_thrd = fi_Thread( target = t16_throcess_trimatry, args = ( i_lst_folder, i_DoDeleteEmptyFiles, i_DoDeleteEmptyFolds, \
					i_CanDeleteTopFolders, i_TreatNestedEmptyFolders, i_TreatZeroesAsNotThere, i_TreatKeepIfEmptyAsPresent, i_TreatNoMediaAsPresent, \
					i_RenameFoldersRatherThanDelete, i_EnumDeleteMode, i_del_from_deep ) )
				p_multi_log.do_logs( "p", "Starting thread t16_throcess_trimatry")
				i_t5a_thrd.start()
				p_multi_log.do_logs( "p", "Leaving thread to run in background")
		elif not cando_clashes :
			m_tkntr.messagebox.showwarning(app_show_name(), "Run refused!\nThere is a clash somewhere across the lists!") 
			t16_label_action_status.configure( text = "No Run. There is a clash somewhere!" )
		elif not cando_fldrlst :
			m_tkntr.messagebox.showwarning(app_show_name(), "Run refused!\nThere are no folders listed!") 
			t16_label_action_status.configure( text = "No Run. There are no folders listed" )
		elif not cando_dowhat :
			m_tkntr.messagebox.showwarning(app_show_name(), "Run refused!\nNeither Files nor Folders set for deletion!") 
			t16_label_action_status.configure( text = "No Run. Neither Files nor Folders set for deletion!" )

	# ----------------------------------------
	# For Tab Organitry Subtab Canprunatry
	def t17_st01_not_running_else_say( str_action ) :
		# concept here is to return whether we're free to make settings and if not to show a message about it
		if i_TheRunStati.t17_st01_Ctrl_Lock :
			m_tkntr.messagebox.showwarning(app_show_name(), str_action + " blocked! A Defoliatry run is in process.") 
			i_not_running = False
		else :
			i_not_running = True
		return i_not_running
	def t17_st01_update_label_re_warning( p_IsRisky ) :
		if p_IsRisky :
			i_txt_Risk = "Warning! Immediate deletion without content checking."
			i_fg = "red"
		else :
			i_txt_Risk = "_"
			i_fg = "black"
		t17_st01_label_re_warning.configure( text = i_txt_Risk, fg = i_fg )
	def t17_label_paths_on_rclick_clear() :
		t17_st01_var_path.set( "" )
	def t17_st01_on_button_browse_paths() :
		got_path = t17_st01_var_path.get()
		got_string = getpathref_fromuser( "Select a path", got_path)
		t17_st01_var_path.set( got_string )
	def t17_st01_on_button_add_folder() :
		if t17_st01_not_running_else_say( "Adding list item" ) :
			got_path = cmntry.hndy_str_to_goodpath( t17_st01_var_path.get() )
			added_path = False
			if ( t17_st01_chkvr_autoclashcheck_paths.get() == 1 ) :
				clash_path, is_sub_path = \
					t17_st01_check_path_for_clashes( got_path )
				if (not clash_path) :
					t17_st01_lb_paths.insert( 0, got_path)
					added_path = True
				else: 
					# provide info about the clashing
					str_advice = "That path would clash!"
					if is_sub_path :
						str_advice = str_advice + "\nMakes Sub-folder in paths."
					elif clash_path :
						str_advice = str_advice + "\nAlready in paths."
					m_tkntr.messagebox.showwarning(app_show_name() + " - Adding Path", str_advice) 
			else :
				# go straight ahead and add, relying on user to then do check
				t17_st01_lb_paths.insert( 0, got_path)
				added_path = True
			if added_path :
				t17_st01_label_paths_alert.configure( text = "Added path: " + got_path)
	def t17_st01_on_button_add_subfolders() :
		# the idea here will be to add all the next level sub-folders of the selected folder 
		# get the list of paths
		if t17_st01_not_running_else_say( "Adding disposal list item" ) :
			got_path = t17_st01_var_path.get()
			i_lst_dspsl_pth = explrtry.Path_GetListOfFirstLevelSubPaths( got_path )
			for i_pth in i_lst_dspsl_pth :
				t17_st01_path_addpath_if_okto( i_pth)
	def t17_st01_on_button_remove_selected_path() :
		if t17_st01_not_running_else_say( "Removing list item" ) :
			lst_index = t17_st01_lb_paths.curselection()
			if len( lst_index) > 0 :
				del_index = int(lst_index[0])
				was_path = t17_st01_lb_paths.get(del_index)
				t17_st01_lb_paths.delete( del_index)
				t17_st01_label_paths_alert.configure( text = "Removed: " + was_path)
			else :
				t17_st01_label_paths_alert.configure( text = "No selection.")
	def t17_st01_on_dblclck_selected_path() :
		if t17_st01_not_running_else_say( "Noting list item" ) :
			lst_index = t17_st01_lb_paths.curselection()
			if len( lst_index) > 0 :
				del_index = int(lst_index[0])
				got_path = t17_st01_lb_paths.get(del_index)
				t17_st01_var_path.set( got_path)
	def t17_st01_on_button_remove_all_paths() :
		if t17_st01_not_running_else_say( "Clearing list" ) :
			t17_st01_lb_paths.delete(0, m_tkntr.END) # clear
			t17_st01_label_paths_alert.configure( text = "Cleared list.")
	def t17_st01_on_button_check_list_clashes_paths() :
		if t17_st01_not_running_else_say( "Checking list for clashes" ) :
			if len(t17_st01_get_paths_list()) > 0 :
				is_all_good = t17_st01_check_paths_for_clashes()
				if is_all_good :
					t17_st01_label_paths_alert.configure( text = "All good, no clashes in list.")
				else :
					t17_st01_label_paths_alert.configure( text = "Problems in list.")
					m_tkntr.messagebox.showwarning(app_show_name(), "Clashes in list.!") 
			else :
				t17_st01_label_paths_alert.configure( text = "Empty list.")
	def t17_st01_on_chkbx_autoclashcheck_paths():
		if ( t17_st01_chkvr_autoclashcheck_paths.get() == 1 ) :
			# so this means changing from Off to On
			# we should if all is ok before allowing this 
			if t17_st01_check_paths_for_clashes():
				t17_st01_label_paths_alert.configure( text = "Auto is on.")
			else :
				t17_st01_chkvr_autoclashcheck_paths.set(0)
				t17_st01_label_paths_alert.configure( text = "Cannot return to Auto with clashes in lists.")
				m_tkntr.messagebox.showwarning(app_show_name(), "Cannot return to Auto with clashes in lists.") 
		else :
			t17_st01_label_paths_alert.configure( text = "Auto is off.")
	def t17_st01_throcess_dedupe( p_lst_paths, p_limit_top_n ) :
		nonlocal i_TheRunStati
		nonlocal p_multi_log
		nonlocal i_queue
		i_TheRunStati.RunState = True
		i_TheRunStati.t17_st01_Ctrl_Lock = True
		t17_st01_label_dedupe_status.configure( text = "Processing...")
		t17_st01_button_dedupe_text.set( Strng_HaltProcess() )
		p_multi_log.do_logs( "sp", "Not yet Calling canprunatry_command")
		i_lst_findings = canprunatry_command( p_lst_paths, p_limit_top_n, PseudoNoInterruptCheck, p_multi_log)
		for i_found in i_lst_findings :
			t17_st01_lb_finds.insert( 0, i_found)
		p_multi_log.do_logs( "sp", "Back from canprunatry_command")
		#t17_st01_label_dedupe_status.configure( text = "Canprunatry was run." )
		#t17_st01_button_dedupe_text.set("Run dedupe !")
		i_queue.put( RunStateMessages.t17_st01_Done )
		p_multi_log.do_logs( "sp", "Completed t17_st01_throcess_dedupe " + (40 * "%") )
	def t17_st01_on_button_dedupe() :
		# set this up ready for when asynchronous running becomes the way
		nonlocal i_TheRunStati
		nonlocal p_multi_log
		t06_t5_get_log_flags_from_checkboxes( )
		i_lst_paths = t17_st01_get_paths_list()
		cando_fldrlst = len( i_lst_paths) > 0
		cando_clashes = t17_st01_check_paths_for_clashes()
		if i_TheRunStati.RunState :
			if not_running_else_say( "Hmm, only synchronous so probably shouldn't be seeing this!" ) :
				t17_st01_label_dedupe_status.configure( text = "Abandoned processing in mode:" + t_EnumDeleteMode_show )
				i_TheRunStati.RunState = False
				t17_st01_button_dedupe_text.set( Strng_RunProcess() )
		elif cando_fldrlst and cando_clashes :
			i_str_report_limit = t17_st01_var_limit.get()
			try:
				i_report_limit = int( i_str_report_limit)
			except:
				i_report_limit = -1 # this will defer to the default value inside the organitry module
			if True:
				if DoNotRunProcessesAsThreads():
					t17_st01_throcess_dedupe( i_lst_paths, i_report_limit )
					# now cheat a bit and use the same method the thread would use to return the widgets to normal
					i_queue.put( RunStateMessages.t17_st01_Done )
				else:
					# run the process in a thread
					p_multi_log.do_logs( "sp", "Setting thread for t17_st01_throcess_dedupe")
					i_t3_thrd = fi_Thread( target = t17_st01_throcess_dedupe, args = ( i_lst_paths, i_report_limit ) )
					p_multi_log.do_logs( "sp", "Starting thread for t17_st01_throcess_dedupe")
					i_t3_thrd.start()
					p_multi_log.do_logs( "sp", "Leaving thread to run in background")
			else :
				p_multi_log.do_logs( "sp", "Canprunatry run abandoned.")
				t17_st01_label_dedupe_status.configure( text = "Canprunatry run abandoned." )
		elif not cando_clashes :
			m_tkntr.messagebox.showwarning(app_show_name(), "Run refused!\nThere is a clash somewhere across the lists!") 
			t17_st01_label_dedupe_status.configure( text = "No Run. There is a clash somewhere!" )
		elif not cando_fldrlst :
			m_tkntr.messagebox.showwarning(app_show_name(), "Run refused!\nThere are no folders listed!") 
			t17_st01_label_dedupe_status.configure( text = "No Run. There are no folders listed" )

	# ----------------------------------------
	# For Tab Organitry Subtab Renametry
	def t17_st02_not_running_else_say( str_action ) :
		# concept here is to return whether we're free to make settings and if not to show a message about it
		if i_TheRunStati.t17_st02_Ctrl_Lock :
			m_tkntr.messagebox.showwarning(app_show_name(), str_action + " blocked! A Defoliatry run is in process.") 
			i_not_running = False
		else :
			i_not_running = True
		return i_not_running
	def t17_st02_update_label_re_warning( p_IsRisky ) :
		if p_IsRisky :
			i_txt_Risk = "Warning! Immediate deletion without content checking."
			i_fg = "red"
		else :
			i_txt_Risk = "_"
			i_fg = "black"
		t17_st02_label_re_warning.configure( text = i_txt_Risk, fg = i_fg )
	def t17_label_paths_on_rclick_clear() :
		t17_st02_var_path.set( "" )
	def t17_st02_on_button_browse_paths() :
		got_path = t17_st02_var_path.get()
		got_string = getpathref_fromuser( "Select a path", got_path)
		t17_st02_var_path.set( got_string )
	def t17_st02_on_button_add_folder() :
		if t17_st02_not_running_else_say( "Adding list item" ) :
			got_path = cmntry.hndy_str_to_goodpath( t17_st02_var_path.get() )
			added_path = False
			if ( t17_st02_chkvr_autoclashcheck_paths.get() == 1 ) :
				clash_path, is_sub_path = \
					t17_st02_check_path_for_clashes( got_path )
				if (not clash_path) :
					t17_st02_lb_paths.insert( 0, got_path)
					added_path = True
				else: 
					# provide info about the clashing
					str_advice = "That path would clash!"
					if is_sub_path :
						str_advice = str_advice + "\nMakes Sub-folder in paths."
					elif clash_path :
						str_advice = str_advice + "\nAlready in paths."
					m_tkntr.messagebox.showwarning(app_show_name() + " - Adding Path", str_advice) 
			else :
				# go straight ahead and add, relying on user to then do check
				t17_st02_lb_paths.insert( 0, got_path)
				added_path = True
			if added_path :
				t17_st02_label_paths_alert.configure( text = "Added path: " + got_path)
	def t17_st02_on_button_add_subfolders() :
		# the idea here will be to add all the next level sub-folders of the selected folder 
		# get the list of paths
		if t17_st02_not_running_else_say( "Adding disposal list item" ) :
			got_path = t17_st02_var_path.get()
			i_lst_dspsl_pth = explrtry.Path_GetListOfFirstLevelSubPaths( got_path )
			for i_pth in i_lst_dspsl_pth :
				t17_st02_path_addpath_if_okto( i_pth)
	def t17_st02_on_button_remove_selected_path() :
		if t17_st02_not_running_else_say( "Removing list item" ) :
			lst_index = t17_st02_lb_paths.curselection()
			if len( lst_index) > 0 :
				del_index = int(lst_index[0])
				was_path = t17_st02_lb_paths.get(del_index)
				t17_st02_lb_paths.delete( del_index)
				t17_st02_label_paths_alert.configure( text = "Removed: " + was_path)
			else :
				t17_st02_label_paths_alert.configure( text = "No selection.")
	def t17_st02_on_dblclck_selected_path() :
		if t17_st02_not_running_else_say( "Noting list item" ) :
			lst_index = t17_st02_lb_paths.curselection()
			if len( lst_index) > 0 :
				del_index = int(lst_index[0])
				got_path = t17_st02_lb_paths.get(del_index)
				t17_st02_var_path.set( got_path)
	def t17_st02_on_button_remove_all_paths() :
		if t17_st02_not_running_else_say( "Clearing list" ) :
			t17_st02_lb_paths.delete(0, m_tkntr.END) # clear
			t17_st02_label_paths_alert.configure( text = "Cleared list.")
	def t17_st02_on_button_check_list_clashes_paths() :
		if t17_st02_not_running_else_say( "Checking list for clashes" ) :
			if len(t17_st02_get_paths_list()) > 0 :
				is_all_good = t17_st02_check_paths_for_clashes()
				if is_all_good :
					t17_st02_label_paths_alert.configure( text = "All good, no clashes in list.")
				else :
					t17_st02_label_paths_alert.configure( text = "Problems in list.")
					m_tkntr.messagebox.showwarning(app_show_name(), "Clashes in list.!") 
			else :
				t17_st02_label_paths_alert.configure( text = "Empty list.")
	def t17_st02_on_chkbx_autoclashcheck_paths():
		if ( t17_st02_chkvr_autoclashcheck_paths.get() == 1 ) :
			# so this means changing from Off to On
			# we should if all is ok before allowing this 
			if t17_st02_check_paths_for_clashes():
				t17_st02_label_paths_alert.configure( text = "Auto is on.")
			else :
				t17_st02_chkvr_autoclashcheck_paths.set(0)
				t17_st02_label_paths_alert.configure( text = "Cannot return to Auto with clashes in lists.")
				m_tkntr.messagebox.showwarning(app_show_name(), "Cannot return to Auto with clashes in lists.") 
		else :
			t17_st02_label_paths_alert.configure( text = "Auto is off.")
	def t17_st02_on_selected_RenametryMode() :
		# get the current selection in the combobox
		i_EnumRenametryMode = t17_st02_cb_EnumRenametryMode.getSelectedKey()
		# get its explanatory text
		i_explain = apltry.EnumRenametryMode_Attrbt_Show( i_EnumRenametryMode )
		# write that to the label 
		t17_st02_label_mode_description.configure( text = i_explain)
	def t17_st02_on_button_rename() :
		pass
		i_lst_paths = t17_st02_get_paths_list()
		cando_fldrlst = len( i_lst_paths) > 0
		is_all_good = cando_fldrlst and t17_st02_check_paths_for_clashes()
		if is_all_good:
			i_EnumRenametryMode = t17_st02_cb_EnumRenametryMode.getSelectedKey()
			i_EnumRenametryDirection = t17_st02_cb_EnumRenametryDirection.getSelectedKey()
			renametry_command( i_lst_paths, i_EnumRenametryMode, i_EnumRenametryDirection, p_multi_log )

	# ----------------------------------------
	# For Tab 6 Settings SubTab 1
	def t06_t1_get_LstDiscardNameMarkers() :
		tpl_discards = t06_t1_lb_malname.get(0, m_tkntr.END)
		lst_discards = list(tpl_discards)
		return lst_discards
	def t06_t1_set_LstDiscardNameMarkers( lst_discards ) :
		lst_get = t06_t1_get_LstDiscardNameMarkers() 
		for pth in lst_discards :
			if not (pth in lst_get) :
				t06_t1_lb_malname.insert( 0, pth)
	def t06_t1_on_button_add_malnamelist() :
		if not_running_else_say( "Adding list item" ) :
			got_path = t06_t1_var_malname.get()
			added_path = False
			if ( t06_t1_chkvr_autoclashcheck_malname.get() == 1 ) :
				clash_malnamelist, made_sub_malnamelist = \
					check_name_for_malnamelist_clashes( got_path )
				if (not clash_malnamelist) :
					t06_t1_lb_malname.insert( 0, got_path)
					added_path = True
				else: 
					# provide info about the clashing
					str_advice = "That match would clash!"
					if made_sub_malnamelist :
						str_advice = str_advice + "\nMakes Sub-match in match list."
					elif clash_malnamelist :
						str_advice = str_advice + "\nAlready in match list."
					m_tkntr.messagebox.showwarning(app_show_name() + " - Adding match", str_advice) 
			else :
				# go straight ahead and add, relying on user to then do check
				t06_t1_lb_malname.insert( 0, got_path)
				added_path = True
			if added_path :
				t06_t1_label_malname_list_alert.configure( text = "Added match: " + got_path)
	def t06_t1_on_button_remove_selected_malnamelist() :
		if not_running_else_say( "Removing match list item" ) :
			lst_index = t06_t1_lb_malname.curselection()
			if len( lst_index) > 0 :
				del_index = int(lst_index[0])
				was_path = t06_t1_lb_malname.get(del_index)
				t06_t1_lb_malname.delete( del_index)
				t06_t1_label_malname_list_alert.configure( text = "Removed: " + was_path)
			else :
				t06_t1_label_malname_list_alert.configure( text = "No match selection.")
	def on_dblclck_selected_malnamelist() :
		if not_running_else_say( "Noting malname list item" ) :
			lst_index = t06_t1_lb_malname.curselection()
			if len( lst_index) > 0 :
				del_index = int(lst_index[0])
				got_strng = t06_t1_lb_malname.get(del_index)
				t06_t1_var_malname.set( got_strng)
	def t06_t1_on_button_remove_all_malnamelist() :
		if not_running_else_say( "Clearing match list" ) :
			t06_t1_lb_malname.delete(0, m_tkntr.END) # clear
			t06_t1_label_malname_list_alert.configure( text = "Cleared match list.")
	def t06_t1_on_button_check_list_clashes_malnamelist() :
		if not_running_else_say( "Checking match list for clashes" ) :
			if len(get_malname_list()) > 0 :
				is_all_good = check_malnamelist_for_clashes()
				if is_all_good :
					t06_t1_label_malname_list_alert.configure( text = "All good, no clashes in match list.")
				else :
					t06_t1_label_malname_list_alert.configure( text = "Problems in match list.")
					m_tkntr.messagebox.showwarning(app_show_name(), "Clashes in match list.!") 
			else :
				t06_t1_label_malname_list_alert.configure( text = "Empty match list.")
	def on_chkbx_autoclashcheck_malnamelist():
		if ( t06_t1_chkvr_autoclashcheck_malname.get() == 1 ) :
			# so this means changing from Off to On
			# we should if all is ok before allowing this 
			if check_malnamelist_for_clashes():
				t06_t1_label_malname_list_alert.configure( text = "Auto is on.")
			else :
				t06_t1_chkvr_autoclashcheck_malname.set(0)
				t06_t1_label_malname_list_alert.configure( text = "Cannot return to Auto with clashes in lists.")
				m_tkntr.messagebox.showwarning(app_show_name(), "Cannot return to Auto with clashes in lists.") 
		else :
			t06_t1_label_malname_list_alert.configure( text = "Auto is off.")
	def t06_t1_on_button_restore_defaults() :
		t06_t1_on_button_remove_all_malnamelist()
		for l_i in default_badmatch_list() :
			t06_t1_lb_malname.insert( 0, l_i)
		pass
	# For Tab 4 Settings SubTab 3 Saved Settings
	def t06_t4_chkbx_autosave_on_release():
		i_autosave = ( t06_t4_chkvr_autosave.get() == 1 )
		if i_autosave :
			# if we've just turned this ON then we don't need to do anything just now
			# later we will re-check this value and maybe do something on closing the application
			t06_t4_label_settings_alert.configure( text = "AutoSave mode is now ON" )
		else :
			# if we've just turned this OFF then we need to delete the autosave file
			t_did, t_delete_str = Delete_AutoSaved_Settings()
			t06_t4_label_settings_alert.configure( text = "Turning off AutoSave " + t_delete_str )
	def t06_t4_label_loadfromfile_on_rclick_clear() :
		t06_t4_var_loadedfromfile.set( "" )
	def t06_t4_on_button_browse_loadfromfile() :
		got_file = t06_t4_var_loadedfromfile.get()
		got_string = getloadfileref_fromuser( "Select a settings file to Load", got_file)
		t06_t4_var_loadedfromfile.set( got_string)
	def t06_t4_on_button_action_loadfromfile() :
		nonlocal p_multi_log
		got_string = t06_t4_var_loadedfromfile.get()
		if len( got_string ) > 0 :
			UpdateSettingFlagsFromGuiCheckboxes()
			# load the checked settings
			r_lst_loaded_settings = FldtryWndw_GuiSettings.LoadFlaggedSettingsFromFile( got_string ) # , p_multi_log
			# apply the loaded settings to the GUI controls
			#print( r_lst_loaded_settings )
			for sk in r_lst_loaded_settings :
				#print( sk)
				#print( FldtryWndw_GuiSettings.Get_SettingValue( sk ) )
				SetSettingIntoGui( sk, FldtryWndw_GuiSettings.Get_SettingValue( sk ) )
			t06_t4_label_settings_alert.configure( text = "Loaded " + str(len(r_lst_loaded_settings)) + " settings from " + got_string)
	def t06_t4_on_button_action_loadfromdefault() :
		# need a message box to advise that this will turn on all the settings flags and then try to load from the default location
		# yes, you're expected to manage this via usage
		nonlocal p_multi_log
		t_default_settings_fileref = cmntry.cfgf_get_default_config_fileref()
		if not t_default_settings_fileref is None and len( t_default_settings_fileref ) > 0 :
			UpdateSettingFlagsFromGuiCheckboxes()
			# load the checked settings
			r_lst_loaded_settings = FldtryWndw_GuiSettings.LoadFlaggedSettingsFromFile( t_default_settings_fileref ) # , p_multi_log
			# apply the loaded settings to the GUI controls
			#print( r_lst_loaded_settings )
			for sk in r_lst_loaded_settings :
				#print( sk)
				#print( FldtryWndw_GuiSettings.Get_SettingValue( sk ) )
				SetSettingIntoGui( sk, FldtryWndw_GuiSettings.Get_SettingValue( sk ) )
			t06_t4_label_settings_alert.configure( text = "Loaded " + str(len(r_lst_loaded_settings)) + " settings from " + t_default_settings_fileref)
	def t06_t4_label_savedintofile_on_rclick_clear() :
		t06_t4_var_savedintofile.set( "" )
	def t06_t4_on_button_browse_saveintofile() :
		got_file = t06_t4_var_savedintofile.get()
		got_string = getsavefileref_fromuser( "Select a settings file to Save", got_file)
		t06_t4_var_savedintofile.set( got_string)
	def t06_t4_on_button_action_saveintofile() :
		nonlocal p_multi_log
		t_fileref = t06_t4_var_savedintofile.get()
		if len( t_fileref ) > 0 :
			# work out which settings to save according to the check boxes 
			UpdateSettingFlagsFromGuiCheckboxes()
			none_yet = False
			for sk in apltry.EnumSettingDef :
				i = sk.value
				FldtryWndw_GuiSettings.Settings_Flags[ sk] = ( t06_t4_dct_cbv[i].get() == 1 )
				none_yet = none_yet and FldtryWndw_GuiSettings.Settings_Flags[ sk]
			# collect the flagged settings  
			for sk in apltry.EnumSettingDef :
				isok, sv = GetSettingFromGui( sk )
				if isok:
					FldtryWndw_GuiSettings.Set_SettingValue( sk, sv )
			FldtryWndw_GuiSettings.SaveFlaggedSettingsIntoFile( t_fileref ) # no , p_multi_log in re-write
			t06_t4_label_settings_alert.configure( text = "Saved: " + t_fileref)
	def t06_t4_on_button_action_saveintodefault():
		# note that unlike the load from default action, this will only save the currently marked settings
		# so you would need to 
		t_didok, t_fileref = Save_Current_Settings_ToBeDefault()
		if t_didok:
			t06_t4_label_settings_alert.configure( text = "Saved: " + t_fileref)
	def t06_t4_on_button_action_deletedefault():
		t_didok, t_fileref = Delete_Default_Settings()
		if t_didok:
			t06_t4_label_settings_alert.configure( text = "Deleted: " + t_fileref)
	def t06_t4_on_button_settings_all_off() :
		for sk in apltry.EnumSettingDef :
			i = sk.value
			t06_t4_dct_cbv[i].set( 0)
	def t06_t4_on_button_settings_all_onn() :
		#print( "t06_t4_on_button_settings_all_onn")
		for sk in apltry.EnumSettingDef :
			i = sk.value
			t06_t4_dct_cbv[i].set( 1)
	# For Tab 4 Settings SubTab 4 Logging Controls
	# pickup presets and make display match them
	def t06_t5_set_chkbx_from_log_flag_status() :
		nonlocal p_multi_log
		new_var = bool_to_chkboxvar( p_multi_log.key_get_b( "s") )
		t06_t5_chkvr_log_status.set(new_var)
	def t06_t5_set_chkbx_from_log_flag_result() :
		nonlocal p_multi_log
		new_var = bool_to_chkboxvar( p_multi_log.key_get_b( "r") )
		t06_t5_chkvr_log_result.set(new_var)
	def t06_t5_set_chkbx_from_log_flag_process() :
		nonlocal p_multi_log
		new_var = bool_to_chkboxvar( p_multi_log.key_get_b( "p") )
		t06_t5_chkvr_log_process.set(new_var)
	def t06_t5_set_chkbx_from_log_flag_detail() :
		nonlocal p_multi_log
		new_var = bool_to_chkboxvar( p_multi_log.key_get_b( "d") )
		t06_t5_chkvr_log_detail.set(new_var)
	def t06_t5_set_chkbx_from_log_flag_debug() :
		nonlocal p_multi_log
		new_var = bool_to_chkboxvar( p_multi_log.key_get_b( "b") )
		t06_t5_chkvr_log_debug.set(new_var)
	# pickup settings from controls
	def t06_t5_set_log_flag_from_checkbox_status() :
		nonlocal p_multi_log
		new_bool = ( t06_t5_chkvr_log_status.get()==1 )
		p_multi_log.key_set_b( "s", new_bool)
	def t06_t5_set_log_flag_from_checkbox_result() :
		nonlocal p_multi_log
		new_bool = ( t06_t5_chkvr_log_result.get()==1 )
		p_multi_log.key_set_b( "r", new_bool)
	def t06_t5_set_log_flag_from_checkbox_process() :
		nonlocal p_multi_log
		new_bool = ( t06_t5_chkvr_log_process.get()==1 )
		p_multi_log.key_set_b( "p", new_bool)
	def t06_t5_set_log_flag_from_checkbox_detail( ) :
		nonlocal p_multi_log
		new_bool = ( t06_t5_chkvr_log_detail.get()==1 )
		p_multi_log.key_set_b( "d", new_bool)
	def t06_t5_set_log_flag_from_checkbox_debug( ) :
		nonlocal p_multi_log
		new_bool = ( t06_t5_chkvr_log_debug.get()==1 )
		p_multi_log.key_set_b( "b", new_bool)
	def t06_t5_get_log_flags_from_checkboxes( ) :
		t06_t5_set_log_flag_from_checkbox_status( )
		t06_t5_set_log_flag_from_checkbox_result( )
		t06_t5_set_log_flag_from_checkbox_process( )
		t06_t5_set_log_flag_from_checkbox_detail( )
		t06_t5_set_log_flag_from_checkbox_debug( )
	def t06_t5_on_chkbx_log_status() :
		t06_t5_set_log_flag_from_checkbox_status( )
	def t06_t5_on_chkbx_log_result() :
		t06_t5_set_log_flag_from_checkbox_result( )
	def t06_t5_on_chkbx_log_process() :
		t06_t5_set_log_flag_from_checkbox_process( )
	def t06_t5_on_chkbx_log_detail() :
		t06_t5_set_log_flag_from_checkbox_detail( )
	def t06_t5_on_chkbx_log_debug() :
		t06_t5_set_log_flag_from_checkbox_debug( )
	#
	def t06_t5_update_logging_checkboxes_from_log_flags():
		t06_t5_set_chkbx_from_log_flag_status()
		t06_t5_set_chkbx_from_log_flag_result()
		t06_t5_set_chkbx_from_log_flag_process()
		t06_t5_set_chkbx_from_log_flag_detail()
		t06_t5_set_chkbx_from_log_flag_debug()
	# $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
	# ------------------------------------------------------------------
	def DevelopMode_Actions():
		print( DevelopModeHeadString() )
		print( DevelopModeDescriptionString() )
		if DevelopMode_ProveMode() : # leave False while creating the new GetSettingFromGui method
			# the Prove calls are designed to crash if implementation is not complete
			print( DevelopModeProveString() )
			Prove_New_GetSettingFromGui() 
			Prove_New_SetSettingIntoGui() 
			ESD_EMSA_Prove() # will intentionally cause a crash
		else :
			# the Check calls are designed to print omissions to the terminal but not be fatal
			print( DevelopModeCheckString() )
			Check_New_GetSettingFromGui() 
			Check_New_SetSettingIntoGui()
			ESD_EMSA_Check() # will just report omissions to the terminal
		print( DevelopModeTailString() )
	# ------------------------------------------------------------------
	# $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
	# ==================================================================
	# main for FoldatryWindow
	#  set a flag to let us ensure only one process is ever running at a time - note that this isn't about
	#  multi-threading in a programming sense, rather it is to protect the user from unexpected intersecting
	#  consequences from overlapping process runs
	process_running = False
	# have a settings object
	FldtryWndw_GuiSettings = apltry.FoldatryGuiSettings()
	# --------- Frame 0 Title
	# Have a top section outside the Tabbed interface
	frame_top = m_tkntr.Frame( p_window_context)
	frame_top.pack( fill = m_tkntr.X)
	# make frame content
	button_exit = m_tkntr.Button( frame_top, text="(X)", command=on_button_exit)
	label_title = m_tkntr.Label( frame_top, text=app_show_name_version() )
	label_explain = m_tkntr.Label( frame_top, text="A tool for analysing and processing trees of folders.")
	# place the content
	button_exit.pack( side=m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	label_title.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	label_explain.pack( side = m_tkntr.RIGHT, padx=tk_padx(), pady=tk_pady() ) 
	#
	# ========== Make the notebook i.e. tabbed interface
	nb = m_tkntr_ttk.Notebook( p_window_context)
	nb.pack( expand=1, fill=m_tkntr.BOTH)
	#
	# ========== Make a tab for: Welcome
	t00_tab = m_tkntr.Frame(nb)
	nb.add( t00_tab, text="??" )
	# ----------
	t00_scrolltext = m_tkntr_tkst.ScrolledText( t00_tab, wrap= "word" )
	t00_scrolltext.pack( side=m_tkntr.LEFT, fill=m_tkntr.BOTH, expand=True )
	t00_scrolltext.insert(m_tkntr.INSERT, "Welcome to " + app_show_name() )
	t00_scrolltext.insert(m_tkntr.INSERT, "\n" )
	t00_scrolltext.insert(m_tkntr.INSERT, app_show_brief_intro() )
	#
	# ========== Make a tab for : Prunatry
	t01_tab = m_tkntr.Frame(nb)
	nb.add( t01_tab, text="Prunatry")
	# --------- Frame 01 Heading
	t01_frame01 = m_tkntr.Frame( t01_tab, bg=colour_bg_neutral())
	t01_frame01.pack( fill = m_tkntr.X)
	t01_label_tab_heading = m_tkntr.Label( t01_frame01, text="Search list of DISPOSALS paths for folders or files to remove by proving they identically exist in SURVIVALS paths", bg=colour_bg_neutral())
	t01_label_tab_heading.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	# --------- Frame 02 Disposal entry first line
	t01_frame02 = m_tkntr.Frame( t01_tab, bg=colour_bg_danger())
	t01_frame02.pack( fill = m_tkntr.X)
	t01_label_heading_disposal = m_tkntr.Label( t01_frame02, text="DISPOSALS", bg=colour_bg_danger())
	t01_label_heading_disposal.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	#
	t01_label_browse_disposal = m_tkntr.Label( t01_frame02, text="Enter path", bg=colour_bg_danger())
	t01_label_browse_disposal.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	t01_label_browse_disposal.bind('<Button-3>',  lambda x: t01_label_path_disposal_on_rclick_clear() ) # because <Button-3> is the Tkinter code for a right-click
	#
	t01_var_disposal = m_tkntr.StringVar()
	t01_entry_disposal = m_tkntr.Entry( t01_frame02, textvariable=t01_var_disposal)
	t01_entry_disposal.pack( side = m_tkntr.LEFT, fill = m_tkntr.X, expand=1, padx=tk_padx(), pady=tk_pady() ) 
	# 
	t01_button_browse_disposal = m_tkntr.Button( t01_frame02, text=Strng_Browse(), command=t01_on_button_browse_disposal)
	t01_button_browse_disposal.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	#
	t01_button_add_disposal = m_tkntr.Button( t01_frame02, text=Strng_AddToList(), command=t01_on_button_add_folder_disposal)
	t01_button_add_disposal.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	#
	t01_button_sub_disposal = m_tkntr.Button( t01_frame02, text=Strng_AddSubs(), command=t01_on_button_add_subfolders_disposal)
	t01_button_sub_disposal.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	# --------- Frame 03 Disposal listbox
	t01_frame03 = m_tkntr.Frame( t01_tab, bg=colour_bg_danger())
	t01_frame03.pack( fill = m_tkntr.BOTH, expand=True)
	t01_lb_disposal = m_tkntr.Listbox( t01_frame03, height=2 )
	t01_lb_disposal.pack( side = m_tkntr.LEFT, fill=m_tkntr.BOTH, expand=True, padx=tk_padx(), pady=tk_pady() )
	t01_lb_disposal.bind('<Double-Button>',  lambda x: t01_on_dblclck_selected_disposal() )
	if __debug__ : # declare this to be a Settings object
		Set_ESD_EMSA( apltry.EnumSettingDef.PrntryLstDisposalPaths, EnumMetaSettingAspect.WidgetObject, t01_lb_disposal)
	# --------- Frame 04 Disposal listbox editing buttons
	t01_frame04 = m_tkntr.Frame( t01_tab, bg=colour_bg_danger())
	t01_frame04.pack( fill = m_tkntr.X)
	#
	t01_label_disposal_selection = m_tkntr.Label( t01_frame04, text="Selected Disposal path:", bg=colour_bg_danger())
	t01_label_disposal_selection.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	t01_button_swap_selected_disposal = m_tkntr.Button( t01_frame04, text="Swap", command=t01_on_button_swap_selected_disposal)
	t01_button_swap_selected_disposal.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	t01_button_remove_selected_disposal = m_tkntr.Button( t01_frame04, text="Remove", command=t01_on_button_remove_selected_disposal)
	t01_button_remove_selected_disposal.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	t01_label_disposal_all = m_tkntr.Label( t01_frame04, text="All paths:", bg=colour_bg_danger())
	t01_label_disposal_all.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	t01_button_remove_all_disposals = m_tkntr.Button( t01_frame04, text="Clear", command=t01_on_button_remove_all_disposals)
	t01_button_remove_all_disposals.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	t01_button_check_list_clashes_disposals = m_tkntr.Button( t01_frame04, text="Check clashes", command=t01_on_button_check_list_clashes_disposals)
	t01_button_check_list_clashes_disposals.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	t01_chkvr_autoclashcheck_disposals = m_tkntr.IntVar()
	t01_chkvr_autoclashcheck_disposals.set(1)
	t01_chkbx_autoclashcheck_disposals = m_tkntr.Checkbutton(t01_frame04, text="Check as added", \
		variable=t01_chkvr_autoclashcheck_disposals, command = t01_on_chkbx_autoclashcheck_disposals)
	t01_chkbx_autoclashcheck_disposals.pack(side=m_tkntr.LEFT, anchor=m_tkntr.W, expand=m_tkntr.YES, padx=tk_padx(), pady=tk_pady() )
	t01_label_disposal_list_alert = m_tkntr.Label( t01_frame04, text="_", fg = "blue", bg=colour_bg_danger()) # , font=('italic')
	t01_label_disposal_list_alert.pack( side = m_tkntr.RIGHT, padx=tk_padx(), pady=tk_pady() ) #
	# --------- Frame 06 Survival entry first line
	t01_frame06 = m_tkntr.Frame( t01_tab, bg=colour_bg_safely())
	t01_frame06.pack( fill = m_tkntr.X)
	#
	t01_label_heading_survival = m_tkntr.Label( t01_frame06, text="SURVIVALS", bg=colour_bg_safely())
	t01_label_heading_survival.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) # 
	#
	t01_label_browse_survival = m_tkntr.Label( t01_frame06, text="Enter path", bg=colour_bg_safely())
	t01_label_browse_survival.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	t01_label_browse_survival.bind('<Button-3>',  lambda x: t01_label_path_survival_on_rclick_clear() ) # because <Button-3> is the Tkinter code for a right-click
	#
	t01_var_survival = m_tkntr.StringVar()
	t01_entry_survival = m_tkntr.Entry( t01_frame06, textvariable=t01_var_survival)
	t01_entry_survival.pack( side = m_tkntr.LEFT, fill = m_tkntr.X, expand=1, padx=tk_padx(), pady=tk_pady() ) #
	#
	t01_button_browse_survival = m_tkntr.Button( t01_frame06, text=Strng_Browse(), command=t01_on_button_browse_survival)
	t01_button_browse_survival.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	#
	t01_button_add_survival = m_tkntr.Button( t01_frame06, text=Strng_AddToList(), command=t01_on_button_add_folder_survival)
	t01_button_add_survival.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	#
	t01_button_sub_survival = m_tkntr.Button( t01_frame06, text=Strng_AddSubs(), command=t01_on_button_add_subs_survival)
	t01_button_sub_survival.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	# --------- Frame 07 Survival listbox
	t01_frame07 = m_tkntr.Frame( t01_tab, bg=colour_bg_safely())
	t01_frame07.pack( fill=m_tkntr.BOTH, expand=True)
	#
	t01_lb_survival = m_tkntr.Listbox( t01_frame07, height=2)
	t01_lb_survival.pack( side = m_tkntr.LEFT, fill=m_tkntr.BOTH, expand=True, padx=tk_padx(), pady=tk_pady() )
	t01_lb_survival.bind('<Double-Button>',  lambda x: t01_on_dblclck_selected_survival() )
	if __debug__ : # declare this to be a Settings object
		Set_ESD_EMSA( apltry.EnumSettingDef.PrntryLstSurvivalPaths, EnumMetaSettingAspect.WidgetObject, t01_lb_survival)
	# --------- Frame 08 Survival listbox editing buttons
	t01_frame08 = m_tkntr.Frame( t01_tab, bg=colour_bg_safely())
	t01_frame08.pack( fill = m_tkntr.X)
	#
	t01_label_survival_selection = m_tkntr.Label( t01_frame08, text="Selected Survival path:", bg=colour_bg_safely())
	t01_label_survival_selection.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	t01_button_swap_selected_survival = m_tkntr.Button( t01_frame08, text="Swap", command=t01_on_button_swap_selected_survival)
	t01_button_swap_selected_survival.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	t01_button_remove_selected_survival = m_tkntr.Button( t01_frame08, text="Remove", command=t01_on_button_remove_selected_survival)
	t01_button_remove_selected_survival.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	t01_label_survival_all = m_tkntr.Label( t01_frame08, text="All paths:", bg=colour_bg_safely())
	t01_label_survival_all.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	t01_button_remove_all_survivals = m_tkntr.Button( t01_frame08, text="Clear", command=t01_on_button_remove_all_survivals)
	t01_button_remove_all_survivals.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	t01_button_check_list_clashes_survivals = m_tkntr.Button( t01_frame08, text="Check clashes", command=t01_on_button_check_list_clashes_survivals)
	t01_button_check_list_clashes_survivals.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	t01_chkvr_autoclashcheck_survivals = m_tkntr.IntVar()
	t01_chkvr_autoclashcheck_survivals.set(1)
	t01_chkbx_autoclashcheck_survivals = m_tkntr.Checkbutton(t01_frame08, text="Check as added", \
		variable=t01_chkvr_autoclashcheck_survivals, command = t01_on_chkbx_autoclashcheck_survivals)
	t01_chkbx_autoclashcheck_survivals.pack(side=m_tkntr.LEFT, anchor=m_tkntr.W, expand=m_tkntr.YES, padx=tk_padx(), pady=tk_pady() )
	t01_label_survival_list_alert = m_tkntr.Label( t01_frame08, text="_", fg = "blue", bg=colour_bg_safely())
	t01_label_survival_list_alert.pack( side = m_tkntr.RIGHT, padx=tk_padx(), pady=tk_pady() ) #
	# --------- Frame 09 Both Lists and run type
	t01_frame09 = m_tkntr.Frame( t01_tab)
	t01_frame09.pack( fill = m_tkntr.X)
	t01_button_swaplists = m_tkntr.Button( t01_frame09, text="Swap lists", command=t01_on_button_swaplists)
	t01_button_checklists = m_tkntr.Button( t01_frame09, text="Check for clashes", command=t01_on_button_checklists)
	# layout
	t01_button_swaplists.pack( side=m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	t01_button_checklists.pack( side=m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	#
	t01_button_clearall = m_tkntr.Button( t01_frame09, text="Clear All", command=t01_on_button_clearall)
	t01_button_clearall.pack( side=m_tkntr.RIGHT, padx=tk_padx(), pady=tk_pady() ) #
	# --------- Frame 10 Settings Line 1 Matches
	t01_frame10 = m_tkntr.Frame( t01_tab)
	t01_frame10.pack( fill = m_tkntr.X)
	#
	# 3 check boxes - Supers Folders Files
	t01_label_runparts = m_tkntr.Label( t01_frame10, text="Choose steps:") # , width=10
	t01_label_runparts.pack( side=m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) # , fill=m_tkntr.BOTH, expand=True
	#
	t01_chkvr_super = m_tkntr.IntVar(value=1)
	t01_chkbx_super = m_tkntr.Checkbutton( t01_frame10, text="Do SuperMatch", variable=t01_chkvr_super)
	t01_chkbx_super.pack(side=m_tkntr.LEFT, anchor=m_tkntr.W, expand=m_tkntr.YES, padx=tk_padx(), pady=tk_pady() )
	t01_chkbx_super.bind('<ButtonRelease>',  lambda x: t01_check_riskiness() )
	if __debug__ : # declare this to be a Settings object
		Set_ESD_EMSA( apltry.EnumSettingDef.PrntryDoSuperMatch, EnumMetaSettingAspect.WidgetObject, t01_chkbx_super)
	#
	t01_chkvr_fldrs = m_tkntr.IntVar()
	t01_chkbx_fldrs = m_tkntr.Checkbutton( t01_frame10, text="Do FolderMatch", variable=t01_chkvr_fldrs)
	t01_chkbx_fldrs.pack(side=m_tkntr.LEFT, anchor=m_tkntr.W, expand=m_tkntr.YES, padx=tk_padx(), pady=tk_pady() )
	t01_chkbx_fldrs.bind('<ButtonRelease>',  lambda x: t01_check_riskiness() )
	if __debug__ : # declare this to be a Settings object
		Set_ESD_EMSA( apltry.EnumSettingDef.PrntryDoFolderMatch, EnumMetaSettingAspect.WidgetObject, t01_chkbx_fldrs)
	#
	t01_chkvr_files = m_tkntr.IntVar()
	t01_chkbx_files = m_tkntr.Checkbutton( t01_frame10, text="Do FileMatch", variable=t01_chkvr_files)
	t01_chkbx_files.pack(side=m_tkntr.LEFT, anchor=m_tkntr.W, expand=m_tkntr.YES, padx=tk_padx(), pady=tk_pady() )
	t01_chkbx_files.bind('<ButtonRelease>',  lambda x: t01_check_riskiness() )
	if __debug__ : # declare this to be a Settings object
		Set_ESD_EMSA( apltry.EnumSettingDef.PrntryDoFileMatch, EnumMetaSettingAspect.WidgetObject, t01_chkbx_files)
	#
	t01_chkvr_stringent = m_tkntr.IntVar(value= 0)
	t01_chkbx_stringent = m_tkntr.Checkbutton( t01_frame10, text="For SuperMatch or FolderMatch: Do file content?", variable=t01_chkvr_stringent)
	t01_chkbx_stringent.pack(side=m_tkntr.LEFT, anchor=m_tkntr.W, expand=m_tkntr.YES, padx=tk_padx(), pady=tk_pady() )
	t01_chkbx_stringent.bind('<ButtonRelease>',  lambda x: t01_check_riskiness() )
	if __debug__ : # declare this to be a Settings object
		Set_ESD_EMSA( apltry.EnumSettingDef.PrntryDoMatchStringent, EnumMetaSettingAspect.WidgetObject, t01_chkbx_stringent)
	# --------- Frame 105 Within Match Strategy Controls
	t01_frame105 = m_tkntr.Frame( t01_tab)
	t01_frame105.pack( fill = m_tkntr.X)
	# Label For FileMatch
	t01_label_runparts = m_tkntr.Label( t01_frame105, text="For FileMatch:") # , width=10
	t01_label_runparts.pack( side=m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	# Operating mode selection
	t01_label_EnumMatchMode = m_tkntr.Label( t01_frame105, text="Group matching:") 
	t01_label_EnumMatchMode.pack( side= m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() )
	t01_cb_EnumMatchMode = PairTupleCombobox( t01_frame105, apltry.EnumMatchMode_LstPairTpls(), apltry.EnumMatchMode_Default(), state="readonly", width = 30) 
	t01_cb_EnumMatchMode.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() )
	t01_cb_EnumMatchMode.bind("<<ComboboxSelected>>", lambda x: t01_on_defmod_combobox_release())
	# Now do four check boxes - Hash Size Name When
	# Check box - Hash
	t01_chkvr_defmod_hash = m_tkntr.IntVar()
	t01_chkbx_defmod_hash = m_tkntr.Checkbutton( t01_frame105, text="Content hashing", variable= t01_chkvr_defmod_hash)
	t01_chkbx_defmod_hash.pack( side=m_tkntr.LEFT, anchor=m_tkntr.W, padx=tk_padx(), pady=tk_pady() ) # expand=m_tkntr.YES, 
	t01_chkbx_defmod_hash.bind('<ButtonRelease>',  lambda x: t01_on_defmod_checkbox_release() )
	if __debug__ : # declare this to be a Settings object
		Set_ESD_EMSA( apltry.EnumSettingDef.PrntryFileDoMatchByHash, EnumMetaSettingAspect.WidgetObject, t01_chkbx_defmod_hash)
	# Check box - Size
	t01_chkvr_defmod_size = m_tkntr.IntVar()
	t01_chkbx_defmod_size = m_tkntr.Checkbutton( t01_frame105, text="Size", variable= t01_chkvr_defmod_size)
	t01_chkbx_defmod_size.pack( side=m_tkntr.LEFT, anchor=m_tkntr.W, padx=tk_padx(), pady=tk_pady() ) # expand=m_tkntr.YES, 
	t01_chkbx_defmod_size.bind('<ButtonRelease>',  lambda x: t01_on_defmod_checkbox_release() )
	if __debug__ : # declare this to be a Settings object
		Set_ESD_EMSA( apltry.EnumSettingDef.PrntryFileDoMatchBySize, EnumMetaSettingAspect.WidgetObject, t01_chkbx_defmod_size)
	# Check box - Name
	t01_chkvr_defmod_name = m_tkntr.IntVar()
	t01_chkbx_defmod_name = m_tkntr.Checkbutton( t01_frame105, text="Name", variable= t01_chkvr_defmod_name)
	t01_chkbx_defmod_name.pack( side=m_tkntr.LEFT, anchor=m_tkntr.W, padx=tk_padx(), pady=tk_pady() ) # expand=m_tkntr.YES, 
	t01_chkbx_defmod_name.bind('<ButtonRelease>',  lambda x: t01_on_defmod_checkbox_release() )
	if __debug__ : # declare this to be a Settings object
		Set_ESD_EMSA( apltry.EnumSettingDef.PrntryFileDoMatchByName, EnumMetaSettingAspect.WidgetObject, t01_chkbx_defmod_name)
	# Check box - When
	t01_chkvr_defmod_when = m_tkntr.IntVar()
	t01_chkbx_defmod_when = m_tkntr.Checkbutton( t01_frame105, text="When", variable= t01_chkvr_defmod_when)
	t01_chkbx_defmod_when.pack( side=m_tkntr.LEFT, anchor=m_tkntr.W, padx=tk_padx(), pady=tk_pady() ) # expand=m_tkntr.YES, 
	t01_chkbx_defmod_when.bind('<ButtonRelease>',  lambda x: t01_on_defmod_checkbox_release() )
	if __debug__ : # declare this to be a Settings object
		Set_ESD_EMSA( apltry.EnumSettingDef.PrntryFileDoMatchByWhen, EnumMetaSettingAspect.WidgetObject, t01_chkbx_defmod_when)
	# --------- Frame 106 For FileMatch: Line 2
	t01_frame106 = m_tkntr.Frame( t01_tab)
	t01_frame106.pack( fill = m_tkntr.X)
	# Label For FileMatch
	t01_label_runparts = m_tkntr.Label( t01_frame106, text="For FileMatch:") # , width=10
	t01_label_runparts.pack( side=m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	# Group Deletion order selection
	t01_label_deletion_order = m_tkntr.Label( t01_frame106, text="Group deletion order:") 
	t01_label_deletion_order.pack( side= m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() )
	#
	t01_cb_EnumDelOrder = PairTupleCombobox( t01_frame106, apltry.EnumDelOrder_LstPairTpls(), apltry.EnumDelOrder_Default(), state="readonly", width = 20) 
	t01_cb_EnumDelOrder.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() )
	t01_cb_EnumDelOrder.bind("<<ComboboxSelected>>", lambda x: t01_on_EnumDelOrder_combobox_release())
	if __debug__ : # declare this to be a Settings object
		Set_ESD_EMSA( apltry.EnumSettingDef.PrntryMtchGrpDelOrder, EnumMetaSettingAspect.WidgetObject, t01_cb_EnumDelOrder)
	#
	t01_label_re_EnumDelOrder = m_tkntr.Label( t01_frame106, text="has viability?") 
	t01_label_re_EnumDelOrder.pack( side= m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() )
	#
	# now trigger the setting of the check boxes to match the combobox
	t01_on_defmod_combobox_release()
	t01_on_EnumDelOrder_combobox_release()
	# --------- Frame 11 Settings Line 2 Deletions
	t01_frame11 = m_tkntr.Frame( t01_tab)
	t01_frame11.pack( fill = m_tkntr.X)
	#
	t01_label_check_options = m_tkntr.Label( t01_frame11, text="Pre deletion check options:") 
	t01_label_check_options.pack( side= m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() )
	#
	t01_chkvr_cngrnt_bf_del = m_tkntr.IntVar()
	t01_chkbx_cngrnt_bf_del = m_tkntr.Checkbutton( t01_frame11, text="Do Congruent before each delete", variable=t01_chkvr_cngrnt_bf_del)
	t01_chkbx_cngrnt_bf_del.pack(side=m_tkntr.LEFT, anchor=m_tkntr.W, padx=tk_padx(), pady=tk_pady() ) # expand=m_tkntr.YES, 
	t01_chkbx_cngrnt_bf_del.bind('<ButtonRelease>',  lambda x: t01_check_riskiness() )
	if __debug__ : # declare this to be a Settings object
		Set_ESD_EMSA( apltry.EnumSettingDef.PrntryDoCongruent, EnumMetaSettingAspect.WidgetObject, t01_chkbx_cngrnt_bf_del)
	#
	t01_chkvr_cngrnt_strngnt = m_tkntr.IntVar()
	t01_chkbx_cngrnt_strngnt = m_tkntr.Checkbutton( t01_frame11, text="Do Stringent Congruent", variable=t01_chkvr_cngrnt_strngnt)
	t01_chkbx_cngrnt_strngnt.pack(side=m_tkntr.LEFT, anchor=m_tkntr.W, padx=tk_padx(), pady=tk_pady() ) # expand=m_tkntr.YES, 
	t01_chkbx_cngrnt_strngnt.bind('<ButtonRelease>',  lambda x: t01_check_riskiness() )
	if __debug__ : # declare this to be a Settings object
		Set_ESD_EMSA( apltry.EnumSettingDef.PrntryDoCngrntStringent, EnumMetaSettingAspect.WidgetObject, t01_chkbx_cngrnt_strngnt)
	# adding in a checkbox for reporting stringent match failures
	t01_chkvr_cngrnt_fails_strngnt = m_tkntr.IntVar()
	t01_chkbx_cngrnt_fails_strngnt = m_tkntr.Checkbutton( t01_frame11, text="Report Stringent Failures", variable=t01_chkvr_cngrnt_fails_strngnt)
	t01_chkbx_cngrnt_fails_strngnt.pack(side=m_tkntr.LEFT, anchor=m_tkntr.W, padx=tk_padx(), pady=tk_pady() ) # expand=m_tkntr.YES, 
	if __debug__ : # declare this to be a Settings object
		Set_ESD_EMSA( apltry.EnumSettingDef.PrntryReportStringentDiffs, EnumMetaSettingAspect.WidgetObject, t01_chkbx_cngrnt_fails_strngnt)
	# Label for ComboBox
	t01_label_EnumDeleteMode = m_tkntr.Label( t01_frame11, text="Deletion action mode:", width=20)
	t01_label_EnumDeleteMode.pack( side=m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) # expand=True, fill=m_tkntr.BOTH, 
	# ComboBox for EnumDeleteMode
	t01_cb_EnumDeleteMode = PairTupleCombobox( t01_frame11, apltry.EnumDeleteMode_LstPairTpls(), apltry.EnumDeleteMode_Default(), state="readonly", width = 20) 
	t01_cb_EnumDeleteMode.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() )
	t01_cb_EnumDeleteMode.bind("<<ComboboxSelected>>", lambda x: t01_delmode_react() )
	if __debug__ : # declare this to be a Settings object
		Set_ESD_EMSA( apltry.EnumSettingDef.PrntryDeletionMode, EnumMetaSettingAspect.WidgetObject, t01_cb_EnumDeleteMode)
	# --------- Frame 12 Action button
	t01_frame12 = m_tkntr.Frame( t01_tab)
	t01_frame12.pack( fill = m_tkntr.X)
	#
	t01_button_prunatry_text = m_tkntr.StringVar()
	t01_button_prunatry_text.set( Strng_RunProcess() )
	t01_button_prunatry = m_tkntr.Button( t01_frame12, textvariable=t01_button_prunatry_text, command=t01_on_button_prunatry)
	t01_button_prunatry.pack( side=m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	# warning label
	t01_label_re_warning = m_tkntr.Label( t01_frame12, text=":")
	t01_label_re_warning.pack( side= m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() )
	# status label
	t01_label_prunatry_status = m_tkntr.Label( t01_frame12, text="_", fg = "blue") # , width=60
	t01_label_prunatry_status.pack( side=m_tkntr.RIGHT, padx=tk_padx(), pady=tk_pady() )
	#
	# ========== Make a tab for: Defoliatry
	t03_tab = m_tkntr.Frame(nb)
	nb.add( t03_tab, text="Defoliatry")
	# --------- Frame 0 Folder list heading
	t03_frame0 = m_tkntr.Frame( t03_tab, bg=colour_bg_neutral())
	t03_frame0.pack( fill = m_tkntr.X)
	#
	t03_label_tab_heading = m_tkntr.Label( t03_frame0, text="Search list of paths to find replicated folders or files then act on them", bg=colour_bg_neutral())
	t03_label_tab_heading.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	# --------- Frame 1 Folder list entry first line
	t03_frame1 = m_tkntr.Frame( t03_tab, bg=colour_bg_danger())
	t03_frame1.pack( fill = m_tkntr.X)
	#
	t03_label_heading_paths = m_tkntr.Label( t03_frame1, text="PATHS", bg=colour_bg_danger())
	t03_label_heading_paths.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	#
	t03_label_browse_paths = m_tkntr.Label( t03_frame1, text="Enter path", bg=colour_bg_danger())
	t03_label_browse_paths.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	t03_label_browse_paths.bind('<Button-3>',  lambda x: t03_label_paths_on_rclick_clear() ) # because <Button-3> is the Tkinter code for a right-click
	# 
	t03_var_path = m_tkntr.StringVar()
	t03_entry_path = m_tkntr.Entry( t03_frame1, textvariable=t03_var_path)
	t03_entry_path.pack( side = m_tkntr.LEFT, fill = m_tkntr.X, expand=1, padx=tk_padx(), pady=tk_pady() ) 
	# 
	t03_button_browse_paths = m_tkntr.Button( t03_frame1, text=Strng_Browse(), command=t03_on_button_browse_paths)
	t03_button_browse_paths.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	#
	t03_button_add_path = m_tkntr.Button( t03_frame1, text=Strng_AddToList(), command=t03_on_button_add_folder)
	t03_button_add_path.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	#
	t03_button_add_subfolders = m_tkntr.Button( t03_frame1, text=Strng_AddSubs(), command=t03_on_button_add_subfolders)
	t03_button_add_subfolders.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	#  layout
	# --------- Frame 2 Folder list listbox
	t03_frame2 = m_tkntr.Frame( t03_tab, bg=colour_bg_danger())
	t03_frame2.pack( fill = m_tkntr.BOTH, expand=True)
	t03_lb_paths = m_tkntr.Listbox( t03_frame2, height=2 )
	t03_lb_paths.pack( side = m_tkntr.LEFT, fill=m_tkntr.BOTH, expand=True, padx=tk_padx(), pady=tk_pady() )
	t03_lb_paths.bind('<Double-Button>',  lambda x: t03_on_dblclck_selected_path() )
	if __debug__ : # declare this to be a Settings object
		Set_ESD_EMSA( apltry.EnumSettingDef.DfltryLstPaths, EnumMetaSettingAspect.WidgetObject, t03_lb_paths)
	# --------- Frame 3 Folder list listbox editing buttons
	t03_frame3 = m_tkntr.Frame( t03_tab, bg=colour_bg_danger())
	t03_frame3.pack( fill = m_tkntr.X)
	#
	t03_label_paths_selection = m_tkntr.Label( t03_frame3, text="Selected path:", bg=colour_bg_danger())
	t03_label_paths_selection.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	#
	t03_button_remove_selected_path = m_tkntr.Button( t03_frame3, text="Remove", command=t03_on_button_remove_selected_path)
	t03_button_remove_selected_path.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	#
	t03_label_paths_all = m_tkntr.Label( t03_frame3, text="All paths:", bg=colour_bg_danger())
	t03_label_paths_all.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	#
	t03_button_remove_all_paths = m_tkntr.Button( t03_frame3, text="Clear", command=t03_on_button_remove_all_paths)
	t03_button_remove_all_paths.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	#
	t03_button_check_list_clashes_paths = m_tkntr.Button( t03_frame3, text="Check clashes", command=t03_on_button_check_list_clashes_paths)
	t03_button_check_list_clashes_paths.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	#
	t03_chkvr_autoclashcheck_paths = m_tkntr.IntVar()
	t03_chkvr_autoclashcheck_paths.set(1)
	t03_chkbx_autoclashcheck_paths = m_tkntr.Checkbutton(t03_frame3, text="Check as added", \
		variable=t03_chkvr_autoclashcheck_paths, command = t03_on_chkbx_autoclashcheck_paths)
	t03_chkbx_autoclashcheck_paths.pack(side=m_tkntr.LEFT, anchor=m_tkntr.W, expand=m_tkntr.YES, padx=tk_padx(), pady=tk_pady() )
	#
	t03_label_paths_alert = m_tkntr.Label( t03_frame3, text="_", fg = "blue", bg=colour_bg_danger()) # , font=('italic')
	t03_label_paths_alert.pack( side = m_tkntr.RIGHT, padx=tk_padx(), pady=tk_pady() )
	# --------- Frame 5 Within Match Strategy Controls
	t03_frame5 = m_tkntr.Frame( t03_tab)
	t03_frame5.pack( fill = m_tkntr.X)
	# Operating mode selection
	t03_label_EnumMatchMode = m_tkntr.Label( t03_frame5, text="Group matching:") 
	t03_label_EnumMatchMode.pack( side= m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() )
	#
	t03_chkvr_ignore0len = m_tkntr.IntVar(value=1)
	t03_chkbx_ignore0len = m_tkntr.Checkbutton( t03_frame5, text="Ignore zero length", variable= t03_chkvr_ignore0len)
	t03_chkbx_ignore0len.pack( side=m_tkntr.LEFT, anchor=m_tkntr.W, padx=tk_padx(), pady=tk_pady() )
	if __debug__ : # declare this to be a Settings object
		Set_ESD_EMSA( apltry.EnumSettingDef.DfltryDoIgnoreZeroLength , EnumMetaSettingAspect.WidgetObject, t03_chkvr_ignore0len)
	#
	t03_cb_EnumMatchMode = PairTupleCombobox( t03_frame5, apltry.EnumMatchMode_LstPairTpls(), apltry.EnumMatchMode_Default(), state="readonly", width = 30) 
	t03_cb_EnumMatchMode.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() )
	t03_cb_EnumMatchMode.bind("<<ComboboxSelected>>", lambda x: t03_on_defmod_combobox_release())
	#
	t03_chkvr_defmod_hash = m_tkntr.IntVar()
	t03_chkbx_defmod_hash = m_tkntr.Checkbutton( t03_frame5, text="Content hashing", variable= t03_chkvr_defmod_hash)
	t03_chkbx_defmod_hash.pack( side=m_tkntr.LEFT, anchor=m_tkntr.W, padx=tk_padx(), pady=tk_pady() ) # expand=m_tkntr.YES, 
	t03_chkbx_defmod_hash.bind('<ButtonRelease>',  lambda x: t03_on_defmod_checkbox_release() )
	if __debug__ : # declare this to be a Settings object
		Set_ESD_EMSA( apltry.EnumSettingDef.DfltryFileDoMatchByHash, EnumMetaSettingAspect.WidgetObject, t03_chkvr_defmod_hash)
	#
	t03_chkvr_defmod_size = m_tkntr.IntVar()
	t03_chkbx_defmod_size = m_tkntr.Checkbutton( t03_frame5, text="Size", variable= t03_chkvr_defmod_size)
	t03_chkbx_defmod_size.pack( side=m_tkntr.LEFT, anchor=m_tkntr.W, padx=tk_padx(), pady=tk_pady() ) # expand=m_tkntr.YES, 
	t03_chkbx_defmod_size.bind('<ButtonRelease>',  lambda x: t03_on_defmod_checkbox_release() )
	if __debug__ : # declare this to be a Settings object || 
		Set_ESD_EMSA( apltry.EnumSettingDef.DfltryFileDoMatchBySize , EnumMetaSettingAspect.WidgetObject, t03_chkvr_defmod_size)
	#
	t03_chkvr_defmod_name = m_tkntr.IntVar()
	t03_chkbx_defmod_name = m_tkntr.Checkbutton( t03_frame5, text="Name", variable= t03_chkvr_defmod_name)
	t03_chkbx_defmod_name.pack( side=m_tkntr.LEFT, anchor=m_tkntr.W, padx=tk_padx(), pady=tk_pady() ) # expand=m_tkntr.YES, 
	t03_chkbx_defmod_name.bind('<ButtonRelease>',  lambda x: t03_on_defmod_checkbox_release() )
	if __debug__ : # declare this to be a Settings object || 
		Set_ESD_EMSA( apltry.EnumSettingDef.DfltryFileDoMatchByName , EnumMetaSettingAspect.WidgetObject, t03_chkvr_defmod_name)
	#
	t03_chkvr_defmod_when = m_tkntr.IntVar()
	t03_chkbx_defmod_when = m_tkntr.Checkbutton( t03_frame5, text="When", variable= t03_chkvr_defmod_when)
	t03_chkbx_defmod_when.pack( side=m_tkntr.LEFT, anchor=m_tkntr.W, padx=tk_padx(), pady=tk_pady() ) # expand=m_tkntr.YES, 
	t03_chkbx_defmod_when.bind('<ButtonRelease>',  lambda x: t03_on_defmod_checkbox_release() )
	if __debug__ : # declare this to be a Settings object
		Set_ESD_EMSA( apltry.EnumSettingDef.DfltryFileDoMatchByWhen , EnumMetaSettingAspect.WidgetObject, t03_chkvr_defmod_when)
	# --------- Frame 5p5 Line 2
	t03_frame5p5 = m_tkntr.Frame( t03_tab)
	t03_frame5p5.pack( fill = m_tkntr.X)
	# Group Deletion order selection
	t03_label_deletion_order = m_tkntr.Label( t03_frame5p5, text="Group deletion order:") 
	t03_label_deletion_order.pack( side= m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() )
	#
	t03_cb_EnumDelOrder = PairTupleCombobox( t03_frame5p5, apltry.EnumDelOrder_LstPairTpls(), apltry.EnumDelOrder_Default(), state="readonly", width = 20) 
	t03_cb_EnumDelOrder.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() )
	t03_cb_EnumDelOrder.bind("<<ComboboxSelected>>", lambda x: t03_on_EnumDelOrder_combobox_release())
	if __debug__ : # declare this to be a Settings object
		Set_ESD_EMSA( apltry.EnumSettingDef.DfltryMtchGrpDelOrder, EnumMetaSettingAspect.WidgetObject, t03_cb_EnumDelOrder)
	#
	t03_label_re_EnumDelOrder = m_tkntr.Label( t03_frame5p5, text="has viability?") 
	t03_label_re_EnumDelOrder.pack( side= m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() )
	# --------- Frame 6 Within Match Strategy Controls
	t03_frame6 = m_tkntr.Frame( t03_tab)
	t03_frame6.pack( fill = m_tkntr.X)
	#
	t03_label_WithinMatch = m_tkntr.Label( t03_frame6, text="Within match group:") 
	t03_label_WithinMatch.pack( side= m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() )
	#
	t03_chkvr_usediscards = m_tkntr.IntVar(value=1)
	t03_chkbx_usediscards = m_tkntr.Checkbutton( t03_frame6, text="Use Discard Matches (see Settings)", variable= t03_chkvr_usediscards)
	t03_chkbx_usediscards.pack( side=m_tkntr.LEFT, anchor=m_tkntr.W, padx=tk_padx(), pady=tk_pady() ) # expand=m_tkntr.YES,
	if __debug__ : # declare this to be a Settings object
		Set_ESD_EMSA( apltry.EnumSettingDef.DfltryDoUseDiscardMarkers , EnumMetaSettingAspect.WidgetObject, t03_chkvr_usediscards)
	# InGroup Strategy selection
	t03_label_EnumKeepWhich = m_tkntr.Label( t03_frame6, text="Within-group mode") 
	t03_label_EnumKeepWhich.pack( side= m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() )
	#
	t03_cb_EnumKeepWhich = PairTupleCombobox( t03_frame6, apltry.EnumKeepWhich_LstPairTpls(), apltry.EnumKeepWhich_Default(), state="readonly", width = 20) 
	t03_cb_EnumKeepWhich.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() )
	if __debug__ : # declare this to be a Settings object || 
		Set_ESD_EMSA( apltry.EnumSettingDef.DfltryFileEnumKeepWhich, EnumMetaSettingAspect.WidgetObject, t03_cb_EnumKeepWhich)
	# ....................
	# now trigger the setting of the check boxes to match the combobox
	t03_on_defmod_combobox_release()
	t03_on_EnumDelOrder_combobox_release()
	# --------- Frame 7 Action Controls
	t03_frame7 = m_tkntr.Frame( t03_tab)
	t03_frame7.pack( fill = m_tkntr.X)
	t03_chkvr_cngrnt_bf_del = m_tkntr.IntVar()
	t03_chkbx_cngrnt_bf_del = m_tkntr.Checkbutton( t03_frame7, text="Do Congruent before each delete", variable=t03_chkvr_cngrnt_bf_del)
	t03_chkbx_cngrnt_bf_del.pack(side=m_tkntr.LEFT, anchor=m_tkntr.W, padx=tk_padx(), pady=tk_pady() ) # expand=m_tkntr.YES, 
	t03_chkbx_cngrnt_bf_del.bind('<ButtonRelease>',  lambda x: t03_check_riskiness() )
	if __debug__ : # declare this to be a Settings object
		Set_ESD_EMSA( apltry.EnumSettingDef.DfltryDoCongruent , EnumMetaSettingAspect.WidgetObject, t03_chkvr_cngrnt_bf_del)
	t03_chkvr_cngrnt_strngnt = m_tkntr.IntVar()
	t03_chkbx_cngrnt_strngnt = m_tkntr.Checkbutton( t03_frame7, text="Do Stringent Congruent", variable=t03_chkvr_cngrnt_strngnt)
	t03_chkbx_cngrnt_strngnt.pack(side=m_tkntr.LEFT, anchor=m_tkntr.W, padx=tk_padx(), pady=tk_pady() ) # expand=m_tkntr.YES, 
	t03_chkbx_cngrnt_strngnt.bind('<ButtonRelease>',  lambda x: t03_check_riskiness() )
	if __debug__ : # declare this to be a Settings object
		Set_ESD_EMSA( apltry.EnumSettingDef.DfltryDoCngrntStringent , EnumMetaSettingAspect.WidgetObject, t03_chkvr_cngrnt_strngnt)
	# adding in a checkbox for reporting stringent match failures
	t03_chkvr_cngrnt_fails_strngnt = m_tkntr.IntVar()
	t03_chkbx_cngrnt_fails_strngnt = m_tkntr.Checkbutton( t03_frame7, text="Report Stringent Failures", variable=t03_chkvr_cngrnt_fails_strngnt)
	t03_chkbx_cngrnt_fails_strngnt.pack(side=m_tkntr.LEFT, anchor=m_tkntr.W, padx=tk_padx(), pady=tk_pady() ) # expand=m_tkntr.YES, 
	if __debug__ : # declare this to be a Settings object
		Set_ESD_EMSA( apltry.EnumSettingDef.DfltryReportStringentDiffs , EnumMetaSettingAspect.WidgetObject, t03_chkvr_cngrnt_fails_strngnt)
	# Deletion mode listbox
	t03_label_EnumDeleteMode = m_tkntr.Label( t03_frame7, text="Deletion action mode:") # , width=12
	t03_label_EnumDeleteMode.pack( side= m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) # , expand=True, fill=m_tkntr.BOTH, 
	t03_cb_EnumDeleteMode = PairTupleCombobox( t03_frame7, apltry.EnumDeleteMode_LstPairTpls(), apltry.EnumDeleteMode_Default(), state="readonly", width = 20) 
	t03_cb_EnumDeleteMode.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() )
	t03_cb_EnumDeleteMode.bind("<<ComboboxSelected>>", lambda x: t03_check_riskiness() )
	if __debug__ : # declare this to be a Settings object
		Set_ESD_EMSA( apltry.EnumSettingDef.DfltryDeletionMode, EnumMetaSettingAspect.WidgetObject, t03_cb_EnumDeleteMode)
	# --------- Frame 8 Action buttons
	t03_frame8 = m_tkntr.Frame( t03_tab)
	t03_frame8.pack( fill = m_tkntr.X)
	#
	t03_button_dedupe_text = m_tkntr.StringVar()
	t03_button_dedupe_text.set( Strng_RunProcess() )
	t03_button_dedupe = m_tkntr.Button( t03_frame8, textvariable=t03_button_dedupe_text, command=t03_on_button_dedupe)
	t03_button_dedupe.pack( side=m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	#
	t03_label_dedupe_status = m_tkntr.Label( t03_frame8, text="_", fg = "blue") # , width=60
	t03_label_dedupe_status.pack( side=m_tkntr.RIGHT, padx=tk_padx(), pady=tk_pady()  )
	# warning label
	t03_label_re_warning = m_tkntr.Label( t03_frame8, text=":") 
	t03_label_re_warning.pack( side= m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() )
	# --------- Frame 9 Options
	#t03_frame9 = m_tkntr.LabelFrame( t03_tab, text="Options")
	#t03_frame9.pack( fill="both", expand="yes" )
	#
	#
	# ========== Make a tab for: Trimatry
	t16_tab = m_tkntr.Frame(nb)
	nb.add( t16_tab, text="Trimatry")
	# --------- Frame 0 Folder list heading
	t16_frame0 = m_tkntr.Frame( t16_tab, bg=colour_bg_neutral())
	t16_frame0.pack( fill = m_tkntr.X)
	#
	t16_label_tab_heading = m_tkntr.Label( t16_frame0, text="Trim empty things from of a list of folders.", bg=colour_bg_neutral())
	t16_label_tab_heading.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	# --------- Frame 1 Folder list entry first line
	t16_frame1 = m_tkntr.Frame( t16_tab, bg=colour_bg_trimatry())
	t16_frame1.pack( fill = m_tkntr.X)
	#
	t16_label_heading_folderlist = m_tkntr.Label( t16_frame1, text="PATHS", bg=colour_bg_trimatry())
	t16_label_heading_folderlist.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	#
	t16_label_browse_folder = m_tkntr.Label( t16_frame1, text="Enter path", bg=colour_bg_trimatry())
	t16_label_browse_folder.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	t16_label_browse_folder.bind('<Button-3>',  lambda x: t16_label_folder_on_rclick_clear() ) # because <Button-3> is the Tkinter code for a right-click
	# 
	t16_var_entry_folder = m_tkntr.StringVar()
	t16_entry_folder = m_tkntr.Entry( t16_frame1, textvariable=t16_var_entry_folder)
	t16_entry_folder.pack( side = m_tkntr.LEFT, fill = m_tkntr.X, expand=1, padx=tk_padx(), pady=tk_pady() ) 
	# 
	t16_button_browse_folderlist = m_tkntr.Button( t16_frame1, text=Strng_Browse(), command=t16_on_button_browse_folder)
	t16_button_browse_folderlist.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	#
	t16_button_add_folderlist = m_tkntr.Button( t16_frame1, text=Strng_AddToList(), command=t16_on_button_add_folder)
	t16_button_add_folderlist.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	#
	t16_button_add_subfolders = m_tkntr.Button( t16_frame1, text=Strng_AddSubs(), command=t16_on_button_add_subfolders)
	t16_button_add_subfolders.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	# --------- Frame 2 Folder list listbox
	t16_frame2 = m_tkntr.Frame( t16_tab, bg=colour_bg_trimatry())
	t16_frame2.pack( fill = m_tkntr.BOTH, expand=True)
	#
	t16_lb_folderlist = m_tkntr.Listbox( t16_frame2, height=2 )
	t16_lb_folderlist.pack( side = m_tkntr.LEFT, fill=m_tkntr.BOTH, expand=True, padx=tk_padx(), pady=tk_pady() )
	t16_lb_folderlist.bind('<Double-Button>',  lambda x: t16_on_dblclck_selected_folderlist() )
	if __debug__ : # declare this to be a Settings object
		Set_ESD_EMSA( apltry.EnumSettingDef.TrmtryLstPaths, EnumMetaSettingAspect.WidgetObject, t16_lb_folderlist)
	# --------- Frame 3 Folder list listbox editing buttons
	t16_frame3 = m_tkntr.Frame( t16_tab, bg=colour_bg_trimatry())
	t16_frame3.pack( fill = m_tkntr.X)
	#
	t16_label_folderlist_selection = m_tkntr.Label( t16_frame3, text="Selected path:", bg=colour_bg_trimatry() )
	t16_label_folderlist_selection.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	#
	t16_button_remove_selected_folderlist = m_tkntr.Button( t16_frame3, text="Remove", command=t16_on_button_remove_selected_folderlist)
	t16_button_remove_selected_folderlist.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	#
	t16_label_folderlist_all = m_tkntr.Label( t16_frame3, text="All paths:", bg=colour_bg_trimatry() )
	t16_label_folderlist_all.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	#
	t16_button_remove_all_folderlist = m_tkntr.Button( t16_frame3, text="Clear", command=t16_on_button_remove_all_folderlist)
	t16_button_remove_all_folderlist.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	#
	t16_button_check_list_clashes_folderlist = m_tkntr.Button( t16_frame3, text="Check clashes", command=t16_on_button_check_list_clashes_folderlist)
	t16_button_check_list_clashes_folderlist.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	#
	t16_chkvr_autoclashcheck_paths = m_tkntr.IntVar()
	t16_chkvr_autoclashcheck_paths.set(1)
	t16_chkbx_autoclashcheck_folderlists = m_tkntr.Checkbutton(t16_frame3, text="Check as added", \
		variable=t16_chkvr_autoclashcheck_paths, command = t16_on_chkbx_autoclashcheck_paths)
	t16_chkbx_autoclashcheck_folderlists.pack(side=m_tkntr.LEFT, anchor=m_tkntr.W, expand=m_tkntr.YES, padx=tk_padx(), pady=tk_pady() )
	#
	t16_label_alert_folderlist = m_tkntr.Label( t16_frame3, text="_", fg = "blue", bg=colour_bg_trimatry()) # , font=('italic')
	t16_label_alert_folderlist.pack( side = m_tkntr.RIGHT, padx=tk_padx(), pady=tk_pady() )
	# --------- Frame 6 Controls Line 0
	t16_frame6 = m_tkntr.Frame( t16_tab )
	t16_frame6.pack( fill = m_tkntr.X)
	# Guide label
	t16_label_DoGuide = m_tkntr.Label( t16_frame6, text=" Choose either or both of: " )
	t16_label_DoGuide.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	# i_DoDeleteEmptyFiles
	t16_chkvr_DoDeleteEmptyFiles = m_tkntr.IntVar()
	t16_chkvr_DoDeleteEmptyFiles.set(0)
	t16_chkbx_DoDeleteEmptyFiles = m_tkntr.Checkbutton( t16_frame6, text="Delete empty Files", \
		variable=t16_chkvr_DoDeleteEmptyFiles)
	t16_chkbx_DoDeleteEmptyFiles.pack( side=m_tkntr.LEFT, anchor=m_tkntr.W, padx=tk_padx(), pady=tk_pady() )
	# i_DoDeleteEmptyFolds
	t16_chkvr_DoDeleteEmptyFolds = m_tkntr.IntVar()
	t16_chkvr_DoDeleteEmptyFolds.set(0)
	t16_chkbx_DoDeleteEmptyFolds = m_tkntr.Checkbutton( t16_frame6, text="Delete empty Folders", \
		variable=t16_chkvr_DoDeleteEmptyFolds)
	t16_chkbx_DoDeleteEmptyFolds.pack( side=m_tkntr.LEFT, anchor=m_tkntr.W, padx=tk_padx(), pady=tk_pady() )
	# --------- Frame 7 Controls Line 1 
	t16_frame7 = m_tkntr.Frame( t16_tab )
	t16_frame7.pack( fill = m_tkntr.X)
	# i_CanDeleteTopFolders
	t16_chkvr_CanDeleteTopFolders = m_tkntr.IntVar()
	t16_chkvr_CanDeleteTopFolders.set(0)
	t16_chkbx_CanDeleteTopFolders = m_tkntr.Checkbutton( t16_frame7, text="Can delete topmost folders", \
		variable=t16_chkvr_CanDeleteTopFolders)
	t16_chkbx_CanDeleteTopFolders.pack( side=m_tkntr.LEFT, anchor=m_tkntr.W, padx=tk_padx(), pady=tk_pady() )
	if __debug__ : # declare this to be a Settings object
		Set_ESD_EMSA( apltry.EnumSettingDef.TrmtryDoCanDelTopFolders, EnumMetaSettingAspect.WidgetObject, t16_chkvr_CanDeleteTopFolders)
	# recursion of folder emptiness
	t16_chkvr_TreatNestedEmptyFolders = m_tkntr.IntVar()
	t16_chkvr_TreatNestedEmptyFolders.set(1)
	t16_chkbx_TreatNestedEmptyFolders = m_tkntr.Checkbutton( t16_frame7, text="Treat Nested Empty Folders", \
		variable=t16_chkvr_TreatNestedEmptyFolders)
	t16_chkbx_TreatNestedEmptyFolders.pack( side=m_tkntr.LEFT, anchor=m_tkntr.W, padx=tk_padx(), pady=tk_pady() )
	if __debug__ : # declare this to be a Settings object
		Set_ESD_EMSA( apltry.EnumSettingDef.TrmtryDoNestedEmptyFolders, EnumMetaSettingAspect.WidgetObject, t16_chkvr_TreatNestedEmptyFolders)
	# Spacer 
	t16_label_ZeroLengths = m_tkntr.Label( t16_frame7, text=" " )
	t16_label_ZeroLengths.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	# Treat Zero Length Files 
	t16_chkvr_TreatZeroesAsNotThere = m_tkntr.IntVar()
	t16_chkvr_TreatZeroesAsNotThere.set(1)
	t16_chkbx_TreatZeroesAsNotThere = m_tkntr.Checkbutton( t16_frame7, text="Treat Zero length files as not there", \
		variable=t16_chkvr_TreatZeroesAsNotThere)
	t16_chkbx_TreatZeroesAsNotThere.pack( side=m_tkntr.LEFT, anchor=m_tkntr.W, padx=tk_padx(), pady=tk_pady() )
	if __debug__ : # declare this to be a Settings object 
		Set_ESD_EMSA( apltry.EnumSettingDef.TrmtryDoTreatZeroAsNothing, EnumMetaSettingAspect.WidgetObject, t16_chkvr_TreatZeroesAsNotThere)
	# --------- Frame 8 Controls Line 2 
	t16_frame8 = m_tkntr.Frame( t16_tab )
	t16_frame8.pack( fill = m_tkntr.X)
	# Exceptions label
	t16_label_Exceptions = m_tkntr.Label( t16_frame8, text=" Exceptions: " )
	t16_label_Exceptions.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	# Keep If Empty
	t16_chkvr_TreatKeepIfEmptyAsPresent = m_tkntr.IntVar()
	t16_chkvr_TreatKeepIfEmptyAsPresent.set(0)
	t16_chkbx_TreatKeepIfEmptyAsPresent = m_tkntr.Checkbutton(t16_frame8 , text="Treat .keepifempty as a presence", \
		variable=t16_chkvr_TreatKeepIfEmptyAsPresent)
	t16_chkbx_TreatKeepIfEmptyAsPresent.pack( side=m_tkntr.LEFT, anchor=m_tkntr.W, padx=tk_padx(), pady=tk_pady() )
	if __debug__ : # declare this to be a Settings object
		Set_ESD_EMSA( apltry.EnumSettingDef.TrmtryDoExceptKeepIfEmpty, EnumMetaSettingAspect.WidgetObject, t16_chkvr_TreatKeepIfEmptyAsPresent)
	# NoMedia
	t16_chkvr_TreatNoMediaAsPresent = m_tkntr.IntVar()
	t16_chkvr_TreatNoMediaAsPresent.set(0)
	t16_chkbx_TreatNoMediaAsPresent = m_tkntr.Checkbutton( t16_frame8, text="Treat .nomedia as a presence", \
		variable=t16_chkvr_TreatNoMediaAsPresent)
	t16_chkbx_TreatNoMediaAsPresent.pack( side=m_tkntr.LEFT, anchor=m_tkntr.W, padx=tk_padx(), pady=tk_pady() )
	if __debug__ : # declare this to be a Settings object 
		Set_ESD_EMSA( apltry.EnumSettingDef.TrmtryDoExceptNoMedia, EnumMetaSettingAspect.WidgetObject, t16_chkvr_TreatNoMediaAsPresent)
	# --------- Frame 9 
	t16_frame9 = m_tkntr.Frame( t16_tab )
	t16_frame9.pack( fill = m_tkntr.X)
	#
	t16_chkvr_RenameFoldersRatherThanDelete = m_tkntr.IntVar()
	t16_chkvr_RenameFoldersRatherThanDelete.set(0)
	t16_chkbx_RenameFoldersRatherThanDelete = m_tkntr.Checkbutton(t16_frame9 , text="Rename Folders rather than Delete them.", \
		variable=t16_chkvr_RenameFoldersRatherThanDelete)
	t16_chkbx_RenameFoldersRatherThanDelete.pack( side=m_tkntr.LEFT, anchor=m_tkntr.W, padx=tk_padx(), pady=tk_pady() )
	if __debug__ : # declare this to be a Settings object
		Set_ESD_EMSA( apltry.EnumSettingDef.TrmtryDoRenameInsteadOfDelete, EnumMetaSettingAspect.WidgetObject, t16_chkvr_RenameFoldersRatherThanDelete)
	# Deletion mode listbox
	t16_label_EnumDeleteMode = m_tkntr.Label( t16_frame9, text="Deletion action mode:" ) # , width=12
	t16_label_EnumDeleteMode.pack( side= m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) # , expand=True, fill=m_tkntr.BOTH, 
	t16_cb_EnumDeleteMode = PairTupleCombobox( t16_frame9, apltry.EnumDeleteMode_LstPairTpls(), apltry.EnumDeleteMode_Default(), state="readonly", width = 20) 
	t16_cb_EnumDeleteMode.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() )
	if __debug__ : # declare this to be a Settings object
		Set_ESD_EMSA( apltry.EnumSettingDef.TrmtryDeletionMode, EnumMetaSettingAspect.WidgetObject, t16_cb_EnumDeleteMode)
	t16_label_spacer = m_tkntr.Label( t16_frame9, text=( " " * 10 ) ) # , width=12
	t16_label_spacer.pack( side= m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) # , expand=True, fill=m_tkntr.BOTH, 
	# DelFromDeep
	t16_chkvr_DelFromDeep = m_tkntr.IntVar()
	t16_chkvr_DelFromDeep.set(1)
	t16_chkbx_DelFromDeep = m_tkntr.Checkbutton( t16_frame9, text="Delete from deepest", \
		variable=t16_chkvr_DelFromDeep)
	t16_chkbx_DelFromDeep.pack( side=m_tkntr.LEFT, anchor=m_tkntr.W, padx=tk_padx(), pady=tk_pady() )
	if __debug__ : # declare this to be a Settings object
		Set_ESD_EMSA( apltry.EnumSettingDef.TrmtryDoDelFromDeepest, EnumMetaSettingAspect.WidgetObject, t16_chkvr_DelFromDeep)
	# --------- Frame 10 Action button for Trimatry
	t16_frame10 = m_tkntr.Frame( t16_tab)
	t16_frame10.pack( fill = m_tkntr.X)
	# action button
	t16_button_action_var = m_tkntr.StringVar()
	t16_button_action_var.set( Strng_RunProcess() )
	t16_button_action = m_tkntr.Button( t16_frame10, textvariable=t16_button_action_var, command=t16_on_button_trimatry)
	t16_button_action.pack( side=m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	# status label at right hand side
	t16_label_action_status = m_tkntr.Label( t16_frame10, text="_", fg = "blue" ) # , width=60
	t16_label_action_status.pack( side=m_tkntr.RIGHT, padx=tk_padx(), pady=tk_pady() )
	#
	# ========== Make a tab for: Replatry ( t14 replaces t04 )
	t14_tab = m_tkntr.Frame(nb)
	nb.add( t14_tab, text= "Replatry")
	# --------- Frame T temporary advice 
	t14_frameT = m_tkntr.Frame( t14_tab, bg=colour_bg_replatry())
	t14_frameT.pack( fill = m_tkntr.X)
	t14_label_temp = m_tkntr.Label( t14_frameT, bg=colour_bg_replatry(), \
		text= "Re-Place a Tree! Copy, Move & Delete variations. NOTE: Only some parts of Replatry are currently functional")
	t14_label_temp.pack( side=m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() )
	# Make a sub-notebook i.e. tabbed interface
	t14_snb = m_tkntr_ttk.Notebook( t14_tab)
	t14_snb.pack( expand=1, fill=m_tkntr.BOTH)
	# ~~~~~~~~~~ Make subtab = About
	t14_tab_0 = m_tkntr.Frame( t14_snb)
	t14_snb.add( t14_tab_0, text= "Overview")
	# --------- Frame 1 Title
	t14_About_frame_1 = m_tkntr.Frame( t14_tab_0, bg= colour_bg_replatry())
	t14_About_frame_1.pack( fill= m_tkntr.X)
	t14_About_label_dscrptn = m_tkntr.Label( t14_About_frame_1, text= "About the Replatry features", bg= colour_bg_replatry())
	t14_About_label_dscrptn.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() )
	# --------- Frame 2 About A
	t14_About_frame_2 = m_tkntr.Frame( t14_tab_0, bg= colour_bg_replatry())
	t14_About_frame_2.pack( fill= m_tkntr.BOTH, expand=True )
	#
	t14_About_scrolltext = m_tkntr_tkst.ScrolledText( t14_About_frame_2, bg= colour_bg_replatry(), wrap= "word", relief= "solid", borderwidth= 0 ) # , bordercolor= colour_bg_replatry()
	t14_About_scrolltext.pack( anchor= m_tkntr.N, side=m_tkntr.LEFT, fill=m_tkntr.BOTH, expand=True )
	t14_About_scrolltext.insert(m_tkntr.INSERT, app_show_about_replatry() )
	# ~~~~~~~~~~ Make 1st subtab = Distinctry
	t14_tab_1 = m_tkntr.Frame( t14_snb)
	t14_snb.add( t14_tab_1, text= "Distinctry")
	# --------- Frame 1 Title
	t14_Dstnctry_frame_1 = m_tkntr.Frame( t14_tab_1, bg= colour_bg_replatry())
	t14_Dstnctry_frame_1.pack( fill= m_tkntr.X)
	t14_Dstnctry_label_dscrptn = m_tkntr.Label( t14_Dstnctry_frame_1, text= app_show_about_Distinctry(), bg= colour_bg_replatry())
	t14_Dstnctry_label_dscrptn.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() )
	# --------- Frame 2 About A
	t14_Dstnctry_frame_2 = m_tkntr.Frame( t14_tab_1, bg= colour_bg_safely())
	t14_Dstnctry_frame_2.pack( fill= m_tkntr.X)
	t14_Dstnctry_label_about_path_A = m_tkntr.Label( t14_Dstnctry_frame_2, text= "A is the source of files to replicate", bg= colour_bg_safely())
	t14_Dstnctry_label_about_path_A.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() ) # 
	# --------- Frame 3 Select Path A
	t14_Dstnctry_frame_3 = m_tkntr.Frame( t14_tab_1, bg= colour_bg_safely())
	t14_Dstnctry_frame_3.pack( fill= m_tkntr.X)
	t14_Dstnctry_label_browse_path_A = m_tkntr.Label( t14_Dstnctry_frame_3, text= "Path A", bg= colour_bg_safely())
	t14_Dstnctry_label_browse_path_A.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() ) 
	t14_Dstnctry_label_browse_path_A.bind('<Button-3>',  lambda x: t14_Dstnctry_label_path_A_on_rclick_clear() ) # because <Button-3> is the Tkinter code for a right-click
	# 
	t14_Dstnctry_var_path_A = m_tkntr.StringVar()
	t14_Dstnctry_entry_path_A = m_tkntr.Entry( t14_Dstnctry_frame_3, textvariable= t14_Dstnctry_var_path_A)
	t14_Dstnctry_entry_path_A.pack( side= m_tkntr.LEFT, fill= m_tkntr.X, expand= 1, padx= tk_padx(), pady= tk_pady() ) # 
	t14_Dstnctry_button_browse_path_A = m_tkntr.Button( t14_Dstnctry_frame_3, text= Strng_Browse(), command= t14_Dstnctry_on_button_browse_path_A )
	t14_Dstnctry_button_browse_path_A.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() ) #
	# --------- Frame 4 About B
	t14_Dstnctry_frame_4 = m_tkntr.Frame( t14_tab_1, bg= colour_bg_compare())
	t14_Dstnctry_frame_4.pack( fill= m_tkntr.X)
	t14_Dstnctry_label_about_path_B = m_tkntr.Label( t14_Dstnctry_frame_4, text= "B is the location to check against", bg= colour_bg_compare())
	t14_Dstnctry_label_about_path_B.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() ) # 
	# --------- Frame 5 Select Path B
	t14_Dstnctry_frame_5 = m_tkntr.Frame( t14_tab_1, bg= colour_bg_compare())
	t14_Dstnctry_frame_5.pack( fill= m_tkntr.X)
	t14_Dstnctry_label_browse_path_B = m_tkntr.Label( t14_Dstnctry_frame_5, text= "Path B", bg= colour_bg_compare())
	t14_Dstnctry_label_browse_path_B.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() ) 
	t14_Dstnctry_label_browse_path_B.bind('<Button-3>',  lambda x: t14_Dstnctry_label_path_B_on_rclick_clear() ) # because <Button-3> is the Tkinter code for a right-click
	# 
	t14_Dstnctry_var_path_B = m_tkntr.StringVar()
	t14_Dstnctry_entry_path_B = m_tkntr.Entry( t14_Dstnctry_frame_5, textvariable= t14_Dstnctry_var_path_B)
	t14_Dstnctry_entry_path_B.pack( side= m_tkntr.LEFT, fill= m_tkntr.X, expand= 1, padx= tk_padx(), pady= tk_pady() ) # 
	t14_Dstnctry_button_browse_path_B = m_tkntr.Button( t14_Dstnctry_frame_5, text= Strng_Browse(), command= t14_Dstnctry_on_button_browse_path_B)
	t14_Dstnctry_button_browse_path_B.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() ) #
	# --------- Frame 6 About C
	t14_Dstnctry_frame_6 = m_tkntr.Frame( t14_tab_1, bg= colour_bg_modify())
	t14_Dstnctry_frame_6.pack( fill= m_tkntr.X)
	# left label
	t14_Dstnctry_label_about_path_C = m_tkntr.Label( t14_Dstnctry_frame_6, text= "C is the location to put A files that are not in B", bg= colour_bg_modify())
	t14_Dstnctry_label_about_path_C.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() ) # 
	# right label
	t14_Dstnctry_label_remark_path_C = m_tkntr.Label( t14_Dstnctry_frame_6, text= "Must be an empty folder", fg = "blue", bg= colour_bg_modify())
	t14_Dstnctry_label_remark_path_C.pack( side= m_tkntr.RIGHT, padx= tk_padx(), pady= tk_pady() ) # 
	# --------- Frame 7 Select Path C
	t14_Dstnctry_frame_7 = m_tkntr.Frame( t14_tab_1, bg= colour_bg_modify())
	t14_Dstnctry_frame_7.pack( fill= m_tkntr.X)
	t14_Dstnctry_label_browse_path_C = m_tkntr.Label( t14_Dstnctry_frame_7, text= "Path C", bg= colour_bg_modify())
	t14_Dstnctry_label_browse_path_C.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() ) # 
	t14_Dstnctry_label_browse_path_C.bind('<Button-3>',  lambda x: t14_Dstnctry_label_path_C_on_rclick_clear() ) # because <Button-3> is the Tkinter code for a right-click
	# entry and browse button
	t14_Dstnctry_var_path_C = m_tkntr.StringVar()
	t14_Dstnctry_entry_path_C = m_tkntr.Entry( t14_Dstnctry_frame_7, textvariable= t14_Dstnctry_var_path_C)
	t14_Dstnctry_entry_path_C.pack( side= m_tkntr.LEFT, fill= m_tkntr.X, expand= 1, padx= tk_padx(), pady= tk_pady() ) # 
	t14_Dstnctry_button_browse_path_C = m_tkntr.Button( t14_Dstnctry_frame_7, text= Strng_Browse(), command= t14_Dstnctry_on_button_browse_path_C)
	t14_Dstnctry_button_browse_path_C.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() ) #
	# --------- Frame 8 Options
	t14_Dstnctry_frame_8 = m_tkntr.Frame( t14_tab_1)
	t14_Dstnctry_frame_8.pack( fill = m_tkntr.X)
	t14_Dstnctry_label_options = m_tkntr.Label( t14_Dstnctry_frame_8, text="Options")
	t14_Dstnctry_label_options.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) # 
	# Recreate the source parent folder in the destination
	t14_Dstnctry_chkvr_RecreateSrcFldr = m_tkntr.IntVar()
	t14_Dstnctry_chkbx_RecreateSrcFldr = m_tkntr.Checkbutton( t14_Dstnctry_frame_8, text="Recreate source folder", variable=t14_Dstnctry_chkvr_RecreateSrcFldr)
	t14_Dstnctry_chkbx_RecreateSrcFldr.pack(side=m_tkntr.LEFT, anchor=m_tkntr.W, expand=m_tkntr.YES, padx=tk_padx(), pady=tk_pady() )
	# time and date stamps
	t14_Dstnctry_chkvr_KeepTuch = m_tkntr.IntVar()
	t14_Dstnctry_chkbx_KeepTuch = m_tkntr.Checkbutton( t14_Dstnctry_frame_8, text="Keep Touch", variable=t14_Dstnctry_chkvr_KeepTuch)
	t14_Dstnctry_chkbx_KeepTuch.pack(side=m_tkntr.LEFT, anchor=m_tkntr.W, expand=m_tkntr.YES, padx=tk_padx(), pady=tk_pady() )
	# flags, read-only, hidden, system
	t14_Dstnctry_chkvr_KeepAttr = m_tkntr.IntVar()
	t14_Dstnctry_chkbx_KeepAttr = m_tkntr.Checkbutton( t14_Dstnctry_frame_8, text="Keep Attributes", variable=t14_Dstnctry_chkvr_KeepAttr)
	t14_Dstnctry_chkbx_KeepAttr.pack(side=m_tkntr.LEFT, anchor=m_tkntr.W, expand=m_tkntr.YES, padx=tk_padx(), pady=tk_pady() )
	# linux file-system permissions
	t14_Dstnctry_chkvr_KeepPrmt = m_tkntr.IntVar()
	t14_Dstnctry_chkbx_KeepPrmt = m_tkntr.Checkbutton( t14_Dstnctry_frame_8, text="Keep Permissions", variable=t14_Dstnctry_chkvr_KeepPrmt)
	t14_Dstnctry_chkbx_KeepPrmt.pack(side=m_tkntr.LEFT, anchor=m_tkntr.W, expand=m_tkntr.YES, padx=tk_padx(), pady=tk_pady() )
	# OS specific and ACLs
	t14_Dstnctry_chkvr_KeepMeta = m_tkntr.IntVar()
	t14_Dstnctry_chkbx_KeepMeta = m_tkntr.Checkbutton( t14_Dstnctry_frame_8, text="Keep Metadata", variable=t14_Dstnctry_chkvr_KeepMeta)
	t14_Dstnctry_chkbx_KeepMeta.pack(side=m_tkntr.LEFT, anchor=m_tkntr.W, expand=m_tkntr.YES, padx=tk_padx(), pady=tk_pady() )
	# --------- Frame 9 Action button
	t14_Dstnctry_frame_9 = m_tkntr.Frame( t14_tab_1)
	t14_Dstnctry_frame_9.pack( fill= m_tkntr.X)
	t14_Dstnctry_button_Process_text = m_tkntr.StringVar()
	t14_Dstnctry_button_Process_text.set( Strng_RunProcess() )
	t14_Dstnctry_button_Process = m_tkntr.Button( t14_Dstnctry_frame_9, textvariable= t14_Dstnctry_button_Process_text, command= t14_Dstnctry_on_button_Process)
	t14_Dstnctry_button_Process.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() ) #
	t14_Dstnctry_label_Process_Status = m_tkntr.Label( t14_Dstnctry_frame_9, text= "Note: Distinctry is not yet functional.", fg = "blue")
	t14_Dstnctry_label_Process_Status.pack( side= m_tkntr.RIGHT, padx= tk_padx(), pady= tk_pady() )
	#
	# ~~~~~~~~~~ Make subtab = Clonatry
	t14_tab_2 = m_tkntr.Frame( t14_snb)
	t14_snb.add( t14_tab_2, text= "Clonatry")
	# --------- Frame 1 Title
	t14_Clntry_frame_1 = m_tkntr.Frame( t14_tab_2, bg= colour_bg_replatry())
	t14_Clntry_frame_1.pack( fill= m_tkntr.X)
	t14_Clntry_label_dscrptn = m_tkntr.Label( t14_Clntry_frame_1, text= app_show_about_Clonatry(), bg= colour_bg_replatry())
	t14_Clntry_label_dscrptn.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() )
	# --------- Frame 2 About A
	t14_Clntry_frame_2 = m_tkntr.Frame( t14_tab_2, bg= colour_bg_safely())
	t14_Clntry_frame_2.pack( fill= m_tkntr.X)
	t14_Clntry_label_about_path_A = m_tkntr.Label( t14_Clntry_frame_2, text= "Specify the source location", bg= colour_bg_safely())
	t14_Clntry_label_about_path_A.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() ) # 
	# --------- Frame 3 Select Path A
	t14_Clntry_frame_3 = m_tkntr.Frame( t14_tab_2, bg= colour_bg_safely())
	t14_Clntry_frame_3.pack( fill= m_tkntr.X)
	t14_Clntry_label_browse_path_A = m_tkntr.Label( t14_Clntry_frame_3, text= "Path", bg= colour_bg_safely())
	t14_Clntry_label_browse_path_A.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() ) 
	t14_Clntry_label_browse_path_A.bind('<Button-3>',  lambda x: t14_Clntry_label_path_A_on_rclick_clear() ) # because <Button-3> is the Tkinter code for a right-click
	# 
	t14_Clntry_var_path_A = m_tkntr.StringVar()
	t14_Clntry_entry_path_A = m_tkntr.Entry( t14_Clntry_frame_3, textvariable= t14_Clntry_var_path_A)
	t14_Clntry_entry_path_A.pack( side= m_tkntr.LEFT, fill= m_tkntr.X, expand= 1, padx= tk_padx(), pady= tk_pady() ) 
	if __debug__ : # declare this to be a Settings object
		Set_ESD_EMSA( apltry.EnumSettingDef.ClntryStrPathA, EnumMetaSettingAspect.WidgetObject, t14_Clntry_var_path_A)
	# 
	t14_Clntry_button_browse_path_A = m_tkntr.Button( t14_Clntry_frame_3, text= Strng_Browse(), command= t14_Clntry_on_button_browse_path_A )
	t14_Clntry_button_browse_path_A.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() ) #
	# --------- Frame 6 About B
	t14_Clntry_frame_6 = m_tkntr.Frame( t14_tab_2, bg= colour_bg_modify())
	t14_Clntry_frame_6.pack( fill= m_tkntr.X)
	# left label
	t14_Clntry_label_about_path_B = m_tkntr.Label( t14_Clntry_frame_6, text= "Specify the destination location", bg= colour_bg_modify())
	t14_Clntry_label_about_path_B.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() ) # 
	# right label
	t14_Clntry_label_remark_path_B = m_tkntr.Label( t14_Clntry_frame_6, text= "Must be an empty folder", fg = "blue", bg= colour_bg_modify())
	t14_Clntry_label_remark_path_B.pack( side= m_tkntr.RIGHT, padx= tk_padx(), pady= tk_pady() ) # 
	# --------- Frame 7 Select Path B
	t14_Clntry_frame_7 = m_tkntr.Frame( t14_tab_2, bg= colour_bg_modify())
	t14_Clntry_frame_7.pack( fill= m_tkntr.X)
	t14_Clntry_label_browse_path_B = m_tkntr.Label( t14_Clntry_frame_7, text= "Path", bg= colour_bg_modify())
	t14_Clntry_label_browse_path_B.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() ) 
	t14_Clntry_label_browse_path_B.bind('<Button-3>',  lambda x: t14_Clntry_label_path_C_on_rclick_clear() ) # because <Button-3> is the Tkinter code for a right-click
	# 
	t14_Clntry_var_path_B = m_tkntr.StringVar()
	t14_Clntry_entry_path_B = m_tkntr.Entry( t14_Clntry_frame_7, textvariable= t14_Clntry_var_path_B)
	t14_Clntry_entry_path_B.pack( side= m_tkntr.LEFT, fill= m_tkntr.X, expand= 1, padx= tk_padx(), pady= tk_pady() ) # 
	if __debug__ : # declare this to be a Settings object
		Set_ESD_EMSA( apltry.EnumSettingDef.ClntryStrPathB, EnumMetaSettingAspect.WidgetObject, t14_Clntry_var_path_B)
	#
	t14_Clntry_button_browse_path_B = m_tkntr.Button( t14_Clntry_frame_7, text= Strng_Browse(), command= t14_Clntry_on_button_browse_path_B)
	t14_Clntry_button_browse_path_B.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() ) #
	# --------- Frame 8 Options
	t14_Clntry_frame_8 = m_tkntr.Frame( t14_tab_2)
	t14_Clntry_frame_8.pack( fill = m_tkntr.X)
	t14_Clntry_label_options = m_tkntr.Label( t14_Clntry_frame_8, text="Options")
	t14_Clntry_label_options.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) # 
	# Recreate the source parent folder in the destination
	t14_Clntry_chkvr_RecreateSrcFldr = m_tkntr.IntVar()
	t14_Clntry_chkbx_RecreateSrcFldr = m_tkntr.Checkbutton( t14_Clntry_frame_8, text="Recreate source folder", variable=t14_Clntry_chkvr_RecreateSrcFldr)
	t14_Clntry_chkbx_RecreateSrcFldr.pack(side=m_tkntr.LEFT, anchor=m_tkntr.W, expand=m_tkntr.YES, padx=tk_padx(), pady=tk_pady() )
	# t14_Clntry_chkbx_RecreateSrcFldr.configure( state= m_tkntr.DISABLED)
	if __debug__ : # declare this to be a Settings object
		Set_ESD_EMSA( apltry.EnumSettingDef.ClntryRecreateSourceFolder, EnumMetaSettingAspect.WidgetObject, t14_Clntry_chkvr_RecreateSrcFldr)
	# time and date stamps
	t14_Clntry_chkvr_KeepTuch = m_tkntr.IntVar(value=1)
	t14_Clntry_chkbx_KeepTuch = m_tkntr.Checkbutton( t14_Clntry_frame_8, text="Keep Touch", variable=t14_Clntry_chkvr_KeepTuch)
	t14_Clntry_chkbx_KeepTuch.pack(side=m_tkntr.LEFT, anchor=m_tkntr.W, expand=m_tkntr.YES, padx=tk_padx(), pady=tk_pady() )
	t14_Clntry_chkbx_KeepTuch.configure( state= m_tkntr.DISABLED)
	if __debug__ : # declare this to be a Settings object
		Set_ESD_EMSA( apltry.EnumSettingDef.ClntryDoKeepTouch, EnumMetaSettingAspect.WidgetObject, t14_Clntry_chkvr_KeepTuch)
	# flags, read-only, hidden, system
	t14_Clntry_chkvr_KeepAttr = m_tkntr.IntVar(value=1)
	t14_Clntry_chkbx_KeepAttr = m_tkntr.Checkbutton( t14_Clntry_frame_8, text="Keep Attributes", variable=t14_Clntry_chkvr_KeepAttr)
	t14_Clntry_chkbx_KeepAttr.pack(side=m_tkntr.LEFT, anchor=m_tkntr.W, expand=m_tkntr.YES, padx=tk_padx(), pady=tk_pady() )
	t14_Clntry_chkbx_KeepAttr.configure( state= m_tkntr.DISABLED)
	if __debug__ : # declare this to be a Settings object
		Set_ESD_EMSA( apltry.EnumSettingDef.ClntryDoKeepAttrib, EnumMetaSettingAspect.WidgetObject, t14_Clntry_chkvr_KeepAttr)
	# linux file-system permissions
	t14_Clntry_chkvr_KeepPrmt = m_tkntr.IntVar(value=1)
	t14_Clntry_chkbx_KeepPrmt = m_tkntr.Checkbutton( t14_Clntry_frame_8, text="Keep Permissions", variable=t14_Clntry_chkvr_KeepPrmt)
	t14_Clntry_chkbx_KeepPrmt.pack(side=m_tkntr.LEFT, anchor=m_tkntr.W, expand=m_tkntr.YES, padx=tk_padx(), pady=tk_pady() )
	t14_Clntry_chkbx_KeepPrmt.configure( state= m_tkntr.DISABLED)
	if __debug__ : # declare this to be a Settings object
		Set_ESD_EMSA( apltry.EnumSettingDef.ClntryDoKeepPerm, EnumMetaSettingAspect.WidgetObject, t14_Clntry_chkvr_KeepPrmt)
	# OS specific and ACLs
	t14_Clntry_chkvr_KeepMeta = m_tkntr.IntVar(value=0)
	t14_Clntry_chkbx_KeepMeta = m_tkntr.Checkbutton( t14_Clntry_frame_8, text="Keep Metadata", variable=t14_Clntry_chkvr_KeepMeta)
	t14_Clntry_chkbx_KeepMeta.pack(side=m_tkntr.LEFT, anchor=m_tkntr.W, expand=m_tkntr.YES, padx=tk_padx(), pady=tk_pady() )
	t14_Clntry_chkbx_KeepMeta.configure( state= m_tkntr.DISABLED)
	if __debug__ : # declare this to be a Settings object
		Set_ESD_EMSA( apltry.EnumSettingDef.ClntryDoKeepMeta, EnumMetaSettingAspect.WidgetObject, t14_Clntry_chkvr_KeepMeta)
	# --------- Frame 9 Action button
	t14_Clntry_frame_9 = m_tkntr.Frame( t14_tab_2)
	t14_Clntry_frame_9.pack( fill= m_tkntr.X)
	t14_Clntry_button_Process_text = m_tkntr.StringVar()
	t14_Clntry_button_Process_text.set( Strng_RunProcess() )
	t14_Clntry_button_Process = m_tkntr.Button( t14_Clntry_frame_9, textvariable= t14_Clntry_button_Process_text, command= t14_Clntry_on_button_Process)
	t14_Clntry_button_Process.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() ) #
	t14_Clntry_label_Process_Status = m_tkntr.Label( t14_Clntry_frame_9, text= "_", fg = "blue")
	t14_Clntry_label_Process_Status.pack( side= m_tkntr.RIGHT, padx= tk_padx(), pady= tk_pady() )
	#
	# ~~~~~~~~~~ Make subtab = Dubatry
	t14_tab_3 = m_tkntr.Frame( t14_snb)
	t14_snb.add( t14_tab_3, text= "Dubatry")
	# --------- Frame 1 Title
	t14_Dbtry_frame_1 = m_tkntr.Frame( t14_tab_3, bg= colour_bg_replatry())
	t14_Dbtry_frame_1.pack( fill= m_tkntr.X)
	t14_Dbtry_label_dscrptn = m_tkntr.Label( t14_Dbtry_frame_1, text= app_show_about_Dubatry(), bg= colour_bg_replatry())
	t14_Dbtry_label_dscrptn.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() )
	# --------- Frame 2 About A
	t14_Dbtry_frame_2 = m_tkntr.Frame( t14_tab_3, bg= colour_bg_safely())
	t14_Dbtry_frame_2.pack( fill= m_tkntr.X)
	t14_Dbtry_label_about_path_A = m_tkntr.Label( t14_Dbtry_frame_2, text= "Specify the source location", bg= colour_bg_safely())
	t14_Dbtry_label_about_path_A.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() ) # 
	# --------- Frame 3 Select Path A
	t14_Dbtry_frame_3 = m_tkntr.Frame( t14_tab_3, bg= colour_bg_safely())
	t14_Dbtry_frame_3.pack( fill= m_tkntr.X)
	t14_Dbtry_label_browse_path_A = m_tkntr.Label( t14_Dbtry_frame_3, text= "Path", bg= colour_bg_safely())
	t14_Dbtry_label_browse_path_A.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() ) 
	t14_Dbtry_label_browse_path_A.bind('<Button-3>',  lambda x: t14_Dbtry_label_path_A_on_rclick_clear() ) # because <Button-3> is the Tkinter code for a right-click
	# 
	t14_Dbtry_var_path_A = m_tkntr.StringVar()
	t14_Dbtry_entry_path_A = m_tkntr.Entry( t14_Dbtry_frame_3, textvariable= t14_Dbtry_var_path_A)
	t14_Dbtry_entry_path_A.pack( side= m_tkntr.LEFT, fill= m_tkntr.X, expand= 1, padx= tk_padx(), pady= tk_pady() ) # 
	t14_Dbtry_button_browse_path_A = m_tkntr.Button( t14_Dbtry_frame_3, text= Strng_Browse(), command= t14_Dbtry_on_button_browse_path_A )
	t14_Dbtry_button_browse_path_A.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() ) #
	# --------- Frame 6 About C
	t14_Dbtry_frame_6 = m_tkntr.Frame( t14_tab_3, bg= colour_bg_modify())
	t14_Dbtry_frame_6.pack( fill= m_tkntr.X)
	t14_Dbtry_label_about_path_C = m_tkntr.Label( t14_Dbtry_frame_6, text= "Specify the destination location", bg= colour_bg_modify())
	t14_Dbtry_label_about_path_C.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() ) # 
	# --------- Frame 7 Select Path C
	t14_Dbtry_frame_7 = m_tkntr.Frame( t14_tab_3, bg= colour_bg_modify())
	t14_Dbtry_frame_7.pack( fill= m_tkntr.X)
	t14_Dbtry_label_browse_path_C = m_tkntr.Label( t14_Dbtry_frame_7, text= "Path", bg= colour_bg_modify())
	t14_Dbtry_label_browse_path_C.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() ) 
	t14_Dbtry_label_browse_path_C.bind('<Button-3>',  lambda x: t14_Dbtry_label_path_C_on_rclick_clear() ) # because <Button-3> is the Tkinter code for a right-click
	# 
	t14_Dbtry_var_path_C = m_tkntr.StringVar()
	t14_Dbtry_entry_path_C = m_tkntr.Entry( t14_Dbtry_frame_7, textvariable= t14_Dbtry_var_path_C)
	t14_Dbtry_entry_path_C.pack( side= m_tkntr.LEFT, fill= m_tkntr.X, expand= 1, padx= tk_padx(), pady= tk_pady() ) # 
	t14_Dbtry_button_browse_path_C = m_tkntr.Button( t14_Dbtry_frame_7, text= Strng_Browse(), command= t14_Dbtry_on_button_browse_path_C)
	t14_Dbtry_button_browse_path_C.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() ) #
	# --------- Frame 8 Action button
	t14_Dbtry_frame_8 = m_tkntr.Frame( t14_tab_3)
	t14_Dbtry_frame_8.pack( fill= m_tkntr.X)
	t14_Dbtry_button_Process_text = m_tkntr.StringVar()
	t14_Dbtry_button_Process_text.set( Strng_RunProcess() )
	t14_Dbtry_button_Process = m_tkntr.Button( t14_Dbtry_frame_8, textvariable= t14_Dbtry_button_Process_text, command= t14_Dbtry_on_button_Process)
	t14_Dbtry_button_Process.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() ) #
	t14_Dbtry_label_Process_Status = m_tkntr.Label( t14_Dbtry_frame_8, text= "Note: not yet functional.", fg = "blue")
	t14_Dbtry_label_Process_Status.pack( side= m_tkntr.RIGHT, padx= tk_padx(), pady= tk_pady() )
	#
	# ~~~~~~~~~~ Make subtab = Mergetry
	t14_tab_4 = m_tkntr.Frame( t14_snb)
	t14_snb.add( t14_tab_4, text= "Mergetry")
	# --------- Frame 1 Title
	t14_Mrgtry_frame_1 = m_tkntr.Frame( t14_tab_4, bg= colour_bg_replatry())
	t14_Mrgtry_frame_1.pack( fill= m_tkntr.X)
	t14_Mrgtry_label_dscrptn = m_tkntr.Label( t14_Mrgtry_frame_1, text= app_show_about_Mergetry(), bg= colour_bg_replatry())
	t14_Mrgtry_label_dscrptn.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() )
	# --------- Frame 2 About A
	t14_Mrgtry_frame_2 = m_tkntr.Frame( t14_tab_4, bg= colour_bg_safely())
	t14_Mrgtry_frame_2.pack( fill= m_tkntr.X)
	t14_Mrgtry_label_about_path_A = m_tkntr.Label( t14_Mrgtry_frame_2, text= "Specify the destination location", bg= colour_bg_safely())
	t14_Mrgtry_label_about_path_A.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() ) # 
	# --------- Frame 3 Select Path A
	t14_Mrgtry_frame_3 = m_tkntr.Frame( t14_tab_4, bg= colour_bg_safely())
	t14_Mrgtry_frame_3.pack( fill= m_tkntr.X)
	t14_Mrgtry_label_browse_path_A = m_tkntr.Label( t14_Mrgtry_frame_3, text= "Path", bg= colour_bg_safely())
	t14_Mrgtry_label_browse_path_A.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() ) 
	t14_Mrgtry_label_browse_path_A.bind('<Button-3>',  lambda x: t14_Mrgtry_label_path_A_on_rclick_clear() ) # because <Button-3> is the Tkinter code for a right-click
	# 
	t14_Mrgtry_var_path_A = m_tkntr.StringVar()
	t14_Mrgtry_entry_path_A = m_tkntr.Entry( t14_Mrgtry_frame_3, textvariable= t14_Mrgtry_var_path_A)
	t14_Mrgtry_entry_path_A.pack( side= m_tkntr.LEFT, fill= m_tkntr.X, expand= 1, padx= tk_padx(), pady= tk_pady() ) # 
	t14_Mrgtry_button_browse_path_A = m_tkntr.Button( t14_Mrgtry_frame_3, text= Strng_Browse(), command= t14_Mrgtry_on_button_browse_path_A )
	t14_Mrgtry_button_browse_path_A.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() ) #
	# --------- Frame 6 About C
	t14_Mrgtry_frame_6 = m_tkntr.Frame( t14_tab_4, bg= colour_bg_danger())
	t14_Mrgtry_frame_6.pack( fill= m_tkntr.X)
	t14_Mrgtry_label_about_path_C = m_tkntr.Label( t14_Mrgtry_frame_6, text= "Specify the destination location", bg= colour_bg_danger())
	t14_Mrgtry_label_about_path_C.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() ) # 
	# --------- Frame 7 Select Path C
	t14_Mrgtry_frame_7 = m_tkntr.Frame( t14_tab_4, bg= colour_bg_danger())
	t14_Mrgtry_frame_7.pack( fill= m_tkntr.X)
	t14_Mrgtry_label_browse_path_C = m_tkntr.Label( t14_Mrgtry_frame_7, text= "Path", bg= colour_bg_danger())
	t14_Mrgtry_label_browse_path_C.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() ) 
	t14_Mrgtry_label_browse_path_C.bind('<Button-3>',  lambda x: t14_Mrgtry_label_path_C_on_rclick_clear() ) # because <Button-3> is the Tkinter code for a right-click
	# 
	t14_Mrgtry_var_path_C = m_tkntr.StringVar()
	t14_Mrgtry_entry_path_C = m_tkntr.Entry( t14_Mrgtry_frame_7, textvariable= t14_Mrgtry_var_path_C)
	t14_Mrgtry_entry_path_C.pack( side= m_tkntr.LEFT, fill= m_tkntr.X, expand= 1, padx= tk_padx(), pady= tk_pady() ) # 
	t14_Mrgtry_button_browse_path_C = m_tkntr.Button( t14_Mrgtry_frame_7, text= Strng_Browse(), command= t14_Mrgtry_on_button_browse_path_C)
	t14_Mrgtry_button_browse_path_C.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() ) #
	# --------- Frame 8 Action button
	t14_Mrgtry_frame_8 = m_tkntr.Frame( t14_tab_4)
	t14_Mrgtry_frame_8.pack( fill= m_tkntr.X)
	t14_Mrgtry_button_Process_text = m_tkntr.StringVar()
	t14_Mrgtry_button_Process_text.set( Strng_RunProcess() )
	t14_Mrgtry_button_Process = m_tkntr.Button( t14_Mrgtry_frame_8, textvariable= t14_Mrgtry_button_Process_text, command= t14_Mrgtry_on_button_Process)
	t14_Mrgtry_button_Process.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() ) #
	t14_Mrgtry_label_Process_Status = m_tkntr.Label( t14_Mrgtry_frame_8, text= "Note: not yet functional.", fg = "blue")
	t14_Mrgtry_label_Process_Status.pack( side= m_tkntr.RIGHT, padx= tk_padx(), pady= tk_pady() )
	#
	# ~~~~~~~~~~ Make subtab = Synopsitry
	t14_tab_5 = m_tkntr.Frame( t14_snb)
	t14_snb.add( t14_tab_5, text= "Synopsitry")
	# --------- Frame 1 Title
	t14_Synpstry_frame_1 = m_tkntr.Frame( t14_tab_5, bg= colour_bg_replatry())
	t14_Synpstry_frame_1.pack( fill= m_tkntr.X)
	t14_Synpstry_label_dscrptn = m_tkntr.Label( t14_Synpstry_frame_1, text= app_show_about_Synopsitry(), bg= colour_bg_replatry())
	t14_Synpstry_label_dscrptn.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() )
	# --------- Frame 2 About A
	t14_Synpstry_frame_2 = m_tkntr.Frame( t14_tab_5, bg= colour_bg_safely())
	t14_Synpstry_frame_2.pack( fill= m_tkntr.X)
	t14_Synpstry_label_about_path_A = m_tkntr.Label( t14_Synpstry_frame_2, text= "Specify the source location", bg= colour_bg_safely())
	t14_Synpstry_label_about_path_A.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() ) # 
	# --------- Frame 3 Select Path A
	t14_Synpstry_frame_3 = m_tkntr.Frame( t14_tab_5, bg= colour_bg_safely())
	t14_Synpstry_frame_3.pack( fill= m_tkntr.X)
	t14_Synpstry_label_browse_path_A = m_tkntr.Label( t14_Synpstry_frame_3, text= "Path", bg= colour_bg_safely())
	t14_Synpstry_label_browse_path_A.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() ) 
	t14_Synpstry_label_browse_path_A.bind('<Button-3>',  lambda x: t14_Synpstry_label_path_A_on_rclick_clear() ) # because <Button-3> is the Tkinter code for a right-click
	# 
	t14_Synpstry_var_path_A = m_tkntr.StringVar()
	t14_Synpstry_entry_path_A = m_tkntr.Entry( t14_Synpstry_frame_3, textvariable= t14_Synpstry_var_path_A)
	t14_Synpstry_entry_path_A.pack( side= m_tkntr.LEFT, fill= m_tkntr.X, expand= 1, padx= tk_padx(), pady= tk_pady() ) # 
	t14_Synpstry_button_browse_path_A = m_tkntr.Button( t14_Synpstry_frame_3, text= Strng_Browse(), command= t14_Synpstry_on_button_browse_path_A )
	t14_Synpstry_button_browse_path_A.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() ) #
	# --------- Frame 6 About C
	t14_Synpstry_frame_6 = m_tkntr.Frame( t14_tab_5, bg= colour_bg_modify())
	t14_Synpstry_frame_6.pack( fill= m_tkntr.X)
	# left label
	t14_Synpstry_label_about_path_C = m_tkntr.Label( t14_Synpstry_frame_6, text= "Specify the destination location", bg= colour_bg_modify())
	t14_Synpstry_label_about_path_C.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() ) # 
	# right label
	t14_Synpstry_label_remark_path_C = m_tkntr.Label( t14_Synpstry_frame_6, text= "Must be an empty folder", fg = "blue", bg= colour_bg_modify())
	t14_Synpstry_label_remark_path_C.pack( side= m_tkntr.RIGHT, padx= tk_padx(), pady= tk_pady() ) # 
	# --------- Frame 7 Select Path C
	t14_Synpstry_frame_7 = m_tkntr.Frame( t14_tab_5, bg= colour_bg_modify())
	t14_Synpstry_frame_7.pack( fill= m_tkntr.X)
	t14_Synpstry_label_browse_path_C = m_tkntr.Label( t14_Synpstry_frame_7, text= "Path", bg= colour_bg_modify())
	t14_Synpstry_label_browse_path_C.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() ) 
	t14_Synpstry_label_browse_path_C.bind('<Button-3>',  lambda x: t14_Synpstry_label_path_C_on_rclick_clear() ) # because <Button-3> is the Tkinter code for a right-click
	# 
	t14_Synpstry_var_path_C = m_tkntr.StringVar()
	t14_Synpstry_entry_path_C = m_tkntr.Entry( t14_Synpstry_frame_7, textvariable= t14_Synpstry_var_path_C)
	t14_Synpstry_entry_path_C.pack( side= m_tkntr.LEFT, fill= m_tkntr.X, expand= 1, padx= tk_padx(), pady= tk_pady() ) # 
	t14_Synpstry_button_browse_path_C = m_tkntr.Button( t14_Synpstry_frame_7, text= Strng_Browse(), command= t14_Synpstry_on_button_browse_path_C)
	t14_Synpstry_button_browse_path_C.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() ) #
	# --------- Frame 8 Options
	t14_Synpstry_frame_8 = m_tkntr.Frame( t14_tab_5)
	t14_Synpstry_frame_8.pack( fill = m_tkntr.X)
	t14_Synpstry_label_options = m_tkntr.Label( t14_Synpstry_frame_8, text="Options")
	t14_Synpstry_label_options.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) # 
	# Recreate the source parent folder in the destination
	t14_Synpstry_chkvr_RecreateSrcFldr = m_tkntr.IntVar(value=0)
	t14_Synpstry_chkbx_RecreateSrcFldr = m_tkntr.Checkbutton( t14_Synpstry_frame_8, text="Recreate source folder", variable=t14_Synpstry_chkvr_RecreateSrcFldr)
	t14_Synpstry_chkbx_RecreateSrcFldr.pack(side=m_tkntr.LEFT, anchor=m_tkntr.W, expand=m_tkntr.YES, padx=tk_padx(), pady=tk_pady() )
	t14_Synpstry_chkbx_RecreateSrcFldr.configure( state= m_tkntr.DISABLED)
	# time and date stamps
	t14_Synpstry_chkvr_KeepTuch = m_tkntr.IntVar(value=1)
	t14_Synpstry_chkbx_KeepTuch = m_tkntr.Checkbutton( t14_Synpstry_frame_8, text="Keep Touch", variable=t14_Synpstry_chkvr_KeepTuch)
	t14_Synpstry_chkbx_KeepTuch.pack(side=m_tkntr.LEFT, anchor=m_tkntr.W, expand=m_tkntr.YES, padx=tk_padx(), pady=tk_pady() )
	t14_Synpstry_chkbx_KeepTuch.configure( state= m_tkntr.DISABLED)
	# flags, read-only, hidden, system
	t14_Synpstry_chkvr_KeepAttr = m_tkntr.IntVar(value=1)
	t14_Synpstry_chkbx_KeepAttr = m_tkntr.Checkbutton( t14_Synpstry_frame_8, text="Keep Attributes", variable=t14_Synpstry_chkvr_KeepAttr)
	t14_Synpstry_chkbx_KeepAttr.pack(side=m_tkntr.LEFT, anchor=m_tkntr.W, expand=m_tkntr.YES, padx=tk_padx(), pady=tk_pady() )
	t14_Synpstry_chkbx_KeepAttr.configure( state= m_tkntr.DISABLED)
	# linux file-system permissions
	t14_Synpstry_chkvr_KeepPrmt = m_tkntr.IntVar(value=1)
	t14_Synpstry_chkbx_KeepPrmt = m_tkntr.Checkbutton( t14_Synpstry_frame_8, text="Keep Permissions", variable=t14_Synpstry_chkvr_KeepPrmt)
	t14_Synpstry_chkbx_KeepPrmt.pack(side=m_tkntr.LEFT, anchor=m_tkntr.W, expand=m_tkntr.YES, padx=tk_padx(), pady=tk_pady() )
	t14_Synpstry_chkbx_KeepPrmt.configure( state= m_tkntr.DISABLED)
	# OS specific and ACLs
	t14_Synpstry_chkvr_KeepMeta = m_tkntr.IntVar(value=0)
	t14_Synpstry_chkbx_KeepMeta = m_tkntr.Checkbutton( t14_Synpstry_frame_8, text="Keep Metadata", variable=t14_Synpstry_chkvr_KeepMeta)
	t14_Synpstry_chkbx_KeepMeta.pack(side=m_tkntr.LEFT, anchor=m_tkntr.W, expand=m_tkntr.YES, padx=tk_padx(), pady=tk_pady() )
	t14_Synpstry_chkbx_KeepMeta.configure( state= m_tkntr.DISABLED)
	# --------- Frame 9 Action button
	t14_Synpstry_frame_9 = m_tkntr.Frame( t14_tab_5)
	t14_Synpstry_frame_9.pack( fill= m_tkntr.X)
	t14_Synpstry_button_Process_text = m_tkntr.StringVar()
	t14_Synpstry_button_Process_text.set( Strng_RunProcess() )
	t14_Synpstry_button_Process = m_tkntr.Button( t14_Synpstry_frame_9, textvariable= t14_Synpstry_button_Process_text, command= t14_Synpstry_on_button_Process)
	t14_Synpstry_button_Process.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() ) #
	t14_Synpstry_label_Process_Status = m_tkntr.Label( t14_Synpstry_frame_9, text= "_", fg = "blue")
	t14_Synpstry_label_Process_Status.pack( side= m_tkntr.RIGHT, padx= tk_padx(), pady= tk_pady() )
	#
	# ~~~~~~~~~~ Make subtab = Skeletry
	t14_tab_7 = m_tkntr.Frame( t14_snb)
	t14_snb.add( t14_tab_7, text= "Skeletry")
	# --------- Frame 1 Title
	t14_Skltry_frame_1 = m_tkntr.Frame( t14_tab_7, bg= colour_bg_replatry())
	t14_Skltry_frame_1.pack( fill= m_tkntr.X)
	t14_Skltry_label_dscrptn = m_tkntr.Label( t14_Skltry_frame_1, text= app_show_about_Skeletry(), bg= colour_bg_replatry())
	t14_Skltry_label_dscrptn.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() )
	# --------- Frame 2 About A
	t14_Skltry_frame_2 = m_tkntr.Frame( t14_tab_7, bg= colour_bg_danger())
	t14_Skltry_frame_2.pack( fill= m_tkntr.X)
	t14_Skltry_label_about_path_A = m_tkntr.Label( t14_Skltry_frame_2, text= "Specify the location at which to delete the files", bg= colour_bg_danger())
	t14_Skltry_label_about_path_A.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() ) # 
	# --------- Frame 3 Select Path A
	t14_Skltry_frame_3 = m_tkntr.Frame( t14_tab_7, bg= colour_bg_danger())
	t14_Skltry_frame_3.pack( fill= m_tkntr.X)
	t14_Skltry_label_browse_path_A = m_tkntr.Label( t14_Skltry_frame_3, text= "Path", bg= colour_bg_danger())
	t14_Skltry_label_browse_path_A.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() ) 
	t14_Skltry_label_browse_path_A.bind('<Button-3>',  lambda x: t14_Skltry_label_path_A_on_rclick_clear() ) # because <Button-3> is the Tkinter code for a right-click
	# 
	t14_Skltry_var_path_A = m_tkntr.StringVar()
	t14_Skltry_entry_path_A = m_tkntr.Entry( t14_Skltry_frame_3, textvariable= t14_Skltry_var_path_A)
	t14_Skltry_entry_path_A.pack( side= m_tkntr.LEFT, fill= m_tkntr.X, expand= 1, padx= tk_padx(), pady= tk_pady() ) # 
	t14_Skltry_button_browse_path_A = m_tkntr.Button( t14_Skltry_frame_3, text= Strng_Browse(), command= t14_Skltry_on_button_browse_path_A )
	t14_Skltry_button_browse_path_A.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() ) #
	# --------- Frame 7 Settings
	t14_Skltry_frame_7 = m_tkntr.Frame( t14_tab_7)
	t14_Skltry_frame_7.pack( fill= m_tkntr.X)
	# Label for EnumDeleteMode ComboBox
	t14_Skltry_label_EnumDeleteMode = m_tkntr.Label( t14_Skltry_frame_7, text= "Deletion action mode:", width= 20)
	t14_Skltry_label_EnumDeleteMode.pack( side=m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) # expand=True, fill=m_tkntr.BOTH, 
	# ComboBox for EnumDeleteMode
	t14_Skltry_cb_EnumDeleteMode = PairTupleCombobox( t14_Skltry_frame_7, apltry.EnumDeleteMode_LstPairTpls(), apltry.EnumDeleteMode_Default(), state="readonly", width = 20) 
	t14_Skltry_cb_EnumDeleteMode.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() )
	# --------- Frame 8 Action button
	t14_Skltry_frame_8 = m_tkntr.Frame( t14_tab_7)
	t14_Skltry_frame_8.pack( fill= m_tkntr.X)
	# Process button
	t14_Skltry_button_Process_text = m_tkntr.StringVar()
	t14_Skltry_button_Process_text.set( Strng_RunProcess() )
	t14_Skltry_button_Process = m_tkntr.Button( t14_Skltry_frame_8, textvariable= t14_Skltry_button_Process_text, command= t14_Skltry_on_button_Process)
	t14_Skltry_button_Process.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() ) #
	t14_Skltry_label_Process_Status = m_tkntr.Label( t14_Skltry_frame_8, text= "_", fg = "blue")
	t14_Skltry_label_Process_Status.pack( side= m_tkntr.RIGHT, padx= tk_padx(), pady= tk_pady() )
	#
	# ~~~~~~~~~~ Make subtab = Migratry
	t14_tab_6 = m_tkntr.Frame( t14_snb)
	t14_snb.add( t14_tab_6, text= "Migratry")
	# --------- Frame 1 Title
	t14_Mgrtry_frame_1 = m_tkntr.Frame( t14_tab_6, bg= colour_bg_replatry())
	t14_Mgrtry_frame_1.pack( fill= m_tkntr.X)
	t14_Mgrtry_label_dscrptn = m_tkntr.Label( t14_Mgrtry_frame_1, text= app_show_about_Migratry(), bg= colour_bg_replatry())
	t14_Mgrtry_label_dscrptn.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() )
	# --------- Frame 2 About A
	t14_Mgrtry_frame_2 = m_tkntr.Frame( t14_tab_6, bg= colour_bg_danger())
	t14_Mgrtry_frame_2.pack( fill= m_tkntr.X)
	t14_Mgrtry_label_about_path_A = m_tkntr.Label( t14_Mgrtry_frame_2, text= "Specify the source location", bg= colour_bg_danger())
	t14_Mgrtry_label_about_path_A.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() ) # 
	# --------- Frame 3 Select Path A
	t14_Mgrtry_frame_3 = m_tkntr.Frame( t14_tab_6, bg= colour_bg_danger())
	t14_Mgrtry_frame_3.pack( fill= m_tkntr.X)
	t14_Mgrtry_label_browse_path_A = m_tkntr.Label( t14_Mgrtry_frame_3, text= "Path", bg= colour_bg_danger())
	t14_Mgrtry_label_browse_path_A.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() ) 
	t14_Mgrtry_label_browse_path_A.bind('<Button-3>',  lambda x: t14_Mgrtry_label_path_A_on_rclick_clear() ) # because <Button-3> is the Tkinter code for a right-click
	# 
	t14_Mgrtry_var_path_A = m_tkntr.StringVar()
	t14_Mgrtry_entry_path_A = m_tkntr.Entry( t14_Mgrtry_frame_3, textvariable= t14_Mgrtry_var_path_A)
	t14_Mgrtry_entry_path_A.pack( side= m_tkntr.LEFT, fill= m_tkntr.X, expand= 1, padx= tk_padx(), pady= tk_pady() ) # 
	t14_Mgrtry_button_browse_path_A = m_tkntr.Button( t14_Mgrtry_frame_3, text= Strng_Browse(), command= t14_Mgrtry_on_button_browse_path_A )
	t14_Mgrtry_button_browse_path_A.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() ) #
	# --------- Frame 6 About C
	t14_Mgrtry_frame_6 = m_tkntr.Frame( t14_tab_6, bg= colour_bg_modify())
	t14_Mgrtry_frame_6.pack( fill= m_tkntr.X)
	t14_Mgrtry_label_about_path_C = m_tkntr.Label( t14_Mgrtry_frame_6, text= "Specify the destination location", bg= colour_bg_modify())
	t14_Mgrtry_label_about_path_C.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() ) # 
	# right label
	t14_Mgrtry_label_remark_path_C = m_tkntr.Label( t14_Mgrtry_frame_6, text= "Must be an empty folder", fg = "blue", bg= colour_bg_modify())
	t14_Mgrtry_label_remark_path_C.pack( side= m_tkntr.RIGHT, padx= tk_padx(), pady= tk_pady() ) # 
	# --------- Frame 7 Select Path C
	t14_Mgrtry_frame_7 = m_tkntr.Frame( t14_tab_6, bg= colour_bg_modify())
	t14_Mgrtry_frame_7.pack( fill= m_tkntr.X)
	t14_Mgrtry_label_browse_path_C = m_tkntr.Label( t14_Mgrtry_frame_7, text= "Path", bg= colour_bg_modify())
	t14_Mgrtry_label_browse_path_C.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() ) 
	t14_Mgrtry_label_browse_path_C.bind('<Button-3>',  lambda x: t14_Mgrtry_label_path_C_on_rclick_clear() ) # because <Button-3> is the Tkinter code for a right-click
	# 
	t14_Mgrtry_var_path_C = m_tkntr.StringVar()
	t14_Mgrtry_entry_path_C = m_tkntr.Entry( t14_Mgrtry_frame_7, textvariable= t14_Mgrtry_var_path_C)
	t14_Mgrtry_entry_path_C.pack( side= m_tkntr.LEFT, fill= m_tkntr.X, expand= 1, padx= tk_padx(), pady= tk_pady() ) # 
	t14_Mgrtry_button_browse_path_C = m_tkntr.Button( t14_Mgrtry_frame_7, text= Strng_Browse(), command= t14_Mgrtry_on_button_browse_path_C)
	t14_Mgrtry_button_browse_path_C.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() ) #
	# --------- Frame 8 Options
	t14_Mgrtry_frame_8 = m_tkntr.Frame( t14_tab_6)
	t14_Mgrtry_frame_8.pack( fill = m_tkntr.X)
	t14_Mgrtry_label_options = m_tkntr.Label( t14_Mgrtry_frame_8, text="Options")
	t14_Mgrtry_label_options.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) # 
	# Recreate the source parent folder in the destination
	t14_Mgrtry_chkvr_RecreateSrcFldr = m_tkntr.IntVar( value= 0 )
	t14_Mgrtry_chkbx_RecreateSrcFldr = m_tkntr.Checkbutton( t14_Mgrtry_frame_8, text="Recreate source folder", variable=t14_Mgrtry_chkvr_RecreateSrcFldr)
	t14_Mgrtry_chkbx_RecreateSrcFldr.pack(side=m_tkntr.LEFT, anchor=m_tkntr.W, expand=m_tkntr.YES, padx=tk_padx(), pady=tk_pady() )
	t14_Mgrtry_chkbx_RecreateSrcFldr.configure( state= m_tkntr.DISABLED)
	# time and date stamps
	t14_Mgrtry_chkvr_KeepTuch = m_tkntr.IntVar( value= 1 )
	t14_Mgrtry_chkbx_KeepTuch = m_tkntr.Checkbutton( t14_Mgrtry_frame_8, text="Keep Touch", variable=t14_Mgrtry_chkvr_KeepTuch)
	t14_Mgrtry_chkbx_KeepTuch.pack(side=m_tkntr.LEFT, anchor=m_tkntr.W, expand=m_tkntr.YES, padx=tk_padx(), pady=tk_pady() )
	t14_Mgrtry_chkbx_KeepTuch.configure( state= m_tkntr.DISABLED)
	# flags, read-only, hidden, system
	t14_Mgrtry_chkvr_KeepAttr = m_tkntr.IntVar( value= 1 )
	t14_Mgrtry_chkbx_KeepAttr = m_tkntr.Checkbutton( t14_Mgrtry_frame_8, text="Keep Attributes", variable=t14_Mgrtry_chkvr_KeepAttr)
	t14_Mgrtry_chkbx_KeepAttr.pack(side=m_tkntr.LEFT, anchor=m_tkntr.W, expand=m_tkntr.YES, padx=tk_padx(), pady=tk_pady() )
	t14_Mgrtry_chkbx_KeepAttr.configure( state= m_tkntr.DISABLED)
	# linux file-system permissions
	t14_Mgrtry_chkvr_KeepPrmt = m_tkntr.IntVar( value= 1 )
	t14_Mgrtry_chkbx_KeepPrmt = m_tkntr.Checkbutton( t14_Mgrtry_frame_8, text="Keep Permissions", variable=t14_Mgrtry_chkvr_KeepPrmt)
	t14_Mgrtry_chkbx_KeepPrmt.pack(side=m_tkntr.LEFT, anchor=m_tkntr.W, expand=m_tkntr.YES, padx=tk_padx(), pady=tk_pady() )
	t14_Mgrtry_chkbx_KeepPrmt.configure( state= m_tkntr.DISABLED)
	# OS specific and ACLs
	t14_Mgrtry_chkvr_KeepMeta = m_tkntr.IntVar( value= 0 )
	t14_Mgrtry_chkbx_KeepMeta = m_tkntr.Checkbutton( t14_Mgrtry_frame_8, text="Keep Metadata", variable=t14_Mgrtry_chkvr_KeepMeta)
	t14_Mgrtry_chkbx_KeepMeta.pack(side=m_tkntr.LEFT, anchor=m_tkntr.W, expand=m_tkntr.YES, padx=tk_padx(), pady=tk_pady() )
	t14_Mgrtry_chkbx_KeepMeta.configure( state= m_tkntr.DISABLED)
	# --------- Frame 9 Action button
	t14_Mgrtry_frame_9 = m_tkntr.Frame( t14_tab_6)
	t14_Mgrtry_frame_9.pack( fill= m_tkntr.X)
	t14_Mgrtry_button_Process_text = m_tkntr.StringVar()
	t14_Mgrtry_button_Process_text.set( Strng_RunProcess() )
	t14_Mgrtry_button_Process = m_tkntr.Button( t14_Mgrtry_frame_9, textvariable= t14_Mgrtry_button_Process_text, command= t14_Mgrtry_on_button_Process)
	t14_Mgrtry_button_Process.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() ) #
	t14_Mgrtry_label_Process_Status = m_tkntr.Label( t14_Mgrtry_frame_9, text= "_", fg = "blue")
	t14_Mgrtry_label_Process_Status.pack( side= m_tkntr.RIGHT, padx= tk_padx(), pady= tk_pady() )
	#
	# ~~~~~~~~~~ Make subtab = Movetry
	t14_tab_8 = m_tkntr.Frame( t14_snb)
	t14_snb.add( t14_tab_8, text= "Movetry")
	# --------- Frame 1 Title
	t14_Mvtry_frame_1 = m_tkntr.Frame( t14_tab_8, bg= colour_bg_replatry())
	t14_Mvtry_frame_1.pack( fill= m_tkntr.X)
	t14_Mvtry_label_dscrptn = m_tkntr.Label( t14_Mvtry_frame_1, text= app_show_about_Movetry(), bg= colour_bg_replatry())
	t14_Mvtry_label_dscrptn.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() )
	# --------- Frame 2 About A
	t14_Mvtry_frame_2 = m_tkntr.Frame( t14_tab_8, bg= colour_bg_danger())
	t14_Mvtry_frame_2.pack( fill= m_tkntr.X)
	t14_Mvtry_label_about_path_A = m_tkntr.Label( t14_Mvtry_frame_2, text= "Specify the source location", bg= colour_bg_danger())
	t14_Mvtry_label_about_path_A.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() ) # 
	# --------- Frame 3 Select Path A
	t14_Mvtry_frame_3 = m_tkntr.Frame( t14_tab_8, bg= colour_bg_danger())
	t14_Mvtry_frame_3.pack( fill= m_tkntr.X)
	t14_Mvtry_label_browse_path_A = m_tkntr.Label( t14_Mvtry_frame_3, text= "Path", bg= colour_bg_danger())
	t14_Mvtry_label_browse_path_A.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() ) 
	t14_Mvtry_label_browse_path_A.bind('<Button-3>',  lambda x: t14_Mvtry_label_path_A_on_rclick_clear() ) # because <Button-3> is the Tkinter code for a right-click
	# 
	t14_Mvtry_var_path_A = m_tkntr.StringVar()
	t14_Mvtry_entry_path_A = m_tkntr.Entry( t14_Mvtry_frame_3, textvariable= t14_Mvtry_var_path_A)
	t14_Mvtry_entry_path_A.pack( side= m_tkntr.LEFT, fill= m_tkntr.X, expand= 1, padx= tk_padx(), pady= tk_pady() ) # 
	t14_Mvtry_button_browse_path_A = m_tkntr.Button( t14_Mvtry_frame_3, text= Strng_Browse(), command= t14_Mvtry_on_button_browse_path_A )
	t14_Mvtry_button_browse_path_A.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() ) #
	#
	t14_Mvtry_button_add_path_A = m_tkntr.Button( t14_Mvtry_frame_3, text=Strng_AddToList(), command=t14_Mvtry_on_button_add_folder_paths_A)
	t14_Mvtry_button_add_path_A.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	#
	t14_Mvtry_button_add_subfolders = m_tkntr.Button( t14_Mvtry_frame_3, text=Strng_AddSubs(), command=t14_Mvtry_on_button_add_subfolders_paths_A)
	t14_Mvtry_button_add_subfolders.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	#  layout
	# --------- Frame 2 Folder list listbox
	t14_Mvtry_frame_4 = m_tkntr.Frame( t14_tab_8, bg=colour_bg_danger())
	t14_Mvtry_frame_4.pack( fill = m_tkntr.BOTH, expand=True)
	t14_Mvtry_lb_paths_A = m_tkntr.Listbox( t14_Mvtry_frame_4, height=2 )
	t14_Mvtry_lb_paths_A.pack( side = m_tkntr.LEFT, fill=m_tkntr.BOTH, expand=True, padx=tk_padx(), pady=tk_pady() )
	t14_Mvtry_lb_paths_A.bind('<Double-Button>',  lambda x: t14_Mvtry_on_dblclck_selected_path_A() )
	#if __debug__ : # declare this to be a Settings object
	#	Set_ESD_EMSA( apltry.EnumSettingDef.DfltryLstPaths, EnumMetaSettingAspect.WidgetObject, t14_Mvtry_lb_paths_A)
	# --------- Frame 3 Folder list listbox editing buttons
	t14_Mvtry_frame_5 = m_tkntr.Frame( t14_tab_8, bg=colour_bg_danger())
	t14_Mvtry_frame_5.pack( fill = m_tkntr.X)
	#
	t14_Mvtry_label_paths_A_selection = m_tkntr.Label( t14_Mvtry_frame_5, text="Select a path:", bg=colour_bg_danger())
	t14_Mvtry_label_paths_A_selection.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	#
	t14_Mvtry_button_remove_selected_path_A = m_tkntr.Button( t14_Mvtry_frame_5, text="Remove", command=t14_Mvtry_on_button_remove_selected_path_A)
	t14_Mvtry_button_remove_selected_path_A.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	#
	t14_Mvtry_label_paths_A_all = m_tkntr.Label( t14_Mvtry_frame_5, text="All paths:", bg=colour_bg_danger())
	t14_Mvtry_label_paths_A_all.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	#
	t14_Mvtry_button_remove_all_paths_A = m_tkntr.Button( t14_Mvtry_frame_5, text="Clear", command=t14_Mvtry_on_button_remove_all_paths_A)
	t14_Mvtry_button_remove_all_paths_A.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	#
	t14_Mvtry_label_paths_A_all = m_tkntr.Label( t14_Mvtry_frame_5, text="Clashes:", bg=colour_bg_danger())
	t14_Mvtry_label_paths_A_all.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	#
	t14_Mvtry_button_check_list_clashes_paths_A = m_tkntr.Button( t14_Mvtry_frame_5, text="Check clashes", command=t14_Mvtry_on_button_check_list_clashes_paths_A)
	t14_Mvtry_button_check_list_clashes_paths_A.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	#
	t14_Mvtry_chkvr_autoclashcheck_paths_A = m_tkntr.IntVar()
	t14_Mvtry_chkvr_autoclashcheck_paths_A.set(1)
	t14_Mvtry_chkbx_autoclashcheck_paths_A = m_tkntr.Checkbutton(t14_Mvtry_frame_5, text="Check as added", \
		variable=t14_Mvtry_chkvr_autoclashcheck_paths_A, command = t14_Mvtry_on_chkbx_autoclashcheck_paths_A)
	t14_Mvtry_chkbx_autoclashcheck_paths_A.pack(side=m_tkntr.LEFT, anchor=m_tkntr.W, padx=tk_padx(), pady=tk_pady() )
	#
	t14_Mvtry_chkvr_clashovercase_paths_A = m_tkntr.IntVar()
	t14_Mvtry_chkvr_clashovercase_paths_A.set(0) # default is for Linux et al
	t14_Mvtry_chkbx_clashovercase_paths_A = m_tkntr.Checkbutton(t14_Mvtry_frame_5, text="Assume Case insensitive", \
		variable=t14_Mvtry_chkvr_clashovercase_paths_A, command = t14_Mvtry_on_chkbx_clashovercase_paths_A)
	t14_Mvtry_chkbx_clashovercase_paths_A.pack(side=m_tkntr.LEFT, anchor=m_tkntr.W, padx=tk_padx(), pady=tk_pady() ) # , expand=m_tkntr.YES
	#
	t14_Mvtry_label_paths_A_alert = m_tkntr.Label( t14_Mvtry_frame_5, text="_", fg = "blue", bg=colour_bg_danger()) # , font=('italic')
	t14_Mvtry_label_paths_A_alert.pack( side = m_tkntr.RIGHT, padx=tk_padx(), pady=tk_pady() )
	# --------- Frame 6 About C
	t14_Mvtry_frame_6 = m_tkntr.Frame( t14_tab_8, bg= colour_bg_modify())
	t14_Mvtry_frame_6.pack( fill= m_tkntr.X)
	t14_Mvtry_label_about_path_C = m_tkntr.Label( t14_Mvtry_frame_6, text= "Specify the destination location", bg= colour_bg_modify())
	t14_Mvtry_label_about_path_C.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() ) # 
	# --------- Frame 7 Select Path C
	t14_Mvtry_frame_7 = m_tkntr.Frame( t14_tab_8, bg= colour_bg_modify())
	t14_Mvtry_frame_7.pack( fill= m_tkntr.X)
	t14_Mvtry_label_browse_path_C = m_tkntr.Label( t14_Mvtry_frame_7, text= "Path", bg= colour_bg_modify())
	t14_Mvtry_label_browse_path_C.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() ) 
	t14_Mvtry_label_browse_path_C.bind('<Button-3>',  lambda x: t14_Mvtry_label_path_C_on_rclick_clear() ) # because <Button-3> is the Tkinter code for a right-click
	# 
	t14_Mvtry_var_path_C = m_tkntr.StringVar()
	t14_Mvtry_entry_path_C = m_tkntr.Entry( t14_Mvtry_frame_7, textvariable= t14_Mvtry_var_path_C)
	t14_Mvtry_entry_path_C.pack( side= m_tkntr.LEFT, fill= m_tkntr.X, expand=m_tkntr.YES, padx= tk_padx(), pady= tk_pady() ) 
	# 
	t14_Mvtry_button_browse_path_C = m_tkntr.Button( t14_Mvtry_frame_7, text= Strng_Browse(), command= t14_Mvtry_on_button_browse_path_C)
	t14_Mvtry_button_browse_path_C.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() ) 
	#
	t14_Mvtry_button_check_path_C = m_tkntr.Button( t14_Mvtry_frame_7, text="Check", command= t14_Mvtry_on_button_check_path_C)
	t14_Mvtry_button_check_path_C.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() ) 
	#
	t14_Mvtry_chkvr_DestAutoCheck = m_tkntr.IntVar( value= 1 )
	t14_Mvtry_chkbx_DestAutoCheck = m_tkntr.Checkbutton( t14_Mvtry_frame_7, text="Auto check", variable=t14_Mvtry_chkvr_DestAutoCheck)
	t14_Mvtry_chkbx_DestAutoCheck.pack(side=m_tkntr.LEFT, anchor=m_tkntr.W, padx=tk_padx(), pady=tk_pady() )
	# --------- Frame 8a Deletion Options
	t14_Mvtry_frame_8a = m_tkntr.Frame( t14_tab_8)
	t14_Mvtry_frame_8a.pack( fill = m_tkntr.X)
	t14_Mvtry_label_options = m_tkntr.Label( t14_Mvtry_frame_8a, text="Deletion After")
	t14_Mvtry_label_options.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) # 
	# Delete the destination after unsuccessful copy
	t14_Mvtry_chkvr_DeleteBadCopy = m_tkntr.IntVar( value= 1 )
	t14_Mvtry_chkbx_DeleteBadCopy = m_tkntr.Checkbutton( t14_Mvtry_frame_8a, text="Delete destination on copy failure", variable=t14_Mvtry_chkvr_DeleteBadCopy)
	t14_Mvtry_chkbx_DeleteBadCopy.pack(side=m_tkntr.LEFT, anchor=m_tkntr.W, expand=m_tkntr.YES, padx=tk_padx(), pady=tk_pady() )
	#t14_Mvtry_chkbx_DeleteBadCopy.configure( state= m_tkntr.DISABLED)
	# Delete the source parent folder after successful copy
	t14_Mvtry_chkvr_DeleteSourceAfter = m_tkntr.IntVar( value= 0 )
	t14_Mvtry_chkbx_DeleteSourceAfter = m_tkntr.Checkbutton( t14_Mvtry_frame_8a, text="Delete source folder", variable=t14_Mvtry_chkvr_DeleteSourceAfter)
	t14_Mvtry_chkbx_DeleteSourceAfter.pack(side=m_tkntr.LEFT, anchor=m_tkntr.W, expand=m_tkntr.YES, padx=tk_padx(), pady=tk_pady() )
	t14_Mvtry_chkbx_DeleteSourceAfter.configure( state= m_tkntr.NORMAL)
	t14_Mvtry_chkbx_DeleteSourceAfter.bind('<ButtonRelease>',  lambda x: t14_Mvtry_chkbx_DeleteSourceAfter_on_release() )

	# --------- Frame 8b Copy Options
	t14_Mvtry_frame_8b = m_tkntr.Frame( t14_tab_8)
	t14_Mvtry_frame_8b.pack( fill = m_tkntr.X)
	t14_Mvtry_label_options = m_tkntr.Label( t14_Mvtry_frame_8b, text="Copying Options")
	t14_Mvtry_label_options.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) # 
	# Recreate the source parent folder in the destination
	t14_Mvtry_chkvr_RecreateSrcFldr = m_tkntr.IntVar( value= 0 )
	t14_Mvtry_chkbx_RecreateSrcFldr = m_tkntr.Checkbutton( t14_Mvtry_frame_8b, text="Recreate source folder", variable=t14_Mvtry_chkvr_RecreateSrcFldr)
	t14_Mvtry_chkbx_RecreateSrcFldr.pack(side=m_tkntr.LEFT, anchor=m_tkntr.W, expand=m_tkntr.YES, padx=tk_padx(), pady=tk_pady() )
	t14_Mvtry_chkbx_RecreateSrcFldr.bind('<ButtonRelease>',  lambda x: t14_Mvtry_chkbx_RecreateSrcFldr_on_release() )
	# t14_Mvtry_chkbx_RecreateSrcFldr.configure( state= m_tkntr.DISABLED)
	# time and date stamps
	t14_Mvtry_chkvr_KeepTuch = m_tkntr.IntVar( value= 1 )
	t14_Mvtry_chkbx_KeepTuch = m_tkntr.Checkbutton( t14_Mvtry_frame_8b, text="Keep Touch", variable=t14_Mvtry_chkvr_KeepTuch)
	t14_Mvtry_chkbx_KeepTuch.pack(side=m_tkntr.LEFT, anchor=m_tkntr.W, expand=m_tkntr.YES, padx=tk_padx(), pady=tk_pady() )
	t14_Mvtry_chkbx_KeepTuch.configure( state= m_tkntr.DISABLED)
	# flags, read-only, hidden, system
	t14_Mvtry_chkvr_KeepAttr = m_tkntr.IntVar( value= 0 )
	t14_Mvtry_chkbx_KeepAttr = m_tkntr.Checkbutton( t14_Mvtry_frame_8b, text="Keep Attributes", variable=t14_Mvtry_chkvr_KeepAttr)
	t14_Mvtry_chkbx_KeepAttr.pack(side=m_tkntr.LEFT, anchor=m_tkntr.W, expand=m_tkntr.YES, padx=tk_padx(), pady=tk_pady() )
	t14_Mvtry_chkbx_KeepAttr.configure( state= m_tkntr.DISABLED)
	# linux file-system permissions
	t14_Mvtry_chkvr_KeepPrmt = m_tkntr.IntVar( value= 0 )
	t14_Mvtry_chkbx_KeepPrmt = m_tkntr.Checkbutton( t14_Mvtry_frame_8b, text="Keep Permissions", variable=t14_Mvtry_chkvr_KeepPrmt)
	t14_Mvtry_chkbx_KeepPrmt.pack(side=m_tkntr.LEFT, anchor=m_tkntr.W, expand=m_tkntr.YES, padx=tk_padx(), pady=tk_pady() )
	t14_Mvtry_chkbx_KeepPrmt.configure( state= m_tkntr.DISABLED)
	# OS specific and ACLs
	t14_Mvtry_chkvr_KeepMeta = m_tkntr.IntVar( value= 0 )
	t14_Mvtry_chkbx_KeepMeta = m_tkntr.Checkbutton( t14_Mvtry_frame_8b, text="Keep Metadata", variable=t14_Mvtry_chkvr_KeepMeta)
	t14_Mvtry_chkbx_KeepMeta.pack(side=m_tkntr.LEFT, anchor=m_tkntr.W, expand=m_tkntr.YES, padx=tk_padx(), pady=tk_pady() )
	t14_Mvtry_chkbx_KeepMeta.configure( state= m_tkntr.DISABLED)
	# --------- Frame 9 Action button
	t14_Mvtry_frame_9 = m_tkntr.Frame( t14_tab_8)
	t14_Mvtry_frame_9.pack( fill= m_tkntr.X)
	t14_Mvtry_button_Process_text = m_tkntr.StringVar()
	t14_Mvtry_button_Process_text.set( Strng_RunProcess() )
	t14_Mvtry_button_Process = m_tkntr.Button( t14_Mvtry_frame_9, textvariable= t14_Mvtry_button_Process_text, command= t14_Mvtry_on_button_Process)
	t14_Mvtry_button_Process.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() ) #
	#
	t14_Mvtry_label_modehead = m_tkntr.Label( t14_Mvtry_frame_9, text="Mode:")
	t14_Mvtry_label_modehead.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) # 
	#
	t14_Mvtry_label_modestate = m_tkntr.Label( t14_Mvtry_frame_9, text="Single source folder", fg = "green")
	t14_Mvtry_label_modestate.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) # 
	#
	t14_Mvtry_label_modealert = m_tkntr.Label( t14_Mvtry_frame_9, text="", fg = "red")
	t14_Mvtry_label_modealert.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) # 
	#
	t14_Mvtry_label_Process_Status = m_tkntr.Label( t14_Mvtry_frame_9, text= "Note: currently being tested.", fg = "blue")
	t14_Mvtry_label_Process_Status.pack( side= m_tkntr.RIGHT, padx= tk_padx(), pady= tk_pady() )
	#
	# ~~~~~~~~~~ Make subtab = Shiftatry
	t14_tab_9 = m_tkntr.Frame( t14_snb)
	t14_snb.add( t14_tab_9, text= "Shiftatry")
	# --------- Frame 1 Title
	t14_Shfttry_frame_1 = m_tkntr.Frame( t14_tab_9, bg= colour_bg_replatry())
	t14_Shfttry_frame_1.pack( fill= m_tkntr.X)
	t14_Shfttry_label_dscrptn = m_tkntr.Label( t14_Shfttry_frame_1, text= app_show_about_Shiftatry(), bg= colour_bg_replatry())
	t14_Shfttry_label_dscrptn.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() )
	# --------- Frame 2 About A
	t14_Shfttry_frame_2 = m_tkntr.Frame( t14_tab_9, bg= colour_bg_danger())
	t14_Shfttry_frame_2.pack( fill= m_tkntr.X)
	t14_Shfttry_label_about_path_A = m_tkntr.Label( t14_Shfttry_frame_2, text= "Specify the source location", bg= colour_bg_danger())
	t14_Shfttry_label_about_path_A.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() ) # 
	# --------- Frame 3 Select Path A
	t14_Shfttry_frame_3 = m_tkntr.Frame( t14_tab_9, bg= colour_bg_danger())
	t14_Shfttry_frame_3.pack( fill= m_tkntr.X)
	t14_Shfttry_label_browse_path_A = m_tkntr.Label( t14_Shfttry_frame_3, text= "Path", bg= colour_bg_danger())
	t14_Shfttry_label_browse_path_A.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() ) 
	t14_Shfttry_label_browse_path_A.bind('<Button-3>',  lambda x: t14_Shfttry_label_path_A_on_rclick_clear() ) # because <Button-3> is the Tkinter code for a right-click
	# 
	t14_Shfttry_var_path_A = m_tkntr.StringVar()
	t14_Shfttry_entry_path_A = m_tkntr.Entry( t14_Shfttry_frame_3, textvariable= t14_Shfttry_var_path_A)
	t14_Shfttry_entry_path_A.pack( side= m_tkntr.LEFT, fill= m_tkntr.X, expand= 1, padx= tk_padx(), pady= tk_pady() ) # 
	t14_Shfttry_button_browse_path_A = m_tkntr.Button( t14_Shfttry_frame_3, text= Strng_Browse(), command= t14_Shfttry_on_button_browse_path_A )
	t14_Shfttry_button_browse_path_A.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() ) #
	# --------- Frame 6 About C
	t14_Shfttry_frame_6 = m_tkntr.Frame( t14_tab_9, bg= colour_bg_modify())
	t14_Shfttry_frame_6.pack( fill= m_tkntr.X)
	t14_Shfttry_label_about_path_C = m_tkntr.Label( t14_Shfttry_frame_6, text= "Specify the destination location", bg= colour_bg_modify())
	t14_Shfttry_label_about_path_C.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() ) # 
	# --------- Frame 7 Select Path C
	t14_Shfttry_frame_7 = m_tkntr.Frame( t14_tab_9, bg= colour_bg_modify())
	t14_Shfttry_frame_7.pack( fill= m_tkntr.X)
	t14_Shfttry_label_browse_path_C = m_tkntr.Label( t14_Shfttry_frame_7, text= "Path", bg= colour_bg_modify())
	t14_Shfttry_label_browse_path_C.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() ) 
	t14_Shfttry_label_browse_path_C.bind('<Button-3>',  lambda x: t14_Shfttry_label_path_C_on_rclick_clear() ) # because <Button-3> is the Tkinter code for a right-click
	# 
	t14_Shfttry_var_path_C = m_tkntr.StringVar()
	t14_Shfttry_entry_path_C = m_tkntr.Entry( t14_Shfttry_frame_7, textvariable= t14_Shfttry_var_path_C)
	t14_Shfttry_entry_path_C.pack( side= m_tkntr.LEFT, fill= m_tkntr.X, expand= 1, padx= tk_padx(), pady= tk_pady() ) # 
	t14_Shfttry_button_browse_path_C = m_tkntr.Button( t14_Shfttry_frame_7, text= Strng_Browse(), command= t14_Shfttry_on_button_browse_path_C)
	t14_Shfttry_button_browse_path_C.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() ) 
	#
	# --------- Frame 8 Action button
	t14_Shfttry_frame_8 = m_tkntr.Frame( t14_tab_9)
	t14_Shfttry_frame_8.pack( fill= m_tkntr.X)
	t14_Shfttry_button_Process_text = m_tkntr.StringVar()
	t14_Shfttry_button_Process_text.set( Strng_RunProcess() )
	t14_Shfttry_button_Process = m_tkntr.Button( t14_Shfttry_frame_8, textvariable= t14_Shfttry_button_Process_text, command= t14_Shfttry_on_button_Process)
	t14_Shfttry_button_Process.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() ) #
	t14_Shfttry_label_Process_Status = m_tkntr.Label( t14_Shfttry_frame_8, text= "Note: not yet functional.", fg = "blue")
	t14_Shfttry_label_Process_Status.pack( side= m_tkntr.RIGHT, padx= tk_padx(), pady= tk_pady() )
	#
	# ~~~~~~~~~~ Make subtab = Shiftafile
	t14_tab_10 = m_tkntr.Frame( t14_snb)
	t14_snb.add( t14_tab_10, text= "Shiftafile")
	# --------- Frame 1 Title
	t14_Shftfl_frame_1 = m_tkntr.Frame( t14_tab_10, bg= colour_bg_replatry())
	t14_Shftfl_frame_1.pack( fill= m_tkntr.X)
	t14_Shftfl_label_dscrptn = m_tkntr.Label( t14_Shftfl_frame_1, text= app_show_about_Shiftafile(), bg= colour_bg_replatry())
	t14_Shftfl_label_dscrptn.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() )
	# --------- Frame 2 About A
	t14_Shftfl_frame_2 = m_tkntr.Frame( t14_tab_10, bg= colour_bg_danger())
	t14_Shftfl_frame_2.pack( fill= m_tkntr.X)
	t14_Shftfl_label_about_file_A = m_tkntr.Label( t14_Shftfl_frame_2, text= "Specify the source file", bg= colour_bg_danger())
	t14_Shftfl_label_about_file_A.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() ) # 
	# --------- Frame 3 Select Path A
	t14_Shftfl_frame_3 = m_tkntr.Frame( t14_tab_10, bg= colour_bg_danger())
	t14_Shftfl_frame_3.pack( fill= m_tkntr.X)
	t14_Shftfl_label_browse_file_A = m_tkntr.Label( t14_Shftfl_frame_3, text= "File", bg= colour_bg_danger())
	t14_Shftfl_label_browse_file_A.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() ) 
	t14_Shftfl_label_browse_file_A.bind('<Button-3>',  lambda x: t14_Shftfl_label_file_A_on_rclick_clear() ) # because <Button-3> is the Tkinter code for a right-click
	# 
	t14_Shftfl_var_file_A = m_tkntr.StringVar()
	t14_Shftfl_entry_file_A = m_tkntr.Entry( t14_Shftfl_frame_3, textvariable= t14_Shftfl_var_file_A)
	t14_Shftfl_entry_file_A.pack( side= m_tkntr.LEFT, fill= m_tkntr.X, expand= 1, padx= tk_padx(), pady= tk_pady() ) # 
	t14_Shftfl_button_browse_file_A = m_tkntr.Button( t14_Shftfl_frame_3, text= Strng_Browse(), command= t14_Shftfl_on_button_browse_file_A )
	t14_Shftfl_button_browse_file_A.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() ) #
	# --------- Frame 6 About C
	t14_Shftfl_frame_6 = m_tkntr.Frame( t14_tab_10, bg= colour_bg_modify())
	t14_Shftfl_frame_6.pack( fill= m_tkntr.X)
	t14_Shftfl_label_about_path_C = m_tkntr.Label( t14_Shftfl_frame_6, text= "Specify the destination location for the file", bg= colour_bg_modify())
	t14_Shftfl_label_about_path_C.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() ) # 
	# --------- Frame 7 Select Path C
	t14_Shftfl_frame_7 = m_tkntr.Frame( t14_tab_10, bg= colour_bg_modify())
	t14_Shftfl_frame_7.pack( fill= m_tkntr.X)
	t14_Shftfl_label_browse_path_C = m_tkntr.Label( t14_Shftfl_frame_7, text= "Path", bg= colour_bg_modify())
	t14_Shftfl_label_browse_path_C.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() ) 
	t14_Shftfl_label_browse_path_C.bind('<Button-3>',  lambda x: t14_Shftfl_label_path_C_on_rclick_clear() ) # because <Button-3> is the Tkinter code for a right-click
	# 
	t14_Shftfl_var_path_C = m_tkntr.StringVar()
	t14_Shftfl_entry_path_C = m_tkntr.Entry( t14_Shftfl_frame_7, textvariable= t14_Shftfl_var_path_C)
	t14_Shftfl_entry_path_C.pack( side= m_tkntr.LEFT, fill= m_tkntr.X, expand= 1, padx= tk_padx(), pady= tk_pady() ) # 
	t14_Shftfl_button_browse_path_C = m_tkntr.Button( t14_Shftfl_frame_7, text= Strng_Browse(), command= t14_Shftfl_on_button_browse_path_C)
	t14_Shftfl_button_browse_path_C.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() ) #
	# --------- Frame 8 Action button
	t14_Shftfl_frame_8 = m_tkntr.Frame( t14_tab_10)
	t14_Shftfl_frame_8.pack( fill= m_tkntr.X)
	t14_Shftfl_button_Process_text = m_tkntr.StringVar()
	t14_Shftfl_button_Process_text.set( Strng_RunProcess() )
	t14_Shftfl_button_Process = m_tkntr.Button( t14_Shftfl_frame_8, textvariable= t14_Shftfl_button_Process_text, command= t14_Shftfl_on_button_Process)
	t14_Shftfl_button_Process.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() ) #
	t14_Shftfl_label_Process_Status = m_tkntr.Label( t14_Shftfl_frame_8, text= "Note: not yet functional.", fg = "blue")
	t14_Shftfl_label_Process_Status.pack( side= m_tkntr.RIGHT, padx= tk_padx(), pady= tk_pady() )
	#
	# ========== Make a tab for: Organitry
	t17_tab = m_tkntr.Frame(nb)
	nb.add( t17_tab, text="Organitry")
	# --------- Frame T temporary advice 
	t17_frameT = m_tkntr.Frame( t17_tab, bg=colour_bg_organitry())
	t17_frameT.pack( fill = m_tkntr.X)
	t17_label_temp = m_tkntr.Label( t17_frameT, bg=colour_bg_organitry(), \
		text= "Various tools for re-organising files and folders. NOTE: Only some parts are currently functional")
	t17_label_temp.pack( side=m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() )
	# Make a sub-notebook i.e. tabbed interface
	t17_snb = m_tkntr_ttk.Notebook( t17_tab)
	t17_snb.pack( expand=1, fill=m_tkntr.BOTH)
	# ~~~~~~~~~~ Make subtab = About
	t17_tab_0 = m_tkntr.Frame( t17_snb)
	t17_snb.add( t17_tab_0, text= "Overview")
	# --------- Frame 1 Title
	t17_About_frame_1 = m_tkntr.Frame( t17_tab_0, bg= colour_bg_organitry())
	t17_About_frame_1.pack( fill= m_tkntr.X)
	t17_About_label_dscrptn = m_tkntr.Label( t17_About_frame_1, text= "About the Organitry features", bg= colour_bg_organitry())
	t17_About_label_dscrptn.pack( side= m_tkntr.LEFT, padx= tk_padx(), pady= tk_pady() )
	# --------- Frame 2 About A
	t17_About_frame_2 = m_tkntr.Frame( t17_tab_0, bg= colour_bg_organitry())
	t17_About_frame_2.pack( fill= m_tkntr.BOTH, expand=True )
	#
	t17_About_scrolltext = m_tkntr_tkst.ScrolledText( t17_About_frame_2, bg= colour_bg_organitry(), wrap= "word", relief= "solid", borderwidth= 0 ) # , bordercolor= colour_bg_organitry()
	t17_About_scrolltext.pack( anchor= m_tkntr.N, side=m_tkntr.LEFT, fill=m_tkntr.BOTH, expand=True )
	t17_About_scrolltext.insert(m_tkntr.INSERT, app_show_about_organitry() )
	# ~~~~~~~~~~ Make 1st subtab = Canprunatry
	t17_tab_1 = m_tkntr.Frame( t17_snb)
	t17_snb.add( t17_tab_1, text= "Canprunatry")
	# --------- Frame 0 Folder list heading
	t17_st01_frame0 = m_tkntr.Frame( t17_tab_1, bg=colour_bg_neutral())
	t17_st01_frame0.pack( fill = m_tkntr.X)
	#
	t17_st01_label_tab_heading = m_tkntr.Label( t17_st01_frame0, text="Search list of paths to find replicated folders or files then act on them", bg=colour_bg_neutral())
	t17_st01_label_tab_heading.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	# --------- Frame 1 Folder list entry first line
	t17_st01_frame1 = m_tkntr.Frame( t17_tab_1, bg=colour_bg_danger())
	t17_st01_frame1.pack( fill = m_tkntr.X)
	#
	t17_st01_label_heading_paths = m_tkntr.Label( t17_st01_frame1, text="PATHS", bg=colour_bg_danger())
	t17_st01_label_heading_paths.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	#
	t17_st01_label_browse_paths = m_tkntr.Label( t17_st01_frame1, text="Enter path", bg=colour_bg_danger())
	t17_st01_label_browse_paths.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	t17_st01_label_browse_paths.bind('<Button-3>',  lambda x: t17_label_paths_on_rclick_clear() ) # because <Button-3> is the Tkinter code for a right-click
	# 
	t17_st01_var_path = m_tkntr.StringVar()
	t17_st01_entry_path = m_tkntr.Entry( t17_st01_frame1, textvariable=t17_st01_var_path)
	t17_st01_entry_path.pack( side = m_tkntr.LEFT, fill = m_tkntr.X, expand=1, padx=tk_padx(), pady=tk_pady() ) 
	# 
	t17_st01_button_browse_paths = m_tkntr.Button( t17_st01_frame1, text=Strng_Browse(), command=t17_st01_on_button_browse_paths)
	t17_st01_button_browse_paths.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	#
	t17_st01_button_add_path = m_tkntr.Button( t17_st01_frame1, text=Strng_AddToList(), command=t17_st01_on_button_add_folder)
	t17_st01_button_add_path.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	#
	t17_st01_button_add_subfolders = m_tkntr.Button( t17_st01_frame1, text=Strng_AddSubs(), command=t17_st01_on_button_add_subfolders)
	t17_st01_button_add_subfolders.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	#  layout
	# --------- Frame 2 Folder list listbox
	t17_st01_frame2 = m_tkntr.Frame( t17_tab_1, bg=colour_bg_danger())
	t17_st01_frame2.pack( fill = m_tkntr.BOTH, expand=True)
	t17_st01_lb_paths = m_tkntr.Listbox( t17_st01_frame2, height=2 )
	t17_st01_lb_paths.pack( side = m_tkntr.LEFT, fill=m_tkntr.BOTH, expand=True, padx=tk_padx(), pady=tk_pady() )
	t17_st01_lb_paths.bind('<Double-Button>',  lambda x: t17_st01_on_dblclck_selected_path() )
	# --------- Frame 3 Folder list listbox editing buttons
	t17_st01_frame3 = m_tkntr.Frame( t17_tab_1, bg=colour_bg_danger())
	t17_st01_frame3.pack( fill = m_tkntr.X)
	#
	t17_st01_label_paths_selection = m_tkntr.Label( t17_st01_frame3, text="Selected path:", bg=colour_bg_danger())
	t17_st01_label_paths_selection.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	#
	t17_st01_button_remove_selected_path = m_tkntr.Button( t17_st01_frame3, text="Remove", command=t17_st01_on_button_remove_selected_path)
	t17_st01_button_remove_selected_path.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	#
	t17_st01_label_paths_all = m_tkntr.Label( t17_st01_frame3, text="All paths:", bg=colour_bg_danger())
	t17_st01_label_paths_all.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	#
	t17_st01_button_remove_all_paths = m_tkntr.Button( t17_st01_frame3, text="Clear", command=t17_st01_on_button_remove_all_paths)
	t17_st01_button_remove_all_paths.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	#
	t17_st01_button_check_list_clashes_paths = m_tkntr.Button( t17_st01_frame3, text="Check clashes", command=t17_st01_on_button_check_list_clashes_paths)
	t17_st01_button_check_list_clashes_paths.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	#
	t17_st01_chkvr_autoclashcheck_paths = m_tkntr.IntVar()
	t17_st01_chkvr_autoclashcheck_paths.set(1)
	t17_st01_chkbx_autoclashcheck_paths = m_tkntr.Checkbutton(t17_st01_frame3, text="Check as added", \
		variable=t17_st01_chkvr_autoclashcheck_paths, command = t17_st01_on_chkbx_autoclashcheck_paths)
	t17_st01_chkbx_autoclashcheck_paths.pack(side=m_tkntr.LEFT, anchor=m_tkntr.W, expand=m_tkntr.YES, padx=tk_padx(), pady=tk_pady() )
	#
	t17_st01_label_paths_alert = m_tkntr.Label( t17_st01_frame3, text="_", fg = "blue", bg=colour_bg_danger()) # , font=('italic')
	t17_st01_label_paths_alert.pack( side = m_tkntr.RIGHT, padx=tk_padx(), pady=tk_pady() )
	# --------- Frame 5 Action buttons
	t17_st01_frame5 = m_tkntr.Frame( t17_tab_1)
	t17_st01_frame5.pack( fill = m_tkntr.X)
	# 
	t17_st01_label_limit = m_tkntr.Label( t17_st01_frame5, text="Limit report count:", bg=colour_bg_neutral()) # , font=('italic')
	t17_st01_label_limit.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() )
	# use a Spinbox for the reporting limit
	t17_st01_var_limit= m_tkntr.StringVar()
	t17_st01_var_limit.set("20")
	t17_st01_spbx_limit = m_tkntr.Spinbox( t17_st01_frame5, from_= 0, to= 100, width= 10, relief="sunken", repeatdelay=500, repeatinterval=100, textvariable=t17_st01_var_limit ) #,
		# font=("Arial", 12)) # , bg="lightgrey", fg="blue", command=on_spinbox_change)
	# Setting options for the Spinbox
	# t17_st01_spbx_limit.config(state="normal", cursor="hand2", bd=3, justify="center", wrap=True)
	# Placing the Spinbox in the window
	t17_st01_spbx_limit.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() )
	#
	t17_st01_button_dedupe_text = m_tkntr.StringVar()
	t17_st01_button_dedupe_text.set( Strng_RunProcess() )
	t17_st01_button_dedupe = m_tkntr.Button( t17_st01_frame5, textvariable=t17_st01_button_dedupe_text, command=t17_st01_on_button_dedupe)
	t17_st01_button_dedupe.pack( side=m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	#
	t17_st01_label_dedupe_status = m_tkntr.Label( t17_st01_frame5, text="_", fg = "blue") # , width=60
	t17_st01_label_dedupe_status.pack( side=m_tkntr.RIGHT, padx=tk_padx(), pady=tk_pady()  )
	# warning label
	t17_st01_label_re_warning = m_tkntr.Label( t17_st01_frame5, text=":") 
	t17_st01_label_re_warning.pack( side= m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() )
	# --------- Frame 7 Findings
	t17_st01_frame7 = m_tkntr.Frame( t17_tab_1, bg=colour_bg_organitry())
	t17_st01_frame7.pack( fill = m_tkntr.X )
	#
	t17_st01_label_findings = m_tkntr.Label( t17_st01_frame7, text="Discoveries", bg=colour_bg_organitry())
	t17_st01_label_findings.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	# --------- Frame 9 Findings listbox
	t17_st01_frame8 = m_tkntr.Frame( t17_tab_1, bg=colour_bg_organitry())
	t17_st01_frame8.pack( fill = m_tkntr.BOTH, expand=True)
	#
	t17_st01_lb_finds = m_tkntr.Listbox( t17_st01_frame8, height=2 )
	t17_st01_lb_finds.pack( side = m_tkntr.LEFT, fill=m_tkntr.BOTH, expand=True, padx=tk_padx(), pady=tk_pady() )



	# ~~~~~~~~~~ Make 2nd subtab = Renametry
	t17_tab_2 = m_tkntr.Frame( t17_snb)
	t17_snb.add( t17_tab_2, text= "Renametry")
	# --------- Frame 0 Folder list heading
	t17_st02_frame0 = m_tkntr.Frame( t17_tab_2, bg=colour_bg_neutral())
	t17_st02_frame0.pack( fill = m_tkntr.X)
	#
	t17_st02_label_tab_heading = m_tkntr.Label( t17_st02_frame0, text="Controlled renaming folders and/or files", bg=colour_bg_neutral())
	t17_st02_label_tab_heading.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	# --------- Frame 1 Folder list entry first line
	t17_st02_frame1 = m_tkntr.Frame( t17_tab_2, bg=colour_bg_danger())
	t17_st02_frame1.pack( fill = m_tkntr.X)
	#
	t17_st02_label_heading_paths = m_tkntr.Label( t17_st02_frame1, text="PATHS", bg=colour_bg_danger())
	t17_st02_label_heading_paths.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	#
	t17_st02_label_browse_paths = m_tkntr.Label( t17_st02_frame1, text="Enter path", bg=colour_bg_danger())
	t17_st02_label_browse_paths.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	t17_st02_label_browse_paths.bind('<Button-3>',  lambda x: t17_label_paths_on_rclick_clear() ) # because <Button-3> is the Tkinter code for a right-click
	# 
	t17_st02_var_path = m_tkntr.StringVar()
	t17_st02_entry_path = m_tkntr.Entry( t17_st02_frame1, textvariable=t17_st02_var_path)
	t17_st02_entry_path.pack( side = m_tkntr.LEFT, fill = m_tkntr.X, expand=1, padx=tk_padx(), pady=tk_pady() ) 
	# 
	t17_st02_button_browse_paths = m_tkntr.Button( t17_st02_frame1, text=Strng_Browse(), command=t17_st02_on_button_browse_paths)
	t17_st02_button_browse_paths.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	#
	t17_st02_button_add_path = m_tkntr.Button( t17_st02_frame1, text=Strng_AddToList(), command=t17_st02_on_button_add_folder)
	t17_st02_button_add_path.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	#
	t17_st02_button_add_subfolders = m_tkntr.Button( t17_st02_frame1, text=Strng_AddSubs(), command=t17_st02_on_button_add_subfolders)
	t17_st02_button_add_subfolders.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	#  layout
	# --------- Frame 2 Folder list listbox
	t17_st02_frame2 = m_tkntr.Frame( t17_tab_2, bg=colour_bg_danger())
	t17_st02_frame2.pack( fill = m_tkntr.BOTH, expand=True)
	t17_st02_lb_paths = m_tkntr.Listbox( t17_st02_frame2, height=2 )
	t17_st02_lb_paths.pack( side = m_tkntr.LEFT, fill=m_tkntr.BOTH, expand=True, padx=tk_padx(), pady=tk_pady() )
	t17_st02_lb_paths.bind('<Double-Button>',  lambda x: t17_st02_on_dblclck_selected_path() )
	# --------- Frame 3 Folder list listbox editing buttons
	t17_st02_frame3 = m_tkntr.Frame( t17_tab_2, bg=colour_bg_danger())
	t17_st02_frame3.pack( fill = m_tkntr.X)
	#
	t17_st02_label_paths_selection = m_tkntr.Label( t17_st02_frame3, text="Selected path:", bg=colour_bg_danger())
	t17_st02_label_paths_selection.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	#
	t17_st02_button_remove_selected_path = m_tkntr.Button( t17_st02_frame3, text="Remove", command=t17_st02_on_button_remove_selected_path)
	t17_st02_button_remove_selected_path.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	#
	t17_st02_label_paths_all = m_tkntr.Label( t17_st02_frame3, text="All paths:", bg=colour_bg_danger())
	t17_st02_label_paths_all.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	#
	t17_st02_button_remove_all_paths = m_tkntr.Button( t17_st02_frame3, text="Clear", command=t17_st02_on_button_remove_all_paths)
	t17_st02_button_remove_all_paths.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	#
	t17_st02_button_check_list_clashes_paths = m_tkntr.Button( t17_st02_frame3, text="Check clashes", command=t17_st02_on_button_check_list_clashes_paths)
	t17_st02_button_check_list_clashes_paths.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	#
	t17_st02_chkvr_autoclashcheck_paths = m_tkntr.IntVar()
	t17_st02_chkvr_autoclashcheck_paths.set(1)
	t17_st02_chkbx_autoclashcheck_paths = m_tkntr.Checkbutton(t17_st02_frame3, text="Check as added", \
		variable=t17_st02_chkvr_autoclashcheck_paths, command = t17_st02_on_chkbx_autoclashcheck_paths)
	t17_st02_chkbx_autoclashcheck_paths.pack(side=m_tkntr.LEFT, anchor=m_tkntr.W, expand=m_tkntr.YES, padx=tk_padx(), pady=tk_pady() )
	#
	t17_st02_label_paths_alert = m_tkntr.Label( t17_st02_frame3, text="_", fg = "blue", bg=colour_bg_danger()) # , font=('italic')
	t17_st02_label_paths_alert.pack( side = m_tkntr.RIGHT, padx=tk_padx(), pady=tk_pady() )
	# --------- Frame 4 Mode option selection
	t17_st02_frame4 = m_tkntr.Frame( t17_tab_2, bg=colour_bg_danger())
	t17_st02_frame4.pack( fill = m_tkntr.X)
	#
	t17_st02_label_mode_selection = m_tkntr.Label( t17_st02_frame4, text="Renaming mode:", bg=colour_bg_danger())
	t17_st02_label_mode_selection.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	#
	t17_st02_cb_EnumRenametryMode = PairTupleCombobox( t17_st02_frame4, apltry.EnumRenametryMode_LstPairTpls(), apltry.EnumRenametryMode_Default(), state="readonly", width = 50) 
	t17_st02_cb_EnumRenametryMode.bind("<<ComboboxSelected>>", lambda x: t17_st02_on_selected_RenametryMode() )
	t17_st02_cb_EnumRenametryMode.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() )
	#
	t17_st02_label_direction_selection = m_tkntr.Label( t17_st02_frame4, text=" direction:", bg=colour_bg_danger())
	t17_st02_label_direction_selection.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	#
	t17_st02_cb_EnumRenametryDirection = PairTupleCombobox( t17_st02_frame4, apltry.EnumRenametryDirection_LstPairTpls(), apltry.EnumRenametryDirection_Default(), state="readonly", width = 50) 
	t17_st02_cb_EnumRenametryDirection.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() )
	# --------- Frame 5 Mode selected description
	t17_st02_frame5 = m_tkntr.Frame( t17_tab_2, bg=colour_bg_danger())
	t17_st02_frame5.pack( fill = m_tkntr.X)
	#
	t17_st02_label_mode_selected = m_tkntr.Label( t17_st02_frame5, text="Renaming mode description:", bg=colour_bg_danger())
	t17_st02_label_mode_selected.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	#
	t17_st02_label_mode_description = m_tkntr.Label( t17_st02_frame5, text="?", bg=colour_bg_danger())
	t17_st02_label_mode_description.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 

	# --------- Frame 5 Action buttons
	t17_st02_frame9 = m_tkntr.Frame( t17_tab_2)
	t17_st02_frame9.pack( fill = m_tkntr.X)
	# 
	t17_st02_button_rename_text = m_tkntr.StringVar()
	t17_st02_button_rename_text.set( Strng_RunProcess() )
	t17_st02_button_rename = m_tkntr.Button( t17_st02_frame9, textvariable=t17_st02_button_rename_text, command=t17_st02_on_button_rename)
	t17_st02_button_rename.pack( side=m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	#
	t17_st02_label_rename_status = m_tkntr.Label( t17_st02_frame9, text="_", fg = "blue") # , width=60
	t17_st02_label_rename_status.pack( side=m_tkntr.RIGHT, padx=tk_padx(), pady=tk_pady()  )
	# warning label
	t17_st02_label_re_warning = m_tkntr.Label( t17_st02_frame9, text=":") 
	t17_st02_label_re_warning.pack( side= m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() )
	if False:
		t17_st02_label_limit = m_tkntr.Label( t17_st02_frame9, text="Limit report count:", bg=colour_bg_neutral()) # , font=('italic')
		t17_st02_label_limit.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() )
		# use a Spinbox for the reporting limit
		t17_st02_var_limit= m_tkntr.StringVar()
		t17_st02_var_limit.set("20")
		t17_st02_spbx_limit = m_tkntr.Spinbox( t17_st02_frame9, from_= 0, to= 100, width= 10, relief="sunken", repeatdelay=500, repeatinterval=100, textvariable=t17_st02_var_limit ) #,
			# font=("Arial", 12)) # , bg="lightgrey", fg="blue", command=on_spinbox_change)
		# Setting options for the Spinbox
		# t17_st02_spbx_limit.config(state="normal", cursor="hand2", bd=3, justify="center", wrap=True)
		# Placing the Spinbox in the window
		t17_st02_spbx_limit.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() )
		#
		# --------- Frame 7 Findings
		t17_st02_frame7 = m_tkntr.Frame( t17_tab_2, bg=colour_bg_organitry())
		t17_st02_frame7.pack( fill = m_tkntr.X )
		#
		t17_st02_label_findings = m_tkntr.Label( t17_st02_frame7, text="Discoveries", bg=colour_bg_organitry())
		t17_st02_label_findings.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
		# --------- Frame 9 Findings listbox
		t17_st02_frame8 = m_tkntr.Frame( t17_tab_2, bg=colour_bg_organitry())
		t17_st02_frame8.pack( fill = m_tkntr.BOTH, expand=True)
		#
		t17_st02_lb_finds = m_tkntr.Listbox( t17_st02_frame8, height=2 )
		t17_st02_lb_finds.pack( side = m_tkntr.LEFT, fill=m_tkntr.BOTH, expand=True, padx=tk_padx(), pady=tk_pady() )

	# ========== Make a tab for: Congruentry
	t02_tab = m_tkntr.Frame(nb)
	nb.add( t02_tab, text="Congruentry")
	# --------- Frame 01 Heading
	t02_frame01 = m_tkntr.Frame( t02_tab, bg=colour_bg_neutral())
	t02_frame01.pack( fill = m_tkntr.X)
	t02_label_tab_heading = m_tkntr.Label( t02_frame01, text="Here you can check/prove that two paths or two files are identical", bg=colour_bg_neutral())
	t02_label_tab_heading.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	# Folder comparisons
	# ________________________________________
	# PATHS
	# --------- Frame Paths
	t02_frameP = m_tkntr.Frame( t02_tab, bg=colour_bg_cngrntry_fldrs())
	t02_frameP.pack( fill = m_tkntr.X)
	# --------- Frame P_01 label
	t02_frameP_01 = m_tkntr.Frame( t02_frameP, bg=colour_bg_cngrntry_fldrs())
	t02_frameP_01.pack( fill = m_tkntr.X)
	t02_label_section_paths = m_tkntr.Label( t02_frameP_01, text="Compare Two Paths", bg=colour_bg_cngrntry_fldrs())
	t02_label_section_paths.configure( font= ( "TkDefaultFont",12,"bold" ) )
	t02_label_section_paths.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) # 
	# ........................................
	# --------- Frame Path A
	t02_frameA = m_tkntr.Frame( t02_frameP, bg=colour_bg_cngrnt_a())
	t02_frameA.pack( fill = m_tkntr.X, padx=tk_padx() )
	# sub-frame 1 = the path selection
	t02_fA_frame1 = m_tkntr.Frame( t02_frameA, bg=colour_bg_cngrnt_a())
	t02_fA_frame1.pack( fill = m_tkntr.X, padx=tk_padx() )
	#
	t02_label_browse_patha = m_tkntr.Label( t02_fA_frame1, text="Path A", bg=colour_bg_cngrnt_a(), fg=colour_fg_ask() )
	t02_label_browse_patha.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) # 
	t02_label_browse_patha.bind('<Button-3>',  lambda x: t02_label_path_patha_on_rclick_clear() ) # because <Button-3> is the Tkinter code for a right-click
	#
	t02_var_patha = m_tkntr.StringVar()
	t02_entry_patha = m_tkntr.Entry( t02_fA_frame1, textvariable=t02_var_patha)
	t02_entry_patha.pack( side = m_tkntr.LEFT, fill = m_tkntr.X, expand=1, padx=tk_padx(), pady=tk_pady() ) # 
	t02_button_browse_patha = m_tkntr.Button( t02_fA_frame1, text=Strng_Browse(), command=t02_on_button_browse_patha)
	t02_button_browse_patha.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() )
	if __debug__ : # declare this to be a Settings object
		Set_ESD_EMSA( apltry.EnumSettingDef.CngrntryTwoPathAStrPath , EnumMetaSettingAspect.WidgetObject, t02_var_patha)
	# ........................................
	# # custom control initialisation flag - this needs to exist before we create the widgets that it controls
	t02_fA_Cstm_Controls_Created = False 
	# ........................................
	# sub-frame 2 = the EnumFileSysArch
	t02_fA_frame2 = m_tkntr.Frame( t02_frameA, bg=colour_bg_cngrnt_a())
	t02_fA_frame2.pack( fill = m_tkntr.X, padx=tk_padx() )
	#
	t02_fA_label_Archetype = m_tkntr.Label( t02_fA_frame2, text="Archetype:", bg=colour_bg_cngrnt_a(), fg=colour_fg_ask() )
	t02_fA_label_Archetype.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) # 
	#
	t02_fA_cb_EnumFileSysArch = PairTupleCombobox( t02_fA_frame2, apltry.EnumFileSysArch_LstPairTpls(), apltry.EnumFileSysArch_Default(), state="readonly", width = 50) 
	t02_fA_cb_EnumFileSysArch.bind("<<ComboboxSelected>>", lambda x: t02_fA_on_selected_EnumFileSysArch() )
	t02_fA_cb_EnumFileSysArch.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() )
	#
	t02_fA_button_path_detect = m_tkntr.Button( t02_fA_frame2, text="Detect", command=t02_fA_on_button_path_detect)
	t02_fA_button_path_detect.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	#
	t02_fA_label_detected = m_tkntr.Label( t02_fA_frame2, text="", bg=colour_bg_cngrnt_a(), fg=colour_fg_ask() )
	t02_fA_label_detected.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) # 
	#
	# ........................................
	# sub-frame 3 = description of OR the manual selection of ArcheType
	t02_fA_frame3 = m_tkntr.Frame( t02_frameA, bg=colour_bg_cngrnt_a())
	#t02_fA_frame3.pack( fill = m_tkntr.X, padx=tk_padx() )
	t02_fA_ArchDesc_Repacks()
	#
	t02_fA_label_fsa_lead = m_tkntr.Label( t02_fA_frame3, text="Archetype specifics:", bg=colour_bg_cngrnt_a(), fg=colour_fg_ask() )
	t02_fA_label_fsa_lead.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) # 
	#
	t02_fA_label_fsa_desc = m_tkntr.Label( t02_fA_frame3, text="?", bg=colour_bg_cngrnt_a(), fg=colour_fg_ask() )
	t02_fA_label_fsa_desc.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() )
	t02_fA_reflect_EnumFileSysArch()
	#
	# ........................................
	# sub-frame 4 = Custom manual selections
	t02_fA_frame4 = m_tkntr.Frame( t02_frameA, bg=colour_bg_cngrnt_a())
	# t02_fA_frame4.pack( fill = m_tkntr.X, padx=tk_padx() )
	t02_fA_Cstm_Repacks()
	#
	# ?? Encoding - select from list
	# - label
	t02_fA_Cstm_label_CharEnc = m_tkntr.Label( t02_fA_frame4, text="Encoding:", bg=colour_bg_cngrnt_a())
	t02_fA_Cstm_label_CharEnc.pack( side=m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() )
	# - combobox
	t02_fA_Cstm_cb_CharEnc = PairTupleCombobox( t02_fA_frame4, apltry.EnumCharEnc_LstPairTpls(), apltry.EnumCharEnc_Default(), state="readonly", width = 40) 
	# t02_fA_Cstm_cb_CharEnc.bind("<<ComboboxSelected>>", lambda x: t02_fA_on_selected_CharEnc() )
	t02_fA_Cstm_cb_CharEnc.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() )
	if __debug__ : # declare this to be a Settings object
		Set_ESD_EMSA( apltry.EnumSettingDef.CngrntryTwoPathACustomCharEnc, EnumMetaSettingAspect.WidgetObject, t02_fA_Cstm_cb_CharEnc)
	# ?? Case Sensitive - checkbox
	# CaseSensitive = True
	t02_fA_Cstm_chkvr_CaseSensitive = m_tkntr.IntVar()
	t02_fA_Cstm_chkvr_CaseSensitive.set(1)
	t02_fA_Cstm_chkbx_CaseSensitive = m_tkntr.Checkbutton( t02_fA_frame4, text="Case sensitive", variable=t02_fA_Cstm_chkvr_CaseSensitive )
	t02_fA_Cstm_chkbx_CaseSensitive.pack(side=m_tkntr.LEFT, anchor=m_tkntr.W )
	if __debug__ : # declare this to be a Settings object || 
		Set_ESD_EMSA( apltry.EnumSettingDef.CngrntryTwoPathACustomDoCase, EnumMetaSettingAspect.WidgetObject, t02_fA_Cstm_chkvr_CaseSensitive)
	#
	# - label
	t02_fA_Cstm_label_CharEncItem = m_tkntr.Label( t02_fA_frame4, text="Microsoft issues:", bg=colour_bg_cngrnt_a())
	t02_fA_Cstm_label_CharEncItem.pack( side=m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() )
	#
	## ?? Bad Names - checkbox
	# BadName = True :: checkbox
	t02_fA_Cstn_chkvr_BadName = m_tkntr.IntVar()
	t02_fA_Cstn_chkvr_BadName.set(1)
	t02_fA_Cstn_chkbx_BadName = m_tkntr.Checkbutton(t02_fA_frame4, text="Forbidden names", variable=t02_fA_Cstn_chkvr_BadName )
	t02_fA_Cstn_chkbx_BadName.pack( side=m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) # 
	if __debug__ : # declare this to be a Settings object
		Set_ESD_EMSA( apltry.EnumSettingDef.CngrntryTwoPathACustomDoMsName, EnumMetaSettingAspect.WidgetObject, t02_fA_Cstn_chkvr_BadName)
	# BadName_A_Mod = BadNameModItem.Default :: label combobox
	#
	## ?? Bad Chars - checkbox
	# CharEnc_A_Try = True
	t02_fA_Cstm_chkvr_BadChars = m_tkntr.IntVar()
	t02_fA_Cstm_chkvr_BadChars.set(1)
	t02_fA_Cstm_chkbx_BadChars = m_tkntr.Checkbutton(t02_fA_frame4, text="Characters", variable=t02_fA_Cstm_chkvr_BadChars )
	t02_fA_Cstm_chkbx_BadChars.pack( side=m_tkntr.LEFT ) # 
	if __debug__ : # declare this to be a Settings object || 
		Set_ESD_EMSA( apltry.EnumSettingDef.CngrntryTwoPathACustomDoMsChar, EnumMetaSettingAspect.WidgetObject, t02_fA_Cstm_chkvr_BadChars)
	# CharEnc_A_Lst = [CharEncItem.UTF8]
	#
	## ?? 8.3 - checkbox
	# Wind8p3 = False :: checkbox
	t02_fA_Cstn_chkvr_Wind8p3 = m_tkntr.IntVar()
	t02_fA_Cstn_chkvr_Wind8p3.set(1)
	t02_fA_Cstn_chkbx_Wind8p3 = m_tkntr.Checkbutton( t02_fA_frame4, text="8.3 names", variable=t02_fA_Cstn_chkvr_Wind8p3 )
	t02_fA_Cstn_chkbx_Wind8p3.pack( side=m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	if __debug__ : # declare this to be a Settings object
		Set_ESD_EMSA( apltry.EnumSettingDef.CngrntryTwoPathACustomDoMs8p3, EnumMetaSettingAspect.WidgetObject, t02_fA_Cstn_chkvr_Wind8p3)
	#
	t02_fA_Cstm_Unpacks()
	t02_fA_Cstm_Controls_Created = True
	# 
	# ________________________________________
	# --------- Frame 2 Path B
	t02_frameB = m_tkntr.Frame( t02_frameP, bg=colour_bg_cngrnt_a())
	t02_frameB.pack( fill = m_tkntr.X, padx=tk_padx() )
	# sub-frame 1 = the path selection
	t02_fB_frame2 = m_tkntr.Frame( t02_frameB, bg=colour_bg_cngrnt_b())
	t02_fB_frame2.pack( fill = m_tkntr.X, padx=tk_padx() )
	#
	t02_label_browse_pathb = m_tkntr.Label( t02_fB_frame2, text="Path B", bg=colour_bg_cngrnt_b(), fg=colour_fg_ask() )
	t02_label_browse_pathb.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	t02_label_browse_pathb.bind('<Button-3>',  lambda x: t02_label_path_pathb_on_rclick_clear() ) # because <Button-3> is the Tkinter code for a right-click
	# 
	t02_var_pathb = m_tkntr.StringVar()
	t02_entry_pathb = m_tkntr.Entry( t02_fB_frame2, textvariable=t02_var_pathb)
	t02_entry_pathb.pack( side = m_tkntr.LEFT, fill = m_tkntr.X, expand=1, padx=tk_padx(), pady=tk_pady() ) 
	if __debug__ : # declare this to be a Settings object 
		Set_ESD_EMSA( apltry.EnumSettingDef.CngrntryTwoPathBStrPath, EnumMetaSettingAspect.WidgetObject, t02_var_pathb)
	# 
	t02_button_browse_pathb = m_tkntr.Button( t02_fB_frame2, text=Strng_Browse(), command=t02_on_button_browse_pathb)
	t02_button_browse_pathb.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	# ........................................
	# # custom control initialisation flag - this needs to exist before we create the widgets that it controls 
	t02_fB_Cstm_Controls_Created = False
	# ........................................
	# sub-frame 2 = the EnumFileSysArch
	t02_fB_frame2 = m_tkntr.Frame( t02_frameB, bg=colour_bg_cngrnt_b())
	t02_fB_frame2.pack( fill = m_tkntr.X, padx=tk_padx() )
	#
	t02_fB_label_Archetype = m_tkntr.Label( t02_fB_frame2, text="Archetype:", bg=colour_bg_cngrnt_b(), fg=colour_fg_ask() )
	t02_fB_label_Archetype.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) # 
	#
	t02_fB_cb_EnumFileSysArch = PairTupleCombobox( t02_fB_frame2, apltry.EnumFileSysArch_LstPairTpls(), apltry.EnumFileSysArch_Default(), state="readonly", width = 50) 
	t02_fB_cb_EnumFileSysArch.bind("<<ComboboxSelected>>", lambda x: t02_fB_on_selected_EnumFileSysArch() )
	t02_fB_cb_EnumFileSysArch.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() )
	#
	t02_fB_button_path_detect = m_tkntr.Button( t02_fB_frame2, text="Detect", command=t02_fB_on_button_path_detect)
	t02_fB_button_path_detect.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	#
	t02_fB_label_detected = m_tkntr.Label( t02_fB_frame2, text="", bg=colour_bg_cngrnt_b(), fg=colour_fg_ask() )
	t02_fB_label_detected.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) # 
	# ........................................
	# sub-frame 3 = description of OR the manual selection of ArcheType
	t02_fB_frame3 = m_tkntr.Frame( t02_frameB, bg=colour_bg_cngrnt_b())
	t02_fB_frame3.pack( fill = m_tkntr.X, padx=tk_padx() )
	#
	t02_fB_label_fsa_lead = m_tkntr.Label( t02_fB_frame3, text="Archetype specifics:", bg=colour_bg_cngrnt_b(), fg=colour_fg_ask() )
	t02_fB_label_fsa_lead.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) # 
	#
	t02_fB_label_fsa_desc = m_tkntr.Label( t02_fB_frame3, text="?", bg=colour_bg_cngrnt_b(), fg=colour_fg_ask() )
	t02_fB_label_fsa_desc.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() )
	t02_fB_reflect_EnumFileSysArch()
	# ........................................
	# sub-frame 4 = Custom manual selections
	t02_fB_frame4 = m_tkntr.Frame( t02_frameB, bg=colour_bg_cngrnt_a())
	# t02_fB_frame4.pack( fill = m_tkntr.X, padx=tk_padx() )
	t02_fB_Cstm_Repacks()
	#
	# ?? Encoding - select from list
	# - label
	t02_fB_Cstm_label_CharEnc = m_tkntr.Label( t02_fB_frame4, text="Encoding:", bg=colour_bg_cngrnt_a())
	t02_fB_Cstm_label_CharEnc.pack( side=m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() )
	# - combobox
	t02_fB_Cstm_cb_CharEnc = PairTupleCombobox( t02_fB_frame4, apltry.EnumCharEnc_LstPairTpls(), apltry.EnumCharEnc_Default(), state="readonly", width = 40) 
	# t02_fB_Cstm_cb_CharEnc.bind("<<ComboboxSelected>>", lambda x: t02_fB_on_selected_CharEnc() )
	t02_fB_Cstm_cb_CharEnc.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() )
	if __debug__ : # declare this to be a Settings object
		Set_ESD_EMSA( apltry.EnumSettingDef.CngrntryTwoPathBCustomCharEnc, EnumMetaSettingAspect.WidgetObject, t02_fB_Cstm_cb_CharEnc)
	# ?? Case Sensitive - checkbox
	# CaseSensitive = True
	t02_fB_Cstm_chkvr_CaseSensitive = m_tkntr.IntVar()
	t02_fB_Cstm_chkvr_CaseSensitive.set(1)
	t02_fB_Cstm_chkbx_CaseSensitive = m_tkntr.Checkbutton( t02_fB_frame4, text="Case sensitive", variable=t02_fB_Cstm_chkvr_CaseSensitive )
	t02_fB_Cstm_chkbx_CaseSensitive.pack(side=m_tkntr.LEFT, anchor=m_tkntr.W )
	if __debug__ : # declare this to be a Settings object
		Set_ESD_EMSA( apltry.EnumSettingDef.CngrntryTwoPathBCustomDoCase, EnumMetaSettingAspect.WidgetObject, t02_fB_Cstm_chkvr_CaseSensitive)
	#
	# - label
	t02_fB_Cstm_label_CharEncItem = m_tkntr.Label( t02_fB_frame4, text="Microsoft issues:", bg=colour_bg_cngrnt_a())
	t02_fB_Cstm_label_CharEncItem.pack( side=m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() )
	#
	## ?? Bad Names - checkbox
	# BadName = True :: checkbox
	t02_fB_Cstn_chkvr_BadName = m_tkntr.IntVar()
	t02_fB_Cstn_chkvr_BadName.set(1)
	t02_fB_Cstn_chkbx_BadName = m_tkntr.Checkbutton(t02_fB_frame4, text="Forbidden names", variable=t02_fB_Cstn_chkvr_BadName )
	t02_fB_Cstn_chkbx_BadName.pack( side=m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) # 
	if __debug__ : # declare this to be a Settings object
		Set_ESD_EMSA( apltry.EnumSettingDef.CngrntryTwoPathBCustomDoMsName, EnumMetaSettingAspect.WidgetObject, t02_fB_Cstn_chkvr_BadName)
		
	# BadName_A_Mod = BadNameModItem.Default :: label combobox
	#
	## ?? Bad Chars - checkbox
	# CharEnc_A_Try = True
	t02_fB_Cstm_chkvr_BadChars = m_tkntr.IntVar()
	t02_fB_Cstm_chkvr_BadChars.set(1)
	t02_fB_Cstm_chkbx_BadChars = m_tkntr.Checkbutton(t02_fB_frame4, text="Characters", variable=t02_fB_Cstm_chkvr_BadChars )
	t02_fB_Cstm_chkbx_BadChars.pack( side=m_tkntr.LEFT ) # 
	if __debug__ : # declare this to be a Settings object || 
		Set_ESD_EMSA( apltry.EnumSettingDef.CngrntryTwoPathBCustomDoMsChar, EnumMetaSettingAspect.WidgetObject, t02_fB_Cstm_chkvr_BadChars)
	# CharEnc_A_Lst = [CharEncItem.UTF8]
	#
	## ?? 8.3 - checkbox
	# Wind8p3 = False :: checkbox
	t02_fB_Cstn_chkvr_Wind8p3 = m_tkntr.IntVar()
	t02_fB_Cstn_chkvr_Wind8p3.set(1)
	t02_fB_Cstn_chkbx_Wind8p3 = m_tkntr.Checkbutton( t02_fB_frame4, text="8.3 names", variable=t02_fB_Cstn_chkvr_Wind8p3 )
	t02_fB_Cstn_chkbx_Wind8p3.pack( side=m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	if __debug__ : # declare this to be a Settings object
		Set_ESD_EMSA( apltry.EnumSettingDef.CngrntryTwoPathBCustomDoMs8p3, EnumMetaSettingAspect.WidgetObject, t02_fB_Cstn_chkvr_Wind8p3)
	#
	t02_fB_Cstm_Unpacks()
	t02_fB_Cstm_Controls_Created = True
	# 
	# ........................................
	# --------- Frame 90 Options
	t02_frame90 = m_tkntr.LabelFrame( t02_frameP, text="Options", bg=colour_bg_cngrntry_fldrs() )
	t02_frame90.pack( fill=m_tkntr.X, padx=tk_padx()  ) # , expand=m_tkntr.YES
	# --------- Frame 3 Controls Line 1
	t02_frame3 = m_tkntr.Frame( t02_frame90, bg=colour_bg_cngrntry_fldrs())
	t02_frame3.pack( fill = m_tkntr.X)
	t02_label_depth = m_tkntr.Label( t02_frame3, text="Analytical depth:", bg=colour_bg_cngrntry_fldrs(), fg=colour_fg_ask() ) # , width=10
	t02_chkvr_Stringent = m_tkntr.IntVar()
	t02_chkbx_Stringent = m_tkntr.Checkbutton( t02_frame3, text="Stringent", variable=t02_chkvr_Stringent, highlightthickness=0, bg=colour_bg_cngrntry_fldrs() )
	if __debug__ : # declare this to be a Settings object
		Set_ESD_EMSA( apltry.EnumSettingDef.CngrntryAnalyseStringent, EnumMetaSettingAspect.WidgetObject, t02_chkvr_Stringent)
	#
	#  layout
	t02_label_depth.pack( side=m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) # , fill=m_tkntr.BOTH, expand=True
	t02_chkbx_Stringent.pack( side=m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) # , anchor=m_tkntr.W, expand=m_tkntr.YES)
	# --------- Frame 4 Controls Line 2
	t02_frame4 = m_tkntr.Frame( t02_frame90, bg=colour_bg_cngrntry_fldrs())
	t02_frame4.pack( fill = m_tkntr.X)
	t02_label_stops = m_tkntr.Label( t02_frame4, text="Stops:", bg=colour_bg_cngrntry_fldrs(), fg=colour_fg_ask() ) # , width=10
	t02_label_stops.pack( side=m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) # , fill=m_tkntr.BOTH, expand=True
	t02_chkvr_StopNotCongruent = m_tkntr.IntVar()
	t02_chkbx_StopNotCongruent = m_tkntr.Checkbutton( t02_frame4, text="Stop when Not Congruent", variable=t02_chkvr_StopNotCongruent, highlightthickness=0, bg=colour_bg_cngrntry_fldrs() )
	t02_chkbx_StopNotCongruent.pack(side=m_tkntr.LEFT, anchor=m_tkntr.W, expand=m_tkntr.YES, padx=tk_padx(), pady=tk_pady() )
	if __debug__ : # declare this to be a Settings object
		Set_ESD_EMSA( apltry.EnumSettingDef.CngrntryStopNotCongruent, EnumMetaSettingAspect.WidgetObject, t02_chkvr_StopNotCongruent)
	t02_chkvr_StopNotMoiety = m_tkntr.IntVar(value=1)
	t02_chkbx_StopNotMoiety = m_tkntr.Checkbutton( t02_frame4, text="Stop when Not Moiety", variable=t02_chkvr_StopNotMoiety, highlightthickness=0, bg=colour_bg_cngrntry_fldrs() )
	t02_chkbx_StopNotMoiety.pack(side=m_tkntr.LEFT, anchor=m_tkntr.W, expand=m_tkntr.YES, padx=tk_padx(), pady=tk_pady() )
	if __debug__ : # declare this to be a Settings object
		Set_ESD_EMSA( apltry.EnumSettingDef.CngrntryStopNotMoiety, EnumMetaSettingAspect.WidgetObject, t02_chkvr_StopNotMoiety)
	# --------- Frame 5 Controls Line 3
	t02_frame5 = m_tkntr.Frame( t02_frame90, bg=colour_bg_cngrntry_fldrs())
	t02_frame5.pack( fill = m_tkntr.X)
	# label Reporting options
	t02_label_reports = m_tkntr.Label( t02_frame5, text="Reporting:", bg=colour_bg_cngrntry_fldrs(), fg=colour_fg_ask() ) # , width=10
	t02_label_reports.pack( side=m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) # , fill=m_tkntr.BOTH, expand=True
	# checkbox output a report of differences
	t02_chkvr_ReportDiffs = m_tkntr.IntVar()
	t02_chkbx_ReportDiffs = m_tkntr.Checkbutton( t02_frame5, text="ReportDiffs", variable=t02_chkvr_ReportDiffs, highlightthickness=0, bg=colour_bg_cngrntry_fldrs() )
	t02_chkbx_ReportDiffs.pack( side=m_tkntr.LEFT, anchor=m_tkntr.W, expand=m_tkntr.YES, padx=tk_padx(), pady=tk_pady() )
	if __debug__ : # declare this to be a Settings object
		Set_ESD_EMSA( apltry.EnumSettingDef.CngrntryReportDiffs, EnumMetaSettingAspect.WidgetObject, t02_chkvr_ReportDiffs)
	# Checkbox output a summary of differences
	t02_chkvr_SummaryDiffs = m_tkntr.IntVar(value=1)
	t02_chkbx_SummaryDiffs = m_tkntr.Checkbutton( t02_frame5, text="SummaryDiffs", variable=t02_chkvr_SummaryDiffs, highlightthickness=0, bg=colour_bg_cngrntry_fldrs() )
	t02_chkbx_SummaryDiffs.pack( side=m_tkntr.LEFT, anchor=m_tkntr.W, expand=m_tkntr.YES, padx=tk_padx(), pady=tk_pady() )
	if __debug__ : # declare this to be a Settings object || 
		Set_ESD_EMSA( apltry.EnumSettingDef.CngrntryDiffSummary, EnumMetaSettingAspect.WidgetObject, t02_chkvr_SummaryDiffs)
	# adding in a checkbox for reporting stringent match failures
	t02_chkvr_cngrnt_fails_strngnt = m_tkntr.IntVar()
	t02_chkbx_cngrnt_fails_strngnt = m_tkntr.Checkbutton( t02_frame5, text="Report Stringent Failures", variable=t02_chkvr_cngrnt_fails_strngnt, highlightthickness=0, bg=colour_bg_cngrntry_fldrs() )
	t02_chkbx_cngrnt_fails_strngnt.pack( side=m_tkntr.LEFT, anchor=m_tkntr.W, expand=m_tkntr.YES, padx=tk_padx(), pady=tk_pady() )
	t02_chkvr_cngrnt_fails_strngnt.set(1)
	if __debug__ : # declare this to be a Settings object
		Set_ESD_EMSA( apltry.EnumSettingDef.CngrntryReportStringentDiffs, EnumMetaSettingAspect.WidgetObject, t02_chkvr_cngrnt_fails_strngnt)
	# sub-frame 91 - inside the LabelFrame
	t02_frame91 = m_tkntr.Frame( t02_frame90, bg=colour_bg_cngrntry_fldrs() )
	t02_frame91.pack( fill = m_tkntr.X)
	t02_label_GenCopyScript = m_tkntr.Label( t02_frame91, text="Generate:", bg=colour_bg_cngrntry_fldrs(), fg=colour_fg_ask() ) # , bg=colour_bg_compare() , width=10
	t02_label_GenCopyScript.pack( side=m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() )
	t02_chkvr_GenCopyScript = m_tkntr.IntVar()
	t02_chkbx_GenCopyScript = m_tkntr.Checkbutton( t02_frame91, text="Generate a copy script", variable=t02_chkvr_GenCopyScript, highlightthickness=0, bg=colour_bg_neutral() )
	t02_chkvr_GenCopyScript.set(1)
	t02_chkbx_GenCopyScript.pack( side=m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() )
	if __debug__ : # declare this to be a Settings object
		Set_ESD_EMSA( apltry.EnumSettingDef.CngrntryGenCopyScript, EnumMetaSettingAspect.WidgetObject, t02_chkvr_GenCopyScript)
	#t02_chkbx_GenCopyScript.configure(state=m_tkntr.DISABLED)
	t02_label_shell = m_tkntr.Label( t02_frame91, text="Script shell:", bg=colour_bg_cngrntry_fldrs() ) # width=20, 
	t02_label_shell.pack( side=m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) # fill=m_tkntr.BOTH, expand=True
	t02_cb_EnumGenCopyScriptAs = PairTupleCombobox( t02_frame91, apltry.EnumGenCopyScriptAs_LstPairTpls(), apltry.EnumGenCopyScriptAs_Default(), state="readonly", width = 20) 
	t02_cb_EnumGenCopyScriptAs.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() )
	if __debug__ : # declare this to be a Settings object
		Set_ESD_EMSA( apltry.EnumSettingDef.CngrntryCopyScriptShell, EnumMetaSettingAspect.WidgetObject, t02_cb_EnumGenCopyScriptAs)
	# --------- Frame 8 Path Action buttons
	t02_frame8 = m_tkntr.Frame( t02_frameP, bg=colour_bg_cngrntry_fldrs())
	t02_frame8.pack( fill = m_tkntr.X)
	#
	t02_button_paths_process_text = m_tkntr.StringVar()
	t02_button_paths_process_text.set( Strng_RunProcess() )
	t02_button_paths_process = m_tkntr.Button( t02_frame8, textvariable=t02_button_paths_process_text, command=t02_on_button_congruentry_paths_process)
	t02_label_paths_process_status = m_tkntr.Label( t02_frame8, text="_", fg = "blue", bg=colour_bg_cngrntry_fldrs()) # , width=60
	#
	t02_button_paths_process.pack( side=m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	t02_label_paths_process_status.pack( side=m_tkntr.RIGHT, padx=tk_padx(), pady=tk_pady() )
	# ........................................
	# FILES
	# File comparisons
	# --------- Frame Paths
	t02_frameF = m_tkntr.Frame( t02_tab, bg=colour_bg_neutral2())
	t02_frameF.pack( fill = m_tkntr.X)
	# --------- Frame 0 Files
	t02_frame10 = m_tkntr.Frame( t02_frameF, bg=colour_bg_neutral2())
	t02_frame10.pack( fill = m_tkntr.X)
	#
	t02_label_section_files = m_tkntr.Label( t02_frame10, text="Compare Two Files", bg=colour_bg_neutral2())
	t02_label_section_files.configure( font= ( "TkDefaultFont",12,"bold" ) )
	t02_label_section_files.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) # 
	# --------- Frame File A
	t02_frame11 = m_tkntr.Frame( t02_frameF, bg=colour_bg_cngrnt_a())
	t02_frame11.pack( fill = m_tkntr.X, padx=tk_padx() )
	#
	t02_label_browse_filea = m_tkntr.Label( t02_frame11, text="File A", bg=colour_bg_cngrnt_a())
	t02_label_browse_filea.bind('<Button-3>',  lambda x: t02_label_path_filea_on_rclick_clear() ) # because <Button-3> is the Tkinter code for a right-click
	t02_label_browse_filea.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) # 
	#
	t02_var_filea = m_tkntr.StringVar()
	t02_entry_filea = m_tkntr.Entry( t02_frame11, textvariable=t02_var_filea)
	t02_entry_filea.pack( side = m_tkntr.LEFT, fill = m_tkntr.X, expand=1, padx=tk_padx(), pady=tk_pady() ) # 
	if __debug__ : # declare this to be a Settings object || 
		Set_ESD_EMSA( apltry.EnumSettingDef.CngrntryTwoFileAStrFileref, EnumMetaSettingAspect.WidgetObject, t02_var_filea)
	# 
	t02_button_browse_filea = m_tkntr.Button( t02_frame11, text=Strng_Browse(), command=t02_on_button_browse_filea)
	t02_button_browse_filea.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	# --------- Frame 2 File B
	t02_frame12 = m_tkntr.Frame( t02_frameF, bg=colour_bg_cngrnt_b())
	t02_frame12.pack( fill = m_tkntr.X, padx=tk_padx() )
	#
	t02_label_browse_fileb = m_tkntr.Label( t02_frame12, text="File B", bg=colour_bg_cngrnt_b() )
	t02_label_browse_fileb.bind('<Button-3>',  lambda x: t02_label_path_fileb_on_rclick_clear() ) # because <Button-3> is the Tkinter code for a right-click
	t02_label_browse_fileb.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) # 
	#
	t02_var_fileb = m_tkntr.StringVar()
	t02_entry_fileb = m_tkntr.Entry( t02_frame12, textvariable=t02_var_fileb)
	t02_entry_fileb.pack( side = m_tkntr.LEFT, fill = m_tkntr.X, expand=1, padx=tk_padx(), pady=tk_pady() ) # 
	if __debug__ : # declare this to be a Settings object || 
		Set_ESD_EMSA( apltry.EnumSettingDef.CngrntryTwoFileBStrFileref, EnumMetaSettingAspect.WidgetObject, t02_var_fileb)
	# 
	t02_button_browse_fileb = m_tkntr.Button( t02_frame12, text=Strng_Browse(), command=t02_on_button_browse_fileb)
	t02_button_browse_fileb.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	# --------- Frame for Controls
	t02_frame13 = m_tkntr.Frame( t02_frameF, bg=colour_bg_neutral2())
	t02_frame13.pack( fill = m_tkntr.X)
	#
	t02_label_files_depth = m_tkntr.Label( t02_frame13, text="Analytical depth:", bg=colour_bg_neutral2() ) # , width=10
	t02_label_files_depth.pack( side=m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) # , fill=m_tkntr.BOTH, expand=True
	#
	t02_chkvr_files_Stringent = m_tkntr.IntVar()
	t02_chkbx_files_Stringent = m_tkntr.Checkbutton( t02_frame13, text="Stringent", variable=t02_chkvr_files_Stringent, bd=0, highlightthickness=0, bg=colour_bg_neutral2() ) # 
	t02_chkvr_files_Stringent.set(1)
	t02_chkbx_files_Stringent.configure(state=m_tkntr.DISABLED)
	t02_chkbx_files_Stringent.pack(side=m_tkntr.LEFT, anchor=m_tkntr.W, expand=m_tkntr.YES)
	# --------- Frame for File Action buttons
	t02_frame14 = m_tkntr.Frame( t02_frameF, bg=colour_bg_neutral2())
	t02_frame14.pack( fill = m_tkntr.X)
	#
	t02_button_files_process_text = m_tkntr.StringVar()
	t02_button_files_process_text.set( Strng_RunProcess() )
	t02_button_processfiles = m_tkntr.Button( t02_frame14, textvariable=t02_button_files_process_text, command=t02_on_button_congruentry_files_process)
	t02_label_files_process_status = m_tkntr.Label( t02_frame14, text="_", fg = "blue", bg=colour_bg_neutral2()) # , width=60
	#
	t02_button_processfiles.pack( side=m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	t02_label_files_process_status.pack( side=m_tkntr.RIGHT, padx=tk_padx(), pady=tk_pady() )
	#
	# ========== Make a tab for: Veritry
	t19_tab = m_tkntr.Frame(nb)
	nb.add( t19_tab, text="Veritry")
	#
	# --------- Frame 01 Heading
	t19_frame01 = m_tkntr.Frame( t19_tab, bg=colour_bg_neutral())
	t19_frame01.pack( fill = m_tkntr.X)
	t19_label_tab_heading = m_tkntr.Label( t19_frame01, text="Here you can check/prove that files found in folder set A are identical if in set B", bg=colour_bg_neutral())
	t19_label_tab_heading.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	# --------- Frame 02 Disposal entry first line
	t19_frame02 = m_tkntr.Frame( t19_tab, bg=colour_bg_compare())
	t19_frame02.pack( fill = m_tkntr.X)
	t19_label_heading_disposal = m_tkntr.Label( t19_frame02, text="Set A", bg=colour_bg_compare())
	t19_label_heading_disposal.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	#
	t19_label_browse_disposal = m_tkntr.Label( t19_frame02, text="Enter path", bg=colour_bg_compare())
	t19_label_browse_disposal.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	t19_label_browse_disposal.bind('<Button-3>',  lambda x: t19_label_path_disposal_on_rclick_clear() ) # because <Button-3> is the Tkinter code for a right-click
	# 
	t19_var_disposal = m_tkntr.StringVar()
	t19_entry_disposal = m_tkntr.Entry( t19_frame02, textvariable=t19_var_disposal)
	t19_entry_disposal.pack( side = m_tkntr.LEFT, fill = m_tkntr.X, expand=1, padx=tk_padx(), pady=tk_pady() ) 
	# 
	t19_button_browse_disposal = m_tkntr.Button( t19_frame02, text=Strng_Browse(), command=t19_on_button_browse_disposal)
	t19_button_browse_disposal.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	#
	t19_button_add_disposal = m_tkntr.Button( t19_frame02, text=Strng_AddToList(), command=t19_on_button_add_folder_disposal)
	t19_button_add_disposal.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	#
	t19_button_sub_disposal = m_tkntr.Button( t19_frame02, text=Strng_AddSubs(), command=t19_on_button_add_subfolders_disposal)
	t19_button_sub_disposal.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() )
	# --------- Frame 03 Disposal listbox
	t19_frame03 = m_tkntr.Frame( t19_tab, bg=colour_bg_compare())
	t19_frame03.pack( fill = m_tkntr.BOTH, expand=True)
	t19_lb_disposal = m_tkntr.Listbox( t19_frame03, height=2 )
	t19_lb_disposal.pack( side = m_tkntr.LEFT, fill=m_tkntr.BOTH, expand=True, padx=tk_padx(), pady=tk_pady() )
	t19_lb_disposal.bind('<Double-Button>',  lambda x: t19_on_dblclck_selected_disposal() )
	# --------- Frame 04 Disposal listbox editing buttons
	t19_frame04 = m_tkntr.Frame( t19_tab, bg=colour_bg_compare())
	t19_frame04.pack( fill = m_tkntr.X)
	#
	t19_label_disposal_selection = m_tkntr.Label( t19_frame04, text="Selected A path:", bg=colour_bg_compare())
	t19_label_disposal_selection.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	t19_button_swap_selected_disposal = m_tkntr.Button( t19_frame04, text="Swap", command=t19_on_button_swap_selected_disposal)
	t19_button_swap_selected_disposal.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	t19_button_remove_selected_disposal = m_tkntr.Button( t19_frame04, text="Remove", command=t19_on_button_remove_selected_disposal)
	t19_button_remove_selected_disposal.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	t19_label_disposal_all = m_tkntr.Label( t19_frame04, text="All paths:", bg=colour_bg_compare())
	t19_label_disposal_all.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	t19_button_remove_all_disposals = m_tkntr.Button( t19_frame04, text="Clear", command=t19_on_button_remove_all_disposals)
	t19_button_remove_all_disposals.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	t19_button_check_list_clashes_disposals = m_tkntr.Button( t19_frame04, text="Check clashes", command=t19_on_button_check_list_clashes_disposals)
	t19_button_check_list_clashes_disposals.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	t19_chkvr_autoclashcheck_disposals = m_tkntr.IntVar()
	t19_chkvr_autoclashcheck_disposals.set(1)
	t19_chkbx_autoclashcheck_disposals = m_tkntr.Checkbutton(t19_frame04, text="Check as added", \
		variable=t19_chkvr_autoclashcheck_disposals, command = t19_on_chkbx_autoclashcheck_disposals)
	t19_chkbx_autoclashcheck_disposals.pack(side=m_tkntr.LEFT, anchor=m_tkntr.W, expand=m_tkntr.YES, padx=tk_padx(), pady=tk_pady() )
	t19_label_disposal_list_alert = m_tkntr.Label( t19_frame04, text="_", fg = "blue", bg=colour_bg_compare()) # , font=('italic')
	t19_label_disposal_list_alert.pack( side = m_tkntr.RIGHT, padx=tk_padx(), pady=tk_pady() ) #
	# --------- Frame 06 Survival entry first line
	t19_frame06 = m_tkntr.Frame( t19_tab, bg=colour_bg_safely())
	t19_frame06.pack( fill = m_tkntr.X)
	#
	t19_label_heading_survival = m_tkntr.Label( t19_frame06, text="Set B", bg=colour_bg_safely())
	t19_label_heading_survival.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) # 
	#
	t19_label_browse_survival = m_tkntr.Label( t19_frame06, text="Enter path", bg=colour_bg_safely())
	t19_label_browse_survival.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	t19_label_browse_survival.bind('<Button-3>',  lambda x: t19_label_path_survival_on_rclick_clear() ) # because <Button-3> is the Tkinter code for a right-click
	#
	t19_var_survival = m_tkntr.StringVar()
	t19_entry_survival = m_tkntr.Entry( t19_frame06, textvariable=t19_var_survival)
	t19_entry_survival.pack( side = m_tkntr.LEFT, fill = m_tkntr.X, expand=1, padx=tk_padx(), pady=tk_pady() ) #
	#
	t19_button_browse_survival = m_tkntr.Button( t19_frame06, text=Strng_Browse(), command=t19_on_button_browse_survival)
	t19_button_browse_survival.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	#
	t19_button_add_survival = m_tkntr.Button( t19_frame06, text=Strng_AddToList(), command=t19_on_button_add_folder_survival)
	t19_button_add_survival.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	#
	t19_button_sub_survival = m_tkntr.Button( t19_frame06, text=Strng_AddSubs(), command=t19_on_button_add_subs_survival)
	t19_button_sub_survival.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	# --------- Frame 07 Survival listbox
	t19_frame07 = m_tkntr.Frame( t19_tab, bg=colour_bg_safely())
	t19_frame07.pack( fill=m_tkntr.BOTH, expand=True)
	#
	t19_lb_survival = m_tkntr.Listbox( t19_frame07, height=2)
	t19_lb_survival.pack( side = m_tkntr.LEFT, fill=m_tkntr.BOTH, expand=True, padx=tk_padx(), pady=tk_pady() )
	t19_lb_survival.bind('<Double-Button>',  lambda x: t19_on_dblclck_selected_survival() )
	# --------- Frame 08 Survival listbox editing buttons
	t19_frame08 = m_tkntr.Frame( t19_tab, bg=colour_bg_safely())
	t19_frame08.pack( fill = m_tkntr.X)
	#
	t19_label_survival_selection = m_tkntr.Label( t19_frame08, text="Selected B path:", bg=colour_bg_safely())
	t19_label_survival_selection.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	t19_button_swap_selected_survival = m_tkntr.Button( t19_frame08, text="Swap", command=t19_on_button_swap_selected_survival)
	t19_button_swap_selected_survival.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	t19_button_remove_selected_survival = m_tkntr.Button( t19_frame08, text="Remove", command=t19_on_button_remove_selected_survival)
	t19_button_remove_selected_survival.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	t19_label_survival_all = m_tkntr.Label( t19_frame08, text="All paths:", bg=colour_bg_safely())
	t19_label_survival_all.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	t19_button_remove_all_survivals = m_tkntr.Button( t19_frame08, text="Clear", command=t19_on_button_remove_all_survivals)
	t19_button_remove_all_survivals.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	t19_button_check_list_clashes_survivals = m_tkntr.Button( t19_frame08, text="Check clashes", command=t19_on_button_check_list_clashes_survivals)
	t19_button_check_list_clashes_survivals.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	t19_chkvr_autoclashcheck_survivals = m_tkntr.IntVar()
	t19_chkvr_autoclashcheck_survivals.set(1)
	t19_chkbx_autoclashcheck_survivals = m_tkntr.Checkbutton(t19_frame08, text="Check as added", \
		variable=t19_chkvr_autoclashcheck_survivals, command = t19_on_chkbx_autoclashcheck_survivals)
	t19_chkbx_autoclashcheck_survivals.pack(side=m_tkntr.LEFT, anchor=m_tkntr.W, expand=m_tkntr.YES, padx=tk_padx(), pady=tk_pady() )
	t19_label_survival_list_alert = m_tkntr.Label( t19_frame08, text="_", fg = "blue", bg=colour_bg_safely())
	t19_label_survival_list_alert.pack( side = m_tkntr.RIGHT, padx=tk_padx(), pady=tk_pady() ) #
	# --------- Frame 09 Both Lists and run type
	t19_frame09 = m_tkntr.Frame( t19_tab)
	t19_frame09.pack( fill = m_tkntr.X)
	t19_button_swaplists = m_tkntr.Button( t19_frame09, text="Swap lists", command=t19_on_button_swaplists)
	t19_button_swaplists.pack( side=m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	#
	t19_button_checklists = m_tkntr.Button( t19_frame09, text="Check for clashes", command=t19_on_button_checklists)
	t19_button_checklists.pack( side=m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	#
	t19_button_clearall = m_tkntr.Button( t19_frame09, text="Clear All", command=t19_on_button_clearall)
	t19_button_clearall.pack( side=m_tkntr.RIGHT, padx=tk_padx(), pady=tk_pady() ) #
	# --------- Frame 105 Within Match Strategy Controls
	t19_frame105 = m_tkntr.Frame( t19_tab)
	t19_frame105.pack( fill = m_tkntr.X)
	# Operating mode selection
	t19_label_EnumMatchMode = m_tkntr.Label( t19_frame105, text="Group matching:") 
	t19_label_EnumMatchMode.pack( side= m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() )
	t19_cb_EnumMatchMode = PairTupleCombobox( t19_frame105, apltry.EnumMatchMode_LstPairTpls(), apltry.EnumMatchMode_Default(), state="readonly", width = 30) 
	t19_cb_EnumMatchMode.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() )
	t19_cb_EnumMatchMode.bind("<<ComboboxSelected>>", lambda x: t19_on_defmod_combobox_release())
	# Now do four check boxes - Hash Size Name When
	# Check box - Hash
	t19_chkvr_defmod_hash = m_tkntr.IntVar()
	t19_chkbx_defmod_hash = m_tkntr.Checkbutton( t19_frame105, text="Content hashing", variable= t19_chkvr_defmod_hash)
	t19_chkbx_defmod_hash.pack( side=m_tkntr.LEFT, anchor=m_tkntr.W, padx=tk_padx(), pady=tk_pady() ) # expand=m_tkntr.YES, 
	t19_chkbx_defmod_hash.bind('<ButtonRelease>',  lambda x: t19_on_defmod_checkbox_release() )
	# Check box - Size
	t19_chkvr_defmod_size = m_tkntr.IntVar()
	t19_chkbx_defmod_size = m_tkntr.Checkbutton( t19_frame105, text="Size", variable= t19_chkvr_defmod_size)
	t19_chkbx_defmod_size.pack( side=m_tkntr.LEFT, anchor=m_tkntr.W, padx=tk_padx(), pady=tk_pady() ) # expand=m_tkntr.YES, 
	t19_chkbx_defmod_size.bind('<ButtonRelease>',  lambda x: t19_on_defmod_checkbox_release() )
	# Check box - Name
	t19_chkvr_defmod_name = m_tkntr.IntVar()
	t19_chkbx_defmod_name = m_tkntr.Checkbutton( t19_frame105, text="Name", variable= t19_chkvr_defmod_name)
	t19_chkbx_defmod_name.pack( side=m_tkntr.LEFT, anchor=m_tkntr.W, padx=tk_padx(), pady=tk_pady() ) # expand=m_tkntr.YES, 
	t19_chkbx_defmod_name.bind('<ButtonRelease>',  lambda x: t19_on_defmod_checkbox_release() )
	# Check box - When
	t19_chkvr_defmod_when = m_tkntr.IntVar()
	t19_chkbx_defmod_when = m_tkntr.Checkbutton( t19_frame105, text="When", variable= t19_chkvr_defmod_when)
	t19_chkbx_defmod_when.pack( side=m_tkntr.LEFT, anchor=m_tkntr.W, padx=tk_padx(), pady=tk_pady() ) # expand=m_tkntr.YES, 
	t19_chkbx_defmod_when.bind('<ButtonRelease>',  lambda x: t19_on_defmod_checkbox_release() )
	# --------- Frame 106 For FileMatch: Line 2
	t19_frame106 = m_tkntr.Frame( t19_tab)
	t19_frame106.pack( fill = m_tkntr.X)
	# --------- Frame 11 Settings Line 2 Deletions
	t19_frame11 = m_tkntr.Frame( t19_tab)
	t19_frame11.pack( fill = m_tkntr.X)
	#
	t19_chkvr_cngrnt_strngnt = m_tkntr.IntVar(value=1)
	t19_chkbx_cngrnt_strngnt = m_tkntr.Checkbutton( t19_frame11, text="Do Stringent Congruent", variable=t19_chkvr_cngrnt_strngnt)
	t19_chkbx_cngrnt_strngnt.pack(side=m_tkntr.LEFT, anchor=m_tkntr.W, padx=tk_padx(), pady=tk_pady() ) # expand=m_tkntr.YES, 
	#t19_chkbx_cngrnt_strngnt.bind('<ButtonRelease>',  lambda x: t19_check_riskiness() )
	# checkbox for reporting stringent match failures
	t19_chkvr_cngrnt_goods_strngnt = m_tkntr.IntVar(value=1)
	t19_chkbx_cngrnt_goods_strngnt = m_tkntr.Checkbutton( t19_frame11, text="Report Stringent Successes", variable=t19_chkvr_cngrnt_goods_strngnt)
	t19_chkbx_cngrnt_goods_strngnt.pack(side=m_tkntr.LEFT, anchor=m_tkntr.W, padx=tk_padx(), pady=tk_pady() ) # expand=m_tkntr.YES, 
	# checkbox for reporting stringent match failures
	t19_chkvr_cngrnt_fails_strngnt = m_tkntr.IntVar(value=1)
	t19_chkbx_cngrnt_fails_strngnt = m_tkntr.Checkbutton( t19_frame11, text="Report Stringent Failures", variable=t19_chkvr_cngrnt_fails_strngnt)
	t19_chkbx_cngrnt_fails_strngnt.pack(side=m_tkntr.LEFT, anchor=m_tkntr.W, padx=tk_padx(), pady=tk_pady() ) # expand=m_tkntr.YES, 
	# Match group checking order selection
	# - label
	t19_label_checking_order = m_tkntr.Label( t19_frame11, text="Matched group checking order:") 
	t19_label_checking_order.pack( side= m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() )
	# - checkbox
	t19_cb_EnumDelOrder = PairTupleCombobox( t19_frame11, apltry.EnumDelOrder_LstPairTpls(), apltry.EnumDelOrder_Default(), state="readonly", width = 20) 
	t19_cb_EnumDelOrder.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() )
	t19_cb_EnumDelOrder.bind("<<ComboboxSelected>>", lambda x: t19_on_EnumDelOrder_combobox_release())
	# label to warn when check order makes no sense
	t19_label_re_EnumDelOrder = m_tkntr.Label( t19_frame11, text="has viability?") 
	t19_label_re_EnumDelOrder.pack( side= m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() )
	# now trigger the setting of the check boxes to match the combobox
	t19_on_defmod_combobox_release()
	# --------- Frame 12 Action button
	t19_frame12 = m_tkntr.Frame( t19_tab)
	t19_frame12.pack( fill = m_tkntr.X)
	#
	t19_button_prunatry_text = m_tkntr.StringVar()
	t19_button_prunatry_text.set( Strng_RunProcess() )
	t19_button_prunatry = m_tkntr.Button( t19_frame12, textvariable=t19_button_prunatry_text, command=t19_on_button_veritry)
	t19_button_prunatry.pack( side=m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	# warning label
	t19_label_re_warning = m_tkntr.Label( t19_frame12, text=":")
	t19_label_re_warning.pack( side= m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() )
	# status label
	t19_label_prunatry_status = m_tkntr.Label( t19_frame12, text="_", fg = "blue") # , width=60
	t19_label_prunatry_status.pack( side=m_tkntr.RIGHT, padx=tk_padx(), pady=tk_pady() )
	#
	# ========== Make a tab for: Summatry
	t05_tab = m_tkntr.Frame(nb)
	nb.add( t05_tab, text="Summatry")
	# --------- Frame 0 Folder list heading
	t05_frame0 = m_tkntr.Frame( t05_tab, bg=colour_bg_neutral())
	t05_frame0.pack( fill = m_tkntr.X)
	#
	t05_label_tab_heading = m_tkntr.Label( t05_frame0, text="Summarise the contents of a list of folders.", bg=colour_bg_neutral())
	t05_label_tab_heading.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	# --------- Frame 1 Folder list entry first line
	t05_frame1 = m_tkntr.Frame( t05_tab, bg=colour_bg_summatry())
	t05_frame1.pack( fill = m_tkntr.X)
	#
	t05_label_heading_folderlist = m_tkntr.Label( t05_frame1, text="PATHS", bg=colour_bg_summatry())
	t05_label_heading_folderlist.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	#
	t05_label_browse_folder = m_tkntr.Label( t05_frame1, text="Enter path", bg=colour_bg_summatry())
	t05_label_browse_folder.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	t05_label_browse_folder.bind('<Button-3>',  lambda x: t05_label_folder_on_rclick_clear() ) # because <Button-3> is the Tkinter code for a right-click
	# 
	t05_var_entry_folder = m_tkntr.StringVar()
	t05_entry_folder = m_tkntr.Entry( t05_frame1, textvariable=t05_var_entry_folder)
	t05_entry_folder.pack( side = m_tkntr.LEFT, fill = m_tkntr.X, expand=1, padx=tk_padx(), pady=tk_pady() ) # 
	#
	t05_button_browse_folderlist = m_tkntr.Button( t05_frame1, text=Strng_Browse(), command=t05_on_button_browse_folder )
	t05_button_browse_folderlist.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	#
	t05_button_add_folderlist = m_tkntr.Button( t05_frame1, text=Strng_AddToList(), command=t05_on_button_add_folder )
	t05_button_add_folderlist.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	#
	t05_button_add_subfolders = m_tkntr.Button( t05_frame1, text=Strng_AddSubs(), command=t05_on_button_add_subfolders )
	t05_button_add_subfolders.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	#  layout
	# --------- Frame 2 Folder list listbox
	t05_frame2 = m_tkntr.Frame( t05_tab, bg=colour_bg_summatry())
	t05_frame2.pack( fill = m_tkntr.BOTH, expand=True)
	#
	t05_lb_folderlist = m_tkntr.Listbox( t05_frame2, height=2 )
	t05_lb_folderlist.pack( side = m_tkntr.LEFT, fill=m_tkntr.BOTH, expand=True, padx=tk_padx(), pady=tk_pady() )
	t05_lb_folderlist.bind('<Double-Button>',  lambda x: t05_on_dblclck_selected_folderlist() )
	if __debug__ : # declare this to be a Settings object
		Set_ESD_EMSA( apltry.EnumSettingDef.SmmtryLstPaths, EnumMetaSettingAspect.WidgetObject, t05_lb_folderlist)
	# --------- Frame 3 Folder list listbox editing buttons
	t05_frame3 = m_tkntr.Frame( t05_tab, bg=colour_bg_summatry())
	t05_frame3.pack( fill = m_tkntr.X)
	#
	t05_label_folderlist_selection = m_tkntr.Label( t05_frame3, text="Selected path:", bg=colour_bg_summatry() )
	t05_label_folderlist_selection.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	#
	t05_button_remove_selected_folderlist = m_tkntr.Button( t05_frame3, text="Remove", command=t05_on_button_remove_selected_folderlist)
	t05_button_remove_selected_folderlist.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	#
	t05_label_folderlist_all = m_tkntr.Label( t05_frame3, text="All paths:", bg=colour_bg_summatry() )
	t05_label_folderlist_all.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	#
	t05_button_remove_all_folderlist = m_tkntr.Button( t05_frame3, text="Clear", command=t05_on_button_remove_all_folderlist)
	t05_button_remove_all_folderlist.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	#
	t05_button_check_list_clashes_folderlist = m_tkntr.Button( t05_frame3, text="Check clashes", command=t05_on_button_check_list_clashes_folderlist)
	t05_button_check_list_clashes_folderlist.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	#
	t05_chkvr_autoclashcheck_paths = m_tkntr.IntVar()
	t05_chkvr_autoclashcheck_paths.set(1)
	t05_chkbx_autoclashcheck_folderlists = m_tkntr.Checkbutton(t05_frame3, text="Check as added", \
		variable=t05_chkvr_autoclashcheck_paths, command = t05_on_chkbx_autoclashcheck_paths)
	t05_chkbx_autoclashcheck_folderlists.pack(side=m_tkntr.LEFT, anchor=m_tkntr.W, expand=m_tkntr.YES, padx=tk_padx(), pady=tk_pady() )
	#
	t05_label_alert_folderlist = m_tkntr.Label( t05_frame3, text="_", fg = "blue", bg=colour_bg_summatry()) # , font=('italic')
	t05_label_alert_folderlist.pack( side = m_tkntr.RIGHT, padx=tk_padx(), pady=tk_pady() )
	# --------- Frame 8 Action button for Summatry
	t05_frame8 = m_tkntr.Frame( t05_tab, bg=colour_bg_summary() )
	t05_frame8.pack( fill = m_tkntr.X)
	# Settings label
	t05_label_checkfor = m_tkntr.Label( t05_frame8, text="General summary:", bg=colour_bg_summary() )
	t05_label_checkfor.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) # 
	# Button
	t05_button_action_var = m_tkntr.StringVar()
	t05_button_action_var.set( Strng_RunProcess() )
	t05_button_action = m_tkntr.Button( t05_frame8, textvariable=t05_button_action_var, command=t05_on_button_summatry)
	t05_button_action.pack( side=m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	# Status
	t05_label_action_status = m_tkntr.Label( t05_frame8, text="_", fg = "blue", bg=colour_bg_summary() ) # , width=60
	t05_label_action_status.pack( side=m_tkntr.RIGHT, padx=tk_padx(), pady=tk_pady() )
	# --------- Frame 9 Controls and Action button for CheckFor
	t05_frame9 = m_tkntr.Frame( t05_tab, bg=colour_bg_checkfor() )
	t05_frame9.pack( fill = m_tkntr.X)
	# Settings label
	t05_label_checkfor = m_tkntr.Label( t05_frame9, text="Check filenames for problem DOS/Windows:", bg=colour_bg_checkfor() )
	t05_label_checkfor.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) # 
	t05_chkvr_checkfor_badwinfname = m_tkntr.IntVar()
	t05_chkvr_checkfor_badwinfname.set(1)
	t05_chkbx_checkfor_badwinfname = m_tkntr.Checkbutton(t05_frame9, text="names", \
		variable=t05_chkvr_checkfor_badwinfname)
	t05_chkbx_checkfor_badwinfname.pack( side=m_tkntr.LEFT, anchor=m_tkntr.W, padx=tk_padx(), pady=tk_pady() )
	if __debug__ : # declare this to be a Settings object
		Set_ESD_EMSA( apltry.EnumSettingDef.SmmtryDoMsName, EnumMetaSettingAspect.WidgetObject, t05_chkvr_checkfor_badwinfname)
	#
	t05_chkvr_checkfor_badwinchars = m_tkntr.IntVar()
	t05_chkvr_checkfor_badwinchars.set(1)
	t05_chkbx_checkfor_badwinchars = m_tkntr.Checkbutton(t05_frame9, text="characters", \
		variable=t05_chkvr_checkfor_badwinchars)
	t05_chkbx_checkfor_badwinchars.pack( side=m_tkntr.LEFT, anchor=m_tkntr.W, padx=tk_padx(), pady=tk_pady() )
	if __debug__ : # declare this to be a Settings object
		Set_ESD_EMSA( apltry.EnumSettingDef.SmmtryDoMsChar, EnumMetaSettingAspect.WidgetObject, t05_chkvr_checkfor_badwinchars)
	# Button
	t05_button_checkfor_var = m_tkntr.StringVar()
	t05_button_checkfor_var.set( Strng_RunProcess() )
	t05_button_checkfor = m_tkntr.Button( t05_frame9, textvariable=t05_button_checkfor_var, command=t05_on_button_checkfor)
	t05_button_checkfor.pack( side=m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	# Status
	t05_label_checkfor_status = m_tkntr.Label( t05_frame9, text="_", fg = "blue", bg=colour_bg_checkfor() ) # , width=60
	t05_label_checkfor_status.pack( side=m_tkntr.RIGHT, padx=tk_padx(), pady=tk_pady() )
	# --------- Frame 10 Controls and Action button for Counts By File Extension
	t05_frame10 = m_tkntr.Frame( t05_tab, bg=colour_bg_countsbfe() )
	t05_frame10.pack( fill = m_tkntr.X)
	# Label 
	t05_label_countsbfe = m_tkntr.Label( t05_frame10, text="Counts by Filename Extensions:", bg=colour_bg_countsbfe() )
	t05_label_countsbfe.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) # 
	# Button
	t05_button_countsbfe_var = m_tkntr.StringVar()
	t05_button_countsbfe_var.set( Strng_RunProcess() )
	t05_button_countsbfe = m_tkntr.Button( t05_frame10, textvariable=t05_button_countsbfe_var, command=t05_on_button_countsbfe)
	t05_button_countsbfe.pack( side=m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	# Status
	t05_label_countsbfe_status = m_tkntr.Label( t05_frame10, text="_", fg = "blue", bg=colour_bg_countsbfe() ) # , width=60
	t05_label_countsbfe_status.pack( side=m_tkntr.RIGHT, padx=tk_padx(), pady=tk_pady() )
	#
	# ========== Make a tab for: Cartulatry
	t15_tab = m_tkntr.Frame(nb)
	nb.add( t15_tab, text="Cartulatry")
	# --------- Frame 0 Folder list heading
	t15_frame0 = m_tkntr.Frame( t15_tab, bg=colour_bg_neutral())
	t15_frame0.pack( fill = m_tkntr.X)
	#
	t15_label_heading_folderlist = m_tkntr.Label( t15_frame0, text="Collect directory information from multiple places", bg=colour_bg_neutral())
	t15_label_heading_folderlist.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	# --------- Frame 1 Folder list entry first line
	t15_frame1 = m_tkntr.Frame( t15_tab, bg=colour_bg_folders_cartulatry())
	t15_frame1.pack( fill = m_tkntr.X)
	#
	t15_label_heading_folderlist = m_tkntr.Label( t15_frame1, text="PATHS", bg=colour_bg_folders_cartulatry())
	t15_label_heading_folderlist.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	#
	t15_label_browse_folder = m_tkntr.Label( t15_frame1, text="Enter path", bg=colour_bg_folders_cartulatry())
	t15_label_browse_folder.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	t15_label_browse_folder.bind('<Button-3>',  lambda x: t15_label_folder_on_rclick_clear() ) # because <Button-3> is the Tkinter code for a right-click
	# 
	t15_var_entry_folder = m_tkntr.StringVar()
	t15_entry_folder = m_tkntr.Entry( t15_frame1, textvariable=t15_var_entry_folder)
	t15_entry_folder.pack( side = m_tkntr.LEFT, fill = m_tkntr.X, expand=1, padx=tk_padx(), pady=tk_pady() ) 
	# 
	t15_button_browse_folderlist = m_tkntr.Button( t15_frame1, text=Strng_Browse(), command=t15_on_button_browse_folder)
	t15_button_browse_folderlist.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	#
	t15_button_add_folderlist = m_tkntr.Button( t15_frame1, text=Strng_AddToList(), command=t15_on_button_add_folder)
	t15_button_add_folderlist.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	#
	t15_button_add_subfolders = m_tkntr.Button( t15_frame1, text=Strng_AddSubs(), command=t15_on_button_add_subfolders)
	t15_button_add_subfolders.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	# --------- Frame 2 Folder list listbox
	t15_frame2 = m_tkntr.Frame( t15_tab, bg=colour_bg_folders_cartulatry())
	t15_frame2.pack( fill = m_tkntr.BOTH, expand=True)
	#
	t15_lb_folderlist = m_tkntr.Listbox( t15_frame2, height=2 )
	t15_lb_folderlist.pack( side = m_tkntr.LEFT, fill=m_tkntr.BOTH, expand=True, padx=tk_padx(), pady=tk_pady() )
	t15_lb_folderlist.bind('<Double-Button>',  lambda x: t15_on_dblclck_selected_folderlist() )
	# --------- Frame 3 Folder list listbox editing buttons
	t15_frame3 = m_tkntr.Frame( t15_tab, bg=colour_bg_folders_cartulatry())
	t15_frame3.pack( fill = m_tkntr.X)
	#
	t15_label_folderlist_selection = m_tkntr.Label( t15_frame3, text="Selected path:", bg=colour_bg_folders_cartulatry() )
	t15_label_folderlist_selection.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	#
	t15_button_remove_selected_folderlist = m_tkntr.Button( t15_frame3, text="Remove", command=t15_on_button_remove_selected_folderlist)
	t15_button_remove_selected_folderlist.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	#
	t15_label_folderlist_all = m_tkntr.Label( t15_frame3, text="All paths:", bg=colour_bg_folders_cartulatry() )
	t15_label_folderlist_all.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	#
	t15_button_remove_all_folderlist = m_tkntr.Button( t15_frame3, text="Clear", command=t15_on_button_remove_all_folderlist)
	t15_button_remove_all_folderlist.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	#
	t15_button_check_list_clashes_folderlist = m_tkntr.Button( t15_frame3, text="Check clashes", command=t15_on_button_check_list_clashes_folderlist)
	t15_button_check_list_clashes_folderlist.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	#
	t15_chkvr_autoclashcheck_paths = m_tkntr.IntVar()
	t15_chkvr_autoclashcheck_paths.set(1)
	t15_chkbx_autoclashcheck_folderlists = m_tkntr.Checkbutton(t15_frame3, text="Check as added", \
		variable=t15_chkvr_autoclashcheck_paths, command = t15_on_chkbx_autoclashcheck_paths)
	t15_chkbx_autoclashcheck_folderlists.pack(side=m_tkntr.LEFT, anchor=m_tkntr.W, expand=m_tkntr.YES, padx=tk_padx(), pady=tk_pady() )
	#
	t15_label_alert_folderlist = m_tkntr.Label( t15_frame3, text="_", fg = "blue", bg=colour_bg_folders_cartulatry()) # , font=('italic')
	t15_label_alert_folderlist.pack( side = m_tkntr.RIGHT, padx=tk_padx(), pady=tk_pady() )
	# --------- Frame 5 Both types of Exclusion matches
	t15_frame5 = m_tkntr.Frame( t15_tab, bg=colour_bg_cartulatry_exclusions() ) 
	t15_frame5.pack( fill = m_tkntr.BOTH, expand=True)
	# --------- Frame 5 Part 0 for the Label
	t15_frame5_0 = m_tkntr.Frame( t15_frame5, bg=colour_bg_cartulatry_exclusions() )
	t15_frame5_0.pack( fill = m_tkntr.X )
	#
	t15_label_exclusions = m_tkntr.Label( t15_frame5_0, text="Exclusions (not yet implemented, just a dummy GUI)", bg=colour_bg_cartulatry_exclusions() )
	t15_label_exclusions.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	# --------- Frame 5 Part 1 for both of the two exclusion lists
	t15_frame5_1 = m_tkntr.Frame( t15_frame5, bg=colour_bg_cartulatry_exclusions() )
	t15_frame5_1.pack( fill= m_tkntr.BOTH, expand= True )
	#
	# --------- Frame for the File exclusion matches
	t15_frame5_A = m_tkntr.Frame( t15_frame5_1, bg=colour_bg_cartulatry_exclude_files() )
	t15_frame5_A.pack( fill= m_tkntr.BOTH, expand= True, side= m_tkntr.LEFT)
	#
	# --------- Frame 5_1 exclusion list entry first line
	t15_frame5_A_1 = m_tkntr.Frame( t15_frame5_A, bg=colour_bg_cartulatry_exclude_files())
	t15_frame5_A_1.pack( fill = m_tkntr.X)
	#
	t15_label_file_exclusions = m_tkntr.Label( t15_frame5_A_1, text="File Exclusions", bg=colour_bg_cartulatry_exclude_files())
	t15_label_file_exclusions.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	#
	t15_var_entry_file_exclusion = m_tkntr.StringVar()
	t15_entry_file_exclusion = m_tkntr.Entry( t15_frame5_A_1, textvariable=t15_var_entry_file_exclusion )
	t15_entry_file_exclusion.pack( side = m_tkntr.LEFT, fill = m_tkntr.X, expand=1, padx=tk_padx(), pady=tk_pady() ) 
	# 
	t15_button_add_file_exclusion = m_tkntr.Button( t15_frame5_A_1, text=Strng_AddToList(), command= t15_on_button_add_file_exclusion)
	t15_button_add_file_exclusion.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	# --------- Frame 5_2 exclusion list listbox
	t15_frame5_A_2 = m_tkntr.Frame( t15_frame5_A, bg=colour_bg_cartulatry_exclude_files())
	t15_frame5_A_2.pack( fill = m_tkntr.BOTH, expand=True)
	#
	t15_lb_file_exclusions = m_tkntr.Listbox( t15_frame5_A_2, height=2 )
	t15_lb_file_exclusions.pack( side = m_tkntr.LEFT, fill=m_tkntr.BOTH, expand=True, padx=tk_padx(), pady=tk_pady() )
	t15_lb_file_exclusions.bind('<Double-Button>',  lambda x: t15_on_dblclck_selected_file_exclusions() )
	# --------- Frame 5_3 exclusion list listbox editing buttons
	t15_frame5_A_3 = m_tkntr.Frame( t15_frame5_A, bg=colour_bg_cartulatry_exclude_files())
	t15_frame5_A_3.pack( fill = m_tkntr.X)
	#
	t15_label_file_exclusions_selection = m_tkntr.Label( t15_frame5_A_3, text="Selected:", bg=colour_bg_cartulatry_exclude_files() )
	t15_label_file_exclusions_selection.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	t15_button_remove_selected_file_exclusion = m_tkntr.Button( t15_frame5_A_3, text="Remove", command= t15_on_button_remove_selected_file_exclusion )
	t15_button_remove_selected_file_exclusion.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	#
	t15_label_file_exclusions_all = m_tkntr.Label( t15_frame5_A_3, text="All :", bg=colour_bg_cartulatry_exclude_files() )
	t15_label_file_exclusions_all.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	#
	t15_button_remove_all_file_exclusions = m_tkntr.Button( t15_frame5_A_3, text="Clear", command= t15_on_button_remove_all_file_exclusions )
	t15_button_remove_all_file_exclusions.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	#
	# Apply exclusion strings to the pull path
	t15_chkvr_fullpath_file_exclusions = m_tkntr.IntVar()
	t15_chkvr_fullpath_file_exclusions.set(0)
	t15_chkbx_fullpath_file_exclusions = m_tkntr.Checkbutton( t15_frame5_A_3, text="Apply to full path:", variable=t15_chkvr_fullpath_file_exclusions )
	t15_chkbx_fullpath_file_exclusions.pack( side=m_tkntr.LEFT, anchor=m_tkntr.W, padx=tk_padx(), pady=tk_pady() )
	# 
	t15_label_alert_file_exclusions = m_tkntr.Label( t15_frame5_A_3, text="_", fg = "blue", bg=colour_bg_cartulatry_exclude_files()) # , font=('italic')
	t15_label_alert_file_exclusions.pack( side = m_tkntr.RIGHT, padx=tk_padx(), pady=tk_pady() )
	#
	# --------- Frame for the Folder exclusion matches
	t15_frame5_B = m_tkntr.Frame( t15_frame5_1, bg=colour_bg_cartulatry_exclude_folds() )
	t15_frame5_B.pack( fill= m_tkntr.BOTH, expand= True, side= m_tkntr.LEFT)
	# --------- Frame 5_1 exclusion list entry first line
	t15_frame5_B_1 = m_tkntr.Frame( t15_frame5_B, bg=colour_bg_cartulatry_exclude_folds())
	t15_frame5_B_1.pack( fill = m_tkntr.X)
	#
	t15_label_fold_exclusions = m_tkntr.Label( t15_frame5_B_1, text="Folder Exclusions", bg=colour_bg_cartulatry_exclude_folds())
	t15_label_fold_exclusions.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	#
	t15_var_entry_fold_exclusion = m_tkntr.StringVar()
	t15_entry_fold_exclusion = m_tkntr.Entry( t15_frame5_B_1, textvariable=t15_var_entry_fold_exclusion )
	t15_entry_fold_exclusion.pack( side = m_tkntr.LEFT, fill = m_tkntr.X, expand=1, padx=tk_padx(), pady=tk_pady() ) 
	# 
	t15_button_add_fold_exclusion = m_tkntr.Button( t15_frame5_B_1, text=Strng_AddToList(), command= t15_on_button_add_fold_exclusion)
	t15_button_add_fold_exclusion.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	# --------- Frame 5_2 exclusion list listbox
	t15_frame5_B_2 = m_tkntr.Frame( t15_frame5_B, bg=colour_bg_cartulatry_exclude_folds())
	t15_frame5_B_2.pack( fill = m_tkntr.BOTH, expand=True)
	#
	t15_lb_fold_exclusions = m_tkntr.Listbox( t15_frame5_B_2, height=2 )
	t15_lb_fold_exclusions.pack( side = m_tkntr.LEFT, fill=m_tkntr.BOTH, expand=True, padx=tk_padx(), pady=tk_pady() )
	t15_lb_fold_exclusions.bind('<Double-Button>',  lambda x: t15_on_dblclck_selected_fold_exclusions() )
	# --------- Frame 5_3 exclusion list listbox editing buttons
	t15_frame5_B_3 = m_tkntr.Frame( t15_frame5_B, bg=colour_bg_cartulatry_exclude_folds())
	t15_frame5_B_3.pack( fill = m_tkntr.X)
	#
	t15_label_fold_exclusions_selection = m_tkntr.Label( t15_frame5_B_3, text="Selected:", bg=colour_bg_cartulatry_exclude_folds() )
	t15_label_fold_exclusions_selection.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	t15_button_remove_selected_fold_exclusion = m_tkntr.Button( t15_frame5_B_3, text="Remove", command= t15_on_button_remove_selected_fold_exclusion )
	t15_button_remove_selected_fold_exclusion.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	#
	t15_label_fold_exclusions_all = m_tkntr.Label( t15_frame5_B_3, text="All :", bg=colour_bg_cartulatry_exclude_folds() )
	t15_label_fold_exclusions_all.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	#
	t15_button_remove_all_fold_exclusions = m_tkntr.Button( t15_frame5_B_3, text="Clear", command= t15_on_button_remove_all_fold_exclusions )
	t15_button_remove_all_fold_exclusions.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	#
	# Apply exclusion strings to the pull path
	t15_chkvr_fullpath_fold_exclusions = m_tkntr.IntVar()
	t15_chkvr_fullpath_fold_exclusions.set(0)
	t15_chkbx_fullpath_fold_exclusions = m_tkntr.Checkbutton( t15_frame5_B_3, text="Apply to full path:", variable=t15_chkvr_fullpath_fold_exclusions )
	t15_chkbx_fullpath_fold_exclusions.pack( side=m_tkntr.LEFT, anchor=m_tkntr.W, padx=tk_padx(), pady=tk_pady() )
	# 
	t15_label_alert_fold_exclusions = m_tkntr.Label( t15_frame5_B_3, text="_", fg = "blue", bg=colour_bg_cartulatry_exclude_folds()) # , font=('italic')
	t15_label_alert_fold_exclusions.pack( side = m_tkntr.RIGHT, padx=tk_padx(), pady=tk_pady() )

	# --------- Frame 6 Controls Part 2
	t15_frame6 = m_tkntr.Frame( t15_tab, bg=colour_bg_cartulatry() )
	t15_frame6.pack( fill = m_tkntr.X)
	# e.g. ignore zero length files
	# IgnoreZeroLengthFiles
	t15_chkvr_IgnoreZeroLengthFiles = m_tkntr.IntVar()
	t15_chkvr_IgnoreZeroLengthFiles.set(0)
	t15_chkbx_IgnoreZeroLengthFiles = m_tkntr.Checkbutton( t15_frame6, text="Ignore Zero Length Files", variable=t15_chkvr_IgnoreZeroLengthFiles)
	t15_chkbx_IgnoreZeroLengthFiles.pack( side=m_tkntr.LEFT, anchor=m_tkntr.W, padx=tk_padx(), pady=tk_pady() )
	# --------- Frame 4 Controls Part 1 
	t15_frame4 = m_tkntr.Frame( t15_tab, bg=colour_bg_cartulatry() )
	t15_frame4.pack( fill = m_tkntr.X)
	#
	t15_label_run_name = m_tkntr.Label( t15_frame4, text="Run Name:", bg=colour_bg_cartulatry() )
	t15_label_run_name.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	# 
	t15_var_run_name = m_tkntr.StringVar()
	t15_run_name = m_tkntr.Entry( t15_frame4, textvariable=t15_var_run_name)
	t15_run_name.pack( side = m_tkntr.LEFT, fill = m_tkntr.X, expand=1, padx=tk_padx(), pady=tk_pady() ) 
	#
	t15_label_run_explain = m_tkntr.Label( t15_frame4, text="This will be stored as a description.", bg=colour_bg_cartulatry() )
	t15_label_run_explain.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	# --------- Frame 10 Action button for Cartulatry
	t15_frame10 = m_tkntr.Frame( t15_tab)
	t15_frame10.pack( fill = m_tkntr.X)
	# action button
	t15_button_action_var = m_tkntr.StringVar()
	t15_button_action_var.set( Strng_RunProcess() )
	t15_button_action = m_tkntr.Button( t15_frame10, textvariable=t15_button_action_var, command=t15_on_button_cartulatry)
	t15_button_action.pack( side=m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	# status label at right hand side
	t15_label_action_status = m_tkntr.Label( t15_frame10, text="_", fg = "blue" ) # , width=60
	t15_label_action_status.pack( side=m_tkntr.RIGHT, padx=tk_padx(), pady=tk_pady() )
	#
	# ========== Make a tab for: Settings
	t06_tab = m_tkntr.Frame(nb)
	nb.add( t06_tab, text="Settings")
	# Make a sub-notebook i.e. tabbed interface
	t06_nb = m_tkntr_ttk.Notebook( t06_tab)
	t06_nb.pack( expand=1, fill=m_tkntr.BOTH)
	# ~~~~~~~~~~ Make 1st subtab = Discard Matches
	t06_tab_1 = m_tkntr.Frame( t06_nb)
	t06_nb.add( t06_tab_1, text="Discard Markers")
	# --------- Frame 01 Discard Markers heading
	t06_t1_frame01 = m_tkntr.Frame( t06_tab_1, bg=colour_bg_config())
	t06_t1_frame01.pack( fill = m_tkntr.X)
	t06_t1_label_heading_malname = m_tkntr.Label( t06_t1_frame01, text="Discard Marker Strings", bg=colour_bg_config())
	t06_t1_label_heading_malname.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) # 
	t06_t1_label_malname_list_alert = m_tkntr.Label( t06_t1_frame01, text="_", fg = "blue", bg=colour_bg_config()) 
	t06_t1_label_malname_list_alert.pack( side = m_tkntr.RIGHT, padx=tk_padx(), pady=tk_pady() ) #
	# --------- Frame 02 Discard Markers entry first line
	t06_t1_frame02 = m_tkntr.Frame( t06_tab_1, bg=colour_bg_config())
	t06_t1_frame02.pack( fill = m_tkntr.X)
	t06_t1_label_browse_malname = m_tkntr.Label( t06_t1_frame02, text="Discard Marker Candidate", bg=colour_bg_config())
	t06_t1_label_browse_malname.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) # 
	t06_t1_var_malname = m_tkntr.StringVar()
	t06_t1_entry_malname = m_tkntr.Entry( t06_t1_frame02, textvariable=t06_t1_var_malname)
	t06_t1_entry_malname.pack( side = m_tkntr.LEFT, fill = m_tkntr.X, expand=1, padx=tk_padx(), pady=tk_pady() ) # 
	t06_t1_button_add_malname = m_tkntr.Button( t06_t1_frame02, text=Strng_AddToList(), command=t06_t1_on_button_add_malnamelist )
	t06_t1_button_add_malname.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	# --------- Frame 03 Discard Markers listbox
	t06_t1_frame03 = m_tkntr.Frame( t06_tab_1, bg=colour_bg_config())
	t06_t1_frame03.pack( fill = m_tkntr.BOTH, expand=True)
	t06_t1_lb_malname = m_tkntr.Listbox( t06_t1_frame03, height=2 )
	t06_t1_on_button_restore_defaults()
	t06_t1_lb_malname.pack( side = m_tkntr.LEFT, fill=m_tkntr.BOTH, expand=True, padx=tk_padx(), pady=tk_pady() )
	t06_t1_lb_malname.bind('<Double-Button>',  lambda x: on_dblclck_selected_malnamelist() )
	if __debug__ : # declare this to be a Settings object || 
		Set_ESD_EMSA( apltry.EnumSettingDef.SttngLstDiscardNameMarkers, EnumMetaSettingAspect.WidgetObject, t06_t1_lb_malname)

	# --------- Frame 04 Discard Markers listbox editing buttons
	t06_t1_frame04 = m_tkntr.Frame( t06_tab_1, bg=colour_bg_config())
	t06_t1_frame04.pack( fill = m_tkntr.X)
	# define
	t06_t1_label_malname_selection = m_tkntr.Label( t06_t1_frame04, text="Selected match:", bg=colour_bg_config())
	t06_t1_button_remove_selected_malname = m_tkntr.Button( t06_t1_frame04, text="Remove", command=t06_t1_on_button_remove_selected_malnamelist )
	t06_t1_label_malname_all = m_tkntr.Label( t06_t1_frame04, text="All matches:", bg=colour_bg_config())
	t06_t1_button_remove_all_malname = m_tkntr.Button( t06_t1_frame04, text="Clear", command=t06_t1_on_button_remove_all_malnamelist )
	t06_t1_button_check_list_clashes_malname = m_tkntr.Button( t06_t1_frame04, text="Check clashes", command=t06_t1_on_button_check_list_clashes_malnamelist )
	t06_t1_chkvr_autoclashcheck_malname = m_tkntr.IntVar()
	t06_t1_chkvr_autoclashcheck_malname.set(1)
	t06_t1_chkbx_autoclashcheck_malname = m_tkntr.Checkbutton(t06_t1_frame04, text="Check as added", \
		variable=t06_t1_chkvr_autoclashcheck_malname, command = on_chkbx_autoclashcheck_malnamelist )
	t06_t1_button_restore_defaults = m_tkntr.Button( t06_t1_frame04, text="Restore", command=t06_t1_on_button_restore_defaults )
	# position
	t06_t1_label_malname_selection.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	t06_t1_button_remove_selected_malname.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	t06_t1_label_malname_all.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	t06_t1_button_remove_all_malname.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	t06_t1_button_check_list_clashes_malname.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	t06_t1_chkbx_autoclashcheck_malname.pack(side=m_tkntr.LEFT, anchor=m_tkntr.W, expand=m_tkntr.YES)
	t06_t1_button_restore_defaults.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	# ~~~~~~~~~~ Make 2nd subtab = Congruency 
	t06_tab_2 = m_tkntr.Frame( t06_nb)
	t06_nb.add( t06_tab_2, text="Congruencies")

	# ~~~~~~~~~~ Make 3rd subtab = Fingerprinting
	t06_tab_3 = m_tkntr.Frame( t06_nb)
	t06_nb.add( t06_tab_3, text="Fingerprinting")
	# --------- Frame 01 
	t06_t3_frame01 = m_tkntr.Frame( t06_tab_3, bg=colour_bg_config())
	t06_t3_frame01.pack( fill = m_tkntr.X)
	t06_t3_label_heading_settings = m_tkntr.Label( t06_t3_frame01, text="File and Folder Fingerprint Options", bg=colour_bg_config())
	t06_t3_label_heading_settings.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) # 
	t06_t3_label_settings_alert = m_tkntr.Label( t06_t3_frame01, text="Note: currently just concept sketches", fg = "blue", bg=colour_bg_config()) 
	t06_t3_label_settings_alert.pack( side = m_tkntr.RIGHT, padx=tk_padx(), pady=tk_pady() ) #
	# --------- Frame 12 Checkboxes
	t06_t3_frame12 = m_tkntr.Frame( t06_tab_3, bg=colour_bg_config())
	t06_t3_frame12.pack( fill = m_tkntr.BOTH)
	# line label
	t06_t3_label_hashing = m_tkntr.Label( t06_t3_frame12, text="Hash file content for:", bg=colour_bg_config()) # , width=10
	t06_t3_label_hashing.pack( side=m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) # , fill=m_tkntr.BOTH, expand=True
	# Hash file content for folders - Matchsubtry
	t06_t3_chkvr_hashfilecontent4supr = m_tkntr.IntVar(value=0) # default is OFF
	t06_t3_chkbx_hashfilecontent4supr = m_tkntr.Checkbutton( t06_t3_frame12, text="Matchsubtry", bg=colour_bg_config(), variable=t06_t3_chkvr_hashfilecontent4supr)
	t06_t3_chkbx_hashfilecontent4supr.pack(side=m_tkntr.LEFT, anchor=m_tkntr.W, expand=m_tkntr.YES, padx=tk_padx(), pady=tk_pady() )
	t06_t3_chkbx_hashfilecontent4supr.configure(state=m_tkntr.DISABLED)
	#t06_t3_chkbx_hashfilecontent4supr.configure(state=m_tkntr.DISABLED)
	#t06_t3_chkbx_hashfilecontent4supr.configure(state=m_tkntr.NORMAL)
	# Hash file content for files - Filedupetry
	t06_t3_chkvr_hashfilecontent4file = m_tkntr.IntVar(value=1) # default is ON
	t06_t3_chkbx_hashfilecontent4file = m_tkntr.Checkbutton( t06_t3_frame12, text="Filedupetry", variable=t06_t3_chkvr_hashfilecontent4file)
	t06_t3_chkbx_hashfilecontent4file.pack(side=m_tkntr.LEFT, anchor=m_tkntr.W, expand=m_tkntr.YES, padx=tk_padx(), pady=tk_pady() )
	t06_t3_chkbx_hashfilecontent4file.configure(state=m_tkntr.DISABLED)
	#t06_t3_chkbx_hashfilecontent4file.configure(state=m_tkntr.NORMAL)
	# ~~~~~~~~~~ Make 4th subtab = Saved Settings
	t06_tab_4 = m_tkntr.Frame( t06_nb, bg=colour_bg_config())
	t06_nb.add( t06_tab_4, text="Saved Settings")
	# --------- Frame 01 Discard Markers heading
	t06_t4_frame01 = m_tkntr.Frame( t06_tab_4, bg=colour_bg_config())
	t06_t4_frame01.pack( fill = m_tkntr.X)
	t06_t4_label_heading_settings = m_tkntr.Label( t06_t4_frame01, text="Settings", bg=colour_bg_config())
	t06_t4_label_heading_settings.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) # 
	#
	# Hash file content for files - Filedupetry
	t06_t4_chkvr_autosave = m_tkntr.IntVar( value= 0) # default is OFF
	t06_t4_chkbx_autosave = m_tkntr.Checkbutton( t06_t4_frame01, text="Autosave", variable=t06_t4_chkvr_autosave)
	t06_t4_chkbx_autosave.pack(side=m_tkntr.LEFT, anchor=m_tkntr.W, expand=m_tkntr.YES, padx=tk_padx(), pady=tk_pady() )
	#t06_t4_chkbx_autosave.configure(state=m_tkntr.DISABLED)
	#t06_t4_chkbx_autosave.configure(state=m_tkntr.NORMAL)
	t06_t4_chkbx_autosave.bind('<ButtonRelease>',  lambda x: t06_t4_chkbx_autosave_on_release() )
	#
	t06_t4_label_settings_alert = m_tkntr.Label( t06_t4_frame01, text="_", fg = "blue", bg=colour_bg_config()) 
	t06_t4_label_settings_alert.pack( side = m_tkntr.RIGHT, padx=tk_padx(), pady=tk_pady() ) #
	# --------- Frame 12 Save settings into file
	t06_t4_frame12 = m_tkntr.Frame( t06_tab_4, bg=colour_bg_config())
	t06_t4_frame12.pack( fill = m_tkntr.X)
	t06_t4_label_browse_saveintofile = m_tkntr.Label( t06_t4_frame12, text="Save settings into file", bg=colour_bg_config())
	t06_t4_label_browse_saveintofile.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	t06_t4_label_browse_saveintofile.bind('<Button-3>',  lambda x: t06_t4_label_savedintofile_on_rclick_clear() ) # because <Button-3> is the Tkinter code for a right-click
	# 
	t06_t4_var_savedintofile = m_tkntr.StringVar()
	t06_t4_entry_savedintofile = m_tkntr.Entry( t06_t4_frame12, textvariable=t06_t4_var_savedintofile)
	t06_t4_entry_savedintofile.pack( side = m_tkntr.LEFT, fill = m_tkntr.X, expand=1, padx=tk_padx(), pady=tk_pady() ) 
	# 
	t06_t4_button_browse_saveintofile = m_tkntr.Button( t06_t4_frame12, text=Strng_Browse(), command=t06_t4_on_button_browse_saveintofile)
	t06_t4_button_browse_saveintofile.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) 
	#
	t06_t4_button_action_saveintofile = m_tkntr.Button( t06_t4_frame12, text="Save", command=t06_t4_on_button_action_saveintofile)
	t06_t4_button_action_saveintofile.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	#
	t06_t4_button_action_saveintodefault = m_tkntr.Button( t06_t4_frame12, text="Save as defaults", command=t06_t4_on_button_action_saveintodefault)
	t06_t4_button_action_saveintodefault.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	#
	t06_t4_button_action_deletedefault = m_tkntr.Button( t06_t4_frame12, text="Delete defaults", command=t06_t4_on_button_action_deletedefault)
	t06_t4_button_action_deletedefault.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	# --------- Frame 13 Load settings from file
	t06_t4_frame13 = m_tkntr.Frame( t06_tab_4, bg=colour_bg_config())
	t06_t4_frame13.pack( fill = m_tkntr.X)
	t06_t4_label_browse_loadfromfile = m_tkntr.Label( t06_t4_frame13, text="Load settings from file", bg=colour_bg_config())
	t06_t4_label_browse_loadfromfile.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) # 
	t06_t4_label_browse_loadfromfile.bind('<Button-3>',  lambda x: t06_t4_label_loadfromfile_on_rclick_clear() ) # because <Button-3> is the Tkinter code for a right-click
	#
	t06_t4_var_loadedfromfile = m_tkntr.StringVar()
	t06_t4_entry_loadedfromfile = m_tkntr.Entry( t06_t4_frame13, textvariable=t06_t4_var_loadedfromfile)
	t06_t4_entry_loadedfromfile.pack( side = m_tkntr.LEFT, fill = m_tkntr.X, expand=1, padx=tk_padx(), pady=tk_pady() ) # 
	#
	t06_t4_button_browse_loadfromfile = m_tkntr.Button( t06_t4_frame13, text=Strng_Browse(), command=t06_t4_on_button_browse_loadfromfile)
	t06_t4_button_browse_loadfromfile.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	#
	t06_t4_button_action_loadfromfile = m_tkntr.Button( t06_t4_frame13, text="Load", command=t06_t4_on_button_action_loadfromfile)
	t06_t4_button_action_loadfromfile.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	#
	t06_t4_button_action_loadfromdefault = m_tkntr.Button( t06_t4_frame13, text="Load saved defaults", command=t06_t4_on_button_action_loadfromdefault)
	t06_t4_button_action_loadfromdefault.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) #
	# --------- Frame 01 Saved Settings heading
	t06_t4_frame01 = m_tkntr.Frame( t06_tab_4, bg=colour_bg_config())
	t06_t4_frame01.pack( fill = m_tkntr.X)
	t06_t4_label_heading_malname = m_tkntr.Label( t06_t4_frame01, text="Settings flags - control which are affected by Load or Save", bg=colour_bg_config())
	t06_t4_label_heading_malname.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) # 
	t06_t4_label_malname_list_alert = m_tkntr.Label( t06_t4_frame01, text="_", fg = "blue", bg=colour_bg_config()) 
	t06_t4_label_malname_list_alert.pack( side = m_tkntr.RIGHT, padx=tk_padx(), pady=tk_pady() ) #
	# --------- Frame 11 buttons to turn all the check boxes on or off
	t06_t4_button_settings_all_off = m_tkntr.Button( t06_t4_frame01, text="Clear all", command=t06_t4_on_button_settings_all_off)
	t06_t4_button_settings_all_onn = m_tkntr.Button( t06_t4_frame01, text="Mark all", command=t06_t4_on_button_settings_all_onn)
	t06_t4_button_settings_all_off.pack( side = m_tkntr.RIGHT, padx=tk_padx(), pady=tk_pady() ) #
	t06_t4_button_settings_all_onn.pack( side = m_tkntr.RIGHT, padx=tk_padx(), pady=tk_pady() ) #
	# --------- Frame 41 Items to have affected by a Save or a Load:
	# A place or list for each setting with a check box
	t06_t4_frame41 = tkntr_VerticalScrolledFrame( t06_tab_4, bg=colour_bg_config())
	t06_t4_frame41.pack( fill = m_tkntr.X, expand=m_tkntr.YES)
	# -- Checkbox set
	t06_t4_dct_frm = {}
	t06_t4_dct_cbv = {}
	t06_t4_dct_cbx = {}
	# for i in range(25) :
	for sk in apltry.EnumSettingDef :
		i = sk.value
		t = apltry.SettingDef_Show( sk )
		t06_t4_dct_frm[i] = m_tkntr.Frame( t06_t4_frame41.interior, bg=colour_bg_config())
		t06_t4_dct_frm[i].pack( fill = m_tkntr.X ) # , padx=tk_padx()
		t06_t4_dct_cbv[i] = m_tkntr.IntVar()
		t06_t4_dct_cbx[i] = m_tkntr.Checkbutton(t06_t4_dct_frm[i], text=t, variable=t06_t4_dct_cbv[i], bg=colour_bg_config() )
		t06_t4_dct_cbx[i].pack(side=m_tkntr.LEFT, anchor=m_tkntr.W, expand=m_tkntr.YES ) # , padx=tk_padx()
	# ~~~~~~~~~~ Make 5th subtab = Logging Settings
	t06_tab_5 = m_tkntr.Frame( t06_nb)
	t06_nb.add( t06_tab_5, text="Logging")
	# a structure for controllable output to multiple files & screen
	# this uses a multifold scheme:
	#   s = Status - in-window list
	#   r = Results - in-window list
	#   p = Process - in-window list
	#   d = Details - file
	#   b = deBug - file
	# --------- Frame 01 Label
	t06_t5_frame01 = m_tkntr.Frame( t06_tab_5, bg=colour_bg_config())
	t06_t5_frame01.pack( fill = m_tkntr.X)
	t06_t5_label_browse_saveintofile = m_tkntr.Label( t06_t5_frame01, text="Log control settings", bg=colour_bg_config())
	t06_t5_label_browse_saveintofile.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) # 
	# --------- Frame 02 Checkboxes
	t06_t5_frame02 = m_tkntr.Frame( t06_tab_5, bg=colour_bg_config())
	t06_t5_frame02.pack( fill = m_tkntr.X)
	# define Checkboxes
	# status
	t06_t5_chkvr_log_status = m_tkntr.IntVar()
	t06_t5_chkbx_log_status = m_tkntr.Checkbutton(t06_t5_frame02, text="Log to Status", bg=colour_bg_config(), \
		command = t06_t5_on_chkbx_log_status, variable=t06_t5_chkvr_log_status)
	t06_t5_chkbx_log_status.pack(side=m_tkntr.LEFT, anchor=m_tkntr.W, expand=m_tkntr.YES, padx=tk_padx(), pady=tk_pady() )
	if __debug__ : # declare this to be a Settings object
		Set_ESD_EMSA( apltry.EnumSettingDef.LggngStatus, EnumMetaSettingAspect.WidgetObject, t06_t5_chkvr_log_status)
	# results
	t06_t5_chkvr_log_result = m_tkntr.IntVar()
	t06_t5_chkbx_log_result = m_tkntr.Checkbutton(t06_t5_frame02, text="Log Results", bg=colour_bg_config(), \
		command = t06_t5_on_chkbx_log_result, variable=t06_t5_chkvr_log_result )  
	t06_t5_chkbx_log_result.pack(side=m_tkntr.LEFT, anchor=m_tkntr.W, expand=m_tkntr.YES, padx=tk_padx(), pady=tk_pady() )
	if __debug__ : # declare this to be a Settings object
		Set_ESD_EMSA( apltry.EnumSettingDef.LggngResults, EnumMetaSettingAspect.WidgetObject, t06_t5_chkvr_log_result)
	# process
	t06_t5_chkvr_log_process = m_tkntr.IntVar()
	t06_t5_chkbx_log_process = m_tkntr.Checkbutton(t06_t5_frame02, text="Log Process", bg=colour_bg_config(), \
		command = t06_t5_on_chkbx_log_process, variable=t06_t5_chkvr_log_process )  
	t06_t5_chkbx_log_process.pack(side=m_tkntr.LEFT, anchor=m_tkntr.W, expand=m_tkntr.YES, padx=tk_padx(), pady=tk_pady() )
	if __debug__ : # declare this to be a Settings object
		Set_ESD_EMSA( apltry.EnumSettingDef.LggngProcess, EnumMetaSettingAspect.WidgetObject, t06_t5_chkvr_log_process)
	# details
	t06_t5_chkvr_log_detail = m_tkntr.IntVar()
	t06_t5_chkbx_log_detail = m_tkntr.Checkbutton(t06_t5_frame02, text="Log Details", \
		variable=t06_t5_chkvr_log_detail, bg=colour_bg_config(), command = t06_t5_on_chkbx_log_detail )
	t06_t5_chkbx_log_detail.pack(side=m_tkntr.LEFT, anchor=m_tkntr.W, expand=m_tkntr.YES, padx=tk_padx(), pady=tk_pady() )
	if __debug__ : # declare this to be a Settings object
		Set_ESD_EMSA( apltry.EnumSettingDef.LggngDetails, EnumMetaSettingAspect.WidgetObject, t06_t5_chkvr_log_detail)
	# debug
	t06_t5_chkvr_log_debug = m_tkntr.IntVar()
	t06_t5_chkbx_log_debug = m_tkntr.Checkbutton(t06_t5_frame02, text="Log for debugging", \
		variable=t06_t5_chkvr_log_debug, bg=colour_bg_config(), command = t06_t5_on_chkbx_log_debug )
	t06_t5_chkbx_log_debug.pack(side=m_tkntr.LEFT, anchor=m_tkntr.W, expand=m_tkntr.YES, padx=tk_padx(), pady=tk_pady() )
	if __debug__ : # declare this to be a Settings object
		Set_ESD_EMSA( apltry.EnumSettingDef.LggngDebug, EnumMetaSettingAspect.WidgetObject, t06_t5_chkvr_log_debug)
	# layout
	# Note: we won't set the state of these checkboxes yet, as not all the variables holding that info exist just yet
	# --------- Frame 03 Label
	t06_t5_frame03 = m_tkntr.Frame( t06_tab_5, bg=colour_bg_config())
	t06_t5_frame03.pack( fill = m_tkntr.X)
	t06_t5_label_proc_ctrl = m_tkntr.Label( t06_t5_frame03, text="Process Run control settings", bg=colour_bg_config())
	t06_t5_label_proc_ctrl.pack( side = m_tkntr.LEFT, padx=tk_padx(), pady=tk_pady() ) # 
	# --------- Frame 04 Checkboxes
	t06_t5_frame04 = m_tkntr.Frame( t06_tab_5, bg=colour_bg_config())
	t06_t5_frame04.pack( fill = m_tkntr.X)
	# define Checkboxes
	# status
	t06_t5_chkvr_thread_status = m_tkntr.IntVar()
	t06_t5_chkbx_thread_status = m_tkntr.Checkbutton(t06_t5_frame04, text="Do not run processes as threads", bg=colour_bg_config(), \
		variable=t06_t5_chkvr_thread_status)
	t06_t5_chkbx_thread_status.pack(side=m_tkntr.LEFT, anchor=m_tkntr.W, expand=m_tkntr.YES, padx=tk_padx(), pady=tk_pady() )
	#
	# ========== Make a tab for: Process Logs
	t07_tab = m_tkntr.Frame(nb)
	nb.add( t07_tab, text="Process Logs")
	# ----------
	t07_st_plogs = m_tkntr_tkst.ScrolledText( t07_tab, wrap= "word" )
	t07_st_plogs.pack( side=m_tkntr.LEFT, fill=m_tkntr.BOTH, expand=True )
	#t07_st_plogs.insert(m_tkntr.INSERT, "GUI logging enabled.")
	#t07_st_plogs.insert(m_tkntr.END, " in ScrolledText")
	# Create textLogger that will use the ScrolledText
	plogs_text_handler = Cls_Lggng_TextHandler( t07_st_plogs )
	# Add the handler to the module logger
	#print("Testing addkey_k_b_handler")
	p_multi_log.addkey_k_b_handler( "p", True, plogs_text_handler )
	p_multi_log.do_logs( "p", "Initiated log connection to ScrolledText for Process.")
	#
	#
	# ========== Make a tab for: Results
	t08_tab = m_tkntr.Frame(nb)
	nb.add( t08_tab, text="Results")
	# ----------
	t08_st_rslts = m_tkntr_tkst.ScrolledText( t08_tab, wrap= "word" )
	t08_st_rslts.pack( side=m_tkntr.LEFT, fill=m_tkntr.BOTH, expand=True )
	#t08_st_rslts.insert(m_tkntr.INSERT, "No results yet!")
	# Create textLogger that will use the ScrolledText
	rslts_text_handler = Cls_Lggng_TextHandler( t08_st_rslts )
	# Add the handler to the module logger
	#print("Testing addkey_k_b_handler")
	p_multi_log.addkey_k_b_handler( "r", True, rslts_text_handler )
	p_multi_log.do_logs( "r", "Initiated log connection to ScrolledText for Results.")
	#
	#
	# ========== Make a tab for: Status
	t09_tab = m_tkntr.Frame(nb)
	nb.add( t09_tab, text="Status")
	# ----------
	t09_st_stts = m_tkntr_tkst.ScrolledText( t09_tab, wrap= "word" )
	t09_st_stts.pack( side=m_tkntr.LEFT, fill=m_tkntr.BOTH, expand=True )
	#t09_st_stts.insert(m_tkntr.INSERT, "No status yet!")
	# Create textLogger that will use the ScrolledText
	if True:
		stts_text_handler = Cls_Lggng_TextHandler_Tkinter_ScrolledText_Limited( t09_st_stts )
	else:
		# alternative is to use the unlimited version 
		# - e.g. when debugging though this adds a memory risk
		stts_text_handler = Cls_Lggng_TextHandler( t09_st_stts )
	# Add the handler to the module logger
	p_multi_log.addkey_k_b_handler( "s", True, stts_text_handler )
	# need to override earlier impossible pickup of this flag
	p_multi_log.do_logs( "s", "Initiated Status logging as a ScrolledText for Status.")
	# ==================================================================
	# Post-Function "Main" section
	# $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
	# ------------------------------------------------------------------
	if __debug__:
		DevelopMode_Actions()
	# ------------------------------------------------------------------
	# $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
	# ========== the "Main" code section for the application window
	# With all the GUI widgets constructed, set the state of the log flags
	# This is just about having them turned on if Python was executed in debugging mode (the default)
	# and turned off if it wasn't.
	t06_t5_update_logging_checkboxes_from_log_flags()
	# --------- Saved Settings -- either AutoSaved or Defaults
	DetectThenProcess_AnySaved_Settings()
	# -------------------------------------------------
	# Now setup and run the background thread that will pick up when processes 
	# have completed and update the GUI elements to indicate so
	i_t = fi_Thread( target=background_loop, args=( [ i_TheRunStati ] ) ) # note that args must be iterable, so while we have only one, it must be a list
	i_t.daemon = True
	i_t.start() # this will terminate when the GUI object closes
	# -------------------------------------------------



def main_gui_interact( p_lst_instructions, p_multi_log ) :
	# Brief:
	# Enact the graphic user interface, allow the user set values and then launch actions.
	# Parameters:
	# Returns:
	# Notes:
	# this acts as a parent holder for the GUI as a window form with tkinter functions
	# Code:
	# setup the log controls
	# prep and launch the GUI
	root = m_tkntr.Tk()
	# toying with Tkinter font arrangements
	if False:
		def_font = m_tkntr_font.nametofont("TkDefaultFont")
		def_font.config( size = 9 )
	elif True :
		# tkinter Fonts list
		#('fixed', 'oemfixed', 'TkDefaultFont', 'TkMenuFont', 'ansifixed', 'systemfixed', 'TkHeadingFont', 'device', 'TkTooltipFont', 'defaultgui', 'TkTextFont', 'ansi', 'TkCaptionFont', 'system', 'TkSmallCaptionFont', 'TkFixedFont', 'TkIconFont')
		tk_fonts = [ 'TkDefaultFont', 'TkMenuFont', 'TkHeadingFont', 'TkTooltipFont', 'TkTextFont', 'TkCaptionFont', 'TkSmallCaptionFont', 'TkFixedFont', 'TkIconFont' ]
		font_defs = {}
		new_fnt_size = 10
		for tkf in tk_fonts :
			font_defs[ tkf ] = m_tkntr_font.nametofont(tkf)
			font_defs[ tkf ].config( size = new_fnt_size )
	root.wm_geometry( "1000x650" )
	root.title(app_show_name())
	# open it as maximised
	i_platform_sys = im_pltfrm.system()
	if i_platform_sys.lower() == "linux" :
		root.attributes('-zoomed', True)
	elif i_platform_sys.lower() == "windows" :
		root.state('zoom')
	else :
		root.state('normal')
		# root.attributes('-fullscreen', True)
	# define the main window with tkinter features
	FoldatryWindow(root, p_multi_log)
	# get the GUI in action
	root.mainloop()
	p_multi_log.do_logs( "pb", "GUI ended." )

def setup_logging() :
	# this uses the custom wrapper module "multilog"
	cmntry.lgff_make_home_log_folder()
	m_log = cmntry.make_multilog_object( app_code_name() )
	return m_log

def command_line_actions( p_lst_instructions, p_multi_log ) :
	if __debug__:
		i_log_default = True
	else :
		i_log_default = False
	# preset that the "s" log goes to standard output
	p_multi_log.addkey_k_b_terminal( "s", i_log_default )
	p_multi_log.do_logs( "srdb", "command_line_actions BEGIN" )
	# have a settings object
	FldtryCLI_Settings = apltry.FoldatrySettings()
	for i_instruction in p_lst_instructions :
		if i_instruction.Instruction == EnumCmdInstruction.LoadSettings :
			if len( i_instruction.FileRef ) > 0 :
				# call loading the settings from loadfilename 
				p_multi_log.do_logs( "srdb", "loading settings from: " + i_instruction.FileRef )
				#print("loading settings from: " + i_instruction.FileRef )
				i_got_ok, i_load_count = FldtryCLI_Settings.LoadSettingsFromFle( i_instruction.FileRef )
				p_multi_log.do_logs( "srdb", "i_got_ok, i_load_count = " + str( i_got_ok) + " & " + str( i_load_count) )
				#print( "i_got_ok, i_load_count = " + str( i_got_ok) + " & " + str( i_load_count) )
				# reset the Logging flags
				new_bool = FldtryCLI_Settings.Get_SettingValue( apltry.EnumSettingDef.LggngStatus )
				p_multi_log.key_set_b( "s", new_bool)
				new_bool = FldtryCLI_Settings.Get_SettingValue( apltry.EnumSettingDef.LggngResults )
				p_multi_log.key_set_b( "r", new_bool)
				new_bool = FldtryCLI_Settings.Get_SettingValue( apltry.EnumSettingDef.LggngProcess )
				p_multi_log.key_set_b( "p", new_bool)
				new_bool = FldtryCLI_Settings.Get_SettingValue( apltry.EnumSettingDef.LggngDetails )
				p_multi_log.key_set_b( "d", new_bool)
				new_bool = FldtryCLI_Settings.Get_SettingValue( apltry.EnumSettingDef.LggngDebug )
				p_multi_log.key_set_b( "b", new_bool)
		elif i_instruction.Instruction == EnumCmdInstruction.DoPrunatry :
			p_multi_log.do_logs( "srdb", "Doing Prunatry.."  )
			i_lst_disposal = FldtryCLI_Settings.Get_SettingValue( apltry.EnumSettingDef.PrntryLstDisposalPaths )
			i_lst_survival = FldtryCLI_Settings.Get_SettingValue( apltry.EnumSettingDef.PrntryLstSurvivalPaths )
			i_do_sbtry_super = FldtryCLI_Settings.Get_SettingValue( apltry.EnumSettingDef.PrntryDoSuperMatch )
			i_do_sbtry_fldrs = FldtryCLI_Settings.Get_SettingValue( apltry.EnumSettingDef.PrntryDoFolderMatch )
			i_do_filedupetry = FldtryCLI_Settings.Get_SettingValue( apltry.EnumSettingDef.PrntryDoFileMatch )
			i_sbtry_hash_super = FldtryCLI_Settings.Get_SettingValue( apltry.EnumSettingDef.PrntryDoMatchStringent )
			i_UseHash = FldtryCLI_Settings.Get_SettingValue( apltry.EnumSettingDef.PrntryFileDoMatchByHash )
			i_UseSize = FldtryCLI_Settings.Get_SettingValue( apltry.EnumSettingDef.PrntryFileDoMatchBySize )
			i_UseName = FldtryCLI_Settings.Get_SettingValue( apltry.EnumSettingDef.PrntryFileDoMatchByName )
			i_UseWhen = FldtryCLI_Settings.Get_SettingValue( apltry.EnumSettingDef.PrntryFileDoMatchByWhen )
			i_FileFlags = cmntry.tpl_FileFlags( i_UseName, i_UseSize, i_UseWhen, i_UseHash)
			i_EnumDelOrder = FldtryCLI_Settings.Get_SettingValue( apltry.EnumSettingDef.PrntryMtchGrpDelOrder )
			i_docongruentbeforedelete = FldtryCLI_Settings.Get_SettingValue( apltry.EnumSettingDef.PrntryDoCongruent )
			i_congruentstringent = FldtryCLI_Settings.Get_SettingValue( apltry.EnumSettingDef.PrntryDoCngrntStringent )
			i_PrntryReportStringentDiffs = FldtryCLI_Settings.Get_SettingValue( apltry.EnumSettingDef.PrntryReportStringentDiffs )
			i_EnumDeleteMode = FldtryCLI_Settings.Get_SettingValue( apltry.EnumSettingDef.PrntryDeletionMode )
			i_is_ok, i_lst_why = prunatry_command_recce( i_lst_survival, i_lst_disposal, \
				i_do_sbtry_super, i_do_sbtry_fldrs, i_do_filedupetry, \
				i_sbtry_hash_super, \
				i_FileFlags, i_EnumDelOrder, \
				i_docongruentbeforedelete, i_congruentstringent, i_EnumDeleteMode )
			if i_is_ok :
				prunatry_command( i_lst_survival, i_lst_disposal, \
					i_do_sbtry_super, i_do_sbtry_fldrs, i_do_filedupetry, \
					i_sbtry_hash_super, \
					i_FileFlags, i_EnumDelOrder, \
					i_docongruentbeforedelete, i_congruentstringent, i_PrntryReportStringentDiffs, i_EnumDeleteMode, PseudoNoInterruptCheck, p_multi_log)
				p_multi_log.do_logs( "srdb", "Did Prunatry!"  )
			else :
				#print( "Did not do Prunatry!" )
				p_multi_log.do_logs( "srdb", "Did not do Prunatry!"  )
				for why in i_lst_why :
					#print( why )
					p_multi_log.do_logs( "srdb", why  )
		elif i_instruction.Instruction == EnumCmdInstruction.DoDefoliatry :
			p_multi_log.do_logs( "srdb", "Doing Defoliatry.."  )
			i_lst_paths = FldtryCLI_Settings.Get_SettingValue( apltry.EnumSettingDef.DfltryLstPaths )
			# fake this bit for now by reading the GUI
			i_UseHash = FldtryCLI_Settings.Get_SettingValue( apltry.EnumSettingDef.DfltryFileDoMatchByHash )
			i_UseSize = FldtryCLI_Settings.Get_SettingValue( apltry.EnumSettingDef.DfltryFileDoMatchBySize )
			i_UseName = FldtryCLI_Settings.Get_SettingValue( apltry.EnumSettingDef.DfltryFileDoMatchByName )
			i_UseWhen = FldtryCLI_Settings.Get_SettingValue( apltry.EnumSettingDef.DfltryFileDoMatchByWhen )
			i_FileFlags = cmntry.tpl_FileFlags( i_UseName, i_UseSize, i_UseWhen, i_UseHash)
			i_use_names = FldtryCLI_Settings.Get_SettingValue( apltry.EnumSettingDef.DfltryDoUseDiscardMarkers )
			i_ignore_zero_len = FldtryCLI_Settings.Get_SettingValue( apltry.EnumSettingDef.DfltryDoIgnoreZeroLength )
			i_EnumKeepWhich = FldtryCLI_Settings.Get_SettingValue( apltry.EnumSettingDef.DfltryFileEnumKeepWhich )
			i_EnumDelOrder = FldtryCLI_Settings.Get_SettingValue( apltry.EnumSettingDef.DfltryMtchGrpDelOrder )
			i_EnumDeleteMode = FldtryCLI_Settings.Get_SettingValue( apltry.EnumSettingDef.DfltryDeletionMode )
			i_docongruentbeforedelete = FldtryCLI_Settings.Get_SettingValue( apltry.EnumSettingDef.DfltryDoCongruent )
			i_congruentstringent = FldtryCLI_Settings.Get_SettingValue( apltry.EnumSettingDef.DfltryDoCngrntStringent ) 
			i_ReportFailuresStringent = FldtryCLI_Settings.Get_SettingValue( apltry.EnumSettingDef.DfltryReportStringentDiffs )
			i_lst_names_bad = FldtryCLI_Settings.Get_SettingValue( apltry.EnumSettingDef.SttngLstDiscardNameMarkers )
			i_is_ok, i_lst_why = defoliatry_command_recce( i_lst_paths, \
					i_FileFlags, \
					i_EnumKeepWhich, i_EnumDelOrder, \
					i_ignore_zero_len, i_use_names, i_lst_names_bad, \
					i_docongruentbeforedelete, i_congruentstringent, i_ReportFailuresStringent, i_ReportFailuresStringent)
			if i_is_ok :
				defoliatry_command( i_lst_paths, \
					i_FileFlags, \
					i_EnumKeepWhich, i_EnumDelOrder, \
					i_ignore_zero_len, i_use_names, i_lst_names_bad, i_docongruentbeforedelete, i_congruentstringent, i_ReportFailuresStringent, i_EnumDeleteMode, p_multi_log)
				p_multi_log.do_logs( "srdb", "Did Defoliatry!"  )
			else :
				p_multi_log.do_logs( "srdb", "Did not do Defoliatry!"  )
				for why in i_lst_why :
					p_multi_log.do_logs( "srdb", why  )
		elif i_instruction.Instruction == EnumCmdInstruction.DoTrimatry :
			p_multi_log.do_logs( "srdb", "Doing Trimatry.."  )
			i_LstPaths = FldtryCLI_Settings.Get_SettingValue( apltry.EnumSettingDef.TrmtryLstPaths )
			i_DoCanDelTopFolders = FldtryCLI_Settings.Get_SettingValue( apltry.EnumSettingDef.TrmtryDoCanDelTopFolders )
			i_DoNestedEmptyFolders = FldtryCLI_Settings.Get_SettingValue( apltry.EnumSettingDef.TrmtryDoNestedEmptyFolders )
			i_DoTreatZeroAsNothing = FldtryCLI_Settings.Get_SettingValue( apltry.EnumSettingDef.TrmtryDoTreatZeroAsNothing )
			i_DoExceptKeepIfEmpty = FldtryCLI_Settings.Get_SettingValue( apltry.EnumSettingDef.TrmtryDoExceptKeepIfEmpty )
			i_DoExceptNoMedia = FldtryCLI_Settings.Get_SettingValue( apltry.EnumSettingDef.TrmtryDoExceptNoMedia )
			i_RenameFoldersRatherThanDelete = FldtryCLI_Settings.Get_SettingValue( apltry.EnumSettingDef.TrmtryDoRenameInsteadOfDelete )
			i_DeletionMode = FldtryCLI_Settings.Get_SettingValue( apltry.EnumSettingDef.TrmtryDeletionMode )
			i_DoDelFromDeepest = FldtryCLI_Settings.Get_SettingValue( apltry.EnumSettingDef.TrmtryDoDelFromDeepest )
			i_is_ok, i_lst_why = trimatry_command_recce( i_LstPaths, \
				i_DoCanDelTopFolders, i_DoNestedEmptyFolders, i_DoTreatZeroAsNothing, \
				i_DoExceptKeepIfEmpty, i_DoExceptNoMedia, i_RenameFoldersRatherThanDelete, i_DeletionMode, i_DoDelFromDeepest )
			if i_is_ok :
				trimatry_command( i_LstPaths, \
				i_DoCanDelTopFolders, i_DoNestedEmptyFolders, i_DoTreatZeroAsNothing, \
				i_DoExceptKeepIfEmpty, i_DoExceptNoMedia, i_RenameFoldersRatherThanDelete, i_DeletionMode, i_DoDelFromDeepest, p_multi_log )
				p_multi_log.do_logs( "srdb", "Did Trimatry!" )
			else :
				p_multi_log.do_logs( "srdb", "Did not do Trimatry!"  )
				for why in i_lst_why :
					p_multi_log.do_logs( "srdb", why  )
		elif i_instruction.Instruction == EnumCmdInstruction.DoCongruentryPaths :
			p_multi_log.do_logs( "srdb", "Doing CongruentryPaths.."  )
			#print( "do congruentry" )
			i_A_str_path = FldtryCLI_Settings.Get_SettingValue( apltry.EnumSettingDef.CngrntryTwoPathAStrPath )
			# 
			i_A_EnumCharEnc = FldtryCLI_Settings.Get_SettingValue( apltry.EnumSettingDef.CngrntryTwoPathACustomCharEnc )
			i_A_CaseSensitive = FldtryCLI_Settings.Get_SettingValue( apltry.EnumSettingDef.CngrntryTwoPathACustomDoCase )
			i_A_BadName = FldtryCLI_Settings.Get_SettingValue( apltry.EnumSettingDef.CngrntryTwoPathACustomDoMsName )
			i_A_BadChars = FldtryCLI_Settings.Get_SettingValue( apltry.EnumSettingDef.CngrntryTwoPathACustomDoMsChar )
			i_A_Wind8p3 = FldtryCLI_Settings.Get_SettingValue( apltry.EnumSettingDef.CngrntryTwoPathACustomDoMs8p3 )
			i_A_FnameMatchSetting = apltry.FnameMatchSetting()
			i_A_FnameMatchSetting.set_UseEnumCharEnc( i_A_EnumCharEnc)
			i_A_FnameMatchSetting.set_DoCaseSensitive( i_A_CaseSensitive)
			i_A_FnameMatchSetting.set_DoMsBadNames( i_A_BadName)
			i_A_FnameMatchSetting.set_DoMsBadChars( i_A_BadChars )
			i_A_FnameMatchSetting.set_DoMs8p3( i_A_Wind8p3 )
			#
			i_B_str_path = FldtryCLI_Settings.Get_SettingValue( apltry.EnumSettingDef.CngrntryTwoPathBStrPath )
			#
			i_B_EnumCharEnc = FldtryCLI_Settings.Get_SettingValue( apltry.EnumSettingDef.CngrntryTwoPathBCustomCharEnc )
			i_B_CaseSensitive = FldtryCLI_Settings.Get_SettingValue( apltry.EnumSettingDef.CngrntryTwoPathBCustomDoCase )
			i_B_BadName = FldtryCLI_Settings.Get_SettingValue( apltry.EnumSettingDef.CngrntryTwoPathBCustomDoMsName )
			i_B_BadChars = FldtryCLI_Settings.Get_SettingValue( apltry.EnumSettingDef.CngrntryTwoPathBCustomDoMsChar )
			i_B_Wind8p3 = FldtryCLI_Settings.Get_SettingValue( apltry.EnumSettingDef.CngrntryTwoPathBCustomDoMs8p3 )
			i_B_FnameMatchSetting = apltry.FnameMatchSetting()
			i_B_FnameMatchSetting.set_UseEnumCharEnc( i_B_EnumCharEnc)
			i_B_FnameMatchSetting.set_DoCaseSensitive( i_B_CaseSensitive)
			i_B_FnameMatchSetting.set_DoMsBadNames( i_B_BadName)
			i_B_FnameMatchSetting.set_DoMsBadChars( i_B_BadChars )
			i_B_FnameMatchSetting.set_DoMs8p3( i_B_Wind8p3 )
			#
			i_Stringent = True # FldtryCLI_Settings.Get_SettingValue( apltry.EnumSettingDef.CngrntryAnalyseStringent )
			i_StopNotCongruent = True # FldtryCLI_Settings.Get_SettingValue( apltry.EnumSettingDef.CngrntryStopNotCongruent )
			i_StopNotMoiety = True # FldtryCLI_Settings.Get_SettingValue( apltry.EnumSettingDef.CngrntryStopNotMoiety )
			i_ReportDiffs = False # FldtryCLI_Settings.Get_SettingValue( apltry.EnumSettingDef.CngrntryReportDiffs )
			i_SummaryDiffs = True # FldtryCLI_Settings.Get_SettingValue( apltry.EnumSettingDef.CngrntryDiffSummary )
			p_ReportFailuresStringent = True
			i_GenCopyScript = False # FldtryCLI_Settings.Get_SettingValue( apltry.EnumSettingDef.CngrntryGenCopyScript )
			i_CopyScriptShell = None # FldtryCLI_Settings.Get_SettingValue( apltry.EnumSettingDef.CngrntryCopyScriptShell )
			i_is_ok, i_lst_why = congruentry_paths_command_recce( i_A_str_path, i_B_str_path, i_A_FnameMatchSetting, i_B_FnameMatchSetting, \
					i_Stringent, i_StopNotCongruent, i_StopNotMoiety, i_ReportDiffs, i_SummaryDiffs, i_GenCopyScript )
			if i_is_ok :
				got_chckd_congruent, got_chckd_subset, got_chckd_extra_side = \
					congruentry_paths_command( i_A_str_path, i_B_str_path, i_A_FnameMatchSetting, i_B_FnameMatchSetting, \
						i_Stringent, i_StopNotCongruent, i_StopNotMoiety, i_ReportDiffs, i_SummaryDiffs, p_ReportFailuresStringent, i_GenCopyScript, \
						p_multi_log)
				str_outcome = "Congruentry outcome:"
				str_outcome = str_outcome + "\n" + "got_chckd_congruent: " + str( got_chckd_congruent )
				str_outcome = str_outcome + "\n" + "got_chckd_subset: " + str( got_chckd_subset )
				str_outcome = str_outcome + "\n" + "got_chckd_extra_side: " + got_chckd_extra_side
				#print( "did congruentry" )
				#print( str_outcome) 
				p_multi_log.do_logs( "srdb", "Did CongruentryPaths!"  )
				p_multi_log.do_logs( "srdb", "Outcome:" + str_outcome )
			else :
				#print( "Did not do CongruentryPaths!" )
				p_multi_log.do_logs( "srdb", "Did not do CongruentryPaths!"  )
				for why in i_lst_why :
					#print( why )
					p_multi_log.do_logs( "srdb", why  )
		elif i_instruction.Instruction == EnumCmdInstruction.DoCongruentryFiles :
			p_multi_log.do_logs( "srdb", "Doing CongruentryFiles.."  )
			i_str_filea = FldtryCLI_Settings.Get_SettingValue( apltry.EnumSettingDef.CngrntryTwoFileAStrFileref )
			i_str_fileb = FldtryCLI_Settings.Get_SettingValue( apltry.EnumSettingDef.CngrntryTwoFileBStrFileref )
			i_is_ok, i_lst_why = congruentry_files_command_recce( i_str_filea, i_str_fileb )
			if i_is_ok :
				got_chckd_congruent = congruentry_files_command( i_str_filea, i_str_fileb, p_multi_log)
				p_multi_log.do_logs( "sr", "Congruentry Files run:")
				p_multi_log.do_logs( "sr", "File A: " + i_str_filea )
				p_multi_log.do_logs( "sr", "File B: " + i_str_fileb )
				str_outcome = "- outcome:"
				str_outcome = str_outcome + "\n" + "got_chckd_congruent: " + str( got_chckd_congruent )
				p_multi_log.do_logs( "sr", str_outcome)
				p_multi_log.do_logs( "srdb", "Did CongruentryFiles!"  )
			else :
				p_multi_log.do_logs( "srdb", "Did not do CongruentryFiles!"  )
				for why in i_lst_why :
					p_multi_log.do_logs( "srdb", why  )
		elif i_instruction.Instruction == EnumCmdInstruction.DoSummatry :
			p_multi_log.do_logs( "srdb", "Doing Summatry.."  )
			i_LstPaths = FldtryCLI_Settings.Get_SettingValue( apltry.EnumSettingDef.SmmtryLstPaths )
			i_is_ok, i_lst_why = summatry_command_recce( i_LstPaths)
			if i_is_ok :
				summatry_command( i_LstPaths, p_multi_log )
				p_multi_log.do_logs( "srdb", "Did Summatry!" )
			else :
				p_multi_log.do_logs( "srdb", "Did not do Summatry!"  )
				for why in i_lst_why :
					p_multi_log.do_logs( "srdb", why  )
		elif i_instruction.Instruction == EnumCmdInstruction.DoCheckNames :
			p_multi_log.do_logs( "srdb", "Doing CheckNames.."  )
			i_LstPaths = FldtryCLI_Settings.Get_SettingValue( apltry.EnumSettingDef.SmmtryLstPaths )
			i_DoMsName = FldtryCLI_Settings.Get_SettingValue( apltry.EnumSettingDef.SmmtryDoMsName )
			i_DoMsChar = FldtryCLI_Settings.Get_SettingValue( apltry.EnumSettingDef.SmmtryDoMsChar )
			i_is_ok, i_lst_why = summatry_checkfor_recce( i_LstPaths, i_DoMsName, i_DoMsChar )
			if i_is_ok :
				summatry_checkfor( i_LstPaths, i_DoMsName, i_DoMsChar, p_multi_log )
				p_multi_log.do_logs( "srdb", "Did CheckNames!"  )
			else :
				p_multi_log.do_logs( "srdb", "Did not do CheckNames!"  )
				for why in i_lst_why :
					#print( why )
					p_multi_log.do_logs( "srdb", why  )
		elif i_instruction.Instruction == EnumCmdInstruction.DoCountExtensions :
			p_multi_log.do_logs( "srdb", "Doing CountExtensions.."  )
			i_LstPaths = FldtryCLI_Settings.Get_SettingValue( apltry.EnumSettingDef.SmmtryLstPaths )
			i_is_ok, i_lst_why = summatry_counts_by_filename_ext_recce( i_LstPaths )
			if i_is_ok :
				summatry_counts_by_filename_ext( i_LstPaths, p_multi_log )
				p_multi_log.do_logs( "srdb", "Did CountExtensions!"  )
			else :
				p_multi_log.do_logs( "srdb", "Did not do CountExtensions!"  )
				for why in i_lst_why :
					p_multi_log.do_logs( "srdb", why  )
		else :
			p_multi_log.do_logs( "srdb", "unknown command line option" )	
	p_multi_log.do_logs( "srdb", "command_line_actions ENDED" )


def ShowCommandLineHelp( p_called_as ) :
	print( app_show_name_version() )
	print( "Usage:" )
	print( p_called_as + " [-L settingsfilename] [-P] [-D] [-T] [-CP] [-CF] [-S] [-CN] [-CE] [-H]" )

def parse_command_line( lst_arg, p_multi_log ) :
	if __debug__:
		p_multi_log.do_logs( "sb", "parse_command_line" )
	if __debug__:
		p_multi_log.do_logs( "b", "parsing arguments" )
	r_do_gui = True # any single non-LOAD command option will mean not going into GUI mode
	# conversely, if all the options are LOAD then those will be passed to the GUI mode for action
	expecting_load_fileref = False
	got_valid_arg = False
	instruction = None
	loadfileref = ""
	r_lst_instructions = []
	for arg in lst_arg :
		arg_upper = arg.upper()
		if __debug__:
			p_multi_log.do_logs( "b", "ARG:" + arg )
		if arg_upper in ["-L", "--LOAD"] :
			instruction = EnumCmdInstruction.LoadSettings
			expecting_load_fileref = True
		elif arg_upper in ["-P", "--PRUNATRY"] :
			instruction = EnumCmdInstruction.DoPrunatry
			got_valid_arg = True
			r_do_gui = False
		elif arg_upper in ["-D", "--DEFOLIATRY"] :
			instruction = EnumCmdInstruction.DoDefoliatry
			got_valid_arg = True
			r_do_gui = False
		elif arg_upper in ["-T", "--TRIMATRY"] :
			instruction = EnumCmdInstruction.DoTrimatry
			got_valid_arg = True
			r_do_gui = False
		elif arg_upper in ["-CP", "--CONGRUENTRYPATHS"] :
			instruction = EnumCmdInstruction.DoCongruentryPaths
			got_valid_arg = True
			r_do_gui = False
		elif arg_upper in ["-CF", "--CONGRUENTRYFILES"] :
			instruction = EnumCmdInstruction.DoCongruentryFiles
			got_valid_arg = True
			r_do_gui = False
		elif arg_upper in ["-S", "--SUMMATRY"] :
			instruction = EnumCmdInstruction.DoSummatry
			got_valid_arg = True
			r_do_gui = False
		elif arg_upper in ["-CN", "--CHECKNAMES"] :
			instruction = EnumCmdInstruction.DoCheckNames
			got_valid_arg = True
			r_do_gui = False
		elif arg_upper in ["-CE", "--COUNTEXTS"] :
			instruction = EnumCmdInstruction.DoCountExtensions
			got_valid_arg = True
			r_do_gui = False
		elif arg_upper in ["-H", "--HELP"] :
			instruction = EnumCmdInstruction.ShowCmdLineHelp
			got_valid_arg = True
			r_do_gui = False
		elif expecting_load_fileref :
			# must be the fileref for load settings
			loadfileref = arg
			got_valid_arg = True
		if got_valid_arg :
			# append a tuple to the instruction list
			r_lst_instructions.append( nmdtpl_Instruction( instruction,loadfileref ) )
			instruction = None
			loadfileref = ""
			got_valid_arg = False 
			expecting_load_fileref = False
			# note that this is what causes one of the other command options to cancel a LOAD that didn't specify a filename
			# this also means that you can't specify a settings filename that exactly matches any of the command line options -  ce la vie
	return r_do_gui, r_lst_instructions

# main to execute only if run as a program
if __name__ == "__main__":
	# setup logging
	i_multi_log = setup_logging()
	if __debug__:
		i_multi_log.do_logs( "b", "Invocation as Main")
	# command line parameter checking
	# a simple check that no command line options were passed, so go stright into the GUI mode
	# but, if there are some options passed, send those to a parser routine to build a list of 
	# "instructions" - as a list of named tuples - and also to note whether any of those imply
	# NOT going into the GUI mode
	arg_count = len(im_sys.argv)
	if arg_count < 2 :
		# in effect no parameters as the first parameter is the command itself
		i_gui_mode = True
		i_lst_instructions = []
	else:
		if __debug__:
			i_multi_log.do_logs( "b", "Passing to handle_command_line() with Argument count:" + str( arg_count) )
		i_gui_mode, i_lst_instructions = parse_command_line( im_sys.argv, i_multi_log )
	if i_gui_mode :
		if __debug__:
			i_multi_log.do_logs( "b", "Passing to main_gui_interact()" )
		main_gui_interact( i_lst_instructions, i_multi_log ) # later make pass preloadfilename through for pickup into the GUI mode
	elif ( len( i_lst_instructions) == 1 ) and ( i_lst_instructions[0] == EnumCmdInstruction.ShowCmdLineHelp ) :
		ShowCommandLineHelp( im_sys.argv[0] )
	else :
		command_line_actions( i_lst_instructions, i_multi_log )
	if __debug__:
		i_multi_log.do_logs( "b", "Completion as Main")
