#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# organitry = organise a tree 
#
# --------------------------------------------------
# Copyright (C) 2024 Gerald Waters - Melbourne, Australia
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# --------------------------------------------------
# Imports - Standard
# --------------------------------------------------

from collections import namedtuple

import os as im_os
import os.path as im_osp

import copy as im_copy

import re as im_re

# --------------------------------------------------
# Imports - Custom
# --------------------------------------------------

import commontry as cmntry
import applitry as apltry
import exploratry as explrtry

from deforestry import matchsubtry_command

# import multilog

# --------------------------------------------------
# Support Custom
# --------------------------------------------------

def mod_code_name():
	return "organitry"

def mod_code_abbr():
	return "orgntry"

def mod_show_name():
	return "Organitry"

# --------------------------------------------------
# Feature Structure Definitions
# --------------------------------------------------

# --------------------------------------------------
# Support Stock
# --------------------------------------------------

# --------------------------------------------------
# Support Custom - i.e. mostly generic functions on simple types
# --------------------------------------------------


# --------------------------------------------------
# Feature Support
# --------------------------------------------------

# --------------------------------------------------
# set up custom named tuple for holding 
tpl_forf_rename = namedtuple('tpl_forf_rename', 'rn_path rn_from rn_into')

def forf_enact_rename_path_tuple( p_tpl_frf_rn ):
	fref_from = im_osp.join( p_tpl_frf_rn.rn_path, p_tpl_frf_rn.rn_from)
	fref_into = im_osp.join( p_tpl_frf_rn.rn_path, p_tpl_frf_rn.rn_into)
	try:
		im_os.rename( fref_from, fref_into )
		r_did_ok = True
	except:
		r_did_ok = False
	return r_did_ok

# --------------------------------------------------
# Feature: Canprunatry = can prune a tree
# --------------------------------------------------

def canprunatry_discovery( p_lst_folders, p_limit_top_n, p_fn_Check_NoInterruption, p_multi_log ):
	zz_procname = "canprunatry_discovery"
	# print( zz_procname )
	i_multi_log = im_copy.deepcopy( p_multi_log)
	cmntry.re_filename_multilog_object( i_multi_log, zz_procname )
	def def_name():
		return zz_procname
	def def_name_prfx():
		return def_name() + " "
	# Debugging info
	i_multi_log.do_logs( "b", "Called: " + def_name() )
	# proofread the parameter
	if p_limit_top_n < 1 :
		i_limit_top_n = 10
	else :
		i_limit_top_n = p_limit_top_n
	i_lst_all_tree_paths = p_lst_folders
	i_multi_log.do_logs( "sdb", "i_lst_all_tree_paths" + " has " + str( len(i_lst_all_tree_paths)) )
	i_use_datetime_str = cmntry.bfff_datetime_ident_str() 
	i_bashfilename_str = cmntry.scrf_make_bash_filename( i_use_datetime_str, zz_procname )
	# - call matchsubtry - should get back these things:
	#     mtc_dct_hash_lst_fldrs - dictionary of hashes, each with a list of folders
	#     mtc_dct_fldr_selves - dictionary of folders
	#     mtc_dct_fldr_superfolder - dictionary of folders, each with their superfolder
	#     mtc_dct_superfolder_folders - dictionary of superfolders, each with a list of fsub-folders
	#     mtc_dct_hash_best_superhash - dictionary of hashes, each their best superhash
	#     mtc_dct_superhash_hashes - dictionary of superhashes, each with all their subhashes
	#     mtc_dct_sized_superhashes - dictionary of sizes, each with a list of superhashes
	i_multi_log.do_logs( "spdb", def_name() + ": Step 0 Traversals and Collation" )
	p_sbtry_hash_files = False
	dct_hash_lst_fldrs, dct_fldr_selves, dct_fldr_superfolder, \
		dct_superfolder_folders, dct_hash_best_superhash, dct_superhash_hashes, \
		dct_sized_superhashes = matchsubtry_command( i_lst_all_tree_paths, p_sbtry_hash_files, p_fn_Check_NoInterruption, i_multi_log)
	i_multi_log.do_logs( "sdb", mod_code_name() + " matchsubtry_command results" )
	i_multi_log.do_logs( "sdb", "dct_hash_lst_fldrs" + " has " + str( len(dct_hash_lst_fldrs)) )
	i_multi_log.do_logs( "sdb", "dct_fldr_selves" + " has " + str( len(dct_fldr_selves)) )
	i_multi_log.do_logs( "sdb", "dct_fldr_superfolder" + " has " + str( len(dct_fldr_superfolder)) )
	i_multi_log.do_logs( "sdb", "dct_superfolder_folders" + " has " + str( len(dct_superfolder_folders)) )
	i_multi_log.do_logs( "sdb", "dct_hash_best_superhash" + " has " + str( len(dct_hash_best_superhash)) )
	i_multi_log.do_logs( "sdb", "dct_superhash_hashes" + " has " + str( len(dct_superhash_hashes)) )
	i_multi_log.do_logs( "sdb", "dct_sized_superhashes" + " has " + str( len(dct_sized_superhashes)) )
	# p_EnumDeleteMode = "makebashrm"
	if len( dct_hash_lst_fldrs) > 0 :
		#print( def_name_prfx() + " " + "dct_hash_lst_fldrs" )
		#print( dct_hash_lst_fldrs )
		pass
		# for each hash, this has a list of tpl_PathInfo
	if len( dct_fldr_selves) > 0 :
		#print( def_name_prfx() + " " + "dct_fldr_selves" )
		#print( dct_fldr_selves )
		pass
		# for each folderpath this is a tuple item
	if len( dct_fldr_superfolder) > 0 :
		#print( def_name_prfx() + " " + "dct_fldr_superfolder" )
		#print( dct_fldr_superfolder )
		pass
		# for each
	if len( dct_superfolder_folders) > 0 :
		#print( def_name_prfx() + " " + "dct_superfolder_folders" )
		#print( dct_superfolder_folders )
		pass
		# for each
	if len( dct_hash_best_superhash) > 0 :
		#print( def_name_prfx() + " " + "dct_hash_best_superhash" )
		#print( dct_hash_best_superhash )
		pass
		# for each
	if len( dct_superhash_hashes) > 0 :
		#print( def_name_prfx() + " " + "dct_superhash_hashes" )
		#print( dct_superhash_hashes )
		pass
		# for each
	if len( dct_sized_superhashes) > 0 :
		i_multi_log.do_logs( "srdb", "=" * 50 )
		i_multi_log.do_logs( "srdb", "Canprunatry discoveries for the folders:" )
		for i_path in p_lst_folders :
			i_multi_log.do_logs( "srdb", "  " + cmntry.pfn4print( i_path ) )
		i_multi_log.do_logs( "srdb", "We found " + str( len(dct_sized_superhashes)) + " sizes of superhashes. The top " + str( i_limit_top_n) + " will be listed in descending storage size.")
		#print( def_name_prfx() + " " + "dct_sized_superhashes" )
		#print( dct_sized_superhashes )
		# for each
		# get desending list of sizes
		sz_lst = sorted( list( dct_sized_superhashes.keys() ), reverse=True )
		for sz_n in sz_lst[0:i_limit_top_n - 1] :
			i_multi_log.do_logs( "srdb", "Size: " + cmntry.hndy_GetHumanReadable( sz_n) )
			n = 0
			for shash in dct_sized_superhashes[ sz_n ] :
				n += 1
				i_folder_count = len( dct_hash_lst_fldrs[shash] )
				i_size_saving = ( i_folder_count - 1 ) * sz_n
				i_multi_log.do_logs( "srdb", "Hash # " + str( n) + " has " + str( i_folder_count ) + " folders, meaning that " + cmntry.hndy_GetHumanReadable( i_size_saving) + " bytes could be saved" )
				i_multi_log.do_logs( "srdb", "The folders to choose among are:" )
				for fldr in dct_hash_lst_fldrs[ shash ] :
					i_multi_log.do_logs( "srdb", "  " + cmntry.pfn4print( fldr.pi_thispath ) )
		i_multi_log.do_logs( "srdb", "-" * 50 )

def canprunatry_command_recce( p_lst_folders ):
	return cmntry.consider_list_of_folders( p_lst_folders, True )

def canprunatry_command( p_lst_folders, p_limit_top_n, p_fn_Check_NoInterruption, p_multi_log ):
	zz_procname = "canprunatry_command"
	#print( zz_procname)
	#print( "p_lst_folders" )
	#print( p_lst_folders )
	#print( zz_procname + " Ended" )
	# to begin, we'll simply return a list of strings as being the "findings"
	canprunatry_discovery( p_lst_folders, p_limit_top_n, p_fn_Check_NoInterruption, p_multi_log )
	r_lst_findings = [ "Findings are not yet being structured, See them in the Results tab." ]
	return r_lst_findings

# --------------------------------------------------
# Feature: Renametry -  rename a tree
# --------------------------------------------------

def forf_name_revision_mode_DosWinLossy( p_EnumRenametryDirection, p_old_fref):
	if cmntry.FNameHasBadWinChars( p_old_fref):
		r_did = True
		r_new_name = cmntry.Fname_BadWinChars_Removed( p_old_fref )
	else:
		r_did = False
		r_new_name = p_old_fref
	if r_new_name in cmntry.BadWinFileFoldName_Lst():
		r_did = True
		r_new_name = cmntry.BadWinFileFoldName_MaybeRenamed( r_new_name )
	return r_did, r_new_name

def string_as_utf8_hexes( p_str ):
	i_utf8 = p_str.encode('utf-8', 'surrogateescape')
	r_str = "".join( format(x, "02x") for x in i_utf8 )
	return r_str

def DosWinKeepy_Infix():
	# used by both Encode and Decode functions
	return "%%"

def DosWinKeepy_Encode( p_name ):
	r_name = cmntry.do_nonMSchar_encoding( p_name )
	return r_name

def DosWinKeepy_Decode( p_name ):
	# note, this assumes that there is definitely decoding to be done
	r_name = cmntry.do_nonMSchar_decoding( p_name )
	return r_name

def forf_name_revision_mode_DosWinKeepy_Encode( p_old_fref ):
	#print( "forf_name_revision_mode_DosWinKeepy_Encode " + cmntry.pfn4print( p_old_fref ) )
	i_old_name, i_old_ext = im_osp.splitext( p_old_fref )
	if cmntry.FNameHasBadWinChars( i_old_name):
		r_new_name = DosWinKeepy_Encode( i_old_name )
		r_did = True
	else:
		r_did = False
		r_new_name = i_old_name # doing this simplifies the next line
	if False: # for now, let's ignore the issue of bad names - e.g. CON etc
		if r_new_name in cmntry.BadWinFileFoldName_Lst():
			i_new_name = ""
			for i_char in r_new_name :
				i_new_name += "%" + string_as_utf8_hexes( i_char )
			r_did = True
			r_new_name = i_new_name
	if r_did:
		# construct the new name
		r_new_fref = r_new_name + "." + DosWinKeepy_Infix() + i_old_ext
	else:
		r_new_fref = p_old_fref # allow the caller to not need to evaluate the boolean
	return r_did, r_new_fref

def forf_name_revision_mode_DosWinKeepy_Decode( p_old_fref ):
	#print( "forf_name_revision_mode_DosWinKeepy_Decode " + cmntry.pfn4print( p_old_fref ) )
	# pull off the two extensions
	i_a_name, i_a_ext = im_osp.splitext( p_old_fref )
	i_b_name, i_b_ext = im_osp.splitext( i_a_name )
	if False:
		print( "i_a_name" )
		print( i_a_name )
		print( "i_a_ext" )
		print( i_a_ext )
		print( "i_b_name" )
		print( i_b_name )
		print( "i_b_ext" )
		print( i_b_ext )
	# is the sentinel infix either of those?
	sentinel = "." + DosWinKeepy_Infix()
	if sentinel in [ i_a_ext, i_b_ext ] :
		#print( "detected sentinel")
		if i_a_ext == sentinel :
			#print( "sentinel is i_a_ext")
			i_new_name = DosWinKeepy_Decode( i_a_name )
			r_new_fref = i_new_name
			r_did = True
		elif i_b_ext == sentinel :
			#print( "sentinel is i_b_ext")
			i_new_name = DosWinKeepy_Decode( i_b_name )
			r_new_fref = i_new_name + i_a_ext
			r_did = True
		else:
			#print( "sentinel what the?")
			r_did = False
	else:
		r_did = False
		r_new_fref = p_old_fref
	return r_did, r_new_fref

def forf_name_revision_mode_DosWinKeepy( p_EnumRenametryDirection, p_old_fref ):
	if p_EnumRenametryDirection == apltry.EnumRenametryDirection.DoDecode :
		r_did, r_new_fref = forf_name_revision_mode_DosWinKeepy_Decode( p_old_fref )
	else:
		r_did, r_new_fref = forf_name_revision_mode_DosWinKeepy_Encode( p_old_fref )
	return r_did, r_new_fref

def forf_name_revision_mode_DateTranspose( p_EnumRenametryDirection, p_old_fref ):
	return False, p_old_fref

def forf_name_revision_mode_DateTransDash( p_EnumRenametryDirection, p_old_fref ):
	return False, p_old_fref

_forf_name_revision_functions_by_mode = {}
_forf_name_revision_functions_by_mode[ apltry.EnumRenametryMode.DosWinKeepy ] = forf_name_revision_mode_DosWinKeepy
# EnumRenametryMode.DosWinLossy
# EnumRenametryMode.DosWinKeepy
# EnumRenametryMode.DateTranspose
# EnumRenametryMode.DateTransDash

def forf_name_revision_by_mode( p_EnumRenametryMode, p_EnumRenametryDirection, p_old_fref):
	if p_EnumRenametryMode in [ apltry.EnumRenametryMode.DosWinKeepy ] :
		r_did, r_new_fref = _forf_name_revision_functions_by_mode[ apltry.EnumRenametryMode.DosWinKeepy ]( p_EnumRenametryDirection, p_old_fref )
	else:
		r_did = False 
		r_new_fref = p_old_fref
	return r_did, r_new_fref

# start with a hack to do some traversal and trying filerefs
def forf_renaming_traversal( p_from_path, p_EnumRenametryMode, p_EnumRenametryDirection, p_multi_log ):
	p_multi_log.do_logs( "db", "forf_renaming_traversal: " + p_from_path )
	r_dct_depth_lst_rename_tuples = {}
	r_hit_count = 0
	def add_tplrename_to_dct_for_depth( p_tpl_rename, p_depth ):
		nonlocal r_dct_depth_lst_rename_tuples
		if p_depth in r_dct_depth_lst_rename_tuples :
			# must already be a list,so append to it
			r_dct_depth_lst_rename_tuples[ p_depth ].append( p_tpl_rename )
		else:
			# create new key and put a list there
			r_dct_depth_lst_rename_tuples[ p_depth ] = [ p_tpl_rename ]
	def forf_renaming_traverse_recurse( p_path, p_depth ):
		nonlocal r_hit_count
		p_multi_log.do_logs( "db", "forf_renaming_traverse_recurse depth: " + str( p_depth) + " path: " + p_path )
		r_lst_new_rename_tuples = []
		if im_os.access( p_path, im_os.R_OK):
			p_multi_log.do_logs( "db", "traverse getting contents of: " + p_path )
			i_entries = explrtry.get_lst_sorted_os_scandir( p_path )
		else:
			i_entries = []
		for i_entry in i_entries:
			p_multi_log.do_logs( "db", "entry: " + i_entry.name )
			# do the renaming
			i_did, i_new_fref = forf_name_revision_by_mode( p_EnumRenametryMode, p_EnumRenametryDirection, i_entry.name)
			if i_did:
				i_tpl_rename = tpl_forf_rename( p_path, i_entry.name, i_new_fref )
				r_lst_new_rename_tuples.append( i_tpl_rename )
				add_tplrename_to_dct_for_depth( i_tpl_rename, p_depth )
				r_hit_count += 1
			# do the recursing into subfolders
			if i_entry.is_file(follow_symlinks=False):
				pass
			elif i_entry.is_dir( follow_symlinks=False ):
				i_new_subpath = im_osp.join( p_path, i_entry.name)
				i_new_subist = forf_renaming_traverse_recurse( i_new_subpath, p_depth + 1 )
				r_lst_new_rename_tuples.extend( i_new_subist )
		return r_lst_new_rename_tuples
	r_lst_all_rename_tuples = forf_renaming_traverse_recurse( p_from_path, 0 )
	return r_lst_all_rename_tuples, r_hit_count, r_dct_depth_lst_rename_tuples

def merge_dct_depth_lst_with_dct_depth_lst( t_dct_depth_lst_a, t_dct_depth_lst_b ):
	# this will modify t_dct_depth_lst_a by adding things from t_dct_depth_lst_b
	for k in t_dct_depth_lst_b:
		if k in t_dct_depth_lst_a:
			# there _shoud_ be a list already there, so extend it with its equivalent from b
			t_dct_depth_lst_a[ k].extend( t_dct_depth_lst_b[ k] )
		else:
			# new key
			t_dct_depth_lst_a[ k] = t_dct_depth_lst_b[ k]

def renametry_command( p_lst_folders, p_EnumRenametryMode, p_EnumRenametryDirection, p_multi_log ):
	zz_procname = "renametry_command"
	#print( zz_procname)
	#print( "p_lst_folders" )
	#print( p_lst_folders )
	#print( "p_EnumRenametryMode" )
	#print( p_EnumRenametryMode )
	#
	i_multi_log = im_copy.deepcopy( p_multi_log)
	#cmntry.re_filename_multilog_object( i_multi_log, zz_procname )
	i_multi_log.do_logs( "dbs", "Renametry for : " + str( len( p_lst_folders ) ) + " folders." )
	i_lst_found = [] # deprecated, pending dictionary to handle levels proven good
	i_dct_depth_list_finds = {}
	r_hit_count = 0
	for i_path in p_lst_folders :
		i_multi_log.do_logs( "dbs", "Renametry : " + i_path )
		i_lst_finds, t_hit_count, t_dct_depth_list_finds = forf_renaming_traversal( i_path, p_EnumRenametryMode, p_EnumRenametryDirection, i_multi_log )
		i_lst_found.extend( i_lst_finds )
		merge_dct_depth_lst_with_dct_depth_lst( i_dct_depth_list_finds, t_dct_depth_list_finds )
		r_hit_count += t_hit_count
		#print( "inside loop i_dct_depth_list_finds" )
		#print( i_dct_depth_list_finds )
	i_multi_log.do_logs( "rdbs", "Renametry found: " + str( r_hit_count ) + " instances in the dictionary." )
	if False and len( i_lst_found ) > 0 :
		#print( "after loop i_dct_depth_list_finds" )
		#print( i_dct_depth_list_finds )
		i_multi_log.do_logs( "rdb", "Renametry found: " + str( len( i_lst_found ) ) + " listed instances." )
		# i_multi_log.do_logs( "r", zz_procname + " " +  \
		#	cmntry.hndy_str_of_boolean( p_badwinfname, "Problem Windows names " ) + "  " + \
		#	cmntry.hndy_str_of_boolean( p_badwinchars, "Problem Windows characters " ) )
		#i_multi_log.do_logs( "r", zz_procname + ": " + "Analysis of paths:" )
		#i_this_run_infix = cmntry.bfff_datetime_ident_str()
		# output the folders ?? why though ??
		#i_list_filename = cmntry.dtff_make_data_filename( i_this_run_infix, zz_procname + "_Folders" )
		#cmntry.file_in_home_text_new_list_as_data_head( i_list_filename, ["path"] )
		#for i_path in p_lst_folders :
		#	cmntry.file_in_home_text_add_list_as_data_line( i_list_filename, [ cmntry.pfn4print( i_path ) ] )
		#	i_multi_log.do_logs( "r", cmntry.pfn4print( i_path ) )
		# output the fond files
		#i_data_filename = cmntry.dtff_make_data_filename( i_this_run_infix, zz_procname + "_FileRefs" )
		i = 0
		#cmntry.file_in_home_text_new_list_as_data_head( i_data_filename, ["pathfilename"] )
		for fref in i_lst_found :
			i += 1
			if cmntry.is_n_to_show_progess( i ):
				log_ctrl_str = "rdbs"
			else :
				log_ctrl_str = "dbs"
			i_multi_log.do_logs( log_ctrl_str, "Rename #" + str(i) )
			i_multi_log.do_logs( log_ctrl_str, "@ " + cmntry.pfn4print( fref.rn_path ) )
			i_multi_log.do_logs( log_ctrl_str, "< " + cmntry.pfn4print( fref.rn_from ) )
			i_multi_log.do_logs( log_ctrl_str, "> " + cmntry.pfn4print( fref.rn_into ) )
			did_ok = False #forf_enact_rename_path_tuple( fref )
			i_multi_log.do_logs( log_ctrl_str, "Action success? " + str(did_ok) )
			#cmntry.file_in_home_text_add_list_as_data_line( i_data_filename, [ cmntry.pfn4print( fref ) ] )
	if len( i_dct_depth_list_finds ) > 0 :
		i_counter = 0
		#print( i_dct_depth_list_finds )
		i_lst_keys = list( i_dct_depth_list_finds.keys() )
		#print( "i_lst_keys" )
		#print( i_lst_keys )
		s_lst_keys = reversed( sorted( i_lst_keys) )
		#print( "s_lst_keys" )
		#print( s_lst_keys )
		for i_depth in s_lst_keys:
			t_lst_found = i_dct_depth_list_finds[ i_depth ]
			for fref in t_lst_found :
				i_counter += 1
				if cmntry.is_n_to_show_progess( i_counter ):
					log_ctrl_str = "rdbs"
				else :
					log_ctrl_str = "dbs"
				i_multi_log.do_logs( log_ctrl_str, "Depth " + str(i_depth) + " Rename #" + str(i_counter) )
				i_multi_log.do_logs( log_ctrl_str, "@ " + cmntry.pfn4print( fref.rn_path ) )
				i_multi_log.do_logs( log_ctrl_str, "< " + cmntry.pfn4print( fref.rn_from ) )
				i_multi_log.do_logs( log_ctrl_str, "> " + cmntry.pfn4print( fref.rn_into ) )
				did_ok = forf_enact_rename_path_tuple( fref )
				i_multi_log.do_logs( log_ctrl_str, "Action success? " + str(did_ok) )
				#cmntry.file_in_home_text_add_list_as_data_line( i_data_filename, [ cmntry.pfn4print( fref ) ] )
	# print( zz_procname + " Ended" )
	pass

# --------------------------------------------------
# Main - only if run as a program
# --------------------------------------------------

def setup_logging() :
	# this uses the custom wrapper module "multilog"
	m_log = cmntry.make_multilog_object( mod_code_abbr() )
	return m_log

# main 
if __name__ == "__main__":
	print( "This is the module: organitry")
	# setup logging
	i_multi_log = setup_logging()
	if __debug__:
		i_multi_log.do_logs( "b", "Invocation as Main")
	i_multi_log.do_logs( "dbs", mod_code_name() )
	i_lst_folder = [ "/home/ger/Downloads", "/home/ger/dev_tests" ]
	organitry_command( i_lst_folder, i_multi_log)
	i_multi_log.do_logs( "dbs", mod_code_name() + " Completed!" )
