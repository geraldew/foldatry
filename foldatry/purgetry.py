#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# purgetry = purge a tree
#
# This forms part of the foldatry application but can be used as an
# independent command line tool.
#
# --------------------------------------------------
# Copyright (C) 2018-2024  Gerald Waters - Melbourne, Australia
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# --------------------------------------------------
# Imports - Standard
# --------------------------------------------------

import sys as im_sys
import os as im_os
#import os.path as osp
import shutil as im_shutil
from tkinter import filedialog
from tkinter import messagebox
from tkinter import *

# --------------------------------------------------
# Imports - Custom
# --------------------------------------------------

import commontry as cmntry
import applitry as apltry

# --------------------------------------------------
# Support Custom
# --------------------------------------------------

def mod_code_name():
	return "purgetry"

def mod_code_abbr():
	return "prgtry"

def mod_show_name():
	return "Purgetry"

# --------------------------------------------------
# Support Custom - i.e. mostly generic functions on simple types
# --------------------------------------------------


# --------------------------------------------------
# Sub-features
# --------------------------------------------------

# shutil.rmtree(path,ignore_errors=False,onerror=errorRemoveReadonly) 
def errorRemoveReadonly(func, path, exc):
    excvalue = exc[1]
    if func in (im_os.rmdir, im_os.remove) and excvalue.errno == errno.EACCES:
        # change the file to be readable,writable,executable: 0777
        im_os.chmod(path, stat.S_IRWXU | stat.S_IRWXG | stat.S_IRWXO)  
        # retry
        func(path)
    else:
        # raiseenter code here
        print( "excvalue.errno", excvalue.errno)

def rmtree(top):
    for root, dirs, files in im_os.walk(top, topdown=False):
        for name in files:
            filename = im_os.path.join(root, name)
            im_os.chmod(filename, stat.S_IWUSR)
            im_os.remove(filename)
        for name in dirs:
            im_os.rmdir(im_os.path.join(root, name))
    im_os.rmdir(top)      

def old_bashify_pathfilename( p_path_str) :
	if True :
		# for bash, wrapping in single quotes obviates special character handling 
		# except for single quotes, so replace any internal single quotes with a sequence of:
		# -single double-single-double single-
		i_path_str = "'" + p_path_str.replace( "'", "'" + '"' + "'" + '"' + "'") + "'"
	elif True :
		if "'" in p_path_str :
			i_path_str = p_path_str
			prepend = "'"
			postpend = "'"
			if True:
				if "'" in i_path_str :
					# replace any single quotes with '"'"'
					qr = "'" + '"' + "'" + '"' + "'"
					i_path_str.replace( "'", qr)
			else:
				# separate dealing with an initial single quote
				if i_path_str[0] == "'" :
					# get all but the trailing single quote
					i_path_str = i_path_str[1:]
					prepend = "\'"
				# separate dealing with a trailing single quote
				if i_path_str[-1] == "'" :
					# get all but the trailing single quote
					i_path_str = i_path_str[:-2]
					postpend = "\'"
				# now wrap in single quotes
				i_path_str = "'" + i_path_str + "'"
				i_path_str = prepend + i_path_str + postpend
		else :
			# no single quotes at all so it's a simple wrapping in single quotes
			i_path_str = "'" + p_path_str + "'"
	else :
		# wrap in double quotes
		i_path_str = '"' + p_path_str + '"'
	# print( "bashified: " + i_path_str)
	return i_path_str

def bashify_pathfilename( p_path_str) :
	# for bash, wrapping in single quotes obviates special character handling 
	# except for single quotes, so replace any internal single quotes with a sequence of:
	# -single double-single-double single-
	if False :
		i_path_str = "'" + p_path_str.replace( "'", "'" + '"' + "'" + '"' + "'") + "'"
	else :
		#print( "Debug checking def bashify_pathfilename" )
		#print( "cmntry.pfn4print( p_path_str ) " )
		#print( cmntry.pfn4print( p_path_str ) )
		i_path_str = cmntry.pfn4bash( p_path_str )
		#print( "i_path_str = cmntry.pfn4bash( p_path_str )" )
		#print( i_path_str )
	return i_path_str

def bash_script_pathref_add_rm( p_bashfilename_str, p_path_str):
	bash_line_str = ""
	if False:
		bash_line_str = "# " + mod_code_name() + " bash line script of rm commands" + "\n"
	b_path_str = bashify_pathfilename( p_path_str)
	bash_line_str = bash_line_str + "echo " + b_path_str + "\n"
	bash_line_str = bash_line_str + "rm -r -- " + b_path_str + "\n"
	cmntry.bash_script_file_add_line( p_bashfilename_str, bash_line_str )

def bash_script_pathref_add_rm_with_rems( p_bashfilename_str, p_path_str, p_lst_rem_ante, p_lst_rem_post):
	bash_line_str = ""
	if False:
		bash_line_str = "# " + mod_code_name() + " bash line script of rm commands" + "\n"
	if len( p_lst_rem_ante ) > 0 :
		for l in p_lst_rem_ante :
			bash_line_str = bash_line_str + "# " + l + "\n"
	b_path_str = bashify_pathfilename( p_path_str)
	bash_line_str = bash_line_str + "echo " + b_path_str + "\n"
	bash_line_str = bash_line_str + "rm -r -- " + b_path_str + "\n"
	if len( p_lst_rem_post ) > 0 :
		for l in p_lst_rem_post :
			bash_line_str = bash_line_str + "# " + l + "\n"
	cmntry.bash_script_file_add_line( p_bashfilename_str, bash_line_str )

def bash_script_fileref_add_rm( p_bashfilename_str, p_file_str):
	bash_line_str = ""
	if False:
		bash_line_str = "# " + mod_code_name() + " bash line script of rm commands" + "\n"
	b_file_str = bashify_pathfilename( p_file_str)
	bash_line_str = bash_line_str + "echo " + b_file_str + "\n"
	bash_line_str = bash_line_str + "rm -- " + b_file_str + "\n"
	cmntry.bash_script_file_add_line( p_bashfilename_str, bash_line_str )

def bash_script_fileref_add_rm_with_rems( p_bashfilename_str, p_file_str, p_lst_rem_ante, p_lst_rem_post):
	bash_line_str = ""
	if False:
		bash_line_str = "# " + mod_code_name() + " bash line script of rm commands" + "\n"
	if len( p_lst_rem_ante ) > 0 :
		for l in p_lst_rem_ante :
			bash_line_str = bash_line_str + "# " + l + "\n"
	b_file_str = bashify_pathfilename( p_file_str)
	bash_line_str = bash_line_str + "echo  " + b_file_str + "\n"
	bash_line_str = bash_line_str + "rm -- " + b_file_str + "\n"
	if len( p_lst_rem_post ) > 0 :
		for l in p_lst_rem_post :
			bash_line_str = bash_line_str + "# " + l + "\n"
	#print( "bash_script_fileref_add_rm_with_rems" )
	#print( cmntry.pfn4print( bash_line_str ) )
	cmntry.bash_script_file_add_line( p_bashfilename_str, bash_line_str )

# --------------------------------------------------
# Deletion Mode Functions - a pair for each mode, one for folders, one for files
# --------------------------------------------------

# ..................................................
# Dry run deletion functions
# ..................................................

def mock_delete_file_now( fileref) :
	didok =False
	return didok

def mock_delete_folder_now( foldref) :
	didok =False
	return didok

# ..................................................
# Trash functions
# ..................................................

# def trash_file_now( fileref) :
# return didok

# def trash_folder_now( foldref) :
# return didok

# ..................................................
# Removal functions
# ..................................................

def remove_file_now( p_FileRef) :
	if im_os.path.exists( p_FileRef) :
		if ( im_os.path.isfile( p_FileRef ) ) :
			try:
				im_os.remove( p_FileRef )
				did_ok = True
			except OSError as e:
				print("Error: %s - %s." % (e.filename, e.strerror))
				did_ok = False
		else :
			did_ok = False
	else :
		did_ok = False
	return did_ok

def remove_folder_now( p_FoldRef) :
	if im_os.path.exists( p_FoldRef) :
		if ( im_os.path.isdir( p_FoldRef ) ) :
			did_ok = False
			# toyed with variations here, leave intact in case they prove needed
			if True :
				try:
					im_shutil.rmtree( p_FoldRef )
					did_ok = True
				except OSError as e:
					print("Error: %s - %s." % (e.filename, e.strerror))
					did_ok = False
			elif False :
				try:
					im_shutil.rmtree( p_FoldRef, ignore_errors=True)
					did_ok = True
				except OSError as e:
					print("Error: %s - %s." % (e.filename, e.strerror))
					did_ok = False
			elif False :
				im_shutil.rmtree( p_FoldRef, ignore_errors=False, onerror=errorRemoveReadonly) 
				did_ok = True
		else :
			did_ok = False
	else :
		did_ok = False
	return did_ok

# ..................................................
# Secure wipe functions
# ..................................................

# def secure_wipe_file_now( fileref) :
# return didok

# def secure_wipe_folder_now( foldref) :
# return didok

# ..................................................
# Bash script functions
# ..................................................

def bash_script_delete_file( p_ScriptFileName, p_FileRef) :
	bash_script_fileref_add_rm( p_ScriptFileName, p_FileRef )
	didok = True
	return didok

def bash_script_delete_file_with_rems( p_ScriptFileName, p_FileRef, p_lst_rem_ante, p_lst_rem_post) :
	bash_script_fileref_add_rm_with_rems( p_ScriptFileName, p_FileRef, p_lst_rem_ante, p_lst_rem_post)
	didok = True
	return didok

def bash_script_delete_folder( p_ScriptFileName, p_FoldRef) :
	bash_script_pathref_add_rm( p_ScriptFileName, p_FoldRef )
	didok = True
	return didok

def bash_script_delete_folder_with_rems( p_ScriptFileName, p_FoldRef, p_lst_rem_ante, p_lst_rem_post) :
	bash_script_pathref_add_rm_with_rems( p_ScriptFileName, p_FoldRef, p_lst_rem_ante, p_lst_rem_post)
	didok = True
	return didok

# --------------------------------------------------
# Primary feature
# --------------------------------------------------

def purgetry_path_command( p_treepath, p_EnumDeleteMode, p_bashfilename_str, p_size, p_multi_log):
	# custom
	# options for p_EnumDeleteMode are:
	# - DryRun = no actual purging will be done
	# - DeleteNow = use the rmtree function from the shutil Python module
	# - Bash = make a bash script of rm calls
	if __debug__:
		p_multi_log.do_logs( "b", "purgetry - deleting a tree." )
		p_multi_log.do_logs( "b", "Path =" + p_treepath )
	did_ok = False
	if p_EnumDeleteMode is apltry.EnumDeleteMode.DryRun :
		if __debug__:
			p_multi_log.do_logs( "b", "purgetry - Dry Run method. Passed: " + apltry.EnumDeleteMode_Attrbt_Brief( p_EnumDeleteMode) )
			p_multi_log.do_logs( "b", "So no actual purging will be done." + apltry.EnumDeleteMode_Attrbt_Brief( p_EnumDeleteMode) )
		did_ok = mock_delete_folder_now( fileref)
	elif p_EnumDeleteMode is apltry.EnumDeleteMode.DeleteNow :
		if __debug__:
			p_multi_log.do_logs( "b", "purgetry - Delete Now method passed: " + apltry.EnumDeleteMode_Attrbt_Brief( p_EnumDeleteMode) )
		did_ok = remove_folder_now( p_treepath)
		if did_ok :
			outcome_str = "Purged Path: " + cmntry.pfn4print( p_treepath )
		else :
			outcome_str = "Purge FAIL: " + cmntry.pfn4print( p_treepath )
		p_multi_log.do_logs( "srdb", outcome_str )
	elif p_EnumDeleteMode is apltry.EnumDeleteMode.Bash :
		if __debug__:
			p_multi_log.do_logs( "b", "purgetry - Bash method. Passed: " + apltry.EnumDeleteMode_Attrbt_Brief( p_EnumDeleteMode) )
		i_lst_rems_before = [ "Folder size: " + cmntry.hndy_GetHumanReadable( p_size) ]
		i_lst_rems_after = []
		did_ok = bash_script_delete_folder_with_rems( p_bashfilename_str, p_treepath, i_lst_rems_before, i_lst_rems_after )
	else :
		if __debug__:
			p_multi_log.do_logs( "b", "purgetry - no valid method passed: " + apltry.EnumDeleteMode_Attrbt_Brief( p_EnumDeleteMode) )
			p_multi_log.do_logs( "b", "Unrecognised method so no purging will be done." + apltry.EnumDeleteMode_Attrbt_Brief( p_EnumDeleteMode) )
		did_ok = False
	return did_ok

# marking out a call to be used by other modules when doing file-to-file hash duplicate detections
# the idea of placing it here rather than letting them do their own deleting is to give this
# the same kinds of checks, handling and options as the folder deletion
def purgetry_file_command( p_pathfilename, p_EnumDeleteMode, p_bashfilename_str, p_lst_rem_ante, p_lst_rem_post, p_multi_log):
	did_ok = False
	if p_EnumDeleteMode is apltry.EnumDeleteMode.DryRun :
		if __debug__:
			p_multi_log.do_logs( "b", "purgetry - Dry run method. Passed: " + apltry.EnumDeleteMode_Attrbt_Brief( p_EnumDeleteMode) )
			p_multi_log.do_logs( "b", "So no actual purging will be done." + apltry.EnumDeleteMode_Attrbt_Brief( p_EnumDeleteMode) )
		did_ok = mock_delete_folder_now( p_pathfilename)
	elif p_EnumDeleteMode is apltry.EnumDeleteMode.DeleteNow :
		if __debug__:
			p_multi_log.do_logs( "b", "purgetry - Delete Now method passed: " + apltry.EnumDeleteMode_Attrbt_Brief( p_EnumDeleteMode) )
		did_ok = remove_file_now( p_pathfilename)
		if did_ok :
			outcome_str = "Purged File: " + cmntry.pfn4print( p_pathfilename )
		else :
			outcome_str = "Purge FAIL: " + cmntry.pfn4print( p_pathfilename )
		p_multi_log.do_logs( "sdbr", outcome_str )
	elif p_EnumDeleteMode is apltry.EnumDeleteMode.Bash :
		if __debug__:
			p_multi_log.do_logs( "b", "purgetry - passed method: " + apltry.EnumDeleteMode_Attrbt_Brief( p_EnumDeleteMode) )
		if len( p_lst_rem_ante ) + len( p_lst_rem_post ) == 0:
			bash_script_delete_file( p_bashfilename_str, p_pathfilename )
		else :
			bash_script_delete_file_with_rems( p_bashfilename_str, p_pathfilename, p_lst_rem_ante, p_lst_rem_post)
		did_ok = True
	else :
		if __debug__:
			p_multi_log.do_logs( "b", "purgetry - passed method: " + apltry.EnumDeleteMode_Attrbt_Brief( p_EnumDeleteMode) )
			p_multi_log.do_logs( "b", "Unrecognised method so no purging will be done." + apltry.EnumDeleteMode_Attrbt_Brief( p_EnumDeleteMode) )
		did_ok = False
	return did_ok

# --------------------------------------------------
# Interactive
# --------------------------------------------------

def getfileref_fromuser( from_folder_name):
	root = Tk()
	root.withdraw()
	FolderNameA = filedialog.askdirectory(title='Choose Folder A', initialdir="/")
	return FolderNameA

# --------------------------------------------------
# Log controls
# --------------------------------------------------

# --------------------------------------------------
# Main - only if run as a program
# --------------------------------------------------

# main 
if __name__ == "__main__":
	print( "This is the module: purgetry")
	print( "Currently no testing is setup for this module.")
