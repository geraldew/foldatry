#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# multilog = a class approach to having organised use of the stock Python logging module
#
# --------------------------------------------------
# Copyright (C) 2019-2024  Gerald Waters - Melbourne, Australia
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# --------------------------------------------------

#
# This was created primarily to mimic a custom-built method that allows a single
# call point in the application to go to multiple logs as controlled by
# a simple string value.
#
# The principle idea here is to allow single code lines of things to log
# even though they may go to multiple places
# e.g. m_log.do_logs( "sd", "Test Only to Screen and Details.")
# where "sd" gives the multiple destination indcator where
# "t" = terminal and "d" = a file of details
# Also, each such keyed destination can be turned off/on 
# e.g. m_log.key_set_b( "r", False)

from enum import Enum, unique, auto

import sys as im_sys
import logging as im_lggng

im_lggng.basicConfig( level = im_lggng.INFO )
# this seems necessary to stop the root logger from logging everything
for handler in im_lggng.root.handlers[:]:
    im_lggng.root.removeHandler(handler)

class MultiLog:
	
	# dct_lg = {}
	
	def __init__( self ):
		# make sure these things exist
		self.dct_lg = {}

	@unique
	class MetaLogPart(Enum):
		Flag = 0
		FileRef = 1
		Logger = 2
		Zilch = 3

	def internal_get_new_log( self, p_key_str ) :
		#print("internal_get_new_log" + " " + p_key_str )
		# having a logger per p_key_str is just for separation
		l = im_lggng.getLogger( p_key_str)
		return l

	def internal_logger_reset_handler_fileref( self, p_lggr, p_fileref_str ) :
		f_handler = im_lggng.FileHandler( p_fileref_str, "a" )
		f_handler.setLevel(im_lggng.INFO)
		f_format = im_lggng.Formatter('File: %(asctime)s - %(message)s')
		#f_format = im_lggng.Formatter('File: %(name)s - %(message)s')
		# f_format = im_lggng.Formatter('File: %(filename)s - %(message)s')
		# f_format = im_lggng.Formatter('File: %(lineno)s - %(message)s')
		# f_format = im_lggng.Formatter('File: %(funcName)s - %(message)s')
		# f_format = im_lggng.Formatter('File: %(levelname)s - %(message)s')
		f_handler.setFormatter(f_format)
		p_lggr.addHandler(f_handler)
		#print( "internal_logger_reset_handler_fileref" + " " + p_fileref_str )

	def addkey_k_b_terminal( self, p_key_str, p_flag) : # goes to standard output
		if p_key_str in self.dct_lg :
			dct[ self.MetaLogPart.FileRef ] = ""
			dct[ self.MetaLogPart.Flag ] = p_flag
			dct[ self.MetaLogPart.Zilch ] = False
		else :
			dct = {}
			dct[ self.MetaLogPart.FileRef ] = ""
			dct[ self.MetaLogPart.Flag ] = p_flag
			dct[ self.MetaLogPart.Zilch ] = False
			# make a new logger for screen
			i_lggr = self.internal_get_new_log( p_key_str )
			c_handler = im_lggng.StreamHandler(im_sys.stdout)
			c_handler.setLevel(im_lggng.INFO)
			c_format = im_lggng.Formatter('Screen: %(name)s - %(levelname)s - %(message)s')
			c_handler.setFormatter(c_format)
			i_lggr.addHandler(c_handler)
			dct[ self.MetaLogPart.Logger ] = i_lggr
			# set to the dictionary 
			self.dct_lg[ p_key_str ] = dct
			#print( "addkey_k_b_terminal " +  p_key_str + " " + str(p_flag) )

	def addkey_k_b_fileref( self, p_key_str, p_flag, p_fileref_str) :
		if p_key_str in self.dct_lg :
			pass
		else :
			dct = {}
			dct[ self.MetaLogPart.Logger ] = self.internal_get_new_log( p_key_str )
			dct[ self.MetaLogPart.Zilch ] = True
			self.dct_lg[ p_key_str ] = dct
		self.dct_lg[ p_key_str ][ self.MetaLogPart.Flag ] = p_flag
		self.dct_lg[ p_key_str ][ self.MetaLogPart.FileRef ] = p_fileref_str
		#print( "addkey_k_b_fileref " + p_key_str + " " + str(p_flag) + " " + p_fileref_str )

	def addkey_k_b_handler( self, p_key_str, p_flag, p_handler) :
		# where p_handler is a valid logging handler
		#print( "addkey_k_b_handler " + p_key_str + " " + str(p_flag) )
		if p_key_str in self.dct_lg :
			pass
		else :
			dct = {}
			dct[ self.MetaLogPart.Logger ] = self.internal_get_new_log( p_key_str )
			dct[ self.MetaLogPart.Zilch ] = True
			self.dct_lg[ p_key_str ] = dct
		self.dct_lg[ p_key_str ][ self.MetaLogPart.Flag ] = p_flag
		self.dct_lg[ p_key_str ][ self.MetaLogPart.FileRef ] = ""
		i_lggr = self.internal_get_new_log( p_key_str )
		i_lggr.addHandler(p_handler)
		self.dct_lg[p_key_str][ self.MetaLogPart.Logger ] = i_lggr
		#print( "addkey_k_b_handler " + p_key_str + " " + str(p_flag) + " Done!" )

	def key_get_b( self, p_key_str) :
		# all we do here is change the flag in the inner dictionary
		if p_key_str in self.dct_lg :
			p_flag = self.dct_lg[ p_key_str ][ self.MetaLogPart.Flag ]
			# print( "key_get_b " + p_key_str + " " + str(p_flag) )
		else:
			p_flag = False
		return p_flag

	def key_set_b( self, p_key_str, p_flag) :
		# all we do here is change the flag in the inner dictionary
		if p_key_str in self.dct_lg :
			self.dct_lg[ p_key_str ][ self.MetaLogPart.Flag ] = p_flag
			#print( "key_set_b " + p_key_str + " " + str(p_flag) )

	def key_set_f( self, p_key_str, p_fileref_str) :
		if p_key_str in self.dct_lg :
			self.dct_lg[ p_key_str ][ self.MetaLogPart.FileRef ] = p_fileref_str
			self.internal_logger_reset_handler_fileref( self.dct_lg[p_key_str][ self.MetaLogPart.Logger ], p_fileref_str )
			#print( "key_set_f " + p_key_str + " " + p_fileref_str )

	def key_add_handler( self, p_key_str, p_handler ) :
		if p_key_str in self.dct_lg :
			self.dct_lg[ p_key_str ][ self.MetaLogPart.Logger ].addHandler( p_handler)
			#print( "key_add_handler " + p_key_str )

	# def m_log.key_set_level( "r", loglevel ) # maybe later, for now just ise all as INFO

	def do_logs( self, p_ctrl_str, p_msg_str) :
		#print( "do_logs" + " Ctrl: " + p_ctrl_str, " Msg: " + p_msg_str)
		for c in p_ctrl_str :
			#print( "do_logs" + " check: " + c )
			if c in self.dct_lg.keys() :
				if self.dct_lg[ c ][ self.MetaLogPart.Flag ] :
					# Trigger the handler if not already
					if self.dct_lg[ c ][ self.MetaLogPart.Zilch ] :
						if len( self.dct_lg[ c ][ self.MetaLogPart.FileRef ] ) >  0 :
							self.internal_logger_reset_handler_fileref( self.dct_lg[ c ][ self.MetaLogPart.Logger ], self.dct_lg[ c ][ self.MetaLogPart.FileRef ] )
						self.dct_lg[ c ][ self.MetaLogPart.Zilch ] = False
					#print( "do_logs action!")
					self.dct_lg[c][ self.MetaLogPart.Logger ].info( p_msg_str )
				else :
					#print( "do_logs flag is off for " + c + " in " + p_ctrl_str)
					pass
			else :
				#print( "do_logs no key match! " + p_ctrl_str )
				pass

"""
Example usage

# Import the module with an alias
import multilog as im_multilog

# Create an instance
m_log = im_multilog.MultiLog()

# Setup various key letters, their boolean flags, their filenames
m_log.addkey_k_b_fileref( "d", True, "/home/usr/multilog_detail.log")
m_log.addkey_k_b_fileref( "b", True, "/home/usr/multilog_debugs.log")
m_log.addkey_k_b_fileref( "r", True, "/home/usr/multilog_result.log")
m_log.addkey_k_b_screen( "s", True, )

# Add a handler and assign it to a key letter
plogs_text_handler = Cls_Lggng_TextHandler(st_plogs)
m_log.addkey_k_b_handler( "p", False, plogs_text_handler ) :

# Example calls during the program
m_log.do_logs( "s", "Test log Screen only.")
m_log.do_logs( "d", "Test log Detail only.")
m_log.do_logs( "b", "Test log Debug only.")
m_log.do_logs( "p", "Test log Process only.")
m_log.do_logs( "r", "Test log Result only.")
m_log.do_logs( "sdbr", "Test All channels.")
m_log.do_logs( "dbr", "Test All to Files.")
m_log.do_logs( "sd", "Test Only to Screen and Details.")
m_log.do_logs( "sr", "Test Only to Screen and Results.")

# To allow for fast running without any debug logging, use the following for all frequent-call uses
if __debug__:
  m_log.do_logs( "dbr", "Test All to Files.")

# To change a key flag  
m_log.key_set_b( "d", True)
m_log.key_set_b( "b", False)
  
"""

# --------------------------------------------------
# Main - only if run as a program
# --------------------------------------------------

# main 
if __name__ == "__main__":
	print( "This is the module: multilog")
	print( "Currently no testing is setup for this module.")
