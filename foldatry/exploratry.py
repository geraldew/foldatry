#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# exploratry = common folder list calls fur use by the try/tree family of programs
#
# This is a set of things to be used after being imported into the other python programs
# that are part of the foldatry application.
# It has no purpose in being run on its own.
#
# --------------------------------------------------
# Copyright (C) 2019-2024  Gerald Waters - Melbourne, Australia
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# --------------------------------------------------
# Imports - Standard
# --------------------------------------------------

import os as im_os
import psutil as im_psutl

# --------------------------------------------------
# Imports - Custom
# --------------------------------------------------

import commontry as cmntry
import applitry as apltry

# --------------------------------------------------
# Support Custom
# --------------------------------------------------

def mod_code_name():
	return "exploratry"

def mod_code_abbr():
	return "explrtry"

def mod_show_name():
	return "Exploratry"

# --------------------------------------------------
# Features for use by other modules
# --------------------------------------------------

def getpath_fromfileref( p_fileref ):
	return im_os.path.dirname( p_fileref)

def getname_fromfileref( p_fileref ):
	return im_os.path.basename( p_fileref)

def join_path_name( p_path, p_name ):
	return im_os.path.join( p_path, p_name )

def does_path_exist( p_path):
	return im_os.path.exists( p_path)

def splitall(path):
	# Brief:
	# Parameters:
	# - path :: string
	# Returns:
	# - allparts :: list of strings := order is important
	# Notes:
	# Code:
	allparts = []
	while 1:
		parts = im_os.path.split(path)
		if parts[0] == path:  # sentinel for absolute paths
			allparts.insert(0, parts[0])
			break
		elif parts[1] == path: # sentinel for relative paths
			allparts.insert(0, parts[1])
			break
		else:
			path = parts[0]
			allparts.insert(0, parts[1])
	return allparts

def subfolderness_a_b( path_a, path_b ) :
	# Brief:
	# test two paths to see if either is a subfolder of the other
	# there are four possible outcomes, marked by four symbols:
	# # = neither is a subfolder of the other 
	# a = path_a is a subfolder of path_b
	# b = path_b is a subfolder of path_a
	# = = both paths are the same
	# Parameters:
	# - path_a - string of a folder path
	# - path_b - string of a folder path
	# - p_dct_rdlg - dictionary of redirectable log controls as used by commontry 
	# Returns:
	# - a string indicating either a b = #
	# Notes:
	# - By using the os module split, this should work _across_ equivalent subdir delimiters
	# Code:
	log_strl_str = "b"
	#p_multi_log.do_logs( "b", "subpathness_a_b" )
	#p_multi_log.do_logs( "b", "a " + path_a )
	#p_multi_log.do_logs( "b", "b " + path_b )
	# a
	lst_path_a_parts = splitall( path_a )
	len_a_parts = len( lst_path_a_parts)
	#p_multi_log.do_logs( "b", "len a " + str( len_a_parts) )
	# b
	lst_path_b_parts = splitall( path_b )
	len_b_parts = len( lst_path_b_parts)
	#p_multi_log.do_logs( "b", "len b " + str( len_b_parts) )
	#
	no_diff = True
	i = 0
	while no_diff and (i < len_a_parts) and (i < len_b_parts) :
		no_diff = ( lst_path_a_parts[i] == lst_path_b_parts[i] )
		if no_diff :
			i = i + 1
	if no_diff :
		if len_a_parts < len_b_parts :
			sub_state = "b"
		elif len_a_parts > len_b_parts :
			sub_state = "a"
		elif len_a_parts == len_b_parts :
			sub_state = "="
		else :
			input( "This should never happen!.")
	else :
		sub_state = "#"
	#p_multi_log.do_logs( "b", "sub_state: " + sub_state )
	return sub_state

def pathlist_path_clash( p_the_list, p_the_item, p_case_insensitive, p_multi_log):
	# Brief:
	# detect if the_item is already listed or clashes as a sub-folder with something in the_list
	# Parameters:
	# - the_list = a list of paths to test against
	# - the_item = the path being checked
	# Returns:
	# - any_is_clash = boolean True if the_item clashed with any path in the_list in any way
	# - any_makes_sub = boolean True if there was any clash by being a sub-folder
	# Notes:
	# Code:
	#p_multi_log.do_logs( "dbs", "Testing item: " + the_item + " for clash against list of " + str(len(the_list)) )
	any_is_clash = False
	any_makes_sub = False
	if p_case_insensitive :
		the_list = [item.lower() for item in p_the_list]
		the_item = p_the_item.lower()
	else:
		the_list = p_the_list
		the_item = p_the_item
	if ( the_item in the_list ) :
		any_is_clash = True
		any_makes_sub = False
		#p_multi_log.do_logs( "dbs", "Item already in list !" )
		# input( "Press <ENTER> to continue")
	elif len( the_list) > 0:
		#p_multi_log.do_logs( "dbs", "Testing against list.." )
		for an_item in the_list:
			#p_multi_log.do_logs( "dbs", "Testing: " + an_item )
			sbfldrnss_a_b = subfolderness_a_b( the_item, an_item )
			#p_multi_log.do_logs( "dbs", "subfolderness_a_b: " + sbfldrnss_a_b )
			if sbfldrnss_a_b == "#" :
				this_is_clash = False
				this_makes_sub = False
			else :
				# either a b or =
				this_is_clash = True
				if sbfldrnss_a_b == "=" :
					this_makes_sub = False
				else :
					# that leaves either a or b
					this_makes_sub = True
			# if is_either_a_sub_path( the_item, item, p_dct_rdlg ) :
			#	is_clash = True
			#p_multi_log.do_logs( "dbs", "SubFolder Clash!" )
			#p_multi_log.do_logs( "dbs", "Had: " + item )
			#p_multi_log.do_logs( "dbs", "New: " + the_item )
			# input( "Press <ENTER> to continue")
			any_is_clash = any_is_clash or this_is_clash
			any_makes_sub = any_makes_sub or this_makes_sub
	if __debug__:
		p_multi_log.do_logs( "b", "Decided:" + " any_is_clash:" + str(any_is_clash) + " any_makes_sub:" + str(any_makes_sub) )
	return any_is_clash, any_makes_sub

def pathlist_self_clash( p_the_list, p_case_insensitive, p_multi_log):
	# Brief:
	# detect if there are clashes internal to a list of folders
	# Parameters:
	# - the_list
	# Returns:
	# - any_clash :: boolean := yes there was a clash of some sort
	# - any_sub :: boolean := yes one of the clashes was as a subset
	# Notes:
	# this is to serve some other features that will presume a list free of internal clashes
	# hence the need to check for that first
	# general idea is to take each item out of the list and test it against the rest
	# detect if the_item is already listed or clashes as a sub-folder with something in the_list
	# Code:
	# parameters:
	# - a_list & b_list = a lists of paths to test
	any_clash = False
	any_sub = False
	if p_case_insensitive :
		the_list = [item.lower() for item in p_the_list]
	else:
		the_list = p_the_list
	len_list = len( the_list)
	i = 0
	while i < len_list :
		the_item = the_list[i]
		test_list = the_list[:i] + the_list[i+1:]
		clash_was, made_sub_was = pathlist_path_clash( test_list, the_item, p_case_insensitive, p_multi_log)
		any_clash = any_clash or clash_was
		any_sub = any_sub or made_sub_was
		i = i + 1
	return any_clash, any_sub

def pathlist_pathlist_clash( p_a_list, p_b_list, p_case_insensitive, p_multi_log):
	# Brief:
	# detect if the_item is already listed or clashes as a sub-folder with something in the_list
	# Parameters:
	# - a_list - list of paths to test
	# - b_list - list of paths to test
	# Returns:
	# Notes:
	# really this is just providing an abstraction of testing two lists against each other
	# Code:
	# parameters:
	if p_case_insensitive :
		a_list = [item.lower() for item in p_a_list]
		b_list = [item.lower() for item in p_b_list]
	else:
		a_list = p_a_list
		b_list = p_b_list
	# test each list for internal clashes
	self_clash_a, self_subclash_a = pathlist_self_clash( a_list, p_case_insensitive, p_multi_log)
	self_clash_b, self_subclash_b = pathlist_self_clash( b_list, p_case_insensitive, p_multi_log)
	# test items of a against b
	any_a_item_clash_b = False
	any_a_item_sub_b = False
	for the_item in a_list :
		an_a_item_clash_b, an_a_item_sub_b = pathlist_path_clash( b_list, the_item, p_case_insensitive, p_multi_log)
		any_a_item_clash_b = any_a_item_clash_b or an_a_item_clash_b
		any_a_item_sub_b = any_a_item_sub_b or an_a_item_sub_b
	# test items of a against b
	any_b_item_clash_a = False
	any_b_item_sub_a = False
	for the_item in b_list :
		an_b_item_clash_a, an_b_item_sub_a = pathlist_path_clash( a_list, the_item, p_case_insensitive, p_multi_log)
		any_b_item_clash_a = any_b_item_clash_a or an_b_item_clash_a
		any_b_item_sub_a = any_b_item_sub_a or an_b_item_sub_a
	some_clash = self_clash_a or self_clash_b or any_a_item_clash_b or any_b_item_clash_a
	return some_clash

def Path_GetListOfFirstLevelSubPaths( p_path ):
	# for a given path, return a list of the first level sub-paths
	i_lst_paths = []
	if im_os.access( p_path, im_os.R_OK):
		i_entries = im_os.scandir(p_path)
	else:
		i_entries = []
	for i_entry in i_entries:
		if i_entry.is_dir( follow_symlinks=False ):
			i_new_subpath = im_os.path.join(p_path, i_entry.name)
			i_lst_paths.append( i_new_subpath )
	return i_lst_paths

def path_check_is_empty( p_path ):
	if im_os.path.exists( p_path) and not im_os.path.isfile( p_path): 
		# Checking if the directory is empty or not 
		if not im_os.listdir( p_path): 
			return True
		else: 
			return False
	else: 
		return False

# --------------------------------------------------
# Emulation functions - here to emulate former local calls
# The usage of these should be deprecated
# --------------------------------------------------

def DeprecationAlert( p_defname ):
	print("DEPRECATION ALERT: " + p_defname )

def is_either_a_sub_path( path_a, path_b ) :
	DeprecationAlert( "is_either_a_sub_path" )
	subness = subfolderness_a_b( path_a, path_b )
	return subness != "#"

def addtostrlist_ifnoclash( the_list, the_item ):
	DeprecationAlert( "addtostrlist_ifnoclash" )
	# only add the item if it is not a left substring 
	is_subpath = False
	if len( the_list) > 0:
		for item in the_list:
			if True :
				is_subpath = is_either_a_sub_path( the_item, item )
			else :
				if item.find(the_item) == 0:
					is_subpath = True
				if the_item.find(item) == 0:
					is_subpath = True
			if is_subpath:
				pass
				#p_multi_log.do_logs( "b", "Folder selection Clash!" )
				#p_multi_log.do_logs( "b", "Had: " + item )
				#p_multi_log.do_logs( "b", "New: " + the_item )
				#? input( "FYI now press ENTER>")
	if not is_subpath:
		the_list.append( the_item)
		#p_multi_log.do_logs( "b", "Added: " + the_item )

def try_lst_sorted_os_scandir( p_path ):
	# this is a bullet-proofing of scandir as well as replacing its iterator with a real list
	# was required because of failures with 
	#  = sorted(im_os.scandir( p_path), key = lambda x: x.name)
	# and attempts to inspect easily cause the iterator to lose the contents
	had_errors = False
	try:
		got_entries = im_os.scandir( p_path)
		# print( "Ok from: im_os.scandir( p_path) for " +  p_path)
	except:
		got_entries = []
		had_errors = True
		# print( "Error with: im_os.scandir( p_path) for " +  p_path)
	lst_entries = []
	try:
		for e in got_entries:
			lst_entries.append( e)
	except:
		had_errors = True
		# print( "Error with: for e in got_entries for " +  p_path)
	got_entries = sorted( lst_entries, key = lambda x: x.name)
	return got_entries, had_errors

def get_lst_sorted_os_scandir( p_path ):
	entries, errors = try_lst_sorted_os_scandir( p_path )
	return entries

# --------------------------------------------------
# file system type discovery
# put here partly to avoid circular references by having it in commontry
# --------------------------------------------------

def get_fs_type_a( mypath ):
	root_type = ""
	for part in im_psutl.disk_partitions(True): # or True ?
		if part.mountpoint == '/':
			root_type = part.fstype
			continue
		if mypath.startswith( part.mountpoint ):
			return part.fstype
	return root_type

def get_fs_type_b(path):
    partition = {}
    for part in im_psutl.disk_partitions():
        partition[part.mountpoint] = (part.fstype, part.device)
    if path in partition:
        return partition[path]
    splitpath = path.split(im_os.sep)  
    for i in range(len(splitpath),0,-1): # adapted from: for i in xrange(len(splitpath),0,-1):
        path = im_os.sep.join(splitpath[:i]) + im_os.sep
        if path in partition:
            return partition[path]
        path = im_os.sep.join(splitpath[:i])
        if path in partition:
            return partition[path]
    return ("unkown","none")
    
def get_fs_type_c_linux_only( p_path ):
	cmd = 'lsblk -no fstype "$(findmnt --target "' + p_path + '" -no SOURCE)"'
	p = Popen(cmd , shell=True, stdout=PIPE, stderr=PIPE)
	out, err = p.communicate()
	#print( "Return code: ", p.returncode )
	#print( out.rstrip(), err.rstrip() )
	r_tf = out.rstrip().decode( 'utf-8' ) # as bash these days will provide UTF-8
	return r_tf

    # lsblk -no fstype "$(findmnt --target "/media/ger/HD423/" -no SOURCE)"

def get_fs_type( p_path):
	i_tpl = get_fs_type_b( p_path)
	#print( "get_fs_type")
	#print( "now using: get_fs_type_b")
	#print( "p_path: " + p_path )
	#print( "fs_type: " )
	#print( i_tpl )
	r_ft = i_tpl[0]
	if r_ft == "fuseblk" : # this should only show up on Linux systems
		r_ft = get_fs_type_c_linux_only( p_path )
	return r_ft

def default_encoding_for_fs_type( p_fs_type ):
	i_fs = p_fs_type.upper()
	if i_fs == "NTFS" :
		return apltry.EnumCharEnc.ChEnc_UTF8
	elif i_fs == "ISO9660" :
		return apltry.EnumCharEnc.ChEnc_cp1252
	else :
		return None
	pass

def for_path_get_fstype_charenc( p_path):
	r_fs_type = get_fs_type( p_path )
	r_enc = default_encoding_for_fs_type( r_fs_type )
	return r_fs_type, r_enc

#def for_path_get_fstype_charenc_str( p_path):
#	i_fs_type, r_enc = for_path_get_fstype_charenc( p_path)
#	r_str = CharEncMode_EnumValueString( r_enc )
#	return r_str

def for_path_get_fstype_set_charenc( p_path, p_enc):
	r_fst, i_enc = for_path_get_fstype_charenc( p_path)
	if not p_enc is None :
		r_enc = p_enc
	elif not i_enc is None :
		r_enc = i_enc
	else :
		r_enc = apltry.EnumCharEnc_Default()
	return r_fst, r_enc


# --------------------------------------------------
# Local functions - as used just by the Main, not features for export
# --------------------------------------------------

def setup_logging() :
	# this uses the custom wrapper module "multilog"
	m_log = cmntry.make_multilog_object( mod_code_name() )
	return m_log

# --------------------------------------------------
# Main - which with this being a support module, is just for testing
# --------------------------------------------------

# main only if run as a program
if __name__ == "__main__":
	print( "This is the module: exploratry")
	# setup logging
	multi_log = setup_logging()
	if __debug__:
		multi_log.do_logs( "b", "Invocation as Main")
	print( "Testing Functions" )
	# prep redirectable log dictionary of rdbs controls
	#? dct_log_set = cmmntry_prep_rdlg_dct()
	print( "Completed!")

					#p_multi_log.do_logs( "b", "" )
