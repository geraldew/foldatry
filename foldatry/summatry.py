#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# summatry = summarise a tree ?
#
# This forms part of the foldatry application but can be used as an
# independent command line tool.
#
# --------------------------------------------------
# Copyright (C) 2018-2024 Gerald Waters - Melbourne, Australia
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# --------------------------------------------------
# Imports - Standard
# --------------------------------------------------

import sys as im_sys
import os as im_os
import os.path as im_osp
# import time as im_time
# import stat as im_stat
from collections import namedtuple

import copy as im_copy

# --------------------------------------------------
# Imports - Custom
# --------------------------------------------------

import commontry as cmntry
import exploratry as explrtry
import multilog

# --------------------------------------------------
# Support Custom
# --------------------------------------------------

def mod_code_name():
	return "summatry"

def mod_code_abbr():
	return "smmtry"

def mod_show_name():
	return "Summatry"

# --------------------------------------------------
# Feature Structure Definitions
# --------------------------------------------------

# --------------------------------------------------
# set up custom named tuple for holding 
tpl_Extn_Info = namedtuple('tpl_Extn_Info', 'ei_Count ei_SizeSum ei_SizeMin ei_SizeMax')

def ei_Count_of( p_tpl_Extn_Info ):
	return p_tpl_Extn_Info.ei_Count

def ei_Count_at_key( p_dct_tpl_Extn_Info, p_key ):
	return ei_Count_of( p_dct_tpl_Extn_Info[ p_key ] )

def fn_ei_Count_at_key( p_dct_tpl_Extn_Info ):
	def fn_ei_Count_of_dct_at_key( p_key ):
		return ei_Count_at_key( p_dct_tpl_Extn_Info, p_key )
	return fn_ei_Count_of_dct_at_key

# --------------------------------------------------
# set up custom named tuple
tpl_FldrNode = namedtuple('tpl_FldrNode', 'fn_thispath fn_parentpath fn_foldcount fn_filecount fn_sizesum')

# Class for holding the discovered tallies per folder
class Fltree(object):
	def __init__(self, p_name):
		# core properties - i.e. what makes this a tree
		self.name = p_name # name of the file or folder
		self.children = [] # list of the subtree objects
		# base properties - as per the initial conception
		self.foldcount = -1 # count of sub-directories
		self.filecount = -1 # count of files in this and all sub-directories
		self.sizetally = -1 # sum of file sizes in this and all sub-directories - does not concern itself with overheads of folders
		# extra properties
		self.MinFileSize = -1 # size of the smallest file in this and all sub-directories
		self.MinNonEmptyFileSize = -1 # size of the smallest file in this and all sub-directories
		self.MaxFileSize = -1 # size of the largest file in this and all sub-directories
		self.PathNameOfMaxSizeFile = ""
		self.MaxNumFilesInSomeFolder = -1 # number of files in this or any sub-directories that has the largest such count of them, note that is only for each folder, no accmuluations
		self.FolderOfMaxNumFiles = ""
		self.MaxNumFoldersInSomeFolder = -1 # number of files in this or any sub-directories that has the largest such count of them, note that is only for each folder, no accmuluations
		self.FolderOfMaxNumFolders = ""
		# security properties
		self.permits = {} # this will be a dictionary where the keys are various permission combinations and the values are the counts of items with that pattern
		self.owners = {} # this will be a dictionary where the keys are various owners and the values are the counts of items with that owner
		self.groups = {} # this will be a dictionary where the keys are various groups and the values are the counts of items with that group
	def add_new_child(self, obj):
		self.children.append(obj)
	def set_dfs(self, p_foldcount, p_filecount, p_sizetally):
		self.foldcount = p_foldcount
		self.filecount = p_filecount
		self.sizetally = p_sizetally
	def set_aggs(self, p_MinFileSize, p_MinNonEmptyFileSize, p_MaxFileSize, p_PathNameOfMaxSizeFile, p_MaxNumFilesInSomeFolder, p_FolderOfMaxNumFiles, p_MaxNumFoldersInSomeFolder, p_FolderOfMaxNumFolders):
		self.MinFileSize = p_MinFileSize
		self.MinNonEmptyFileSize = p_MinNonEmptyFileSize
		self.MaxFileSize = p_MaxFileSize
		self.PathNameOfMaxSizeFile = ""
		self.MaxNumFilesInSomeFolder = p_MaxNumFilesInSomeFolder
		self.FolderOfMaxNumFiles = p_FolderOfMaxNumFiles
		self.MaxNumFoldersInSomeFolder = p_MaxNumFoldersInSomeFolder
		self.FolderOfMaxNumFolders = p_FolderOfMaxNumFolders
	def set_secs(self, p_dct_permits, p_dct_owners, p_dct_groups):
		self.permits = p_dct_permits
		self.owners = p_dct_owners
		self.groups = p_dct_groups
		#print( str( self.filecount))
	def add_grown_child(self, p_fltree ):
		# add node
		self.children.append( p_fltree )
		# now reset aggregates
		self.foldcount += p_fltree.foldcount
		self.filecount += p_fltree.filecount
		self.sizetally += p_fltree.sizetally
		# and the conmparson aggregates
		if p_fltree.MinFileSize < self.MinFileSize :
			self.MinFileSize = p_fltree.MinFileSize
		if p_fltree.MinNonEmptyFileSize < self.MinNonEmptyFileSize :
			self.MinNonEmptyFileSize = p_fltree.MinNonEmptyFileSize
		if p_fltree.MaxFileSize > self.MaxFileSize :
			self.MaxFileSize = p_fltree.MaxFileSize
		if p_fltree.MaxNumFilesInSomeFolder > self.MaxNumFilesInSomeFolder :
			self.FolderOfMaxNumFiles = p_fltree.FolderOfMaxNumFiles
			self.MaxNumFilesInSomeFolder = p_fltree.MaxNumFilesInSomeFolder
		if p_fltree.MaxNumFoldersInSomeFolder > self.MaxNumFoldersInSomeFolder :
			self.FolderOfMaxNumFolders = p_fltree.FolderOfMaxNumFolders
			self.MaxNumFoldersInSomeFolder = p_fltree.MaxNumFoldersInSomeFolder

# holder for the triplets of Threshold values 
tpl_Thresholds = namedtuple('tpl_Thresholds', 'th_foldcount th_filecount th_sizesum')
# x = tpl_Thresholds( foldcount, filecount, sizesum)

# --------------------------------------------------
# Support Stock
# --------------------------------------------------

def smmtry_calc_conform( fcode, x, S, A, B, C ):
	if fcode is cmntry.FnCodeConform.Atan :
		return cmntry.CalcConform_GW_Atan( x, S, A, B )
	elif fcode is cmntry.FnCodeConform.Exp :
		return cmntry.CalcConform_GW_Exp( x, S, A, B ) # x, S, F, N
	else:
		return None

def smmtry_calc_combination( fcode, lst, A, B, C ):
	if fcode is cmntry.FnCodeCombin.MPFA :
		return cmntry.CalcCombo_MxPlsFctrdAvg( lst )
	elif fcode is cmntry.FnCodeCombin.Min :
		return min( lst )
	elif fcode is cmntry.FnCodeCombin.Max :
		return max( lst )
	elif fcode is cmntry.FnCodeCombin.Avg :
		return sum(lst) / len(lst)
	elif fcode is cmntry.FnCodeCombin.Geo :
		return cmntry.CalcCombo_GeoMean( lst )
	elif fcode is cmntry.FnCodeCombin.RMS :
		return cmntry.CalcCombo_RMS( lst )
	else:
		return None

# --------------------------------------------------
# Support Custom - i.e. mostly generic functions on simple types
# --------------------------------------------------

def updated_tpl_Extn_Info( p_tpl_Extn_Info, p_Size ):
	if p_Size < p_tpl_Extn_Info.ei_SizeMin :
		t_SizeMin = p_Size
	else :
		t_SizeMin = p_tpl_Extn_Info.ei_SizeMin
	if p_Size > p_tpl_Extn_Info.ei_SizeMax :
		t_SizeMax = p_Size
	else :
		t_SizeMax = p_tpl_Extn_Info.ei_SizeMax
	new_tpl = tpl_Extn_Info( p_tpl_Extn_Info.ei_Count + 1 , p_tpl_Extn_Info.ei_SizeSum + p_Size, t_SizeMin, t_SizeMax )
	return new_tpl

def str_of_tpl_Extn_Info( p_tpl_Extn_Info ):
	s = "Count= " + cmntry.hndy_GetHumanReadable_Count( p_tpl_Extn_Info.ei_Count)
	s += " | Sum= " + cmntry.hndy_GetHumanReadable( p_tpl_Extn_Info.ei_SizeSum)
	s += " | Min= " + cmntry.hndy_GetHumanReadable( p_tpl_Extn_Info.ei_SizeMin)
	s += " | Max= " + cmntry.hndy_GetHumanReadable( p_tpl_Extn_Info.ei_SizeMax)
	s += " | Avg= " + cmntry.hndy_GetHumanReadable( p_tpl_Extn_Info.ei_SizeSum / p_tpl_Extn_Info.ei_Count )
	return s
# --------------------------------------------------
# Process - Traverse
# --------------------------------------------------

# recursively traverse a folder structure, deriving and storing hashes
# this is the outer non-recursive part
def smmtry_traverse_tree( p_s_tree_path, p_multi_log ):
	prcref = "smmtry_traverse_tree"
	p_multi_log.do_logs( "b", prcref + " #Begin# " + p_s_tree_path )
	#
	# collection dictionaries
	#i_dct_permits = {} # the keys will be the permission codes and at each index will be the count of instances for it
	#i_dct_groups= {} # the keys will be the permission group naames encountered and and at each index will be the count of instances for it
	# setup for progress indicating
	call_n = 0
	i_time_mark = cmntry.is_n_to_show_progess_timed_get_mark()
	#
	# recursively traverse a folder structure, summarising so that each node
	# of a tree depends on the contents of its sub-trees
	def smmtry_traverse_tree_recurse( p_tree, p_d_folders, p_s_tree_path, p_s_parent_path, p_n_tree_depth, p_multi_log ):
		# p_d_folders is a dictionary being built, with folderpaths for keys, and tuples for values
		# p_s_tree_path is the folder location now being accounted 
		# p_s_parent_path is to go explicitly on the record 
		nonlocal i_time_mark
		nonlocal call_n
		p_multi_log.do_logs( "b", "@ " + cmntry.pfn4print( p_s_tree_path ) )
		call_n += 1
		b, i_time_mark = cmntry.is_n_to_show_progess_timed_stock( call_n, i_time_mark)
		if b :
			p_multi_log.do_logs( "sp", "At folder # " + str( call_n ) + " " + cmntry.pfn4print( p_s_tree_path ) )
		# preset base values, these are set to zero even if there are no contents for this folder
		r_filcount = 0
		r_dircount = 0
		r_sizsum = 0
		i_filesizsum = 0
		# preset extra values, these are set to -2 so that that becomes a sentinal if there was no content at all -2 is itself quite arbitrary and may later be replaced by a function call
		r_MinFileSize = -2
		r_MinNonEmptyFileSize = -2
		r_MaxFileSize = -2
		r_PathNameOfMaxSizeFile = ""
		r_MaxNumFilesInSomeFolder = -2
		r_FolderOfMaxNumFiles = ""
		r_MaxNumFoldersInSomeFolder = -2
		r_FolderOfMaxNumFolders = ""
		#print( cmntry.pfn4print( p_s_tree_path ) )
		#print( p_tree.name )
		if im_os.access( p_s_tree_path, im_os.R_OK):
			i_entries = explrtry.get_lst_sorted_os_scandir( p_s_tree_path )
		else:
			# for now, we're not doing anything about places where we failed to read, we just don't Summatry them
			i_entries = []
		# re-write, to split into two lists, so that we can process all the files first and then the folders
		i_file_entries = []
		i_fold_entries = []
		for i_entry in i_entries:
			if i_entry.is_file(follow_symlinks=False):
				i_file_entries.append( i_entry )
			elif i_entry.is_dir( follow_symlinks=False ):
				i_fold_entries.append( i_entry )
			else :
				pass # for now, just ignore, we might later want to account for whatever these might be
		for i_file_entry in i_file_entries:
			r_filcount += 1
			#print( "File :" + i_file_entry.name + " count at:" + str(r_filcount) )
			i_fpath = im_osp.join(p_s_tree_path, i_file_entry.name) 
			if True:
				i_size = i_file_entry.stat().st_size
			else:
				i_size = im_osp.getsize( i_fpath)
			i_filesizsum += i_size
			if (r_MinFileSize < 0) or ( i_size < r_MinFileSize ) :
				r_MinFileSize = i_size
			if ( i_size > 0 ) and ( (r_MinNonEmptyFileSize < 0) or ( i_size < r_MinNonEmptyFileSize ) ) :
				r_MinNonEmptyFileSize = i_size
				#print( "from file: r_MinNonEmptyFileSize=" +  str( r_MinNonEmptyFileSize ) + " " + cmntry.pfn4print( p_s_tree_path ) )
			if i_size > r_MaxFileSize :
				r_MaxFileSize = i_size
				r_PathNameOfMaxSizeFile = i_fpath
		r_sizsum = i_filesizsum
		r_MaxNumFilesInSomeFolder = r_filcount
		r_FolderOfMaxNumFiles = p_s_tree_path
		i_just_in_this_dir_dircount = 0
		for i_fold_entry in i_fold_entries:
			i_just_in_this_dir_dircount += 1
			r_dircount += 1
			#print( "Fold :" + i_fold_entry.name  + " count at:" + str(r_dircount))
			i_new_subpath = im_osp.join(p_s_tree_path, i_fold_entry.name)
			i_tree = Fltree( i_fold_entry.name)
			( i_sub_filcount, i_sub_dircount, i_sub_sizsum, i_sub_MinFileSize, i_sub_MinNonEmptyFileSize, \
				i_sub_MaxFileSize, i_sub_PathNameOfMaxSizeFile, \
				i_sub_MaxNumFilesInSomeFolder, i_sub_FolderOfMaxNumFiles, \
				i_sub_MaxNumFoldersInSomeFolder, i_sub_FolderOfMaxNumFolders ) = \
				smmtry_traverse_tree_recurse( i_tree, p_d_folders, i_new_subpath, p_s_tree_path, p_n_tree_depth + 1, p_multi_log )
			p_tree.add_new_child( i_tree)
			r_sizsum += i_sub_sizsum
			r_filcount += i_sub_filcount
			r_dircount += i_sub_dircount
			if (i_sub_MinFileSize >= 0) and ( (r_MinFileSize < 0) or ( i_sub_MinFileSize < r_MinFileSize ) ) :
				r_MinFileSize = i_sub_MinFileSize
			#print( "subfold: i_sub_MinNonEmptyFileSize=" +  str( i_sub_MinNonEmptyFileSize ) + " " + cmntry.pfn4print( p_s_tree_path ) )
			#print( "holding: r_MinNonEmptyFileSize=" +  str( r_MinNonEmptyFileSize ) + " " + cmntry.pfn4print( p_s_tree_path ) )
			if ( i_sub_MinNonEmptyFileSize > 0 ) and ( (r_MinNonEmptyFileSize < 0) or ( i_sub_MinNonEmptyFileSize < r_MinNonEmptyFileSize ) ) :
				r_MinNonEmptyFileSize = i_sub_MinNonEmptyFileSize
				#print( "updated: r_MinNonEmptyFileSize=" +  str( r_MinNonEmptyFileSize ) + " " + cmntry.pfn4print( p_s_tree_path ) )
			else :
				pass
				#print( "left as: r_MinNonEmptyFileSize=" +  str( r_MinNonEmptyFileSize ) + " " + cmntry.pfn4print( p_s_tree_path ) )
			if i_sub_MaxFileSize > r_MaxFileSize :
				r_MaxFileSize = i_sub_MaxFileSize
				r_PathNameOfMaxSizeFile = i_sub_PathNameOfMaxSizeFile
			if i_sub_MaxNumFilesInSomeFolder > r_MaxNumFilesInSomeFolder :
				r_MaxNumFilesInSomeFolder = i_sub_MaxNumFilesInSomeFolder
				r_FolderOfMaxNumFiles = i_sub_FolderOfMaxNumFiles
			if i_sub_MaxNumFoldersInSomeFolder > r_MaxNumFoldersInSomeFolder :
				r_MaxNumFoldersInSomeFolder = i_sub_MaxNumFoldersInSomeFolder
				r_FolderOfMaxNumFolders = i_sub_FolderOfMaxNumFolders
			if i_just_in_this_dir_dircount > r_MaxNumFoldersInSomeFolder :
				r_MaxNumFoldersInSomeFolder = i_just_in_this_dir_dircount
				r_FolderOfMaxNumFolders = p_s_tree_path
		if r_dircount > 0 or r_filcount > 0:
			#print( "Something or something " + cmntry.pfn4print( p_s_tree_path ) )
			# don't bother storing empty folders - it seems meaningless
			# make named tuple
			n_t = tpl_FldrNode( p_s_tree_path, p_s_parent_path, r_dircount, r_filcount, r_sizsum )
			p_d_folders[ p_s_tree_path ] = n_t
			# store base values
			p_tree.set_dfs( r_dircount, r_filcount, r_sizsum)
			# store extra values
			p_tree.set_aggs( r_MinFileSize, r_MinNonEmptyFileSize, \
				r_MaxFileSize, r_PathNameOfMaxSizeFile, \
				r_MaxNumFilesInSomeFolder,r_FolderOfMaxNumFiles, \
				r_MaxNumFoldersInSomeFolder,r_FolderOfMaxNumFolders )
		else :
			pass
			#print( "Empty empty " + cmntry.pfn4print( p_s_tree_path ) )
		p_multi_log.do_logs( "b", cmntry.pfn4print( p_s_tree_path ) )
		#print( "r_MinNonEmptyFileSize=" +  str( r_MinNonEmptyFileSize ) + " " + p_s_tree_path )
		return r_filcount, r_dircount, r_sizsum, r_MinFileSize, r_MinNonEmptyFileSize, \
			r_MaxFileSize, r_PathNameOfMaxSizeFile, \
			r_MaxNumFilesInSomeFolder, r_FolderOfMaxNumFiles, \
			r_MaxNumFoldersInSomeFolder, r_FolderOfMaxNumFolders
	# ----------------------------
	# main of smmtry_traverse_tree
	i_tree = Fltree( p_s_tree_path)
	i_d_folders = {}
	( i_filcount, i_dircount, i_sizsum, i_MinFileSize, i_MinNonEmptyFileSize, \
		i_MaxFileSize, i_PathNameOfMaxSizeFile, \
		i_MaxNumFilesInSomeFolder, i_FolderOfMaxNumFiles, \
		i_MaxNumFoldersInSomeFolder, i_FolderOfMaxNumFolders ) = \
		smmtry_traverse_tree_recurse( i_tree, i_d_folders, p_s_tree_path, "", 0, p_multi_log )
	i_tree.set_dfs( i_dircount, i_filcount, i_sizsum)
	p_multi_log.do_logs( "r", prcref + " #Results#" )
	p_multi_log.do_logs( "r", "File count: " + str( i_filcount) )
	p_multi_log.do_logs( "r", "Directory count: " + str( i_dircount) )
	p_multi_log.do_logs( "r", "Storage total: " + cmntry.hndy_GetHumanReadable( i_sizsum) )
	p_multi_log.do_logs( "r", "Minimum File size: " + cmntry.hndy_GetHumanReadable( i_MinFileSize) )
	p_multi_log.do_logs( "r", "Minimum Non-Empty File size: " + cmntry.hndy_GetHumanReadable( i_MinNonEmptyFileSize) )
	p_multi_log.do_logs( "r", "Maximum File size: " + cmntry.hndy_GetHumanReadable( i_MaxFileSize) )
	p_multi_log.do_logs( "r", "Maximum size File is: " + cmntry.pfn4print( i_PathNameOfMaxSizeFile) )
	p_multi_log.do_logs( "r", "Maximum number of Files in some Folder: " + str( i_MaxNumFilesInSomeFolder) )
	p_multi_log.do_logs( "r", "Folder of Maximum number of Files: " + cmntry.pfn4print( i_FolderOfMaxNumFiles) )
	p_multi_log.do_logs( "r", "Maximum number of Folders in some Folder: " + str( i_MaxNumFoldersInSomeFolder) )
	p_multi_log.do_logs( "r", "Folder of Maximum number of Folders: " + cmntry.pfn4print( i_FolderOfMaxNumFolders) )
	p_multi_log.do_logs( "p", prcref + " #End#" )
	return i_tree

def smmtry_explore_tree_recurse( p_tree, p_path, p_depth, p_multi_log ) :
	i_here_path = im_osp.join( p_path, p_tree.name )
	if False :
		print( "Here node: " + i_here_path )
		print( "Tree node has " + str( len( p_tree.children ) ) + " immediate children")
		print( "Tree node has " + str( p_tree.filecount ) + " files")
		print( "Tree node has " + str( p_tree.foldcount ) + " folders")
		print( "Tree node has " + cmntry.hndy_GetHumanReadable( p_tree.sizetally ) + " storage")
	else :
		hey_s = "Depth: " + str(p_depth) + " " + i_here_path + " (c:" + str( len( p_tree.children ) ) + \
			" f:" + str( p_tree.filecount ) + \
			" d:" + str( p_tree.foldcount ) + \
			" s:" + cmntry.hndy_GetHumanReadable( p_tree.sizetally ) + " )"
		p_multi_log.do_logs( "d", hey_s )
	for sub_tree in p_tree.children :
		smmtry_explore_tree_recurse( sub_tree, i_here_path, p_depth + 1, p_multi_log )

def smmtry_explore_tree( p_tree, p_multi_log ) :
	if ( p_tree.filecount > -1 ) and ( p_tree.foldcount > -1 ) and ( p_tree.sizetally > -1 ) :
		smmtry_explore_tree_recurse( p_tree, "" , 1, p_multi_log)

def smmtry_gatekeep_shortlist_lenghLimit() :
	return 7

def smmtry_gatekeep_threshold_root_minimum() :
	return 0.1

def smmtry_gatekeep_threshold_push_to_one_factor() :
	return 0.2

def smmtry_gatekeep_threshold_push_towards_one( p_was_threshold, p_depth ) :
	i_threshold = p_was_threshold + ( (1.0 - p_was_threshold ) * smmtry_gatekeep_threshold_push_to_one_factor() )
	return i_threshold

def smmtry_gatekeep_threshold_recalc_minimum( p_was_threshold, p_depth ) :
	if p_was_threshold < 0.0 :
		p_new_threshold = smmtry_gatekeep_threshold_root_minimum()
	elif p_was_threshold > 1.0 :
		p_new_threshold = 1.0 - smmtry_gatekeep_threshold_root_minimum()
	elif p_depth > 0 :
		#p_new_threshold = p_was_threshold * (1.0 - ( 1.0 / p_depth ) )
		# go partway to 1.0
		p_new_threshold = smmtry_gatekeep_threshold_push_towards_one( p_was_threshold, p_depth )
	else :
		p_new_threshold = p_was_threshold * smmtry_gatekeep_threshold_root_minimum()
	return p_new_threshold

def smmtry_gatekeep_tree( p_tree, p_multi_log ) :
	zz_procname = "smmtry_gatekeep_tree"
	# define the recursive sub-function
	i_data_filename = ""
	def smmtry_gatekeep_tree_recurse( p_tree, p_path, p_depth, p_gatekeep_threshold_minimum, p_multi_log ) :
		# where
		# - p_tree = the tree point being re-traversed, this time from root to leaf 
		# - p_path = the acculative path to this point
		# - p_depth = counting the levels as we go down - used for adjusting the threshold(s)
		# - p_gatekeep_threshold_minimum = the current threshold
		# nonlocal
		nonlocal i_data_filename
		#
		i_here_path = im_osp.join( p_path, p_tree.name )
		hey_s = "Notable is path: " + cmntry.pfn4print( i_here_path ) + "  {" + "Depth:" + str( p_depth) + " Childs:" + str( len( p_tree.children ) ) + \
			" Files:" + str( p_tree.filecount ) + \
			" Firs:" + str( p_tree.foldcount ) + \
			" Size:" + cmntry.hndy_GetHumanReadable( p_tree.sizetally ) + " }"
		p_multi_log.do_logs( "rd", hey_s )
		#print( "Using threshold:" + str(p_gatekeep_threshold_minimum))
		cmntry.file_in_home_text_add_list_as_data_line( i_data_filename, [ \
			cmntry.pfn4print( i_here_path ), str( p_depth), str( len( p_tree.children ) ), str( p_tree.filecount ), str( p_tree.foldcount), str( p_tree.sizetally) ] )
		lst_possibles = []
		for sub_tree in p_tree.children :
			# by the number of files
			# print( "sub_tree.filecount: " + str(sub_tree.filecount) )
			if p_tree.filecount > 0 :
				grade_filecount = sub_tree.filecount / p_tree.filecount
			else :
				grade_filecount = 0
			pass_threshold_filecount = sub_tree.filecount >= ( p_tree.filecount * p_gatekeep_threshold_minimum )
			# threshold by the number of folders
			# print( "sub_tree.foldcount: " + str(sub_tree.foldcount) )
			grade_foldcount = sub_tree.foldcount / p_tree.foldcount
			pass_threshold_foldcount = sub_tree.foldcount >= ( p_tree.foldcount * p_gatekeep_threshold_minimum )
			# threshold by the storage
			# print( "sub_tree.sizetally: " + str(sub_tree.sizetally) )
			if p_tree.sizetally > 0 :
				grade_sizetally = sub_tree.sizetally / p_tree.sizetally
			else:
				grade_sizetally = 0
			pass_threshold_sizetally = sub_tree.sizetally >= ( p_tree.sizetally * p_gatekeep_threshold_minimum )
			# make string for debug log info
			hey_s = "Graded: " + sub_tree.name + " (" + \
				" file count:" + str( sub_tree.filecount ) + \
				" dir count:" + str( sub_tree.foldcount ) + \
				" subsize:" + cmntry.hndy_GetHumanReadable( sub_tree.sizetally ) + " ) {" + \
				" f ppn:" + "{:.2f}".format( grade_filecount ) + \
				" d ppn:" + "{:.2f}".format( grade_foldcount ) + \
				" s ppn:" + "{:.2f}".format( grade_sizetally ) + "}" + \
				" !:" + "{:.2f}".format( p_gatekeep_threshold_minimum)
			p_multi_log.do_logs( "d", hey_s )
			if (False and pass_threshold_foldcount) or (True and pass_threshold_sizetally) or (False and pass_threshold_filecount) :
				# print( "Adding sub-tree to be noted")
				lst_possibles.append( sub_tree )
			else :
				#print( "Skipping sub-tree")
				pass
		# now we recurse deeper into our selections
		for sub_tree in p_tree.children :
			if sub_tree in lst_possibles :
				# recalculate the threshold as we go down
				i_gatekeep_threshold_minimum = smmtry_gatekeep_threshold_recalc_minimum( p_gatekeep_threshold_minimum, p_depth)
				smmtry_gatekeep_tree_recurse( sub_tree, i_here_path, p_depth + 1, i_gatekeep_threshold_minimum, p_multi_log  )
	# main for smmtry_gatekeep_tree
	if ( p_tree.filecount > -1 ) and ( p_tree.foldcount > -1 ) and ( p_tree.sizetally > -1 ) :
		p_multi_log.do_logs( "dbs", mod_code_name() + "We have a tree to summarise" )
		i_this_run_infix = cmntry.bfff_datetime_ident_str()
		i_data_filename = cmntry.dtff_make_data_filename( i_this_run_infix, zz_procname + "_FileRefs" )
		cmntry.file_in_home_text_new_list_as_data_head( i_data_filename, [ "NotableFolder", "Depth", "ChildCount", "FileCount", "DirCount", "SubSize" ] )
		i_gatekeep_threshold_minimum = smmtry_gatekeep_threshold_root_minimum()
		# i_tpl_Thresholds = tpl_Thresholds( foldcount, filecount, sizesum)
		p_multi_log.do_logs( "rd", "Running " + zz_procname )
		smmtry_gatekeep_tree_recurse( p_tree, "", 1, i_gatekeep_threshold_minimum, p_multi_log )
	else :
		p_multi_log.do_logs( "rdbs", mod_code_name() + " Empty tree, no summary" )

def summatry_command_recce( p_lst_folders ) :
	return cmntry.consider_list_of_folders( p_lst_folders, True )

def summatry_command( p_lst_folders, p_multi_log ) :
	zz_procname = "summatry_command"
	# print( zz_procname )
	i_multi_log = im_copy.deepcopy( p_multi_log)
	cmntry.re_filename_multilog_object( i_multi_log, zz_procname )
	if False:
		# original implementation, doing each path as a separate analysis
		for i_path in p_lst_folders :
			#print( "i_path " + i_path)
			i_tree = smmtry_traverse_tree( i_path, i_multi_log )
			smmtry_explore_tree( i_tree, i_multi_log )
			smmtry_gatekeep_tree( i_tree, i_multi_log )
	else:
		# revised implementation, attempting analysis integrated across all the paths
		root_tree = Fltree( "")
		for i_path in p_lst_folders :
			#print( "i_path " + i_path)
			i_tree = smmtry_traverse_tree( i_path, i_multi_log )
			root_tree.add_grown_child( i_tree)
		smmtry_explore_tree( root_tree, i_multi_log )
		smmtry_gatekeep_tree( root_tree, i_multi_log )
	p_multi_log.do_logs( "s", zz_procname + " completed." )

# --------------------------------------------------
# Feature: Checking for problematic names
# --------------------------------------------------

def checkfor_traverse_recurse( p_path, p_checkfor_badwinfn_name, p_checkfor_badwinfn_char, p_multi_log ):
	#p_multi_log.do_logs( "dbs", "Checkfor traversing: " + p_path )
	r_newlist = []
	if im_os.access( p_path, im_os.R_OK):
		#p_multi_log.do_logs( "dbs", "Checkfor traverse getting contents of: " + p_path )
		i_entries = explrtry.get_lst_sorted_os_scandir( p_path )
	else:
		i_entries = []
	for i_entry in i_entries:
		#p_multi_log.do_logs( "dbs", "Checkfor entry: " + i_entry.name )
		if p_checkfor_badwinfn_name :
			if i_entry.name.upper() in cmntry.BadWinFileFoldName_Lst() :
				i_fpath = im_osp.join( p_path, i_entry.name)
				r_newlist.append( i_fpath )
		if p_checkfor_badwinfn_char :
			if cmntry.FNameHasBadWinChars( i_entry.name) :
				i_fpath = im_osp.join( p_path, i_entry.name)
				r_newlist.append( i_fpath )
		if i_entry.is_file(follow_symlinks=False):
			pass
		elif i_entry.is_dir( follow_symlinks=False ):
			i_new_subpath = im_osp.join( p_path, i_entry.name)
			i_new_subist = checkfor_traverse_recurse( i_new_subpath, p_checkfor_badwinfn_name, p_checkfor_badwinfn_char, p_multi_log )
			r_newlist.extend( i_new_subist )
	return r_newlist

def summatry_checkfor_recce( p_lst_folders, p_badwinfname, p_badwinchars ) :
	return cmntry.consider_list_of_folders( p_lst_folders, True )

def summatry_checkfor( p_lst_folders, p_badwinfname, p_badwinchars, p_multi_log ) :
	zz_procname = "summatry_checkfor"
	# print( zz_procname)
	i_multi_log = im_copy.deepcopy( p_multi_log)
	cmntry.re_filename_multilog_object( i_multi_log, zz_procname )
	i_multi_log.do_logs( "dbs", "Checking for : " + str( len( p_lst_folders ) ) + " folders." )
	i_lst_found = []
	for i_path in p_lst_folders :
		i_multi_log.do_logs( "dbs", "Checkfor : " + i_path )
		i_lst_finds = checkfor_traverse_recurse( i_path, p_badwinfname, p_badwinchars, i_multi_log )
		i_lst_found.extend( i_lst_finds )
	i_multi_log.do_logs( "rdbs", "Checkfor found: " + str( len( i_lst_found ) ) + " instances." )
	if len( i_lst_found ) > 0 :
		i_multi_log.do_logs( "r", zz_procname + " " +  \
			cmntry.hndy_str_of_boolean( p_badwinfname, "Problem Windows names " ) + "  " + \
			cmntry.hndy_str_of_boolean( p_badwinchars, "Problem Windows characters " ) )
		i_multi_log.do_logs( "r", zz_procname + ": " + "Analysis of paths:" )
		i_this_run_infix = cmntry.bfff_datetime_ident_str()
		# output the folders ?? why though ??
		i_list_filename = cmntry.dtff_make_data_filename( i_this_run_infix, zz_procname + "_Folders" )
		cmntry.file_in_home_text_new_list_as_data_head( i_list_filename, ["path"] )
		for i_path in p_lst_folders :
			cmntry.file_in_home_text_add_list_as_data_line( i_list_filename, [ cmntry.pfn4print( i_path ) ] )
			i_multi_log.do_logs( "r", cmntry.pfn4print( i_path ) )
		# output the fond files
		i_data_filename = cmntry.dtff_make_data_filename( i_this_run_infix, zz_procname + "_FileRefs" )
		i = 0
		cmntry.file_in_home_text_new_list_as_data_head( i_data_filename, ["pathfilename"] )
		for fref in i_lst_found :
			i += 1
			if cmntry.is_n_to_show_progess( i ):
				log_ctrl_str = "rdb"
			else :
				log_ctrl_str = "db"
			i_multi_log.do_logs( log_ctrl_str, "#" + str(i) + ": " + cmntry.pfn4print( fref ) )
			cmntry.file_in_home_text_add_list_as_data_line( i_data_filename, [ cmntry.pfn4print( fref ) ] )

# --------------------------------------------------
# Feature: Counts by filename extension 
# --------------------------------------------------

def summatry_counts_by_filename_ext_recce( p_lst_folders ) :
	return cmntry.consider_list_of_folders( p_lst_folders, True )

def summatry_counts_by_filename_ext( p_lst_folders, p_multi_log ) :
	zz_procname = "summatry_counts_by_filename_ext"
	# print( zz_procname)
	# print( "p_lst_folders" )
	# print( p_lst_folders )
	i_multi_log = im_copy.deepcopy( p_multi_log)
	cmntry.re_filename_multilog_object( i_multi_log, zz_procname )
	# define progress monitoring counts
	lcl_file_count = 0
	lcl_fold_count = 0
	# define the recursive internal function 
	def countsbfe_traverse_recurse( p_path, r_dct_counts, i_multi_log ):
		zz_procname = "countsbfe_traverse_recurse"
		# print( zz_procname + " path: " + p_path )
		# i_multi_log.do_logs( "dbs", zz_procname + " Checkfor traversing: " + p_path )
		nonlocal lcl_file_count
		nonlocal lcl_fold_count
		lcl_fold_count += 1
		if cmntry.is_n_to_show_progess( lcl_fold_count ):
			pass
			i_multi_log.do_logs( "dbs", zz_procname + " Scanning folder #" + str( lcl_fold_count ) + ": " + p_path )
		if im_os.access( p_path, im_os.R_OK):
			# i_multi_log.do_logs( "dbs", zz_procname + " Checkfor traverse getting contents of: " + p_path )
			i_entries = explrtry.get_lst_sorted_os_scandir( p_path )
		else:
			i_entries = []
		# print( zz_procname + " i_entries: "  )
		# print( i_entries )
		for i_entry in i_entries:
			#i_multi_log.do_logs( "dbs", zz_procname + " Checkfor entry: " + i_entry.name )
			if i_entry.is_file(follow_symlinks=False):
				lcl_file_count += 1
				d_nam, i_ext = im_osp.splitext( i_entry )
				fpath = im_osp.join( p_path, i_entry.name)
				p_Size = im_osp.getsize( fpath )
				if i_ext in r_dct_counts :
					r_dct_counts[ i_ext ] = updated_tpl_Extn_Info( r_dct_counts[ i_ext ], p_Size )
				else :
					r_dct_counts[ i_ext ] = tpl_Extn_Info( 1, p_Size, p_Size, p_Size )
				if cmntry.is_n_to_show_progess( lcl_file_count ):
					i_multi_log.do_logs( "dbs", zz_procname + " Scanning file #" + str( lcl_file_count ) + ": " + i_entry.name )
			elif i_entry.is_dir( follow_symlinks=False ):
				i_new_subpath = im_osp.join( p_path, i_entry.name)
				countsbfe_traverse_recurse( i_new_subpath, r_dct_counts, i_multi_log )
	# main section
	i_multi_log.do_logs( "dbs", zz_procname + " Checking for : " + str( len( p_lst_folders ) ) + " folders." )
	i_dct_counts = {}
	i_path_count = 0
	for i_path in p_lst_folders :
		i_path_count += 1
		# print( "i_path" )
		# print( i_path )
		# i_multi_log.do_logs( "dbs", zz_procname + " Traversing path " + i_path )
		if cmntry.is_n_to_show_progess( i_path_count ):
			i_multi_log.do_logs( "dbs", zz_procname + " Scanning listed path #" + str( i_path_count ) + ": " + cmntry.pfn4print( i_path ) )
		countsbfe_traverse_recurse( i_path, i_dct_counts, i_multi_log )
	i_multi_log.do_logs( "rdbs", zz_procname + " Found: " + str( len( i_dct_counts ) ) + " filename extensions." )
	if len( i_dct_counts ) > 0 :
		i_multi_log.do_logs( "r", "Analysis of paths:" )
		i_this_run_infix = cmntry.bfff_datetime_ident_str()
		i_list_filename = cmntry.dtff_make_data_filename( i_this_run_infix, zz_procname + "_Folders" )
		cmntry.file_in_home_text_new_list_as_data_head( i_list_filename, ["path"] )
		for i_path in p_lst_folders :
			cmntry.file_in_home_text_add_list_as_data_line( i_list_filename, [ cmntry.pfn4print( i_path ) ] )
			i_multi_log.do_logs( "r", cmntry.pfn4print( i_path ) )
		i_data_filename = cmntry.dtff_make_data_filename( i_this_run_infix, zz_procname + "_Finds" )
		cmntry.file_in_home_text_new_list_as_data_head( i_data_filename, ["filename_ext", "count", "sizesum", "sizemin", "sizemax"] )
		# print( i_dct_counts )
		showing_all_below = 51
		i = 0
		# lst_keys_sorted = sorted( i_dct_counts, key=i_dct_counts.get) 
		lst_keys_sorted = i_dct_counts.keys()
		#lst_keys_sorted = sorted( i_dct_counts, key=i_dct_counts.get.ei_Count, reverse=True )
		lst_keys_sorted = sorted( i_dct_counts, key=fn_ei_Count_at_key( i_dct_counts), reverse=True )
		for k_ext in lst_keys_sorted :
			i_tpl = i_dct_counts[ k_ext ]
			# print( i_tpl )
			cmntry.file_in_home_text_add_list_as_data_line( i_data_filename, [ k_ext, str( i_tpl.ei_Count ), str( i_tpl.ei_SizeSum ), str( i_tpl.ei_SizeMin ), str( i_tpl.ei_SizeMax ) ] )
			i += 1
			if cmntry.is_n_to_show_progess_showing_all_below_x( i, showing_all_below ):
				log_ctrl_str = "rdb"
			else :
				log_ctrl_str = "db"
			# print( k_ext )
			# print( i_dct_counts[ k_ext ] )
			i_multi_log.do_logs( log_ctrl_str, k_ext + " " + str_of_tpl_Extn_Info( i_dct_counts[ k_ext ] ) )
			if len( lst_keys_sorted ) >= showing_all_below :
				i_see = " More extensions than displayed here."
			else :
				 i_see = ""
		i_multi_log.do_logs( "dbsr", zz_procname + i_see + " See full list in data file: " + i_data_filename )

# --------------------------------------------------
# Main - only if run as a program
# --------------------------------------------------

def setup_logging() :
	# this uses the custom wrapper module "multilog"
	m_log = cmntry.make_multilog_object( mod_code_abbr() )
	return m_log

# main 
if __name__ == "__main__":
	print( "This is the module: summatry")
	# setup logging
	i_multi_log = setup_logging()
	if __debug__:
		i_multi_log.do_logs( "b", "Invocation as Main")
	i_multi_log.do_logs( "dbs", mod_code_name() )
	i_d_folders = {}
	i_path = "/home/ger/Downloads"
	i_path = "/home/ger/Pictures"
	i_path = "/home/ger"
	i_path = "/home/sharelocal"
	le_tree = smmtry_traverse_tree( i_d_folders, i_path, i_multi_log )
	#smmtry_explore_tree( le_tree, i_multi_log )
	smmtry_gatekeep_tree( le_tree, i_multi_log )
	i_multi_log.do_logs( "dbs", mod_code_name() + " Completed!" )
