#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# clonatry = 
#
# --------------------------------------------------
# Copyright (C) 2018-2024  Gerald Waters - Melbourne, Australia
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# --------------------------------------------------
# Imports - Standard
# --------------------------------------------------

import shutil as im_shutil
import os as im_os
import os.path as im_osp

# --------------------------------------------------
# Imports - Custom
# --------------------------------------------------

import commontry as cmntry
import applitry as apltry

import exploratry as explrtry
from distinctry import distinctry_paths_command
from purgetry import purgetry_path_command
from purgetry import purgetry_file_command
from congruentry import congruency_paths_process

# --------------------------------------------------
# Customisations of stock modules
# --------------------------------------------------

# --------------------------------------------------
# Interior actions
# --------------------------------------------------

def path_termite( p_str_folderpath, p_EnumDeleteMode, p_multi_log):
	# "Clear the structure of files leaving the folders in place"
	zz_procname = "path_termite"
	i_use_datetime_str = cmntry.bfff_datetime_ident_str() 
	i_bashfilename_str = cmntry.scrf_make_bash_filename( i_use_datetime_str, zz_procname )
	i_lst_rem_ante = ""
	i_lst_rem_post = ""
	r_did_ok = True
	r_filcount = 0
	r_filesum = 0
	r_dircount = 0
	def path_recurse_termite( p_str_path ):
		# "Clear the structure of files leaving the folders in place"
		zz_procname = "path_recurse_termite"
		p_multi_log.do_logs( "sdb", zz_procname + " here for " + cmntry.pfn4print( p_str_path ) )
		nonlocal i_use_datetime_str
		nonlocal i_bashfilename_str
		nonlocal i_lst_rem_ante
		nonlocal i_lst_rem_post
		nonlocal r_did_ok
		nonlocal r_filcount
		nonlocal r_filesum
		nonlocal r_dircount
		entries = explrtry.get_lst_sorted_os_scandir( p_str_path )
		for entry in entries:
			if entry.is_file(follow_symlinks=False):
				# assemble specifics and derive hash
				pfn = im_osp.join( p_str_path, entry.name) 
				size = entry.stat().st_size
				if True:
					p_multi_log.do_logs( "sdb", zz_procname + " calling purgetry_file_command for " + cmntry.pfn4print( pfn ) )
					t_did_ok = purgetry_file_command( pfn, p_EnumDeleteMode, i_bashfilename_str, i_lst_rem_ante, i_lst_rem_post, p_multi_log)
				else :
					t_did_ok = True
					# an alternate size derivation is: 
					# size = im_os.path.getsize( pfn)
					if p_EnumDeleteMode == apltry.EnumDeleteMode.DeleteNow :
						try :
							im_os.remove( pfn)
							r_filcount += 1
							r_filesum += size
						except :
							t_did_ok = False
							break
						# logging
						if __debug__:
							if t_did_ok :
								p_multi_log.do_logs( "b", zz_procname + " Deleted file: " + cmntry.pfn4print( pfn ) )
							else :
								p_multi_log.do_logs( "b", zz_procname + " Could not delete file: " + cmntry.pfn4print( pfn ) )
					elif p_EnumDeleteMode == apltry.EnumDeleteMode.Bash :
						p_multi_log.do_logs( "db", zz_procname + " Have not yet implemented bash construction: " + cmntry.pfn4print( pfn ) )
					elif p_EnumDeleteMode == apltry.EnumDeleteMode.DryRun :
						p_multi_log.do_logs( "db", zz_procname + " But for Dry Run would have deleted: " + cmntry.pfn4print( pfn ) )
					else :
						p_multi_log.do_logs( "db", zz_procname + " Unhandled EnumDeleteMode for: " + cmntry.pfn4print( pfn ) )
				if t_did_ok :
					r_filcount += 1
					r_filesum += size
				else :
					r_did_ok = False
			elif entry.is_dir( follow_symlinks= False):
				new_subpath = im_os.path.join( p_str_path, entry.name)
				path_recurse_termite( new_subpath )
				r_dircount += 1
				# logging
				if __debug__:
					p_multi_log.do_logs( "b", zz_procname + " Processed sub-folder: " + cmntry.pfn4print( new_subpath ) )
	path_recurse_termite( p_str_folderpath )
	return r_did_ok, r_filcount, r_filesum, r_dircount

# --------------------------------------------------
# Action
# --------------------------------------------------

def copy_emptyfolders_of_folder_a_to_existing_empty_folder_b( p_str_path_a, p_str_path_b, p_multi_log):
	# use copy2 which is identical to copy() except that copy2() also attempts to preserve file metadata.
	zz_procname = "copy_emptyfolders_of_folder_a_to_existing_empty_folder_b"
	r_did_ok = True # set to True and will become False at any error
	# check that p_str_path_b is empty
	t_path_b_is_empty = explrtry.path_check_is_empty( p_str_path_b ) # returning True means the location exists and is empty
	if t_path_b_is_empty :
		if im_os.access( p_str_path_a, im_os.R_OK):
			i_entries = explrtry.get_lst_sorted_os_scandir( p_str_path_a )
		else:
			i_entries = []
		i_lst_folds = []
		# get the directory content lists but make a list of only the sub-folders
		for i_entry in i_entries:
			if i_entry.is_dir( follow_symlinks =False ):
				i_lst_folds.append( i_entry.name )
		# copy the folders
		for i_foldname in i_lst_folds :
			i_path_foldname_a = im_osp.join( p_str_path_a, i_foldname )
			i_path_foldname_b = im_osp.join( p_str_path_b, i_foldname )
			#print( cmntry.pfn4print( i_path_foldname_a ) )
			#print( cmntry.pfn4print( i_path_foldname_b ) )
			t_did_copy_ok = True
			t_copy_bad_msg = ""
			t_made_destination = ""
			try:
				im_os.mkdir( i_path_foldname_b)
			except im_shutil.SameFileError:
				# If source and destination are same
				t_did_copy_ok = False
				t_copy_bad_msg = "Source and destination represents the same file"
			except PermissionError:
				# If there is any permission issue
				t_did_copy_ok = False
				t_copy_bad_msg = "Permission denied"
			except:
				# For other errors
				t_did_copy_ok = False
				t_copy_bad_msg = "Unknown error" 
			if t_did_copy_ok :
				# now try to
				try:
					 im_shutil.copystat( i_path_foldname_a, i_path_foldname_b, follow_symlinks= False )
				except:
					# For other errors
					t_did_copy_ok = False
					t_copy_bad_msg = "copystat error" 
			if t_did_copy_ok :
				# time to recurse
				t_did_ok = copy_emptyfolders_of_folder_a_to_existing_empty_folder_b( i_path_foldname_a, i_path_foldname_b, p_multi_log)
				if not t_did_ok :
					r_did_ok = False
			else :
				r_did_ok = False # so the function will return False for any error throughout the process - a simple method for now
				p_multi_log.do_logs( "sb", zz_procname + " copytree failed because of " + t_copy_bad_msg + " from " + cmntry.pfn4print( t_path_filename_a ) + " to " + cmntry.pfn4print( t_path_filename_b ) )
	else :
		r_did_ok = False
		p_multi_log.do_logs( "sb", zz_procname + " not-empty destination " + cmntry.pfn4print( p_str_path_b ) )
	return r_did_ok

def copy_content_of_folder_a_to_existing_empty_folder_b( p_str_path_a, p_str_path_b, p_multi_log):
	# this is simpler because a new empty folder can not encounter clashes
	# use copy2 which is identical to copy() except that copy2() also attempts to preserve file metadata.
	zz_procname = "copy_content_of_folder_a_to_existing_empty_folder_b"
	p_multi_log.do_logs( "sb", zz_procname + " destination: " + cmntry.pfn4print( p_str_path_b ) )
	r_did_ok = True # set to True and will become False at any error
	# check that p_str_path_b is empty
	t_path_b_is_empty = explrtry.path_check_is_empty( p_str_path_b ) # returning True means the location exists and is empty
	if t_path_b_is_empty :
		p_multi_log.do_logs( "sb", zz_procname + " destination is indeed empty: " + cmntry.pfn4print( p_str_path_b ) )
		p_multi_log.do_logs( "sb", zz_procname + " getting contents of source folder: " + cmntry.pfn4print( p_str_path_a ) )
		if im_os.access( p_str_path_a, im_os.R_OK):
			i_lst_entries = explrtry.get_lst_sorted_os_scandir( p_str_path_a )
		else:
			i_lst_entries = []
		i_lst_files = []
		i_lst_folds = []
		p_multi_log.do_logs( "sb", zz_procname + " making list of files and list of folders" )
		for i_entry in i_lst_entries:
			p_multi_log.do_logs( "dbs", zz_procname + " Checkfor entry: " + i_entry.name )
			if i_entry.is_file( follow_symlinks =False):
				i_lst_files.append( i_entry.name )
			elif i_entry.is_dir( follow_symlinks =False ):
				i_lst_folds.append( i_entry.name )
		# copy all the files
		p_multi_log.do_logs( "sb", zz_procname + " copying the files" )
		for i_filename in i_lst_files :
			t_path_filename_a = im_osp.join( p_str_path_a, i_filename )
			t_path_filename_b = im_osp.join( p_str_path_b, i_filename )
			p_multi_log.do_logs( "sb", zz_procname + " copy: " + cmntry.pfn4print( t_path_filename_a ) )
			p_multi_log.do_logs( "sb", zz_procname + " tobe: " + cmntry.pfn4print( t_path_filename_b ) )
			t_did_copy_ok = True
			t_copy_bad_msg = ""
			try:
				im_shutil.copy2( t_path_filename_a, t_path_filename_b, follow_symlinks=False )
				# print("File copied successfully.")
			except im_shutil.SameFileError:
				# If source and destination are same
				t_did_copy_ok = False
				t_copy_bad_msg = "Source and destination represents the same file"
			except PermissionError:
				# If there is any permission issue
				t_did_copy_ok = False
				t_copy_bad_msg = "Permission denied"
			except:
				# For other errors
				t_did_copy_ok = False
				t_copy_bad_msg = "Unknown error" 
			if not t_did_copy_ok :
				r_did_ok = False # so the function will return False for any error throughout the process - a simple method for now
				p_multi_log.do_logs( "sb", zz_procname + " copy2 failed because of " + t_copy_bad_msg + " from " + cmntry.pfn4print( t_path_filename_a ) + " to " + cmntry.pfn4print( t_path_filename_b ) )
		# copy the folders
		p_multi_log.do_logs( "sb", zz_procname + " copying the folders" )
		for i_foldname in i_lst_folds :
			t_path_foldname_a = im_osp.join( p_str_path_a, i_foldname )
			t_path_foldname_b = im_osp.join( p_str_path_b, i_foldname )
			#print( zz_procname + " im_shutil.copytree " )
			#print( cmntry.pfn4print( t_path_foldname_a ) )
			#print( "to" )
			#print( cmntry.pfn4print( t_path_foldname_b ) )
			p_multi_log.do_logs( "sb", zz_procname + " copy: " + cmntry.pfn4print( t_path_foldname_a ) )
			p_multi_log.do_logs( "sb", zz_procname + " tobe: " + cmntry.pfn4print( t_path_foldname_b ) )
			t_did_copy_ok = True
			t_copy_bad_msg = ""
			t_made_destination = ""
			try:
				t_made_destination = im_shutil.copytree( t_path_foldname_a, t_path_foldname_b, \
					symlinks= False, ignore= None, copy_function= im_shutil.copy2, ignore_dangling_symlinks= True, dirs_exist_ok= False )
			except im_shutil.SameFileError:
				# If source and destination are same
				t_did_copy_ok = False
				t_copy_bad_msg = "Source and destination represents the same file"
			except PermissionError:
				# If there is any permission issue
				t_did_copy_ok = False
				t_copy_bad_msg = "Permission denied"
			except:
				# For other errors
				t_did_copy_ok = False
				t_copy_bad_msg = "Unknown error" 
			if len( t_made_destination ) == 0 :
				t_did_copy_ok = False
				t_copy_bad_msg = "returned blank"
			if not t_did_copy_ok :
				r_did_ok = False # so the function will return False for any error throughout the process - a simple method for now
				p_multi_log.do_logs( "sb", zz_procname + " copytree failed because of " + t_copy_bad_msg + " from " + cmntry.pfn4print( t_path_filename_a ) + " to " + cmntry.pfn4print( t_path_filename_b ) )
	else :
		r_did_ok = False
		p_multi_log.do_logs( "sb", zz_procname + " not-empty destination " + cmntry.pfn4print( p_str_path_b ) )
	return r_did_ok

# --------------------------------------------------
# Externally callable Commands
# --------------------------------------------------
# 
def distinctry_command( p_str_path_a, p_str_path_b, p_str_path_c, \
		p_RecreateSrcFldr, p_KeepTuch, p_KeepAttr, p_KeepPrmt, p_KeepMeta, p_fn_Check_NoInterruption, p_multi_log):
	"""
	Distinctry - (distinct tree) - copy the things in A that are not in B, to C - see separate note just about that. Really no apparent good name, so at least this one is distinct
	"""
	zz_procname = "distinctry_command"
	p_multi_log.do_logs( "spdb", zz_procname + " Calling distinctry_paths_command" )
	distinctry_paths_command( p_str_path_a, p_str_path_b, p_str_path_c, p_fn_Check_NoInterruption, p_multi_log)

def clonatry_command( p_str_path_a, p_str_path_b, \
		p_RecreateSrcFldr, p_KeepTuch, p_KeepAttr, p_KeepPrmt, p_KeepMeta, p_multi_log):
	"""
	Clonatry - (clone a tree) - copy to new location - after the operation, it should be possible to use Congruentry to prove that the copy was all good.
	"""
	zz_procname = "clonatry_command"
	p_multi_log.do_logs( "spdb", zz_procname + " TESTING IMPLEMENTATION !" )
	p_multi_log.do_logs( "spdb", zz_procname + " We are presuming the required safety testing is done BEFORE this function is called." )
	p_multi_log.do_logs( "spdb", zz_procname + " p_str_path_a is " + cmntry.pfn4print( p_str_path_a ) )
	p_multi_log.do_logs( "spdb", zz_procname + " p_str_path_b is " + cmntry.pfn4print( p_str_path_b ) )
	i_did_ok = copy_content_of_folder_a_to_existing_empty_folder_b( p_str_path_a, p_str_path_b, p_multi_log)
	if i_did_ok :
		p_multi_log.do_logs( "srpdb", zz_procname + " Made copy ok " )
	else :
		p_multi_log.do_logs( "srpdb", zz_procname + " Failed to copy tree in some way." )

def dubatry_command( p_str_path_a, p_str_path_b, p_multi_log):
	"""
	Dubatry (dub a tree) - copy to location but only where non-clashing - core idea is that no files are deleted by this process. Here the name is borrowed from music mixing where a dub is an addition to the existing sound. For this, the brief is that all files and folders at the target do not get overwritten. Might need an option for whether a folder clash leads to new folders.
	"""
	zz_procname = "dubatry_command"
	p_multi_log.do_logs( "spdb", zz_procname + " NOT YET IMPLEMENTED !" )

def mergetry_command( p_str_path_a, p_str_path_b, p_multi_log):
	"""
	Mergetry - (merge a tree) - copy into perhaps existing structures with over-writing, or maybe automated renaming of files at the target - after this operation it should be possible to do a moiety run of Congruentry to prove that the process was good.
	"""
	zz_procname = "mergetry_command"
	p_multi_log.do_logs( "spdb", zz_procname + " NOT YET IMPLEMENTED !" )

def synopsitry_command( p_str_path_a, p_str_path_b, \
		p_RecreateSrcFldr, p_KeepTuch, p_KeepAttr, p_KeepPrmt, p_KeepMeta, p_multi_log):
	"""
	Synopsitry (synopsis of a tree) - copy folder structures but not the files -  i.e. create a synopsis
	"""
	zz_procname = "synopsitry_command"
	p_multi_log.do_logs( "spdb", zz_procname + " TESTING IMPLEMENTATION !" )
	p_multi_log.do_logs( "spdb", zz_procname + " We are presuming the required safety testing is done BEFORE this function is called." )
	p_multi_log.do_logs( "spdb", zz_procname + " p_str_path_a is " + cmntry.pfn4print( p_str_path_a ) )
	p_multi_log.do_logs( "spdb", zz_procname + " p_str_path_b is " + cmntry.pfn4print( p_str_path_b ) )
	i_did_ok = copy_emptyfolders_of_folder_a_to_existing_empty_folder_b( p_str_path_a, p_str_path_b, p_multi_log)
	if i_did_ok :
		p_multi_log.do_logs( "srpdb", zz_procname + " Made copy ok " )
	else :
		p_multi_log.do_logs( "srpdb", zz_procname + " Failed to copy tree in some way." )

def skeletry_command( p_str_path, p_EnumDeleteMode, p_multi_log):
	"""
	Skeletry - (skeleton a tree) - delete files but not the folder structures - i.e. reduce to a skeleton
	"""
	zz_procname = "skeletry_command"
	p_multi_log.do_logs( "spdb", zz_procname + " Calling path_termite" )
	i_did_ok, i_filcount, i_filesum, i_dircount = path_termite( p_str_path, p_EnumDeleteMode, p_multi_log)

def migratry_command( p_str_path_a, p_str_path_b, \
		p_RecreateSrcFldr, p_KeepTuch, p_KeepAttr, p_KeepPrmt, p_KeepMeta, p_multi_log):
	"""
	Migratry - (migrate a tree) - move folders and files, leaving behind the folder structures - the anlogy being how people migrate leaving gaps where they used to live
	The current implementation presumes we are copying into an empty location, notably so that Clonatry can be use to prove copy fidelity
	In effect, this is a matter of doing:
	- clonatry_command on p_str_path_a, p_str_path_b
	- congrentry on p_str_path_a, p_str_path_b
	- skeletry_command on p_str_path_b
	"""
	zz_procname = "migratry_command"
	if False :
		p_multi_log.do_logs( "spdb", zz_procname + " NOT YET IMPLEMENTED !" )
	else :
		p_multi_log.do_logs( "spdb", zz_procname + " Attempting to Clonatry" )
		i_did_clone_ok = copy_content_of_folder_a_to_existing_empty_folder_b( p_str_path_a, p_str_path_b, p_multi_log)
		if i_did_clone_ok :
			p_multi_log.do_logs( "spdb", zz_procname + " Clonatry seemed ok, so checking with Congruentry" )
			# for now, just the default FnameMatchSetting, which for reference is probably still (i.e. you should check
			# EnumCharEnc.ChEnc_UTF8, DoCaseSensitive= True, DoMsBadNames= False, DoMsBadChars= False, DoMs8p3= False
			i_FnameMatchSetting_A = apltry.FnameMatchSetting()
			i_FnameMatchSetting_B = apltry.FnameMatchSetting()
			# for now, manually set things
			i_Stringent = True
			i_StopNotCongruent = True
			i_StopNotMoiety = True
			i_ReportDiffs = False
			i_SummaryDiffs = False
			i_ReportFailuresStringent = False
			i_GenCopyScript = False
			i_chckd_congruent, i_chckd_subset, i_chckd_extra_side = congruency_paths_process( p_str_path_a, p_str_path_b, i_FnameMatchSetting_A, i_FnameMatchSetting_B, \
					i_Stringent, i_StopNotCongruent, i_StopNotMoiety, i_ReportDiffs, i_SummaryDiffs, i_ReportFailuresStringent, i_GenCopyScript, \
					p_multi_log)
			if i_chckd_congruent :
				p_multi_log.do_logs( "spdb", zz_procname + " Congruentry proven, so attempting to Skeletry" )
				i_EnumDeleteMode = apltry.EnumDeleteMode.DeleteNow
				i_did_termite_ok, i_filcount, i_filesum, i_dircount = path_termite( p_str_path_a, i_EnumDeleteMode, p_multi_log)
				if i_did_termite_ok:
					p_multi_log.do_logs( "spdb", zz_procname + " Skeletry successful, hence Migratry successful" )
				else :
					p_multi_log.do_logs( "spdb", zz_procname + " Skeletry failure, hence Migratry unsuccessful" )
			else :
				p_multi_log.do_logs( "spdb", zz_procname + " Congruentry failure, hence Migratry unsuccessful" )
		else :
			p_multi_log.do_logs( "spdb", zz_procname + " Clonatry failure, hence Migratry unsuccessful" )

def movetry_command_content_of_one_folder( p_str_path_a, p_str_path_b, p_DeleteDestinationOnFailure, \
		p_KeepTuch, p_KeepAttr, p_KeepPrmt, p_KeepMeta, p_multi_log):
	"""
	Movetry - (move a tree) - ordinary combination of copy and then delete source - i.e. requires enough space at the target to hold it all, then prove intact before removal of source
	This is the content_of_one_folder version
	This presumes the destination folder exists and is empty and writable
	Note: it is available for use from outside this module, but mainly exists for use by the following function
	"""
	zz_procname = "movetry_command_content_of_one_folder"
	r_bad_msg = "No problems."
	def internal_purge_folder( p_str_path ):
		i_EnumDeleteMode = apltry.EnumDeleteMode.DeleteNow
		i_bashfilename_str = ""
		i_size = -1 # for now, just pass a dummy value for display/advice
		r_did_purge_ok = purgetry_path_command( p_str_path, i_EnumDeleteMode, i_bashfilename_str, i_size, p_multi_log)
		return r_did_purge_ok
	def internal_attempt_unroll():
		nonlocal r_bad_msg
		if p_DeleteDestinationOnFailure:
			r_bad_msg += " Deleting failed destination copy."
			p_multi_log.do_logs( "spdb", zz_procname + " " + r_bad_msg )
			i_did_purge_ok = internal_purge_folder( p_str_path_b )
			if i_did_purge_ok :
				r_bad_msg += " Deleted ok."
			else:
				r_bad_msg += " But did not delete."
		else:
			r_bad_msg += " Leaving failed destination copy in place."
			p_multi_log.do_logs( "spdb", zz_procname + " " + r_bad_msg )
		p_multi_log.do_logs( "spdb", zz_procname + " " + r_bad_msg )
	r_ran_ok = True
	r_did_ok = False
	r_lst_overhang_folds_full = [] # list of complete folders that perhaps now need to be deleted
	r_lst_overhang_folds_part = [] # list of complete folders that perhaps now need to be deleted
	r_lst_overhang_files = [] # list of flies that perhaps now need to be deleted
	p_multi_log.do_logs( "spdb", zz_procname )
	p_multi_log.do_logs( "spdb", "From: " + cmntry.pfn4print( p_str_path_a) )
	p_multi_log.do_logs( "spdb", "Into: " + cmntry.pfn4print( p_str_path_b) )
	p_multi_log.do_logs( "spdb", zz_procname + " Copying..." )
	i_did_copy_ok = copy_content_of_folder_a_to_existing_empty_folder_b( p_str_path_a, p_str_path_b, p_multi_log)
	p_multi_log.do_logs( "spdb", zz_procname + " after copying .." )
	if i_did_copy_ok:
		# now check for congruence
		p_multi_log.do_logs( "spdb", zz_procname + " checking for congruence .." )
		i_FnameMatchSetting_A = apltry.FnameMatchSetting()
		i_FnameMatchSetting_B = apltry.FnameMatchSetting()
		i_Stringent = True
		i_StopNotCongruent = True 
		i_StopNotMoiety = True 
		i_ReportDiffs = False
		i_SummaryDiffs = False 
		i_ReportFailuresStringent = False
		i_GenCopyScript = False
		i_copy_was_perfect, i_chckd_subset, i_chckd_extra_side = congruency_paths_process( p_str_path_a, p_str_path_b, i_FnameMatchSetting_A, i_FnameMatchSetting_B, \
			i_Stringent, i_StopNotCongruent, i_StopNotMoiety, i_ReportDiffs, i_SummaryDiffs, i_ReportFailuresStringent, i_GenCopyScript, \
			p_multi_log)
		p_multi_log.do_logs( "spdb", zz_procname + " after checking .." )
		if i_copy_was_perfect:
			# delete the source folder
			p_multi_log.do_logs( "spdb", zz_procname + " checked ok, so now purging .." )
			i_did_purge_ok = internal_purge_folder( p_str_path_a )
			r_did_ok = i_did_purge_ok
		else:
			p_multi_log.do_logs( "spdb", zz_procname + " check failed .." )
			# Replace the No Problems message
			r_bad_msg = "Copy wasn't perfect."
			internal_attempt_unroll()
	else:
		p_multi_log.do_logs( "spdb", zz_procname + " copying failed .." )
		r_ran_ok = False
		r_did_ok = False
		# Replace the No Problems message
		r_bad_msg = "Failed copying to destination."
		internal_attempt_unroll()
	return r_ran_ok, r_did_ok, r_bad_msg

def movetry_command_list_of_folders( p_lst_paths_a, p_str_path_b, p_AbandonAtFirstFailure, p_DeleteDestinationOnFailure, \
		p_KeepTuch, p_KeepAttr, p_KeepPrmt, p_KeepMeta, p_multi_log):
	"""
	Movetry - (move a tree) - ordinary combination of copy and then delete source - i.e. requires enough space at the target to hold it all, then prove intact before removal of source
	This is the list of folders version
	For each listed folder, a corresponding folder will be created in the destination
	"""
	zz_procname = "movetry_command_list_of_folders"
	p_multi_log.do_logs( "spdb", zz_procname )
	p_multi_log.do_logs( "spdb", "Into: " + cmntry.pfn4print( p_str_path_b) )
	for i_path in p_lst_paths_a :
		p_multi_log.do_logs( "spdb", "From: " + cmntry.pfn4print( i_path) )
	# set the return values, in case we got passed an empty list
	r_ran_ok = True
	r_did_ok = True
	r_bad_msg = ""
	for i_path in p_lst_paths_a :
		# see if there is already a destination
		i_path_foldername = im_osp.basename( i_path )
		i_dest_pathfolder = im_osp.join( p_str_path_b, i_path_foldername )
		p_multi_log.do_logs( "spdb", "Tackling folder " + cmntry.pfn4print( i_path ) )
		p_multi_log.do_logs( "spdb", "To go into folder " + cmntry.pfn4print( i_dest_pathfolder) )
		if im_osp.exists( i_dest_pathfolder ) :
			p_multi_log.do_logs( "spdb", "Destination already exists, checking it for empty.." )
			looks_ok = explrtry.path_check_is_empty( i_dest_pathfolder )
			if looks_ok :
				p_multi_log.do_logs( "spdb", "Yes it is empty." )
			else :
				p_multi_log.do_logs( "spdb", "No it is not empty!" )
		else :
			p_multi_log.do_logs( "spdb", "Destination does not exist, trying to make it.." )
			p_multi_log.do_logs( "spdb", cmntry.pfn4print( i_dest_pathfolder) )
			# make an empty one
			try:
				im_os.mkdir( i_dest_pathfolder)
				looks_ok = True
				p_multi_log.do_logs( "spdb", "Made destination folder." )
			except:
				looks_ok = False
				p_multi_log.do_logs( "spdb", "Failed to make destination folder!" )
		if looks_ok :
			p_multi_log.do_logs( "spdb", "All good, moving folder "  + cmntry.pfn4print( i_path_foldername ) )
			i_ran_ok, i_did_ok, i_bad_msg = movetry_command_content_of_one_folder( i_path, i_dest_pathfolder, p_DeleteDestinationOnFailure, \
				p_KeepTuch, p_KeepAttr, p_KeepPrmt, p_KeepMeta, p_multi_log)
		else :
			i_ran_ok = False
			i_did_ok = False
			i_bad_msg = "Could not establish and empty destination at " + cmntry.pfn4print( i_dest_pathfolder)
		if i_ran_ok :
			if i_did_ok :
				p_multi_log.do_logs( "spdb", "Moved folder "  + cmntry.pfn4print( i_path) )
			r_did_ok = r_did_ok and i_did_ok
		else :
			r_ran_ok = False
			r_bad_msg += "\n" + i_bad_msg
			if p_AbandonAtFirstFailure :
				break
	return r_ran_ok, r_did_ok, r_bad_msg

def shiftatry_command( p_str_path_a, p_str_path_b, p_multi_log):
	"""
	Shiftatry - (shift a tree) - relocate by multiple sub-shifts - the idea here is to minimise the free space required at the target (to be the size of the largest file)
	"""
	zz_procname = "shiftatry_command"
	p_multi_log.do_logs( "spdb", zz_procname + " NOT YET IMPLEMENTED !" )

def shiftafile_command( p_str_file, p_str_path, p_multi_log):
	"""
	Shiftafile - (shift a file) - relocate by multiple sub-shifts of internal parts of a file - a much more dubious kind of operation, as during the process there is no longer an intact file at either end. This will require considerable extra attention to make bullet-proof.
	"""
	zz_procname = "shiftafile_command"
	p_multi_log.do_logs( "spdb", zz_procname + " NOT YET IMPLEMENTED !" )

# --------------------------------------------------
# Main - only if run as a program
# --------------------------------------------------

# main 
if __name__ == "__main__":
	print( "This is the module: clonatry")
	print( "Currently no testing is setup for this module.")
