# codegen = internal code generation for Foldatry 
#
# --------------------------------------------------
# Copyright (C) 2024  Gerald Waters - Melbourne, Australia
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# This module exists purely for the purpose of auto-generating Python 
# program code for then implanting elsewhere in the source.

# At initial creation, this will be just for the gradual implementation
# of various Settings

#=======================================================================
# Settings coverage

# See the module applitry for the definition of the enumeration:
# - 

def ForSettings_Generate_Python_Function_GetSetting( p_EnumSettingDef ):
	boilerplate = """	def GetSettingFromGui_1t3m():
		r_setting_value = (tkinter_widget.get() == 1 )
		r_isok = True
		return r_isok, r_setting_value"""
	r_str = boilerplate.replace("1t3m", p_EnumSettingDef.name)
	return r_str

def ForSettings_Generate_Python_Line_GetSetting_AssignFunction( p_EnumSettingDef ):
	boilerplate = "		r_dct_Fn[ apltry.EnumSettingDef.1t3m ] = GetSettingFromGui_1t3m"
	r_str = boilerplate.replace("1t3m", p_EnumSettingDef.name)
	return r_str

def ForSettings_Generate_Python_Lines_TkWidget_AddToTrackingDictionary( p_EnumSettingDef ):
	boilerplate = """	if __debug__ : # declare this to be a Settings object
		Set_ESD_EMSA( apltry.EnumSettingDef.1t3m, EnumMetaSettingAspect.WidgetObject, tkinter_widget)"""
	r_str = boilerplate.replace("1t3m", p_EnumSettingDef.name)
	return r_str

def ForSettings_Generate_Python_Function_SetSetting( p_EnumSettingDef ):
	boilerplate = """	def SetSettingIntoGui_1t3m( p_val ):
		tkinter_widget.set( p_val )"""
	r_str = boilerplate.replace("1t3m", p_EnumSettingDef.name)
	return r_str

def ForSettings_Generate_Python_Line_SetSetting_AssignFunction( p_EnumSettingDef ):
	boilerplate = "		r_dct_Fn[ apltry.EnumSettingDef.1t3m ] = SetSettingIntoGui_1t3m"
	r_str = boilerplate.replace("1t3m", p_EnumSettingDef.name)
	return r_str

def ForSettings_Generate_Python_Function_GetSetting_ForCommandLine( p_EnumSettingDef ):
	boilerplate = """			i_1t3m = FldtryCLI_Settings.Get_SettingValue( apltry.EnumSettingDef.1t3m )"""
	r_str = boilerplate.replace("1t3m", p_EnumSettingDef.name)
	return r_str

# Usage:
# ForSettings_Generate_Python_Function_GetSetting( p_EnumSettingDef ):
# ForSettings_Generate_Python_Line_GetSetting_AssignFunction( p_EnumSettingDef ):
# ForSettings_Generate_Python_Lines_TkWidget_AddToTrackingDictionary( p_EnumSettingDef )
# ForSettings_Generate_Python_Function_SetSetting( p_EnumSettingDef ):
# ForSettings_Generate_Python_Line_SetSetting_AssignFunction( p_EnumSettingDef ):

#=======================================================================
