#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# veritry = methods for verifying files are identical across trees
#
# This forms part of the foldatry application but can be used as an
# independent command line tool.
#
# --------------------------------------------------
# Copyright (C) 2018-2024  Gerald Waters - Melbourne, Australia
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# ==================================================
# Imports - Standard
# --------------------------------------------------

import sys as im_sys
import os as im_os
# import os.path as osp
import operator

import pathlib as pthlb

import copy as im_copy

# ==================================================
# Imports - Custom
# --------------------------------------------------

import commontry as cmntry
import applitry as apltry

import exploratry as explrtry

from matchsubtry import matchsubtry_command

from congruentry import congruentry_paths_action
from congruentry import congruentry_files_action

from filedupetry import filedupetry_collect
from filedupetry import hasherdabbery

# ==================================================
# Support Stock
# --------------------------------------------------

def ThisIsDeprecated( p_function_name ):
	print( "Function " + p_function_name + "is DEPRECATED !")
	print( "Which means it should not have been called."  )
	input( "Press ENTER to continue regardless, or do Ctrl-C to break and inspect the call stack."  )

# ==================================================
# Support Custom
# --------------------------------------------------

def mod_code_name():
	return "veritry"

def mod_code_abbr():
	return "vrtry"

def mod_show_name():
	return "Veritry"

# --------------------------------------------------
# Support Custom
# --------------------------------------------------

# ==================================================
# Features - i.e. requiring awareness of custom structures
# --------------------------------------------------

# --------------------------------------------------
# Approach == as Lists of Folders
# --------------------------------------------------

def does_lst_pfn_have_both_PathSetA_and_PathSetB( p_lst_pfn, p_lst_paths_PathSetB, p_lst_paths_PathSetA ):
	# Brief:
	# for a list of file references, determine if there is both some in a PathSetB path and some in a PathSetA path
	# and get back which are in which set 
	# Parameters:
	# - p_lst_pfn :: list := list of parth-filenames being checked and/or split
	# - p_lst_paths_PathSetB :: list := list of path strings - i.e. potential parent folders
	# - p_lst_paths_PathSetA :: list := list of path strings - i.e. potential parent folders
	# Returns:
	# - in_both :: boolean
	# - r_lst_PathSetB
	# - r_lst_PathSetA
	# Notes:
	# Code:
	log_strl_str = "b"
	log_prefix = "DTFLHSVD_"
	#p_multi_log.do_logs( "dbs", "does_lst_pfn_have_both_PathSetA_and_PathSetB" )
	#p_multi_log.do_logs( "b", log_prefix + "len of p_lst_foldertpls is " + str(len(p_lst_pfn)) )
	r_lst_PathSetB = []
	r_lst_PathSetA = []
	in_PathSetB_any = False
	in_PathSetA_any = False
	in_both = False
	i = 0
	for i_pathfile in p_lst_pfn :
		i = i + 1
		s_i = "(" + str(i) + ")"
		#p_multi_log.do_logs( "b", log_prefix + s_i + "testing pathfile: " + i_pathfile )
		# PathSetB
		in_PathSetB = False
		j = 0
		for f_PathSetB in p_lst_paths_PathSetB :
			j = j + 1
			s_j = "(s" + str(j) + ")"
			#p_multi_log.do_logs( log_strl_str, log_prefix + s_i + s_j + "-against PathSetB: " + f_PathSetB )
			sbfldrnss_PathSetB = explrtry.subfolderness_a_b( i_pathfile, f_PathSetB )
			in_this_PathSetB = sbfldrnss_PathSetB in ["a", "="]
			#p_multi_log.do_logs( log_strl_str, log_prefix + s_i + s_j + "in_this_PathSetB: " + str(in_this_PathSetB) )
			in_PathSetB = in_PathSetB or in_this_PathSetB
			#p_multi_log.do_logs( log_strl_str, log_prefix + s_i + s_j + "in_PathSetB: " + str(in_PathSetB) )
		if in_PathSetB :
			r_lst_PathSetB.append( i_pathfile)
		in_PathSetB_any = in_PathSetB_any or in_PathSetB
		#p_multi_log.do_logs( log_strl_str, log_prefix + s_i + "in_PathSetB_any: " + str(in_PathSetB_any) )
		# PathSetA
		in_PathSetA = False
		j = 0
		for f_PathSetA in p_lst_paths_PathSetA :
			j = j + 1
			s_j = "(d" + str(j) + ")"
			#p_multi_log.do_logs( log_strl_str, log_prefix + s_i + s_j + "against PathSetA: " + f_PathSetA )
			sbfldrnss_PathSetA = explrtry.subfolderness_a_b( i_pathfile, f_PathSetA )
			in_this_PathSetA = sbfldrnss_PathSetA in ["a", "="]
			#p_multi_log.do_logs( log_strl_str, log_prefix + s_i + s_j + "in_this_PathSetA: " + str(in_this_PathSetA) )
			in_PathSetA = in_PathSetA or in_this_PathSetA
			#p_multi_log.do_logs( log_strl_str, log_prefix + s_i + s_j + "in_PathSetA: " + str(in_PathSetA) )
		if in_PathSetA :
			r_lst_PathSetA.append( i_pathfile)
		in_PathSetA_any = in_PathSetA_any or in_PathSetA
		#p_multi_log.do_logs( log_strl_str, log_prefix + s_i + "in_PathSetA_any: " + str(in_PathSetA_any) )
	r_in_both = in_PathSetB_any and in_PathSetA_any
	if r_in_both :
		pass
		#p_multi_log.do_logs( log_strl_str, log_prefix + "in both overall: " + str( r_in_both))
	return r_in_both, r_lst_PathSetB, r_lst_PathSetA

def check_if_stringent( p_lst_pfn_PathSetB, p_lst_pfn_PathSetA, p_congruentstringent, p_ReportSuccessStringent, p_ReportFailuresStringent, \
	p_datafilename_same_str, p_datafilename_diff_str, p_multi_log ):
	# Brief:
	# this operates on lists of identical files
	# which merely need a final check before deleting those in the PathSetA list
	# much of the time each list will be a single file but the method is able to cope with more
	# Parameters:
	# - p_lst_pfn_PathSetB :: list :=
	# - p_lst_pfn_PathSetA :: list :=
	# - p_enum_method :: DeleteMode := enumeration from commontry
	# Returns:
	# :: boolean := can_erode
	# :: boolean := did_erode
	# Notes:
	# Code:
	p_multi_log.do_logs( "db", "check_if_stringent" )
	len_p_lst_pfn_PathSetB = len( p_lst_pfn_PathSetB)
	len_p_lst_pfn_PathSetA = len( p_lst_pfn_PathSetA)
	p_multi_log.do_logs( "db", "p_lst_pfn_PathSetB" + " has " + str( len_p_lst_pfn_PathSetB) )
	p_multi_log.do_logs( "db", "p_lst_pfn_PathSetA" + " has " + str( len_p_lst_pfn_PathSetA) )
	r_Lst_StringentSuccess = []
	r_Lst_StringentFailures = []
	if ( len_p_lst_pfn_PathSetB > 0 ) and ( len_p_lst_pfn_PathSetA > 0 ) :
		# as assume other processes have decided these are all identical just use the first PathSetB in list
		i_pfn_PathSetB = p_lst_pfn_PathSetB[0]  # note the [0]
		i_counter = 0
		for i_pfn_PathSetA in p_lst_pfn_PathSetA :
			i_counter += 1
			p_multi_log.do_logs( "db", "Within match, am at " + str( i_counter ) + " of " + str( len_p_lst_pfn_PathSetA ) )
			if p_congruentstringent :
				p_multi_log.do_logs( "db", "Calling congruentry_files_action" + " for:" )
				p_multi_log.do_logs( "db", "PathSetA: " + cmntry.pfn4print( i_pfn_PathSetA ) )
				p_multi_log.do_logs( "db", "PathSetB: " + cmntry.pfn4print( i_pfn_PathSetB ) )
				is_match = congruentry_files_action( i_pfn_PathSetB, i_pfn_PathSetA, p_multi_log)
				if is_match :
					if p_ReportSuccessStringent :
						r_Lst_StringentSuccess.append( ( i_pfn_PathSetB, i_pfn_PathSetA ) )
						cmntry.file_in_home_text_add_list_as_data_line( p_datafilename_same_str, cmntry.lst_pfn4print_of_lst_pfn( [ i_pfn_PathSetB, i_pfn_PathSetA ] ) )
				elif p_ReportFailuresStringent :
					r_Lst_StringentFailures.append( ( i_pfn_PathSetB, i_pfn_PathSetA ) )
					cmntry.file_in_home_text_add_list_as_data_line( p_datafilename_diff_str, cmntry.lst_pfn4print_of_lst_pfn( [ i_pfn_PathSetB, i_pfn_PathSetA ] ) )
			else :
				# note that the value of this kind of check is debatable
				is_match = True
				# might later replace this with a call to check only via metadata, but that idea begs a few questions
			p_multi_log.do_logs( "db", "Got checked_congruent: " + str(is_match) )
	return r_Lst_StringentSuccess, r_Lst_StringentFailures

# -------- replacements section

def GetSizeOfExampleInList( p_dct_pfn_filemeta, p_lst_pfn ):
	r_size = None
	if len( p_lst_pfn ) > 0 :
		i_pfn = p_lst_pfn[ 0]
		i_pfn_filemeta = p_dct_pfn_filemeta[ i_pfn ]
		r_size = i_pfn_filemeta.fi_fsize
	return r_size
def GetWhenOfExampleInList( p_dct_pfn_filemeta, p_lst_pfn ):
	r_when = None
	if len( p_lst_pfn ) > 0 :
		i_pfn_filemeta = p_dct_pfn_filemeta[ p_lst_pfn[ 0] ]
		r_when = i_pfn_filemeta.fi_fwhen
	return r_when
def GetNameOfExampleInList( p_dct_pfn_filemeta, p_lst_pfn ):
	r_name = None
	if len( p_lst_pfn ) > 0 :
		i_pfn_filemeta = p_dct_pfn_filemeta[ p_lst_pfn[ 0] ]
		r_name = i_pfn_filemeta.fi_fname
	return r_name

def DelOrderMode_DeriveSortValueForKeyTuple( p_keytuple, p_DelOrderMode, i_dct_tuplekey_lst_pfn, p_dct_pfn_filemeta ):
	r_value = None
	if p_DelOrderMode == apltry.EnumDelOrder.DelFromBig or p_DelOrderMode == apltry.EnumDelOrder.DelSmaller :
		r_value = GetSizeOfExampleInList( p_dct_pfn_filemeta, i_dct_tuplekey_lst_pfn[ p_keytuple ] )
	elif p_DelOrderMode == apltry.EnumDelOrder.DelOldest or p_DelOrderMode == apltry.EnumDelOrder.DelNewest :
		r_value = GetWhenOfExampleInList( p_dct_pfn_filemeta, i_dct_tuplekey_lst_pfn[ p_keytuple ] )
	elif p_DelOrderMode == apltry.EnumDelOrder.DelAmost or p_DelOrderMode == apltry.EnumDelOrder.DelZmost :
		r_value = GetNameOfExampleInList( p_dct_pfn_filemeta, i_dct_tuplekey_lst_pfn[ p_keytuple ] )
	else :
		print( "what the?")
		print( "p_keytuple")
		print( p_keytuple)
		print( "p_DelOrderMode")
		print( p_DelOrderMode)
		print( "i_dct_tuplekey_lst_pfn")
		print( i_dct_tuplekey_lst_pfn)
		print( "p_dct_pfn_filemeta")
		print( p_dct_pfn_filemeta)
	return r_value

# this is a more generic Reverse boolean function
def DelOrderMode_DeriveSortReverse( p_DelOrderMode ):
	r_reverse = False
	if p_DelOrderMode == apltry.EnumDelOrder.DelFromBig :
		r_reverse = True
	elif p_DelOrderMode == apltry.EnumDelOrder.DelSmaller :
		r_reverse = False
	elif p_DelOrderMode == apltry.EnumDelOrder.DelOldest :
		r_reverse = False
	elif p_DelOrderMode == apltry.EnumDelOrder.DelNewest :
		r_reverse = False
	elif p_DelOrderMode == apltry.EnumDelOrder.DelAmost :
		r_reverse = False
	elif p_DelOrderMode == apltry.EnumDelOrder.DelZmost :
		r_reverse = False
	return r_reverse

def GetSizeTotalForListOfKeys( p_dct_matchkeytuple_lst_pfn, p_dct_pfn_filemeta ):
	r_size_sum = 0
	for i_key in p_dct_matchkeytuple_lst_pfn :
		i_lst_pfn = p_dct_matchkeytuple_lst_pfn[ i_key ]
		i_size = GetSizeOfExampleInList( p_dct_pfn_filemeta, i_lst_pfn )
		r_size_sum += i_size
	return r_size_sum

def process_checking_by_strategy_in_order( \
		p_lst_paths_PathSetB, p_lst_paths_PathSetA, \
		p_dct_pfn_filemeta, p_dct_shortmatchkey_lst_pfn, p_dct_fullmatchkey_lst_pfn, \
		p_FileFlags, \
		p_congruentstringent, p_ReportSuccessStringent, p_ReportFailuresStringent, p_DelOrderMode, \
		p_datafilename_same_str, p_datafilename_diff_str, p_multi_log ) :
	# Brief:
	# with the results of a filedupetry run. and given lists of PathSetB and PathSetA folders,
	# delete files as directed, acting first on the largest size files, while always pre-checking
	# that we're safe to delete a file because there are other verified copies to remain
	# Parameters:
	# - p_FileFlags
	# - p_lst_paths_PathSetB :: list := 
	# - p_lst_paths_PathSetA :: list := 
	# - p_dct_pfn_filemeta, p_dct_shortmatchkey_lst_pfn, p_dct_fullmatchkey_lst_pfn :: dictionary := 
	# - p_docongruentbeforedelete :: boolean := 
	# - p_congruentstringent :: boolean := 
	# Returns:
	# :: boolean := did we (or did we think we) deleted any files 
	# Notes:
	# for the return value, it's not clear whether to distinguish between actually deleting files and 
	# setting up to delete files as would be the case if the directed action is to build a script 
	# Code:
	if False:
		print( "process_checking_by_strategy_in_order" )
		print( "p_FileFlags" )
		print( p_FileFlags )
		print( "p_DelOrderMode" )
		print( p_DelOrderMode )
		print( "p_docongruentbeforedelete" )
		print( p_docongruentbeforedelete )
		print( "p_congruentstringent" )
		print( p_congruentstringent )
		print( "p_EnumDeleteMode" )
		print( p_EnumDeleteMode )
		input( "Press ENTER" )
	i_dct_matchkeytuple_lst_pfn = {}
	def DelOrderMode_SortValueForKeyTuple( p_keytuple ):
		nonlocal p_DelOrderMode
		nonlocal i_dct_matchkeytuple_lst_pfn
		return DelOrderMode_DeriveSortValueForKeyTuple( p_keytuple, p_DelOrderMode, i_dct_matchkeytuple_lst_pfn, p_dct_pfn_filemeta )
	def DelOrderMode_SortReverse():
		nonlocal p_DelOrderMode
		return DelOrderMode_DeriveSortReverse( p_DelOrderMode )
	if __debug__:
		p_multi_log.do_logs( "dbs", "process_checking_by_strategy_in_order" )
	r_Lst_StringentSuccess = []
	r_Lst_StringentFailures = []
	did_files_by_hash = False
	# which tuple index type are we using? Current code can use the same sort-item-value function by relying on sub-element coincidence but leave as two calls in case that changes
	if p_FileFlags.ff_Hash :
		i_dct_matchkeytuple_lst_pfn = p_dct_fullmatchkey_lst_pfn
		# get list of keys - later make this a smart call to get them sorted as per the Fileflags 
		i_lst_unsorted_keys = p_dct_fullmatchkey_lst_pfn.keys()
		i_lst_keys_grps = sorted( i_lst_unsorted_keys, key= lambda x: DelOrderMode_SortValueForKeyTuple( x ), reverse=DelOrderMode_SortReverse() )
	else :
		i_dct_matchkeytuple_lst_pfn = p_dct_shortmatchkey_lst_pfn
		# get list of keys - later make this a smart call to get them sorted as per the Fileflags 
		i_lst_unsorted_keys = p_dct_shortmatchkey_lst_pfn.keys()
		i_lst_keys_grps = sorted( i_lst_unsorted_keys, key= lambda x: DelOrderMode_SortValueForKeyTuple( x ), reverse=DelOrderMode_SortReverse() )
		pass
	i_grps_count = len( i_dct_matchkeytuple_lst_pfn )
	if i_grps_count > 0 :
		i_SizeTotal = GetSizeTotalForListOfKeys( i_dct_matchkeytuple_lst_pfn, p_dct_pfn_filemeta)
		i_SizeSoFar = 0
		i_time_mark_0 = cmntry.is_n_to_show_progess_timed_get_mark()
		i_tuplkey_at = 0
		# set up tracking object to make completion predictions - to start with we'll do the very dumb thing of counting group keys that we loop through
		i_TrackObject = cmntry.ProgressTrackObject( i_SizeTotal, i_time_mark_0 )
		i_time_mark_n = i_time_mark_0
		# for the top n sizes
		for i_tuplkey in i_lst_keys_grps:
			# for each i_tuplkeye
			if False:
				print( "i_tuplkey" )
				print( i_tuplkey )
			i_tuplkey_at = i_tuplkey_at + 1
			i_show_progress, i_time_mark_n = cmntry.is_n_to_show_progess_timed_stock( i_tuplkey_at, i_time_mark_n)
			if i_show_progress or ( i_tuplkey_at < 11 ):
				p_multi_log.do_logs( "s", "Diff=  " + str( len( r_Lst_StringentFailures ) ) + " and Same= " + str( len( r_Lst_StringentSuccess ) ) )
				p_multi_log.do_logs( "s", "Check " + str( i_tuplkey_at ) + " of " + str( len( i_lst_keys_grps ) ) )
				# p_multi_log.do_logs( "s", "Files: " + cmntry.pfn4print( i_pfn_PathSetA ) + " {vs} " + cmntry.pfn4print( i_pfn_PathSetB ) )
			p_multi_log.do_logs( "db", "i_tuplkeye #" + str( i_tuplkey_at) + " with " + str( i_grps_count) + " of i_tuplkeye = " + cmntry.hndy_GetHumanReadable(i_tuplkey_at) + " " + str( i_tuplkey_at) )
			# get the hashes
			i_lst_pfn = i_dct_matchkeytuple_lst_pfn[ i_tuplkey ]
			if __debug__:
				p_multi_log.do_logs( "b", str( len( i_lst_pfn) ) + " files: " )
			i_has_both, i_lst_PathSetB, i_lst_PathSetA = does_lst_pfn_have_both_PathSetA_and_PathSetB( \
				i_lst_pfn, p_lst_paths_PathSetB, p_lst_paths_PathSetA )
			if i_has_both :
				p_multi_log.do_logs( "db", "We can do some checking." )
				p_multi_log.do_logs( "db", "In Set A " + str( len( i_lst_PathSetB) ) + " files: " )
				p_multi_log.do_logs( "db", "In Set B " + str( len( i_lst_PathSetA) ) + " files: " )
				i_Lst_StringentSuccess, i_Lst_StringentFailures = check_if_stringent( i_lst_PathSetB, i_lst_PathSetA, \
					p_congruentstringent, p_ReportSuccessStringent, p_ReportFailuresStringent, p_datafilename_same_str, p_datafilename_diff_str, p_multi_log )
				r_Lst_StringentSuccess.extend( i_Lst_StringentSuccess )
				r_Lst_StringentFailures.extend( i_Lst_StringentFailures )
				# update tracking object
				i_SizeJustDone = GetSizeOfExampleInList( p_dct_pfn_filemeta, i_lst_PathSetB )
				i_SizeSoFar += i_SizeJustDone
				i_show_progress, i_time_mark_n = cmntry.is_n_to_show_progess_timed_stock( i_tuplkey_at, i_time_mark_n)
				i_TrackObject.AddInfoPoint( i_SizeSoFar, i_time_mark_n )
				i_time_sofar = i_time_mark_n - i_time_mark_0
				if i_show_progress or ( i_tuplkey_at < 51 ):
					i_Predict_A = i_TrackObject.PredictComplete_ViaWhole()
					if not i_Predict_A is None:
						p_multi_log.do_logs( "s", "Predicting completion in " + cmntry.hndy_GetHumanReadable_Seconds( i_Predict_A) + \
							" of total " + cmntry.hndy_GetHumanReadable_Seconds( i_Predict_A + i_time_sofar ) )
					i_Predict_50 = i_TrackObject.PredictComplete_ViaRecent( 50)
					if not i_Predict_50 is None:
						p_multi_log.do_logs( "s", "Prediction using 50 latest: " + cmntry.hndy_GetHumanReadable_Seconds( i_Predict_50) + \
							" of total " + cmntry.hndy_GetHumanReadable_Seconds( i_Predict_50 + i_time_sofar ) )
					i_Predict_10 = i_TrackObject.PredictComplete_ViaRecent( 10)
					if not i_Predict_10 is None:
						p_multi_log.do_logs( "s", "Prediction using 10 latest: " + cmntry.hndy_GetHumanReadable_Seconds( i_Predict_10) + \
							" of total " + cmntry.hndy_GetHumanReadable_Seconds( i_Predict_10 + i_time_sofar ) )
			else :
				p_multi_log.do_logs( "db", "No PathSetA+PathSetB combination for this matchkey so cannot automate purging." )
				if len( i_lst_PathSetA ) > 0 :
					p_multi_log.do_logs( "db", "Example PathSetA item is:" + cmntry.pfn4print( i_lst_PathSetA[0] ) )
				if len( i_lst_PathSetB ) > 0 :
					p_multi_log.do_logs( "db", "Example PathSetB item is:" + cmntry.pfn4print( i_lst_PathSetB[0] ) )
	return r_Lst_StringentSuccess, r_Lst_StringentFailures




def make_dct_matchkey_lst_pfn_in_both_sets( p_dct_shortmatchkey_lst_pfn, p_lst_paths_PathSetB, p_lst_paths_PathSetA, p_multi_log ):
	def def_name():
		return "make_dct_matchkey_lst_pfn_in_both_sets"
	p_multi_log.do_logs( "spdb", def_name() + ": Passed p_dct_shortmatchkey_lst_pfn of length " + str( len( p_dct_shortmatchkey_lst_pfn) ) )
	r_dct_matchkey_lst_pfn_in_both_sets = {}
	i_key_checked_count = 0
	i_key_added_count = 0
	i_time_mark = cmntry.is_n_to_show_progess_timed_get_mark()
	for i_matchkey in p_dct_shortmatchkey_lst_pfn.keys() :
		i_key_checked_count += 1
		i_lst_pfn = p_dct_shortmatchkey_lst_pfn[ i_matchkey ]
		i_has_both, i_lst_PathSetB, i_lst_PathSetA = does_lst_pfn_have_both_PathSetA_and_PathSetB( \
			i_lst_pfn, p_lst_paths_PathSetB, p_lst_paths_PathSetA )
		if i_has_both :
			r_dct_matchkey_lst_pfn_in_both_sets[ i_matchkey ] = i_lst_pfn
			i_key_added_count += 1
		b, i_time_mark = cmntry.is_n_to_show_progess_timed_stock( i_key_checked_count, i_time_mark )
		if b :
			p_multi_log.do_logs( "spdb", def_name() + ": Keys checked = " + str( i_key_checked_count) + " Keys added = " + str( i_key_added_count) )
	p_multi_log.do_logs( "spdb", def_name() + ": Returning r_dct_matchkey_lst_pfn_in_both_sets of length " + str( len( r_dct_matchkey_lst_pfn_in_both_sets) ) )
	return r_dct_matchkey_lst_pfn_in_both_sets

# --------------------------------------------------
# Command - as would be called by other modules
# --------------------------------------------------

# general idea is:
# - get passed two sets of folders (PathSetB, PathSetA), a flag and a logging control setup
# - combine the two folder sets
# - find all the replicated places in all the folders
# - analyse the results to find all the replicants that can be safely removed (i.e. delete in PathSetA, keep in PathSetB)
# - - currently part-implemented in "find_common_roots"
# - use congruenty to double-check eash deletion candidate
# - use purgetry to perform the verified deletions

# to be considered:
# - how it might be possible to offer a user choosing among replicants that are only in the PathSetA set
# - whether that step should be before processing the suvivor-v-PathSetA deletions
# - - maybe offer to support both

# TO DO: add FnameMatchSetting to the parameters - 
# - need to check out all the places that call this

def veritry_command_recce( p_lst_paths_PathSetB, p_lst_paths_PathSetA, \
		p_FileFlags, p_ReportSuccessStringent, p_congruentstringent ):
	# as an exercise, let's compile ALL the reasons why
	r_is_ok = True
	r_lst_why = [] # this will be a list of strings
	# A
	if isinstance( p_lst_paths_PathSetB, list ) :
		for i_path_PathSetB in p_lst_paths_PathSetB :
			a_is_ok, a_lst_why = cmntry.consider_path_str( i_path_PathSetB, "PathSetB" )
			r_is_ok = r_is_ok and a_is_ok
			r_lst_why.extend( a_lst_why )
	else:
		r_is_ok = False
		r_lst_why.append( "PathSetB is not a list" )
	# B
	if isinstance( p_lst_paths_PathSetA, list ) :
		for i_path_PathSetA in p_lst_paths_PathSetA :
			b_is_ok, b_lst_why = cmntry.consider_path_str( i_path_PathSetA, "PathSetA" )
			r_is_ok = r_is_ok and b_is_ok
			r_lst_why.extend( b_lst_why )
	else:
		r_is_ok = False
		r_lst_why.append( "PathSetA is not a list" )
	return r_is_ok, r_lst_why

def veritry_command( p_lst_paths_PathSetB, p_lst_paths_PathSetA, \
		p_FileFlags, p_congruentstringent, p_ReportSuccessStringent, p_ReportFailuresStringent, p_EnumDelOrder, \
		p_multi_log):
	# Brief:
	# Provides a common base of functionality callable by either the GUI or command line routines
	# The "veritry" approach uses a base concept of supplying two sets of folders:
	# - a PathSetB set, that will not be affected
	# - a PathSetA set, that WILL be affected
	# Parameters:
	# - p_lst_paths_PathSetB :: list of strings := giving the paths to be analysed then kept intact
	# - p_lst_paths_PathSetA :: list of strings := giving the paths to be analysed then kept modified
	# - p_congruentstringent :: boolean := should file contents be done for the fileduptry run 
	# - p_dct_rdlg :: dictionary of log flags and filenames as per Commontry module
	# Returns:
	# - a quite basic Boolean of whether things were done or not
	# Notes:
	# Code:
	zz_procname = "veritry_command"
	# print( zz_procname )
	i_multi_log = im_copy.deepcopy( p_multi_log)
	cmntry.re_filename_multilog_object( i_multi_log, zz_procname )
	def def_name():
		return zz_procname
	def def_name_prfx():
		return def_name() + " "
	# Debugging info
	i_multi_log.do_logs( "b", "Called: " + def_name() )
	i_multi_log.do_logs( "spdb", def_name_prfx() + " " + cmntry.GetStringToShow_tpl_FileFlags( p_FileFlags) )
	#i_multi_log.do_logs( "b", " p_lst_paths_PathSetB: " + p_lst_paths_PathSetB )
	#i_multi_log.do_logs( "b", " p_lst_paths_PathSetA: " + p_lst_paths_PathSetA )
	i_multi_log.do_logs( "b", " p_congruentstringent: " + str( p_congruentstringent ) )
	#
	i_break_after = -1
	#
	i_multi_log.do_logs( "spdb", def_name() + ": Pre-process" )
	i_multi_log.do_logs( "db", "p_lst_paths_PathSetB" + " has " + str( len(p_lst_paths_PathSetB)) )
	i_multi_log.do_logs( "db", "p_lst_paths_PathSetA" + " has " + str( len(p_lst_paths_PathSetA)) )
	# combine the two lists and call matchsubtry
	i_multi_log.do_logs( "dbs", "Making a combination of PathSetB and PathSetA" )
	# - combine the two lists into a fresh copy
	i_lst_all_tree_paths = p_lst_paths_PathSetA.copy()
	i_lst_all_tree_paths.extend(p_lst_paths_PathSetB)
	i_multi_log.do_logs( "db", "i_lst_all_tree_paths" + " has " + str( len(i_lst_all_tree_paths)) )
	i_use_datetime_str = cmntry.bfff_datetime_ident_str() 
	i_datafilename_same_str = cmntry.dtff_make_data_filename( i_use_datetime_str, zz_procname + "_same" )
	i_datafilename_diff_str = cmntry.dtff_make_data_filename( i_use_datetime_str, zz_procname + "_diff" )
	# preparation for bringing in use of filedupetry for pure file hash matching
	i_multi_log.do_logs( "psdb", def_name() + ": Step 3 File Process" )
	i_multi_log.do_logs( "spdb", def_name() + ": Step 3 A calling: filedupetry_collect" )
	# Collect file information by traversing all the paths
	i_dct_pfn_filemeta, i_dct_shortmatchkey_lst_pfn = \
		filedupetry_collect( i_lst_all_tree_paths, p_FileFlags, i_multi_log)
	i_dct_shortmatchkey_lst_pfn_in_both_sets = make_dct_matchkey_lst_pfn_in_both_sets( i_dct_shortmatchkey_lst_pfn, \
		p_lst_paths_PathSetB, p_lst_paths_PathSetA, i_multi_log )
	if False:
		print( "i_dct_shortmatchkey_lst_pfn" )
		print( i_dct_shortmatchkey_lst_pfn )
		print( "i_dct_shortmatchkey_lst_pfn_in_both_sets" )
		print( i_dct_shortmatchkey_lst_pfn_in_both_sets )
		input( "It seems we have a problem!")
	if p_FileFlags.ff_Hash :
		i_multi_log.do_logs( "spdb", mod_code_name() + ": Step 3 B calling: hasherdabbery" )
		# first, run through all files deriving hashes and storing them
		i_include_lists_of_one = False # just making this clear
		i_dct_fullmatchkey_lst_pfn = hasherdabbery( i_dct_shortmatchkey_lst_pfn_in_both_sets, i_dct_pfn_filemeta, i_include_lists_of_one, p_FileFlags, i_multi_log)
	else :
		i_multi_log.do_logs( "dbsp", def_name_prfx() + "No Step 3 B (Hashing) Required" )
		# set empty dictionary for the return
		i_dct_fullmatchkey_lst_pfn = {}
	# write the ouput file header rows
	cmntry.file_in_home_text_new_list_as_data_head( i_datafilename_same_str, [ "filename_a", "filename_b" ] )
	cmntry.file_in_home_text_new_list_as_data_head( i_datafilename_diff_str, [ "filename_a", "filename_b" ] )
	#
	i_multi_log.do_logs( "spdb", mod_code_name() + ": Step 3 C calling: process_checking_by_strategy_in_order" )
	i_Lst_StringentSuccess, i_Lst_StringentFailures = process_checking_by_strategy_in_order( \
		p_lst_paths_PathSetB, p_lst_paths_PathSetA, 
		i_dct_pfn_filemeta, i_dct_shortmatchkey_lst_pfn_in_both_sets, i_dct_fullmatchkey_lst_pfn, \
		p_FileFlags, p_congruentstringent, p_ReportSuccessStringent, p_ReportFailuresStringent, p_EnumDelOrder, \
		i_datafilename_same_str, i_datafilename_diff_str, i_multi_log )
	i_count_stringent_success = len( i_Lst_StringentSuccess )
	i_count_stringent_failure = len( i_Lst_StringentFailures )
	# ---- having moved the output file writing to a deeper call, the following code is parked for now
	if False and p_ReportSuccessStringent :
		# write to a data log file i_datafilename_same_str
		cmntry.file_in_home_text_new_list_as_data_head( i_datafilename_same_str, [ "filename_a", "filename_b" ] )
		for goodname in i_Lst_StringentSuccess:
			cmntry.file_in_home_text_add_list_as_data_line( i_datafilename_same_str, cmntry.lst_pfn4print_of_lst_pfn( list( goodname ) ) )
	if False and p_ReportFailuresStringent :
		# write to a data log file i_datafilename_diff_str
		cmntry.file_in_home_text_new_list_as_data_head( i_datafilename_diff_str, [ "filename_a", "filename_b" ] )
		for failname in i_Lst_StringentFailures:
			cmntry.file_in_home_text_add_list_as_data_line( i_datafilename_diff_str, cmntry.lst_pfn4print_of_lst_pfn( list( failname ) ) )
	# --------------------
	r_problem = ( i_count_stringent_failure > 0 )
	i_multi_log.do_logs( "dbr", def_name_prfx() + "Stringent comparison successes= " + str( i_count_stringent_success ) )
	i_multi_log.do_logs( "br", def_name_prfx() + "Written to file " + cmntry.pfn4print( i_datafilename_same_str ) )
	i_multi_log.do_logs( "dbr", def_name_prfx() + "Stringent comparison failures= " + str( i_count_stringent_failure ) )
	i_multi_log.do_logs( "br", def_name_prfx() + "Written to file " + cmntry.pfn4print( i_datafilename_diff_str ) )
	return r_problem

# --------------------------------------------------
# Interactive
# --------------------------------------------------

# Removed - all GUI interaction is now handled by the foldatry module

# --------------------------------------------------
# Command line mode
# --------------------------------------------------

# Removed - all GUI interaction is now handled by the foldatry module


# --------------------------------------------------
# Log controls
# --------------------------------------------------

def setup_logging() :
	# this uses the custom wrapper module "multilog"
	m_log = cmntry.make_multilog_object( mod_code_name() )
	return m_log

# main to execute only if run as a program
# Note: the GUI has been moved to Foldatry
if __name__ == "__main__":
	print( "This is the module: deforestry")
	# later do some smart command line parameter checking
	# e.g. if enough info is passed and/or indication to avoid the GUI
	# setup logging
	multi_log = setup_logging()
	if __debug__:
		multi_log.do_logs( "b", "Not currently running any tests for this module.")
	print( "Not currently running any tests for this module.")
