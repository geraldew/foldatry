#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# trimatry = trim a tree ?
#
# This forms part of the foldatry application but can be used as an
# independent command line tool.
#
# --------------------------------------------------
# Copyright (C) 2023-2024 Gerald Waters - Melbourne, Australia
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# --------------------------------------------------
# Imports - Standard
# --------------------------------------------------

import os as im_os
import os.path as im_osp

from enum import Enum, unique, auto

import copy as im_copy

# --------------------------------------------------
# Imports - Custom
# --------------------------------------------------

import commontry as cmntry
import applitry as apltry

import exploratry as explrtry

from purgetry import purgetry_path_command
from purgetry import purgetry_file_command

# --------------------------------------------------
# Support Custom
# --------------------------------------------------

def mod_code_name():
	return "trimatry"

def mod_code_abbr():
	return "trmtry"

def mod_show_name():
	return "Trimatry"

# --------------------------------------------------
# Feature Structure Definitions
# --------------------------------------------------

@unique
class ForF( Enum ):
	IsFile = auto() 
	IsFold = auto() 

def ForF_InfixTag( p_ForF ):
	if p_ForF == ForF.IsFile :
		return "files"
	elif p_ForF == ForF.IsFold :
		return "folds"
	else :
		return "error"


# --------------------------------------------------
# Support Stock
# --------------------------------------------------

# --------------------------------------------------
# Support Custom - i.e. mostly generic functions on simple types
# --------------------------------------------------

# --------------------------------------------------
# Marker names

def Marker_Name_NoMedia():
	return ".nomedia"

def Marker_Name_KeepIfEmpty():
	return ".keepifempty"

def IsName_NoMedia( p_name ):
	return p_name.lower() == Marker_Name_NoMedia().lower()

def IsName_KeepIfEmpty( p_name ):
	return p_name.lower() == Marker_Name_KeepIfEmpty().lower()

# --------------------------------------------------
# Item Objects
# where Mpty is shorthand for empty

# Files

def MptyEff_Obj_Create():
	zz_procname = "MptyEff_Obj_Create"
	#print( zz_procname)
	r_dct = {}
	return r_dct

def MptyEff_AddItem_DepthPathfile( r_obj, p_Depth, p_Pathfile ):
	zz_procname = "MptyEff_AddItem_DepthPathfile"
	#print( zz_procname)
	if p_Depth in r_obj :
		r_obj[ p_Depth ].append( p_Pathfile )
	else :
		r_obj[ p_Depth ] = [ p_Pathfile ]

def MptyEff_GetListOf_Depth( p_obj ):
	zz_procname = "MptyEff_GetListOf_Depth"
	#print( zz_procname)
	r_lst_Depth = p_obj.keys()
	return r_lst_Depth

def MptyEff_GetListOf_Pathfile_ForDepth( p_obj, p_Depth ):
	zz_procname = "MptyEff_GetListOf_Pathfile_ForDepth"
	#print( zz_procname)
	r_lst_Pathfile = []
	if p_Depth in p_obj :
		for i_path in p_obj[p_Depth] :
			r_lst_Pathfile.append( i_path )
	return r_lst_Pathfile

def MptyEff_IsSomethingThere( p_obj ):
	zz_procname = "MptyEff_IsSomethingThere"
	#print( zz_procname)
	if len( p_obj ) > 0 :
		for d in p_obj :
			if len( p_obj[ d] ) > 0 :
				return True
		return False
	else :
		return False


def MptyEff_Output( p_obj, p_ForF, p_multi_log ):
	zz_procname = "MptyEff_Output"
	#print( zz_procname)
	if MptyEff_IsSomethingThere( p_obj ):
		i_lst_depths = MptyEff_GetListOf_Depth( p_obj )
		i_this_run_ident = cmntry.bfff_datetime_ident_str()
		i_data_filename = cmntry.dtff_make_data_filename( i_this_run_ident, mod_code_name() + "_listofempty_" + ForF_InfixTag( p_ForF ) )
		cmntry.file_in_home_text_new_list_as_data_head( i_data_filename, [ "empty_" + ForF_InfixTag( p_ForF ) ] )
		for d in i_lst_depths :
			i_lst_paths = MptyEff_GetListOf_Pathfile_ForDepth( p_obj, d )
			i_lst_paths.sort( reverse=True )
			if len( i_lst_paths ) > 0 :
				# print( i_lst_paths )
				showing_all_below = 51
				i = 0
				for pathf in i_lst_paths :
					cmntry.file_in_home_text_add_list_as_data_line( i_data_filename, [ pathf ] )
					i += 1
					if cmntry.is_n_to_show_progess_showing_all_below_x( i, showing_all_below ):
						log_ctrl_str = "rdb"
					else :
						log_ctrl_str = "db"
					p_multi_log.do_logs( log_ctrl_str, pathf )
					if len( i_lst_paths ) >= showing_all_below :
						i_see = " More than displayed here."
					else :
						 i_see = ""
				p_multi_log.do_logs( "dbsr", mod_code_name() + i_see + " See full list in data file: " + i_data_filename )

def MptyEff_Erode( p_obj, p_ForF, p_EnumDeleteMode, p_bashfilename_str, p_multi_log ):
	zz_procname = "MptyEff_Erode"
	#print( zz_procname)
	i_lst_depths = MptyEff_GetListOf_Depth( p_obj )
	for d in i_lst_depths :
		i_lst_paths = MptyEff_GetListOf_Pathfile_ForDepth( p_obj, d )
		if len( i_lst_paths ) > 0 :
			# print( i_lst_paths )
			i = 0
			# lst_keys_sorted = sorted( i_dct_counts, key=i_dct_counts.get) 
			for pathf in i_lst_paths :
				i += 1
				if cmntry.is_n_to_show_progess( i ):
					log_ctrl_str = "rdb"
				else :
					log_ctrl_str = "db"
				p_multi_log.do_logs( log_ctrl_str, "Purging: " + pathf )
				if p_ForF == ForF.IsFile :
					did_erode = purgetry_file_command( pathf, p_EnumDeleteMode, p_bashfilename_str, [], [], p_multi_log )
				elif p_ForF == ForF.IsFold :
					did_erode = purgetry_path_command( pathf, p_EnumDeleteMode, p_bashfilename_str, -1, p_multi_log )
				else :
					p_multi_log.do_logs( log_ctrl_str, "Impossible Purging Request for: " + pathf )

# Folders

# --------------------------------------------------
# Feature: Counts by filename extension 
# --------------------------------------------------


def find_empty_folders_and_files( p_lst_folders, p_DoDeleteEmptyFiles, p_DoDeleteEmptyFolds, \
		p_CanDeleteTopFolders, p_TreatNestedEmptyFolders, \
		p_TreatZeroesAsNotThere, p_TreatKeepIfEmptyAsPresent, p_TreatNoMediaAsPresent, \
		p_del_from_deep, p_multi_log ) :
	zz_procname = "find_empty_folders_and_files"
	# p_TreatNestedEmptyFolders = default is ON, in which nested emptiness gets treated so as to delete from the parent that is thus empty 
	# p_TreatZeroesAsNotThere = default is OFF, means to a folder of empty files as being an empty folder
	# p_TreatKeepIfEmptyAsPresent = default is ON, measn that a zero length file of partiucular name prevents a folder from being deleted as empty (even if 
	# p_TreatNoMediaAsPresent = default is OFF, 
	# print( zz_procname)
	# print( "p_lst_folders" )
	# print( p_lst_folders )
	# define progress monitoring counts
	#MethodD
	r_dct_folds = {}
	r_dct_files = {}
	lcl_file_count = 0
	lcl_fold_count = 0
	#MethodO
	r_obj_files = MptyEff_Obj_Create()
	r_obj_folds = MptyEff_Obj_Create()
	# define the recursive internal function 
	def fefaf_traverse_recurse( p_path, p_depth ):
		zz_procname = "fefaf_traverse_recurse" # fefaf == fined empty files and folders
		# print( zz_procname + " path: " + p_path )
		p_multi_log.do_logs( "b", zz_procname + " traversing: " + p_path )
		#MethodD
		nonlocal r_dct_folds
		nonlocal r_dct_files
		nonlocal lcl_file_count
		nonlocal lcl_fold_count
		#MethodO
		nonlocal r_obj_files
		nonlocal r_obj_folds
		lcl_fold_count += 1
		if cmntry.is_n_to_show_progess( lcl_fold_count ):
			p_multi_log.do_logs( "dbs", zz_procname + " Scanning folder #" + str( lcl_fold_count ) + ": " + p_path )
		if im_os.access( p_path, im_os.R_OK):
			# p_multi_log.do_logs( "dbs", zz_procname + " traverse getting contents of: " + p_path )
			i_entries = explrtry.get_lst_sorted_os_scandir( p_path )
		else:
			i_entries = []
		# print( zz_procname + " i_entries: "  )
		# print( i_entries )
		r_have_deletion_blockers = False
		i_lst_deletable_sub_folders = []
		for i_entry in i_entries:
			#p_multi_log.do_logs( "dbs", zz_procname + " Checkfor entry: " + i_entry.name )
			if i_entry.is_file(follow_symlinks=False):
				lcl_file_count += 1
				d_nam, i_ext = im_osp.splitext( i_entry )
				fpath = im_osp.join( p_path, i_entry.name)
				p_Size = im_osp.getsize( fpath )
				i_size_zero = p_Size == 0
				i_IsName_NoMedia = IsName_NoMedia( i_entry.name )
				i_IsName_KeepIfEmpty = IsName_KeepIfEmpty( i_entry.name )
				i_delete_file = i_size_zero and p_TreatZeroesAsNotThere \
					and not ( p_TreatNoMediaAsPresent and i_IsName_NoMedia ) \
					and not ( p_TreatKeepIfEmptyAsPresent and i_IsName_KeepIfEmpty )
				if i_delete_file :
					#MethodD
					# for now we're just storing the zero as a place marker
					r_dct_files[ fpath ] = p_Size
					#MethodO
					MptyEff_AddItem_DepthPathfile( r_obj_files, p_depth, fpath )
				else :
					r_have_deletion_blockers = True
				if cmntry.is_n_to_show_progess( lcl_file_count ):
					p_multi_log.do_logs( "dbs", zz_procname + " Scanning file #" + str( lcl_file_count ) + ": " + i_entry.name )
			elif i_entry.is_dir( follow_symlinks=False ):
				i_new_subpath = im_osp.join( p_path, i_entry.name)
				i_have_deletion_blockers = fefaf_traverse_recurse( i_new_subpath, p_depth + 1 )
				if not i_have_deletion_blockers :
					# add to list of deleteable folders at this level
					i_lst_deletable_sub_folders.append( i_new_subpath )
				else :
					r_have_deletion_blockers = True
		if r_have_deletion_blockers or p_del_from_deep :
			# so this whole folder won't be deletable, therefore list all the empty subfolders for deletion
			for subpath in i_lst_deletable_sub_folders:
				#MethodD
				r_dct_folds[ subpath ] = 0
				#MethodO
				MptyEff_AddItem_DepthPathfile( r_obj_folds, p_depth, subpath )
		return r_have_deletion_blockers
	# main section - at this point we only have folders, meaning file only get handled by the inner routine
	p_multi_log.do_logs( "dbs", zz_procname + " Checking for : " + str( len( p_lst_folders ) ) + " folders." )
	i_path_count = 0
	for i_path in p_lst_folders :
		i_path_count += 1
		# print( "i_path" )
		# print( i_path )
		p_multi_log.do_logs( "dbs", zz_procname + " Traversing path " + i_path )
		if cmntry.is_n_to_show_progess( i_path_count ):
			p_multi_log.do_logs( "dbs", zz_procname + " Scanning listed path #" + str( i_path_count ) + ": " + i_path )
		i_depth = 0
		i_have_deletion_blockers = fefaf_traverse_recurse( i_path, i_depth )
		if p_CanDeleteTopFolders and not i_have_deletion_blockers :
			r_dct_folds[ i_path ] = 0
			MptyEff_AddItem_DepthPathfile( r_obj_folds, i_depth, i_path )
	#MethodD
	p_multi_log.do_logs( "rdbs", zz_procname + " Found: " + str( len( r_dct_folds ) ) + " empty folders." )
	p_multi_log.do_logs( "rdbs", zz_procname + " Found: " + str( len( r_dct_files ) ) + " empty files." )
	# Both #MethodD and #MethodO
	return r_dct_folds, r_dct_files, r_obj_files, r_obj_folds

# --------------------------------------------------
# Feature: 
# --------------------------------------------------

def trimatry_command_recce( p_lst_folders, p_CanDeleteTopFolders, p_TreatNestedEmptyFolders, p_TreatZeroesAsNotThere, p_TreatKeepIfEmptyAsPresent, p_TreatNoMediaAsPresent, \
		p_RenameFoldersRatherThanDelete, p_EnumDeleteMode, p_del_from_deep ):
	return cmntry.consider_list_of_folders( p_lst_folders, True )

def trimatry_command( p_lst_folders, p_DoDeleteEmptyFiles, p_DoDeleteEmptyFolds, p_CanDeleteTopFolders, p_TreatNestedEmptyFolders, p_TreatZeroesAsNotThere, p_TreatKeepIfEmptyAsPresent, p_TreatNoMediaAsPresent, \
		p_RenameFoldersRatherThanDelete, p_EnumDeleteMode, p_del_from_deep, p_multi_log):
	zz_procname = "trimatry_command"
	# print( zz_procname)
	# print( "p_lst_folders" )
	# print( p_lst_folders )
	# ---
	# development control values
	i_ctrl_method_D = False # the original Dictionary method - or what's left of it
	i_ctrl_method_O = True # the newer Object method
	i_multi_log = im_copy.deepcopy( p_multi_log)
	cmntry.re_filename_multilog_object( i_multi_log, zz_procname )
	i_multi_log.do_logs( "dbsp", mod_code_name() + zz_procname + " was called" )
	i_dct_folds, i_dct_files, i_obj_files, i_obj_folds = find_empty_folders_and_files( p_lst_folders, p_DoDeleteEmptyFiles, p_DoDeleteEmptyFolds, \
		p_CanDeleteTopFolders, p_TreatNestedEmptyFolders, p_TreatZeroesAsNotThere, p_TreatKeepIfEmptyAsPresent, p_TreatNoMediaAsPresent, \
		p_del_from_deep, i_multi_log )
	#MethodD
	def output_empties( p_dct, p_subset_name ) :
		if len( p_dct ) > 0 :
			i_this_run_ident = cmntry.bfff_datetime_ident_str()
			i_data_filename = cmntry.dtff_make_data_filename( i_this_run_ident, zz_procname + "_empty_" + p_subset_name + "_finds" )
			cmntry.file_in_home_text_new_list_as_data_head( i_data_filename, [ "empty_" + p_subset_name ] )
			# print( i_dct_counts )
			showing_all_below = 51
			i = 0
			# lst_keys_sorted = sorted( i_dct_counts, key=i_dct_counts.get) 
			lst_keys_sorted = p_dct.keys()
			for k_pathf in lst_keys_sorted :
				cmntry.file_in_home_text_add_list_as_data_line( i_data_filename, [ k_pathf ] )
				i += 1
				if cmntry.is_n_to_show_progess_showing_all_below_x( i, showing_all_below ):
					log_ctrl_str = "rdb"
				else :
					log_ctrl_str = "db"
				i_multi_log.do_logs( log_ctrl_str, k_pathf )
				if len( lst_keys_sorted ) >= showing_all_below :
					i_see = " More than displayed here."
				else :
					 i_see = ""
			i_multi_log.do_logs( "dbsr", zz_procname + i_see + " See full list in data file: " + i_data_filename )
	# ---- Output the findings
	#MethodD
	if i_ctrl_method_D :
		# First method export data files
		output_empties( i_dct_folds, "folder" )
		output_empties( i_dct_files, "file" )
	#MethodO
	if i_ctrl_method_O :
		# Second method export data files
		MptyEff_Output( i_obj_files, ForF.IsFile, i_multi_log )
		# Second method export data folders
		MptyEff_Output( i_obj_folds, ForF.IsFold, i_multi_log )
	# ---- Do the deletions
	i_use_datetime_str = cmntry.bfff_datetime_ident_str()
	i_bashfilename_str = cmntry.scrf_make_bash_filename( i_use_datetime_str, zz_procname )
	# delete empty files
	if p_DoDeleteEmptyFiles :
		#MethodD
		if i_ctrl_method_D :
			for k_pathf in i_dct_files.keys() :
				did_erode = purgetry_file_command( k_pathf, p_EnumDeleteMode, i_bashfilename_str, [], [], i_multi_log )
		#MethodO
		if i_ctrl_method_O :
			MptyEff_Erode( i_obj_files, ForF.IsFile, p_EnumDeleteMode, i_bashfilename_str, i_multi_log )
	# delete empty folders
	if p_DoDeleteEmptyFolds :
		#MethodD
		if i_ctrl_method_D :
			for k_pathf in i_dct_files.keys() :
				did_erode = purgetry_path_command( k_pathf, p_EnumDeleteMode, i_bashfilename_str, -1, i_multi_log )
		#MethodO
		if i_ctrl_method_O :
			MptyEff_Erode( i_obj_folds, ForF.IsFold, p_EnumDeleteMode, i_bashfilename_str, i_multi_log )
	#
	i_multi_log.do_logs( "dbsp", mod_code_name() + zz_procname + " completed" )

# --------------------------------------------------
# Main - only if run as a program
# --------------------------------------------------

def setup_logging() :
	# this uses the custom wrapper module "multilog"
	m_log = cmntry.make_multilog_object( mod_code_abbr() )
	return m_log

# main 
if __name__ == "__main__":
	print( "This is the module: trimatry")
	# setup logging
	i_multi_log = setup_logging()
	if __debug__:
		i_multi_log.do_logs( "b", "Invocation as Main")
	i_multi_log.do_logs( "dbs", mod_code_name() )
	i_lst_folder = [ "/home/ger/Downloads", "/home/ger/dev_tests" ]
	i_DoDeleteEmptyFiles = False
	i_DoDeleteEmptyFolds = False
	i_CanDeleteTopFolders = False
	i_TreatNestedEmptyFolders = False
	i_TreatZeroesAsNotThere = True 
	i_TreatKeepIfEmptyAsPresent = False
	i_TreatNoMediaAsPresent = False
	i_EnumDeleteMode = apltry.EnumDeleteMode_Default()
	trimatry_command( i_lst_folder, i_DoDeleteEmptyFiles, i_DoDeleteEmptyFolds, \
		i_CanDeleteTopFolders, i_TreatNestedEmptyFolders, i_TreatZeroesAsNotThere, i_TreatKeepIfEmptyAsPresent, i_TreatNoMediaAsPresent, i_EnumDeleteMode, i_multi_log)
	i_multi_log.do_logs( "dbs", mod_code_name() + " Completed!" )
