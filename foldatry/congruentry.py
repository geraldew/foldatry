#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# congruentry = congruent tree ?
#
# This forms part of the foldatry application but can be used as an
# independent command line tool.
#
# --------------------------------------------------
# Copyright (C) 2018-2024 Gerald Waters - Melbourne, Australia
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# --------------------------------------------------
# Imports - Standard
# --------------------------------------------------

import sys as im_sys
import os as im_os
import os.path as im_osp
# import time as im_time
import stat as im_stat

from collections import namedtuple

# import filecmp as im_filecmp
#from filecmp import dircmp as fi_dircmp
#from filecmp import cmpfiles as fi_cmpfiles
#from filecmp import cmp as fi_cmp

import hashlib as im_hashlib

import copy as im_copy

# --------------------------------------------------
# Imports - Custom
# --------------------------------------------------

import commontry as cmntry
import applitry as apltry
import multilog

import filecmp_debug as im_filecmp

# --------------------------------------------------
# Support Custom
# --------------------------------------------------

def mod_code_name():
	return "congruentry"

def mod_code_abbr():
	return "cngrntry"

def mod_show_name():
	return "Congruentry"

# --------------------------------------------------
# Feature Structure Definitions
# --------------------------------------------------

# --------------------------------------------------
# set up custom named tuples

# tuple for filename and its re-encoded version
nmdtpl_Name_Recode = namedtuple('nmdtpl_Name_Recode', 'Name Recoded')

# tuple for filename matches found in lfmf_second_go_filename_matching
nmdtpl_NameA_NameB = namedtuple('nmdtpl_NameA_NameB', 'NameA NameB')

# tuple for copy file info
nmdtpl_CopyFileInfo = namedtuple('nmdtpl_CopyFileInfo', 'cfi_from_path cfi_from_name cfi_into_path')

# tuple for pathfilename pairs of Stringent comparison failures
nmdtpl_PathNameA_PathNameB = namedtuple('nmdtpl_PathNameA_PathNameB', 'PathNameA PathNameB')

# --------------------------------------------------
# Support Stock
# --------------------------------------------------

def calc_file_hash( p_path_file ):
    blocksize = 64*1024
    sha = im_hashlib.sha256()
    with open( p_path_file, 'rb') as fp:
        while True:
            data = fp.read(blocksize)
            if not data:
                break
            sha.update( data)
    return sha.hexdigest() 

# --------------------------------------------------
# Support Custom - i.e. mostly generic functions on simple types
# --------------------------------------------------

def layerstr(light_touch, depth_number, sided):
	if light_touch:
		ts = ";"
	else:
		ts = ":"
	return "{D" + ts + str(depth_number) + "-" + sided + "}"

# --------------------------------------------------
# Support Emulation - to provide abstraction for later changes to the library calls being used
# --------------------------------------------------

# support functions

def fcmp_for_itemlist_get_list_onlyfiles( p_path, p_lst_dircmp_items ):
	r_lst_filenames = []
	for fn in namelist:
		test_path_fref = im_osp.join( p_path, fn)
		if im_osp.exists( test_path_fref):
			# test for folder/directory
			i_is_fil = im_stat.S_ISREG(im_os.stat(test_path_fileref).st_mode)
			if i_is_fil != 0 :
				r_lst_filenames.append( fn )
	return r_lst_filenames

def fcmp_for_path_file_get_filesize( p_path, p_file):
	i_pathfile = im_osp.join( p_path, p_file)
	r_size = im_os.path.getsize( i_pathfile)
	return r_size


def fcmp_for_path_files_get_sum_filesize( p_path, p_lst_files):
	klang = False
	r_sum = 0
	for i_file in p_lst_files :
		i_sum = fcmp_for_path_file_get_filesize( p_path, i_file)
		if not i_sum is None :
			r_sum = r_sum + i_sum
		else:
			# a single failed filesize means the sum is invalid
			klang = klang or True
	if klang :
			r_sum = -1
	return r_sum

# the replacement functions

# dcmp references
#
# dcmp.left_only
# dcmp.right_only
# dcmp.same_files
# dcmp.left
# dcmp.right
# dcmp.common_funny
# dcmp.diff_files
# dcmp.same_files
# dcmp.funny_files
# dcmp.common_dirs


def fcmp_for_two_dirs_compare_get_object( p_path_a, p_path_b):
	# we're only using this call for the non-Stingent part of the process, so it can defer to the default of shallow=True
	r_dcmp = im_filecmp.dircmp( p_path_a, p_path_b )
	return r_dcmp

# proposed replacement for the get object call
def fcmp_for_two_dirs_compare_get_lists( p_path_a, p_path_b):
	r_dcmp = im_filecmp.dircmp( p_path_a, p_path_b )
	r_match_size_sum = fcmp_for_path_files_get_sum_filesize( p_path_a, r_dcmp.same_files)
	return r_dcmp, r_match_size_sum

def fcmp_for_dirs_compare_contents_files_get_lists( p_path_a, p_path_b, p_lst_common_files):
	# we're only using this call for Stingent mode, so it can set shallow=False
	r_files_match, r_files_mismatch, r_files_errors = im_filecmp.cmpfiles( p_path_a, p_path_b, p_lst_common_files, shallow=False )
	return r_files_match, r_files_mismatch, r_files_errors

def fcmp_for_two_files_compare_contents_get_bool( p_pathfile_a, p_pathfile_b, p_multi_log ):
	# we're only using this call for Stingent mode
	defname = "fcmp_for_two_files_compare_contents_get_bool"
	p_multi_log.do_logs( "b", defname )
	p_multi_log.do_logs( "b", cmntry.pfn4print( p_pathfile_a ) )
	p_multi_log.do_logs( "b", cmntry.pfn4print( p_pathfile_b ) )
	if True:
		p_multi_log.do_logs( "b", defname + " doing im_filecmp.clear_cache()" )
		im_filecmp.clear_cache()
		p_multi_log.do_logs( "b", defname + " doing Stringent comparison via im_filecmp.cmp" )
		r_same = im_filecmp.cmp( p_pathfile_a, p_pathfile_b, shallow=False)
	else: # original logic, now not considered as wise
		p_multi_log.do_logs( "b", defname + " doing shallow comparison via im_filecmp.cmp" )
		i_same_shallow = im_filecmp.cmp( p_pathfile_a, p_pathfile_b, shallow=True)
		if i_same_shallow :
			p_multi_log.do_logs( "b", defname + " doing Stringent comparison via im_filecmp.cmp" )
			r_same = im_filecmp.cmp( p_pathfile_a, p_pathfile_b, shallow=False)
		else:
			r_same = False
	p_multi_log.do_logs( "b", defname + " r_same = " + str( r_same ) )
	return r_same

# --------------------------------------------------
# Sub-features
# --------------------------------------------------

#? not used
def is_allinlist_ispipe( test_path, namelist):
	tested_list = False
	bool_all_pipes = True
	for fn in namelist:
		tested_list = True
		test_path_fileref = im_osp.join( test_path, fn)
		test_is_pipe = im_stat.S_ISFIFO(im_os.stat(test_path_fileref).st_mode)
		this_one_is_pipe = (test_is_pipe != 0)
		bool_all_pipes = bool_all_pipes and this_one_is_pipe
	return tested_list and bool_all_pipes

#? not used
def is_allinlist_isnotreg( test_path, namelist):
	tested_list = False
	bool_all_notreg = True
	for fn in namelist:
		tested_list = True
		test_path_fileref = im_osp.join( test_path, fn)
		test_is_reg = im_stat.S_ISREG(im_os.stat(test_path_fileref).st_mode)
		this_one_is_notreg = (test_is_reg == 0)
		bool_all_notreg = bool_all_notreg and this_one_is_notreg
	return tested_list and bool_all_notreg

#? not used
def is_allinlist_notimportant( test_path, namelist):
	# work in progress, the question being which types of things shouldn't count
	# if/when they appear to be the reason a directory list is different
	tested_list = False
	bool_all_notimportant = True
	for fn in namelist:
		tested_list = True
		test_path_fileref = im_osp.join( test_path, fn)
		# ignore a symbolic link
		test_is_link = im_stat.S_ISLNK(im_os.stat(test_path_fileref).st_mode)
		this_one_is_link = (test_is_link != 0)
		# ignore a pipe or fifo
		test_is_pipe = im_stat.S_ISFIFO(im_os.stat(test_path_fileref).st_mode)
		this_one_is_pipe = (test_is_pipe != 0)
		this_one_is_notimportant = this_one_is_link or this_one_is_pipe
		bool_all_notimportant = bool_all_notimportant and this_one_is_notimportant
	return tested_list and bool_all_notimportant

def listindir_counts( test_path, namelist):
	count_file = 0
	count_folder = 0
	count_ignore = 0
	count_other = 0
	for fn in namelist:
		test_path_fileref = im_osp.join( test_path, fn)
		if im_osp.exists( test_path_fileref):
			# test for folder/directory
			test_is_dir = im_stat.S_ISDIR(im_os.stat(test_path_fileref).st_mode)
			this_one_is_dir = (test_is_dir != 0)
			if this_one_is_dir:
				count_folder = count_folder + 1
			else:
				# test for symbolic link
				test_is_file = im_stat.S_ISREG(im_os.stat(test_path_fileref).st_mode)
				this_one_is_file = (test_is_file != 0)
				if this_one_is_file:
					count_file = count_file + 1
				else:
					# test for symbolic link
					test_is_link = im_stat.S_ISLNK(im_os.stat(test_path_fileref).st_mode)
					this_one_is_link = (test_is_link != 0)
					# test for a pipe or fifo
					test_is_pipe = im_stat.S_ISFIFO(im_os.stat(test_path_fileref).st_mode)
					this_one_is_pipe = (test_is_pipe != 0)
					if this_one_is_link or this_one_is_pipe:
						count_ignore = count_ignore + 1
					else:
						count_other = count_other + 1
	return count_file, count_folder, count_ignore, count_other

def filename_re_encode( p_PythonStr, p_enum_StrCoding ):
	if p_enum_StrCoding is None :
		return p_PythonStr
	else:
		try:
			# note that encode('utf-8', 'surrogateescape') is here because that is what Python3 is understood to "correctly" undo what Python has done internally 
			return p_PythonStr.encode('utf-8', 'surrogateescape').decode( apltry.EnumCharEnc_PythonRef( p_enum_StrCoding), 'backslashreplace')
		except:
			return p_PythonStr

def filename_make_dos8p3( p_PythonStr ):
	# for now, just do this on a per-file basis, later we might pass in the list so that duplicate outcomes can be handled better
	return cmntry.LongWinFileFoldName_MaybeRenamed( p_PythonStr )

# not used - from an abandoned idea of using a dictionary, seeing as the lists we'll deal with are going unique uniquely keyed
# but as it happens, we'll always want to go through all items, so we gain little from using a dictionary, and as file systems themselves will
# supply unique names, we don't need to do anything to enforce that (the way that dictionary keys mush be unique)
def make_dct_of_lst_filename_re_encode( p_lst_name, p_enum_StrCoding ):
	r_dct = {}
	for i_name in p_lst_name :
		r_dct[ i_name ] = filename_re_encode( i_name, p_enum_StrCoding )
	return r_dct

# --------------------------------------------------
# The Make List of Tuple functions
# These lists of tuples are about each tuple having a pair of: the original name and the alternative match name
# e.g. for the "lowercase" version and incoming string "FileName" becomes the tuple-pair ( "FileName", "filename" )
# There needs to be one of these for each scheme for providing match alternatives:
# - lowercase, re-encoded characters, names not allowed by DOS/Windows etc
# A reason this is always handled as a list, is that some concepts use what-else-is-in-the-list to decied what to do
# - except that at time of writing none of those type have been written
# --------------------------------------------------

def make_lst_tpl_name_with_lowercase( p_lst_name ):
	r_lst = []
	for i_name in p_lst_name :
		tpl = nmdtpl_Name_Recode( i_name, i_name.lower() )
		r_lst.append( tpl)
	return r_lst

def make_lst_tpl_name_with_re_encode( p_lst_name, p_enum_StrCoding ):
	# this receives a list of names and a desired encoding then returns a list of named tuples, 
	# where the first part of the named tuple is the original name and the second is the alternative match candidate 
	r_lst = []
	for i_name in p_lst_name :
		tpl = nmdtpl_Name_Recode( i_name, filename_re_encode( i_name, p_enum_StrCoding ) )
		r_lst.append( tpl)
	return r_lst

def make_lst_tpl_name_with_bad_windows_names( p_lst_name ):
	r_lst = []
	for i_name in p_lst_name :
		alt_name = cmntry.BadWinFileFoldName_MaybeRenamed( i_name )
		if not alt_name is None:
			tpl = nmdtpl_Name_Recode( i_name, alt_name )
			print( tpl)
			r_lst.append( tpl)
	return r_lst

def make_lst_tpl_name_with_bad_windows_chars( p_lst_name ):
	r_lst = []
	for i_name in p_lst_name :
		alt_name = cmntry.Fname_BadWinChars_Removed( i_name )
		if alt_name != i_name :
			tpl = nmdtpl_Name_Recode( i_name, alt_name )
			print( tpl)
			r_lst.append( tpl)
	return r_lst

def make_lst_tpl_name_with_dos8p3( p_lst_name ):
	r_lst = []
	for i_name in p_lst_name :
		alt_name = filename_make_dos8p3( i_name )
		if alt_name != i_name :
			tpl = nmdtpl_Name_Recode( i_name, alt_name )
			r_lst.append( tpl)
	return r_lst

def make_lst_tpl_name_with_no_change( p_lst_name ):
	r_lst = []
	for i_name in p_lst_name :
		tpl = nmdtpl_Name_Recode( i_name, i_name )
		r_lst.append( tpl)
	return r_lst

# pull just the names from the list of tuples
def get_lst_names_from_lst_nmdtpl_Name_Recode( p_lst_tpl ):
	r_lst_names = []
	for tpl in p_lst_tpl:
		r_lst_names.append( tpl.Name )
	return r_lst_names


# v-------------------v
# now for a correlated set of functions to enable switchable/re-usable calling for 2nd round matchings
# lfmf = list of filerefs matching by filenames

# setup a function maker to enable having a controlled type of function
# this gets passed a list of file-folder names and returns a function that will operate on those names
def lfmf_make_function_for_NoChange_lst_tpl( ):
	def passable_function_NoChange( p_lst_names ):
		lst_tpl_todo = make_lst_tpl_name_with_no_change( p_lst_names )
		return lst_tpl_todo
	return passable_function_NoChange

# setup a function maker to enable having a controlled type of function
def lfmf_make_function_for_filename_match_by_lowercase( ):
	def passable_function_lcase( p_lst_names ):
		lst_tpl_todo = make_lst_tpl_name_with_lowercase( p_lst_names )
		return lst_tpl_todo
	return passable_function_lcase

# setup a function maker to enable having a controlled type of function
def lfmf_make_function_for_filename_match_by_encoding( p_enum_StrCoding ):
	def passable_function_encode( p_lst_names ):
		lst_tpl_todo = make_lst_tpl_name_with_re_encode( p_lst_names, p_enum_StrCoding )
		return lst_tpl_todo
	return passable_function_encode

# setup a function maker to enable having a controlled type of function
def lfmf_make_function_for_filename_match_bad_windows_names( p_dummy_method ):
	def passable_function_bad( p_lst_names ):
		lst_tpl_todo = make_lst_tpl_name_with_bad_windows_names( p_lst_names )
		return lst_tpl_todo
	return passable_function_bad

# setup a function maker to enable having a controlled type of function
def lfmf_make_function_for_filename_match_bad_windows_chars( p_dummy_method ):
	def passable_function_bad( p_lst_names ):
		lst_tpl_todo = make_lst_tpl_name_with_bad_windows_chars( p_lst_names )
		return lst_tpl_todo
	return passable_function_bad

# setup a function maker to enable having a controlled type of function
def lfmf_make_function_for_filename_match_dos8p3( p_dummy_method ):
	def passable_function_8p3( p_lst_names ):
		lst_tpl_todo = make_lst_tpl_name_with_dos8p3( p_lst_names )
		return lst_tpl_todo
	return passable_function_8p3

# this is the inner call - it will be told which functions to deploy on the two filename lists 
def lfmf_functional_lst_filref_matching_filenames( \
		p_lst_names_only_in_a, p_fn_a, \
		p_lst_names_only_in_b, p_fn_b, \
		p_multi_log):
	zz_procname = "lfmf_functional_lst_filref_matching_filenames"
	# where each of p_fn_a & p_fn_b must be a function that can taek 
	p_multi_log.do_logs( "db", "Now in def lfmf_second_go_filename_matching" )
	# go through the two lists of names and revise the matches based on the specified re-encodings
	r_lst_tpl_tpl_names_matched =  [] 
	lst_tpl_a_todo = p_fn_a( p_lst_names_only_in_a )
	lst_tpl_b_todo = p_fn_b( p_lst_names_only_in_b )
	lst_tpl_a_done = []
	while len( lst_tpl_a_todo ) > 0 :
		tpl_name_a = lst_tpl_a_todo[ 0] # always using the first in the a list because we'll use pop to remove first one after we've compared it to all the b list
		p_multi_log.do_logs( "db", "tpl_name_a.Name    : " + cmntry.pfn4print( tpl_name_a.Name ) + " ["  + cmntry.pfn4print_hexes( tpl_name_a.Name ) + "]" )
		p_multi_log.do_logs( "db", "tpl_name_a.Recoded : " + cmntry.pfn4print( tpl_name_a.Recoded ) + " ["  + cmntry.pfn4print_hexes( tpl_name_a.Recoded ) + "]" )
		no_b_match = True
		lst_tpl_b_redo = []
		while no_b_match and ( len( lst_tpl_b_todo ) > 0 ) :
			i_tpl_name_b = lst_tpl_b_todo[ 0]
			p_multi_log.do_logs( "db", "tpl_name_b.Name    : " + cmntry.pfn4print( i_tpl_name_b.Name ) + " ["  + cmntry.pfn4print_hexes( i_tpl_name_b.Name ) + "]" )
			p_multi_log.do_logs( "db", "tpl_name_b.Recoded : " + cmntry.pfn4print( i_tpl_name_b.Recoded ) + " ["  + cmntry.pfn4print_hexes( i_tpl_name_b.Recoded ) + "]" )
			if tpl_name_a.Recoded == i_tpl_name_b.Recoded :
				p_multi_log.do_logs( "db", "We have a match!" )
				# list shuffles
				r_lst_tpl_tpl_names_matched.append( nmdtpl_NameA_NameB( tpl_name_a, i_tpl_name_b ) )
				lst_tpl_a_todo.pop( 0 )
				# drop out of the b loop 
				no_b_match = False
			else : 
				lst_tpl_b_redo.append( i_tpl_name_b )
			lst_tpl_b_todo.pop( 0 )
		# for b move the redo items back to the todo list ready for the next a
		lst_tpl_b_todo.extend( lst_tpl_b_redo )
		# progress through a by removing it (if we didn't already as a match )
		if no_b_match :
			lst_tpl_a_done.append( tpl_name_a )
			lst_tpl_a_todo.pop( 0 )
	r_lst_names_only_in_a = get_lst_names_from_lst_nmdtpl_Name_Recode( lst_tpl_a_done )
	r_lst_names_only_in_b = get_lst_names_from_lst_nmdtpl_Name_Recode( lst_tpl_b_todo )
	return r_lst_names_only_in_a, r_lst_names_only_in_b, r_lst_tpl_tpl_names_matched


# this is the meta call - it will determine how often to run over the filename lists and which functions to deploy
def lfmf_second_go_filename_matching( p_lst_names_only_in_a, p_lst_names_only_in_b, p_FnameMatchSetting_A, p_FnameMatchSetting_B, p_multi_log):
	# this True block is only here to satisfy Python's really poor scoping rules, i.e. acting as de facto declaration of variables
	zz_procname = "lfmf_second_go_filename_matching"
	p_multi_log.do_logs( "sdb", zz_procname + " will be trying alternative matches for two lists of unmatched names" )
	if True :
		i_lst_names_only_in_a = []
		i_lst_names_only_in_b = []
		i_lst_tpl_tpl_names_matched = []
		r_lst_names_only_in_a = []
		r_lst_names_only_in_b = []
		r_lst_tpl_tpl_names_matched = []
	# now define the sub-routine and ensure that the scoping is definitely to outside this "inner run"
	def inner_run() :
		nonlocal i_lst_names_only_in_a
		nonlocal i_lst_names_only_in_b
		nonlocal i_lst_tpl_tpl_names_matched
		nonlocal r_lst_names_only_in_a
		nonlocal r_lst_names_only_in_b
		nonlocal r_lst_tpl_tpl_names_matched
		i_lst_names_only_in_a, i_lst_names_only_in_b, i_lst_tpl_tpl_names_matched = \
			lfmf_functional_lst_filref_matching_filenames( \
				r_lst_names_only_in_a, i_fn_a, \
				r_lst_names_only_in_b, i_fn_b, p_multi_log )
		r_lst_names_only_in_a = i_lst_names_only_in_a
		r_lst_names_only_in_b = i_lst_names_only_in_b
		r_lst_tpl_tpl_names_matched.extend( i_lst_tpl_tpl_names_matched )
	# pull all the control values out of the passed in NameMatchSetting
	i_do_CaseInsensitive = not p_FnameMatchSetting_A.get_DoCaseSensitive() or not p_FnameMatchSetting_B.get_DoCaseSensitive()
	# - the A side
	i_A_do_StrCoding = p_FnameMatchSetting_A.get_DoCharEnc()
	i_A_UseCharEncMode = p_FnameMatchSetting_A.get_UseEnumCharEnc()
	i_A_DoMsBadNames = p_FnameMatchSetting_A.get_DoMsBadNames()
	i_A_DoMsBadChars = p_FnameMatchSetting_A.get_DoMsBadChars()
	i_A_DoMs8p3 = p_FnameMatchSetting_A.get_DoMs8p3()
	# - the B side
	i_B_do_StrCoding = p_FnameMatchSetting_B.get_DoCharEnc()
	i_B_UseCharEncMode = p_FnameMatchSetting_B.get_UseEnumCharEnc()
	i_B_DoMsBadNames = p_FnameMatchSetting_B.get_DoMsBadNames()
	i_B_DoMsBadChars = p_FnameMatchSetting_B.get_DoMsBadChars()
	i_B_DoMs8p3 = p_FnameMatchSetting_B.get_DoMs8p3()
	# copy the name lists so we can work on them safely
	r_lst_names_only_in_a = [ nam for nam in p_lst_names_only_in_a ]
	r_lst_names_only_in_b = [ nam for nam in p_lst_names_only_in_b ]
	# prep the tuples list
	r_lst_tpl_tpl_names_matched = []
	# -------------
	# now deal with each of the possible issues available to be set - for now the order of doing these is hard coded
	# ==== deal with case insensitivity
	if i_do_CaseInsensitive :
		i_fn_a = lfmf_make_function_for_filename_match_by_lowercase()
		i_fn_b = lfmf_make_function_for_filename_match_by_lowercase()
		p_multi_log.do_logs( "sdb", zz_procname + " now trying alternative matches re CaseInsensitive" )
		inner_run()
	# ==== deal with character encoding
	if i_A_do_StrCoding or i_B_do_StrCoding :
		i_fn_a = lfmf_make_function_for_NoChange_lst_tpl()
		i_fn_b = lfmf_make_function_for_NoChange_lst_tpl()
		if i_A_do_StrCoding :
			i_fn_a = lfmf_make_function_for_filename_match_by_encoding( i_A_UseCharEncMode )
		if i_B_do_StrCoding :
			i_fn_b = lfmf_make_function_for_filename_match_by_encoding( i_B_UseCharEncMode )
		p_multi_log.do_logs( "sdb", zz_procname + " now trying alternative matches re character encoding" )
		inner_run()
	# ==== deal with prohibited Windows/DOS names
	if i_A_DoMsBadNames or i_B_DoMsBadNames :
		i_fn_a = lfmf_make_function_for_NoChange_lst_tpl()
		i_fn_b = lfmf_make_function_for_NoChange_lst_tpl()
		if i_A_DoMsBadNames :
			i_fn_a = lfmf_make_function_for_filename_match_bad_windows_names( "dummy_method" )
		if i_B_DoMsBadNames :
			i_fn_b = lfmf_make_function_for_filename_match_bad_windows_names( "dummy_method" )
		p_multi_log.do_logs( "sdb", zz_procname + " now trying alternative matches re problem MS-DOS names" )
		inner_run()
	# ==== deal with prohibited Windows/DOS chars in names
	if i_A_DoMsBadChars or i_B_DoMsBadChars :
		i_fn_a = lfmf_make_function_for_NoChange_lst_tpl()
		i_fn_b = lfmf_make_function_for_NoChange_lst_tpl()
		if i_A_DoMsBadChars :
			i_fn_a = lfmf_make_function_for_filename_match_bad_windows_chars( "dummy_method" )
		if i_B_DoMsBadChars :
			i_fn_b = lfmf_make_function_for_filename_match_bad_windows_chars( "dummy_method" )
		p_multi_log.do_logs( "sdb", zz_procname + " now trying alternative matches re problem MS-DOS characters" )
		inner_run()
	# ==== deal with 8.3 DOS/Win names
	if i_A_DoMs8p3 or i_B_DoMs8p3 :
		i_fn_a = lfmf_make_function_for_NoChange_lst_tpl()
		i_fn_b = lfmf_make_function_for_NoChange_lst_tpl()
		if i_A_DoMs8p3 :
			i_fn_a = lfmf_make_function_for_filename_match_dos8p3( "dummy_method" )
		if i_B_DoMs8p3 :
			i_fn_b = lfmf_make_function_for_filename_match_dos8p3( "dummy_method" )
		p_multi_log.do_logs( "sdb", zz_procname + " now trying alternative matches re problem MS-DOS 8.3 translations" )
		inner_run()
	p_multi_log.do_logs( "sdb", zz_procname + " ended" )
	return r_lst_names_only_in_a, r_lst_names_only_in_b, r_lst_tpl_tpl_names_matched

# ==========================================
# Note:
# because this is a three level function, the usual p_ parameter names have addtional prefixes to clarify which layer they are from:
# hence:
#   cct_p_ = congruency_check_trees - the outermmost call
#   tp_p_ = trees_pass - the middle layer call
#   ccs_p_ = congruency_check_subtrees - the innermmost call
#
# to ensure Python handles the nested scopes Pascal-style (instead of the Python a-write-creates-a-new-local style) there is a tedious
# sprinkling of "nonlocal" keywords, and because the Python parser requires those identifiers to be already scanned then there are 
# meaningless assignments to these names even though they actually get written to in the "main" part of the function 
# Oh? Did someone say Python is a well designed language? 

def congruency_check_trees( cct_p_Path_A, cct_p_Path_B, cct_p_FnameMatchSetting_A, cct_p_FnameMatchSetting_B, cct_p_Stringent, \
		cct_p_StopNotCongruent, cct_p_StopNotMoiety, cct_p_ReportDiffs, cct_p_SummaryDiffs, cct_ReportFailuresStringent, cct_p_GenCopyScript, \
		p_multi_log ):
	zz_procname = "congruency_check_trees"
	#print( zz_procname )
	# Parameters:
	# - cct_p_Path_A, cct_p_Path_B : string - two folder paths to compare, order doesn't matter but the output will match the order used
	# - cct_p_Stringent: boolean(False) - should the file contents be byte-for-byte checked?
	# - cct_p_StopNotCongruent : boolean(False) - Stop when not Congruent
	# - cct_p_StopNotMoiety: boolean(True) - Stop when not Moiety i.e. one is subset of the other
	# - cct_p_ReportDiffs: boolean(False) - Report differences
	# - cct_p_SummaryDiffs :boolean(True) - Summarise differences i.e. just the folders where they are whole difference  
	# - p_multi_log - logging object reference
	# Experimental parameters:
	# - cct_p_badwinfname
	# - cct_p_badwinchars
	# - cct_p_enum_charset_a
	# - cct_p_enum_charset_b
	# Returns:
	# - is_congruent = boolean, 
	# - is_subset = boolean, 
	# - extra_side = string
	# - folder_count = integer
	# - file_count = integer
	# - lopside_count = integer
	# - checked_file_bytes = integer
	if False:
		# Here we have a set of variables, that need to be "declared" here in order to be quoted as nonlocal in
		# the inner nested functions - not totally sure about Python in this regard, seems a bit dumb really 
		# these will all be re set in the nested functions anyway
		# lists to be compiled during the runs
		# - missings, do during the shallow runs
		# When items are in A but not in B
		cct_in_A_bni_B_folds = [] # cct_in_A_bni_B_folds # bni = but not in
		cct_in_A_bni_B_files = [] # cct_in_A_bni_B_files
		cct_lst_ntpl_tocopy_A2B = []
		# When items in B are missing
		cct_in_B_bni_A_folds = []
		cct_in_B_bni_A_files = []
		cct_lst_ntpl_tocopy_B2A = []
		# - diffs - only do for Stringent runs, and only report files
		cct_lst_diffs_files = []
		#
		g_folder_count = 0
		g_file_count = 0
		g_byte_sum = 0
		g_lopside_count = 0
		# left
		g_folder_l_count = 0
		g_file_l_count = 0
		g_byte_l_sum = 0
		g_lopside_l_count = 0
		# right
		g_folder_r_count = 0
		g_file_r_count = 0
		g_byte_r_sum = 0
		g_lopside_r_count = 0
	#
	def trees_pass( tp_p_Stringent ):
		# ----------------------
		# Re-scopings
		# - Outer parameters:
		nonlocal cct_p_Path_A
		nonlocal cct_p_Path_B
		#
		nonlocal cct_p_FnameMatchSetting_A
		nonlocal cct_p_FnameMatchSetting_B
		#
		nonlocal cct_p_StopNotCongruent
		nonlocal cct_p_StopNotMoiety
		nonlocal cct_p_ReportDiffs
		nonlocal cct_p_SummaryDiffs
		nonlocal cct_ReportFailuresStringent
		nonlocal cct_p_GenCopyScript
		## nonlocal cct_p_NameMatchSetting
		nonlocal p_multi_log
		# - Outer list variables:
		nonlocal cct_in_A_bni_B_folds
		nonlocal cct_in_A_bni_B_files
		nonlocal cct_lst_ntpl_tocopy_A2B
		nonlocal cct_in_B_bni_A_folds
		nonlocal cct_in_B_bni_A_files
		nonlocal cct_lst_ntpl_tocopy_B2A
		nonlocal cct_lst_diffs_files
		#
		nonlocal g_folder_count
		nonlocal g_file_count
		nonlocal g_byte_sum
		nonlocal g_lopside_count
		# left
		nonlocal g_folder_l_count
		nonlocal g_file_l_count
		nonlocal g_byte_l_sum
		nonlocal g_lopside_l_count
		# left
		nonlocal g_folder_r_count
		nonlocal g_file_r_count
		nonlocal g_byte_r_sum
		nonlocal g_lopside_r_count
		# ----------------------
		# Clearings for this run
		cct_in_A_bni_B_folds = []
		cct_in_A_bni_B_files = []
		cct_lst_ntpl_tocopy_A2B = []
		cct_in_B_bni_A_folds = []
		cct_in_B_bni_A_files = []
		cct_lst_ntpl_tocopy_B2A = []
		cct_lst_diffs_files = []
		#
		# Setup globals outside/across the recursion layer
		# Re python, we need to use lists just to be simple variables just to be suitably "mutable" in scope
		# when we pass them out to other functions
		# joint
		g_folder_count = 0
		g_file_count = 0
		g_byte_sum = 0
		g_lopside_count = 0
		# left
		g_folder_l_count = 0
		g_file_l_count = 0
		g_byte_l_sum = 0
		g_lopside_l_count = 0
		# right
		g_folder_r_count = 0
		g_file_r_count = 0
		g_byte_r_sum = 0
		g_lopside_r_count = 0
		# ----------------------
		#
		def congruency_check_subtrees( ccs_p_Path_A, ccs_p_Path_B, ccs_p_depth, ccs_p_still_congruent, ccs_p_extra_side ):
			# This is the recursion point that will call itself to traverse the paths
			# Parameters:
			# - ccs_p_Path_A, ccs_p_Path_B = string, two folder paths to compare, order doesn't matter but the output will match the order used
			# - ccs_p_depth = integer, 
			# - skimming = boolean, should the file contents be byte-for-byte checked?
			# - ccs_p_still_congruent = boolean, 
			# - ccs_p_extra_side = string text marker for whether 
			# Returns:
			# - now_congruent = boolean
			# - still_subset = boolean
			# - now_extra_side = string
			# ----------------------
			# Re-scopings
			nonlocal cct_p_Path_A
			nonlocal cct_p_Path_B
			#
			nonlocal cct_p_FnameMatchSetting_A
			nonlocal cct_p_FnameMatchSetting_B
			#
			nonlocal cct_p_StopNotCongruent
			nonlocal cct_p_StopNotMoiety
			nonlocal cct_p_ReportDiffs
			nonlocal cct_p_SummaryDiffs
			nonlocal cct_ReportFailuresStringent
			nonlocal cct_p_GenCopyScript
			## nonlocal cct_p_NameMatchSetting
			#
			nonlocal cct_in_A_bni_B_folds
			nonlocal cct_in_A_bni_B_files
			nonlocal cct_lst_ntpl_tocopy_A2B
			nonlocal cct_in_B_bni_A_folds
			nonlocal cct_in_B_bni_A_files
			nonlocal cct_lst_ntpl_tocopy_B2A
			nonlocal cct_lst_diffs_files
			#
			nonlocal g_folder_count
			nonlocal g_file_count
			nonlocal g_byte_sum
			nonlocal g_lopside_count
			# left
			nonlocal g_folder_l_count
			nonlocal g_file_l_count
			nonlocal g_byte_l_sum
			nonlocal g_lopside_l_count
			# left
			nonlocal g_folder_r_count
			nonlocal g_file_r_count
			nonlocal g_byte_r_sum
			nonlocal g_lopside_r_count
			#
			nonlocal p_multi_log
			# but note that we bring in the Stringent control boolean from the middle layer not the outer layer
			nonlocal tp_p_Stringent
			# ----------------------
			r_Lst_NtpPathNameAPathNameB_FailuresStringent = []
			#
			now_extra_side = ccs_p_extra_side
			now_congruent = ccs_p_still_congruent
			#
			# was_extra_side = [now_extra_side]
			# custom status display string
			def get_show_at_str():
				nonlocal now_congruent
				nonlocal tp_p_Stringent
				if now_congruent:
					ss = "="
				else:
					ss = "!"
				if tp_p_Stringent:
					ts = ":"
					strngntstr = "Yes"
				else:
					ts = ";"
					strngntstr = "No"
				if False and was_extra_side[0] != now_extra_side:
					input( "Change of side! " + was_extra_side[0] + " -> " + now_extra_side + " Press <Enter>")
				the_show_at_str = "{Stringent=" + strngntstr + ",Congruency=" + ss + ",Sidedness=" + now_extra_side + ",Depth=" + str(ccs_p_depth) + ",FolderCount=" + str(g_folder_count) + ",LopsideCount=" + str(g_lopside_count) 
				if tp_p_Stringent:
					the_show_at_str += ",FileCount=" + str(g_file_count)
				the_show_at_str += "}"
				return the_show_at_str
			# was_extra_side[0] = now_extra_side
			g_folder_count = g_folder_count + 1
			show_at_str = get_show_at_str()
			if __debug__:
				p_multi_log.do_logs( "b", show_at_str )
			if cmntry.is_n_to_show_progess( g_folder_count ) or cmntry.is_n_to_show_progess( g_file_count ) :
				# note that during light touch pass the g_file_count does not increment
				p_multi_log.do_logs( "dbs", show_at_str )
			if __debug__:
				p_multi_log.do_logs( "b", "path_a:" + ccs_p_Path_A )
				p_multi_log.do_logs( "b", "path_b:" + ccs_p_Path_B )
			new_lopside_count = 0
			if __debug__:
				p_multi_log.do_logs( "b", "congruency_check_subtrees" )
			# compare directories - but note ths only does shallow file checks
			if __debug__:
				p_multi_log.do_logs( "b", "calling: filecmp.dircmp( path_a, path_b )" )
			# ----------------------
			# Call the directory comparison module - Yes this bit does all the hard work, the rest is organising the results
			# note that the returned dcmp is a complex object that then needs exploring
			dcmp = fcmp_for_two_dirs_compare_get_object( ccs_p_Path_A, ccs_p_Path_B ) # ? shallow=True
			i_match_size_sum = fcmp_for_path_files_get_sum_filesize( ccs_p_Path_A, dcmp.same_files )
			# ----------------------
			# make local lists that we can safely manipulate
			# ----------------------
			# here is where we insert the use of lfmf_second_go_filename_matching
			if ( len(dcmp.left_only) > 0 ) and ( len(dcmp.right_only) > 0 ) and \
					apltry.For_FnameMatchSettings_Get_DoWeDo2ndRound( cct_p_FnameMatchSetting_A, cct_p_FnameMatchSetting_B ) :
				p_multi_log.do_logs( "spdb", "lfmf_second_go_filename_matching " + ccs_p_Path_A + " & " + ccs_p_Path_B )
				i_lst_names_only_in_a, i_lst_names_only_in_b, r_lst_tpl_tpl_names_x_matched = \
					lfmf_second_go_filename_matching( dcmp.left_only, dcmp.right_only, cct_p_FnameMatchSetting_A, cct_p_FnameMatchSetting_B, p_multi_log)
				if len( r_lst_tpl_tpl_names_x_matched ) > 0 :
					p_multi_log.do_logs( "sdb", "Well, we seem to have found " + str(len( r_lst_tpl_tpl_names_x_matched )) + " additional matches!" )
					for tpl in r_lst_tpl_tpl_names_x_matched :
						p_multi_log.do_logs( "sdb", "Tuple of A and B respectively:" )
						p_multi_log.do_logs( "sdb", cmntry.pfn4print( tpl.NameA.Name ) + " [Hex: " + cmntry.pfn4print_hexes( tpl.NameA.Name ) + "]" )
						p_multi_log.do_logs( "sdb", cmntry.pfn4print( tpl.NameB.Name ) + " [Hex: " + cmntry.pfn4print_hexes( tpl.NameB.Name ) + "]" )
			else :
				i_lst_names_only_in_a = dcmp.left_only[:]
				i_lst_names_only_in_b = dcmp.right_only[:]
				r_lst_tpl_tpl_names_x_matched = []
			# ----------------------
			if __debug__:
				p_multi_log.do_logs( "b", "called: filecmp.dircmp( path_a, path_b )" )
				p_multi_log.do_logs( "b", "left path:" + str(dcmp.left) )
				p_multi_log.do_logs( "b", "right path:" + str(dcmp.right) )
			if __debug__:
				p_multi_log.do_logs( "b", "doing: = len(i_lst_names_only_in_a)" )
			found_n_things_leftonly = len(i_lst_names_only_in_a)
			if __debug__:
				p_multi_log.do_logs( "b", "found_n_things_leftonly: " + str(found_n_things_leftonly) )
			if __debug__:
				p_multi_log.do_logs( "b", "doing: = len(i_lst_names_only_in_b)" )
			found_n_things_rightonly = len(i_lst_names_only_in_b)
			if __debug__:
				p_multi_log.do_logs( "b", "found_n_things_rightonly: " + str(found_n_things_rightonly) )
			if __debug__:
				p_multi_log.do_logs( "b", "doing: = len(dcmp.common_funny)" )
			found_n_files_funny = len(dcmp.common_funny)
			if __debug__:
				p_multi_log.do_logs( "b", "found_n_files_funny: " + str(found_n_files_funny) )
			if cct_p_ReportDiffs :
				for dif in dcmp.diff_files :
					f_A = im_osp.join(ccs_p_Path_A, dif )
					f_B = im_osp.join(ccs_p_Path_B, dif )
					cct_lst_diffs_files.append( ( f_A, f_B ) )
			# non-functional inspections
			if False :
				# files
				if __debug__:
					p_multi_log.do_logs( "b", "doing: = len(dcmp.same_files)" )
				found_n_files_shallow_same = len(dcmp.same_files)
				if __debug__:
					p_multi_log.do_logs( "b", "found_n_files_shallow_same: " + str(found_n_files_shallow_same) )
				if __debug__:
					p_multi_log.do_logs( "b", "doing: = len(dcmp.diff_files)" )
				found_n_files_diff = len(dcmp.diff_files)
				if __debug__:
					p_multi_log.do_logs( "b", "found_n_files_diff: " + str(found_n_files_diff) )
				if __debug__:
					p_multi_log.do_logs( "b", "doing: = len(dcmp.funny_files)" )
				found_n_funny_files = len(dcmp.funny_files)
				if __debug__:
					p_multi_log.do_logs( "b", "found_n_funny_files: " + str(found_n_funny_files) )
			# folders
			found_n_folds_common = len(dcmp.common_dirs)
			# trace
			if __debug__:
				p_multi_log.do_logs( "b", "found_n_folds_common=" + str(found_n_folds_common) )
				p_multi_log.do_logs( "b", "lopsides: left=" + str(found_n_things_leftonly) + " right=" + str(found_n_things_rightonly) )
			# for now just do these regardless - to check they work ok
			# later, only do these if there's some reason to
			# the idea being that most of the time there will be no difference 
			if found_n_things_leftonly + found_n_things_rightonly == 0:
				lopsided_left = False
				lopsided_right = False
			else:
				# because the dircmp mixes file and folders, we now need to sort out how many of each are there
				# get counts of left-only : files, folders, ignores, others
				lo_c_file, lo_c_folder, lo_c_ignore, lo_c_other = listindir_counts( ccs_p_Path_A, i_lst_names_only_in_a)
				if __debug__:
					p_multi_log.do_logs( "b", show_at_str + "left only>> files:" + str(lo_c_file) + " dirs:" + str(lo_c_folder) + " ignored:" + str(lo_c_ignore) + " other:" + str(lo_c_other) )
				# get counts of right-only : files, folders, ignores, others
				ro_c_file, ro_c_folder, ro_c_ignore, ro_c_other = listindir_counts( ccs_p_Path_B, i_lst_names_only_in_b)
				if __debug__:
					p_multi_log.do_logs( "b", show_at_str + "right only>> files:" + str(ro_c_file) + " dirs:" + str(ro_c_folder) + " ignored:" + str(ro_c_ignore) + " other:" + str(ro_c_other) )
				# lopsided check, 
				lopsided_left = (lo_c_file > 0) or (lo_c_folder > 0)
				lopsided_right = (ro_c_file > 0) or (ro_c_folder > 0)
			if __debug__:
				p_multi_log.do_logs( "b", show_at_str + "Lopsides: Left=" + str(lopsided_left) + " Right=" + str(lopsided_right) )
			if (found_n_files_funny != 0):
				# meaning there are things we've not planned for !!! panic - might yet be accounted by the new list inspection function 
				crashout = True
				still_subset = False
				now_extra_side = "?" 
			elif lopsided_left and lopsided_right:
				crashout = True
				still_subset = False
				now_congruent = False
				now_extra_side = "#" 
			else:
				# not an immediate crash-out but needs more checking
				# first we need to work out what's the state of this new folder
				if (not lopsided_left) and (not lopsided_right):
					new_extra_side = "="
				elif lopsided_left and (not lopsided_right):
					new_extra_side = "L"
					new_lopside_count = len(i_lst_names_only_in_a)
					now_congruent = False
					# input('new_extra_side ' + new_extra_side + " Press <Enter>")
					if __debug__:
						p_multi_log.do_logs( "b", show_at_str + "path_a:" + cmntry.pfn4print( ccs_p_Path_A ) )
						p_multi_log.do_logs( "b", "Lopsided left: (par:now:new) " + ccs_p_extra_side + ":" + now_extra_side + ":" + new_extra_side )
					# list all the things
					p_multi_log.do_logs( "db", "L path " + ccs_p_Path_A )
					for lo_fn in i_lst_names_only_in_a:
						missing_fileref = im_osp.join(ccs_p_Path_A, lo_fn )
						if cct_p_ReportDiffs:
							cct_in_A_bni_B_files.append( missing_fileref )
						if cct_p_GenCopyScript:
							tpl_copy_info_item = nmdtpl_CopyFileInfo( ccs_p_Path_A, lo_fn, ccs_p_Path_B )
							cct_lst_ntpl_tocopy_A2B.append( tpl_copy_info_item )
						p_multi_log.do_logs( "db", "L+= " + cmntry.pfn4print( lo_fn ) )
				elif (not lopsided_left) and lopsided_right:
					new_extra_side = "R"
					new_lopside_count = len(i_lst_names_only_in_b)
					now_congruent = False
					# input('new_extra_side ' + new_extra_side + " Press <Enter>")
					if __debug__:
						p_multi_log.do_logs( "b", show_at_str + "path_b:" + ccs_p_Path_B )
					p_multi_log.do_logs( "db", "Lopsided right: (par:now:new) " + ccs_p_extra_side + ":" + now_extra_side + ":" + new_extra_side )
					# list all the things
					p_multi_log.do_logs( "db", "R path " + cmntry.pfn4print( ccs_p_Path_B ) )
					for ro_fn in i_lst_names_only_in_b:
						missing_fileref = im_osp.join(ccs_p_Path_B, ro_fn )
						if cct_p_ReportDiffs:
							cct_in_B_bni_A_files.append( missing_fileref )
						if cct_p_GenCopyScript:
							tpl_copy_info_item = nmdtpl_CopyFileInfo( ccs_p_Path_B, ro_fn, ccs_p_Path_A )
							cct_lst_ntpl_tocopy_B2A.append( tpl_copy_info_item )
						p_multi_log.do_logs( "db", "R+= " + cmntry.pfn4print( ro_fn ) )
				else:
					new_extra_side = "!" # shouldn't ever happen
				if (now_extra_side == ""):
					now_extra_side = new_extra_side
				# so, are the new states and old states compatible?
				if cmntry.hndy_off_sided(now_extra_side, new_extra_side):
					crashout = True
					still_subset = False
					now_congruent = False
					p_multi_log.do_logs( "rdb", "DirCmp Offsided: (par:now:new) " + ccs_p_extra_side + ":" + now_extra_side + ":" + new_extra_side )
				else:
					if (now_extra_side == "="):
						now_extra_side = new_extra_side
					crashout = False
					still_subset = True
					g_lopside_count = g_lopside_count + new_lopside_count 
			# folder decision point
			if crashout and cct_p_StopNotMoiety :
				show_at_str = get_show_at_str()
				log_str = "Folder struck out!"
				log_str = log_str + "\n" + show_at_str
				log_str = log_str + "\n" + "Crashed congruency_check_subtrees"
				log_str = log_str + "\n" + "Path A: " + ccs_p_Path_A
				log_str = log_str + "\n" + "Path B: " + ccs_p_Path_B
				# log_str = log_str + "\n" + "found_n_things_leftonly " + str(found_n_things_leftonly)
				# log_str = log_str + "\n" + "found_n_things_rightonly " + str(found_n_things_rightonly)
				# log_str = log_str + "\n" + "found_n_files_diff " + str(found_n_files_diff)
				# log_str = log_str + "\n" + "found_n_files_shallow_same " + str(found_n_files_shallow_same)
				p_multi_log.do_logs( "db", log_str )
				return now_congruent, still_subset, now_extra_side, r_Lst_NtpPathNameAPathNameB_FailuresStringent
			# Now the rationale is to either:
			# - check deeper folders first
			# - check file contents first
			# There are two reasons for going deeper first:
			# - checking file content is CPU and IO expensive
			# - this is what we'd do for a skimming check anyway 
			# So, if there were common subfolders then deeper we go, with recursion
			if found_n_folds_common > 0:
				# recurse
				show_at_str = get_show_at_str()
				if __debug__:
					p_multi_log.do_logs( "b", show_at_str + "found_n_folds_common " + str(found_n_folds_common) )
					#p_multi_log.do_logs( "b", layerstr(skimming, depth,"=")+"recursing" )
				i = 0
				for subdir in dcmp.common_dirs:
					i = i + 1
					show_at_str = get_show_at_str()
					new_subpath_a = im_osp.join(ccs_p_Path_A, subdir)
					new_subpath_b = im_osp.join(ccs_p_Path_B, subdir)
					if __debug__:
						p_multi_log.do_logs( "b", "going into subpath #" + str(i) + " of " + str(found_n_folds_common) + " " + subdir )
					sub_congruent, still_subset, sub_extra_side, i_Lst_NtpPathNameAPathNameB_FailuresStringent = \
						congruency_check_subtrees( new_subpath_a, new_subpath_b, ccs_p_depth + 1, now_congruent, now_extra_side )
						# congruency_check_subtrees(new_subpath_a, new_subpath_b, ccs_p_depth + 1, skimming, now_congruent, now_extra_side, p_do_missings, p_missings)
						# ccs_p_ReportDiffs, ccs_p_SummaryDiffs, ccs_p_depth, ccs_p_extra_side, p_multi_log
					if (now_extra_side == ""):
						now_extra_side = sub_extra_side
					if cmntry.hndy_off_sided(now_extra_side, sub_extra_side):
						still_subset = False
						now_congruent = False
						if __debug__:
							p_multi_log.do_logs( "b", show_at_str + "Sub Offsided: (par:now:sub) " + ccs_p_extra_side + ":" + now_extra_side + ":" + sub_extra_side )
						# input('Press <Enter>')
					else:
						if (now_extra_side == "="):
							now_extra_side = sub_extra_side
						now_congruent = (now_congruent and sub_congruent)
					if len( i_Lst_NtpPathNameAPathNameB_FailuresStringent ) > 0 :
						r_Lst_NtpPathNameAPathNameB_FailuresStringent.extend( i_Lst_NtpPathNameAPathNameB_FailuresStringent )
					if not still_subset and cct_p_StopNotMoiety:
						if __debug__:
							p_multi_log.do_logs( "b", show_at_str + "Crashed congruency_check_subtrees" )
							p_multi_log.do_logs( "b", "Subpath A " + new_subpath_a )
							p_multi_log.do_logs( "b", "Subpath B " + new_subpath_b )
						return now_congruent, still_subset, now_extra_side, r_Lst_NtpPathNameAPathNameB_FailuresStringent
			# recursive call now done
			if not tp_p_Stringent:
				if __debug__:
					p_multi_log.do_logs( "b", "not stringent so no files checks: "  + ccs_p_Path_A + " & " + ccs_p_Path_B )
				return now_congruent, still_subset, now_extra_side, r_Lst_NtpPathNameAPathNameB_FailuresStringent
			else:
				# now do deep file checks
				show_at_str = get_show_at_str()
				if __debug__:
					p_multi_log.do_logs( "b", "starting deep files check at"  + ccs_p_Path_A + " & " + ccs_p_Path_B )
				# first, do the one where the names are identical - as can use a stock call 
				fils_match, fils_mismatch, fils_errors = fcmp_for_dirs_compare_contents_files_get_lists(
					ccs_p_Path_A, ccs_p_Path_B, dcmp.common_files )
				if cct_ReportFailuresStringent:
					# don't quite know what to do here yet, partly because the information is different here to below
					for i_file_mismatch in fils_mismatch:
						t_Str_PathFileName_A = im_osp.join( ccs_p_Path_A, i_file_mismatch)
						t_Str_PathFileName_B = im_osp.join( ccs_p_Path_B, i_file_mismatch)
						t_Ntp_PathNameAPathNameB = nmdtpl_PathNameA_PathNameB( t_Str_PathFileName_A, t_Str_PathFileName_B )
						r_Lst_NtpPathNameAPathNameB_FailuresStringent.append( t_Ntp_PathNameAPathNameB )
				i_match_size_sum = fcmp_for_path_files_get_sum_filesize( ccs_p_Path_A, fils_match )
				# second, do the tuples of additional matches we found
				fils_XAB_match = []
				fils_XAB_mismatch = []
				if len( r_lst_tpl_tpl_names_x_matched ) > 0 :
					p_multi_log.do_logs( "sdb", "About to deep compare the found additional matches!" )
					for tpl in r_lst_tpl_tpl_names_x_matched :
						p_multi_log.do_logs( "sdb", "Tuple of A and B respectively:" )
						p_multi_log.do_logs( "sdb", cmntry.pfn4print( tpl.NameA.Name ) + " [Hex: " + cmntry.pfn4print_hexes( tpl.NameA.Name ) + "]" )
						p_multi_log.do_logs( "sdb", cmntry.pfn4print( tpl.NameB.Name ) + " [Hex: " + cmntry.pfn4print_hexes( tpl.NameB.Name ) + "]" )
						pathfile_a = im_osp.join( ccs_p_Path_A, tpl.NameA.Name )
						pathfile_b = im_osp.join( ccs_p_Path_B, tpl.NameB.Name )
						chckd_XAB_congruent = fcmp_for_two_files_compare_contents_get_bool( pathfile_a, pathfile_b, p_multi_log)
						if chckd_XAB_congruent :
							fils_XAB_match.append( tpl )
						else :
							fils_XAB_mismatch.append( tpl )
							if cct_ReportFailuresStringent :
								# don't quite know what to do here yet, partly because the information is different here to above
								pass
				#
				if __debug__:
					p_multi_log.do_logs( "b", "finished deep files check at"  + ccs_p_Path_A + " & " + ccs_p_Path_B )
				found_n_files_deep_match = len( fils_match) + len( fils_XAB_match)
				found_n_files_deep_mismatch = len( fils_mismatch) + len( fils_XAB_mismatch )
				found_n_files_deep_errors = len( fils_errors)
				was_file_Count = g_file_count
				new_file_Count = g_file_count + found_n_files_deep_match + found_n_files_deep_mismatch + found_n_files_deep_errors
				if cmntry.is_n_range_to_show_progess( was_file_Count, new_file_Count ) :
					p_multi_log.do_logs( "dbs", "Files:" + show_at_str )
				# g_file_count = g_file_count + found_n_files_deep_match + found_n_files_deep_mismatch + found_n_files_deep_errors
				g_file_count = new_file_Count
				# files decision point
				if len(fils_mismatch) > 0 or len(fils_errors) > 0:
					p_multi_log.do_logs( "rdb", "files struck out!" )
					p_multi_log.do_logs( "db", "Path A " + ccs_p_Path_A )
					p_multi_log.do_logs( "db", "Path B " + ccs_p_Path_B )
					p_multi_log.do_logs( "db", "found_n_files_deep_match " + str(found_n_files_deep_match) )
					p_multi_log.do_logs( "db", "found_n_files_deep_mismatch " + str(found_n_files_deep_mismatch) )
					p_multi_log.do_logs( "db", "found_n_files_deep_errors " + str(found_n_files_deep_errors) )					
					now_congruent = False
					still_subset = False
					if found_n_files_deep_mismatch > 0 :
						p_multi_log.do_logs( "db", "Mismatches are:" )
						for nm in fils_mismatch :
							p_multi_log.do_logs( "db", nm )
					if found_n_files_deep_errors > 0 :
						p_multi_log.do_logs( "db", "Errors are:" )
						for nm in fils_errors :
							p_multi_log.do_logs( "db", nm )
				return now_congruent, still_subset, now_extra_side, r_Lst_NtpPathNameAPathNameB_FailuresStringent
		# main of trees_pass
		pass_congruent, pass_subset, pass_extra_side, r_Lst_NtpPathNameAPathNameB_FailuresStringent = \
			congruency_check_subtrees( cct_p_Path_A, cct_p_Path_B, 0, True, "" )
			#congruency_check_subtrees( \
			#tp_p_Path_A, tp_p_Path_B, tp_p_Stringent, tp_p_StopNotCongruent, tp_p_StopNotMoiety, \
			#tp_p_ReportDiffs, tp_p_SummaryDiffs, 0, "", p_multi_log )
			# path_a, path_b, 0, skimming, True, "", p_do_missings, tp_missings)
		return pass_congruent, pass_subset, pass_extra_side, g_folder_count, g_file_count, g_lopside_count, r_Lst_NtpPathNameAPathNameB_FailuresStringent
	# main
	p_multi_log.do_logs( "dbs", zz_procname + " - paths:" )
	p_multi_log.do_logs( "dbs", cct_p_Path_A )
	p_multi_log.do_logs( "dbs", cct_p_Path_B )
	p_multi_log.do_logs( "dbs", zz_procname + " - light touch pass - only folders as lists" )
	is_congruent, is_subset, extra_side, folder_count, file_count, lopside_count, r_Lst_NtpPathNameAPathNameB_FailuresStringent = \
		trees_pass( False )
	if is_subset and cct_p_Stringent:
		p_multi_log.do_logs( "dbs", zz_procname + "Checked " + str(folder_count) + " folders, " + str(file_count) + " files, " + str(lopside_count) + " lopsides." )
		p_multi_log.do_logs( "dbs", zz_procname + " - heavy touch pass - file contents" )
		# input( "Press <Enter>" )
		is_congruent, is_subset, extra_side, folder_count, file_count, lopside_count, r_Lst_NtpPathNameAPathNameB_FailuresStringent = \
			trees_pass( True )
		checked_file_bytes = True
	else:
		checked_file_bytes = False
	p_multi_log.do_logs( "dbs", zz_procname + "Checked " + str(folder_count) + " folders, " + str(file_count) + " files, " + str(lopside_count) + " lopsides." )
	p_multi_log.do_logs( "db", zz_procname + " - done" + ( "=" * 20 ) )
	return is_congruent, is_subset, extra_side, folder_count, file_count, lopside_count, checked_file_bytes, \
		cct_in_A_bni_B_folds, cct_in_A_bni_B_files, cct_lst_ntpl_tocopy_A2B, \
		cct_in_B_bni_A_folds, cct_in_B_bni_A_files, cct_lst_ntpl_tocopy_B2A, cct_lst_diffs_files, r_Lst_NtpPathNameAPathNameB_FailuresStringent

def congruency_paths_process( p_str_path_A, p_str_path_B, p_FnameMatchSetting_A, p_FnameMatchSetting_B, \
				p_Stringent, p_StopNotCongruent, p_StopNotMoiety, p_ReportDiffs, p_SummaryDiffs, p_ReportFailuresStringent, p_GenCopyScript, \
				p_multi_log):
	# this is really just a reporting wrap-around for calling the part that does all the work
	# i.e. congruency_check_trees
	zz_procname = "congruency_paths_process"
	if not im_osp.exists( p_str_path_A) or not im_osp.exists( p_str_path_B) :
		p_multi_log.do_logs( "rdb", zz_procname + " Bad folder reference" )
		if not im_osp.exists( p_str_path_A) :
			p_multi_log.do_logs( "rdb", zz_procname + " Bad A= " + cmntry.pfn4print( p_str_path_A ) )
		if not im_osp.exists( p_str_path_B) :
			p_multi_log.do_logs( "rdb", zz_procname + " Bad B= " + cmntry.pfn4print( p_str_path_B ) )
		return False, False, "?"
	# log the parameters
	p_multi_log.do_logs( "pd", "Congruentry - checking two trees for congruency." )
	p_multi_log.do_logs( "pd", "- (cpc) parameters" )
	p_multi_log.do_logs( "pd", "Path A =" + p_str_path_A )
	p_multi_log.do_logs( "pd", "Path B =" + p_str_path_B )
	# get string summary for A side of current MatchSchemeSetting
	t_str_DoPathA = apltry.Make_StrDescription_For_FnameMatchSetting( p_FnameMatchSetting_A )
	t_str_DoPathB = apltry.Make_StrDescription_For_FnameMatchSetting( p_FnameMatchSetting_B )
	p_multi_log.do_logs( "pd", "A char encoding as " + t_str_DoPathA )
	p_multi_log.do_logs( "pd", "B char encoding as " + t_str_DoPathB )
	# Note: removed the discovery, new appraoch is to let user detect in the GUI
	# traversal
	p_multi_log.do_logs( "pd", "- (cpc) traversal" )
	chckd_congruent, chckd_subset, chckd_extra_side, folder_count, file_count, lopside_count, file_bytes_checked, \
		cpc_in_A_bni_B_folds, cpc_in_A_bni_B_files, cpc_lst_ntpl_tocopy_A2B, \
		cpc_in_B_bni_A_folds, cpc_in_B_bni_A_files, cpc_lst_ntpl_tocopy_B2A, cpc_lst_diffs_files, i_Lst_NtpPathNameAPathNameB_FailuresStringent = \
		congruency_check_trees( p_str_path_A, p_str_path_B, p_FnameMatchSetting_A, p_FnameMatchSetting_B, p_Stringent, \
			p_StopNotCongruent, p_StopNotMoiety, p_ReportDiffs, p_SummaryDiffs, p_ReportFailuresStringent, p_GenCopyScript, \
			p_multi_log )
	# aftermath
	p_multi_log.do_logs( "pd", "- (cpc) aftermath" )
	if chckd_congruent:
		p_multi_log.do_logs( "sdb", "Paths are found TO BE congruent" )
	else:
		p_multi_log.do_logs( "sdb", "Paths are NOT congruent" )
		if chckd_subset:
			log_str = "However, one path IS a subset of the other"
			log_str = log_str + "\n" + "The side with extra content is " + chckd_extra_side
			log_str = log_str + "\n" + "One path is a subset of the other"
			p_multi_log.do_logs( "srdb", log_str )
			if chckd_extra_side == "L":
				x_side_path = p_str_path_A
			elif chckd_extra_side == "R":
				x_side_path = p_str_path_B
			else:
				x_side_path = ""
			log_str = "The path with extra content is:"
			log_str = log_str + "\n" + "> " + x_side_path
			p_multi_log.do_logs( "srdb", log_str )
		else:
			p_multi_log.do_logs( "srdb", "Paths are incompatible" )
		if p_ReportDiffs:
			# now log details
			p_multi_log.do_logs( "srdb", "Reporting differences - see the log (if enabled) for details" )
			p_multi_log.do_logs( "srdb", "Files present but different: " + str( len( cpc_lst_diffs_files ) ) )
			for fref_A, fref_B in cpc_lst_diffs_files:
				p_multi_log.do_logs( "rdb", "Difference between" )
				p_multi_log.do_logs( "rdb", cmntry.pfn4print(fref_A) )
				p_multi_log.do_logs( "rdb", cmntry.pfn4bash(fref_A) )
				p_multi_log.do_logs( "rdb", "- and -" )
				p_multi_log.do_logs( "rdb", cmntry.pfn4print(fref_B) )
				p_multi_log.do_logs( "rdb", cmntry.pfn4bash(fref_B) )
			p_multi_log.do_logs( "rdb", "Missing left folders: " + str( len( cpc_in_A_bni_B_folds ) ) )
			for fref in cpc_in_A_bni_B_folds:
				p_multi_log.do_logs( "rdb", cmntry.pfn4print(fref) )
				p_multi_log.do_logs( "rdb", cmntry.pfn4bash(fref) )
			p_multi_log.do_logs( "rdb", "Missing left files: " + str( len( cpc_in_A_bni_B_files ) ) )
			for fref in cpc_in_A_bni_B_files:
				p_multi_log.do_logs( "rdb", cmntry.pfn4print(fref) )
				p_multi_log.do_logs( "rdb", cmntry.pfn4bash(fref) )
			p_multi_log.do_logs( "rdb", "Missing right folders: " + str( len( cpc_in_B_bni_A_folds ) ) )
			for fref in cpc_in_B_bni_A_folds:
				p_multi_log.do_logs( "rdb", cmntry.pfn4print(fref) )
				p_multi_log.do_logs( "rdb", cmntry.pfn4bash(fref) )
			p_multi_log.do_logs( "rdb", "Missing right files: " + str( len( cpc_in_B_bni_A_files ) ) )
			for fref in cpc_in_B_bni_A_files:
				p_multi_log.do_logs( "rdb", cmntry.pfn4print(fref) )
				p_multi_log.do_logs( "rdb", cmntry.pfn4bash(fref) )
		if p_GenCopyScript:
			p_multi_log.do_logs( "pdb", "Generating copy script lines" )
			i_path_a_use_enum_charenc = apltry.For_FnameMatchSetting_Get_CharEncOrDefault( p_FnameMatchSetting_A )
			i_path_b_use_enum_charenc = apltry.For_FnameMatchSetting_Get_CharEncOrDefault( p_FnameMatchSetting_B )
			# print( "i_path_a_use_enum_charenc")
			# print( EnumCharEnc_Attrbt_Brief( i_path_a_use_enum_charenc ) )
			# print( "i_path_b_use_enum_charenc")
			# print( EnumCharEnc_Attrbt_Brief( i_path_b_use_enum_charenc ) )
			i_a_enc_str = apltry.EnumCharEnc_PythonRef( i_path_a_use_enum_charenc )
			i_b_enc_str = apltry.EnumCharEnc_PythonRef( i_path_b_use_enum_charenc )
			i_use_datetime_str = cmntry.bfff_datetime_ident_str()
			i_bashfilename_str = cmntry.scrf_make_bash_filename( i_use_datetime_str, zz_procname + "_GenCopyScript" )
			cmntry.bash_script_file_add_line( i_bashfilename_str, "# Congruentry generated copy script lines" ) 
			#
			p_multi_log.do_logs( "pdb", "From A to B" )
			cmntry.bash_script_file_add_line( i_bashfilename_str, "# From A to B" ) 
			for ntpl in cpc_lst_ntpl_tocopy_A2B :
				cpstr = cmntry.make_os_script_file_copy_fromcoding_intocoding( ntpl.cfi_from_path, ntpl.cfi_from_name, \
					ntpl.cfi_into_path, i_a_enc_str, i_b_enc_str )
				p_multi_log.do_logs( "db", cpstr )
				cmntry.bash_script_file_add_line( i_bashfilename_str, cpstr ) 
			#
			p_multi_log.do_logs( "pdb", "From B to A" )
			cmntry.bash_script_file_add_line( i_bashfilename_str, "# From B to A" ) 
			for ntpl in cpc_lst_ntpl_tocopy_B2A :
				cpstr = cmntry.make_os_script_file_copy_fromcoding_intocoding( ntpl.cfi_from_path, ntpl.cfi_from_name, \
					ntpl.cfi_into_path, i_b_enc_str, i_a_enc_str )
				p_multi_log.do_logs( "db", cpstr )
				cmntry.bash_script_file_add_line( i_bashfilename_str, cpstr ) 
			#
			p_multi_log.do_logs( "prdb", "Generated copy script lines" )
		if p_ReportFailuresStringent and ( len( i_Lst_NtpPathNameAPathNameB_FailuresStringent ) > 0 ) :
			#print( "ReportFailuresStringent" )
			#print( "PathNameA PathNameB" )
			i_data_filename = cmntry.dtff_make_data_filename( cmntry.bfff_datetime_ident_str(), zz_procname + "_FailuresStringent" )
			cmntry.file_in_home_text_new_list_as_data_head( i_data_filename, [ "PathNameA", "PathNameB" ] )
			for i_NtpPathNameAPathNameB_FailuresStringent in i_Lst_NtpPathNameAPathNameB_FailuresStringent :
				#print( cmntry.pfn4print( "\t".join( [ i_NtpPathNameAPathNameB_FailuresStringent.PathNameA , i_NtpPathNameAPathNameB_FailuresStringent.PathNameB ] ) ) )
				cmntry.file_in_home_text_add_list_as_data_line( i_data_filename, [ i_NtpPathNameAPathNameB_FailuresStringent.PathNameA , i_NtpPathNameAPathNameB_FailuresStringent.PathNameB ] )
			p_multi_log.do_logs( "spdb", "Stringent Failures Reported! See " + cmntry.pfn4print( i_data_filename ) )
	if file_bytes_checked:
		p_multi_log.do_logs( "pdb", "File contents were checked" )
	else:
		p_multi_log.do_logs( "pdb", "Note: File contents were NOT checked" )
	p_multi_log.do_logs( "sdb", "Checked " + str(folder_count) + " folders, " + str(file_count) + " files, " + str(lopside_count) + " lopsides." )
	p_multi_log.do_logs( "pdbs", zz_procname + " - done" + ( "=" * 20 ) )
	return chckd_congruent, chckd_subset, chckd_extra_side

# --------------------------------------------------
# the paths calls for use from outside this module
# --------------------------------------------------

def congruentry_paths_command_recce( p_str_path_A, p_str_path_B, p_FnameMatchSetting_A, p_FnameMatchSetting_B, \
				p_Stringent, p_StopNotCongruent, p_StopNotMoiety, p_ReportDiffs, p_SummaryDiffs, p_GenCopyScript):
	# as an exercise, let's compile ALL the reasons why
	r_is_ok = True
	r_lst_why = [] # this will be a list of strings
	# A
	a_is_ok, a_lst_why = cmntry.consider_path_str( p_str_path_A, "A" )
	r_is_ok = r_is_ok and a_is_ok
	r_lst_why.extend( a_lst_why )
	# B
	b_is_ok, b_lst_why = cmntry.consider_path_str( p_str_path_B, "B" )
	r_is_ok = r_is_ok and b_is_ok
	r_lst_why.extend( b_lst_why )
	return r_is_ok, r_lst_why

def congruentry_paths_command( p_str_path_A, p_str_path_B, p_FnameMatchSetting_A, p_FnameMatchSetting_B, \
				p_Stringent, p_StopNotCongruent, p_StopNotMoiety, p_ReportDiffs, p_SummaryDiffs, p_ReportFailuresStringent, p_GenCopyScript, \
				p_multi_log):
	zz_procname = "congruentry_paths_command"
	# print( zz_procname )
	i_multi_log = im_copy.deepcopy( p_multi_log)
	cmntry.re_filename_multilog_object( i_multi_log, zz_procname )
	# this is really just a reporting wrap-around for calling the part that does all the work
	return congruency_paths_process( p_str_path_A, p_str_path_B, p_FnameMatchSetting_A, p_FnameMatchSetting_B, \
				p_Stringent, p_StopNotCongruent, p_StopNotMoiety, p_ReportDiffs, p_SummaryDiffs, p_ReportFailuresStringent, p_GenCopyScript, \
				i_multi_log)

def congruentry_paths_action( p_str_path_A, p_str_path_B, p_FnameMatchSetting_A, p_FnameMatchSetting_B, \
				p_Stringent, p_StopNotCongruent, p_StopNotMoiety, p_ReportDiffs, p_SummaryDiffs, p_ReportFailuresStringent, p_GenCopyScript, \
				p_multi_log):
	# this is the "internal call" variant of the above
	# intended for use from within commands in other modules
	# hence it does not re-create a new multi_log object
	return congruency_paths_process( p_str_path_A, p_str_path_B, p_FnameMatchSetting_A, p_FnameMatchSetting_B, \
				p_Stringent, p_StopNotCongruent, p_StopNotMoiety, p_ReportDiffs, p_SummaryDiffs, p_ReportFailuresStringent, p_GenCopyScript, \
				p_multi_log)

# --------------------------------------------------
# the files calls for use from outside this module
# --------------------------------------------------

def congruentry_files_command_recce( pathfile_a, pathfile_b ):
	# as an exercise, let's compile ALL the reasons why
	r_is_ok = True
	r_lst_why = [] # this will be a list of strings
	# A
	a_is_ok, a_lst_why = cmntry.consider_path_str( pathfile_a, "A" )
	r_is_ok = r_is_ok and a_is_ok
	r_lst_why.extend( a_lst_why )
	# B
	b_is_ok, b_lst_why = cmntry.consider_path_str( pathfile_b, "B" )
	r_is_ok = r_is_ok and b_is_ok
	r_lst_why.extend( b_lst_why )
	return r_is_ok, r_lst_why

def congruentry_files_command( pathfile_a, pathfile_b, p_multi_log):
	zz_procname = "congruentry_files_command"
	# i.e. congruency check of two files
	# note that by using im_filecmp.cmp this provides caching for repeated checks of the same file
	# which is a likely action when considering removing multiple duplicates 
	i_multi_log = im_copy.deepcopy( p_multi_log)
	cmntry.re_filename_multilog_object( i_multi_log, zz_procname )
	if not im_osp.exists( pathfile_a) or not im_osp.exists( pathfile_b) :
		i_multi_log.do_logs( "rdb", "Bad file reference" )
		return False
	i_multi_log.do_logs( "pdbs", "Congruentry Files - checking two files for congruency." )
	i_multi_log.do_logs( "pdbs", "PathFile A =" + cmntry.pfn4print( pathfile_a ) )
	i_multi_log.do_logs( "pdbs", "PathFile B =" + cmntry.pfn4print( pathfile_b ) )
	r_chckd_congruent = fcmp_for_two_files_compare_contents_get_bool( pathfile_a, pathfile_b, i_multi_log)
	if r_chckd_congruent:
		if __debug__:
			i_multi_log.do_logs( "b", "Files are found to be congruent" )
	else:
		if __debug__:
			i_multi_log.do_logs( "b", "Files are NOT congruent" )
	i_multi_log.do_logs( "pdbs", zz_procname + " - done" + ( "=" * 20 ) )
	return r_chckd_congruent

def congruentry_files_action( pathfile_a, pathfile_b, p_multi_log):
	# this is the "internal call" variant of the above
	# intended for use from within commands in other modules
	# hence it does not re-create a new multi_log object
	if not im_osp.exists( pathfile_a) or not im_osp.exists( pathfile_b) :
		p_multi_log.do_logs( "prdb", "Bad file reference" )
		return False
	r_chckd_congruent = fcmp_for_two_files_compare_contents_get_bool( pathfile_a, pathfile_b, p_multi_log)
	return r_chckd_congruent

# --------------------------------------------------
# Interactive
# --------------------------------------------------

# Removed - all GUI interaction is now handled by the foldatry module
# If used separately this module will need to be passed command line parameters

# --------------------------------------------------
# Log controls
# --------------------------------------------------

# --------------------------------------------------
# Main - only if run as a program
# --------------------------------------------------

# main 
if __name__ == "__main__":
	print( "This is the module: congruentry")
	print( "Currently no testing is setup for this module.")
