#  Note: this file has been copied from
# https://github.com/python/cpython/blob/3.7/Lib/filecmp.py
# and then slightly modfied
# It is therefore under the PYTHON SOFTWARE FOUNDATION LICENSE VERSION 2
# for details see
# https://github.com/python/cpython/blob/3.7/LICENSE

"""Utilities for comparing files and directories.

Classes:
    dircmp

Functions:
    cmp(f1, f2, shallow=True) -> int
    cmpfiles(a, b, common) -> ([], [], [])
    clear_cache()

"""

import os
import stat
from itertools import filterfalse

# temporary for while we log the file comparison time for the two methods
import commontry as cmntry

def app_code_name():
	return "filecmp_debug"

def module_experimenting():
	if __debug__:
		return True
	else :
		return False

def file_compare_method_log_filename():
	return "Foldatry_DevTemp_Log"

__all__ = ['clear_cache', 'cmp', 'dircmp', 'cmpfiles', 'DEFAULT_IGNORES']

_cache = {}
BUFSIZE = 8*1024

DEFAULT_IGNORES = [
    'RCS', 'CVS', 'tags', '.git', '.hg', '.bzr', '_darcs', '__pycache__']

def _sig(st):
	#print( app_code_name() + " _sig" )
	#print( stat.S_IFMT(st.st_mode) )
	#print( st.st_size )
	#print( st.st_mtime )
	# return a tuple of three elements
	return ( stat.S_IFMT(st.st_mode), st.st_size, int( st.st_mtime ) )

def clear_cache():
	#print( app_code_name() + " clear_cache" )
	"""Clear the filecmp cache."""
	_cache.clear()

def _cmp_caching(f1, f2, shallow=True):
    """Compare two files.

    Arguments:

    f1 -- First file name

    f2 -- Second file name

    shallow -- Just check stat signature (do not read the files).
               defaults to True.

    Return value:

    True if the files are the same, False otherwise.

    This function uses a cache for past comparisons and the results,
    with cache entries invalidated if their stat information
    changes.  The cache may be cleared by calling clear_cache().

    """

    s1 = _sig(os.stat(f1))
    s2 = _sig(os.stat(f2))
    #print( s1 )
    #print( s2 )
    if s1[0] != stat.S_IFREG or s2[0] != stat.S_IFREG:
        return False
    if shallow and s1 == s2:
        return True
    if s1[1] != s2[1]:
        return False

    outcome = _cache.get((f1, f2, s1, s2))
    if outcome is None:
        outcome = _do_cmp(f1, f2)
        if len(_cache) > 100:      # limit the maximum size of the cache
            clear_cache()
        _cache[f1, f2, s1, s2] = outcome
    return outcome

def _cmp_no_meta_cache(f1, f2, shallow=True):
	local_debug = False
	if local_debug:
		print( app_code_name() + " _cmp_no_meta_cache" )
		#print( f1 )
		#print( f2 )
		#print( shallow )
	f1_stat = os.stat( f1)
	f1_mode = stat.S_IFMT(f1_stat.st_mode)
	f1_size = f1_stat.st_size
	f1_mtime = int( f1_stat.st_mtime )
	f1_int_mtime = int( f1_mtime )
	#
	f2_stat = os.stat( f2)
	f2_mode = stat.S_IFMT(f2_stat.st_mode)
	f2_size = f2_stat.st_size
	f2_mtime = int( f2_stat.st_mtime )
	f2_int_mtime = int( f2_mtime )
	#
	if f1_mode != stat.S_IFREG or f2_mode != stat.S_IFREG:
		if local_debug:
			print( "Not regular")
		return False
	if f1_size != f2_size :
		if local_debug:
			print( "Different size")
		return False
	if shallow:
		return True # comment this line to do mtime comparison
		return f1_int_mtime == f2_int_mtime
	else:
		# debug, show timestamps
		if False:
			print( "Showing raw Modification Timestamps")
			print( f1_mtime )
			print( f2_mtime )
			print( abs( f2_mtime - f1_mtime ) )
			print( "Showing int Modification Timestamps")
			print( f1_int_mtime )
			print( f2_int_mtime )
			print( abs( f2_int_mtime - f1_int_mtime ) )
			# do full file comparison
			print( "Comparing content using _do_cmp")
		return _do_cmp(f1, f2)

def cmp(f1, f2, shallow=True):
	if False:
		r_outcome = _cmp_caching(f1, f2, shallow)
	else:
		r_outcome = _cmp_no_meta_cache(f1, f2, shallow)
	return r_outcome


def _do_cmp_original(f1, f2):
    # this is the code as was in the original module
    # if has unknowable problems with buffering
    bufsize = BUFSIZE
    with open(f1, 'rb') as fp1, open(f2, 'rb') as fp2:
        while True:
            b1 = fp1.read(bufsize)
            b2 = fp2.read(bufsize)
            if b1 != b2:
                return False
            if not b1:
                return True

def _do_cmp_via_os( f1, f2 ):
	# this is my replacement, which uses the os module's open and fdopen
	# it's coded with a paranoid amount of try except
	import mmap
	DIRECT_BUFSIZE = 2 * 4096 # we must use a multiple of 
	r_match = True
	#
	fd1 = None
	try:
		fd1 = os.open(f1, os.O_RDONLY | os.O_DIRECT)
		fd1_ok = True
	except:
		fd1_ok = False
	if fd1_ok :
		fd2 = None
		try:
			fd2 = os.open( f2, os.O_RDONLY | os.O_DIRECT)
			fd2_ok = True
		except:
			fd2_ok = False
		if fd2_ok :
			fo1 = os.fdopen( fd1, 'rb+', 0)
			fo2 = os.fdopen( fd2, 'rb+', 0)
			m1 = mmap.mmap( -1, DIRECT_BUFSIZE)
			m2 = mmap.mmap( -1, DIRECT_BUFSIZE)
			while True :
				fo1_ok = True
				try :
					n1 = fo1.readinto( m1)
					# print( "n1" )
					# print( n1 )
				except:
					fo1_ok = False
				if fo1_ok :
					fo2_ok = True
					try :
						n2 = fo2.readinto( m2)
						# print( "n2" )
						# print( n2 )
					except:
						fo2_ok = False
					if fo2_ok :
						if n1 != n2 :
							r_match = False
							print( "Read different number of bytes" )
							break
						if n1 == 0 :
							r_match = True # redundant
							break
						if m1[: n1 -1 ] != m2[: n2 -1 ] :
							r_match = False
							print( "Read block mismatch" )
							break
					else :
						r_match = False
						print( "Could not read f2" )
						break
				else :
					r_match = False
					print( "Could not read f1" )
					break
			os.close( fd2)
		else :
			r_match = False
			print( "Could not open f2" )
		os.close( fd1)
	else :
		r_match = False
		print( "Could not open f1" )
	return r_match

def _do_cmp_for_platform( f1, f2 ):
	# this provides OS-platform based decision of which version of the file comparison to run
	i_EnumOS, i_WhichOS = cmntry.GetWhichOS()
	#print( i_EnumOS )
	#print( i_WhichOS )
	if i_EnumOS == cmntry.EnumOS.OsLinux :
		#print( "Will compare files content assuming Linux os module abilities" )
		return _do_cmp_via_os( f1, f2 )
	else :
		return _do_cmp_original( f1, f2)

def _do_cmp_both_methods_assured(f1, f2):
	# this was/is used to run both methods and affirm that they agree
	old_way = _do_cmp_original( f1, f2)
	new_way = _do_cmp_via_os( f1, f2)
	if old_way != new_way :
		input( "_do_cmp Methods do not agree. Press ENTER")
		return False
	else :
		return old_way

def _do_cmp_logger(f1, f2):
	# the does running of both methods and adds some timing and logging thereof across the two methods
	# intended for occasional dev-time use
	import time as im_time
	def do_old():
		i_started_old_when = im_time.perf_counter()
		r_boolean = _do_cmp_original( f1, f2)
		r_duration = im_time.perf_counter() - i_started_old_when
		return r_boolean, r_duration
	def do_new():
		i_started_new_when = im_time.perf_counter()
		r_boolean = _do_cmp_via_os( f1, f2)
		r_duration = im_time.perf_counter() - i_started_new_when
		return r_boolean, r_duration
	i_diffs_show = False # change to False to have things only show when an unexepcted time difference is a factor of more than 4
	i_diffs_logs = False # change to False to have things only show when an unexepcted time difference is a factor of more than 4
	# control the order in which we do the old and new comparison methods
	i_old_then_new = True
	if i_old_then_new :
		old_boolean, old_duration = do_old()
	# then do the new regardless
	new_boolean, new_duration = do_new()
	if not i_old_then_new :
		old_boolean, old_duration = do_old()
	# now decide what to show and/or log
	if new_duration > old_duration * 7 :
		i_diffs_show = True
		i_diffs_logs = True
	elif new_duration > old_duration * 2 :
		i_diffs_logs = True
	if old_boolean != new_boolean :
		r_boolean = False
		i_method = "both differ"
	else :
		i_method = "both same"
		r_boolean = old_boolean
	if i_diffs_show:
		print( "_do_cmp_logger" )
		print( f1 )
		print( f2 )
		if i_old_then_new :
			print( "did Old then New" )
		else :
			print( "did New then Old" )
		print( "Old" + " r_boolean= " + str(old_boolean) + " i_duration= " + str(old_duration) )
		print( "New" + " r_boolean= " + str(new_boolean) + " i_duration= " + str(new_duration) )
		#
		#
	if i_diffs_logs:
		i_size = os.path.getsize( f1 )
		i_datafilename_same_str = cmntry.dtff_make_data_filename( file_compare_method_log_filename(), "FileCompareMethodTimes" )
		# cmntry.file_in_home_text_new_list_as_data_head( i_datafilename_same_str, [ "FileSize", "OldDuration", "NewDuration",  ] )
		cmntry.file_in_home_text_add_list_as_data_line( i_datafilename_same_str, [ str(i_size), str(old_duration), str(new_duration), cmntry.pfn4print( f1 ), cmntry.pfn4print( f2 ) ] )
	return r_boolean

def _do_cmp(f1, f2):
	# this abstracts the original call name, and let's us do dev-time redirections for various testing purposes
	if True :
		return _do_cmp_for_platform( f1, f2)
	elif False :
		return _do_cmp_via_os( f1, f2)
	elif False: # module_experimenting():
		return _do_cmp_logger( f1, f2)
	else :
		return _do_cmp_original( f1, f2)

# Directory comparison class.
#
class dircmp:
    """A class that manages the comparison of 2 directories.

    dircmp(a, b, ignore=None, hide=None)
      A and B are directories.
      IGNORE is a list of names to ignore,
        defaults to DEFAULT_IGNORES.
      HIDE is a list of names to hide,
        defaults to [os.curdir, os.pardir].

    High level usage:
      x = dircmp(dir1, dir2)
      x.report() -> prints a report on the differences between dir1 and dir2
       or
      x.report_partial_closure() -> prints report on differences between dir1
            and dir2, and reports on common immediate subdirectories.
      x.report_full_closure() -> like report_partial_closure,
            but fully recursive.

    Attributes:
     left_list, right_list: The files in dir1 and dir2,
        filtered by hide and ignore.
     common: a list of names in both dir1 and dir2.
     left_only, right_only: names only in dir1, dir2.
     common_dirs: subdirectories in both dir1 and dir2.
     common_files: files in both dir1 and dir2.
     common_funny: names in both dir1 and dir2 where the type differs between
        dir1 and dir2, or the name is not stat-able.
     same_files: list of identical files.
     diff_files: list of filenames which differ.
     funny_files: list of files which could not be compared.
     subdirs: a dictionary of dircmp objects, keyed by names in common_dirs.
     """

    def __init__(self, a, b, ignore=None, hide=None): # Initialize
        #print( app_code_name() + " __init__" )
        self.left = a
        self.right = b
        if hide is None:
            self.hide = [os.curdir, os.pardir] # Names never to be shown
        else:
            self.hide = hide
        if ignore is None:
            self.ignore = DEFAULT_IGNORES
        else:
            self.ignore = ignore

    def phase0(self): # Compare everything except common subdirectories
        #print( app_code_name() + " phase0" )
        # separating out the steps to handle permission and other blockages
        try:
            t_left_listdir = os.listdir(self.left)
        except:
            t_left_listdir = []
        self.left_list = _filter( t_left_listdir, self.hide+self.ignore)
        try:
            t_right_listdir = os.listdir(self.right)
        except:
            t_right_listdir = []
        self.right_list = _filter( t_right_listdir, self.hide+self.ignore)
        self.left_list.sort()
        self.right_list.sort()

    def phase1(self): # Compute common names
        #print( app_code_name() + " phase1" )
        a = dict(zip(map(os.path.normcase, self.left_list), self.left_list))
        b = dict(zip(map(os.path.normcase, self.right_list), self.right_list))
        self.common = list(map(a.__getitem__, filter(b.__contains__, a)))
        self.left_only = list(map(a.__getitem__, filterfalse(b.__contains__, a)))
        self.right_only = list(map(b.__getitem__, filterfalse(a.__contains__, b)))

    def phase2(self): # Distinguish files, directories, funnies
        #print( app_code_name() + " phase2" )
        self.common_dirs = []
        self.common_files = []
        self.common_funny = []

        for x in self.common:
            a_path = os.path.join(self.left, x)
            b_path = os.path.join(self.right, x)

            ok = 1
            try:
                a_stat = os.stat(a_path)
            except OSError as why:
                # print('Can\'t stat', a_path, ':', why.args[1])
                ok = 0
            try:
                b_stat = os.stat(b_path)
            except OSError as why:
                # print('Can\'t stat', b_path, ':', why.args[1])
                ok = 0

            if ok:
                a_type = stat.S_IFMT(a_stat.st_mode)
                b_type = stat.S_IFMT(b_stat.st_mode)
                if a_type != b_type:
                    self.common_funny.append(x)
                elif stat.S_ISDIR(a_type):
                    self.common_dirs.append(x)
                elif stat.S_ISREG(a_type):
                    self.common_files.append(x)
                else:
                    self.common_funny.append(x)
            else:
                self.common_funny.append(x)

    def phase3(self): # Find out differences between common files
        #print( app_code_name() + " phase3" )
        xx = cmpfiles(self.left, self.right, self.common_files)
        self.same_files, self.diff_files, self.funny_files = xx

    def phase4(self): # Find out differences between common subdirectories
        # A new dircmp object is created for each common subdirectory,
        # these are stored in a dictionary indexed by filename.
        # The hide and ignore properties are inherited from the parent
        #print( app_code_name() + " phase4" )
        self.subdirs = {}
        for x in self.common_dirs:
            a_x = os.path.join(self.left, x)
            b_x = os.path.join(self.right, x)
            self.subdirs[x]  = dircmp(a_x, b_x, self.ignore, self.hide)

    def phase4_closure(self): # Recursively call phase4() on subdirectories
        #print( app_code_name() + " phase4_closure" )
        self.phase4()
        for sd in self.subdirs.values():
            sd.phase4_closure()

    def report(self): # Print a report on the differences between a and b
        #print( app_code_name() + " report" )
        # Output format is purposely lousy
        print('diff', self.left, self.right)
        if self.left_only:
            self.left_only.sort()
            print('Only in', self.left, ':', self.left_only)
        if self.right_only:
            self.right_only.sort()
            print('Only in', self.right, ':', self.right_only)
        if self.same_files:
            self.same_files.sort()
            print('Identical files :', self.same_files)
        if self.diff_files:
            self.diff_files.sort()
            print('Differing files :', self.diff_files)
        if self.funny_files:
            self.funny_files.sort()
            print('Trouble with common files :', self.funny_files)
        if self.common_dirs:
            self.common_dirs.sort()
            print('Common subdirectories :', self.common_dirs)
        if self.common_funny:
            self.common_funny.sort()
            print('Common funny cases :', self.common_funny)

    def report_partial_closure(self): # Print reports on self and on subdirs
        #print( app_code_name() + " report_partial_closure" )
        self.report()
        for sd in self.subdirs.values():
            print()
            sd.report()

    def report_full_closure(self): # Report on self and subdirs recursively
        #print( app_code_name() + " report_full_closure" )
        self.report()
        for sd in self.subdirs.values():
            print()
            sd.report_full_closure()

    methodmap = dict(subdirs=phase4,
                     same_files=phase3, diff_files=phase3, funny_files=phase3,
                     common_dirs = phase2, common_files=phase2, common_funny=phase2,
                     common=phase1, left_only=phase1, right_only=phase1,
                     left_list=phase0, right_list=phase0)

    def __getattr__(self, attr):
        if attr not in self.methodmap:
            raise AttributeError(attr)
        self.methodmap[attr](self)
        return getattr(self, attr)

def cmpfiles(a, b, common, shallow=True):
    #print( app_code_name() + " cmpfiles")
    #print( a )
    #print( b )
    #print( common )
    #print( shallow )
    """Compare common files in two directories.

    a, b -- directory names
    common -- list of file names found in both directories
    shallow -- if true, do comparison based solely on stat() information

    Returns a tuple of three lists:
      files that compare equal
      files that are different
      filenames that aren't regular files.

    """
    res = ([], [], [])
    for x in common:
        ax = os.path.join(a, x)
        bx = os.path.join(b, x)
        res[_cmp(ax, bx, shallow)].append(x)
    return res


# Compare two files.
# Return:
#       0 for equal
#       1 for different
#       2 for funny cases (can't stat, etc.)
#
def _cmp(a, b, sh, abs=abs, cmp=cmp):
    #print( app_code_name() + " _cmp" )
    try:
        return not abs(cmp(a, b, sh))
    except OSError:
        return 2


# Return a copy with items that occur in skip removed.
#
def _filter(flist, skip):
    #print( app_code_name() + " _filter" )
    return list(filterfalse(skip.__contains__, flist))


# Demonstration and testing.
#
def demo():
    #print( app_code_name() + " demo" )
    import sys
    import getopt
    options, args = getopt.getopt(sys.argv[1:], 'r')
    if len(args) != 2:
        raise getopt.GetoptError('need exactly two args', None)
    dd = dircmp(args[0], args[1])
    if ('-r', '') in options:
        dd.report_full_closure()
    else:
        dd.report()

if __name__ == '__main__':
	print( "This is the module: filecmp_debug")
	print( "Unlike the rest of Foldatry, this is a fork of the stock module filecmp")
	print( "and it merely modifies the numeric handling of file timestamps in one line of code")
	print( "so as to be more consistent across different file systesm.")
	demo()
