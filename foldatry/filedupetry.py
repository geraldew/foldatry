#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# filedupetry =  find file duplicates
#
# This forms part of the foldatry application but can be used as an
# independent command line tool.
#
# --------------------------------------------------
# Copyright (C) 2018-2024  Gerald Waters - Melbourne, Australia
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# --------------------------------------------------
# Imports - Standard
# --------------------------------------------------

import sys as im_sys
# import os.path
import os as im_os
import os.path as im_osp
# import stat as im_stat
import hashlib as im_hashlib
from collections import namedtuple
# import time as im_time

import copy as im_copy

# --------------------------------------------------
# Imports - Custom
# --------------------------------------------------

import commontry as cmntry
import exploratry as explrtry

# --------------------------------------------------
# Support Custom
# --------------------------------------------------

def mod_code_name():
	return "filedupetry"

def mod_code_abbr():
	return "fldptry"

def mod_show_name():
	return "Filedupetry"

# --------------------------------------------------
# Feature Structure Definitions
# --------------------------------------------------

# --------------------------------------------------
# set up custom named tuples

# tuple for file info when keyed on the fullpathname
# deprecated
tpl_FileInfo = namedtuple('tpl_FileInfo', 'fi_pfn fi_fname fi_fsize fi_fhash')
# incoming
tpl_FileMeta = namedtuple('tpl_FileMeta', 'fi_pfn fi_fname fi_fsize fi_fwhen')
tpl_FileInfoHash = namedtuple('tpl_FileInfoHash', 'fi_pfn fi_fname fi_fsize fi_fwhen fi_fhash')

# --------------------------------------------------
# Support Stock
# --------------------------------------------------

# --------------------------------------------------
# Support Custom
# --------------------------------------------------

def dbg_print_traverse_progress( fld_cnt, dpth):
	if (fld_cnt % 1000) == 0:
		print( "Processed " + str( fld_cnt) + " folders, now at depth of " + str(dpth) )

# --------------------------------------------------
# Features - i.e. requiring awareness of custom structures
# --------------------------------------------------

def make_tpl_shortmatchkey( p_tpl_FileMeta, p_UseSize, p_UseName, p_UseWhen ):
	# note that p_UseHash was NOT a parameter, so responsibility is on the calling routine to set p_UseSize
	i_lst_to_become_tuple = []
	if p_UseSize :
		i_lst_to_become_tuple.append( p_tpl_FileMeta.fi_fsize )
	if p_UseName :
		i_lst_to_become_tuple.append( p_tpl_FileMeta.fi_fname )
	if p_UseWhen :
		i_lst_to_become_tuple.append( p_tpl_FileMeta.fi_fwhen )
	if False and p_tpl_FileMeta.fi_fwhen == -1 :
		print( "make_tpl_shortmatchkey")
		print( "i_lst_to_become_tuple")
		print( i_lst_to_become_tuple)
		input( "Press ENTER")
	return tuple( i_lst_to_become_tuple ) # might need to bullet-proof this? or rely on never being given all False flags

def make_tpl_fullmatchkey( p_tpl_FileMeta, p_HashValue, p_FileFlags ):
	i_lst_to_become_tuple = []
	if p_FileFlags.ff_Hash :
		i_lst_to_become_tuple.append( p_HashValue )
	if p_FileFlags.ff_Size :
		i_lst_to_become_tuple.append( p_tpl_FileMeta.fi_fsize )
	if p_FileFlags.ff_Name :
		i_lst_to_become_tuple.append( p_tpl_FileMeta.fi_fname )
	if p_FileFlags.ff_When :
		i_lst_to_become_tuple.append( p_tpl_FileMeta.fi_fwhen )
	if False and p_tpl_FileMeta.fi_fwhen == -1 :
		print( "make_tpl_fullmatchkey")
		print( "i_lst_to_become_tuple")
		print( i_lst_to_become_tuple)
		input( "Press ENTER")
	return tuple( i_lst_to_become_tuple ) # might need to bullet-proof this? or rely on never being given all False flags

# storage routines
	# - r_dct_pfn_filemeta = dictionary of files by pathfilename each with their metainfo 
	# - r_dct_matchkey_lst_pfn = dictionary of matchkey tuples each with a list of pathfilenames ( ergo pointing to r_dct_pfn_filemeta )
	# - r_dct_hash_lst_pfn = dictionary of content hashes each with a list of pathfilenames ( ergo pointing to r_dct_pfn_filemeta )
	# - r_dct_matchkey_lst_hash = dictionary of matchkey tuples each with a list of hashes ( ergo pointing to r_dct_hash_lst_pfn )

# store a file - key is the pathfilename, content is a named tuple of type tpl_FileInfo
# this is just here to provide a reverse index from pathfilename to get its hash and maybe other info
def store_dct_pfn_tpl_FileMeta( v_dct_pfn_store, p_tpl_FileMeta ):
	# Add or append the file path
	if p_tpl_FileMeta.fi_pfn in v_dct_pfn_store:
		# this shouldn't ever happen so interrupt if it does - e.g. maybe different "paths" led to the same place 
		input( "Warning, unexpected repeat for " + p_tpl_FileMeta.fi_pfn)
	else:
		v_dct_pfn_store[ p_tpl_FileMeta.fi_pfn ] = p_tpl_FileMeta

def store_in_dct_matchkey_lst_pfn( v_dct_matchkey_lst_pfn, p_matchkey, p_pfn ):
	# Add or append the file path
	if p_matchkey in v_dct_matchkey_lst_pfn:
		# trust that we already put a list in there, so add the new tuple to the list (of tuples)
		v_dct_matchkey_lst_pfn[ p_matchkey ].append( p_pfn)
	else:
		# need to put a list there - of just the new tuple (as a list of one item)
		v_dct_matchkey_lst_pfn[ p_matchkey ] = [ p_pfn ]

# store a hashed file into a dictionary
# key is the hash, content is a list of files with that hash
# thus the hashes that have more than one in the list are about replicated content
def store_dct_hash_lst_pfn( v_dct_hash, p_hash, p_pfn ):
	# Add or append the file path
	if p_hash in v_dct_hash:
		# trust that we already put a list in there, so add the new tuple to the list (of tuples)
		v_dct_hash[ p_hash ].append( p_pfn)
	else:
		# need to put a list there - of just the new tuple (as a list of one item)
		v_dct_hash[ p_hash ] = [ p_pfn ]

def store_dct_fullmatchkey_lst_fileinfohash( v_dct_matchkey, p_mkey, p_fileinfohash ):
	# Add or append the file path
	if p_mkey in v_dct_matchkey:
		# trust that we already put a list in there, so add the new tuple to the list (of tuples)
		v_dct_matchkey[ p_mkey ].append( p_fileinfohash)
	else:
		# need to put a list there - of just the new tuple (as a list of one item)
		v_dct_matchkey[ p_mkey ] = [ p_fileinfohash ]

def store_dct_fullmatchkey_lst_pfn( v_dct_matchkey, p_mkey, p_pfn ):
	# Add or append the file path
	if p_mkey in v_dct_matchkey:
		# trust that we already put a list in there, so add the new tuple to the list (of tuples)
		v_dct_matchkey[ p_mkey ].append( p_pfn)
	else:
		# need to put a list there - of just the new tuple (as a list of one item)
		v_dct_matchkey[ p_mkey ] = [ p_pfn ]

# becoming obsolete

# store a file - key is the pathfilename, content is a named tuple of type tpl_FileInfo
# this is just here to provide a reverse index from pathfilename to get its hash and maybe other info
def store_dct_pfn_tpl_FileInfo( dct_pfn_store, p_tpl_FileInfo ):
	# Add or append the file path
	if p_tpl_FileInfo.fi_pfn in dct_pfn_store:
		# this shouldn't ever happen so interrupt if it does - e.g. maybe different "paths" led to the same place 
		input( "Warning, unexpected repeat for " + p_tpl_FileInfo.fi_pfn)
	else:
		dct_pfn_store[p_tpl_FileInfo.fi_pfn] = p_tpl_FileInfo

def store_dct_size_lst_add_PathFile( p_dct_size_lst_pfn, p_tpl_FileInfo ):
	# store a sized file into a dictionary
	# key is the size, content is a list of files with that size - as the file pathname
	# thus the sizes that have more than one in the list are worth hashing
	# Add or append the file path
	if p_tpl_FileInfo.fi_fsize in p_dct_size_lst_pfn:
		# trust that we already put a list in there, so add the new tuple to the list (of tuples)
		p_dct_size_lst_pfn[ p_tpl_FileInfo.fi_fsize ].append( p_tpl_FileInfo)
	else:
		# need to put a list there - of just the new tuple (as a list of one item)
		p_dct_size_lst_pfn[ p_tpl_FileInfo.fi_fsize ] = [p_tpl_FileInfo]


# store a hashed file into a dictionary
# key is the hash, content is a list of files with that hash - as named tuples of type tpl_FileInfo
# thus the hashes that have more than one in the list are about replicated content
def store_dct_hash_lst_add_tpl_FileInfo( p_dct_hash_store, p_tpl_FileInfo ):
	# Add or append the file path
	if p_tpl_FileInfo.fi_fhash in p_dct_hash_store:
		# trust that we already put a list in there, so add the new tuple to the list (of tuples)
		p_dct_hash_store[p_tpl_FileInfo.fi_fhash].append( p_tpl_FileInfo)
	else:
		# need to put a list there - of just the new tuple (as a list of one item)
		p_dct_hash_store[p_tpl_FileInfo.fi_fhash] = [p_tpl_FileInfo]
	#p_multi_log.do_logs( "b", "store_dct_hash_lst_add_tpl_FileInfo" + "#=" + p_tpl_FileInfo.fi_fhash + " pfn=" + cmntry.pfn4print( p_tpl_FileInfo.fi_pfn), p_dct_log_set )



# hash derivations

# per file hash - basically a token method to be replaced by something faster later on
def filehash( filepath):
	blocksize = 64*1024
	sha = im_hashlib.md5()
	#sha = im_hashlib.sha256()
	try:
		with open(filepath, 'rb') as fp:
			while True:
				data = fp.read(blocksize)
				if not data:
					break
				sha.update(data)
		got_hash = sha.hexdigest()
		got_ok = True
	except IOError as e:
		print( "I/O error({0}): {1}".format(e.errno, e.strerror) )
		print( "File: " + filepath )
		got_ok = False
		got_hash = ""
	except: #handle other exceptions such as attribute errors
		print( "Unexpected error:", sys.exc_info()[0] )
		print( "File: " + filepath )
		got_ok = False
		got_hash = ""
	return got_ok, got_hash

def hash_of_lst_str( p_strings ):
	i_hash = im_hashlib.sha1()
	for s in p_strings:
		i_s = cmntry.pfn4print( s )
		i_hash.update( struct.pack( "I", len( i_s ) ) )
		i_hash.update( i_s.encode( 'utf-8' ) )
	return i_hash.hexdigest()

# --------------------------------------------------
# Process - Traverse
# --------------------------------------------------

# recursively traverse a folder structure, deriving and storing the paths for each size+ as key
def traverse_tree_make_sizes_dct( v_dct_size_lst_pfn, v_dct_folderselfs, p_tree_path, p_multi_log):
	# this is the outer non-recursive part
	# currently the only difference is the use of a depth counter in the recursive call
	# and that's only there to help with debugging
	if __debug__:
		p_multi_log.do_logs( "b", "#Begin#" )
		p_multi_log.do_logs( "b", "? " + cmntry.pfn4print( p_tree_path ) )
	i_ncalls = 0
	def traverse_tree_recurse( v_dct_size_lst_pfn, v_dct_pfn_stock, p_tree_path, p_tree_depth, p_multi_log):
		# d_hash_lst_tplfilinf is a dictionary being built, with hashes for keys, and lists of tuples for values
		# v_dct_pfn_stock is a dictionary being built, with folderpaths for keys, and tuples for values
		# p_tree_path is the folder location now being accounted 
		nonlocal i_ncalls
		i_ncalls += 1
		if __debug__:
			# log every path
			p_multi_log.do_logs( "b", "@ " + cmntry.pfn4print( p_tree_path ) )
		# dbg_print_traverse_progress( i_ncalls, p_tree_depth)
		#print( p_tree_path )
		#print( "i_ncalls: " + str(i_ncalls) )
		if cmntry.is_n_to_show_progess( i_ncalls ) :
			p_multi_log.do_logs( "s", "Folder# " + str(i_ncalls) + " Depth:" + str(p_tree_depth) + " @ " + cmntry.pfn4print( p_tree_path ) )
			#print( p_tree_path )
		entries = explrtry.get_lst_sorted_os_scandir( p_tree_path )
		filcount = 0
		dircount = 0
		for entry in entries:
			if entry.is_file(follow_symlinks=False):
				# assemble specifics and derive hash
				pfn = im_osp.join( p_tree_path, entry.name) 
				size = entry.stat().st_size
				# an alternate size derivation is: 
				# size = im_os.path.getsize( pfn)
				# make as a tuple
				n_t = tpl_FileInfo( pfn, entry.name, size, "")
				# now store it
				store_dct_size_lst_add_PathFile( v_dct_size_lst_pfn, n_t )
				store_dct_pfn_tpl_FileInfo( v_dct_pfn_stock, n_t )
				# logging
				if __debug__:
					pass
					# p_multi_log.do_logs( "b", "# " + sha + " " + pfn )
				filcount = filcount + 1
			elif entry.is_dir(follow_symlinks=False):
				# recursive call returning the hash of the subfolder
				new_subpath = im_os.path.join(p_tree_path, entry.name)
				traverse_tree_recurse( v_dct_size_lst_pfn, v_dct_pfn_stock, new_subpath, p_tree_depth + 1, p_multi_log)
				# count subfolders and prepare for recursion further down
				dircount = dircount + 1
			elif False :
				# for now, just ignore anything else but might use this sometime in the future 
				# because a) not interested in comparing; b) seeing some lockups on stat.S_ISLNK calls on a FIFO
				test_path_fileref = im_osp.join(p_tree_path, entry.name) 
				# test for symbolic link
				test_is_link = stat.S_ISLNK(im_os.stat(test_path_fileref).st_mode)
				this_one_is_link = (test_is_link != 0)
				# test for a pipe or fifo
				test_is_pipe = stat.S_ISFIFO(im_os.stat(test_path_fileref).st_mode)
				this_one_is_pipe = (test_is_pipe != 0)
				if this_one_is_link or this_one_is_pipe:
					pass
					# count_ignore = count_ignore + 1
				else:
					pass
					# count_other = count_other + 1
					p_multi_log.do_logs( "bs", mod_code_name() + " WHAT THE? " + " " + cmntry.pfn4print( p_tree_path ) + " " + cmntry.pfn4print( entry.name ) )
		if __debug__:
			p_multi_log.do_logs( "b", " Done folder:" + cmntry.pfn4print( p_tree_path ) )
		return 
	#
	traverse_tree_recurse( v_dct_size_lst_pfn, v_dct_folderselfs, p_tree_path, 0, p_multi_log)
	if __debug__:
		p_multi_log.do_logs( "b", "#End#" )
	return

# recursively traverse a folder structure, deriving and storing the paths for each size+ as key
def traverse_tree_filling_dictionaries_pfn_matchkey( v_dct_pfn_tplmeta, v_dct_shortmatchkey_of_lst_pfn, p_tree_path, \
	p_UseSize, p_UseName, p_UseWhen, p_multi_log):
	# this is the outer non-recursive part
	# currently the only difference is the use of a depth counter in the recursive call
	# and that's only there to help with debugging
	# d_hash_lst_tplfilinf is a dictionary being built, with hashes for keys, and lists of tuples for values
	# v_dct_pfn_tplmeta is a dictionary being built, with folderpaths for keys, and tuples for values
	# p_tree_path is the folder location now being accounted 
	def def_name():
		return "traverse_tree_filling_dictionaries_pfn_matchkey "
	def def_name_prfx():
		return def_name() + " "
	# Debugging info
	p_multi_log.do_logs( "dbsp", def_name_prfx() + "(p_UseSize, p_UseName, p_UseWhen) " + str(p_UseSize) + "," + str(p_UseName) + "," + str(p_UseWhen) )
	if __debug__:
		p_multi_log.do_logs( "b", def_name_prfx() + "#Begin#" )
		p_multi_log.do_logs( "b", def_name_prfx() + "? " + cmntry.pfn4print( p_tree_path ) )
	i_ncalls = 0
	def traverse_tree_recurse( p_tree_path, p_tree_depth, p_multi_log):
		nonlocal v_dct_shortmatchkey_of_lst_pfn 
		nonlocal v_dct_pfn_tplmeta
		nonlocal p_UseSize
		nonlocal p_UseName
		nonlocal p_UseWhen
		nonlocal i_ncalls
		i_ncalls += 1
		if __debug__:
			# log every path
			p_multi_log.do_logs( "b", "@ " + cmntry.pfn4print( p_tree_path ) )
		# dbg_print_traverse_progress( i_ncalls, p_tree_depth)
		#print( p_tree_path )
		#print( "i_ncalls: " + str(i_ncalls) )
		if cmntry.is_n_to_show_progess( i_ncalls ) :
			p_multi_log.do_logs( "s", "Folder# " + str(i_ncalls) + " Depth:" + str(p_tree_depth) + " @ " + cmntry.pfn4print( p_tree_path ) )
			#print( p_tree_path )
		entries = explrtry.get_lst_sorted_os_scandir( p_tree_path )
		filcount = 0
		dircount = 0
		for entry in entries:
			if entry.is_file(follow_symlinks=False):
				# assemble specifics and derive hash
				pfn = im_osp.join( p_tree_path, entry.name) 
				size = entry.stat().st_size
				# an alternate size derivation is: 
				# size = im_os.path.getsize( pfn)
				i_f_when = int( entry.stat().st_mtime )
				# make as a tuple and store it
				t_tpl_FileMeta = tpl_FileMeta( pfn, entry.name, size, i_f_when )
				store_dct_pfn_tpl_FileMeta( v_dct_pfn_tplmeta, t_tpl_FileMeta )
				# make matchkey tuple and store it
				t_matchkey = make_tpl_shortmatchkey( t_tpl_FileMeta, p_UseSize, p_UseName, p_UseWhen )
				store_in_dct_matchkey_lst_pfn( v_dct_shortmatchkey_of_lst_pfn, t_matchkey, pfn )
				# logging
				if __debug__:
					pass
					# p_multi_log.do_logs( "b", "# " + sha + " " + pfn )
				filcount = filcount + 1
			elif entry.is_dir(follow_symlinks=False):
				# recursive call returning the hash of the subfolder
				new_subpath = im_os.path.join(p_tree_path, entry.name)
				traverse_tree_recurse( new_subpath, p_tree_depth + 1, p_multi_log)
				# count subfolders and prepare for recursion further down
				dircount = dircount + 1
			elif False :
				# for now, just ignore anything else but might use this sometime in the future 
				# because a) not interested in comparing; b) seeing some lockups on stat.S_ISLNK calls on a FIFO
				test_path_fileref = im_osp.join(p_tree_path, entry.name) 
				# test for symbolic link
				test_is_link = stat.S_ISLNK(im_os.stat(test_path_fileref).st_mode)
				this_one_is_link = (test_is_link != 0)
				# test for a pipe or fifo
				test_is_pipe = stat.S_ISFIFO(im_os.stat(test_path_fileref).st_mode)
				this_one_is_pipe = (test_is_pipe != 0)
				if this_one_is_link or this_one_is_pipe:
					pass
					# count_ignore = count_ignore + 1
				else:
					pass
					# count_other = count_other + 1
					p_multi_log.do_logs( "bs", mod_code_name() + " WHAT THE? " + " " + cmntry.pfn4print( p_tree_path ) + " " + cmntry.pfn4print( entry.name ) )
		if __debug__:
			p_multi_log.do_logs( "b", " Done folder:" + cmntry.pfn4print( p_tree_path ) )
		return 
	#
	traverse_tree_recurse( p_tree_path, 0, p_multi_log)
	if __debug__:
		p_multi_log.do_logs( "b", "#End#" )
	return


# --------------------------------------------------
# Process - Analyse
# --------------------------------------------------

def build_hash_dct_from_size_dct( p_dct_size_lst_pfn, p_multi_log):
	r_hash_lst_tplfilinf = {}
	hash_n = 0
	for f_size in p_dct_size_lst_pfn.keys() :
		if len( p_dct_size_lst_pfn[ f_size]) > 1 :
			for old_tpl in p_dct_size_lst_pfn[ f_size] :
				hash_n += 1
				hash_ok, f_hash = filehash( old_tpl.fi_pfn )
				new_tpl = tpl_FileInfo( old_tpl.fi_pfn, old_tpl.fi_fname, old_tpl.fi_fsize, f_hash)
				store_dct_hash_lst_add_tpl_FileInfo( r_hash_lst_tplfilinf, new_tpl )
	return r_hash_lst_tplfilinf

def build_dct_hash_lst_pfn_from_dct_shortmatchkey( p_dct_matchkey_lst_pfn, p_include_lists_of_one, p_multi_log):
	p_multi_log.do_logs( "spdb", "build_dct_hash_lst_pfn_from_dct_shortmatchkey" )
	r_dct_hash_lst_pfn = {}
	at_match_num = 0
	at_hash_num = 0
	i_time_mark = cmntry.is_n_to_show_progess_timed_get_mark()
	for f_key in p_dct_matchkey_lst_pfn.keys() :
		at_match_num += 1
		#print( "f_key" )
		#print( f_key )
		# while we're here we may as well ignore lists of just one item
		if p_include_lists_of_one or len( p_dct_matchkey_lst_pfn[ f_key] ) > 1 :
			in_list_at = 0
			#print( "p_dct_matchkey_lst_pfn[ f_key]" )
			#print( p_dct_matchkey_lst_pfn[ f_key] )
			for t_pfn in p_dct_matchkey_lst_pfn[ f_key] :
				in_list_at += 1
				#print( "t_pfn" )
				#print( t_pfn )
				at_hash_num += 1
				# write progress to file logs
				if cmntry.is_n_to_show_progess( at_hash_num ) :
					p_multi_log.do_logs( "db", "Match # " + str( at_match_num) + " list item " + str( in_list_at) + " Hashing file # " + str( at_hash_num) + " " + cmntry.pfn4print( t_pfn ) )
				# but only log progress to screen if delays
				b, i_time_mark = cmntry.is_n_to_show_progess_timed_stock( at_hash_num, i_time_mark)
				if b :
					p_multi_log.do_logs( "sp", "Match # " + str( at_match_num) + " list item " + str( in_list_at) + " Hashing file # " + str( at_hash_num) + " " + cmntry.pfn4print( t_pfn ) )
				hash_ok, f_hash = filehash( t_pfn )
				store_dct_hash_lst_pfn( r_dct_hash_lst_pfn, f_hash, t_pfn )
	return r_dct_hash_lst_pfn

def make_dct_size_lst_hashed_with_multi_files( p_hsh_dctnry ):
	keys_of_multi_list_hashes = []
	for hashkey in p_hsh_dctnry.keys():
		if len( p_hsh_dctnry[hashkey] ) > 1:
			keys_of_multi_list_hashes.append( hashkey)
	dtc_size_lst_hash = {}
	for one_hash_key in keys_of_multi_list_hashes:
		a_tpl = p_hsh_dctnry[one_hash_key][0]
		if a_tpl.fi_fsize in dtc_size_lst_hash :
			dtc_size_lst_hash[ a_tpl.fi_fsize ].append( one_hash_key)
		else :
			dtc_size_lst_hash[ a_tpl.fi_fsize ] = [one_hash_key]
	return dtc_size_lst_hash

def make_dct_fullmatchkey_lst_fileinfohash( p_dct_hash_lst_pfn, p_dct_pfn_filemeta, p_FileFlags, p_multi_log ):
	# this performs the hash derivations by reading all the file contents
	# this version creats a dictionary keyed by mullmatchkey tuples with each value as a list of tpl_FileInfoHash items
	def def_name():
		return "make_dct_fullmatchkey_lst_fileinfohash "
	def def_name_prfx():
		return def_name() + " "
	if __debug__:
		p_multi_log.do_logs( "spd", def_name() )
	r_dct_fullmatchkey_lst_fileinfohash = {}
	at_hash_num = 0
	at_file_num = 0
	for t_hash in p_dct_hash_lst_pfn.keys() :
		at_hash_num += 1
		if at_hash_num > 100 and cmntry.is_n_to_show_progess( at_hash_num ):
			p_multi_log.do_logs( "spd", def_name_prfx() + "Mapping hash # " + str( at_hash_num) )
		t_lst_pfn = p_dct_hash_lst_pfn[ t_hash ]
		for pfn in t_lst_pfn :
			at_file_num += 1
			if at_file_num > 100 and cmntry.is_n_to_show_progess( at_file_num ):
				p_multi_log.do_logs( "spd", def_name_prfx() + "Mapping file # " + str( at_file_num) )
			# fetch the meta info for this file, for to build a new tuple key
			t_tpl_FileMeta = p_dct_pfn_filemeta[ pfn ]
			t_matchkey = make_tpl_fullmatchkey( t_tpl_FileMeta, t_hash, p_FileFlags )
			t_tpl_FileInfoHash = tpl_FileInfoHash( t_tpl_FileMeta.fi_pfn, t_tpl_FileMeta.fi_fname, t_tpl_FileMeta.fi_fsize, t_tpl_FileMeta.fi_fwhen, t_hash )
			store_dct_fullmatchkey_lst_fileinfohash( r_dct_fullmatchkey_lst_fileinfohash, t_matchkey, t_tpl_FileInfoHash )
			if False :
				if len( r_dct_fullmatchkey_lst_fileinfohash ) > at_hash_num :
					print( at_hash_num )
					print( str( len( r_dct_fullmatchkey_lst_fileinfohash ) ) )
					print( t_tpl_FileInfoHash )
					print( r_dct_fullmatchkey_lst_fileinfohash )
					#input()
	return r_dct_fullmatchkey_lst_fileinfohash

def make_dct_fullmatchkey_lst_pfn( p_dct_hash_lst_pfn, p_dct_pfn_filemeta, p_FileFlags, p_multi_log ):
	# this performs the hash derivations by reading all the file contents
	# this version creats a dictionary keyed by fullmatchkey tuples with each value as a list of pfn
	def def_name():
		return "make_dct_fullmatchkey_lst_pfn "
	def def_name_prfx():
		return def_name() + " "
	if __debug__:
		p_multi_log.do_logs( "spd", def_name() )
	r_dct_fullmatchkey_lst_pfn = {}
	at_hash_num = 0
	at_file_num = 0
	for t_hash in p_dct_hash_lst_pfn.keys() :
		at_hash_num += 1
		if at_hash_num > 100 and cmntry.is_n_to_show_progess( at_hash_num ):
			p_multi_log.do_logs( "spd", def_name_prfx() + "Mapping hash # " + str( at_hash_num) )
		t_lst_pfn = p_dct_hash_lst_pfn[ t_hash ]
		for pfn in t_lst_pfn :
			at_file_num += 1
			if at_file_num > 100 and cmntry.is_n_to_show_progess( at_file_num ):
				p_multi_log.do_logs( "spd", def_name_prfx() + "Mapping file # " + str( at_file_num) )
			# fetch the meta info for this file, for to build a new tuple key
			t_tpl_FileMeta = p_dct_pfn_filemeta[ pfn ]
			t_matchkey = make_tpl_fullmatchkey( t_tpl_FileMeta, t_hash, p_FileFlags )
			store_dct_fullmatchkey_lst_pfn( r_dct_fullmatchkey_lst_pfn, t_matchkey, pfn )
			if False :
				if len( r_dct_fullmatchkey_lst_pfn ) > at_hash_num :
					print( at_hash_num )
					print( str( len( r_dct_fullmatchkey_lst_pfn ) ) )
					print( t_tpl_FileInfoHash )
					print( r_dct_fullmatchkey_lst_pfn )
					#input()
	return r_dct_fullmatchkey_lst_pfn


# --------------------------------------------------
# Process - Categorise
# --------------------------------------------------


# --------------------------------------------------
# Expose displays
# --------------------------------------------------

# 
def CheckMatchAtKey(dictx, hashkey):
	le_list = dictx[hashkey]
	if len(le_list) > 0:
		return True
	else:
		return False

def CheckHashHasMultiples(hash_dict, hash_key):
	le_list = hash_dict[hash_key]
	if len(le_list) > 1:
		return True
	else:
		return False

# almost a silly function as this also gets done when building structures but left here in case it's needed for retesting
def GetCount_Hashes_With_MultipleFolders( hsh_dctnry ):
	keys_of_multi_list_hashes = []
	for hashkey in hsh_dctnry.keys():
		if len( hsh_dctnry[hashkey] ) > 1:
			keys_of_multi_list_hashes.append( hashkey)
	multi_hash_count = len(keys_of_multi_list_hashes)
	return multi_hash_count

# only here for use when debugging
def display_multi_matches( hsh_dctnry, pth_dctnry, p_multi_log ):
	if __debug__:
		p_multi_log.do_logs( "b", "#Begin#" + "-------------------------------" )
	keys_of_multi_list_hashes = []
	for hashkey in hsh_dctnry.keys():
		if len( hsh_dctnry[hashkey] ) > 1:
			keys_of_multi_list_hashes.append( hashkey)
	# keys_of_multi_list_hashes = list(filter(lambda x: len(x) > 1, hsh_dctnry.keys()))
	multi_hash_count = len(keys_of_multi_list_hashes)
	if len(keys_of_multi_list_hashes) > 0:
		if __debug__:
			p_multi_log.do_logs( "b", "! Duplicates Found:" )
			p_multi_log.do_logs( "b", "! The following folders match. The file contents could differ, but the lists are identical" )
		for one_hash_key in keys_of_multi_list_hashes:
			p_multi_log.do_logs( "b", "! ====================================" )
			p_multi_log.do_logs( "b", '! Hash :' + one_hash_key )
			if len(hsh_dctnry[one_hash_key]) > 1:
				no_head = True
				for subresult in hsh_dctnry[one_hash_key]:
					p_multi_log.do_logs( "b", " File: " + cmntry.pfn4print( str(subresult.fi_pfn) ) )
	else:
		p_multi_log.do_logs( "rdbs", "! No duplicates found." )
	p_multi_log.do_logs( "b", "#End#" )
	return multi_hash_count

# only here for use when debugging
def display_biggest_multi_matches( p_dct_size_lst_hash_multi_files, p_dct_hash_lst_files, p_dct_file_selves, p_multi_log ):
	if __debug__:
		p_multi_log.do_logs( "b", "#Begin#" + "-------------------------------" )
	for sizekey in sorted( p_dct_size_lst_hash_multi_files.keys(), reverse=True):
		p_multi_log.do_logs( "db", '! Size:' + cmntry.hndy_GetHumanReadable( sizekey) )
		lst_hash = p_dct_size_lst_hash_multi_files[ sizekey]
		for a_hash in lst_hash :
			p_multi_log.do_logs( "db", ' # Hash:' + a_hash )
			for tpl_f in p_dct_hash_lst_files[ a_hash] :
				p_multi_log.do_logs( "db", '  File: ' + cmntry.pfn4print( tpl_f.fi_pfn ) )


# --------------------------------------------------
# Command
# --------------------------------------------------

# challenge is to incorporate 		i_Hash, i_Size, i_Name, i_When
# challenge is to incorporate 		p_UseHash, p_UseSize, p_UseName, p_UseWhen

def filedupetry_command( p_tree_paths, p_multi_log):
	# Parameters
	# p_tree_paths :: list of strings := giving the paths to be analysed
	# p_dct_log_flags :: dictionary := v_dct_k_outctrl_v_flagbool
	# recursively find files and store their hashes and details 
	# Returns:
	# r_dct_hash_lst_tplfilinf = a dictionary of content hashes with for each a list of file-info tuples
	# r_dct_file_selves = a dictionary of the discovered files, with for each a file-info tuples
	# r_dct_size_lst_hash_multi_files = a dictionary of filesize with for each a list of hashes 
	# ---------------------
	def def_name():
		return "filedupetry_command"
	# Debugbing info
	p_multi_log.do_logs( "dbsp", def_name() + " Phase:0: Setup" )
	for p_t_p in p_tree_paths :
		p_multi_log.do_logs( "dbsp", def_name() + " Path= " + cmntry.pfn4print( p_t_p ) )
	# ---------------------
	# Phase 0 - preparations
	# set up structures for Phase 1
	i_dct_size_lst_pfn = {} # dictionary of file sizes, each with a list of pathfiles
	r_dct_file_selves = {} # dictionary of file pathfilenames with meta-info about them
	#
	p_UseHash, p_UseSize, p_UseName, p_UseWhen = ( True, True, False, False ) # the same as the original code was doing
	# force doing a size round if we're going to hash contents
	if p_UseHash :
		p_UseSize = True
	# ---------------------
	# Phase 1 - tree traversal
	#
	p_multi_log.do_logs( "dbsp", def_name() + " Phase:1: Tree traversal" )
	# Traverse the tree calculating and collecting hashes
	tpi = 0
	tpn = len(p_tree_paths)
	for i_tree_path in p_tree_paths:
		tpi = tpi + 1
		p_multi_log.do_logs( "dbs", "Traversing path " + str(tpi) + " of " + str(tpn) + ": " + cmntry.pfn4print( i_tree_path ) )
		# because we're doing this call in a loop, we want to pass mutable dictionaries through to be updated
		traverse_tree_make_sizes_dct( i_dct_size_lst_pfn, r_dct_file_selves, i_tree_path, p_multi_log)
	# report phase results
	# ---------------------
	# Phase 2 - Hashes for files the same size
	#
	p_multi_log.do_logs( "dbsp", def_name() + " Phase 2 - Hashing" )
	r_dct_hash_lst_tplfilinf = build_hash_dct_from_size_dct( i_dct_size_lst_pfn, p_multi_log)
	if __debug__:
		p_multi_log.do_logs( "pd", "In Phase 1 we found " )
		p_multi_log.do_logs( "pd", "Found " + str( len(r_dct_file_selves)) + " files" )
		p_multi_log.do_logs( "pd", "Found " + str( len(r_dct_hash_lst_tplfilinf)) + " hashes" )
		multiple_hash_count = GetCount_Hashes_With_MultipleFolders( r_dct_hash_lst_tplfilinf )
		p_multi_log.do_logs( "pdb", "Found " + str( multiple_hash_count) + " hashes that were shared by multiple files" )
		# leave this here in case we need to check some time during dev
		if False:
			display_multi_matches( r_dct_hash_lst_tplfilinf, r_dct_file_selves, p_multi_log )
	# ---------------------
	# Phase 3 - Analysis
	#
	# make a size based dictionary of the hashes
	p_multi_log.do_logs( "dbsp", def_name() + " Phase 3 - Analysis" )
	r_dct_size_lst_hash_multi_files = make_dct_size_lst_hashed_with_multi_files( r_dct_hash_lst_tplfilinf )
	#
	if __debug__:
		display_biggest_multi_matches( r_dct_size_lst_hash_multi_files, r_dct_hash_lst_tplfilinf, r_dct_file_selves, p_multi_log )
	# ---------------------
	# Final Phase - returns
	#     r_dct_hash_lst_tplfilinf = dictionary of hashes, each with a list of folders
	#     r_dct_file_selves = dictionary of files
	#     r_dct_size_lst_hash_multi_files = 
	return r_dct_hash_lst_tplfilinf, r_dct_file_selves, r_dct_size_lst_hash_multi_files

# this is the replacement function, to use a matchkey according to four boolean settings
def filedupetry_collect( p_tree_paths, p_FileFlags, p_multi_log): 
	# Parameters
	# p_tree_paths :: list of strings := giving the paths to be analysed
	# p_dct_log_flags :: dictionary := v_dct_k_outctrl_v_flagbool
	# recursively find files and store their hashes and details 
	# Returns:
	# r_dct_pfn_filemeta = a dictionary of the discovered files, with for each a file-info tuples
	# r_dct_hash_lst_filemeta = a dictionary of content hashes with for each a list of file-info tuples
	# r_dct_matchkey_lst_hash_multi_files = a dictionary of filesize with for each a list of hashes 
	# ---------------------
	def def_name():
		return "filedupetry_collect "
	def def_name_prfx():
		return def_name() + " "
	# Debugging info
	p_multi_log.do_logs( "dbsp", def_name_prfx() + " " + cmntry.GetStringToShow_tpl_FileFlags( p_FileFlags) )
	for p_t_p in p_tree_paths :
		p_multi_log.do_logs( "dbsp", def_name_prfx() + "Path= " + cmntry.pfn4print( p_t_p ) )
	# ---------------------
	# Phase 0 - preparations
	# set up structures for Phase 1
	r_dct_pfn_filemeta = {} # dictionary of file pathfilenames with meta-info about them
	r_dct_shortmatchkey_lst_pfn = {} # dictionary of file sizes, each with a list of pathfiles
	# force doing a size round if we're going to hash contents
	if p_FileFlags.ff_Hash :
		i_UseSize = True
	else :
		i_UseSize = p_FileFlags.ff_Size
	# ---------------------
	# Phase 1 - tree traversal
	#
	p_multi_log.do_logs( "dbsp", def_name_prfx() + "Phase:1: Tree traversal" )
	# Traverse the tree calculating and collecting hashes
	tpi = 0
	tpn = len(p_tree_paths)
	for i_tree_path in p_tree_paths:
		tpi = tpi + 1
		p_multi_log.do_logs( "dbs", def_name_prfx() + "Traversing path " + str(tpi) + " of " + str(tpn) + ": " + cmntry.pfn4print( i_tree_path ) )
		# because we're doing this call in a loop, we want to pass mutable dictionaries through to be updated
		traverse_tree_filling_dictionaries_pfn_matchkey( r_dct_pfn_filemeta, r_dct_shortmatchkey_lst_pfn, i_tree_path, i_UseSize, p_FileFlags.ff_Name, p_FileFlags.ff_When, p_multi_log)
	# report phase results
	if __debug__:
		p_multi_log.do_logs( "spd", def_name_prfx() + "In Phase 1 we found " )
		p_multi_log.do_logs( "spd", def_name_prfx() + "Found " + str( len(r_dct_pfn_filemeta)) + " files" )
		p_multi_log.do_logs( "spd", def_name_prfx() + "Found " + str( len(r_dct_shortmatchkey_lst_pfn)) + " matchkeys" )
	# ---------------------
	# Final Phase - returns
	# - r_dct_pfn_filemeta = dictionary of files by pathfilename each with their metainfo 
	# - r_dct_shortmatchkey_lst_pfn = dictionary of matchkey tuples each with a list of pathfilenames ( ergo pointing to r_dct_pfn_filemeta )
	# Note that none of these have been reduced to more-than-one lists only, so that might prudently be done straight after the return
	return r_dct_pfn_filemeta, r_dct_shortmatchkey_lst_pfn

def hasherdabbery( r_dct_shortmatchkey_lst_pfn, r_dct_pfn_filemeta, p_include_lists_of_one, p_FileFlags, p_multi_log):
	# Phase 2 - Hashing and/or Ordered Indexing
	# If we're required to do content hashing - which will take quite some time - then we do that and then compile a dictionary of the sizes that points to the hashes
	def def_name():
		return "hasherdabbery "
	def def_name_prfx():
		return def_name() + " "
	p_multi_log.do_logs( "dbsp", def_name_prfx() + "Hash Collection Phase" )
	# build the hash dictionary for all the files
	i_dct_hash_lst_pfn = build_dct_hash_lst_pfn_from_dct_shortmatchkey( r_dct_shortmatchkey_lst_pfn, False, p_multi_log) # p_include_lists_of_one = False - for now
	# build a full-matchkey dictionary of the hashes 
	#X r_dct_fullmatchkey_lst_fileinfohash = make_dct_fullmatchkey_lst_fileinfohash( i_dct_hash_lst_pfn, r_dct_pfn_filemeta, p_FileFlags, p_multi_log )
	# NOTE: the previously interim stage of having a dictionary keyed on just the hashes is now redundent
	# this is because the fullmatchkey tuple INCLUDES THE HASH - i.e. so we don't have any situation of the same shortmatchkey combination having different hashes
	p_multi_log.do_logs( "dbsp", def_name_prfx() + "Full match key dictionary assembly phase" )
	r_dct_fullmatchkey_lst_pfn = make_dct_fullmatchkey_lst_pfn( i_dct_hash_lst_pfn, r_dct_pfn_filemeta, p_FileFlags, p_multi_log )
	if __debug__:
		p_multi_log.do_logs( "spd", def_name_prfx() + "In the Hash Collection Phase we found " )
		p_multi_log.do_logs( "spd", def_name_prfx() + "Found " + str( len( i_dct_hash_lst_pfn)) + " hashes" )
		p_multi_log.do_logs( "spd", def_name_prfx() + "Found " + str( len( r_dct_fullmatchkey_lst_pfn)) + " fullmatchkeys" )
	# - r_dct_hash_lst_pfn = dictionary of content hashes each with a list of pathfilenames ( ergo pointing to r_dct_pfn_filemeta )
	return r_dct_fullmatchkey_lst_pfn

# now some functions to be used after doing the above collect call

def reduce_dct_of_lst_to_only_multiples( v_dct_of_lst_filemeta ):
	# go through the dictionary and remove keys where there is only a list of one
	# while we could have done that within the above function, it isn't practically different to do it outside
	# and allows the usage of the above to include situations where knowing all the single discoveries
	for akey in v_dct_of_lst_filemeta.keys():
		if len( v_dct_of_lst_filemeta[ akey ] ) < 2 :
			v_dct_of_lst_filemeta.pop( akey )

def make_dct_matchkey_of_pfn( p_dct_pfn_filemeta):
	pass

def make_dct_matchkey_lst_multifiles( p_dct_pfn_filemeta, p_DelOrderMode ):
	# get which keys have lists of more than one item
	keys_of_multipath_lists = []
	for matchkey in p_dct_pfn_filemeta.keys():
		if len( p_dct_pfn_filemeta[ matchkey ] ) > 1 :
			keys_of_multipath_lists.append( matchkey )
	r_dct_matchkey_lst_multifiles = {}
	for one_key in keys_of_multipath_lists:
		a_tpl = p_dct_pfn_filemeta[ one_key ][0]
		if a_tpl.fi_fsize in r_dct_matchkey_lst_multifiles :
			r_dct_matchkey_lst_multifiles[ a_tpl.fi_fsize ].append( one_key)
		else :
			r_dct_matchkey_lst_multifiles[ a_tpl.fi_fsize ] = [one_key]
	return r_dct_matchkey_lst_multifiles

"""
		r_dct_matchkey_lst_hash_multi_files = make_dct_size_lst_hashed_with_multi_files( r_dct_hash_lst_filemeta )

		# now make the non-hash equivalent
		r_dct_matchkey_lst_multifiles = make_dct_matchkey_lst_multifiles( r_dct_pfn_filemeta )
"""

# --------------------------------------------------
# Interactive
# --------------------------------------------------

# Removed - all GUI interaction is now handled by the foldatry module
# If used separately this module will need to be passed command line parameters

# --------------------------------------------------
# Log controls
# --------------------------------------------------

# --------------------------------------------------
# Main - only if run as a program
# --------------------------------------------------

# main 
if __name__ == "__main__":
	print( "This is the module: filedupetry")
	print( "Currently no testing is setup for this module.")
