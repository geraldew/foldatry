#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# comparatry = file and folder comparison module for Foldatry 
# 
# Written to avoid the dreck that is the stock module: filecmp
#
# --------------------------------------------------
# Copyright (C) 2018-2024  Gerald Waters - Melbourne, Australia
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# --------------------------------------------------
# Imports - Standard
# --------------------------------------------------

from enum import Enum, unique, auto

import os as im_os
import stat as im_stat

import os.path as im_osp

from time import perf_counter as fi_perf_counter 
from collections import namedtuple

import psutil as im_psutl

# --------------------------------------------------
# Imports - Custom
# --------------------------------------------------

import exploratry as explrtry

# --------------------------------------------------
# enum for the comparison results
# --------------------------------------------------
@unique
class CompareResult(Enum):
	same = auto() # confirmed as the same
	diff = auto() # confirmed as different
	pend = auto() # pending closer comparison
	errr = auto() # unable to test for some reason

# tuple for file info when keyed on the fullpathname
ntpl_CompareResultLookup = namedtuple('ntpl_CompareResultLookup', 'crl_StrShow crl_StrAbbr')

_dct_CompareResult_Lookups = { \
	CompareResult.same : ntpl_CompareResultLookup( "Same", "==" ) ,
	CompareResult.diff : ntpl_CompareResultLookup( "Different", "<>" ) ,
	CompareResult.pend : ntpl_CompareResultLookup( "Same-ish", "??" ) ,
	CompareResult.errr : ntpl_CompareResultLookup( "Error", "XX" ) }

def StrShow_CompareResult( cr):
	return _dct_CompareResult_Lookups[ cr ].crl_StrShow

def StrAbbr_CompareResult( cr):
	return _dct_CompareResult_Lookups[ cr ].crl_StrAbbr

# --------------------------------------------------
# enum for the comparison flags
# --------------------------------------------------
@unique
class CompareFlag(Enum):
	doflag_filename = auto() 
	doflag_size = auto() 
	# timestamps
	doflag_when = auto() # this is the general timestamp
	#
	# doflag_whencreated = auto() 
	# doflag_whenmodified = auto() 
	# doflag_whenrenamed = auto() 
	# doflag_whenaccessed = auto() 
	#
	doflag_contents = auto() # guts body mass gist bulk pith
	#
	# doflag_attributes = auto()
	# doflag_permissions = auto()
	# doflag_metadata = auto()
	#
	doflag_dofiles = auto()
	doflag_dofolds = auto()

def str_CompareFlag( cf):
	if cf == CompareFlag.doflag_filename :
		return "doflag_filename"
	elif cf == CompareFlag.doflag_size :
		return "doflag_size"
	elif cf == CompareFlag.doflag_contents :
		return "doflag_contents"
	else :
		return "WhatThe?"

# --------------------------------------------------
# enum for the comparison modes
# --------------------------------------------------
@unique
class CompareMode(Enum):
	quick = auto() 
	pragmatic = auto() 
	pedantic = auto() 
	obsessive = auto() 
	#custom1 = auto() 
	#custom2 = auto() 
	#custom3 = auto() 
	#custom4 = auto() 

def str_CompareMode( cm):
	if cm == CompareMode.quick :
		return "Quick"
	elif cm == CompareMode.pragmatic :
		return "Pragmatic"
	elif cm == CompareMode.pedantic :
		return "Pedantic"
	elif cm == CompareMode.obsessive :
		return "Obsessive"
	#elif cm == CompareMode.custom1 :
	#	return "Custom 1"
	#elif cm == CompareMode.custom2 :
	#	return "Custom 2"
	#elif cm == CompareMode.custom3 :
	#	return "Custom 3"
	#elif cm == CompareMode.custom4 :
	#	return "Custom 4"
	else :
		return "WhatThe?"

# --------------------------------------------------

def get_dct_flags_for_mode( p_mode ):
	the_dct = {}
	if p_mode == CompareMode.quick :
		the_dct[ CompareFlag.doflag_filename ] = True
		the_dct[ CompareFlag.doflag_size ] = True
		the_dct[ CompareFlag.doflag_contents ] = False
		the_dct[ CompareFlag.doflag_dofiles ] = True
		the_dct[ CompareFlag.doflag_dofolds ] = False
	elif p_mode == CompareMode.pragmatic :
		the_dct[ CompareFlag.doflag_filename ] = True
		the_dct[ CompareFlag.doflag_size ] = True
		the_dct[ CompareFlag.doflag_contents ] = False
		the_dct[ CompareFlag.doflag_dofiles ] = True
		the_dct[ CompareFlag.doflag_dofolds ] = True
	elif p_mode == CompareMode.pedantic :
		the_dct[ CompareFlag.doflag_filename ] = True
		the_dct[ CompareFlag.doflag_size ] = True
		the_dct[ CompareFlag.doflag_contents ] = True
		the_dct[ CompareFlag.doflag_dofiles ] = True
		the_dct[ CompareFlag.doflag_dofolds ] = True
	elif p_mode == CompareMode.obsessive :
		# when more are supported they can be added to this one
		the_dct[ CompareFlag.doflag_filename ] = True
		the_dct[ CompareFlag.doflag_size ] = True
		the_dct[ CompareFlag.doflag_contents ] = True
		the_dct[ CompareFlag.doflag_dofiles ] = True
		the_dct[ CompareFlag.doflag_dofolds ] = True
	return the_dct

# -------------------------------------------------------

def get_memory_available_and_free():
	mem = im_psutl.virtual_memory()
	# svmem(total=10367352832, available=6472179712, percent=37.6, used=8186245120, free=2181107712, active=4748992512, inactive=2758115328, buffers=790724608, cached=3500347392, shared=787554304, slab=199348224)
	return mem.available, mem.free

def print_memory_available_and_free():
	mem_available, mem_free = get_memory_available_and_free()
	print( "- Memory available: " + "{:,}".format( mem_available ) )
	print( "- Memory free: " + "{:,}".format( mem_free ) )

# -------------------------------------------------------

def _do_cmp(f1, f2):
    bufsize = BUFSIZE
    with open(f1, 'rb') as fp1, open(f2, 'rb') as fp2:
        while True:
            b1 = fp1.read(bufsize)
            b2 = fp2.read(bufsize)
            if b1 != b2:
                return False
            if not b1:
                return True

def Intrnl_Buffer_Size_Unit():
	return 1024

def Intrnl_Buffer_Factor_Default():
	return 8

def Intrnl_Buffer_Factor_SafelyHalve( p_factor ):
	if p_factor > 1 :
		return p_factor // 2
	else:
		return 1

def Intrnl_Buffer_Factor_SafelyDouble( p_factor ):
	mem_available, mem_free = get_memory_available_and_free()
	if p_factor * Intrnl_Buffer_Size_Unit() < ( mem_free // 3 ) :
		return p_factor * 2
	elif p_factor < 1024 * 256 :
		return p_factor * 2
	else:
		return p_factor

def Intrnl_Compare_File_Contents( p_filename_a, p_filename_b):
	#?print( "Intrnl_Compare_File_Contents")
	outcome = CompareResult.pend
	try:
		file_a = open( p_filename_a, 'rb')
		a_ok = True
	except:
		a_ok = False
		outcome = CompareResult.errr
	try:
		file_b = open( p_filename_b, 'rb')
		b_ok = True
	except:
		b_ok = False
		outcome = CompareResult.errr
	if (outcome == CompareResult.pend) and a_ok and b_ok :
		buffer_factor = Intrnl_Buffer_Factor_Default()
		buffer_size = buffer_factor * Intrnl_Buffer_Size_Unit()
		uncertain = True
		# mark a start time here
		tac = fi_perf_counter()
		lst_byte_per_tactoc = [] # for tracking the overall progress 
		lst_byte_per_tictoc = [] # for tracking the per buffer progress 
		bytes_read = 0
		lst_factors = [buffer_factor]
		while uncertain:
			# print( "blocking, size=" + str(buffer_size))
			# for handling very large files it would be prudent to adjust the buffer size
			# monitoring the effect that has on the timing
			# mark a before time here
			tic = fi_perf_counter()
			try:
				block_of_a = file_a.read( buffer_size )
				a_read_ok = True
			except:
				a_read_ok = False
				outcome = CompareResult.errr
			try:
				block_of_b = file_b.read( buffer_size)
				b_read_ok = True
			except:
				b_read_ok = False
				outcome = CompareResult.errr
			# mark an after time here
			toc = fi_perf_counter()
			if outcome == CompareResult.errr :
				uncertain = False
			if not ( a_read_ok and b_read_ok ) :
				outcome = CompareResult.pend
				uncertain = False
			elif block_of_a != block_of_b :
				outcome = CompareResult.diff
				uncertain = False
			elif not block_of_a :
				outcome = CompareResult.same
				uncertain = False
			if uncertain: # no point bothering if we have an outcome
				# if enough time has passed since the start time:
				#   calculate and add to lists
				lst_byte_per_tictoc.append( buffer_size / ( toc - tic ))
				bytes_read += buffer_size
				lst_byte_per_tactoc.append( bytes_read / ( tac - toc )  )
				if len( lst_byte_per_tictoc ) > 10 :
					#   analyse the list to choose a new buffer size 
					do_increase_buffer = False
					do_decrease_buffer = False
					#?print( "Recent per buffer rates:" )
					#?for tictoc in lst_byte_per_tictoc[:-5:-1] :
					#?	print( tictoc )
					#?print( "Recent overall progress rates:" )
					#?for tactoc in lst_byte_per_tactoc[:-5:-1] :
					#?	print( tactoc )
					# input( "Press ENTER")
					lst_most_recent_n_tictoc = lst_byte_per_tictoc[:-5:-1]
					lst_previous_n_tictoc = lst_byte_per_tictoc[-5:-10:-1]
					avg_most_recent_n_tictoc = sum( lst_most_recent_n_tictoc ) /  len( lst_most_recent_n_tictoc )
					avg_previous_n_tictoc = sum( lst_previous_n_tictoc ) /  len( lst_previous_n_tictoc )
					if avg_most_recent_n_tictoc >= ( avg_previous_n_tictoc * 0.9 ) :
						do_increase_buffer = True
						do_decrease_buffer = False
					if do_increase_buffer:
						buffer_factor = Intrnl_Buffer_Factor_SafelyDouble( buffer_factor )
						buffer_size = buffer_factor * Intrnl_Buffer_Size_Unit()
						lst_factors.append( buffer_factor)
					elif do_decrease_buffer:
						buffer_factor = Intrnl_Buffer_Factor_SafelyHalve( buffer_factor )
						buffer_size = buffer_factor * Intrnl_Buffer_Size_Unit()
						lst_factors.append( buffer_factor)
		print( "Summary:")
		print( "- Bytes read: " + "{:,}".format( bytes_read ) )
		print( "- Time overall: " + "{:,}".format( toc - tac ) )
		print( "- Rate overall: " + "{:,}".format( bytes_read / (toc - tac) ) )
		print( "- Min rate: " + "{:,}".format( min( lst_byte_per_tictoc ) ) )
		print( "- Max rate: " + "{:,}".format( max( lst_byte_per_tictoc ) ) )
		print( "- Avg rate: " + "{:,}".format( sum( lst_byte_per_tictoc ) / len( lst_byte_per_tictoc ) ) )
		print( "- Block reads: " + "{:,}".format( len( lst_byte_per_tictoc ) ) )
		print( "Factors used: ")
		for f in lst_factors :
			print( f )
	return outcome

# -------------------------------------------------------
# External Usage functions

# two developer level functions, each returning a number of objects from their analysis

def CompareUtility_Files( p_filename_a, p_filename_b, p_mode ):
	# this just compares two nominated files
	# it is a quasi-replacement for the filecmp function def cmp(f1, f2, shallow=True):
	#?print( "CompareUtility_Files")
	outcome = CompareResult.pend
	# get the flags for that mode
	dct_flags = get_dct_flags_for_mode( p_mode )
	if CompareFlag.doflag_filename in dct_flags and dct_flags[ CompareFlag.doflag_filename ] :
		# currently treating this as meaningless, but it might later have a purpose
		pass
	if (outcome == CompareResult.pend) and (CompareFlag.doflag_size in dct_flags) and dct_flags[ CompareFlag.doflag_size ] :
		#?print( "comparing sizes")
		# get their sizes
		try:
			size_a = im_os.path.getsize( p_filename_a )
			a_size_ok = True
		except:
			a_size_ok = False
			outcome = CompareResult.errr
		if (outcome == CompareResult.pend) :
			try:
				size_b = im_os.path.getsize( p_filename_b )
				b_size_ok = True
			except:
				b_size_ok = False
				outcome = CompareResult.errr
		# now compare
		if a_size_ok and b_size_ok :
			if a_size_ok != b_size_ok :
				outcome == CompareResult.diff
				#?print( "diff sizes")
			else:
				#?print( "same sizes")
				pass
		else:
			#?print( "couldn't get a size")
			pass
	if (outcome == CompareResult.pend) and (CompareFlag.doflag_contents in dct_flags) and dct_flags[ CompareFlag.doflag_contents ] :
		#?print( "going to compare contents")
		outcome = Intrnl_Compare_File_Contents( p_filename_a, p_filename_b)
	else:
		#?print( "not going to compare contents")
		pass
	return outcome

def SplitIntoListsByType( p_lst_entries ):
	lst_folds = []
	lst_files = []
	# lst_pipes = []
	# lst_symls = []
	for entry in p_lst_entries:
		if entry.is_file(follow_symlinks=False):
			lst_files.append( entry)
		elif entry.is_dir(follow_symlinks=False):
			lst_folds.append( entry)
	return lst_folds, lst_files #, lst_pipes, lst_symls

def SplitIntoListsByPresence_AssumedSorted( p_lst_a_entries, p_lst_b_entries ):
	lst_a_inonly = []
	lst_b_inonly = []
	lst_inboth = []
	len_a = len( p_lst_a_entries )
	len_b = len( p_lst_b_entries )
	if ( len_a + len_b ) == 0 :
		pass
	elif ( len_a > 0 ) and ( len_b == 0 ) :
		lst_a_inonly = p_lst_a_entries
	elif ( len_a == 0 ) and ( len_b > 0 ) :
		lst_b_inonly = p_lst_b_entries
	elif ( len_a > 0 ) and ( len_b > 0 ) :
		a_ndx = 0
		b_ndx = 0
		# work through the list matching and splitting
		while ( a_ndx < len_a ) and ( b_ndx < len_b ) :
			if p_lst_a_entries[ a_ndx].name == p_lst_b_entries[ b_ndx].name :
				lst_inboth.append( p_lst_a_entries[ a_ndx].name )
				a_ndx += 1
				b_ndx += 1
			elif p_lst_a_entries[ a_ndx].name < p_lst_b_entries[ b_ndx].name :
				lst_a_inonly.append( p_lst_a_entries[ a_ndx].name )
				a_ndx += 1
			elif p_lst_a_entries[ a_ndx].name > p_lst_b_entries[ b_ndx].name :
				lst_b_inonly.append( p_lst_b_entries[ b_ndx].name )
				b_ndx += 1
		# deal with remainders if one list finished first 
		while ( a_ndx < len_a ) :
			lst_inboth.append( p_lst_a_entries[ a_ndx].name )
			a_ndx += 1
		while ( b_ndx < len_b ) :
			lst_inboth.append( p_lst_b_entries[ b_ndx].name )
			b_ndx += 1
	return lst_a_inonly, lst_b_inonly, lst_inboth

def CompareUtility_Folders( p_path_a, p_path_b, p_mode, p_failfast=False ):
	# it is a quasi-replacement for the pair of filecmp components:
	#  filecmp.cmpfiles(dir1, dir2, common, shallow=True) which Returns three lists of file names: match, mismatch, errors.
	#  class filecmp.dircmp(a, b, ignore=None, hide=None) which provides a complex object or properties and methods 
	print( "CompareUtility_Folders")
	outcome = CompareResult.pend
	# the in-only
	lst_a_inonly_folds = []
	lst_a_inonly_files = []
	lst_b_inonly_folds = []
	lst_b_inonly_files = []
	# folders in both
	lst_inboth_folds_same = []
	lst_inboth_folds_diff = []
	lst_inboth_folds_unkn = []
	lst_inboth_folds_bad_a = []
	lst_inboth_folds_bad_b = []
	# files in both
	lst_inboth_files_same = []
	lst_inboth_files_diff = []
	lst_inboth_files_unkn = []
	lst_inboth_files_bad_a = []
	lst_inboth_files_bad_b = []
	# use bulletproofed calls 
	lst_a_entries, a_had_errors = explrtry.try_lst_sorted_os_scandir( p_path_a )
	#?print( "Path a collected " + str(len(lst_a_entries)) + " Error:" + str(a_had_errors) )
	lst_b_entries, b_had_errors = explrtry.try_lst_sorted_os_scandir( p_path_b )
	#?print( "Path b collected " + str(len(lst_b_entries)) + " Error:" + str(b_had_errors) )
	if not a_had_errors and not b_had_errors :
		#?print( "No folder collection errors")
		lst_a_folds, lst_a_files = SplitIntoListsByType( lst_a_entries )
		lst_b_folds, lst_b_files = SplitIntoListsByType( lst_b_entries )
		lst_a_inonly_folds, lst_b_inonly_folds, lst_folds_inboth = SplitIntoListsByPresence_AssumedSorted( lst_a_folds, lst_b_folds )
		lst_a_inonly_files, lst_b_inonly_files, lst_files_inboth = SplitIntoListsByPresence_AssumedSorted( lst_a_files, lst_b_files )
		if len(lst_a_inonly_folds) + len(lst_b_inonly_folds)  + len(lst_a_inonly_files)  + len(lst_b_inonly_files) > 0 :
			outcome = CompareResult.diff
		if p_failfast and ( outcome == CompareResult.diff ) :
			pass
		else :
			for fldname in lst_folds_inboth :
				#?print( "Not yet Comparing folders of same name: " + fldname)
				lst_inboth_folds_unkn.append( fldname )
				pass
			if p_failfast and ( outcome == CompareResult.diff ) :
				pass
			else :
				for filname in lst_files_inboth :
					#?print( "Comparing files of same name: " + filname)
					a_ref =	im_osp.join( p_path_a, filname )
					b_ref =	im_osp.join( p_path_b, filname )
					f_outcome = CompareUtility_Files( a_ref, b_ref, p_mode )
					if f_outcome == CompareResult.same :
						lst_inboth_files_same.append( filname )
					elif f_outcome == CompareResult.diff :
						lst_inboth_files_diff.append( filname )
						if p_failfast :
							break
					else:
						lst_inboth_files_unkn.append( fldname )
	else:
		#?print( "Had folder collection errors")
		pass
	return outcome, \
		lst_a_inonly_folds, lst_a_inonly_files, lst_b_inonly_folds, lst_b_inonly_files , \
		lst_inboth_folds_same, lst_inboth_folds_diff, lst_inboth_folds_unkn, lst_inboth_folds_bad_a, lst_inboth_folds_bad_b , \
		lst_inboth_files_same, lst_inboth_files_diff, lst_inboth_files_unkn, lst_inboth_files_bad_a, lst_inboth_files_bad_b

# two medium level functions, each returning a boolean based on a supplied mode

def CompareFiles_ByMode( p_filename_a, p_filename_b, p_mode ):
	outcome = CompareUtility_Files( p_filename_a, p_filename_b, p_mode )
	return outcome

def CompareFolders_ByMode( p_path_a, p_path_b, p_mode ):
	# call with p_failfast=True so that only the outcome matters
	print( "CompareFolders_ByMode")
	outcome, \
		lst_a_inonly_folds, lst_a_inonly_files, lst_b_inonly_folds, lst_b_inonly_files , \
		lst_inboth_folds_same, lst_inboth_folds_diff, lst_inboth_folds_unkn, lst_inboth_folds_bad_a, lst_inboth_folds_bad_b , \
		lst_inboth_files_same, lst_inboth_files_diff, lst_inboth_files_unkn, lst_inboth_files_bad_a, lst_inboth_files_bad_b \
		 = CompareUtility_Folders( p_path_a, p_path_b, p_mode, p_failfast=True )
	return outcome

def CompareFolders_ByMode_PrintSummary( p_path_a, p_path_b, p_mode ):
	# call with p_failfast=True so that only the outcome matters
	def string_of_lst( lst):
		s = str(len( lst ))
		if len( lst ) > 0 :
			s = s + " First: " + lst[0]
		return s
	print( "CompareFolders_ByMode_PrintSummary Mode=" + str_CompareMode(p_mode) )
	outcome, \
		lst_a_inonly_folds, lst_a_inonly_files, lst_b_inonly_folds, lst_b_inonly_files , \
		lst_inboth_folds_same, lst_inboth_folds_diff, lst_inboth_folds_unkn, lst_inboth_folds_bad_a, lst_inboth_folds_bad_b , \
		lst_inboth_files_same, lst_inboth_files_diff, lst_inboth_files_unkn, lst_inboth_files_bad_a, lst_inboth_files_bad_b \
		= CompareUtility_Folders( p_path_a, p_path_b, p_mode, p_failfast=False )
	print( "Outcome: " + StrShow_CompareResult( outcome ) )
	print( "lst_a_inonly_folds # " + string_of_lst( lst_a_inonly_folds) )
	print( "lst_a_inonly_files # " + string_of_lst( lst_a_inonly_files) )
	print( "lst_b_inonly_folds # " + string_of_lst( lst_b_inonly_folds) )
	print( "lst_b_inonly_files # " + string_of_lst( lst_b_inonly_files) )
	print( "lst_inboth_folds_same # " + string_of_lst( lst_inboth_folds_same) )
	print( "lst_inboth_folds_diff # " + string_of_lst( lst_inboth_folds_diff) )
	print( "lst_inboth_folds_unkn # " + string_of_lst( lst_inboth_folds_unkn) )
	print( "lst_inboth_folds_bad_a # " + string_of_lst( lst_inboth_folds_bad_a) )
	print( "lst_inboth_folds_bad_b # " + string_of_lst( lst_inboth_folds_bad_b) )
	print( "lst_inboth_files_same # " + string_of_lst( lst_inboth_files_same) )
	print( "lst_inboth_files_diff # " + string_of_lst( lst_inboth_files_diff) )
	print( "lst_inboth_files_unkn # " + string_of_lst( lst_inboth_files_unkn) )
	print( "lst_inboth_files_bad_a # " + string_of_lst( lst_inboth_files_bad_a) )
	print( "lst_inboth_files_bad_b # " + string_of_lst( lst_inboth_files_bad_b) )
	return outcome


# a set of pairs of simple level functions, each returning a boolean based on a named mode

# Quick

def CompareFiles_Quick( p_filename_a, p_filename_b ):
	print( "CompareFiles_Quick")
	i_mode = CompareMode.quick
	outcome = CompareUtility_Files( p_filename_a, p_filename_b, i_mode )
	return outcome

def CompareFolders_Quick( p_path_a, p_path_b ):
	print( "CompareFolders_Quick")
	outcome = CompareFolders_ByMode( p_path_a, p_path_b, CompareMode.quick )
	return outcome

# Pragmatic

def CompareFiles_Pragmatic( p_filename_a, p_filename_b ):
	print( "CompareFiles_Pragmatic")
	i_mode = CompareMode.pragmatic
	outcome = CompareUtility_Files( p_filename_a, p_filename_b, i_mode )
	return outcome

def CompareFolders_Pragmatic( p_path_a, p_path_b ):
	i_mode = CompareMode.pragmatic
	outcome = CompareUtility_Folders( p_path_a, p_path_b, i_mode )
	return outcome

# Pedantic

def CompareFiles_Pedantic( p_filename_a, p_filename_b ):
	print( "CompareFiles_Pedantic")
	i_mode = CompareMode.pedantic
	outcome = CompareUtility_Files( p_filename_a, p_filename_b, i_mode )
	return outcome

def CompareFolders_Pedantic( p_path_a, p_path_b ):
	i_mode = CompareMode.pedantic
	outcome = CompareUtility_Folders( p_path_a, p_path_b, i_mode )
	return outcome

# ----------------------------------------

def test():
	fileref_a = "/home/ger/dev_tests/Downloads_Copy/statements/Statement20150521.pdf"
	fileref_b = "/home/ger/dev_tests/Downloads_Subset/statements/Statement20150521.pdf"
	#
	print( "test 1 begin CompareFiles_Quick")
	outcome = CompareFiles_Quick( fileref_a, fileref_b )
	print( StrShow_CompareResult( outcome ) )
	print( "test 1 ended")
	#
	print( "test 2 begin CompareFiles_Pragmatic")
	outcome = CompareFiles_Pragmatic( fileref_a, fileref_b )
	print( StrShow_CompareResult( outcome ) )
	print( "test 2 ended")
	#
	print( "test 3 begin CompareFiles_Pedantic")
	outcome = CompareFiles_Pedantic( fileref_a, fileref_b )
	print( StrShow_CompareResult( outcome ) )
	print( "test 3 ended")
	#
	foldref_a = "/home/ger/dev_tests/Downloads_Copy"
	foldref_b = "/home/ger/dev_tests/Downloads_Subset"
	#
	print( "test 4 begin CompareFolders_Quick")
	outcome = CompareFolders_Quick( foldref_a, foldref_b )
	print( StrShow_CompareResult( outcome ) )
	print( "test 4 ended")
	#
	for i_mode in CompareMode :
		print( "test begin CompareFolders_ByMode_PrintSummary")
		CompareFolders_ByMode_PrintSummary( foldref_a, foldref_b, i_mode )
		print( "test ended")
	# 
	fileref_a = "/home/sharelocal/Video/Swag/Tech/Visualisation/MandelbrotFractalZoom/The Hardest Zoom - Mandelbrot Fractal Zoom.mp4"
	fileref_b = "/home/sharelocal/Video/Swag/Tech/Visualisation/MandelbrotFractalZoom/The Hardest Zoom - Mandelbrot Fractal Zoom.mp4"
	#
	print_memory_available_and_free()
	print( "test 11 begin large file CompareFiles_Pedantic")
	outcome = CompareFiles_Pedantic( fileref_a, fileref_b )
	print( StrShow_CompareResult( outcome ) )
	print( "test 11 ended")
	print_memory_available_and_free()

# main only if run as a program
if __name__ == "__main__":
	print( "This is the module: comparatry" )
	test()
