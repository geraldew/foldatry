#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# commontry = common calls fur use by the try/tree family of programs
#
# --------------------------------------------------
# Copyright (C) 2019-2024  Gerald Waters - Melbourne, Australia
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# --------------------------------------------------
# This is a set of things to be used after being imported into the other python programs
# that are part of the foldatry application.
# It has no purpose in being run on its own.
#
# As a module, it will hold a number of features that are likely to be
# used in the various modules. They may be generic, such as might be used
# in any Python program or they might be specific to Foldatry but not yet
# placed into a more relevant module. If a section seems to be
# worth moving into a module of its own then that will be done.
#
# For example, this has already happened and code then become the multilog module.
# Currently, sets of enumerations are being relocated and adapted to be in
# the applitry modules.
# --------------------------------------------------

# --------------------------------------------------
# Imports - Standard
# --------------------------------------------------

import sys as im_sys
import os as im_os
import platform as im_pltfm
import time as im_time
import shutil as im_shutil
#import datetime as im_datetime
import re
import logging as im_lggng
import math as im_math

from enum import Enum, unique, auto
from subprocess import Popen, PIPE

from collections import namedtuple

# from dateutil.relativedelta import relativedelta as fi_rd

# --------------------------------------------------
# Imports - Custom
# --------------------------------------------------

# customised logging module
import multilog

# --------------------------------------------------
# Global scope objects
# --------------------------------------------------

# This needs to be here, not for time priority but for global scope
thismodule_log = im_lggng.getLogger(__name__)

# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# Module scope constant functions 
# --------------------------------------------------

def app_file_prefix():
	return "fldtry"

# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# Enumerations 
# --------------------------------------------------

# ==================================================
# Cross Module Enumerations
# but not those used for setting selections
# --------------------------------------------------

# ==================================================
# Proposed Enumerations
# not yet in use
# --------------------------------------------------

# --------------------------------------------------
# Enumeration for choice of Conformity functions in Summatry
# --------------------------------------------------

@unique
class FnCodeConform(Enum):
	Same = auto() # leave value as is
	Atan = auto() # 
	Exp = auto() # Decay
	Lin = auto() # Brutal

def FnCodeConform_GetList() :
	return list(map(lambda c: c.value, FnCodeConform))

def FnCodeConform_GetTuple() :
	return tuple( FnCodeConform_GetList() )

def FnCodeConform_Default() :
	return FnCodeConform.Atan

def FnCodeConform_EnumOfString( s ) :
	m = FnCodeConform_Default()
	for member in FnCodeConform :
		if member.value == s :
			m = member
			break
	return m

def FnCodeConform_EnumOfName( s ) :
	# Usage: eok, enm = FnCodeConform_EnumOfName( s ) 
	eok = False
	for member in FnCodeConform :
		if member.name == s :
			enm = member
			eok = True
			break
	return eok, enm

def FnCodeConform_EnumValueString( e ) :
	try:
		s = e.value
	except:
		s = ""
	return s

def FnCodeConform_Default_Value() :
	return FnCodeConform.Atan.value

# --------------------------------------------------
# Enumeration for choice of Combination functions in Summatry
# --------------------------------------------------

@unique
class FnCodeCombin(Enum):
	MPFA = auto() # MaxPlusFactoredAverage 
	Min = auto() 
	Max = auto() 
	Avg = auto() 
	Geo = auto() 
	RMS = auto() 

def FnCodeCombin_GetList() :
	return list(map(lambda c: c.value, FnCodeCombin))

def FnCodeCombin_GetTuple() :
	return tuple( FnCodeCombin_GetList() )

def FnCodeCombin_Default() :
	return FnCodeCombin.MPFA

def FnCodeCombin_EnumOfString( s ) :
	m = FnCodeCombin_Default()
	for member in FnCodeCombin :
		if member.value == s :
			m = member
			break
	return m

def FnCodeCombin_EnumOfName( s ) :
	# Usage: eok, enm = FnCodeCombin_EnumOfName( s ) 
	eok = False
	for member in FnCodeCombin :
		if member.name == s :
			enm = member
			eok = True
			break
	return eok, enm

def FnCodeCombin_EnumValueString( e ) :
	try:
		s = e.value
	except:
		s = ""
	return s

def FnCodeCombin_Default_Value() :
	return FnCodeCombin.MPFA.value


# ==================================================
# Enumeration of the Settings
# while these are only used in the Foldatry module, they often relate to the other enumerations
# and so it seems fit to have them here
# --------------------------------------------------

# --------------------------------------------------

# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# Named Tuples
# --------------------------------------------------

# this is the set of four boolean flags that are used across the GUI, filedupetry and deforestry
tpl_FileFlags = namedtuple('tpl_FileFlags', 'ff_Name ff_Size ff_When ff_Hash')

# this is a matchkey tuple, in this case, the "short" one, using only the three metainfo fields of file: Name, Size, When
tpl_ShortMatch = namedtuple('tpl_ShortMatch', 'sm_Name sm_Size sm_When')
# values of this are used as the key in x_dct_shortmatchkey_lst_pfn dictionary variables

# this is a matchkey tuple, in this case, the "full" one, adding the calculated Hash for the file content
tpl_FullMatch = namedtuple('tpl_FullMatch', 'fm_Name fm_Size fm_When fm_Hash')
# values of this are used as the key in x_dct_fullmatchkey_lst_pfn dictionary variables

# --------------------------------------------------
# custom functions for the Named Tuples
# --------------------------------------------------

def GetStringToShow_tpl_FileFlags( p_tpl_FileFlags):
	r_s = hndy_str_of_boolean( p_tpl_FileFlags.ff_Name, "Name" )
	r_s += " " + hndy_str_of_boolean( p_tpl_FileFlags.ff_Size, "Size" )
	r_s += " " + hndy_str_of_boolean( p_tpl_FileFlags.ff_When, "When" )
	r_s += " " + hndy_str_of_boolean( p_tpl_FileFlags.ff_Hash, "Hash" )
	return r_s

# ==================================================
# Operating System Detection and Control
# --------------------------------------------------

# --------------------------------------------------
# Enumeration for choice of Combination functions in Summatry
# --------------------------------------------------

@unique
class EnumOS(Enum):
	OsLinux = auto()
	OsWindows = auto()
	OsUnknown = auto()


def GetWhichOS():
	i_os_name = im_os.name
	# returns name as a string, such as posix
	i_pltfm_system = im_pltfm.system()
	# returns name as a string, such as 'Linux', 'Darwin', 'Java', 'Windows'.
	if i_pltfm_system == "Linux" :
		r_EnumOS = EnumOS.OsLinux
	elif i_pltfm_system == "Windows" :
		r_EnumOS = EnumOS.OsWindows
	else :
		r_EnumOS = EnumOS.OsUnknown
	return r_EnumOS, i_os_name + " " + i_pltfm_system

# ==================================================
# Other Definitions
# --------------------------------------------------

# --------------------------------------------------
# timestamp string
# --------------------------------------------------

def tempus_timestamp_utc( p_prec=0 ):
    t = im_time.time()
    s = im_time.strftime("%Y_%m_%d_%H_%M_%S", im_time.gmtime(t))
    if p_prec > 0:
        s += "_" + ("%.9f" % (t % 1,))[2:2+p_prec] # 2:2 because we're skipping the 0. as positions 0 and 1
    return s

def tempus_timestamp_local( p_prec=0 ):
    t = im_time.time()
    s = im_time.strftime("%Y_%m_%d_%H_%M_%S", im_time.localtime(t))
    if p_prec > 0:
        s += "_" + ("%.9f" % (t % 1,))[2:2+p_prec]
    return s

# --------------------------------------------------
# bfff = building folder file functions
# these are generic calls adapted from those originally written for the lgff set
# but now to be used for:
# - cfgf - config folders and files
# - lgff = log folders and files
# - dtff - data folders and files
# - scrf - script folders and files
# --------------------------------------------------

def bfff_get_home_folder():
	return im_os.path.expanduser('~')

def bfff_make_folder( p_path_foldname ):
	try:
		im_os.mkdir( p_path_foldname )
		r_did_ok = True
		r_bad_msg = ""
	except im_shutil.SameFileError:
		# If source and destination are same
		r_did_ok = False
		r_bad_msg = "Source and destination represents the same file"
	except PermissionError:
		# If there is any permission issue
		r_did_ok = False
		r_bad_msg = "Permission denied"
	except:
		# For other errors
		r_did_ok = False
		r_bad_msg = "Unknown error" 
	return r_did_ok, r_bad_msg

def bfff_make_home_sub_folder_as_per_name( p_folder_name ):
	i_home_path_str = bfff_get_home_folder()
	i_log_path = im_os.path.join( i_home_path_str, p_folder_name)
	if im_os.path.exists( i_log_path ) :
		r_did_ok = True
	else:
		r_did_ok, i_bad_msg = bfff_make_folder( i_log_path )
	return r_did_ok, i_log_path

def bfff_make_home_sub_folder_as_per_list_of_preferences( p_lst_subnames ):
	# note: there's no return value for this - that's because the function 
	# that tries to find a suitable log folder is independent of whether this function worked or not
	r_did_ok = False
	for i_fldr in p_lst_subnames :
		r_did_ok, r_log_path = bfff_make_home_sub_folder_as_per_name( i_fldr )
		if r_did_ok :
			break

def bfff_get_home_sub_folder_as_per_list_of_preferences( p_lst_subnames ):
	for i_fldr in p_lst_subnames :
		i_home_path_str = bfff_get_home_folder()
		i_log_path = im_os.path.join( i_home_path_str, i_fldr)
		if im_os.path.exists( i_log_path ):
			return i_log_path
	# if we didn't find any such sub-folder, return the home folder
	return bfff_get_home_folder()

def bfff_set_file_at_home( log_filename ):
	i_log_path = bfff_get_home_log_folder()
	lfn_at_home = im_os.path.join( i_log_path, log_filename)
	return lfn_at_home

# for use in making a date and time based filename
def bfff_datetime_ident_str():
	return tempus_timestamp_utc( 4)

#was make_log_results_filename
def bfff_make_filename_from_prefix_unique_infix_suffix_and_extn( p_prefix, p_unique, p_infix, p_suffix, p_extn ):
	strings = [p_prefix, p_unique, p_infix, p_suffix]
	# join with underscores but ignore any empty strings
	s = '_'.join([x for x in strings if x != '']) + "." + p_extn
	return s

# for use in making a date and time based filename
def bfff_datetime_filename_str():
	return bfff_datetime_ident_str()

def bfff_make_filename_from_prefix_unique_suffix_and_extn( p_prefix, p_unique, p_suffix, p_extn ):
	strings = [p_prefix, p_unique, p_suffix]
	# join with underscores but ignore any empty strings
	s = '_'.join([x for x in strings if x != '']) + "." + p_extn
	return s

# --------------------------------------------------
# cfgf = config folder file - Section for config folder and file naming
# --------------------------------------------------

def cfgf_home_config_folders():
	return [ ".config", "config" ]

def cfgf_home_config_foldatry_subfolder():
	return "foldatry"

def cfgf_make_home_config_folder():
	bfff_make_home_sub_folder_as_per_list_of_preferences( cfgf_home_config_folders() )
	i_home_config = bfff_get_home_sub_folder_as_per_list_of_preferences( cfgf_home_config_folders() )
	i_config_subfolder = im_os.path.join( i_home_config, cfgf_home_config_foldatry_subfolder())
	i_did_ok, i_bad_msg = bfff_make_folder( i_config_subfolder )
	# this has no return, it just tries to make a suitable folder, we'll detect later whether it did or not

def cfgf_get_home_config_folder():
	i_home_config = bfff_get_home_sub_folder_as_per_list_of_preferences( cfgf_home_config_folders() )
	i_config_subfolder = im_os.path.join( i_home_config, cfgf_home_config_foldatry_subfolder())
	if im_os.path.exists( i_config_subfolder ) :
		return i_config_subfolder
	else :
		return i_home_config

def cfgf_default_config_filename():
	return "foldatry_default_config.json"

def cfgf_autosave_config_filename():
	return "foldatry_autosave_config.json"

def cfgf_get_generic_config_fileref( p_filename ):
	i_config_folder = cfgf_get_home_config_folder()
	i_config_fileref = im_os.path.join( i_config_folder, p_filename )
	if im_os.path.exists( i_config_fileref ) :
		return i_config_fileref
	else :
		return None

def cfgf_set_generic_config_fileref( p_filename ):
	# try to make a config location
	cfgf_make_home_config_folder()
	i_config_folder = cfgf_get_home_config_folder()
	# make what the fileref would be for that location
	r_config_fileref = im_os.path.join( i_config_folder, p_filename )
	return r_config_fileref

def cfgf_get_default_config_fileref():
	return cfgf_get_generic_config_fileref( cfgf_default_config_filename() )

def cfgf_set_default_config_fileref():
	return cfgf_set_generic_config_fileref( cfgf_default_config_filename() )

def cfgf_get_autosave_config_fileref():
	return cfgf_get_generic_config_fileref( cfgf_autosave_config_filename() )

def cfgf_set_autosave_config_fileref():
	return cfgf_set_generic_config_fileref( cfgf_autosave_config_filename() )

# --------------------------------------------------
# lgff = log file folder - Section for log file and folder naming
# --------------------------------------------------

def lgff_get_home_folder():
	return bfff_get_home_folder()

def lgff_make_folder( p_path_foldname ):
	return bfff_make_folder( p_path_foldname )

def lgff_log_file_extn():
	return "log"

def lgff_home_log_lst_folders():
	return [ ".logs", ".log", "logs", "log" ]

def lgff_make_home_log_folder():
	bfff_make_home_sub_folder_as_per_list_of_preferences( lgff_home_log_lst_folders() )

def lgff_get_home_log_folder():
	return bfff_get_home_sub_folder_as_per_list_of_preferences( lgff_home_log_lst_folders() )

def lgff_set_log_file_at_home( log_filename ):
	i_log_path = lgff_get_home_log_folder()
	lfn_at_home = im_os.path.join( i_log_path, log_filename)
	return lfn_at_home

# handy derivations for three standard redirections to file
#   r = Results
#   d = Details
#   b = deBug
# thus there are three stock filename constructions that can be used

#was make_log_results_filename
def lgff_make_log_filename_logtype( p_prefix, p_unique, p_infix, p_logtype ):
	return bfff_make_filename_from_prefix_unique_infix_suffix_and_extn( p_prefix, p_unique, p_infix, p_logtype, lgff_log_file_extn() )

# !! note, we are changing p_extn from presuming to have "." to not having it
def lgff_make_filename( p_prefix, p_unique, p_suffix, p_extn ):
	return bfff_make_filename_from_prefix_unique_suffix_and_extn( p_prefix, p_unique, p_suffix, p_extn )
	return s

# --------------------------------------------------
# dtff = data folder file - Section for data folder and file naming
# --------------------------------------------------

def dtff_file_extn():
	return "tsv" # "txt"

def dtff_make_data_filename( p_unique, p_suffix ):
	return bfff_make_filename_from_prefix_unique_suffix_and_extn( app_file_prefix(), p_unique, p_suffix, dtff_file_extn() )

# --------------------------------------------------
# scrf = script folder file - Section for script folder and file naming
# --------------------------------------------------

def scrf_file_extn():
	return "bash"

def scrf_make_bash_filename( p_unique, p_suffix ):
	return bfff_make_filename_from_prefix_unique_suffix_and_extn( app_file_prefix(), p_unique, p_suffix, scrf_file_extn() )

# --------------------------------------------------
# Prep for and setup of the logging objects - see multilog.py
# --------------------------------------------------

# --------------------------------------------------
# First the generic versions, which can be passed the app_name string

def make_multilog_filenames_appsubapp( p_app_name, p_subapp_name ) :
	# setup the filenames
	# the sequence is:
	# app_name _ datetime _ subapp_name _ logpart .log
	# set this once so that all the different log levels will match
	i_use_datetime_str = bfff_datetime_ident_str() 
	i_log_debug_fn_str = lgff_set_log_file_at_home( lgff_make_log_filename_logtype( p_app_name, i_use_datetime_str, p_subapp_name, "debug" ))
	i_log_details_fn_str = lgff_set_log_file_at_home( lgff_make_log_filename_logtype( p_app_name, i_use_datetime_str, p_subapp_name, "detail" ))
	i_log_results_fn_str = lgff_set_log_file_at_home( lgff_make_log_filename_logtype( p_app_name, i_use_datetime_str, p_subapp_name, "result" ))
	return i_log_debug_fn_str, i_log_details_fn_str, i_log_results_fn_str

def make_multilog_object_appsubapp( p_app_name, p_subapp_name ) :
	# This uses the custom wrapper module "multilog"
	#
	# Logging codes being used:
	# - b = deBug = under the hood stuff,used for debgging when things really don't work as they should
	# - d = Details = all the details a user might want/need to check 
	# - p = Process = the main stippings and startings - so as to see if process X did complete etc
	# - r = Results = mainly saying where result files were written
	# - s = Status = a window of the X most recent indicator of where the program was doing things
	# - t = Terminal = goes to "standard output", e.g. for seeing new feature info during the writing of it
	#
	# Calls to write the to logs take the form of:
	#   p_multi_log.do_logs( "x", string  )
	# where "x" is the mix of codes appropriate for that part of the code 
	# - e.g. "dps" for something to show up in all of the Details, Process and Status
	#
	# Calls, to the logging that only involve "b" should be inside checks for the debug flag on Python execution
	# if __debug__:
	#
	# Notes:
	# - actual Results outputs are not done via the Logging system
	# - original s and t were just s = Screen and this is in progress for conversion throughout the code
	#
	# setup the filenames for the file logs
	i_log_debug_fn_str, i_log_details_fn_str, i_log_results_fn_str = make_multilog_filenames_appsubapp( p_app_name, p_subapp_name )
	# setup log objects
	#   do we start with all off or all on ? Judge this by the Python debug flag
	if __debug__:
		i_log_default = True
	else :
		i_log_default = False
	# create the logging object
	r_mlog = multilog.MultiLog()
	# create logging keys to go to files for deBug, Details and Results
	r_mlog.addkey_k_b_fileref( "r", i_log_default, i_log_results_fn_str )
	r_mlog.addkey_k_b_fileref( "d", i_log_default, i_log_details_fn_str )
	r_mlog.addkey_k_b_fileref( "b", i_log_default, i_log_debug_fn_str )
	# create a logging key to go to terminal
	r_mlog.addkey_k_b_terminal( "t", i_log_default, )
	# see elsewhere for the addition of handlers going to GUI controls
	# i.e. use of calls to .addkey_k_b_handler with strings of "p" and "s"
	return r_mlog

def re_filename_multilog_object_appsubapp( v_mlg, p_app_name, p_subapp_name ) :
	# get fresh filenames
	i_log_debug_fn_str, i_log_details_fn_str, i_log_results_fn_str = make_multilog_filenames_appsubapp( p_app_name, p_subapp_name )
	v_mlg.key_set_f( "b", i_log_debug_fn_str )
	v_mlg.key_set_f( "d", i_log_details_fn_str )
	v_mlg.key_set_f( "r", i_log_results_fn_str )

# --------------------------------------------------
# Now the the versions that use the app_file_prefix() string

def make_multilog_filenames( p_subapp_name ) :
	return make_multilog_filenames_appsubapp( app_file_prefix(), p_subapp_name ) 

def make_multilog_object( p_subapp_name ) :
	return make_multilog_object_appsubapp( app_file_prefix(), p_subapp_name )

def re_filename_multilog_object( v_mlg, p_subapp_name ) :
	re_filename_multilog_object_appsubapp( v_mlg, app_file_prefix(), p_subapp_name )

# --------------------------------------------------
# String encoding decoding functions 
# --------------------------------------------------

def python_ucs_mode():
	r_m_u = im_sys.maxunicode
	if r_m_u == 65535 :
		r_ucs_mode = "UCS-2"
	elif r_m_u == 1114111 :
		r_ucs_mode = "UCS-4"
	else :
		r_ucs_mode = "Unknown"
	return r_m_u, r_ucs_mode

def python_pfn_as_byte_sequence( p_pfn ):
	# undo the default holding by Python of the byte sequence read from the operating system and file system 
	# the rationale here is:
	# - the Python os module will have by-default put the bytes it got from the file system into a Python Unicode string
	# - we then want to turn that back into the original byte sequence, which is achieved by encode('utf-8', 'surrogateescape')
	# - this works because the doco for the os module says that's what was done to make the Python Unicode string
	# - a deeper technical reason this works is because the constructed Unicode includes "surrogate" encoding (look it up)
	return p_pfn.encode('utf-8', 'surrogateescape')

def pfn4bash( p_pfn ):
	# a single place to define how to write any possible path and file name into a line of bash script
	bs = python_pfn_as_byte_sequence( p_pfn )
	s = ""
	need_quote = False
	for b in bs:
		if b != 39 and b >= 32 and b < 127 :
			s += chr(b) #.encode( cp1252)
			if ( b == 32 ):
				need_quote = True
		else:
			s += r"'" + r"$'\x" + hex(b).lstrip("0x").rstrip("L") + r"'" + r"'"
			need_quote = True
		#print( s)
	if need_quote:
		s = r"'" + s + r"'"
	#print( s)
	return s

def pfn4print( p_pfn ):
	# a single place to define how to display any possible path and file name
	# - for display we're choosing to use the Windows-1252 codec for two reasons:
	# - - it was the most common for a longer time: than the earlier MS-DOS sets and than other language sets
	# - - because it has representations for nearly all the 246 characters of the 8-bit mapping  
	# also see https://stackoverflow.com/questions/27366479/python-3-os-walk-file-paths-unicodeencodeerror-utf-8-codec-cant-encode-s
	return p_pfn.encode('utf-8', 'surrogateescape').decode('cp1252', 'backslashreplace') # the Windows-1252 superset of ISO-8859-1

def pfn4print_hexes( p_pfn ):
	bs = p_pfn.encode('utf-8', 'surrogateescape')
	s = " ".join( format(x, "02x") for x in bs )
	return s

def lst_pfn4print_of_lst_pfn( p_lst_pfn ):
	r_lst = []
	for i_pfn in p_lst_pfn:
		r_lst.append( pfn4print( i_pfn ) )
	return r_lst

# ==================================================
# Windows and DOS File/Folder Naming Substituions
#

# --------------------------------------------------
# Reserved Names
# --------------------------------------------------
# Reference of reserved file and folder names for DOS and Windows
# see https://docs.microsoft.com/en-us/windows/win32/fileio/naming-a-file
# --------------------------------------------------

def BadWinFileFoldName_Lst():
	# list of reserved file and folder names for DOS and Windows
	return ["CON", "PRN", "AUX", "NUL", "COM1", "COM2", "COM3", "COM4", "COM5", "COM6", "COM7", "COM8", "COM9", "LPT1", "LPT2", "LPT3", "LPT4", "LPT5", "LPT6", "LPT7", "LPT8", "LPT9"]

def BadWinFileFoldName_Replacements_Dct():
	# a dictionary of replacements for the DOS and Windows reserved file and folder names
	return {k: k + "_!!" for k in BadWinFileFoldName_Lst() }

_dct_BadWinFileFoldName_Replacements = BadWinFileFoldName_Replacements_Dct()

def BadWinFileFoldName_MaybeRenamed( p_fname ):
	if p_fname in BadWinFileFoldName_Lst():
		return _dct_BadWinFileFoldName_Replacements[ p_fname.upper() ]
	else:
		return None

# --------------------------------------------------
# Unallowed Characters in Names
# --------------------------------------------------

# Windows does not allow the following reserved characters:
#    < (less than)
#    > (greater than)
#    : (colon)
#    " (double quote)
#    / (forward slash)
#    \ (backslash)
#    | (vertical bar or pipe)
#    ? (question mark)
#    * (asterisk)
# Byte value zero, sometimes referred to as the ASCII NUL character.
# Byte values in the range from 1 through 31

def StringOfBadWinChars() :
	return "<>:" + '"' + "/" + "\\" + "|?*"

def FNameHasBadWinChars( p_fname) :
	# note use of r prefix to avoid the backslash being  
	badchars = StringOfBadWinChars()
	is_bad = False
	for c in badchars :
		if c in p_fname :
			is_bad = True
			break
	return is_bad

def Fname_BadWinChars_Removed( p_fname ):
	r_fname = p_fname
	badchars = StringOfBadWinChars()
	for c in badchars :
		r_fname.replace( r_fname, c, "_")
	return r_fname

def make_list_of_nonMSchar_to_code():
	r_lst = list( StringOfBadWinChars() )
	return r_lst

def make_dict_of_nonMSchar_encodings( p_lst_of_nonMSchar_to_code ):
	r_dct = {}
	for i_char in p_lst_of_nonMSchar_to_code:
		i_bytes = i_char.encode('utf-8', 'surrogateescape')
		i_byte_strings = [ hex(b).lstrip("0x").rstrip("L").upper() for b in i_bytes ]
		i_code = "".join( i_byte_strings )
		r_dct[ i_char ] = "%" + i_code
	return r_dct

def make_dict_of_nonMSchar_decodings( p_dct_of_encodings ):
	r_dct = {}
	for i_key in p_dct_of_encodings.keys():
		r_dct[ p_dct_of_encodings[ i_key ] ] = i_key
	return r_dct

_list_of_nonMSchar_to_code = make_list_of_nonMSchar_to_code()
_dict_of_encodings = make_dict_of_nonMSchar_encodings( _list_of_nonMSchar_to_code )
_dict_of_decodings = make_dict_of_nonMSchar_decodings( _dict_of_encodings )

def do_nonMSchar_encoding( p_str ):
	r_str = p_str
	for i_c in _list_of_nonMSchar_to_code:
		r_str = r_str.replace( i_c, _dict_of_encodings[ i_c ])
	return r_str

def do_nonMSchar_decoding( p_str ):
	r_str = p_str
	for i_cdng in _dict_of_decodings.keys() :
		r_str = r_str.replace( i_cdng, _dict_of_decodings[ i_cdng ])
	return r_str

# --------------------------------------------------
# Translation of Long Names into 8.3
# --------------------------------------------------

"""
In Windows 2000, both FAT and NTFS use the Unicode character set for their names, which contain several forbidden characters that MS-DOS cannot read. 
To generate a short MS-DOS-readable file name, Windows 2000 deletes all of these characters from the LFN and removes any spaces. 
Because an MS-DOS-readable file name can have only one period, Windows 2000 also removes all extra periods from the file name. 
Next, Windows 2000 truncates the file name, if necessary, to six characters and appends a tilde ( ~ ) and a number. 
For example, each non-duplicate file name is appended with ~1 . Duplicate file names end with ~2 , then ~3, and so on. 
After the file names are truncated, the file name extensions are truncated to three or fewer characters. 
Finally, when displaying file names at the command line, Windows 2000 translates all characters in the file name and extension to uppercase.

- remove forbidden characters
- remove spaces
- remove extra period characters - presumably leaving the last one
- truncates to six characters
- append a tilde
- append a number
- truncate the extension to three characters
- (store as mixed case but treat as uppercase)
"""

def LongWinFileFoldName_MaybeRenamed( p_fname ):
	# print( "Function is: " + "LongWinFileFoldName_MaybeRenamed" )
	# print( "received p_fname: " + p_fname )
	r_fname = p_fname
	i_must_do = False
	# remove forbidden characters
	if FNameHasBadWinChars( r_fname) :
		print( "Had bad chars")
		b_s = StringOfBadWinChars()
		for c in b_s :
			r_fname = r_fname.replace( c, "")
		i_must_do = True
	# remove spaces
	if " " in r_fname :
		# print( "Had space")
		r_fname = r_fname.replace( " ", "")
		i_must_do = True
	# remove extra period characters - presumably leaving the last one
	had_dot = ( r_fname.count('.') > 0 )
	if had_dot :
		# print( "Had Dot")
		lst_r_fname_parts = r_fname.rsplit( '.', 1)
		i_ext = lst_r_fname_parts[ 1]
		if len( i_ext ) > 3 :
			i_ext = i_ext[0:3]
		r_fname = lst_r_fname_parts[ 0]
		if "." in r_fname :
			r_fname = r_fname.replace( ".", "")
			i_must_do = True
	# truncate to six characters
	if i_must_do or len( r_fname ) > 8 :
		r_fname = r_fname[0:6]
		r_fname += "~"
		r_fname += "1"
	# now reassemble with extension
	if had_dot :
		r_fname = ".".join( [ r_fname, i_ext ] )
	return r_fname

# --------------------------------------------------
# operating and file system functions 
# --------------------------------------------------

def make_os_script_file_copy_bash( p_src, p_tgt ):
	s = "cp" + " " + pfn4bash( p_src ) + " " + pfn4bash( p_tgt )
	return s

def make_os_script_file_copy( p_src, p_tgt ):
	s = make_os_script_file_copy_bash( p_src, p_tgt )
	return s

def make_os_script_file_copy_fromcoding_intocoding( p_src_fold, p_src_name, p_tgt_fold, p_src_enc_str, p_tgt_enc_str ):
	#print("make_os_script_file_copy_fromcoding_intocoding")
	if p_src_enc_str is None or p_src_enc_str == "" :
		i_src_enc_str = "utf_8"
	else:
		i_src_enc_str = p_src_enc_str
	if p_tgt_enc_str is None or p_tgt_enc_str == "" :
		i_tgt_enc_str = "utf_8"
	else:
		i_tgt_enc_str = p_tgt_enc_str
	#print( "p_src_fold: " + p_src_fold )
	#print( "p_src_name: " + "CANNOT BE RELIABLY PRINTED because of surrogates in the Unicode form" )
	#print( "p_tgt_fold: " + p_tgt_fold )
	#print( "i_src_enc_str: " + i_src_enc_str )
	#print( "i_tgt_enc_str: " + i_tgt_enc_str )
	i_u_tgt_name = p_src_name.encode('utf-8', 'surrogateescape').decode( i_src_enc_str, 'backslashreplace')
	#print( "i_u_tgt_name: " + i_u_tgt_name )
	i_src_ref = im_os.path.join( p_src_fold, p_src_name )
	i_tgt_ref = im_os.path.join( p_tgt_fold, i_u_tgt_name )
	#print( "i_src_ref: " + "CANNOT BE RELIABLY PRINTED because of surrogates in the Unicode form" )
	#print( "i_tgt_ref: " + i_tgt_ref )
	s = make_os_script_file_copy_bash( i_src_ref, i_tgt_ref )
	return s

def make_os_script_file_copy_bad_windows_name( p_src_folder, p_src_fnam, p_tgt_folder ):
	print( p_src_folder )
	print( p_src_fnam )
	print( p_src_fnam.upper() )
	print( p_tgt_folder )
	lst = BadWinFileFoldName_Lst()
	print( lst )
	i_src_fnam = p_src_fnam.upper()
	if i_src_fnam in lst :
		print( "in list" )
		print( BadWinFileFoldName_Replacements_Dct()[ i_src_fnam ] )
		p_src = im_os.path.join( p_src_folder, p_src_fnam )
		p_tgt = im_os.path.join( p_tgt_folder, BadWinFileFoldName_Replacements_Dct()[ i_src_fnam ] )
		s = make_os_script_file_copy_bash( p_src, p_tgt )
	else:
		print( "not in list" )
		s = None
	return s

def consider_path_str( p_str_path, p_str_label ):
	r_is_ok = True
	r_lst_why = [] # this will be a list of strings
	if not isinstance(p_str_path, str) :
		r_is_ok = False
		r_lst_why.append( p_str_label + " path is not a String! " + pfn4print( p_str_path) )
	if r_is_ok :
		if len( p_str_path ) == 0 :
			r_is_ok = False
			r_lst_why.append( p_str_label + " path is empty! " + pfn4print( p_str_path) )
	if r_is_ok :
		if not im_os.path.exists( p_str_path ) :
			r_is_ok = False
			r_lst_why.append( p_str_label + " path is not valid! " + pfn4print( p_str_path) )
	if r_is_ok :
		if not im_os.path.isdir( p_str_path ) :
			r_is_ok = False
			r_lst_why.append( p_str_label + " path is not a folder! " + pfn4print( p_str_path) )
	return r_is_ok, r_lst_why

def consider_list_of_folders( p_lst_folders, p_empty_List_is_ok ) :
	r_is_ok = True
	r_lst_why = [] # this will be a list of strings
	if isinstance( p_lst_folders, list) :
		if len( p_lst_folders ) > 0 :
			for fstr in p_lst_folders :
				if isinstance( p_lst_folders, string) :
					a_is_ok, a_lst_why = consider_path_str( p_str_path_A, "A" )
					if not a_is_ok :
						r_is_ok = False
						r_lst_why.extend( a_lst_why )
				else :
					r_lst_why.append( "Was passed a non-string in the list")
					r_is_ok = False
		elif not p_empty_List_is_ok :
			r_is_ok = False
			r_lst_why.append( "Was passed an empty list")
	else :
		r_is_ok = False
		r_lst_why.append( "Not passed a list")
	return r_is_ok, r_lst_why

# --------------------------------------------------
# Text File writing
# --------------------------------------------------

def file_in_home_text_write_line( p_filename, p_mode, p_line_str ):
	# this just to isolate the home location logic to one function
	# perhaps later have it not always add a trailing line break
	# i.e. check for empty or not ending with a line break 
	lfn = im_os.path.join( im_os.path.expanduser('~'), p_filename)
	if not p_mode.lower() in ["a", "w"] :
		p_mode = "a"
	with open( lfn, p_mode) as text_file:
		text_file.write( p_line_str + "\n")

def file_in_home_text_new_list_as_data_head( p_filename, p_lst_fields ):
	file_in_home_text_write_line( p_filename, "w", "\t".join( p_lst_fields ) )

def file_in_home_text_add_list_as_data_line( p_filename, p_lst_fields ):
	file_in_home_text_write_line( p_filename, "a", "\t".join( p_lst_fields ) )

# add to bash script
def bash_script_file_add_line( bash_filename, bash_line_str ):
	# later add some checks about what makes a valid bash line or lines
	file_in_home_text_write_line( bash_filename, "a", bash_line_str )

# --------------------------------------------------
# hndy - Other handy functions
# --------------------------------------------------

def hndy_off_sided(old_side, new_side):
	if (old_side == new_side):
		return False
	elif (old_side == "=") and (new_side == "L") :
		return False
	elif (old_side == "=") and (new_side == "R") :
		return False
	elif (old_side == "L") and (new_side == "=") :
		return False
	elif (old_side == "R") and (new_side == "=") :
		return False
	elif (old_side == "L") and (new_side == "R") :
		return True
	elif (old_side == "R") and (new_side == "L") :
		return True
	else:
		return True

def hndy_GetHumanReadable_Seconds( p_seconds ):
	# time_intervals = ['days','hours','minutes','seconds']
	# x = fi_rd( seconds = p_seconds )
	# return ' '.join('{} {}'.format( getattr( x, k), k ) for k in time_intervals if getattr(x , k ) )
	i_seconds = int( p_seconds )
	r_seconds = i_seconds % 60
	i_minutes = int( i_seconds / 60 )
	r_minutes = i_minutes % 60
	i_hours = int( i_minutes / 60 )
	r_hours = i_hours % 24
	i_days = int( i_hours / 24 )
	r_days = i_days % 7
	i_weeks = int( i_days / 7 )
	r_str = ""
	if i_weeks == 1 :
		r_str += str( i_weeks ) + " week "
	elif i_weeks > 0 :
		r_str += str( i_weeks ) + " weeks "
	if r_days == 1 :
		r_str += str( r_days ) + " day "
	elif r_days > 0 :
		r_str += str( r_days ) + " days "
	if r_hours == 1 :
		r_str += str( r_hours ) + " hour "
	elif r_hours > 0 :
		r_str += str( r_hours ) + " hours "
	if r_minutes == 1 :
		r_str += str( r_minutes ) + " minute "
	elif r_minutes > 0 :
		r_str += str( r_minutes ) + " minutes "
	if r_seconds == 1 :
		r_str += str( r_seconds ) + " second"
	elif r_seconds > 0 :
		r_str += str( r_seconds ) + " seconds"
	return r_str

def Test_hndy_GetHumanReadable_Seconds():
	for i in range( 0, 7200):
		print( hndy_GetHumanReadable_Seconds( i ) )

def hndy_GetHumanReadable_Count( p_number ):
	return '{:n}'.format( p_number )

def hndy_GetHumanReadable_Bytes( p_size, p_precision=2 ):
	# 1024	Ki	kibi
	# 10242	Mi	mebi
	# 10243	Gi	gibi
	# 10244	Ti	tebi
	# 10245	Pi	pebi
	# 10246	Ei	exbi
	# 10247	Zi	zebi
	# 10248	Yi	yobi
    suffixes=[' B',' KiB',' MiB',' GiB',' TiB',' PiB',' EiB',' ZiB',' YiB']
    suffixIndex = 0
    size = p_size
    while size > 1024 and suffixIndex < 8:
        suffixIndex += 1  # increment the index of the suffix
        size = size / 1024.0  # apply the division
    return "%.*f%s"%( p_precision, size, suffixes[ suffixIndex ] )

# deprecated old call name - to be removed everywhere used and then deleted
def hndy_GetHumanReadable(size,precision=2):
    return hndy_GetHumanReadable_Bytes( size, precision )

def hndy_time_prefix() :
	if False :
		return im_time.strftime("%H_%M_%S", im_time.gmtime())
	else :
		return tempus_timestamp_utc(4)

def hndy_str_of_boolean( p_bool, p_name ):
	r_s = p_name + "="
	if p_bool :
		r_s += "Y"
	else:
		r_s += "N"
	return r_s

def hndy_str_to_boolean( p_v ):
	if p_v.lower() in [ "y", "yes", "t", "true", "on" ] :
		return True
	elif p_v.lower() in [ "n", "no", "f", "false", "off" ] :
		return False
	else :
		return None

def hndy_str_to_goodpath( p_str ):
	#print( "hndy_str_to_goodpath")
	#print( pfn4print( p_str ) )
	r_str = p_str
	try:
		r_str = im_os.path.normpath( p_str)
		#print( "used normpath")
	except:
		r_str = p_str.rstrip( "/")
		#print( "used rstrip")
	#print( pfn4print( r_str ) )
	return r_str

# --------------------------------------------------
# hndy - Other handy functions - not yet prefixed
# --------------------------------------------------

# a rough and ready numeric naturaliser
# perhaps later replace with usage of the natsort module
# see https://pypi.org/project/natsort/
def string_naturalised( s): 
	# number of chars in the largest possible int
	_maxint_digits = len(str(im_sys.maxsize))
	convert = lambda text: str(int(text)).zfill(12) if text.isdigit() else text.lower() 
	alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ] 
	ak = alphanum_key( s )
	new_s = "".join( ak ) 
	return new_s

def is_n_to_show_progess( n ) :
	if n < 11:
		b = True
	elif n < 101 :
		b = (n % 10) == 0
	elif n < 1001 :
		b = (n % 100) == 0
	elif n < 10001 :
		b = (n % 1000) == 0
	elif n < 100001 :
		b = (n % 10000) == 0
	elif n < 1000001 :
		b = (n % 10000) == 0
	elif n < 10000001 :
		b = (n % 100000) == 0
	else :
		b = False
	return b

def is_n_to_show_progess_showing_all_below_x( n, x ) :
	if n < x :
		b = True
	else :
		b = is_n_to_show_progess( n )
	return b


def is_n_range_to_show_progess( from_n, upto_n ) :
	def inrange_check( factor, f_n, u_n ) :
		boo = ( (upto_n % factor) == 0 ) or ( (upto_n - from_n) >= factor ) or ( (upto_n % factor) < (from_n % factor) )
		return boo
	if upto_n < 101 :
		b = ( (upto_n % 10) == 0 ) or ( (upto_n - from_n) >= 10 ) or ( (upto_n % 10) < (from_n % 10) )
	elif upto_n < 1001 :
		b = inrange_check( 100, from_n, upto_n )
	elif upto_n < 10001 :
		b = inrange_check( 1000, from_n, upto_n )
	elif upto_n < 100001 :
		b = inrange_check( 10000, from_n, upto_n )
	elif upto_n < 1000001 :
		b = inrange_check( 100000, from_n, upto_n )
	elif upto_n < 10000001 :
		b = inrange_check( 1000000, from_n, upto_n )
	else :
		b = False
	return b

def is_n_to_show_progess_timed_get_mark():
	return im_time.perf_counter()

def is_n_to_show_progess_timed( p_n, min_time_diff, max_time_diff, prev_time_mark) :
	i_t = im_time.perf_counter()
	i_d = i_t - prev_time_mark
	if i_d > max_time_diff :
		r_b = True
	elif i_d > min_time_diff :
		if p_n < 11:
			r_b = True
		elif p_n < 101 :
			r_b = (p_n % 10) == 0
		elif p_n < 1001 :
			r_b = (p_n % 100) == 0
		elif p_n < 10001 :
			r_b = (p_n % 1000) == 0
		elif p_n < 100001 :
			r_b = (p_n % 10000) == 0
		elif p_n < 1000001 :
			r_b = (p_n % 10000) == 0
		elif p_n < 10000001 :
			r_b = (p_n % 100000) == 0
		else :
			r_b = False
	else :
		r_b = False
	if r_b :
		r_new_time_mark = im_time.perf_counter()
	else:
		r_new_time_mark = prev_time_mark
	return r_b, r_new_time_mark

def is_n_to_show_progess_timed_stock( p_n, p_time_mark):
	min_time_diff = 5 # so every is_n will be reported
	max_time_diff = 15 # show something every 15 seconds
	r_b, r_new_time_mark = is_n_to_show_progess_timed( p_n, min_time_diff, max_time_diff, p_time_mark)
	return r_b, r_new_time_mark

class ProgressTrackObject():
	# Usage note: create an instance of the object and then do at least one AddInfoPoint before calling a prediction
	# Otherwise you'll just get a None returned as an initial value does not enable a prediction
	# object design values - i.e. don't vary across instances
	i_pretrack_index = -1 # note: the value is arbitrary but must less than zero
	i_modulus = 55
	def __init__(self, p_limit, p_time ):
		self.dct_amounts = {}
		self.dct_times = {}
		self.n_limit = p_limit
		self.n_at = self.i_pretrack_index
		self.dct_amounts[ self.n_at ] = 0
		self.dct_times[ self.n_at ] = p_time
	def AddInfoPoint(self, p_amount, p_time ):
		# get the prior time mark
		if self.n_at == self.i_pretrack_index :
			i_index = self.i_pretrack_index
		else :
			i_index = self.n_at % self.i_modulus
		# only add a point if time has progressed since the previous - avoids divide by zero elsewhere
		if p_time > self.dct_times[ i_index ]:
			if self.n_at == self.i_pretrack_index :
				self.n_at = 0
			else :
				self.n_at += 1
			if len( self.dct_amounts) < self.i_modulus :
				self.dct_amounts[ self.n_at ] = p_amount
				self.dct_times[ self.n_at ] = p_time
			else :
				t_index = self.n_at % self.i_modulus
				self.dct_amounts[ t_index ] = p_amount
				self.dct_times[ t_index ] = p_time
	def PredictComplete_ViaWhole(self ):
		if len( self.dct_amounts ) < 2 :
			# then we only have the opening point and cannot predict anything
			# really, this shouldn't happen in normal use so we'll let a None return value cause an error
			return None
		t_index = self.n_at % self.i_modulus
		i_done_so_far = self.dct_amounts[ t_index ]
		i_time_since_start = self.dct_times[ t_index ] - self.dct_times[ self.i_pretrack_index ]
		i_rate = i_done_so_far / i_time_since_start
		r_remain = ( self.n_limit - i_done_so_far ) / i_rate
		return r_remain
	def PredictComplete_ViaRecent(self, p_n_range ):
		if len( self.dct_amounts ) < 2 :
			# then we only have the opening point and cannot predict anything
			# really, this shouldn't happen in normal use so we'll let a None return value cause an error
			return None
		if p_n_range >= self.i_modulus :
			i_n_range = self.i_modulus - 1
		elif p_n_range < 0 :
			i_n_range = 1
		else :
			i_n_range = p_n_range
		i_index_b = self.n_at % self.i_modulus
		if ( self.n_at - i_n_range ) < 0 :
			i_index_a = self.i_pretrack_index
		else :
			i_index_a = ( self.n_at - i_n_range ) % self.i_modulus
		i_amount_b = self.dct_amounts[ i_index_b ]
		i_amount_a = self.dct_amounts[ i_index_a ]
		i_amount_diff = i_amount_b - i_amount_a
		i_time_b = self.dct_times[ i_index_b ]
		i_time_a = self.dct_times[ i_index_a ]
		i_time_diff = i_time_b - i_time_a
		i_rate = i_amount_diff / i_time_diff
		r_remain = ( self.n_limit - i_amount_b ) / i_rate
		return r_remain

def UsageExample_ProgressTrackObject():
	import time as im_time
	def do_action_taking_random_time():
		i_zzz = im_rndm.randint(1, 4)
		im_time.sleep( i_zzz )
	print( "Begin")
	i_limit = 500
	n_limit = 900
	i = 0
	n_sum_so_far = 0
	t_z = im_time.perf_counter()
	i_Tracker = ProgressTrackObject( n_limit, t_z)
	while i < i_limit and n_sum_so_far < n_limit :
		i += 1
		print( ("-" * 20 ) + "Iteration #" + str( i ) + " do_action_taking_random_time()" )
		do_action_taking_random_time()
		new_n = im_rndm.random() * 50
		n_sum_so_far += new_n
		i_t = im_time.perf_counter()
		i_time_since_start = i_t - t_z
		print( "Elapsed: " +  "{:.2f}".format( i_time_since_start) + " seconds" )
		i_Tracker.AddInfoPoint( n_sum_so_far, i_t )
		if n_sum_so_far < n_limit :
			print( "PredictComplete_ViaWhole gives remaining as: " + "{:.2f}".format( i_Tracker.PredictComplete_ViaWhole() ) )
			print( "PredictComplete_ViaRecent(5) gives remaining as: " + "{:.2f}".format( i_Tracker.PredictComplete_ViaRecent( 5) ) )
			print( "PredictComplete_ViaRecent(10) gives remaining as: " + "{:.2f}".format( i_Tracker.PredictComplete_ViaRecent( 10) ) )
			print( "PredictComplete_ViaRecent(25) gives remaining as: " + "{:.2f}".format( i_Tracker.PredictComplete_ViaRecent( 25) ) )
	print( "Ended")

# ------------ Example usage
# i_time_mark = is_n_to_show_progess_timed_get_mark()
# do things
# n = n + 1
# b, i_time_mark = is_n_to_show_progess_timed_stock( n, i_time_mark)
# if b :
#   print( "show progress " + str(n) )
# --------------------------------------------------

# --------------------------------------------------
# Combinatory functions 
# --------------------------------------------------

# Combination functions - these take a iist and return a float

# My good ol' combination function
# Made from the Maximum plus a factored Average of the Others MxPlsFctrdAvg
def CalcCombo_MxPlsFctrdAvg( lst ):
	mx = max(lst)
	# MAX + ( ( 1 - MAX ) * ( SUM - MAX ) / ( N - 1 ) )
	return mx + ( ( 1.0 - mx ) * ( sum(lst) - mx ) / ( len(lst) - 1.0 ) )

def CalcCombo_GeoMean( lst ):
	for x in lst:
		multiply = multiply * x
	return (multiply) ** ( 1 / len(lst) )

def CalcCombo_RMS( lst ):
	return im_math.sqrt( sum( n * n for n in lst ) / len( lst) )

# Conformity functions - these take a value and return another value between zero and one
# Of special note, the GW_Atan function can take any float x no matter how large or small
# So it can be a useful function just ge numbers into the conforming range
# The other functions are designed 

def CalcConform_GW_Atan( x, S, A, B ):
	if S:
		return math.atan( (A * x) + C)
	else:
		return 1.0 - math.atan( (A * x) + C)

def CalcConform_GW_Exp( x, S, F, N ):
	if F < 2 :
		F = 2
	if N < 1 :
		N = 1
	if x < 0 :
		x = 0
	if S:
		return 0.0 + x * (F ** (0 - N))
	else:
		return 1.0 - x * (F ** (0 - N))

# --------------------------------------------------
# Local functions - as used just by the Main, not features for export
# --------------------------------------------------

def cmmntry_app_code_name():
	return "commontry"

def Test_Timed_Is_N() :
	print( "Test_Timed_Is_N begun")
	min_time_diff = 0.000000000001
	max_time_diff = 0.1
	n_t = im_time.perf_counter()
	for i in range( 1, 100 ):
		i_b, i_t = is_n_to_show_progess_timed(i, max_time_diff, min_time_diff, n_t )
		if i_b :
			print( i)
	print( "Test_Timed_Is_N ended" )

# --------------------------------------------------
# Main - which with this being a support module, is just for testing
# --------------------------------------------------

# main only if run as a program
if __name__ == "__main__":
	print( "This is the module: commontry" )
	Test_hndy_GetHumanReadable_Seconds()
	if True :
		Test_Timed_Is_N()
	if False:
		print( 'Number of arguments:' + str(len(im_sys.argv)) + ' arguments.')
		print( 'Argument List:' + str(im_sys.argv))
		arg_count = len(im_sys.argv)
		print( "Test DeletionMode")
		print( DeletionMode_GetList() )
		print( DeletionMode_GetTuple() )
		print( "DeletionMode.DryRun: " + str(DeletionMode.DryRun))
		print( "DeletionMode.Bash: " + str(DeletionMode.Bash))
		print( "DeletionMode.DeleteNow: " + str(DeletionMode.DeleteNow))
	elif False:
		print( "BadWinFileFoldName_Lst" )
		print( BadWinFileFoldName_Lst() )
		print( "BadWinFileFoldName_Replacements_Dct" )
		print( BadWinFileFoldName_Replacements_Dct() )
		cs = make_os_script_file_copy_bad_windows_name( "~", "AUX", "~/temp" )
		print( cs )
	elif True:
		print( "Setup for testing: make_os_script_file_copy_fromcoding_intocoding" )
		i_u_src_fold = "/media/thing"
		# original byte sequence name on disc 
		i_b_src_nam = b'\x66\xFC\x72'
		# emulation of what the Python directory read does with it
		i_u_src_nam = i_b_src_nam.decode('utf-8', 'surrogateescape')
		i_tgt_folder = "/home/share"
		#
		print( "i_b_src_nam" )
		print( i_b_src_nam )
		# print( i_u_src_nam ) can't be directly printed because of the surrogates
		print( "i_u_src_fold" )
		print( i_u_src_fold )
		print( "i_tgt_folder" )
		print( i_tgt_folder )
		#
		cs = make_os_script_file_copy_fromcoding_intocoding( i_u_src_fold, i_u_src_nam, i_tgt_folder, "cp1252", None )
		print( "Received back the following as a Bash copy line" )
		print( cs )
		#i_src_fref = "/media/thing_" + '\u0420\u043e\u0441\u0441\u0438\u044f'
	print( "Completed!")
