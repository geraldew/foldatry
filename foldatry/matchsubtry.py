#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# matchsubtry = match sub-trees
#
# This forms part of the foldatry application but can be used as an
# independent command line tool.
#
# --------------------------------------------------
# Copyright (C) 2018-2024  Gerald Waters - Melbourne, Australia
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# --------------------------------------------------
# Imports - Standard
# --------------------------------------------------

import sys as im_sys
# import os.path
import os as im_os
import os.path as im_osp
import hashlib as im_hashlib
from collections import namedtuple
# import time as im_time

# --------------------------------------------------
# Imports - Custom
# --------------------------------------------------

import commontry as cmntry
import exploratry as explrtry

# --------------------------------------------------
# Support Custom
# --------------------------------------------------

def mod_code_name():
	return "matchsubtry"

def mod_code_abbr():
	return "mtchsbtry"

def mod_show_name():
	return "Matchsubtry"

# --------------------------------------------------
# Feature Structure Definitions
# --------------------------------------------------

# --------------------------------------------------
# set up custom named tuples
tpl_PathInfo = namedtuple('tpl_PathInfo', 'pi_thispath pi_parentpath pi_foldcount pi_filecount pi_sizesum')
tpl_AtaFolder = namedtuple('tpl_AtaFolder', 'af_thishash af_parentpath af_foldcount af_filecount af_sizesum')
# tpl_SuperParent = namedtuple('tpl_SuperParent', 'sp_superpath sp_superhash')
tpl_SuperHash = namedtuple('tpl_SuperHash', 'sh_superhash sh_dos')

# --------------------------------------------------
# Support Stock
# --------------------------------------------------

# log and display handling

def depthstr(depth_number):
	return "{D:" + str(depth_number) + "}"

def layerstr(light_touch, depth_number, sided):
	if light_touch:
		ts = ";"
	else:
		ts = ":"
	return "{D" + ts + str(depth_number) + "-" + sided + "}"

def make_log_export_filename( uniq_str ):
	return cmntry.dtff_make_data_filename( uniq_str, mod_code_name() )

# --------------------------------------------------
# Support Custom
# --------------------------------------------------

def dbg_print_traverse_progress( fld_cnt, dpth):
	if (fld_cnt % 1000) == 0:
		print( "Processed " + str( fld_cnt) + " folders, now at depth of " + str(dpth) )

# --------------------------------------------------
# Features - i.e. requiring awareness of custom structures
# --------------------------------------------------

# storage routines

# store a hashed folder into a dictionary
# key is the hash, content is a a list of named tuples of type tpl_PathInfo
# thus the hashes that have more than one in the list are about replicated content
def store_dct_hash_lst_add_tpl_PathInfo( hash_store, hash_code, the_path, the_parent_path, dircount, filcount, sizsum ):
	# hash_store 
	# hash_code 
	# the_path
	# the_parent_path
	# dircount
	# filcount
	# sizsum
	#? prcprf = "Z1>" + " "
	# make named tuple
	n_t = tpl_PathInfo(the_path, the_parent_path, dircount, filcount, sizsum)
	# Add or append the file path
	if hash_code in hash_store:
		# trust that we already put a list in there, so add the new tuple to the list (of tuples)
		hash_store[hash_code].append(n_t)
	else:
		# need to put a list there - of just the new tuple (as a list)
		hash_store[hash_code] = [n_t]
	# return hash_store
	#p_multi_log.do_logs( "b", prcprf + "store_dct_hash_lst_add_tpl_PathInfo" + "#=" + hash_code + " pth=" + the_path )

# store a folder - key is the folderpath, content is a named tuple of type tpl_AtaFolder
# this is mainly just here to provide a reverse index from folderpath to hash
def store_dct_folder_tpl_AtaFolder( fldr_store, fldr_path, fldr_hash_code, parent_path, dircount, filcount, sizsum):
	#? prcprf = "Z2>" + " "
	# make named tuple
	nm_tp = tpl_AtaFolder(fldr_hash_code, parent_path, dircount, filcount, sizsum)
	# Add or append the file path
	if fldr_path in fldr_store:
		# trust that we already put a list in there, so add the new tuple to the list (of tuples)
		input( "Warning, unexpected repeat for " + fldr_path)
	else:
		# assign to dictionary
		# fldr_store[fldr_path] = [nm_tp]
		fldr_store[fldr_path] = nm_tp

# storage for supers - by folder referencing

# store a folder and its superfolder - key is the folder path, content is the superfolder path 
def store_dctnry_folder_superfolder( fldr_store, path_folder, path_super, super_dos ):
	#? prcprf = "Z3>" + " "
	# Add or append the file path
	if path_folder in fldr_store:
		input( "Warning, unexpected repeat for " + path_folder)
	else:
		# assign to dictionary
		fldr_store[path_folder] = path_super
	#p_multi_log.do_logs( "b", prcprf + "sub=" + path_folder + " spr=" + path_super )

# store per superfolder, list of subfolders
def store_dctnry_superfolder_lst_folders( dct_super, super_path, subpath_path, super_dos ):
	#? prcprf = "Z4>" + " "
	# Add or append the file path
	if super_path in dct_super:
		# trust that we already put a list in there, so add the new tuple to the list (of tuples)
		dct_super[super_path].append(subpath_path)
	else:
		# need to put a list there - of just the new tuple (as a list)
		dct_super[super_path] = [subpath_path]
	# return hash_store
	#p_multi_log.do_logs( "b", prcprf + "spr=" + super_path + " sub=" + subpath_path )

# storage for supers - by hash referencing

# store a hash and its superhashes - key is the hash, content is the list of superhashes
def store_dctnry_hash_superhashes( dct_hash, hash_here, hash_super ):
	#? prcprf = "Z5>" + " "
	# Add or append the file path
	if hash_here in dct_hash:
		if True:
			if not (hash_super in dct_hash[hash_here]):
				dct_hash[hash_here].append(hash_super)
		else:
			have_superhash = dct_hash[hash_here]
			if have_superhash != hash_super:
				input( "Warning, unexpected repeat for " + hash_here + " already had " + have_superhash + " before " + hash_super)
	else:
		# assign to dictionary
		dct_hash[hash_here] = [hash_super]
	#p_multi_log.do_logs( "b", prcprf + "sub=" + hash_here + " spr=" + hash_super )

# store per superhash, list of sub-hashes
def store_dctnry_superhash_lst_hashes( dct_super, super_hash, sub_hash, super_dos ):
	#? prcprf = "Z6>" + " "
	# Add or append the file path
	if super_hash in dct_super:
		# trust that we already put a list in there, so add the new item to the list
		if not (sub_hash in dct_super[super_hash]):
			dct_super[super_hash].append(sub_hash)
	else:
		# need to put a list there - of just the item
		dct_super[super_hash] = [sub_hash]
	# return hash_store
	#p_multi_log.do_logs( "b", prcprf + "spr=" + super_hash + " sub=" + sub_hash )

# store a hash and its superhash - key is the hash, content is the superhash and its degree of separation
def store_dctnry_hash_best_superhash( dct_hash_bst_sh, hash_here, hash_super, super_dos ):
	#? prcprf = "Z7>" + " "
	#p_multi_log.do_logs( "b", prcprf + "sub=" + hash_here + " spr=" + hash_super )
	# make named tuple
	nm_tp = tpl_SuperHash( hash_super, super_dos )
	# Add or append the file path
	if hash_here in dct_hash_bst_sh:
		was_dos = dct_hash_bst_sh[hash_here].sh_dos
		was_shsh = dct_hash_bst_sh[hash_here].sh_superhash
		if super_dos > was_dos:
			dct_hash_bst_sh[hash_here] = nm_tp
			#p_multi_log.do_logs( "b", prcprf + "Changed the superhash for " + hash_here + " already had " + was_shsh + " DoS " + str(was_dos) )
			#p_multi_log.do_logs( "b", prcprf + " over to be " + hash_super + " DoS " + str(super_dos) )
	else:
		# assign to dictionary
		dct_hash_bst_sh[hash_here] = nm_tp

# hash derivations

# per file hash 
# for optional use, as for speed this program is intended to scan only folder list info, not file content
def file_read_as_hash( p_pathfilename ):
	blocksize = 64 * 1024
	sha = im_hashlib.sha256()
	r_ok = True
	try:
		with open( p_pathfilename, 'rb') as fp:
			while True:
				try:
					data = fp.read( blocksize)
				except:
					r_ok = False
				if not data or not r_ok :
					break
				sha.update( data)
	except:
		r_ok = False
	return r_ok, sha.hexdigest() 

# per folder hash
def list_hash_digest(strings ):
	#? prcprf = "Z9>" + " "
	#p_multi_log.do_logs( "b", prcprf + "list_hash_digest" )
	import hashlib, struct
	hash = im_hashlib.sha1()
	for s in strings:
		i_s = cmntry.pfn4print( s )
		hash.update( struct.pack( "I", len( i_s ) ) )
		hash.update( i_s.encode( 'utf-8' ) )
		#p_multi_log.do_logs( "b", prcprf + i_s, p_dct_log_set )
	return hash.hexdigest()

# --------------------------------------------------
# Process - Traverse
# --------------------------------------------------


# recursively traverse a folder structure, deriving and storing hashes
# this is the outer non-recursive part
# currently the only difference is the use of a depth counter in the recursive call
# and that's only there to help with debugging
def traverse_tree_add_hash( hashstock_d, folderselfs_d, tree_path, parent_path, hash_files, p_multi_log ):
	#? prcref = "traverse_tree_add_hash"+ " "
	#? prcprf = "C1>" + " "
	#p_multi_log.do_logs( "b", prcprf + prcref + "#Begin#" )
	#p_multi_log.do_logs( "b", prcprf + "? " + tree_path )
	h_fn_calls = 0
	h_fil_count = 0
	i_time_mark = cmntry.is_n_to_show_progess_timed_get_mark()
	# recursively traverse a folder structure, deriving and storing hashes
	# this is a rewrite to settle on a method whereby the hash of a tree depends on the hashes of its sub-trees
	def traverse_tree_recurse( hashstock_d, folderselfs_d, tree_path, parent_path, hash_files, tree_depth ):
		# hashstock_d is a dictionary being built, with hashes for keys, and lists of tuples for values
		# folderselfs_d is a dictionary being built, with folderpaths for keys, and tuples for values
		# tree_path is the folder location now being accounted 
		# parent_path is to go explicitly on the record 
		# parent_hash is to go explicitly on the record
		# hash_files is a boolean for whether file contents should be part of the hash, rather than just name and size
		# bad hack (abuse of optional parameter) to do a call/folder count to mark progress
		def def_name():
			return "Matchsubtry traverse_tree_recurse"
		nonlocal h_fn_calls
		nonlocal h_fil_count
		nonlocal i_time_mark
		nonlocal p_multi_log
		h_fn_calls += 1
		#? prcprf = "D1>" + str( tree_depth) +">" + " "
		#p_multi_log.do_logs( "b", prcprf + "@ " + tree_path )
		#? dbg_print_traverse_progress( h_fn_calls, tree_depth)
		t_b, i_time_mark = cmntry.is_n_to_show_progess_timed_stock( h_fn_calls, i_time_mark )
		if t_b :
			p_multi_log.do_logs( "spdb", def_name() + ": Call count = " + str( h_fn_calls) )
		filnames = []
		dirnames = []
		size = 0
		elements = []
		sizsum = 0
		entries = explrtry.get_lst_sorted_os_scandir( tree_path )
		for entry in entries:
			if entry.is_file(follow_symlinks=False):
				h_fil_count += 1
				t_b, i_time_mark = cmntry.is_n_to_show_progess_timed_stock( h_fil_count, i_time_mark )
				if t_b :
					p_multi_log.do_logs( "spdb", def_name() + ": File count = " + str( h_fil_count) )
				fpath = im_osp.join(tree_path, entry.name) 
				size = entry.stat().st_size
				size = im_os.path.getsize( fpath )
				sizestr = str(size)
				sizsum = sizsum + size
				elements.append(entry.name)
				elements.append(sizestr)
				if hash_files:
					t_read_ok, sha = file_read_as_hash( fpath )
					if t_read_ok:
						elements.append( sha )
				filnames.append(entry.name)
			elif entry.is_dir(follow_symlinks=False):
				elements.append(entry.name)
				# recursive call returning the hash of the subfolder
				new_subpath = im_os.path.join(tree_path, entry.name)
				(subfolder_hash, size) = traverse_tree_recurse( hashstock_d, folderselfs_d, new_subpath, tree_path, hash_files, tree_depth + 1 )
				elements.append(subfolder_hash)
				sizsum = sizsum + size
				# dprint( "subfolder_hash:" + subfolder_hash )
				# count subfolders and prepare for recursion further down
				dirnames.append(entry.name)
		# hash the list
		dirfil_hash = list_hash_digest( elements )
		filcount = len(filnames)
		dircount = len(dirnames)
		if dircount > 0 or filcount > 0:
			# don't bother storing hashes of empty folders - it seems meaningless and all empties will just match 
			store_dct_hash_lst_add_tpl_PathInfo( hashstock_d, dirfil_hash, tree_path, parent_path, dircount, filcount, sizsum )
			store_dct_folder_tpl_AtaFolder( folderselfs_d, tree_path, dirfil_hash, parent_path, dircount, filcount, sizsum)
		#p_multi_log.do_logs( "b", prcprf + dirfil_hash + " " + tree_path + " " + parent_path )
		return dirfil_hash, sizsum
	# mini-main
	dont_care_hash = traverse_tree_recurse( hashstock_d, folderselfs_d, tree_path, parent_path, hash_files, 0 )
	#p_multi_log.do_logs( "b", prcprf + prcref + "#End#" )

# --------------------------------------------------
# Process - Analyse
# --------------------------------------------------

# go through the set of hashes with multiple paths listed
# for each path in the list, try to find the upmost parent that also has a hash with multiple paths listed
# i.e. the SuperFolder
# store an entry per path with its SuperFolder 
# store for each SuperFolder, a list of the folders having it as a superfolder
# this call now also handles the superhash
# to determine the superhash, it relies that the process will visit the whole tree
# so therefore each time that a better  fact that the superhash 
def FindAndStore_Super_FoldersHashes( dct_hash_lst_fldrs, dct_fldr_selves, dct_fldr_superfolder, dct_superfolder_folders, dct_hash_best_superhash ):
	# r dct_hash_lst_fldrs - key=hashes, each is a list of folders
	# r dct_fldr_selves - key is folder, each with its info
	# w dct_fldr_superfolder - key is folder, each with its superfolder
	# w dct_superfolder_folders - key is superfolder, each with list of subfolders
	#
	# w dct_hash_superhash - key is hash, each with its best superparent hash
	# w dct_superhash_hashes - key is superparent-hash, each with list of hashes
	#
	#? prcref = "FindAndStore_Super_FoldersHashes"+ " "
	#? prcprf = "C2>"+ " "
	#p_multi_log.do_logs( "b", prcprf + prcref + "#Begin#" )
	# input('Press <ENTER>')
	# old method, get a list of lists for the hashes with multiple lists with them
	listof_multilisthashes_fldrlists = list(filter(lambda x: len(x) > 1, dct_hash_lst_fldrs.values()))
	# new method get a list of the hashes with multiple lists with them
	listof_multilist_hashes = list(filter(lambda x: len(x) > 1, dct_hash_lst_fldrs.keys()))
	# old if len(listof_multilisthashes_fldrlists) > 0:
	if len(listof_multilist_hashes) > 0:
		# old now have a list of the folder hashes with multiple lists with them
		#p_multi_log.do_logs( "b", prcprf + 'now have a list of the folder hashes that have multiple folder lists with them' )
		# old dprint( str(len(listof_multilisthashes_fldrlists)) + " hashes")
		#p_multi_log.do_logs( "b", prcprf + str(len(listof_multilist_hashes)) + " hashes" )
		for keyhash in listof_multilist_hashes:
		# old for folder_list in listof_multilisthashes_fldrlists:
			folder_list = dct_hash_lst_fldrs[keyhash]
			if len(folder_list) == 1:
				pass
				#p_multi_log.do_logs( "b", prcprf + 'Just one in list, nothing to do.' )
			else:
				#p_multi_log.do_logs( "b", prcprf + 'Multiple folders in list.' + str(len(folder_list)) + " folders." )
				# each hash
				no_head = True
				for pathinfo_tuple in folder_list:
					# each folder with that hash
					current_path = pathinfo_tuple.pi_thispath
					#p_multi_log.do_logs( "b", prcprf + current_path )
					if ( pathinfo_tuple.pi_foldcount == 0 ) and ( pathinfo_tuple.pi_filecount == 0 ):
						input( "This just shouldn't happen as we don't do Hashes for empty folders!!")
					else:
						#p_multi_log.do_logs( "b", prcprf + "# With " + str(pathinfo_tuple.pi_foldcount) + " folders & " + str(pathinfo_tuple.pi_filecount) + " files: " + current_path )
						# now go up the folder tree
						if True:
							# start by getting the parent path from the Hash folderlist entry
							parentpath = pathinfo_tuple.pi_parentpath
							superfldr_so_far = current_path
							superhash_so_far = dct_fldr_selves[current_path].af_thishash
							super_dos = 0
							while len(parentpath) > 0:
								#p_multi_log.do_logs( "b", prcprf + "Checking parent: " + parentpath )
								af_tpl = dct_fldr_selves[parentpath]
								af_hash = af_tpl.af_thishash
								af_nextparent = af_tpl.af_parentpath
								if len(af_hash) > 0:
									# check parent for match
									ParentIsOfHashMulti = CheckHashHasMultiples(dct_hash_lst_fldrs, af_hash)
									if ParentIsOfHashMulti:
										superfldr_so_far = parentpath
										superhash_so_far = af_hash
										super_dos = super_dos + 1
										#p_multi_log.do_logs( "b", prcprf + "New parent. superfldr_so_far: " + superfldr_so_far + " #:" + af_hash )
										#p_multi_log.do_logs( "b", prcprf + "Resetting parent to next" )
										parentpath = af_nextparent
									else:
										#p_multi_log.do_logs( "b", prcprf + "No multi at hash of: " + parentpath )
										parentpath = ""
								else:
									parentpath = ""
							if len(superfldr_so_far) > 0:
								# store the superfolder
								#p_multi_log.do_logs( "b", prcprf + "# Found a superfolder for: " + current_path + " at: " + superfldr_so_far )
								store_dctnry_folder_superfolder( dct_fldr_superfolder, current_path, superfldr_so_far, super_dos )
								store_dctnry_superfolder_lst_folders( dct_superfolder_folders, superfldr_so_far, current_path, super_dos )
								v_hash_here = keyhash
								v_hash_super = superhash_so_far
								store_dctnry_hash_best_superhash( dct_hash_best_superhash, v_hash_here, v_hash_super, super_dos )
								#bad! store_dctnry_superhash_lst_hashes( dct_superhash_hashes, v_hash_super, v_hash_here, super_dos)
						else:
							(got_super, super_parent_folder_path, super_parent_folder_hash) = CheckForSuperParentHashWithMulitple(dct_hash_lst_fldrs, dct_fldr_selves, fldr_parents, folder_tuple.pi_thispath)
							if got_super:
								#p_multi_log.do_logs( "b", prcprf + "# Found a superfolderhash: " + super_parent_folder_hash + " at: " + super_parent_folder_path )
								store_folder_superfolder( fldr_parents, folder_tuple.pi_thispath, super_parent_folder_path, super_parent_folder_hash)
						#p_multi_log.do_logs( "b", prcprf + '------------------------------------' )
	else:
		pass
		#p_multi_log.do_logs( "b", prcprf + '# No hashes had multiple lists.' )
	# input('Press <ENTER>')
	#p_multi_log.do_logs( "b", prcprf + prcref + "#End#" )

# go through the list of hashes with their best found superhashes
# and make a reverse list of that information
# i.e. a dictionary of the superhashes
# with for each, a list the hashes that have them 
#reduce call to only the required parameters
#def Make_Digest_of_Super_Hashes( dct_hash_lst_fldrs, dct_fldr_selves, dct_fldr_superfolder, dct_superfolder_folders, p_dct_hash_superhash, p_dct_superhash_hashes ):
def Make_Digest_of_Super_Hashes( p_dct_hash_superhash, p_dct_superhash_hashes ):
	# dct_hash_lst_fldrs, dct_fldr_selves, dct_fldr_superfolder, dct_superfolder_folders,
	# p_dct_hash_superhash
	# p_dct_superhash_hashes
	prcref = "Make_Digest_of_Super_Hashes"+ " "
	prcprf = "C3>"+ " "
	#p_multi_log.do_logs( "b", prcprf + prcref + "#Begin#" )
	for k in p_dct_hash_superhash.keys():
		tpl_SH = p_dct_hash_superhash[k]
		v_hash_super = tpl_SH.sh_superhash
		v_hash_here = k
		super_dos = tpl_SH.sh_dos
		store_dctnry_superhash_lst_hashes( p_dct_superhash_hashes, v_hash_super, v_hash_here, super_dos )

# make a dictionary where the key is the size and the entry is the hash
def Make_Size_Dict_of_SuperHashes( dct_hash_lst_fldrs, dct_superhash_hashes ):
	prcref = "Make_Size_Dict_of_SuperHashes" + " "
	prcprf = "C5>" + " "
	#p_multi_log.do_logs( "b", prcprf + prcref + "#Begin#" )
	# declare the new dictionary
	dct_siz_sprhashes = {}
	# for each superhash
	for k in dct_superhash_hashes.keys():
		# get the size for the hash
		f_l = dct_hash_lst_fldrs[k]
		# as they're know equal, just getting the first in the list will do
		tpl_PI = f_l[0]
		v_size = tpl_PI.pi_sizesum
		# now put that in the dictionary
		if v_size in dct_siz_sprhashes:
			# trust that we already put a list in there, so add the new hash to the list
			dct_siz_sprhashes[v_size].append(k)
		else:
			# need to put a list there - of just the new hash (as a list)
			dct_siz_sprhashes[v_size] = [k]
	return dct_siz_sprhashes

# --------------------------------------------------
# Process - Categorise
# --------------------------------------------------

# build three new sets of hashes
# - ones with only superfolders - these are the top level tree matches
# - ones with no superfolders - these are the tree matches totally inside the first set
# - ones with some superfolders - these are the tree matches with some partials
def CategoriseHashes( hashes_d, supers_d, folder_d ):
	prcref = "CategoriseHashes" + " "
	prcprf = "C4>" + " "
	#p_multi_log.do_logs( "b", prcprf + prcref + "#Begin#" )
	hashes_all_superfolders_d = {}
	hashes_nil_superfolders_d = {}
	hashes_prt_superfolders_d = {}
	for k in hashes_d.keys():
		if len(hashes_d[k]) > 1:
			#p_multi_log.do_logs( "b", prcprf + '@ Hash:' + k )
			sp_count = 0
			non_sp_count = 0
			# go through the list of tpl_PathInfo tuples at this hash
			for pi_nt in hashes_d[k]:
				#p_multi_log.do_logs( "b", prcprf + '@ This:' + pi_nt.pi_thispath + "to be compared to list of " + str(len(supers_d.keys())) )
				if pi_nt.pi_thispath in supers_d.keys():
					sp_count += 1
					#p_multi_log.do_logs( "b", prcprf + '@ is SP:' + pi_nt.pi_thispath )
				elif pi_nt.pi_thispath in folder_d.keys():
					non_sp_count += 1
					#p_multi_log.do_logs( "b", prcprf + '@ is ok:' + pi_nt.pi_thispath )
			if ( sp_count > 0 ) and (non_sp_count == 0 ):
				# add this hash to hashes_all_superfolders_d
				hashes_all_superfolders_d[k] = k
				#p_multi_log.do_logs( "b", prcprf + '@ Hash added to all_superfolders:' + k )
			elif ( sp_count == 0 ) and (non_sp_count > 0 ):
				# add this hash to hashes_nil_superfolders_d
				hashes_nil_superfolders_d[k] = k
				# dprint('@ Hash added to nil_superfolders:' + k)
			elif ( sp_count > 0 ) and (non_sp_count > 0 ):
				# add this hash to hashes_prt_superfolders_d
				hashes_prt_superfolders_d[k] = k
				#p_multi_log.do_logs( "b", prcprf + '@ Hash added to prt_superfolders:' + k )
			else:
				input( "DANGER Will Robinson!")
	return hashes_all_superfolders_d, hashes_nil_superfolders_d, hashes_prt_superfolders_d
	#p_multi_log.do_logs( "b", prcprf + prcref + "#End#" )

# --------------------------------------------------
# Expose displays
# --------------------------------------------------

# 
def CheckMatchAtKey(dictx, hashkey):
	le_list = dictx[hashkey]
	if len(le_list) > 0:
		return True
	else:
		return False

def CheckHashHasMultiples(hash_dict, hash_key):
	le_list = hash_dict[hash_key]
	if len(le_list) > 1:
		return True
	else:
		return False


def GetCount_Hashes_With_MultipleFolders( hsh_dctnry ):
	prcref = "GetCount_Hashes_With_MultipleFolders"+ " "
	prcprf = "Q1>"+ " "
	#p_multi_log.do_logs( "b", prcprf + prcref + "#Begin#" + "-------------------------------" )
	keys_of_multi_list_hashes = []
	for hashkey in hsh_dctnry.keys():
		if len( hsh_dctnry[hashkey] ) > 1:
			keys_of_multi_list_hashes.append( hashkey)
	# keys_of_multi_list_hashes = list(filter(lambda x: len(x) > 1, hsh_dctnry.keys()))
	multi_hash_count = len(keys_of_multi_list_hashes)
	return multi_hash_count

# variation to show the hashes
# p_dct_k_xxx
# p_dct_k_yyy
def display_multi_matches( hsh_dctnry, pth_dctnry ):
	prcref = "display_multi_matches"+ " "
	prcprf = "R1>"+ " "
	#p_multi_log.do_logs( "b", prcprf + prcref + "#Begin#" + "-------------------------------" )
	keys_of_multi_list_hashes = []
	for hashkey in hsh_dctnry.keys():
		if len( hsh_dctnry[hashkey] ) > 1:
			keys_of_multi_list_hashes.append( hashkey)
	# keys_of_multi_list_hashes = list(filter(lambda x: len(x) > 1, hsh_dctnry.keys()))
	multi_hash_count = len(keys_of_multi_list_hashes)
	if len(keys_of_multi_list_hashes) > 0:
		#p_multi_log.do_logs( "b", prcprf + '! Duplicates Found:' )
		#p_multi_log.do_logs( "b", prcprf + '! The following folders match. The file contents could differ, but the lists are identical' )
		for one_hash_key in keys_of_multi_list_hashes:
			#p_multi_log.do_logs( "b", prcprf + '! ====================================' )
			#p_multi_log.do_logs( "b", prcprf + '! Hash :' + one_hash_key )
			if len(hsh_dctnry[one_hash_key]) > 1:
				no_head = True
				for subresult in hsh_dctnry[one_hash_key]:
					if subresult.pi_foldcount > 0 or subresult.pi_filecount > 0:
						no_head = False
						#p_multi_log.do_logs( "b", prcprf + "! With " + str(subresult.pi_foldcount) + " folders & " + str(subresult.pi_filecount) + " files: " + subresult.pi_thispath )
						if len(subresult.pi_parentpath) > 0:
							parent_tuple = pth_dctnry[ subresult.pi_parentpath ]
							parenthash = parent_tuple.af_thishash 
							# check parent for match
							ParentOfInterest = CheckHashHasMultiples(hsh_dctnry, parenthash)
							if ParentOfInterest:
								pass
								#p_multi_log.do_logs( "b", prcprf + "! Parent " + subresult.pi_parentpath + " has hash worth checking: " + parenthash )
						#p_multi_log.do_logs( "b", prcprf + '------------------------------------' )
	else:
		pass
		#p_multi_log.do_logs( "b", prcprf + '! No duplicates found.' )
	#p_multi_log.do_logs( "b", prcprf + prcref + "#End#" )
	return multi_hash_count

def display_superfolders( ds_dct_superfolder_folders, ds_dct_fldr_selves ):
	prcref = "display_superfolders"+ " "
	prcprf = "R2>"+ " "
	#p_multi_log.do_logs( "b", prcprf + prcref + "-------------------------------" )
	#p_multi_log.do_logs( "b", prcprf + "there are " + str( len( ds_dct_superfolder_folders)) + " super folders." )
	for sp in ds_dct_superfolder_folders.keys():
		af_tpl = ds_dct_fldr_selves[sp]
		af_hash = af_tpl.af_thishash
		#p_multi_log.do_logs( "b", prcprf + "Super:" + sp + " ##:" + af_hash )
		fl = ds_dct_superfolder_folders[sp]
		#p_multi_log.do_logs( "b", prcprf + str(len(fl)) + " folders." )
		for fp in fl:
			af_tpl = ds_dct_fldr_selves[fp]
			af_hash = af_tpl.af_thishash
			#p_multi_log.do_logs( "b", prcprf + ": " + fp + " ##:" + af_hash )
	#p_multi_log.do_logs( "b", prcprf + prcref + "#End#" )

def display_superhashes( ds_dct_superhashes, ds_dct_hash_lst_fldrs, p_multi_log):
	prcref = "display_superhashes"+ " "
	prcprf = "R3>"+ " "
	if __debug__:
		p_multi_log.do_logs( "b", prcprf + prcref + "-------------------------------" )
		p_multi_log.do_logs( "b", prcprf + "there are " + str( len( ds_dct_superhashes)) + " super hashes." )
	for spr_h in ds_dct_superhashes.keys():
		if __debug__:
			p_multi_log.do_logs( "b", prcprf + "SuperHash:" + spr_h )
		fld_lst = ds_dct_hash_lst_fldrs[spr_h]
		if __debug__:
			p_multi_log.do_logs( "b", prcprf + "there are " + str( len( fld_lst)) + " folders with this hash." )
		for fld in fld_lst:
			pass
			if __debug__:
				p_multi_log.do_logs( "b", prcprf + "Folder:" + fld.pi_thispath + " which has Folders:" + str( fld.pi_foldcount) + " Files:" + str( fld.pi_filecount)  + " Size:" + str( fld.pi_sizesum) )
		# explore the sub-hashes - would often be lots and lots of these
		hl = ds_dct_superhashes[spr_h]
		if __debug__:
			p_multi_log.do_logs( "b", prcprf + str(len(hl)) + " hashes have this SuperHash." )
		for hsh in hl:
			# af_tpl = ds_dct_fldr_selves[fp]
			# af_hash = af_tpl.af_thishash
			if __debug__:
				p_multi_log.do_logs( "b", prcprf + " ##:" + hsh )
	if __debug__:
		p_multi_log.do_logs( "b", prcprf + prcref + "#End#" )

def display_top_n_sized_superhashes( ds_dct_sized_superhashes, ds_dct_hash_lst_fldrs, break_after, p_multi_log):
	prcref = "display_top_n_sized_superhashes"+ " "
	prcprf = "R4>"+ " "
	sh_sz_count = len( ds_dct_sized_superhashes)
	if sh_sz_count > 0 :
		p_multi_log.do_logs( "db", prcprf + prcref + "===============================" )
		p_multi_log.do_logs( "db", prcprf + "there are " + str( sh_sz_count) + " sizes of super hashes." )
		# get desending list of sizes
		sz_lst = sorted( list( ds_dct_sized_superhashes.keys() ), reverse=True )
		sz_at = 0
		# for the top n sizes
		for siz in sz_lst:
			# for each size
			# limit to top n
			sz_at = sz_at + 1
			if sz_at > break_after:
				break
			p_multi_log.do_logs( "db", prcprf + "__" + "Hash size #" + str( sz_at) + " size = " + cmntry.hndy_GetHumanReadable(siz) + " " + str( siz) )
			# get the hashes
			h_l = ds_dct_sized_superhashes[siz]
			p_multi_log.do_logs( "db", prcprf + "__" + "has " + str( len( h_l)) + " super hashes." )
			for hsh in h_l:
				# per hash
				p_multi_log.do_logs( "db", prcprf + "__" + "__" + "hash: " + hsh )
				# for each hash, get the list of folders
				fld_lst = ds_dct_hash_lst_fldrs[hsh]
				p_multi_log.do_logs( "db", prcprf + "__" + "__" + str( len( fld_lst) ) + " paths: " )
				for tpl_pi in fld_lst:
					# per folder
					f_path = tpl_pi.pi_thispath
					p_multi_log.do_logs( "db", prcprf + "__" + "__" + "__" + "__" + "path: " + f_path )

def export_superhashes( ds_dct_sized_superhashes, ds_dct_hash_lst_fldrs, export_to_pathfilename, break_after ):
	sh_sz_count = len( ds_dct_sized_superhashes)
	if sh_sz_count > 0 :
		# get descending list of sizes -as this is the data export we might need to do this
		# but it's still a good idea for human reading of the data output file 
		sz_lst = sorted( list( ds_dct_sized_superhashes.keys() ), reverse=True )
		sz_at = 0
		not_done_header = True
		for siz in sz_lst:
			# for each size
			sz_at = sz_at + 1
			# limit to top n
			# allow a negative break limit to mean no limit at all
			if (break_after > 0) and (sz_at > break_after):
				break
			# get the hashes
			h_l = ds_dct_sized_superhashes[siz]
			for hsh in h_l:
				# per hash
				# for each hash, get the list of folders
				fld_lst = ds_dct_hash_lst_fldrs[hsh]
				for tpl_pi in fld_lst:
					f_path = tpl_pi.pi_thispath
					if not_done_header :
						cmntry.file_in_home_text_new_list_as_data_head( export_to_pathfilename, [ "Hash", "Size", "PathFile" ] )
						not_done_header = False
					cmntry.file_in_home_text_add_list_as_data_line( export_to_pathfilename, [ hsh, str( siz), f_path ] )

# --------------------------------------------------
# Command
# --------------------------------------------------

def matchsubtry_command( p_tree_paths, p_hash_files, p_fn_Check_NoInterruption, p_multi_log):
	# Parameters
	# p_tree_paths :: list of strings := giving the paths to be analysed
	# p_tree_path :: string := giving the path to be analysed
	# p_hash_files :: boolean := whether file contents should be part of the hash, rather than just name and size
	# p_dct_log_flags :: dictionary := v_dct_k_outctrl_v_flagbool
	# recursively derive hashes and store them
	# ---------------------
	# Phase 0 - preparations
	#
	i_prcref = mod_code_name() + " matchsubtry_command"+ " "
	i_prcprf = "B>"+ " "
	if __debug__:
		p_multi_log.do_logs( "b", i_prcprf + i_prcref + "Phase:0: Setup" )
	# setup the output controls
	i_use_datetime_str = cmntry.bfff_datetime_ident_str()
	i_data_export_txt_fn_str = make_log_export_filename( i_use_datetime_str)
	#
	p_multi_log.do_logs( "ds", mod_code_name() + " - find all identical sub-trees." )
	for p_t_p in p_tree_paths :
		p_multi_log.do_logs( "ds", "Path= " + p_t_p )
	# set up structures for Phase 1
	mtc_dct_hash_lst_fldrs = {} # dictionary of hashes 
	# i_dct_k_hashstr_v_lst_fldrstr
	mtc_dct_fldr_selves = {} # dictionary of folders with info about them
	# i_dct_k_fldrStr_v_yyy
	# set up structures for Phase 2
	mtc_dct_fldr_superfolder = {} # dictionary of folders with eaches superfolder
	# i_dct_k_fldrStr_v_sprfldrStr
	mtc_dct_superfolder_folders = {} # dictionary of superfolders with all their subfolders 
	# i_dct_k_sprfldrStr_v_lst_subfldrStrs
	mtc_dct_hash_best_superhash = {} # dictionary of folders with eaches best superhash
	# i_dct_k_hashstr_v_bestsuperhashStr
	mtc_dct_superhash_hashes = {} # dictionary of the superhashes with all their subhashes 
	# i_dct_k_superhashStr_v_lst_subhashStrs
	# set structures for Phase 3
	# lists:
	#   hashes_sp_all_d - list of hashes with only superfolders - these are the top level tree matches
	#   hashes_sp_nil_d - list of hashes with no superfolders - these are the tree matches totally inside the first set
	#   hashes_sp_prt_d - list of hashes with some superfolders - these are the tree matches with some partials
	# ---------------------
	# Phase 1 - tree traversal
	#
	p_multi_log.do_logs( "pd", i_prcprf + i_prcref + "Phase:1: Tree traversal" )
	# Traverse the tree calculating and collecting hashes
	tpi = 0
	tpn = len(p_tree_paths)
	for tree_path in p_tree_paths:
		tpi = tpi + 1
		p_multi_log.do_logs( "ds", "Path " + str(tpi) + " of " + str(tpn) + ": " + tree_path )
		# here we could add use of p_fn_Check_NoInterruption
		traverse_tree_add_hash( mtc_dct_hash_lst_fldrs, mtc_dct_fldr_selves, tree_path, "", p_hash_files, p_multi_log )
	# report phase results
	if True:
		# here we could add use of p_fn_Check_NoInterruption
		multiple_hash_count = GetCount_Hashes_With_MultipleFolders(mtc_dct_hash_lst_fldrs )
		p_multi_log.do_logs( "pd", i_prcprf + "In Phase 1 we found " )
		p_multi_log.do_logs( "pd", i_prcprf + "Found " + str( len(mtc_dct_fldr_selves)) + " folders" )
		p_multi_log.do_logs( "pd", i_prcprf + "Found " + str( len(mtc_dct_hash_lst_fldrs)) + " hashes" )
		p_multi_log.do_logs( "pd", i_prcprf + "Found " + str( multiple_hash_count) + " hashes that were shared by multiple folders" )
		#
		display_multi_matches(mtc_dct_hash_lst_fldrs, mtc_dct_fldr_selves )
	# ---------------------
	p_multi_log.do_logs( "pd", i_prcprf + i_prcref + "Phase:2: Super Folders" )
	# ---------------------
	# Phase 2 - determine super folders
	# Analyse to find and mark all the Super parent multiply-appearing hashes
	# here we could add use of p_fn_Check_NoInterruption
	FindAndStore_Super_FoldersHashes(mtc_dct_hash_lst_fldrs, mtc_dct_fldr_selves, mtc_dct_fldr_superfolder, mtc_dct_superfolder_folders, mtc_dct_hash_best_superhash )
	# report phase results
	if True:
		p_multi_log.do_logs( "pd", i_prcprf + "In Phase 2 we found:" )
		p_multi_log.do_logs( "pd", i_prcprf + "Found " + str( len(mtc_dct_fldr_superfolder)) + " folders with superfolders" )
		p_multi_log.do_logs( "pd", i_prcprf + "Found " + str( len(mtc_dct_superfolder_folders)) + " superfolders" )
		p_multi_log.do_logs( "pd", i_prcprf + "Found " + str( len(mtc_dct_hash_best_superhash)) + " hashes with best superhash" )
		#
		display_superfolders( mtc_dct_superfolder_folders, mtc_dct_fldr_selves )
	# ---------------------
	p_multi_log.do_logs( "pd", i_prcprf + i_prcref + "Phase:3: Digest of Super Hashes" )
	# ---------------------
	# Phase 3 - make digest of super hashes
	# 
	#reduce call to only the required parameters
	#Make_Digest_of_Super_Hashes( mtc_dct_hash_lst_fldrs, mtc_dct_fldr_selves, mtc_dct_fldr_superfolder, mtc_dct_superfolder_folders, mtc_dct_hash_best_superhash, mtc_dct_superhash_hashes )
	# here we could add use of p_fn_Check_NoInterruption
	Make_Digest_of_Super_Hashes( mtc_dct_hash_best_superhash, mtc_dct_superhash_hashes )
	# report phase results
	if True:
		p_multi_log.do_logs( "pd", i_prcprf + "In Phase 3 we found:" )
		p_multi_log.do_logs( "pd", i_prcprf + "Found " + str( len(mtc_dct_superhash_hashes)) + " superhashes" )
		#
		display_superhashes( mtc_dct_superhash_hashes, mtc_dct_hash_lst_fldrs, p_multi_log)
	# ---------------------
	p_multi_log.do_logs( "pd", i_prcprf + i_prcref + "Phase:4: Sizes of Super Hashes" )
	# ---------------------
	# Phase 4 - Make_Size_Dict_of_SuperHashes
	#
	# here we could add use of p_fn_Check_NoInterruption
	mtc_dct_sized_superhashes = Make_Size_Dict_of_SuperHashes( mtc_dct_hash_lst_fldrs, mtc_dct_superhash_hashes )
	if True:
		p_multi_log.do_logs( "pdb", i_prcprf + "In Phase 4 we found:" )
		p_multi_log.do_logs( "pdb", i_prcprf + "Found " + str( len(mtc_dct_sized_superhashes)) + " sizes of superhashes" )
		if False: # can be turned on for debugging
			export_superhashes( mtc_dct_sized_superhashes, mtc_dct_hash_lst_fldrs, i_data_export_txt_fn_str, 999)
		# leave the following here for when debugging
		if False:
			display_top_n_sized_superhashes( mtc_dct_sized_superhashes, mtc_dct_hash_lst_fldrs, 10, p_multi_log)
	# ---------------------
	# Phase:5 idea doesn't seem relevant anymore, concepts have changed, hence deprecated for now
	# Leave here in case resurrect the concept
	if False:
		p_multi_log.do_logs( "rds", i_prcprf + "Phase:5: Categorise" )
		# Phase 5 - categorise
		# Make three lists of hashes 
		hashes_sp_all_d, hashes_sp_nil_d, hashes_sp_prt_d = CategoriseHashes( mtc_dct_hash_lst_fldrs, mtc_dct_superfolder_folders, mtc_dct_fldr_selves )
		# report phase results
		if True:
			p_multi_log.do_logs( "sd", i_prcprf + "Phase 4 Found " )
			p_multi_log.do_logs( "sd", i_prcprf + "Found " + str( len(hashes_sp_all_d)) + " hashes with only SuperParents" )
			p_multi_log.do_logs( "sd", i_prcprf + "Found " + str( len(hashes_sp_nil_d)) + " hashes with no SuperParents" )
			p_multi_log.do_logs( "sd", i_prcprf + "Found " + str( len(hashes_sp_prt_d)) + " hashes with mixed parentage" )
	p_multi_log.do_logs( "db", i_prcprf + i_prcref + "#End#" )
	# ---------------------
	# Final Phase - returns
	# - From Phase 1
	#     mtc_dct_hash_lst_fldrs - dictionary of hashes, each with a list of folders
	#     mtc_dct_fldr_selves - dictionary of folders
	# - From Phase 2
	#     mtc_dct_fldr_superfolder - dictionary of folders, each with their superfolder
	#     mtc_dct_superfolder_folders - dictionary of superfolders, each with a list of folders
	#     mtc_dct_hash_best_superhash - dictionary of hashes, each their best superhash
	# - From Phase 3
	#     mtc_dct_superhash_hashes - dictionary of superhashes, each with all their subhashes
	# - From Phase 4
	#     mtc_dct_sized_superhashes - dictionary of sizes, each with a list of superhashes
	# - From Phase 5 - deprecated
	# ---------------------
	p_multi_log.do_logs( "pd", i_prcprf + i_prcref + "Done: Returning results" )
	# ---------------------
	return mtc_dct_hash_lst_fldrs, mtc_dct_fldr_selves, mtc_dct_fldr_superfolder, \
		mtc_dct_superfolder_folders, mtc_dct_hash_best_superhash, mtc_dct_superhash_hashes, \
		mtc_dct_sized_superhashes

# --------------------------------------------------
# Interactive
# --------------------------------------------------

# Removed - all GUI interaction is now handled by the foldatry module
# If used separately this module will need to be passed command line parameters

# --------------------------------------------------
# Log controls
# --------------------------------------------------

def setup_logging() :
	# this uses the custom wrapper module "multilog"
	m_log = cmntry.make_multilog_object( mod_code_name() )
	return m_log

# --------------------------------------------------
# Main - only if run as a program
# --------------------------------------------------

# main 
if __name__ == "__main__":
	print( "This is the module: matchsubtry")
	print( "Currently no testing is setup for this module.")
