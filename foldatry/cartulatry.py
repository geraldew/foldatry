#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# cartulatry = catalogue a tree 
#
# FWIW, the name means:
# - A medieval manuscript register containing full or excerpted transcriptions of important documents, especially of originally loose, single-sheet charters.
# - A collection of original documents bound in one volume.
# - An officer who had charge of records or other public papers.
#
# This forms part of the foldatry application but can be used as an
# independent command line tool.
#
# --------------------------------------------------
# Copyright (C) 2024 Gerald Waters - Melbourne, Australia
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# --------------------------------------------------
# Imports - Standard
# --------------------------------------------------

from collections import namedtuple

import os as im_os
import os.path as im_osp

import copy as im_copy

import re as im_re

# --------------------------------------------------
# Imports - Custom
# --------------------------------------------------

import commontry as cmntry
import exploratry as explrtry

# import multilog

# --------------------------------------------------
# Support Custom
# --------------------------------------------------

def mod_code_name():
	return "cartulatry"

def mod_code_abbr():
	return "crtltry"

def mod_show_name():
	return "Cartulatry"

# --------------------------------------------------
# Feature Structure Definitions
# --------------------------------------------------

# --------------------------------------------------
# Support Stock
# --------------------------------------------------

# --------------------------------------------------
# Support Custom - i.e. mostly generic functions on simple types
# --------------------------------------------------

# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# Named Tuples
# --------------------------------------------------

ntpl_ProcRecord = namedtuple('ntpl_ProcRecord', 'fr_Path fr_Name fr_Size fr_When fr_Made fr_Modi fr_Used fr_Hash')


ntpl_FileRecord = namedtuple('ntpl_FileRecord', 'fr_Path fr_Name fr_Size fr_When fr_Made fr_Modi fr_Used fr_Hash')

# --------------------------------------------------
# custom functions for the Named Tuples
# --------------------------------------------------

def GetStringToShow_ntpl_FileRecord( p_ntpl_FileRecord ):
	r_s = hndy_str_of_boolean( p_tpl_FileFlags.ff_Name, "Name" )
	r_s += " " + hndy_str_of_boolean( p_tpl_FileFlags.ff_Size, "Size" )
	r_s += " " + hndy_str_of_boolean( p_tpl_FileFlags.ff_When, "When" )
	r_s += " " + hndy_str_of_boolean( p_tpl_FileFlags.ff_Hash, "Hash" )
	return r_s


def Filename_AddExt_Meta( p_fn ):
	return p_fn + ".meta"

def Filename_AddExt_Data( p_fn ):
	return p_fn + ".tsv"

# --------------------------------------------------
# Feature Support
# --------------------------------------------------

def choose_to_exclude_str_using_lst_regex( p_str, p_lst_regex ):
	for i_regex in p_lst_regex :
		if not im_re.search( i_regex, p_str) is None:
			return True
	return False

def for_lst_folders_do_traverse_and_export( p_lst_folders, p_IgnoreZeroLengthFiles, \
		p_lst_excl_folds, p_fullpath_fold_exclusions, p_lst_excl_files, p_fullpath_file_exclusions, p_run_name, p_multi_log ) :
	zz_procname = "for_lst_folders_do_traverse_and_export"
	#print( zz_procname)
	#print( "p_lst_folders" )
	#print( p_lst_folders )
	i_multi_log = im_copy.deepcopy( p_multi_log)
	cmntry.re_filename_multilog_object( i_multi_log, zz_procname )
	# define progress monitoring counts
	lcl_file_count = 0
	lcl_fold_count = 0
	# note that the following do NOT get the exact string, because each will be based on a different time
	i_this_run_ident = cmntry.bfff_datetime_ident_str()
	i_this_run_infix = i_this_run_ident
	# write the meta data file
	i_data_folds_filename = cmntry.dtff_make_data_filename( i_this_run_infix, mod_code_abbr() + "_Folders" )
	cmntry.file_in_home_text_new_list_as_data_head( i_data_folds_filename, [ "path", "run_ident" ] )
	for folder_path in p_lst_folders :
		cmntry.file_in_home_text_add_list_as_data_line( i_data_folds_filename, [ cmntry.pfn4print( folder_path ), i_this_run_ident ] )
	# write the header line for the file data
	i_data_files_filename = cmntry.dtff_make_data_filename( i_this_run_infix, mod_code_abbr() + "_Files" )
	cmntry.file_in_home_text_new_list_as_data_head( i_data_files_filename, [ "name", "size", "path", "run_ident" ] )
	# define the recursive internal function 
	def traverse_recurse( p_path, i_multi_log ):
		zz_procname = "traverse_recurse"
		# print( zz_procname + " path: " + p_path )
		# i_multi_log.do_logs( "dbs", zz_procname + " Checkfor traversing: " + p_path )
		nonlocal p_IgnoreZeroLengthFiles
		nonlocal i_this_run_ident
		nonlocal i_data_files_filename
		nonlocal lcl_file_count
		nonlocal lcl_fold_count
		lcl_fold_count += 1
		if cmntry.is_n_to_show_progess( lcl_fold_count ):
			pass
			i_multi_log.do_logs( "dbs", zz_procname + " Scanning folder #" + str( lcl_fold_count ) + ": " + p_path )
		if im_os.access( p_path, im_os.R_OK):
			# i_multi_log.do_logs( "dbs", zz_procname + " Checkfor traverse getting contents of: " + p_path )
			i_entries = explrtry.get_lst_sorted_os_scandir( p_path )
		else:
			i_entries = []
		# print( zz_procname + " i_entries: "  )
		# print( i_entries )
		for i_entry in i_entries:
			#i_multi_log.do_logs( "dbs", zz_procname + " Checkfor entry: " + i_entry.name )
			if i_entry.is_file(follow_symlinks=False):
				fpath = im_osp.join( p_path, i_entry.name)
				if p_fullpath_file_exclusions:
					i_excluding_file = choose_to_exclude_str_using_lst_regex( fpath, p_lst_excl_files )
				else:
					i_excluding_file = choose_to_exclude_str_using_lst_regex( i_entry.name, p_lst_excl_files )
				if not i_excluding_file:
					lcl_file_count += 1
					d_nam, i_ext = im_osp.splitext( i_entry )
					p_Size = im_osp.getsize( fpath )
					if cmntry.is_n_to_show_progess( lcl_file_count ):
						i_multi_log.do_logs( "dbs", zz_procname + " Scanning file #" + str( lcl_file_count ) + ": " + cmntry.pfn4print( i_entry.name ) )
					if ( p_Size > 0 ) or ( ( p_Size == 0 ) and p_IgnoreZeroLengthFiles ):
						cmntry.file_in_home_text_add_list_as_data_line( i_data_files_filename, [ cmntry.pfn4print( i_entry.name ), str( p_Size), cmntry.pfn4print( p_path ), i_this_run_ident ] )
				else:
					i_multi_log.do_logs( "b", zz_procname + " Exlcuding file: " + cmntry.pfn4print( i_entry.name ) )
			elif i_entry.is_dir( follow_symlinks=False ):
				i_new_subpath = im_osp.join( p_path, i_entry.name)
				if p_fullpath_fold_exclusions:
					i_excluding_folder = choose_to_exclude_str_using_lst_regex( i_new_subpath, p_lst_excl_folds )
				else:
					i_excluding_folder = choose_to_exclude_str_using_lst_regex( i_entry.name, p_lst_excl_folds )
				if not i_excluding_folder:
					traverse_recurse( i_new_subpath, i_multi_log )
				else:
					i_multi_log.do_logs( "bs", zz_procname + " Exlcuding folder: " + cmntry.pfn4print( i_entry.name ) )
	# main section
	i_multi_log.do_logs( "dbs", zz_procname + " Scanning for : " + str( len( p_lst_folders ) ) + " folders." )
	i_path_count = 0
	for i_path in p_lst_folders :
		i_path_count += 1
		#print( "i_path" )
		#print( cmntry.pfn4print( i_path ) )
		i_multi_log.do_logs( "dbs", zz_procname + " Traversing path " + i_path )
		if cmntry.is_n_to_show_progess( i_path_count ):
			i_multi_log.do_logs( "dbs", zz_procname + " Scanning listed path #" + str( i_path_count ) + ": " + cmntry.pfn4print( i_path ) )
		traverse_recurse( i_path, i_multi_log )

# --------------------------------------------------
# Feature: 
# --------------------------------------------------

def cartulatry_command_recce( p_lst_folders ):
	return cmntry.consider_list_of_folders( p_lst_folders, True )

def cartulatry_command( p_lst_folders, p_IgnoreZeroLengthFiles, p_lst_excl_folds, p_fullpath_fold_exclusions, p_lst_excl_files, p_fullpath_file_exclusions, p_run_name, p_multi_log ):
	zz_procname = "cartulatry_command"
	#print( zz_procname)
	#print( "p_lst_folders" )
	#print( p_lst_folders )
	for_lst_folders_do_traverse_and_export( p_lst_folders, p_IgnoreZeroLengthFiles, p_lst_excl_folds, p_fullpath_fold_exclusions, p_lst_excl_files, p_fullpath_file_exclusions, p_run_name, p_multi_log )
	#print( zz_procname + " Ended" )

# --------------------------------------------------
# Main - only if run as a program
# --------------------------------------------------

def setup_logging() :
	# this uses the custom wrapper module "multilog"
	m_log = cmntry.make_multilog_object( mod_code_abbr() )
	return m_log

# main 
if __name__ == "__main__":
	print( "This is the module: cartulatry")
	# setup logging
	i_multi_log = setup_logging()
	if __debug__:
		i_multi_log.do_logs( "b", "Invocation as Main")
	i_multi_log.do_logs( "dbs", mod_code_name() )
	i_lst_folder = [ "/home/ger/Downloads", "/home/ger/dev_tests" ]
	cartulatry_command( i_lst_folder, i_multi_log)
	i_multi_log.do_logs( "dbs", mod_code_name() + " Completed!" )
