import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="foldatry",
    version="0.9.9",
    author="Gerald Waters",
    author_email="geraldew@nosuchmail.com",
    description="A tool written in Python for automated tidying up of large amounts of files and folders.",
    long_description="The goal is unattended operation, that you will interact with the interface to set it up and to then run it, with it doing all of its work unattended.",
    long_description_content_type="text/markdown",
    url="https://gitlab.com/geraldew",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GPL3 License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
    install_requires=[
        'tkinter',
        'psutil'
      ],
)
