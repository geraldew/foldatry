#!/bin/bash
echo $0
echo "Setting up for Foldatry usage."
echo "Making folders"
mkdir -p ~/applctn
cd ~/applctn
echo "Making Clone from GitLab"
git clone https://gitlab.com/geraldew/foldatry.git
cd ~/applctn/foldatry
echo "Pulling from GitLab"
git checkout main
git fetch origin main
git reset --hard FETCH_HEAD
git clean -df
echo "Completed"

