#!/bin/bash
echo "Refreshing Foldatry."
cd ~/applctn/foldatry/foldatry
git restore .
git checkout dev
git fetch origin dev
git reset --hard FETCH_HEAD
git clean -df
