# Foldatry Change Log

Note:

- *Most recent items listed first.*
- *The idea here is to have a curated list of feature changes - matters internal to the code won't be addressed. Minor bug fixes will also be overlooked.*
- *At times this log may just be a mess of recently dumped git log lines, waiting for culling*

## 2024-09-15

In general, recent developments have been:

- doing an initial implementation of how to allow for the interruption of process
- this was done for the Prunatry process
- thus after setting a Prunatry process running, it is now possible to signal that it should be interrupted
- inside the Prunatry process are multiple places where it checks for an interruption signal
- hence, while not immediate, the process will cease looping, and execution will cascade upward through the call stack as it would at the conclusion of each layer of looping

Detailed commits over this time:

- Upped the version to v0.9.7.8.2
- Hacked in usages of the passed function p_fn_Check_NoInterruption to enable process interruptions
- Put in some consistent framing for the test implementation of RunnableProcess interrupt
- Improved naming of RunnableProcess state detection functions and added more
- changed the parameter name in background_loop from a p_ to a m_ (for mutable) Also started adding trial abstractions in prep of new thread signalling
- Added an abstraction function RunnableProcess_DoNotInterfere
- Some initial staging code to prepare for adding process interruption
- Committed (well, for now) to having the file comparison use the os hopefully-non-buffering method in the modified filecmp module
- Very minor string changes in the still-yet-to-be-used comparatry module
- Minor tidying of the change log
- Improved the Dev mode terminal display information
- Split up the filecmp old vs new difference handling into screen and log file
- Add logging of significant file comparison differences between the two methods
- Version to v0.9.7.8
- Removal of osbolete GUI settings code and general tidying of Develope mode coding
- Added a git restore to the refresh scripts - this will toss out any local changes before pulling updates
- improved some variable names
- Corrected data type setting for ClntryRecreateSourceFolder
- Moved a pair of return left by one indent, had been stopping for loops from looping
- Bumped to version v0.9.7.7
- Implemented and reorganised code for having and managing both AutoSave and Default settings
- Made a neater set of functions to cover the filerefs for default and autosave of settings


## 2024-07-22 

In general, recent developments have been:

- reworking of how the Settings are managed, with an internal Python code generation making it almost routine to add an extra setting as soon as it has a GUI widget and/or a use in the called modules. This was quite a bit of work wth no major effect on the user.
- the first idea in the Organitry tab has been enacted, and give an ability to analyse what things/locations might be worth runing Prunatry on - and inhenrently something that is safe to run. Previously, this was quite possible using existing features as long as their delection action was for making a script instead of direct action. I felt I wanted to definitely-safe equivalent, hence in a completely distinct place in the application - both in terms of the user interface but also in the program code.
- adding right-click events to clear all the input boxes has made a significant improvement in using the application

Detailed commits over this time:

- Have the NotInDevelopMode set by if __debug__
- Enabled settings enumeration value of TrmtryDoRenameInsteadOfDelete and then applied the (mostly) generated code snippets into place. Note: one part had to be done manually, and that was fed back into code generation improvements
- Improve the code construction outputs
- Added some better display lines for outputting the code constructions
- Corrected code construction in ForSettings_Generate_Python_Function_GetSetting to not do a parameter Added a code construction for ForSettings_Generate_Python_Function_GetSetting_ForCommandLine
- Removed three lines leftover from earlier debugging
- Made corresponding changes to the code generator functions for adding new Settings handlers
- Changed naming from SetSettingFromGui to SetSettingIntoGui and removed erroneous surplus pass-through function
- Added missing variable in assignment from _Dct_Fn_GetSettingFromGui in the checking and proving functions for New_SetSettingFromGui
- Corrected various SetSettingFromGui_ functions where they had .set instead of .setSelectedKey
- Turned on the new SetSettingIntoGui - by renaming the old
- Bumped version code to v0.9.7.6
- Updated all the copyright years to 2024
- Used the code generator module+functions to establish the source code for the dictionary-based handling of the setting the Settings
- Added a Python code generation module that will "write" source code fragments for requested Settings enumerations
- Finished catch-up application of Set_ESD_EMSA (in debug mode) to prove Settings consistency
- Considerable reworking of the Settings infrastructure, adding self-checks and half migration to using a dictionary of functions
- Change version to indicate Debug status
- Corrected duration exception calculation
- Made the experimental _do_cmp be used for debug mode, to run both methods and to then do duration timing and report when exceptionally different
- Changed version code to v0.9.7.4T with T for Temporary
- Added an alternative low level way to compare files, hopefully to avoid OS read caching - but needs to be confirmed as making any difference
- Added GUI widgets and parameter passing for p_DoDeleteEmptyFiles and p_DoDeleteEmptyFolds - this is before actually implementing those as features
- Corrected references to function for_path_get_fstype_set_charenc to get it from explrtry
- Corrected bad function call argument list in deforestry process_and_purge_by_superhash_from_largest when calling check_then_purge
- Version to v0.9.7.3
- Implemented right-click clear actions for all value and Browse pairs
- Added a right-click action to clear path settings in Prunatry
- Updated copyright year to 2024
- Bumped version to v0.9.7.2
- Fixed some typo bugs in Trimatry
- Changed data out file name extension to .tsv and replaced spots where .txt was still hard coded
- Corrected typo in Cartulatry from some Commontry function renaming
- Fixed the label for Canprunatry report limit setting
- Added folder size annotations to bash script generation
- Added a spin control for setting the opt n report limit for Canprunatry
- Bumped version to v0.9.7.1
- Made Organitry present as standard, instead of experimental
- Implemented canprunatry_discovery
- Reduced how much matchsubtry was directing logging to the Results tab
- Added GUI elements for Organitry, Canprunatry and created initial Organitry module
- Corrected variable name in smmtry_reduction_prepare
- Fix basic mistakes in smmtry_reduction but also, remove restriction smmtry_traverse_tree of not storing empty folders
- Began coding the smmtry_reduction concepts
- Minor wording change to display text
- Added log display of superfolder sizes when about to purge them
- Bumped version to v0.9.7.0
- Added buttons and action code for saving and loading of default settings
- Fixed bug with import and use of the shutil library
- Re-jigged the lgff functions to be about more than just logs, to be ready for implementing a default configuration load in the GUI mode So this meant making a new set of underlying bfff functions and adding new higher functions for config (cfgf_), data (dtgf_) and script (scrf_) filenames and folders
- Bumped version to v0.9.6.9
- Added reaction function for the Prunatry DeleteMode combobox to turn on stringent checking
- Updated README to add State of Play at the end
- Updated changelog - albeit by just dumping in a lot of commit comments
- Plotted out the future enumerations in EnumSettingDef and re-ordered some

## 2024-02-11 

- Plotted out the future enumerations in EnumSettingDef and re-ordered some
- _Bumped version to v0.9.6.8_
- In class Fltree changed add_child to add_new_child and added add_grown_child This goes with changes to summatry_command to combine the iterated folder list into a single tree with a dummy root Instead of just doing Summatry for each of the listed folders
- Fixed a couple of divide by zero bugs in Summatry
- Implemented another Add Subfolders feature
- _Bumped version to v0.9.6.7_ as the Add subfolders are now operational
- Implemented two more Add Subfolder features. Also did some renames and made some functions for the button texts.
- Renamed widgets dropping "dupefolderlist" and reverting to simpler "path" and "paths" This was because "dupe" was only meaningful for Defoliatry and the code had since been copied and adapted.
- Moved two functions from being among the widget response functions to being with the support functions
- Implemented t03_on_button_add_subfolders feature
- Revised colour selections and tidied some namings and a little prep of add subfolfer
- Added mention of Cartulatry to the Welcome tab
- _Bumped version to v0.9.6.6_ and made the Cartulatry tab permanent
- Added check boxes for whether Cartulatry regex matches should apply to full path and added implementation for it.
- Added side split with a managed list for potential folder exclusions
- Added passing and use of the run name for Cartulatry, now also writes a file listing the paths scanned
- _Bumped version to v0.9.6.5_
- Added feature of creating and using folder in the user's home for holding all the log files.
- For Cartulatry added GUI elements for ignoring zero length files and managing a list of exclusion strings
- Added support for ignoring zero length files in Cartulatry
- Added all the things needed to enact the new Cartulatry feature, at least as a start
- created new cartulatry module to perform disk cataloguing
- Added lgff_datetime_ident_str as an independent abstraction so that lgff_datetime_filename_str can in future be specialised
- Added a gui_tab_experimenting function to control whether experimental tabs are implemented
- Prepared a tab for a planned Cartulatry feature
- Attempting to make the initial window state work per platform
- minor update of (so far unused) setup.py file
- Tidied up psutil module importations
- Added Windows command scripts for hacky setup
- changed initial setting in root.attributes from zoomed to fullscreen
- In Veritry, added predicted total time to the logging along the predicted remaining time
- Increased the dictionary size limit for the class ProgressTrackObject
- Gave Veritry the ability to provide predictions of completion time
- Added hndy_GetHumanReadable_Seconds function and ProgressTrackObject class to commontry
- Renamed code elements in Veritry to be PathSetA and PathSetB instead of Disposals and Survivals
- Added and moved per-check logging so that progress indications are in the status tab
- Corrected the label text for a checkbox
- Added a function to produce a list with pfn4print applied to all elements of a list
- _Bumped version to v0.9.6.4_
- Added veritry module, adapted from prunatry, to do checks but no deletions Adjusted the GUI to better suit the new Veritry feature
- Added all the GUI elements that will be needed for new feature: Veritry
- Added pointless calls on the Migratry option checkboxes and disabled them
- Removed the "not yet functional" label for Migratry
- _Bumped version to v0.9.6.3_ (because Migratry seems to be working)
- Switched on the migratry_command internal code to begin test running it.
- Activated Migratry to be called from the GUI. Also made all the if DoNotRunProcessesAsThreads calls use the message queue to return widgets to normal
- moved the Skeletry functions to being in the code text before the Migratry ones
- Added success/failure logs inside migratry_command
- In sketched out code for Migratry, settled the default FnameMatchSetting values
- sketched out likely code for migratry_command - as a call of three existing functions also tidied up some other functions from their test phase - being inside an if clause
- Moved the tab for Skeletry to be before Migratry - because in effect the latter uses the former
- Enacted implementation of Skeletry
- Rewrote path_termite to be a nested non- and recursive pairing, mainly to use a single bash filename throughout
- Removed label that incorrectly said Synopsitry wasn't yet functional
- Updated version to v0.9.6.2 as Synopsitry is now enabled
- Implemented Synopsitry functionality
- Created copy_emptyfolders_of_folder_a_to_existing_empty_folder_b ready for use by Synopsitry Added more error trapping for Clonatry's copy_content_of_folder_a_to_existing_empty_folder_b
- _Bumped version to v0.9.6.1_ - as new Clonatry has been implemented
- Wrote work-around for the fact that shutil.copytree can't be told to use an existing destination folder
- Disabled Clonatry widgets for settings that are not yet optional
- Implemented the GUI side of the new (part of Replatry) Clonatry feature.
- Gave several of the still-empty Replatry command functions parameters to be received from the GUI
- Added a path_check_is_empty function to the exploratry module
- Add more Options widgets and labels to remind when empty destinations are required
- _Bumped version to v0.9.6.0_
- Corrected the boolean expression for the riskiness of Prunatry settings
- Added warning label to Prunatry about riskiness of immediate deletion mode without content checking
- Added red text warning for Defoliatry about unsafe immediate deletions Added GUI element for telling trimatry to rename empty folders instead of deleting Changed Prunatry checkbox for stringent analysis to off - because was wasting time by mistakenly leaving it on too often
- Added support for parameter about renaming empty folders instead of deleting them, no implementation yet
- Removed deprecated code from clonatry module and gave all new actions log entries of not implemented
- Deleted some deadwood from the Clonatry rejig and added NotImplemented dialogs for all the Replatry process buttons
- Commented out all of the old Clonatry tab code. Some parts may yet be repurposed for making the new Clonatry sub-tab in Replatry
- Added more RunStateMessage values to cover all the Replatry sub-tab actions Rewrote background_loop to directly access the widgets instead of being passed them all as parameters
- Marked some spots as DEPRECATED pending next edits
- Migrated the skeletonising call from the Clonatry tab to the Replatry tab. Reworked some some of the colour choices in the Replatry tab.
- Added support for a parameter of p_EnumDeleteMode for the path_termite in the clonatry module - for use by t14_Skltry_on_button_Process
- Prepared framing for replacing the single call to Clonatry passing the mode with independent functions
- Put in code for all the Browse buttons in the Replatry sub-tabs
- Customised the A B C labels for all the Replatry sub-tabs
- Copied out all the GUI elements for the remaining sub-tabs of Replatry - they still need to be customised
- Added a description label for each of the Replatry sub-tabs
- More setting out the Replatry sub-tabs before doing them all. Added an About sub-tab to act as an overview introduction to what feature each provides
- Extended EnumClonatryMode to have new values (though these will be dropped soon anyway) Introduced EnumAction as a better way to store the command line parameter action selectors Added the beginning of a sub-tab replacement for the combobox controlled Clonatry GUI t14 to replace t04

## 2023-11-24 

- Added status log lines into check_then_erode so that we see what files are being stringent compared before time is spent doing that
- Changed the stock app_file_prefix to be more brief
- More output filenames made conformant
- Change two more congruentry module filename constructions over to using cmntry.make_xxxx_filename
- created new `_action` congruentry functions to supplment the `_command` calls, and thereby allow usage from within other command calls that won't create new log runs
- Reworked the filename construction functions and how they are called so as to cover log, data and bash files consistently

## 2023-11-22 

- Changed version to v0.9.5.8 - note is prior to running functionality tests
- Changed each major action function call to make its own deepcopy of p_multi_log thereby resetting the time and subapp strings for its run Also removed all the pending temp code in the foldatry module that now will never be needed
- Partway through reworking the handling of the multilog object filenames and moving the cloning of the object to the called modules
- gave the MultiLog class an explicit init function, ready for adding additional instance variables probably to act as passed control/feature values into the modules rather than require the modules to import from the foldatry module
- Changed the self-annotation functions from app_ to mod_ and removed the obsolete ones for version etc
- Changed various per-process log items to quote the correct process
- Added a defoliatry_command_recce function and used it for the command line operation Also prepped some code for re-implementing a reset log file name per run

## 2023-11-19 

- Bumped version to v0.9.5.7
- Improved precision of timestamp strings for generated filenames
- Added more debug info for when requested folders don't exist - prompted by having a USB drive just drop off durng a run!
- Changed fcmp_for_two_files_compare_contents_get_bool to simply always not do shallow call of stock filecmp.cmp Added more enactions for handling stringent comparison failures
- Implemented the collection, return-passing and outputting of Congruentry stringent comparison failure
- Added run-time ability to control whether processes are run as threads. So, new checkbox in the Settings tab, new function to read from it and be the check call used to decided which way to do runs at each Process action
- Put in a threading bypass for use when debugging (as threads can cause unexpected errors to not show in terminal)
- Fixed some oversights from when the Enumerations revised and moved to applitry
- Moved the file system type discovery to the exploratory module (to avoid circular references in commontry)

## 2023-11-15

- Bumped the version to v0.9.5.6
- Tidied the widget layouts to fall in place from the left
- Removed the unused Do Export checkboxes
- Renamed the DoExport settings and expanded code coverage for the remaining Congruentry settings
- Tidied up the function calls for writing out simple data files
- Propagated p_ReportFailuresStringent into the direct file comparisons too - note that actual implementation is still pending
- did some timely function renaming, dropping the by-now-old _newer, and clearer names for the GUI button functions have added cct_ReportFailuresStringent into the places in congruenty where suitable actions need to be decided and coded
- Pushed the new checkbox for reporting stringent failures through to the congruentry function and added a corresponding checkbox to the Congruentry GUI tab
- Added checkbox for ReportFailuresStringent and pushed its value down into functions ready for having a feature added to the called of Congruentry
- Probably pointless comments as prep for adding traversal features
- Added collection of MaxNumFoldersInSomeFolder and FolderOfMaxNumFolders
- removed some odd chars at line ends
- Added average filesize per extension to results display

## 2023-10-21 

- Bumped version to v0.9.5.5
- Added PathNameOfMaxSizeFile as thing to collect during parse and report
- Added p_FolderOfMaxNumFiles and moved recursive def to be inside smmtry_traverse_tree
- for Summatry, have added handling for MinFileSize MinNonEmptyFileSize MaxFileSize and MaxNumFilesInSomeFolder
- Removed deprecated widget class TuplePairCombobox
- Corrected a display typo and added a direct link to the User Guide in the README
- Added and used more _recce functions and added command line calls for more of the process actions
- Created a consider_list_of_folders function for use in the _recce functions
- Added a help option to the command line parameters
- Improved the Wanted eroded and Did erode status output strings
- Corrected the long form command line options for two actions
- Applied hndy_str_to_goodpath when adding folders to a list
- Re-checked hndy_str_to_normpath and renamed to hndy_str_to_goodpath
- Improved then logging early setup and use for command line mode. Is now initially controlled by use of -0 python line parameter and then by the loaded settings
- Improved consider_path_str to show the actual path when fails

## 2023-09-30

Merged in several weeks of major reworks in a "dev" Git branch.
The two main focus areas were:

- restoration of Settings that can be saved to a JSON file and reloaded
- restoration of command line actions

To achieve those, quite a lot of work went in. Nearly all handling of Settings was moved to a new `applitry` module. Only the translation between the Settings object and the GUI Tkinter widgets is still in the `foldatry` module. The applitry module also now holds all the custom Enumerations and their support structures and functions.

The approach used for command line operations has been clarified:

- order of the parameters matters and represents the sequence of events to have happen
- the assumption is that instead of detail parameters, a settings file must be loaded and a parameter used for trigger a process.

Nearly all of those changes are "under the hood" with the only visible changes being:

- in the GUI, that there is a new list of Settings flag for the saving and loading of settings
- command line displays are now as controlled by the "logging" settings, with the Status log defaulting to standard output.

## 2023-08-12
- major reworking of the Wiki - files renamed and numerous text revisions

## 2023-08-07
- improved some widget layouts and frame colours

## 2023-08-06
- version now set at v0.9.5.0

## 2023-08-05

- Added a check and warning for risky operations in Prunatry

## 2023-08-03

The "dev" branch reworkings seem to be complete, and so now a round of testing will ensue before merging "dev" back into "main"

- Tidied all the sub-modules to no longer have command line options in their "main"

## 2023-08-02

- dev version now set at v0.9.4.5
- Removed large amounts of obsolete and deprecated code from deforestry
- Added forced linking of hashing and size for filedupe GUI & tidied away some old if False clauses
- Added function make_dct_matchkey_lst_pfn_in_both_sets for use in prunatry action to avoid pointless hashing
- Improved logging and variable name clarity in make_tpl_shortmatchkey and make_tpl_fullmatchkey
- Split out and created hasherdabbery from filedupetry_collect - and corresponding changes
- Fixed typo from renaming internal variable
- Added hash progress counter and logging to build_dct_hash_lst_pfn_from_dct_shortmatchkey
- Tidied the is_n_to_show_progess_timed code and reset is_n_to_show_progess_timed_stock to use minimum of 5 seconds
- Corrected use of GetSizeOfExampleInList rather than SizeOfExampleInList
- Bumped version v0.9.4.4
- Improved parameter list for process_and_erode_disposals_by_strategy_in_delorder and implemented key sorting for the filedupetry processing in prunatry
- Code line annotation of a currently non-used element
- Improved parameter order for prunatry_command + improved log strings
- Added handy functions GetStringToShow_tpl_FileFlags and hndy_str_of_boolean
- Added a ThisIsDeprecated function to deforestry to aid in removal of deprecated code
- Created more generic versions of the functions for getting example file attribute values from a list of filenames
- Added new function into Prunatry section for handling the new dictionaries
- Tidied names from strategy_mode to InGroupMode ; made the Prunatry settings similar to Defoliatry ; added label colour changing and improved their wording
- Implementation of simultaneous changes to enact the four-file-flag method of hashed had hashless filedupetry There are aspects of flotsam left in the code, to be proven obsolete and removed. Now needs fuller testing of Defoliatry, before then working on Prunatry use of the new features.
- Preparing for writing new process and erode function to suit new method
- Integrated the four boolean filedupetry controls to pass from GUI to test calls for filedupetry_collect Note that the actions in both Prunatry and Defoliatry still use the outcomes of the older function
- Changed the fourth dictionary built to be keyed on fullmatchkey and have lists of fileinfohash tuples. Also added debugging feeds
- Added test call (but not usage) of the new filedupetry method. Also picked up numerous places where pfn4print function should be applied before passing pathfilename to logging calls
- Large amount of reworking, mainly as set of parallel functions to implement the controlled matchkey approach to file duplicate detection
- Changed build_hash_dct_from_size_dct to not use a byref for the built dictionary
- Improved text in display of NOT viable with mode checkboxes
- Added hash_of_lst_str function as prep for future feature
- Added more DelOrderMode values, and made its GUI setting interact with the DefoliatryMode settings
- Added DefoliatryMode categories - values in commontry, GUI elements in foldatry, parameter reception in deforestry, note for adding concept to filedupetry
- clarity touch ups - no actual change
- Clarifying renaming of variables and placement of recursive traversal inside the non-recursive call
- Bump dev to v0.9.4.3

## 2023-07-09

- Forked to a "dev" branch during major reworking of the filedupetry and deforestry modules, mainly about changing from just hash based matching to combinations of hash, size, name and when

## 2023-07-08

- a lot of re-organising as prep for the big enumerations rejig to come
- A bit more renaming and some prep for the big rejig of enumerations to come
- Added an enumeration for DefoliatryMode even it only has one value - i.e. future plannning
- Removed invalid and redundant assignment
- Some internal renaming to match the new tab scheme, for less confusion

## 2023-04-14

- this was the last push to GitLab as a stable version for general use - after this, changes were committed but not pushed.
- bumped version number and hid the Organitry tab pending implementation of features there
- Snall improvement in log display in congreuntry

## 2023-04-09

- Preparations for getting deforestry/defoliatry calls to be told what FnameMatchSetting values to use when calling congruentry functions. Thus to be ready when these are added to the GUI and/or use detection of file system types
- Big cleanout of obsoleted code
- Updated screenshot
- for congruentry wrote and switched to the Phase 2 functions
- Fixed typos and unsurprising errors from preparations for congruentry object passing from the GUI Bumped version code
- Additions to prepare for a staged rewrite of the Congruentry paths function. So this includes short-lived functions get_NameMatchSetting_from_FnameMatchSettings in commontry and congruentry_paths_command_newer_phase_1 in congruentry and comments to cover the transition writing
- Congruentry - Added the B counterpart of the Path file system archetype GUi elememts
- Implemented Congruentry custom File system Archetype selection with hiding and displaying of frames
- Rearranged and added to the enummerations amd related structures to enable rejig of Congruentry controls
- Changed the default window size to fit things at font size 10
- Added comment annotation about the tab prefix coding
- Some tidying and bump version to 0.9.4
- renamings - change function derepfiltry_command to defoliatry_command
- renaming - change over defoliatry prunatry derepfiltry
- renaming - more removal of prune references
- Renaming - multiple modules - remove all use of prune
- Renamings - purgetry module - converted all prunes to purges
- renamings - re ordered the tabs
- Renamings - changed tab display names
- renaming - defoliatry to deforestry - module nane reference
- renamings - defoliatry to deforestry - module name reference
- renamings - defoliatry to deforestry - file renamed
- renamings - defoliatry to deforestry - function name changes
- Renamings - prunatry to purgetry - changed module name references
- renaming - prunatry to purgetry - files renamed
- Part of rename process - prunatry to purgetry - function name changes
- Added checkboxes for prune_from_deep and CanDeleteTopFolders
- Added prune_from_deep option
- Made a custom data type for holding the empties in Trimatry
- Passing parameter for CanDeleteTopFolders into Trimatry
- Rearranged and reduced the required window width in Summatry and Trimatry
- Added license reference for the modified filecmp module
- Bumped to version v0.9.3.8
- More corrections to the Stringent logic
- Changed use of st.st_mtime in metadata signature as filesystems vary in their timestamp precision
- Pulled in copy of the stock module filecmp to tackle mystery bug of files always being fully compared
- Comments added as prep for next works on Trimatry
- Added GUI element for TreatNestedEmptyFolders and associated parameter passing
- Added initial implementation of the trimitry command
- Fixed an oversight where one Browse for folder wasn't being fed by the previous path
- Created the bare trimatry module in which to implement trimatry
- GUI elements prepared for Trimatry features (despite no implementation of actual functionality yet)
- Relabelled the Welcome tab to be smaller as prep for adding another tab
- Added function str_of_tpl_Extn_Info to display the tuples  in the log windows
- Created hndy_GetHumanReadable_Count and renamed the existing related function to hndy_GetHumanReadable_Bytes
- Bumped version code
- Changed the by-file-extension to also collect file sizes
- Improved defoliatry logging for when matches were found but could not be pruned for being on only one side of disposal-survival
- Brought examples up to the status log when defoliatry found matches but only on one side of disposal-survival split
- Fixed crash bug in hashing of folder list contents where non UTF-8 names weren't being handled
- Removed obsolete Tkinter usage in sub-modules - since that all happens in the foldatry module
- In prunatry replaced local function bash_script_file_add_line with call to same one in commontry
- Fix bug where the GUI code wasn't pulling out the selection of InGroupMode for passing in DeRepFiltry
- Ensure Prunatry uses cmntry.pfn4bash before sending a bash line to file and that all the comment lines use cmntry.pfn4print
- Made the logic of setting the log flags and then their settings checkboxes clearer and more correct
- Made a spot fix for the Progress log window not being On by default
- Brought the copyright years up to date
- Brought the light touch pass status notice back and improved the progress display
- Added extra checking dialog for when doing a White Ant deletion

## 2023-03-03

- Slight improvement to what gets shown when attempting Clonatry options

## 2023-03-01

- Tidied the Defoliatry GUI elements to be one less line
- Put progress monitoring into the Counts by filename extension feature 
- Added new feature - Counts by filename extension - into the Summatry tab
- MAJOR BUG FIX - Corrected logic in fcmp_for_two_files_compare_contents_get_bool and bumped version
- Added display of the internal UCS mode of Python - visible to users but not there for them,
- MAJOR BUG FIX - Was missing the making of bash copy script for the B to A copying of missing files
- Removed nearly all documentation from the code repository (was already copied to the wiki repository)

## 2023-01-01 (approximately)

Bookmarking a period of changing and trialling how to manage the use and control of use in Congruentry for handling various name match schemes. This is ongoing as no situation so far is proving satsfactory. This corresponds to multiple layers of changes in the source code as well as GUI experiments for which controls and options should be visible in which parts of the program (notably either in Congreuentry or Settings sections). 

## 2022-08-11

- Added sub-path clash check for Congruentry, and disabled checkboxes for bad windows
- Made the CheckFor action run in a thread
- Reworked the frame usage of CheckFor and added check boxes for the options
- Added writing of findings to text file for Summatry CheckFor

## 2022-08-07

- finally added a CHANGELOG !
- updating documentation
- added a Check For feature into the Summatry tab to find names invalid for Windows/DOS

## 2022-08-06

Now have the Congrentry section dealing well with alternative character encodings. In short, this meant adding:

- ability for the user to specify the encoding for interpreting eff names
- some detection of file system types
- some automated setting of character encoding based on a detected file system type
- ability to generate a copy script - so as to fill the gaps left by an incomplete copy
- that the copy script will do character translation - i.e. from one encoding to another
- that a folder comparison can attempt to map character translations - so that having done the copies with the generated copy script, that Congruentry can work out which should be considered as the same names

Adding the handling for character encodings has been a major undertaking, requiring a lot of experimentation. It is now at the "is working" stage - it will remain to be seen how these issues might be taken up for use in the other areas of Foldatry.
