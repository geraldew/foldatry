echo "Setting up for Foldatry usage."
cd %homedrive%%homepath%
echo "Making folders"
mkdir applctn
cd applctn
echo "Making Clone from GitLab"
git clone https://gitlab.com/geraldew/foldatry.git
cd applctn\foldatry
echo "Pulling from GitLab"
git checkout main
git fetch origin main
git reset --hard FETCH_HEAD
git clean -df
echo "Foldatry setup completed."
echo "Note that if you get an error about the psutil module then run the foldatry_setup_pip.cmd"
