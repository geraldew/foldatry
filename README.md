# Foldatry

Foldatry is a tool written in Python for automated tidying up of large amounts of files and folders.

The major concept is that this is *not* designed to be an interactive tool in the way that most "duplicate" finding tools are. While most of those let you tell them where/what to check, they then run the checks **and then** let you interact to choose what to approve for deletion.

Instead, the goal here is unattended operation. The idea is that you will interact with the interface to set it up and to then run it, with it doing all of its work unattended.

Or, its actions can be deferred by having it generate an action shell script - which can then be perused in a text editor to manually adjust the actions.

By default it will open into a GUI mode but it can also be run as a command line operation.

This README is just the repository overview.

See [the Foldatry Wiki](https://gitlab.com/geraldew/foldatry/-/wikis/home) for the [User Guide](https://gitlab.com/geraldew/foldatry/-/wikis/userguide) and various development details and notes.

The only other support annotation kept with this code repository is the [Change Log](CHANGELOG.md) file.

![screenshot](screenshots/screenshot.png)

## Current status

Current status: It is now mainly functional and being used by its creator. It is not quite ready to be packaged for "beta" deployment and so this has not yet happened.

## Software License

Foldatry is Free Open Source Software (FOSS) using the GPL3 license.

As a note to those who may find that concerning, let me be clear:

- the author has followed the evolution of FOSS since its birth and is well versed in the concepts;
- Yes, I do know that GPL is not the norm for Python applications;
- Copyright - and thus the license - applies to my specific code, so a fork of this source code has to also respect and apply the GPL;
- FOSS means you are free to study the code and use what you learn to write your own software, where you can thereby choose a license. No need to ask, please go ahead and do that (but see below about naming).

## Naming

Copyright (and thus the chosen license) doesn't really cover the titles of things; such being usually a matter of "trademark" and other legal concepts, which vary considerably around the world. The names used here were chosen to be distinct, so re-using the top-most name "Foldatry" without an agreed arrangement will just confuse people. So maybe don't do that. Alas, the way that trademark laws work usually favours whoever pays money for registering a trademark, so unless/until this project gains some patronage that won't be happening.

In short, ruthless actors can make life difficult, and this text is only here to make it clear what the answer would be to AITA questions from any that do so.

If you haven't twigged, the names are all a weak pun on something-tree as they work on file and folder tree structures. It's lame but nicely led to somewhat unique names. 

## Installation

Being written in Python it therefore relies on having Python installed on a computer - however it uses only "stock" modules that are likely to be present in most installations of Python version 3. At time of writing development was happening on an instance of Python version 3.10 - but it is very likely that it only requires a Python 3 version from 3.7 onwards (or perhaps even earlier).

As the author only uses Foldatry on Ubuntu Linux setups, currently the only "installation" method is something that works for that environment. It thereby assumes the ability to use `bash` command scripts.

The "installation" is currently aimed at a single user account, but may require one step to be done as an admin for the whole computer. I don't know enough about managing the Python "virtual environment" system to cater for that.

As an overview, my method acts by:

- as a per-system thing - providing the Ubuntu/Debian advice for ensuring that the Tkinter library installed for the whole computer
- as a per-user thing - creating a sub-folder in the user's home folder, then using Git to load the Python program from GitLab
- providing a bunch of bash scripts for running the program from its sub-folder and also for refreshing it from GitLab

Consequently, for parts of the following, it is assumed that Git is already installed.

I'm currently using a very, very basic method - a small set of bash scripts in the `bash_temp` folder of the repository. They are:

- `apt_installs_for_foldatry.bash` - which use `sudo` to add the required Python modules - i.e. to run as an admin. Despite the ominous tone there, this is really just about adding support for the Tkinter library needed for the GUI - some Python installations will already include this;
- `foldatry_setup.bash` - which if run in a user home folder will setup a suitable folder and then clone the code from GitLab;
- `foldatry_refresh.bash` - which will bring in a latest copy from GitLab;
- `foldatry_refresh_dev.bash` - which will bring in a latest copy of the `dev` branch from GitLab;
- `foldatry_run.bash` - which will run the program NOT in debug mode
- `foldatry_run_debug.bash` - which will run the program with Python in (normal) debug mode

This set will surely be replaced by more usual Python installation methods sometime.

This also means that I have zero advice for installation or use on Microsoft or Apple platforms, as I simply don't use those so don't have advice to give for them. Ditto for Android or other Unix descended platforms.

- *post script* - there is now an alternative folder of scripts for Windows usage - `cmd_temp` which has Windows `.cmd` versions of the bash scripts. While these have been written and trialled, I would not call them "tested" so consider them unsupported.

For now, the way to use those is:

First

- if the Linux is *not* known to have the required dependencies, then an "admin" able account must be used to run the script `apt_installs_for_foldatry.bash` - which will need to be run with `sudo`
    - if not using a Debian based Linux then you'll need to work out how to do the equivalent for yours.

Then

- in the account that will be using Foldatry, download the script `foldatry_setup.bash` and execute it e.g. with the command 
- `bash foldatry_setup.bash`

This will create a folder in which to place a copy of Foldatry and use Git to fetch it from the GitLab.com repository. Currently the folder location is `hard coded` but is easy to amend by editing the script before running it.

The other scripts can then be run to either:

- run Foldatry with no debugging
- run Foldatry with debugging enabled
- refesh the Foldatry copy from the repository.

Note:

- all those bash scripts currently all assume the same hard coded location.

## See Also

As noted above, most of the documentation has now been moved to [the Foldatry Wiki](https://gitlab.com/geraldew/foldatry/-/wikis/home) on GitLab.com

That incldues both user information as well as under-the-hood development notes.

## State of Play

This is a shortlist of the features awaiting development or significant improvements to functionality. The idea is to have something brief that's stored with the code repository. Unlike the more general roadmap document in the wiki, this will be terse.

- Prunatry = all good
- Defoliatry = all good
- Trimatry = **has some issues about deleting topmost folders**
- Replatry = only some parts implemented, and those only just
	- Distinctry = _not yet implemented_
	- Clonatry = requires destination to be empty
	- Dubatry = _not yet implemented_
	- Mergetry = _not yet implemented_
	- Synopsitry = requires destination to be empty
	- Skeletry = **needs clarity about warnings**
	- Migratry = requires destination to be empty
	- Movetry = _implementation is in progress_
	- Shiftatry = _not yet implemented_
	- Shiftafile = _not yet implemented_
- Organitry = some parts implemented, otherwise awaiting concrete plans, perhaps akin to my bash scripts
	- Canprunatry = initial implementation, by adapting a subset of Prunatry
- Congruentry = all good
- Veritry = all good
- Summmatry = all good
- Cartulatry = **needs to have the semantic meaning of runs added to the data recording** also, _clarify the exclusions and inclusions as functional_
- Settings = the list of settings that cab be saveed/loaded is now better integrated into how the application is written
- log tabs = _lots of detail changes required to get the right mix as processes run_

Overall = it needs a more consistent approach to _data_ exports of process runs - i.e. not the logging, but data-ready files that can be routinely imported into a database for reporting and analysis. For this, there is currenly a discord between the existing method that ensures the outputs of each run do not overlap with prior runs, and a desire to have an automated importation into an other database.
